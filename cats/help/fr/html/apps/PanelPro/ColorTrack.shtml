<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="fr">
<head>
<!-- Copyright Bob Jacobsen 2008 -->
<!-- $Id: ColorTrack.shtml,v 1.2 2008/02/17 03:08:19 jacobsen Exp $ -->
<!-- Translated by Blorec Hervé le 2011-10-01-->
<title>JMRI: Displaying Colored Trackwork</title>

<!-- Style -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="/css/default.css" media="screen">
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print">
<link rel="icon" href="/images/jmri.ico" TYPE="image/png">
<link rel="home" title="Home" href="/">
<!-- /Style -->
</head>

<body>
<!--#include virtual="/Header" -->

<!--#include virtual="Sidebar" -->
<div id="mainContent">


<h1>JMRI: Affichage des Voies Colorées</h1>

Ceux qui font du modélisme de chemin de fer moderne pourraient vouloir un panneau de contrôle 
qui affiche une Vue "schématique" de leur réseau de chemin de fer, avec des segments individuels 
qui changent de couleur en fonction de l'occupation des voies.
PanelPro peut le faire pour vos panneaux. Il existe deux approches de base:

<UL>
<LI> Utilisez le nouvel
"<a href="../../../package/jmri/jmrit/display/LayoutEditor.shtml"> Editeur de Panneau </a>"
pour créer votre panneau. Il vous permet de
<a href="../../../package/jmri/jmrit/display/EditTrackSegment.shtml"> dessiner des segments des voies </a>
et
de <a place href="../../../package/jmri/jmrit/display/EditLayoutTurnout.shtml"> placer des aiguillages  </a>
sur l'écran, puis de les configurer afin qu'ils
changent de couleur en fonction de <a href="../../../package/jmri/jmrit/display/EditLayoutBlock.shtml"> l'occupation </a>.

<LI>Si vous utilisez l'"éditeur de panneau" original, vous pouvez également configurer des icônes capteur pour changer 
la couleur de votre voie. Cette technique est décrite sur cette page.
</UL>

<H3>Arrière Plan</h3>

PanelPro détecte l'état de choses sur le circuit, y compris si une voie est occupée, en utilisant des «capteurs». 
Selon le matériel (système DCC, etc) que vous avez sur votre réseau, ces dispositifs peuvent être différents, mais 
pour autant que le programme le sache, ils ont tous un travail identique: dire au programme que le détecteur de 
canton est dans l'un des quatre états:
<UL>
<LI>INACTIF (Inactive) - Le segment de voie est libre
<LI>ACTIF - (Active) Le segment de voie contient tout ou partie d'un train
<LI>INCONNU (Unknow) - La programme ne  connaît pas actuellement l'état de ce capteur, généralement
parce que le programme vient de démarrer et n'a pas encore vu son message
<LI>INCOHERENT(Inconsistent - Une erreur s'est passé, et plutôt que de vous donner une valeur qui peut
être erroné, nous disons simplement que nous sommes confus
</UL>

Pour un "SensorIcon" particulier à un certain endroit sur l'écran, PanelPro associe une image différente à chacun 
de ces états. Lors des changements d'état, la bonne image est placée sur l'écran.
<P>
Donc, le problème de base est d'avoir les bonnes images à mettre sur l'écran
<P>


<H3>Les Instructions pas à pas</h3>

<OL>
<LI>
Dessinez l'arrière plan de base de votre panneau, y compris les voies. Obtenez les
couleurs, positions, etc correctes, parce que revenir plus tard nécessitera de refaire
plusieurs étapes.
<br>
<IMG src="../../../images/tracksegments/WhiteTrack.gif">
<LI>

Prenez votre programme de dessin préféré et découper les petits morceaux d'image qui correspondent à des blocs. Si vous 
avez plusieurs blocs de mêmes formes et tailles exactes , vous n'avez qu'à en faire des copies.
<br>
<IMG src="../../../images/tracksegments/WhiteArc.gif">
<IMG src="../../../images/tracksegments/WhiteLine.gif">
<IMG src="../../../images/tracksegments/WhiteTO.gif">

<LI>
Vous pourrez utiliser ces images pour les colorées pour voir l'occupation. Par exemple, si vous voulez les avoir jaunes 
indiquant que la voie est occupée, blancs indiquant voie libre, et rouges indiquant des erreurs, vous devez copier les 
nouvelles images et créer des versions avec des couleurs appropriées. (Note: Assurez-vous de conserver une copie de l'image 
originale de tous les segments de voies qui contiennent un aiguillage)
<BR>
<IMG src="../../../images/tracksegments/YellowArc.gif">
<IMG src="../../../images/tracksegments/RedArc.gif">
<IMG src="../../../images/tracksegments/WhiteArc.gif">
<lI>
Maintenant, créez le panneau à l'aide de ces images, comme décrit dans les
<A HREF="PanelPro.shtml"> tutoriels </a>..

</OL>

Pour les aiguillages, la situation est un peu plus compliquée. La plupart des gens voudront représenter les <i> deux, 
</i>l'occupation <i> et </i> la position de l'aiguillage. Nous avons besoin encore de quelques étapes pour le faire.

<OL>

<LI>
Créer des image de capteur pour les Voies tel que décrit ci-dessus

<OL>

<LI>

<BR>
<IMG src="../../../images/tracksegments/YellowTO.gif">
<IMG src="../../../images/tracksegments/RedTO.gif">
<IMG src="../../../images/tracksegments/WhiteTO.gif">
<LI>
Maintenant, nous créons des images qui vont se modifier pour voir les position de l'aiguillage. Nous voulons 
créer des images qui sont transparentes où les voies, à l'exception d'une couleur de fond un peu plus claire, 
représentent les position de l'aiguillage.
<BR>
<IMG src="../../../images/tracksegments/TranTOc.gif">
<IMG src="../../../images/tracksegments/TranTOt.gif">
<BR>
Ici la couleur grise représente les parties transparentes de l'image; si on le place sur une des images du capteur, 
la couleur des capteurs de voie brillera à travers.

<li>
Maintenant, créez le panneau à l'aide de ces images, comme décrit dans les  <A  HREF="PanelPro.shtml">tutoriels  </a>. 
Le fonctionnement normal (que nous allons changer ci-dessous) est d'avoir des capteurs à l'avant des aiguillages à 
l'écran. Cela signifie qu'il sera plus facile si vous ajoutez la position des icônes des aiguillages d'abord, 
puis les icônes du capteur.

<LI>
Pour déplacer les icônes capteur derrière les icônes d'aiguillage, vous devez éditer le fichier de panneau et de 
changer leur "niveau". Ouvrez votre fichier dans un éditeur de texte (pas Microsoft Word, qui va essayer d'interpréter
les XML et faire des choses incroyablement stupides avec eux).

<LI>
Trouvez les lignes de votre capteur et de l'aiguillage en recherchant le nom du capteur et de l'aiguillage, par exemple, 
LS33 et LT120. Vous trouverez deux lignes (qui peuvent être séparées, selon la façon dont vous avez créé les groupe) qui 
ressemblent à ceci:

<PRE>
    &lt;turnouticon turnout="LT120" x="20" y="30" level="7" 
closed="resources/icons/smallschematics/tracksegments/os-righthand-west-closed.gif" 
thrown="resources/icons/smallschematics/tracksegments/os-righthand-west-thrown.gif" 
unknown="resources/icons/smallschematics/tracksegments/os-righthand-west-unknown.gif" 
inconsistent="resources/icons/smallschematics/tracksegments/os-righthand-west-error.gif" 
rotate="0" forcecontroloff="false" class="jmri.jmrit.display.configurexml.TurnoutIconXml" /&gt;
    &lt;sensoricon sensor="LS33" x="20" y="30" level="10" 
active="resources/icons/smallschematics/tracksegments/circuit-occupied.gif" 
inactive="resources/icons/smallschematics/tracksegments/circuit-empty.gif" 
unknown="resources/icons/smallschematics/tracksegments/circuit-error.gif" 
inconsistent="resources/icons/smallschematics/tracksegments/circuit-error.gif" 
rotate="0" forcecontroloff="false" momentary="false" class="jmri.jmrit.display.configurexml.SensorIconXml" /&gt;
</PRE>

Notez le «niveau» des attributs qui sont à 7 et 10 respectivement. (Les nombre supérieur sont plus vers l'«Avant», et cacher 
des choses avec des valeurs «inférieur »). Juste échangez ces valeurs, et enregistrez le fichier.
</OL>


Puisque vous couper les petites images à partir de votre diagramme de voie, elles devront être retournées ensemble de 
façon transparente sur l'écran.
<P>
Maintenant le panneau doit afficher les icônes pour le capteur d'occupation et l'aiguillage qui fonctionnent indépendamment 
et montrer à la fois les position (en suivant les ligne) et les occupation (grâce à la couleur) de cet aiguillage.  

    
<!--#include virtual="/Footer" -->
</body>
</html>

