<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<!-- $Id: SetSignalsAtBoundary.shtml,v 1.5 2010/01/09 22:25:56 jacobsen Exp $ -->
<!-- Translated  by Blorec Hervé le 2011-12-22-->
<html lang="fr">
<head>
 <TITLE>
      JMRI: Set Signals at Block Boundary Help
 </TITLE>
    <META http-equiv=Content-Type content="text/html; charset=utf-8">
    <META content="Dave Duchamp" name=Author>
    <META name="keywords" content="JMRI help Layout Editor Tool">

<!-- Style -->
  <META http-equiv=Content-Type content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="/css/default.css" media="screen">
  <link rel="stylesheet" type="text/css" href="/css/print.css" media="print">
  <link rel="icon" href="/images/jmri.ico" type="image/png">
  <link rel="home" title="Home" href="/">
<!-- /Style -->
</head>

<body>
<!--#include virtual="/Header" -->
  <div class="nomenu" id="mBody">
    <div id="mainContent">

<H1> Définir des Signaux aux  Frontières de Canton</H1>

<h2> Introduction</h2>
<p>
Cet outil fournit une procédure automatisée pour l'attribution des signaux à une frontière de canton qui
 n'est pas associée à un aiguillage ou un croisement. Cela se produit lorsqu'un
tronçon de voie en provenance de cantons différents rejoignent un point d'ancrage. Si demandé,
cet outil va placer l'icône du signal sur le TCO
 à la limite des canton spécifiés, et sera mis en place une Logique  simple
Signal pour les signaux spécifiés. Avant d'accéder à cet outil,
les cantons doivent être définies pour les régions de la voie signalée sur le TCO, et
les signaux requises doivent être dans la table signal. Cet outil ne fonctionne
correctement que lorsque voie à travers le point d'ancrage est verticale ou horizontale (ou
essentiellement verticale ou horizontale), alors le tracé de votre TCO est en accord .</p>
<p>
Lorsque cet élément est sélectionné, une boîte de dialogue est affichée pour l'entrée des
noms des deux cantons dont la limite doit être signalée, et pour l'entrée des
noms (système ou utilisateur) d'un ou deux signaux pour être affectées à la frontière du
canton . Pour chaque signal, des cases à cocher sont disponibles pour
sélection si une icône doit être placée sur le TCO, et si une Logique Simple
Signal doit être créée pour le signal. Peu importe si les icônes
sont placées ou si la logique est créée, l'Éditeur de TCO va enregistrer que les entrées
signaux sont affectées à la limite de canton spécifié.</p>

<h2>La boite de Dialogue deConfiguration des Signaux auxs Limites du Canton</h2>
<p>
Dans les deux premiers champs nom du canton, entrez les noms des deux cantons qui se
rejoignent à la limite du canton. Si l'outil ne peut pas trouver de cantons avec ces noms
sur le TCO, il en résultera un message d'erreur. Aussi, si les deux cantons ne se joignent pas
à un point d'ancrage sur le TCO, un message d'erreur en résultera.</p>
<p>
Les noms desSignaux (soit système ou utilisateur) sont entrés dans la section suivante.
Au moins un nom de signal doit être inscrit. Si les signaux à cet
limite de canton ont été saisis précédemment, cliquez <b>Obtenir Sauvegarde</b> pour
récupérer les noms des signaux précédemment saisis. L'entrée supérieure,
<b> Limite Est (ou Sud) </b>, est  le signal vu par un train allant vers l'Est
 (ou le sud si la voie est à la verticale). "Est-» se réfère
à un train allant de gauche à droite <i> sur le TCO</i> au niveau du canton
frontière. De même, «au sud» se réfère à un train allant du haut vers le
 en bas<i> sur le TCO</i>. De même, l'entrée de le second signal,
<b>Limite Ouest (ou nord)</b>, c'est pour un signal vu par un train vers l'ouest,
(Ou Limite Nord) . Parce que les icônes de signal sont correctes pour des voie
horizontales ou verticales, vous devez configurer votre TCO de sorte que les cantons
frontières apparaissent sur une voie qui est presque horizontale ou presque à la verticale.
</ P>
<p>
Remarque: S'il y avait un signal précédemment attribué à l'une des deux
positions, et vous entrez un <i>autre</i>  signal dans le même lieu,
le signal remplacera le signal précédent, et l'icône de
le signal précédent (s'il y en a une) sera supprimée à partir du
TCO.</p>
<p>
Cochez <b> Ajouter icône de signal au TCO</b> pour demander que l'icône d'un signal
 soit placée sur le TCO à la limite de canton. Cet outil peut seulement
placer une icône de signal sur la voie qui est presque à la verticale ou presque horizontale.
Si ce n'est pas le cas, un message est imprimé, et vous devez placer le signal
manuellement en utilisant l'icône de la barre d'outils Éditeur de TCO. Peu importe, le nom du signal qui est affecté à la limite de canton.</p>
<p>
Cochez <b> Configurer Logique</b> pour demander que l'outil mette en place une Logique Simple
Signal pour un signal. L'outil permettra de créer la logique et de 
remplir automatiquement les entrées avec les l'informations disponibles. Si
l'outil ne possède pas assez d'informations disponibles pour configurer la
logique, un message en résultera, et vous aurez à revenir plus tard après que
plusieurs signaux aient été assignés aux aiguillages et aux limites de canton sur le
TCO.</p>
<h2> Notes</h2>
<p>
Lors de la configuration logique, cet outil va suivre la voie dans le
 canton jusqu'à ce qu'il trouve un signal à la fin du canton loin de la 
limite du canton signalé. Si votre réseau contient des sections qui sont
signalées et des sections qui ne le sont pas, pour obtenir que cet outil mette en place une logique
correctement, vous pourriez avoir à placer un signal virtuel (un signal qui ne
correspond pas à un signal réel sur le réseau) 
non signalé la fin du canton qui relie un tronçon de voie qui est
signalée avec une voie qui n'est pas signalée. Si un canton se termine par un
Heurtoir, aucun signal n'est requis à la fin.</p>
<p>
Si un canton a un aiguillage interne (l'aiguillage et les tronçons de voie à sa
pointe et les les deux voies Déviée et Directe font partie du canton ), le programme va attendre les signaux de cet aiguillage, même si ce n'est pas à la fin du canton. Cependant, parfois
 l'utilisateur peut ne pas vouloir signaler un aiguillage rarement utilisé dans un canton.
En suivant la voie à travers un canton, le programme va sauter l'aiguillage interne  non signalé si <b> Passer Aiguillage Interne Non Signalés</b> est coché dans le menu <b>Outils</b> de
l'Éditeur de TCO .
Il avertira toujours, de ce qu'il fait. <b> Utilisez cette option avec
prudence.</b> Il n'y a pas de signal de protection contre un aiguillage ignoré
mal réglé, si un aiguillage interne non signalé n'est pas réglée correctement,
un déraillement des trains pourraient en résulter.</p>
<p>
Cet outil met en place trois aspects de signalisation. Si vous avez une situation particulière
à un signal, ou bien vous voulez quatre aspect de signalisation ou un aspect d'approche, vous
devrez modifier les informations entrées par l'outil. Le dialogue de Simple Logique Signal 
 peut être consulté à partir du menu contextuel de l'icône de signal. Si
votre situation particulière ne peut pas être manipulée par une logique simple signal, vous drevez vous
reporte à Logix pour adapter le signal logique à vos besoins.</p>

<!--#include virtual="/Footer" -->
</body>
</html>

