<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<!-- $Id: SectionTable.shtml,v 1.6 2010/03/30 15:47:52 djduchamp Exp $ -->
<!-- Translated by Hervé Blorec le 2012/01/07-->
<html lang="fr">
<head>
 <TITLE>
      JMRI: Section Table Help
 </TITLE>
    <META http-equiv=Content-Type content="text/html; charset=utf-8">
    <META content="Dave Duchamp" name=Author>
    <META name="keywords" content="JMRI help Section Table">

<!-- Style -->
  <META http-equiv=Content-Type content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="/css/default.css" media="screen">
  <link rel="stylesheet" type="text/css" href="/css/print.css" media="print">
  <link rel="icon" href="/images/jmri.ico" type="image/png">
  <link rel="home" title="Home" href="/">
<!-- /Style -->
</head>

<body>
<!--#include virtual="/Header" -->
  <div class="nomenu" id="mBody">
    <div id="mainContent">

<H1>Sections  et la Table  Sections
</H1>
<H2> Qu'est-ce qu'une Section?
</H2>
<P> Une <b>Section</b> est un groupe de un ou plusieurs <b>Cantons</b> connectés qui peuvent être
attribués à un train circulant dans une direction donnée. Les sections ont été créées pour résoudre les
 problème de direction qui peuvent se produire lorsque vous utilisez directement les Cantons. Une section a seulement
deux directions, "avant" ou "inverse". Les Sections peuvent être traversées par un train dans les deux
directions. La direction de la Section est indépendante de l'orientation définie pour la locomotive par 
un régulateur En plus de résoudre le problème de direction, les sections offrent à l'utilisateur
d'autres fonctionnalités utiles.</p>
 <p> Une Sections peut être considérée comme une extension de Cantons. De nombreuses Sections ne contiennent qu'un seul
Canton. Tous les Cantons contenus dans une Section multi - Cantons doivent être différents.
Les Cantons sont conservés dans l'ordre - le premier canton est relié au second, le second
est relié au troisième, etc. Cela signifie qu'un Canton dans une Section doit être connecté
au Canton avant (s'il en existe un) et au Canton après lui (s'il y a
un), mais ne peut pas être connecté à n'importe quel autre Canton de la Section. Une Section  ne peut
pas contenir une  boucle de retournement.</P>
 <p>Les Sections et leurs <b><a href="TransitTable.shtml"> Transits</a></b> associés
sont conçus pour fournir une assistance pour l'envoi, soit manuel,  (par un
répartiteur assis à une pupitre) , ou envoi automatique  par l'ordinateur. L'Expédition
est l'allocation temporaire de Section pour l'usage exclusif des trains, soit
gérée par des opérateurs avec un régulateur, et/ou exécuter automatiquement par l'ordinateur. L'aide pour
Dispatcher est assuré par <A HREF="../dispatcher/Dispatcher.shtml"><b> Dispatcher</b></a>,
qui prévoit également la mise en place et le fonctionnement des trains automatiquement.</p>
<P> La direction  d'une <b> section</b> est définie par la séquence dans laquelle les cantons sont
ajoutés à une section multi - cantons. Pour une Section à seul Canton la direction est choisie
arbitrairement lorsque la Section est créée. Un train peut fonctionner à travers une Section dans les directions
 <b> avant</b>  (à partir du premier canton au dernier canton) ou <b>inverse</b>
 (du dernier canton au premier  canton n). Si une Section a un seul canton, la
<b> direction</b> de cette section est choisie lors de la mise en place de ses <b>Points d'Entrée </b>
(Voir ci-dessous).</P>
<p> Une Section a un ou plusieurs <b>Points d'Entrée </b>. Un point d'entrée définit une
connexion à partir d'un canton à l'intérieur de la section à un canton en dehors de la section. 
Les points sont mis en place en utilisant les chemins des Cantons dans la section - Les chemins représentent des connexions aux cantons qui ne figurent pas dans la section. Les Points d'entrée sont regroupés soit que l'entrée dans cette Section à ce Point d'Entrée dans la section  se traduira par un train circulant dans le sens "avant" soit l'entrée dans ce Point d'Entrée résulte du déplacement d'un train en direction ''inverse''
, ou si l'entrée à ce point d'entrée se traduira par un train
voyageant dans le direction "inverse".</P>
<p>Les Sections  sont conçues pour être attribuées à un train à la fois. Si une portion de voie connectée
est assez longue  pour soutenir simultanément plusieurs trains voyageant dans
la même direction, l'un suivant l'autre, cette piste connectée devrait être divisée en
deux sections ou plus, si l'utilisateur veut faire circuler des trains suivants dans cette zone .</p>
<H2>Conditions pour l'utilisation de Sections 
</H2>
 <P> Les Sections peuvent être utilisées avec un panneau éditeur de panneau, avec un panneau TCO, avec
un pupitre manuel , ou directement, sans aucun panneau prévu. Les Sections sont construites sur des Cantons JMRI et des structures de Chemins. Alors avant d'essayer de créer des sections, vous devriez avoir
des cantons et des chemins  complètement initialisés pour votre réseau. (Une façon simple d'
initialiser automatiquement vos cantons et des chemins est d'esquisser un panneau TCO
de votre réseau, avec des cantons affectés.)</p>

<p>Le cantonnement du TCO  doit être soigneusement planifiée, afin de fournir "des Sections<i> raisonnables</i>"
 - Sections qui ont un sens pour un répartiteur pour les attribuer à un train. Pour les
aiguillages droits ou gauches sur la voie principale,
, cela signifie habituellement, y compris l'inclusion de l'aiguillage dans le même canton que la voie raccordée à la pointe
, et en fournissant des cantons séparés pour continuer
sur les voies divergentes Certains préfèrent avoir un aiguillage dans un canton séparé, et cela
fonctionne très bien aussi. Du point de vue répartition, Les aiguillages des voies  principale qui
se branchent à des industries n'ont pas besoin d'être bloquées séparément,  mais le cantonnement est ici
acceptable. Un schéma de cantonnement de réseau qui fonctionne bien pour les signaux, devrait fonctionner correctement
pour les sections aussi. </p>
<p>Les Signaux  ne sont pas requis pour l'utilisation de Sections, mais les Sections fonctionnent parfaitement avec des
signaux installés. Si les signaux sont installés, les capteurs de direction inclus dans 
les Sections offrent un moyen facile à mettre en place  une simple de signalisation APB* (voir ci-dessous).</P>
<H2> Support  pour les Capteurs Facultatif de  Direction et d'Arrêt
</H2>
<P>En Option  chaque section peut avoir deux <i>capteurs de direction </i>, l'un pour la direction avant
 et un pour la direction inverse. Ce sont normalement des Capteurs internes, avec un
nom système qui commencent par IS. Ils suivent automatiquement l'état de leur
Section. Une section a trois états, <b> LIBRE</b> (non attribué à un train), <b> AVANT</b>
(Allouée pour les déplacements vers l'avant), et <b> INVERSE</b> (allouée pour les déplacements en
sens inverse. Lorsque l'état de la Section est <b>LIBRE</b> les deux capteurs  de sens
sont fixés à <b> ACTIFS</b>. Lorsque l'état de la Section est <b> AVANT</b>, le
capteur de direction avant est <b> INACTIFS</b> et le capteur en sens inverse est
<b> ACTIF</b>. De même, lorsque l'état de la Section est <b>INVERSE</b>, le
capteur de direction avant est <b> ACTIF</b> et le capteur en sens inverse est <b>INACTIF</b>.
Ces capteurs peuvent être utilisés dans la logique du signal pour forcer les signauxà <b> ROUGE</b> pour les déplacements dans le sens opposée à la direction affectée. Des outils sont fournis ici dans la
Table Section pour ajouter automatiquement des  Capteurs de direction à ajouter à la logique de signal ou pour
supprimer automatiquement tous les capteurs de direction du signal logique, (<a href="#tools"> 
voir ci-dessous</a>). Alternativement, si <A HREF="../dispatcher/Dispatcher.shtml"><b> Dispatcher</b></a>
est utilisé, il peut ajouter des capteurs de direction au signal logique si nécessaire.</P>
<P> Éventuellement aussi chaque section peut avoir deux <i>Capteurs  d'arrêt</i> qui indiquent
quand un train dans la section a atteint la fin de la Section en se déplaçant dans
la direction avant ou en sens inverse. Les Capteurs d'arrêt devrait être de nature physique
 sur le réseau, par exemple des détecteurs ponctuels infrarouge. Pour la répartition manuelle,
ces capteurs peuvent être affichés sur un panneau pour indiquer au répartiteur quand un train
a atteint la fin dela Secton. Pour l'exécution automatique, ces capteurs peuvent
être utilisé pour arrêter un train avant qu'il n'atteigne la fin de sa Section allouée, et
avant le dépassements d'un signal rouge, si les signaux sont présents. </p>
<H2>Utilisation de la Table Section  pour Afficher et de Créer des Sections
</H2>
<P> Toutes les sections que JMRI connaît peuvent être consultées à l'aide de la <b>
Table Section</b>. Sélectionnez <b> Sections</b> dans le sous - menu <b>tables</b>  du menu <b> Outils</b> de la fenêtre principale du programme JMRI. Il y a huit colonnes dans la Table Sections:</P>
<UL>
<LI><b> Nom Système</b> - Le nom système est affecté à la section lors de sa création,
et ne peut pas être changé.</LI>
<LI><b> Nom Utilisateur</b> - Si vous êtes intéressés pour que vos sections aient des noms ''compréhensibles'' 
, vous pouvez cliquer dans la colonne "Nom Utilisateur" et modifier les informations
comme vous voulez.</LI
<LI><b>État</b> - L'état d'une section indique si une section est «affectée»
ou "en utilisation" par un ou plusieurs trains. L'état d'une section nouvellement ajoutée est toujours
 <b>LIBRE</b>. Quand une Section est allouée pour les déplacements dans le sens du premier au dernier
dernier canton, l'État est  <b>AVANT</b>. Lorsque alloué
pour les déplacements dans la direction opposée, l'État est  <b>INVERSE</b>.</LI>
<LI><b>Commentaire</b> - Cette colonne est pour votre usage personnel, pour 'enregistrer tout commentaire que vous voudrez peut-être  utiliser pour garder trace de vos Sections. Il est pour information seulement.</LI>
<LI> Cliquez sur le bouton  <b>Supprimer</b> dans une ligne spécifique pour supprimer la section de cette ligne.
N'oubliez pas d'enlever tous les Transits utilisant cette section. Tout Transit  qui
utilise une section supprimée sera rompu! Utilisez cette touche avec précaution!</LI>
<LI><b>Premier canton</b> - Cette colonne affiche le premier canton de la section.
Puisqu'une section est un groupe de un ou plusieurs cantons connectés, il y a toujours une
entrée.</LI>
<LI><b>Dernier canton</b> - Cette colonne indique le dernier canton de la section.
Puisqu'une section peut contenir un seul canton, le dernier canton peut être le même que le Premier
Canton.</LI>
Cliquez sur le bouton <LI><b>Modifier</b> dans une rangée pour afficher ou modifier les informations saisies
pour une Section. Le Nom  Système ne peut pas être changé. Le Nom Utilisateur peut être changé, les 
cantons peuvent être redéfinis, les points d'entrée peuvent être reconfigurés, etc une seule section
peut être éditée à la fois. Une Section ne peut pas être modifié si une autre est en cours de création.
</LI>
</UL>
<p> Pour créer une nouvelle section, cliquez sur le bouton
"<A HREF="SectionAddEdit.shtml"> Ajouter .."</a>. Une fenêtre va être mise en place
qui vous permettra de définir des cantons inclus dans la nouvelle Section, et si
nécessaire, désigner les points d'entrée pour avoir comme résultat du trajet dans la Section les directions
AVANT et INVERSE .</P>
<p> Actuellement, l'utilisateur doit sélectionner manuellement <b>AVANT</b> ou  <b>INVERSE</b> pour chaque
Point d'entrée, après que tous les cantons aient été ajoutés à une section. Le programme sera mis pour certaines directions
automatiquement, mais d'autres sont laissés en  <b>INCONNU</b>. Un point d'entrée doit être réglé en
AVANT  si un train entrant dans la section à ce point d'entrée se déplacerait dans le sens AVANT. Sinon, il devrait être fixé à INVERSE. Un développement future possible serait de définir les orientations des points d'entrée automatiquement à partir de la connectivité trouvée dans un TCO.</p>
<p> Pour les sections avec plus d'un canton, un trajet d'un  canton  à bas-numéro vers
un canton de numéro supérieur est une marche AVANT. Dit d'une manière différente, passant d'un
canton supérieur dans la liste de cantonnement d'un canton plus bas dans la liste est la marche avant.
De même le déplacement d'un canton de numéro supérieur à un canton inférieure est  une marche INVERSE
. Pour les sections avec un seul canton, il n'est fait aucune différence la
direction à travers la section est désigné AVANT, aussi longtemps que  le Point d'Entrée des directions
directions sont cohérentes. C'est à dire:
<UL>
 <LI>Les directions INVERSE et  AVANT du déplacement doivent être opposée.</Li>
<LI> La saisie à n'importe quel point d'entrée AVANT doit aboutir à voyager dans les mêmes
direction comme entrant à tous les autres points d'entrée AVANT.</li>
<li> LaSaisie à tout point d'entrée INVERSE doit entraîner de voyage
dans la même direction que l'entrée à toutes les autres points d'entrée INVERSE.</li>
</UL>
<p><b>AVERTISSEMENT: Si les points d'entrée ne sont pas compatibles des comportement étrange peuvent subvenir 
lors de l'utilisation de la Section.</b></p>

<a name="tools"><H2> Outils Table section
</H2>
<P> La Table Tections a un menu <b>Outils</b> pour faciliter l'utilisation des Sections.
Normalement, ces outils seraient utilisables  après que toutes les sections aient été définies.
Les éléments du menu et leur utilisation sont décrits ci-dessous:</p>
<ul>
<p><li> Valider tous les articles ...</b> - Lorsque cette option est sélectionnée, chaque Section
est testée pour la cohérence. Les tests actuels comprennent la vérification de la connectivité
entre plusieurs cantons, en vérifiant qu'il n'y a pas de Points d'Entrée manquant ou des
Points d'Entrée supplémentaires, et, si un panneau TCO est disponible, en vérifiant que
les chemins sont correctement mis en place pour les Cantons dans les Sections. Cet outil ne <b>
 vérifier pas si les directions AVANT et INVERSE sont correctement réglées pour chaque
Point d'Entrée</b>, mais cette fonction est un ajout prévu dans l'avenir. Après que les tests sont
terminés, un message s'affiche indiquant les résultats de l'essai. Des messages d'erreur détaillés
, le cas échéant, sont placés dans le journal des erreurs JMRI.</p>
<li><p><b>Régler Capteurs de Direction dans Logique...</b> - Lorsque cet élément est sélectionné,
les capteurs de direction définis pour chaque section sont placés dans la logique de signal
du signal approprié - ces signaux internes pour les Section ou
protégeant l'entrée de la Section.  Les Capteurs de direction suivent l'affectation de l'état de leurs Sections. Quand une Section est LIBRE (non attribuée), deux de ses
capteurs de direction sont ACTIFS ensembles. En plaçant ces capteurs dans la logique appropriée
 du signal, tous les signaux internes ou de protection des points d'entrée de la
Section sont forcés au ROUGE  lorsque la section est libre. Quand une Section  est allouée
pour circuler dans le sens AVANT, son capteur vers l'avant est réglé
INACTIF, la logique permettant au signal de fonctionner normalement pour circuler à travers
la section dans le sens AVANT, mais en gardant  les signaux qui règlent
le sens INVERSE mis au ROUGE. De même, lorsque la section est allouée pour les déplacements
dans le sens INVERSE, le capteur en sens inverse est INACTIF, et le
capteur de direction avant est fixée ACTIF, permettant aux signaux de libérer le trajet dans la direction INVERSE
, mais  bloquer les déplacements vers l'AVANT.  Le réglage de l'état des
capteurs de direction se fait automatiquement par le logiciel de
<A HREF="../dispatcher/Dispatcher.shtml"> Dispatcher</a> qui alloue les 
Sections.</P>
<p> Note: Dispatcher peut placer les capteurs de direction dans la logique du signal SSL automatiquement
quand un train est activé. Dispatcher placera les capteurs de direction s'ils sont fournis 
avec un TCO et autorisé à utiliser sa connectivité (requis pour le
fonctionnement automatique). Cela permet d'utiliser la signalisation basée sur l'orientation même si le panneau est stocké
avec ces capteurs retirés de la logique du signal.</p>
<p> Cet outil devrait être utilisé après que toutes les sections aient été définies et toute la
logique du signal a été mise en place. Un panneau TCO est nécessaire, car
le logiciel a besoins de la connectivité du panneau pour comprendre quel signal
nécessite quel capteur de direction. Si un capteur de direction est déjà dans la
Logique simple signal (SSL) pour un signal, il n'est pas dupliqué. Cependant, si
un capteur de direction incorrecte est présent dans le protocole SSL, il n'est pas supprimé. (Utilisez l'outil 
décrit ci-dessous pour retirer les capteurs de direction.) Après que le placement des capteurs de direction soit
terminé, un message s'affiche qui donne des résultats. Le détail des
messages d'erreur, le cas échéant, sont placés dans le journal des erreurs JMRI. Quand une erreur survient,
les raisons les plus communes sont: 1) les informations manquantes ( signal manquant ou
signal logique n'est pas mis en place) ou 2) des erreurs dans la mise en place des directions des Point d'Entrée</p>
</li>
<b> Retirer Les Capteurs de Direction depuis la Logique ...</b> - Cet outil rassemble une liste
de tous les capteurs de direction définis dans les Sections actuellement dans la Table Sections puis
supprime tous les capteurs qui sont contenus dans cette liste à partir du signal logique simple
(SSL) de tous les signaux. Cet outil annule l'effet de l'outil<b> Orientation Définie des Capteurs dans la Logique ...</b>, supprime touts les capteurs de direction qui peuvent avoir été placés
par <a HREF="../dispatcher/Dispatcher.shtml"> Dispatcher</a>, et en plus retire
les capteurs direction définis
qui pourraient être dans une mauvaise SSL. Si des erreurs ont été notées lors de l' établissement de l'orientation des 
capteurs utilisant l'outil ci-dessus, puis corrigé, exécuter cet outil avant de tenter de mettre des 
capteurs de direction est de nouveau parfois utile.</li>
<p> Un panneau TCO est requis pour exécuter cet outil. Après le retrait des capteurs de  direction,
la logique du signal est terminée, un message est affiché donnant les résultats
d'exécution de l'outil.  Les messages d'erreur détaillés, le cas échéant, sont placés dans le journal des erreurs JMRI
.</p>
</li>
</ul>


<H2> Sections Sauvegarde sur disque
</H2>
<P>Utilisez l'élément <b>Stocker</b> dans le menu <b>Fichier</b> de la Table Sections (ou
l'élément <b> Stocker Panneaux...</b>  dans le menu <b>Panneaux</b> de la fenêtre principales  JMRI
) pour enregistrer les informations Section sur le disque. Cette information (et  toute les information modifiées de 
la logique signal ) est enregistrée avec la configuration et les panneaux dans un fichier XML
, ainsi que l'installation du panneau de commande et les choses similaires. Pour stocker vos panneaux dans le
 même fichier (recommandé), sélectionnez Fichier <b>Stocker Configuration et Panneaux</b> dans 
Fichier ...</b> ou sélectionnez <b> Panneaux> Stocker Panneaux ...</b> dans la fenêtre principale JMRI.
 
*APB Bloc système automatique double sens
</P>
<!--#include virtual="/Footer" -->
</body>
</html>
