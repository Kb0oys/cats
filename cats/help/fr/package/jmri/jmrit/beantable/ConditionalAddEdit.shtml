<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<!-- $Id: ConditionalAddEdit.shtml,v 1.12 2009/05/02 04:10:05 pete_cressman Exp $ -->
<!-- Translated by Hervé Blorec le 2012/01/05-->
<HTML LANG="fr">
<HEAD>
<TITLE>JMRI: Adding/Editing Logix Conditionals</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8"
<META CONTENT="Bob Jacobsen" NAME="Author">
<META NAME="keywords" CONTENT="JMRI help Logix Conditional Add Edit">

<!-- Style -->
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
<LINK REL="stylesheet" TYPE="text/css" HREF="/css/default.css"
	MEDIA="screen">
<LINK REL="stylesheet" TYPE="text/css" HREF="/css/print.css"
	MEDIA="print">
<LINK REL="icon" HREF="/images/jmri.ico" TYPE="image/png">
<LINK REL="home" TITLE="Home" HREF="/">
<!-- /Style -->
</HEAD>

<BODY>
<!--#include virtual="/Header" -->
  <div class="nomenu" id="mBody">
    <div id="mainContent">

<H1> Ajouter/Éditer Logix Conditionnels</H1>
<P>La fenêtre Modifier  Conditionnel est où les expressions logiques sont fixées
et où les actions sont spécifiées. La fenêtre Modifier Conditionnel 
s'affiche quand un Conditionnel est créé, ou lorsque le bouton <B>Édition</b>
d'un Conditionel est pressé dans la
<a href="LogixAddEdit.shtml"> fenêtre Modifier Logix </a>. La fenêtre 'Édition
Conditionnel affiche le nom système et nom 'utilisateur du
Conditionnel au sommet. Le nom  système est automatiquement géré par le
programme et ne peut pas être changé. Le nom utilisateur peut être édité par
entrer/modifier le texte dans le champ Nom Utilisateur. Le nom d'utilisateur peut ne
pas être le même que le nom dutilisateur d'un autre Conditionnel dans ce Logix,
mais peut être le même que le nom d'utilisateur d'un Conditionel dans un autre
Logix.</P>
<P>A Suivre il y a deux sections - une pour l'installation d'une expression logique
et un autre pour mettre en place des actions. Les Conditionnels sont des énoncés de la forme:</P>
<UL>
if (expression logique) then (action).
</UL>
Un Conditionnel a deux parties distinctes: son "expression logique" et ses
«Action». La fenêtre a des sections séparées pour la mise en place de chacun.
</P>
<P> La section expression logique contient un tableau de l'état des variables, avec deux boutons en dessous. La première
colonne de l'état des variables du tableau affiche le numéro de la ligne de la
variable d'état. Ce n'est que de peu d'importance lorsque le choix de <b>l'Opérateur Logique</b>
 est mis à 'mixte'. La deuxième colonne contient une automatique AND lorsque le choix de
<b>l'Opérateur Logique</b> est mis à 'AND' ou un automatique OR
lorsque le choix est «OR». Lorsque le choix est «mixte», l'utilisateur peut
sélectionnez AND ou OR au besoin. La troisième colonne contient une boîte de choix qui permet à l'utilisateur de sélectionner l'opérateur NON au besoin. La quatrième colonne est une description
de la variable d'état type et la condition pour laquelle elle doit être testée. La suivante
est une colonne affichant l'état actuel de la description du test.
L'état affiché inclut l'effet
de l'opérateur NOT, si NON est sélectionné.
<P> La colonne  Calculs Triggers (Déclencheurs) contient des cases à cocher qui, normalement,
sont vérifiées, de sorte qu'un changement dans la variable d'état va déclencher un 
calcul du Logix. Pour les rares cas où un changement dans un
entité spécifique (capteur, aiguillage, etc) ne devrait pas déclencher un
résultat du calcul, vous devez décocher la case en cliquant dessus.
Décochant la case indique au programme de surveiller cette entité pour les
changements d'état et de calculer le l'état du conditionnel, mais il ne prend pas
action en fonction des résultats.
</P>
<P> Les deux dernières colonnes du tableau (les boutons <b>Modifier</b> et <b>Supprimer</b>)
sont utilisés pour modifier ou supprimer cette ligne dans la table de variable d'état.
supprimer une variable d'état si vous décidez qu'elle n'est plus nécessaire.  La colonne État
du tableau montre l'état (vrai ou faux) de chaque État de la
variable quand elle est évaluée.
</P>
<p> Pour l'expression logique, et
donc le Conditionnel, pour calculer vrai, toutes les variables d'état
doivent évaluer à vrai.</P>
<P> À tout moment pendant l'entrée des données des variable d'état, <B> Vérifier
Variables d'État </B> peut être cliqué pour vérifier les données saisies et d'évaluer l'état des variables. Lorsque ce bouton est enfoncé, la vérification et l'évaluation  se produisent
jusqu'à ce que la vérifications e se termine correctement, ou si une erreur est détectée. Si une
erreur est détectée, la vérification s'arrête pour vous permettre de corriger l'erreur et cliquez sur
<B> Vérifiez Variables d'État </B> à nouveau. S'il vous plaît rappelez vous <i> après l'édition
des éléments de données et du  nom , cliquez sur une cellule différente dans la table avant de
cliquer <B> Vérifiez Variables d'État </B> (ou <B> Mise à Jour Conditionnels </B> au
le bas de la fenêtre) pour que le programme soit averti que vous avez
terminé votre saisie. Sinon votre entrée ne peut prendre effet, et une
erreur peut être signalée inutilement. </i> </P>
<p>
L'<b>Opérateur Logique</b> par défaut est 'AND'. Dans ce cas, le conditionnel
permettra de tester si tous les tests variables sont vraies, après la prise du
"NON" négations en compte.
L'utilisation de la boite <b>Opérateur Logique </b> dessous la liste des variables, vous ne pouvez
changer que de "OR" ou "mixtes". «OR» signifie que le conditionnel sera
vrai si l'un des tests de variables sont vraies, après comptabilisation des négations.
Sélection «mixtes» vous permettra de saisir une expression logique dans un nouveau
zone de texte à l'aide de AND, OR et NOT et les numéros de ligne.
Dans cette expression,
chaque variable est mentionnée par numéro, par exemple R1, R2, R3
pour les trois premier variables, vous pouvez utiliser les opérateurs "and", "or"
et "not" en plus de parenthèses.
Quelques exemples:
<pre>
    R1 and R2
    R1 or (R2 et R3)
    (R2 and R3) or (R3 and R1)
</pre>

<H2> Ajout d'une Nouvelle Variable d'État</H2>
 <p>Presse  la <b>Ajouter Variable d'État </b> pour ajouter une
variable d' état (une ligne dans la table des variables d'état). Cela affichera une
 fenêtre avec une boîte de choix pour l'utilisateur pour sélectionner un type de variable d'état.
 Les variables d'état disponibles sont documentées
<a href="#variable"> ci-dessous</a>. Quand un type est choisie la  fenêtre Modifier Variable
 affiche un champ de texte pour le nom de l'entité à utiliser pour
 la variable d'état.
  Quand un nom (soit  nom système ou nom utilisateur ) est entré, 
il doit correspondre à une entité existante (capteur,  aiguillage,
 lumière, etc.) Il est utile d'ouvrir le Tableau Capteur lors de l'entrée du nom du capteur
 ou le Tableau Aiguillage pour montrer les aiguillages disponibles, etc</p>
 <p><b>Attention :</b> Si vous utilisez des noms utilisateur pour spécifier des variables d'état soyez
prudent lorsque vous éditez vos noms d'utilisateur ou vous pouvez casser vos définitions de variable d'état
. Le nom utilisateur spécifié doit correspondre, caractère pour caractère,
blancs, y compris, au nom 'utilisateur réel du capteur, aiguillage, etc, pour que
vos variables d'état  travaillent. Si vous voulez être en mesure d'éditer librement votre
noms utilisateur, utilisez le nom  système pour définir vos variables d'état.</p>
<p> À tout moment pendant l'entrée
des données des variables d'état , le bouton <b>Vérifier Variable d'État</b> peut être
cliqué pour vérifier les données saisies et évaluer les variables d'état. Lorsque
ce bouton est enfoncé, il produit un contrôle et l' évaluation procèdent jusqu'à se terminer
  avec succès, ou si une erreur est détectée. Si une erreur est
détectée, la vérification s'arrête  pour que l'utilisateur  corrige l'erreur et clique sur
<b>Vérifier les Variables d'État</b> à nouveau. S'il vous plaît rappelez vous <i> après l'édition du
nom  système et des données des éléments de cliquer sur une cellule différente dans la table avant de
cliquer <b>Vérifiez Variables d'État</b> (ou <b>Mise à Jour Conditionnels</b>
au bas de la fenêtre) pour que le programme soit averti que vous avez
terminé votre saisie. Sinon votre entrée ne peut prendre effet, et une erreur
peuvent être signalés inutilement.</i></p> 


<H2> Ajouter une Nouvelle Action</H2>

<P> La section action de la fenêtre Éditer Conditionnel prévoit les mesures spécifiques qui doivent être prises quand un conditionnel est calculé.

<P> Pour ajouter une nouvelle action, appuyez sur le bouton "Ajouter  Action". 
Une nouvelle fenêtre «Édition Action"  apparaîtra.
Sélectionnez un type d'action dans le
éléments de la zone Type, et les données nécessaires pour spécifier complètement l'action
apparaissent à la droite de la zone Type. Si vous ne savez pas ce qui doit être
entré dans un champ de données, placez votre curseur dessus, et un message 
apparaîtra. Quand un nom doit être inscrit, le nom doit correspondre à
un nom  système ou un  nom utilisateur d'une entité existante (capteur, aiguillage,
 signal, etc) du type approprié. L'ouverture de la Table capteur lorsque
la saisie des noms de capteur, ou de la table aiguillages peut être utiles pour voir les aiguillages disponibles,
etc. Les  Types d'actions disponibles sont décrits en détail <a HREF= "# d'action"> ci-dessous</A>.</P>
<P> Pour chaque action, trois options sont disponibles pour le moment pour déclencher des actions: 1) Le changement à True (Vrai, 2) sur le changement à False (Faux), et 3) sur
Changement. Elles font référence à l'état calculé de la condition, qui est
égale à la valeur de l'expression logique tel que spécifié dans la
table des variables d'état. Une de ces options doit être sélectionnée. Lorsque vous avez terminé, cliquez sur
"Mettre à jour" pour installer vos modifications,
"Annuler" pour fermer la fenêtre sans aucune modification
ou "Supprimer" pour supprimer l'action entièrement.</P>

<p>Les  Actions sont évaluées dans l'ordre indiqué. Pour changer cet ordre,
cliquez sur le bouton  "Réorganiser" sur la fenêtre "Éditer conditionnel'' . Les boutons les plus à droite
 par les actions pourront ensuite vous permettent de sélectionner la première, la suivante, etc

<P> Lorsque l'expression logique et les actions ont été spécifiées,
cliquez  <b></B> au bas de la fenêtre. Ceci
initie une vérification de l'expression logique (le même que celui effectué par <b> Vérifier Variables d'état</B>) et une vérification des données saisies pour les actions. Si 
le nom utilisateur a été édité, il est également vérifié. Si une erreur est trouvée, un
boîte de message s'ouvre annonçant l'erreur, et la mise à jour est arrêté pour
vous permettent de corriger l'erreur et cliquez sur  <b>Mise à Jour Conditionnels</B>
à nouveau. Si aucune erreur n'est trouvée, l'action est mis à jour avec l'entrée des données, la fenêtre Modifier Conditionnel est fermée, et l'utilisateur est retourné à
la fenêtre Modifier Logix.</P>
<P> Deux autres boutons sont disponibles au bas de la fenêtre Édition Conditionnel . En cliquant sur <b>Annuler</B> vous fermez l la fenêtre Édition Conditionnel sans mise à jour du Conditionnel. En cliquant  <b>Annuler</B> vous perdez  toutes les donnée qui ont été entrées. L'autre bouton,  <b>Supprimer
Conditionnel</B>, fournit un moyen facile de supprimer un Conditionnel  inutile.
Cliquez  <b>Supprimer Conditionnels</B> pour supprimer le Conditionnel en cours d'Édition
et revenir à la fenêtre Modifier Logix.</P>

<P> Les variables d'état Disponibles et les actions sont listées ci-dessous. Pour de plus amples
d'informations, consultez la documentation Logix sur la page web JMRI
http://jmri.sourceforge.net/.</P>

<DT CLASS="left"><A NAME="variable">
<H2> Variables d'État Disponibles</H2>
<dt>


<P>Les variables d'état  qui sont actuellement disponibles pour une utilisation dans les
Conditionnels sont énumérés ci-dessous, ainsi que des informations sur chacune. Les
variables d'état doivent toujours être évaluer soit true ou false. La condition
résultant en true est donnée pour chacune. Si la condition n'est pas remplie, le
variable d'état est évaluée à false. Quand un Logix est actif, les états des
entités (capteur,  aiguillage, d lumière, etc) spécifiés dans les variables d'état de
ses conditionnels sont surveillés, sauf si la case Calcul Déclencheurs
de la variable d'état n'est pas cochée. Un calcul de toutes les conditionels avec
Logix est déclenché lorsqu'un changement d'état surveillé comme indiqué ci-dessous
(S'il n'est pas évident).</p>
<UL>
<LI>  <b>Sensor Active</b>: true si l'état du
capteur spécifié est actif.</LI>
<LI>  <b>Sensor Inactive</b>: true si l'état du
capteur spécifié est inactif.</LI>
<LI>  <b>Turnout Thrown</b>: true si l'état connu de
l'aiguillage spécifié est dévié.</LI>
<LI>  <b>Turnout Closed</b>: true si l'état connu de
l'aiguillage indiqué est direct.</LI>
<LI>  <b>ConditionnalTrue</b>: true si l'état du
conditionnel spécifiée est vraie.</LI>
<LI>  <b>Conditionnel false</b>: true si l'état de
le conditionnel spécifié est faux.</LI>
<LI>  <b>Lumière On</b>: true si la lumière spécifiée est allumée.</LI>
<LI>  <b>Lumière Off</b>: true si la lumière spécifiée est éteinte.</LI>
<LI>  <b>Mémoire Equals</b>: true si la valeur enregistrée dans
la mémoire spécifiée est la même que la valeur spécifiée dans la variable d'état
. Le calcul est déclenchée lorsque la valeur de mémoire surveillée change de ou vers la valeur spécifiée.</LI>
<LI>  <b>Fast Clock Range</b>: true si le temps Horloge rapide en cours
est entre l'heure de début et de fin spécifiées pour le
gamme. Les heures doivent être spécifiées dans le format hh: mm, où hh est heures et
mm est minutes, par rapport à une horloge de 24 heures. Le calcul est déclenché
lorsque le temps d'horloge rapide entre dans la plage et quand le temps d'horloge rapide
sort de la gamme.</LI>
<LI>  <b>Signal Red</b>: true si l'apparition du
 signal spécifié est rouge. Le calcul est déclenché lorsque 
 rouge.</LI>
<LI>  <b>Signal Yellow</b>: true si l'apparition du signal spécifié est jaune. Le calcul est déclenchée lorsque  l'apparence change du ou vers le jaune.</li>
<LI>  <b>Signal Green.</b>: true si l'apparition du signal spécifié est vert. Le calcul est déclenché lorsque  l'apparence change du ou vers le vert.</li>
<LI> <b> Signal  Dark</b>: true si l'apparition du signal est sombre. Le calcul est déclenché lorsque l'apparence
change de ou vers l'obscurité.</li>
<LI>  <b>Signal Flashing Red</b>: true si l'
l'apparence du signal spécifié est rouge clignotant. Le calcul est
déclenché lorsque l'apparence change de ou vers clignotement rouge.</li>
<LI>  <b>Signal Yellow</b>: true si l'
l'apparence du signal spécifié est jaune clignotant. Le calcul
est déclenché lorsque l 'apparence change de ou vers jaune clignotant.</li>
<LI>  <b>Signal Flashing Green</b>: true si l'
l'apparence due signal spécifié est vert clignotant. Le calcul
est déclenché lorsque l'apparence change de ou vers vert clignotant.</li>
<LI>  <b>Signal Lit</b>: true si le signal spécifié  est allumé.</li>
<LI>  <b>Signal Held</b>: true si le signal spécifié tenu
.</li>
</UL>
<P></P>

<DT CLASS="left"><A NAME="action">
<H2> Actions Disponibles</H2>
</A></dt>

<P>Les Actions  qui sont actuellement disponibles pour une utilisation dans les conditionnels sont
énumérés ci-dessous avec des informations sur chacune d'elles:</P>
<UL>
<LI>  <b>Set Turnout</B>: Définit l'aiguillage indiqué avec l'état choisi.
 Spécifiez l’aiguillage en entrant son nom. Spécifiez l'état 
 en choisissant Closed ou Thrown  dans le menu contextuel.</LI>
<LI>  <b>Set Signal Appearance</B>: Définit le signal spécifié
avec l'apparence choisie. Précisez le signal à régler en entrant
son nom. Spécifier l'apparence  en choisissant dans le menu contextuel
.</LI>
<LI>  <b>Set Signal Held</B>: Définit le signal spécifié à tenir
. Précisez le signal à tenir en entrant son nom.</LI>
<LI>  <b>Clear Signal Held</B>: Efface le tenu sur le signal précisé. Précisez le signal en entrant son nom.</LI>
<LI>  <b>Set Signal Dark</B>: Définit le signal spécifié à ne pas
allumé. Précisez le signal en entrant son nom.</LI>
<LI>  <b>Set Signal Lit</B>: Définit la tête signal spécifié et reste allumé.
Précisez la tête du signal en entrant son nom.</LI>
<LI> Trigger Route  <b></B>: Les déclencheurs de l'itinéraire indiqué. Précisez
l'itinéraire en entrant son nom.</LI>
<LI>  <b>Sensor Set</B>: Définit le capteur spécifié dans l'état choisi
. Spécifiez le capteur en entrant son nom. Spécifiez l'
état en choisissant actif ou inactif dans le menu contextuel.</LI>
<LI>  <b>Delayed Set Sensor</B>: Définit le capteur spécifié pour l'état 
choisie après d'attente d'un certain nombre de secondes spécifiées. Précisez
le capteur en entrant son nom. Spécifier l'état en choisissant
actif ou inactif dans le menu contextuel. Indiquez le nombre de
secondes à attendre avant de fixer les capteurs en entrant un nombre dans le champ le
plus à droite. Si cette condition est déjà en attente pour définir ce
capteur, cette action est ignorée, et l'action précédente se poursuit comme
initialement prévue. (voir aussi  <b>Réinitialisation du détecteur a retard</b> ci-dessous.)</li>
<LI>  <b>Light Set</B>: Définit la lumière précisé dans l'état choisi
. Précisez la lumière en entrant son nom. Spécifier l'état
à mettre en choisissant On ou Off dans le menu contextuel.</LI>
<LI> Set Memory <b></B>: Définit la mémoire spécifiée pour entrer une 
valeur. Précisez la mémoire  en entrant son nom. Spécifiez la
valeur à définir dans la mémoire en l'inscrivant dans le champ le plus à droite.</LI>
<LI>  <b>Enable Logix</B>: Active le logix spécifié. Spécifiez le
Logix à activer en entrant son nom.</LI>
<LI>  <b>Disable Logix</B>: désactive le logix spécifié. Précisez
le Logix à désactiver en entrant son nom.</LI>
<LI><b>Play Sound File </B>: lit le fichier son spécifié.
Spécifiez le fichier son à jouer en entrant son nom dans le champ
fournis. Cliquez  <b>Set</B> pour faire apparaître une boîte de dialogue de sélection de fichier d'aide
pour trouver le fichier. Accédez à votre fichier audio, puis cliquez sur votre son
nom de fichier dans la boîte de dialogue pour le copier (y compris son chemin) dans le champ.
</LI>
 <LI>  <b>Run Script</B>: Démarre le script spécifié. Spécifiez le
script à démarrer en entrant son nom dans le champ prévu. Cliquez  <b>Set</B>
pour faire apparaître une boîte de dialogue de sélection de fichiers pour trouver le fichier. Accédez à
votre fichier de script, puis cliquez sur le nom de votre fichier de script dans la boîte de dialogue pour
copier (y compris son chemin) le champ.</LI>
	<LI>  <b>Delayed Set Turnout</B>: Définit l'aiguillage indiqué à l'état
choisi après attente d'un certain nombre de secondes spécifiées. Précisez
l'aiguillage concerné en entrant son nom. Spécifier l'état à fixer par
le choix Closed ou Thrown dans le menu contextuel. Indiquez le nombre de
secondes à attendre avant de fixer l'aiguillage en entrant un nombre dans le champ le
plus à droite. Si cette condition est déjà en attente pour définir cet
aiguillage, cette action est ignorée, et l'action précédente se poursuit comme
initialement prévue. (voir aussi  <b>Réinitialiser Aiguillage Retardé</b> ci-dessous.)</li>
<LI>  <b>Turnout Lock</B>: Verrouille ou déverrouille un aiguillage. Entrez le
nom de l'aiguillage, et de choisir l'opération à partir du menu contextuel.
Remarque, pour contrôler le type d'aiguillage à verrouiller aller à la <A
HREF = "TurnoutTable.shtml"> Table Aiguillageu</A> et sélectionnez le mode de verrouillage.</LI>
<LI>  <b>Reset Delayed Set Sensor</B>: Définit le capteur spécifié dans l'état 
choisi après d'attente d'un certain nombre de secondes spécifiées. Précisez
le capteur concerné en entrant son nom. Spécifier l'état à fixer par le
choix  de actif ou inactif dans le menu contextuel. Indiquez le nombre de
secondes à attendre avant de fixer les capteurs en entrant un nombre dans le champ le
plus à droite sur le terrain. Cette action est la même que <b>Delayed Set Sensor</b>,
sauf si cette condition est déjà activement en attente pour régler
de capteur spécifié, l'attente précédente est annulée, et une nouvelle période d'attente est démarrée.
En effet, le temps d'attente est redémarré. Le capteur n'est pas activé jusqu'à ce que la plus
période d'attente la plus récente expire.</LI>
<LI>  <b>Cancel Sensor Timers</B>: Annule toutes les minuteries d'attente pour régler le capteur
spécifié dans tous les conditionnels dans tous Logixs. Spécifiez le capteur en
entrant son nom. Le capteur n'est pas défini; toutes les actions actives retardée pour des
Réglage du capteur spécifié sont annulés sans réglage du capteur.</LI>
<LI>  <b>Reset Delayed Set Turnout</B>: Définit l'aiguillage indiqué à l'état
choisie après d'attente d'un certain nombre de secondes spécifiées. Précisez
ll 'aiguillage concerné en entrant son nom. Spécifier l'état à fixer par
 le choix Directe ou Dévié dans le menu contextuel. Indiquez le nombre de
secondes à attendre avant de fixer l'aiguillage en entrant un nombre dans le champ le
plus à droite. Cette action est la même que  <b>Delayed Set TurnoutT</b>,
sauf si cette condition est déjà activement d'attente pour régler l'aiguillage spécifiée, l'attente précédente est annulée, et une nouvelle période d'attente est démarrée.
En effet, le temps d'attente est redémarré. La participation n'est pas activé jusqu'à ce que la
période d'attente la plus récente expire.</LI>
<LI>  <b>Cancel Turnout Timers</B>:  Annule toutes les minuteries d'attente pour régler l'
aiguillage spécifié dans toutes les Conditionels avec tous les Logixs. Spécifiez l'aiguillage en
entrant son nom. L'aiguillage n'est pas défini; toutes les actions actives retardée pour régler
les aiguillages spécifiées sont annulées sans réglage de l'aiguillage.</LI>
<LI>  <b>Set Fast Clock Time</B>: Définit l'heure sur l'horloge rapide. L'heure est entré comme <i> hh: mm</i> où <i> hh</i> est des heures et des <i> mm</i> esur une horloge de 24 heures. Cette action définit l'horloge rapide à l'heure spécifiée indépendamment du fait que l'horloge rapide fonctionne ou est arrêté.</LI>
<LI>  <b>Horloge Fast Start</B>: Démarre le horloge rapide JMRI . Si le temps
 est déjà en cours, cette action n'a aucun effet.</LI>
<LI> <b>Stop Fast Clock  </B>: Suspend l'horloge rapide JMRI. Si le temps
 ne fonctionne pas, cette action n'a aucun effet.</LI>
 <LI><b>Copy Memory To Memory</B>: Copie la valeur d'une variable de mémoire pour
une autre variable  mémoire. Précisez la mémoire à copier en entrant son
nom dans le champ gauche. Précisez la mémoire à recevoir la valeur copiée 
entrant son nom dans le champ à droite.</LI>
<LI>  <b>Set Light Intensity</B>: Définit l'intensité de la variable lumineuse spécifiée à la valeur de l'intensité entré. Précisez la lumière concernée en
entrant son nom. Préciser l'intensité en entrant l'intensité en pourcentage
comme un nombre entier dans la gamme 0 à 100. Si la lumière spécifié n'est pas une
variable de la lumière, ou si la valeur d'intensité entré n'est pas un entier dans la
 gamme requise, un message d'erreur est affiché.</LI>

	<LI> <B> Set Light Transition Time </B>: Définit le temps de transition d'un
d'intensité lumineuse variable spécifiée  à l'heure indiquée. Précisez la lumière
concernée en entrant son nom. Spécifiez le temps de transition en entrant le
nombre de minutes d'horloge rapide nécessaire pour passer de 0% d'intensité à 100%
intensité. Si la lumière spécifiée n'est pas une variable de la lumière, ou si un effet positif
entier n'est pas entré pour le nombre de minutes d'horloge rapide, un
 message d'erreur en résultera</LI>
</UL>
<P></P>

<!--#include virtual="/Footer" -->
</BODY>
</HTML>

