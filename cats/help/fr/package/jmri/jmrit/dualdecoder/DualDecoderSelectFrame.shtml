<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="fr">
<head>
<!-- Copyright Bob Jacobsen 2008 -->
<!-- $Id: DualDecoderSelectFrame.shtml,v 1.3 2008/06/03 22:17:21 jacobsen Exp $ -->
<!-- Translated  by Blorec Hervé le 2011-11-28--> 
<title>JMRI: Multi-Decoder Control</title>

<!-- Style -->
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
<LINK REL="stylesheet" TYPE="text/css" HREF="/css/default.css"
	MEDIA="screen">
<LINK REL="stylesheet" TYPE="text/css" HREF="/css/print.css"
	MEDIA="print">
<LINK REL="icon" HREF="/images/jmri.ico" TYPE="image/png">
<LINK REL="home" TITLE="Home" HREF="/">
<!-- /Style -->
</HEAD>

<BODY>
<!--#include virtual="/Header" -->
  <div class="nomenu" id="mBody">
    <div id="mainContent">




<h2> 
JMRI: Commandes Multi-décodeur
</H2>

A l'origine, DCCn'a pas fourni un moyen commode de programmer
Les valeurs de configuration des (CV) quand vous avez plus d'un décodeur installé dans
une locomotive. Par exemple, si vous avez des décodeurs séparés
pour contrôler le moteur et fournir du son, et ils ont tous deux
utiliser les mêmes endroits CV, la programmation d'un décodeur ferait
modifier les paramètres de l'autre, généralement d'une manière indésirable.

<p>
Il y a différentes astuces qui peuvent être utilisés avec des adresses longues et courtes
et la programmation sur la voie principale, mais elles ne sont pas entièrement satisfaisantes.

<p>
Digitrax
<a href="http://www.digitrax.com/v1/LOCK.htm">propose une méthode</a>
pour faire face à cela au
<a href="http://www.nmra.org/standards/DCC/WGpublic/0305051/0305051.html"> groupe de travail DCC</a>.
Ce fut
<a href="http://www.nmra.org/standards/DCC/WGpublic/discussion_topics.html#Topic0305051"> acceptée par la NMRA</a> pour inclusion dans une 
<a href="http://www.nmra.org/standards/DCC/standards_rps/rp922.html#Descriptions%20of%20Configuration%20Variables%20for%20Multi-Function%20Decoders">"Pratique Recommandée</a> ",
et a depuis été adoptée par de nombreux fabricants.

<p>
La méthode utilise CV15 et CV16 pour commander l'écriture et la lecture des CV.
Avant que les décodeurs soient installés dans la locomotive, chacun a une valeur spécifique
programmé dans CV16. (Les valeurs recommandées sont de 1 pour le décodeur de moteur,
2 pour un décodeur de son, 3 pour un décodeur de fonction, et 4-7 pour d'autres usages;
Les décodeurs sont censés être vendus avec une valeur de 0 dans les deux CV15 et CV16 )
Un décodeur ne fera que lire et écrire les opérations si les valeurs CV15 et CV16 
sont les mêmes, à l'exception que vous pouvez toujours écrire dans CV15.
En effet, CV16 est un verrou et CV15 est la clé.

<p>
<a href="../../../../images/DualDecoder.jpg"> 
<img width="122" height="306" align="right" src="../../../../images/DualDecoder.jpg" alt="Dual Décodeur Window"/></a>

DecoderPro a une " Commande Multi-décodeur" pour rendre l'usage de cette fonctionnalité plus facile.
Vous pouvez l'ouvrir à partir de la sélection «programmateurs»  dans le menu "Outils".
Cela ouvre une fenêtre, que vous pouvez également voir sur la droite de cette page.

<p>
La section supérieure de la fenêtre se compose de huit boutons radio.
 Cliquer sur les boutons numérotés de 0 à 6 fera déverrouiller le décodeur
avec le numéro correspondant dans ce verrouillage CV (CV16). DecoderPro
lit alors la valeur de retour dans le CV16 pour confirmer que le décodeur a réussi à se déverrouiller.

<p>
Normalement,un clic sur le bouton pour le décodeur que vous souhaitez programmer,
et le déplacer vers la fenêtre principale DecoderPro pour lire et écrire
valeurs.

<p>
Le bouton "Legacy" écrit un 7 dans CV15, qui est un autre cas particulier
Beaucoup de décodeurs, des modèles surtout les plus âgés, n'ont pas ce mécanisme actuel de blocage
 . Dans certains cas, plus spécifiquement pour un FX3 Digitrax ou un
décodeur plus ancien et un décodeur du début SoundTraxx, il est possible d'émuler
la serrure à l'aide d'écriture séquentielle de CV. Cliquer sur le bouton "Legacy" 
l'active. Malheureusement, ce n'est pas une méthode entièrement fiable.

<p>
Dans la section du milieu sont là des boutons qui
peuvent faire des opérations plus compliquées.
<p>
Le bouton "Rechercher" fonctionne de manière séquentielle par les adresses possibles
, marquant les adresses de décodeurs qui se trouvent  être présentes.
<p>
«Réinit» fonctionne à travers les combinaisons possibles pour débloquer un décodeur qui
a été  verrouillé par inadvertance. Cela devrait être utilisé si vous avez
juste un décodeur dans la locomotive. Si vous avez plus d'un,
il les débloquera tous, ce qui peut causer de la confusion plus tard.

<p>
"Init DH163 + Soundtraxx" configure le mode "Legacy" discuté ci-dessus.

<p>
En dessous, le programme affiche des messages d'état.

<p>
La section du bas vous permet de définir directement le mode de programmation en cours d'utilisation.
Si vous utilisez la programmation  mode ops (programmation sur la voie principale), vous aurez
également besoin de spécifier l'adresse de la locomotive en cours de programmation.

<!--#include virtual="/Footer" -->
</body>
</html>
