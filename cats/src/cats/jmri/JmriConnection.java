/**
 * Name: JmriName.java
 *
 * What:
 * A JMRI System Name is composed of three parts:
 * <ol>
 * <li>a system connection identifier: an upper or lowercase letter followed by optional digits</li>
 * <li>a type (device): a single, uppercase type letter</li>
 * <li>a suffix: system specific, hardware address</li>
 * </ol>
 * The connection identifier plus type letter are called the System Prefix.  The connection
 * identifier designates the layout connection to JMRI.
 * <p>
 * This class has methods for holding the System Prefixes used by an instance of
 * CATS and methods for manipulating the prefixes.
 * <p>
 * Objects of this class are created in two places:
 * <ol>
 * <li>A JMRINAME element in the XML file</li>
 * <li>An IOSPEC element (device definition) in the XML file</li>
 * </ol>
 * <p>
 * Originally, the user had to define the connection (System Prefix) in designer before
 * any devices could be defined.  This was because JMRI required the System Prefix for
 * locating or creating a device.  In time, JMRI matured so that devices could be found
 * and created by System Name or found by device type and User Name.  This hides System Names
 * better.  It also makes interfacing to JMRI easier in CATS.
 * <p>
 * For ease of extending CATS, originally, the prefix definition also included the path
 * to the Java class for the prefix.  This is no longer needed, but retained for
 * backwards compatibility.
 * 
 */
package cats.jmri;

import java.util.ArrayList;
import java.util.List;

import cats.layout.xml.XMLEleFactory;
import cats.layout.xml.XMLEleObject;
import cats.layout.xml.XMLReader;
import jmri.InstanceManager;

/**
  * A JMRI System Name is composed of three parts:
 * <ol>
 * <li>a system connection identifier: an upper or lowercase letter followed by optional digits</li>
 * <li>a type (device): a single, uppercase type letter</li>
 * <li>a suffix: system specific, hardware address</li>
 * </ol>
 * The connection identifier plus type letter are called the System Prefix.  The connection
 * identifier designates the layout connection to JMRI.
 * <p>
 * This class has methods for holding the System Prefixes used by an instance of
 * CATS and methods for manipulating the prefixes.
 * <p>
 * Objects of this class are created in two places:
 * <ol>
 * <li>A JMRINAME element in the XML file</li>
 * <li>An IOSPEC element (device definition) in the XML file</li>
 * </ol>
 * <p>
 * Originally, the user had to define the connection (System Prefix) in designer before
 * any devices could be defined.  This was because JMRI required the System Prefix for
 * locating or creating a device.  In time, JMRI matured so that devices could be found
 * and created by System Name or found by device type and User Name.  This hides System Names
 * better.  It also makes interfacing to JMRI easier in CATS.
 * </p>
 * <p>
 * For ease of extending CATS, originally, the prefix definition also included the path
 * to the Java class for the prefix.  This is no longer needed, but retained for
 * backwards compatibility.
 * </p>
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>
 * Copyright: Copyright (c) 2006, 2009, 2018, 2019, 2020
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public class JmriConnection implements XMLEleObject {

	/*************************************************************
	 * start with some constants definitions. These define the XML tags and
	 * attributes. The deprecated class name is the value of the XML Element.
	 *************************************************************/
	/**
	 * is the tag for identifying a JmriName in the XML file.
	 */
	static final String XML_TAG = "JMRINAME";

	/**
	 * is the XML tag for JMRI prefix.
	 */
	static final String XML_PREFIX = "JMRIPREFIX";

	/**
	 * is the optional JMRI type for objects with Prefix.
	 */
	static final String XML_TYPE = "XMLTYPE";

	/**
	 * is a constant String identifying an old style Loconet metered Sensor
	 * connection.
	 */
	static private final String CATS_METERED_SENSOR_CLASS = "cats.jmri.MeterLnSensorManager";

	/**
	 * is a constant String identifying an old style Loconet metered Turnout
	 * connection.
	 */
	static private final String CATS_METERED_TURNOUT_CLASS = "cats.jmri.MeterLnTurnoutManager";

	/**
	 * is a constant String identifying an old style Loconet metered Light
	 * connection.
	 */
	static private final String CATS_METERED_LIGHT_CLASS = "cats.jmri.MeterLnLightManager";

	/**
	 * is a constant found in the class field that indicates the connection is
	 * metered
	 */
	static private final String XML_METERED = "metered";

	/**************************************************
	 * following are the fields in a JmriPreix structure
	 **************************************************/

	/**
	 * is the alias (known by CATS) for the real JMRI prefix. This is used in
	 * legacy files by metered connections. It usually is the same as MyPrefix.
	 */
	private final String MyPrefix;

	/**
	 * is the JMRI Name. This is how it is known in the JMRI profile.
	 */
	private String MyAlias = null;

	/**
	 * is the object class that the name refers to. In the legacy architecture,
	 * it is literally the class name. In the new architecture, the device type
	 * is encoded in the system prefix (MyPrefix). Thus, is not needed. However,
	 * the XML entry is still included for backwards compatibility and to mark a
	 * metered connection.
	 */
	private String MyClass;

	/**
	 * is true if the connection should be metered by CATS
	 */
	private boolean Metered = false;

	/**
	 * is the list of Managers defined in the CATS XML file.
	 * Note that decoder addresses may refer to manager that has not been
	 * defined in designer - the user may have defined it, created
	 * some decoders, then removed it.
	 */
	private static ArrayList<JmriConnection> Managers;

	/**
	 * the ctor
	 * 
	 * @param name
	 *            is the System Prefix, as known by CATS, which should not be null
	 */
	public JmriConnection(final String name) {
		MyPrefix = name;
	}

	/**
	 * retrieves the System Prefix as known by CATS.
	 * 
	 * @return the JMRI prefix
	 */
	public String getCATSName() {
		return MyPrefix;
	}

	/**
	 * retrieves the CATS metering flag
	 * 
	 * @return true if CATS should meter messages on this connection; false if
	 *         to simply send the messages to teh JMRI connection memo
	 */
	public boolean getMetered() {
		return Metered;
	}

	/**
	 * retrieves the name of the connection, as known by IOSpecs
	 * 
	 * @return the prefix in the decoder field
	 */
	public String getJMRIName() {
		return MyAlias;
	}

	/**
	 * is a predicate for comparing two JmriNames. If the identifiers match,
	 * then they are presumed to be the same. This is different than in designer
	 * because the identifier is the search key and they must be unique.
	 * 
	 * @param candidate
	 *            is a JmriName being compared to.
	 * @return true if the identifier matches.
	 */
	public boolean matches(JmriConnection candidate) {
		return (MyPrefix.equals(candidate.MyPrefix));
	}

	public static boolean verifyConnection(final String name) {
		String connection;
		List<jmri.SystemConnectionMemo> list = InstanceManager.getList(jmri.SystemConnectionMemo.class);
		if ((connection = JmriPrefix.extractConnectionID(name)) != null) {
			for (jmri.SystemConnectionMemo memo : list) {
				if (connection.equals(memo.getSystemPrefix())) {
					return true;
				}
			}
		}
		return false;

	}

	/**
	 * parses a String for being a JMRI system name.  To be a JMRI system name, it must be
	 * formatted properly and the System prefix must identify a connection manager defined
	 * in the active JMRI user profile.
	 * @param name is a candidate JMRI System Name Prefix
	 * @return true if the String parses to a JMRI system name; false if not.
	 */
//	public boolean verifyConnection(final String name) {
//		boolean manager = false;
//		JmriDeviceType type;
//		if ((name != null) && (name.length() > 1)) {
//			if ((type = JmriPrefix.extractDeviceType(name)) != null) {
//				switch (type) {
//				case Memory:
//					manager = verifySingleConnection(name, jmri.MemoryManager.class);
//					break;
//
//				case Light:
//					manager = verifyLightConnection(name);
//					break;
//
//				case Reporter:
//					manager = verifyReporterConnection(name);
//					break;
//
//				case Sensor:
//					manager = verifySensorConnection(name);
//					break;
//
//				case Turnout:
//					manager = verifyTurnoutConnection(name);
//					break;
//
//				case Chain:
//					manager = true;
//					break;
//
//				default:
//				}
//			}
//		}
//		return manager;
//	}
//
//	/**
//	 * test that the managerClass manages NamedBeans of the required System
//	 * Prefix. This test is for only those JMRI system types that do not have
//	 * multiple layout connections.
//	 * 
//	 * @param prefix
//	 *            is a JMRI system prefix
//	 * @param managerClass
//	 *            is the manager class for the JMRI type letter
//	 * @return true if a manager for the requested connection type exists for
//	 *         the requested prefix. False if it does not.
//	 */
//	static public boolean verifySingleConnection(String prefix, Class managerClass) {
//		String systemPrefix;
//		AbstractManager<NamedBean> manager = (AbstractManager<NamedBean>) InstanceManager.getDefault(managerClass);
//		if (manager != null) {
//			systemPrefix = manager.getSystemPrefix();
//			if (prefix.startsWith(systemPrefix)) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//	/**
//	 * test that a layout connection exists for JMRI Lights with the requested
//	 * JMRI system prefix.
//	 * <p>
//	 * This special testing is needed because JMRI Light management uses a proxy
//	 * at the highest level and references are via the Light interface.
//	 * 
//	 * @param prefix
//	 *            is the JMRI prefix (preferences connection+type)
//	 * @return true if a manager for the requested connection type exists for
//	 *         the requested prefix. False if it does not.
//	 */
//	static public boolean verifyLightConnection(String prefix) {
//		String systemPrefix;
//		ProxyLightManager proxy = null;
//		List<Manager<Light>> lightManagers = new ArrayList<Manager<Light>>(1);
//		LightManager lm = InstanceManager.getDefault(LightManager.class);
//		if (lm instanceof ProxyLightManager) {
//			proxy = (ProxyLightManager) lm;
//			lightManagers = proxy.getManagerList();
//		} else {
//			lightManagers.add(lm);
//		}
//		for (Manager<Light> lmgr : lightManagers) {
//			systemPrefix = lmgr.getSystemPrefix();
//			if (prefix.equals(systemPrefix)) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//	/**
//	 * test that a layout connection exists for JMRI Reporters with the
//	 * requested JMRI system prefix.
//	 * <p>
//	 * This special testing is needed because JMRI Reporter management uses a
//	 * proxy at the highest level and references are via the Reporter interface.
//	 * 
//	 * @param prefix
//	 *            is the JMRI prefix (preferences connection+type)
//	 * @return true if a manager for the requested connection type exists for
//	 *         the requested prefix. False if it does not.
//	 */
//	static public boolean verifyReporterConnection(String prefix) {
//		String systemPrefix;
//		ProxyReporterManager proxy = null;
//		List<Manager<Reporter>> reporterManagers = new ArrayList<Manager<Reporter>>(1);
//		ReporterManager rm = InstanceManager.getDefault(ReporterManager.class);
//		if (rm instanceof ProxyReporterManager) {
//			proxy = (ProxyReporterManager) rm;
//			reporterManagers = proxy.getManagerList();
//		} else {
//			reporterManagers.add(rm);
//		}
//		for (Manager<Reporter> rmgr : reporterManagers) {
//			systemPrefix = rmgr.getSystemPrefix();
//			if (prefix.equals(systemPrefix)) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//	/**
//	 * test that a layout connection exists for JMRI Sensors with the requested
//	 * JMRI system prefix.
//	 * <p>
//	 * This special testing is needed because JMRI Sensor management uses a
//	 * proxy at the highest level and references are via the Sensor interface.
//	 * 
//	 * @param prefix
//	 *            is the JMRI prefix (preferences connection+type)
//	 * @return true if a manager for the requested connection type exists for
//	 *         the requested prefix. False if it does not.
//	 */
//	static public boolean verifySensorConnection(String prefix) {
//		String systemPrefix;
//		ProxySensorManager proxy = null;
//		List<Manager<Sensor>> sensorManagers = new ArrayList<Manager<Sensor>>(1);
//		SensorManager sm = InstanceManager.getDefault(SensorManager.class);
//		if (sm instanceof ProxySensorManager) {
//			proxy = (ProxySensorManager) sm;
//			sensorManagers = proxy.getManagerList();
//		} else {
//			sensorManagers.add(sm);
//		}
//		for (Manager<Sensor> smgr : sensorManagers) {
//			systemPrefix = smgr.getSystemPrefix();
//			if (prefix.equals(systemPrefix)) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//	/**
//	 * test that a layout connection exists for JMRI Turnouts with the requested
//	 * JMRI system prefix.
//	 * <p>
//	 * This special testing is needed because JMRI Turnout management uses a
//	 * proxy at the highest level and references are via the Turnout interface.
//	 * 
//	 * @param prefix
//	 *            is the JMRI prefix (preferences connection+type)
//	 * @return true if a manager for the requested connection type exists for
//	 *         the requested prefix. False if it does not.
//	 */
//	static public boolean verifyTurnoutConnection(String prefix) {
//		String systemPrefix;
//		ProxyTurnoutManager proxy = null;
//		List<Manager<Turnout>> turnoutManagers = new ArrayList<Manager<Turnout>>(1);
//		TurnoutManager tm = InstanceManager.getDefault(TurnoutManager.class);
//		if (tm instanceof ProxyTurnoutManager) {
//			proxy = (ProxyTurnoutManager) tm;
//			turnoutManagers = proxy.getManagerList();
//		} else {
//			turnoutManagers.add(tm);
//		}
//		for (Manager<Turnout> tmgr : turnoutManagers) {
//			systemPrefix = tmgr.getSystemPrefix();
//			if (prefix.equals(systemPrefix)) {
//				return true;
//			}
//		}
//		return false;
//	}

	/**
	 * adds a prefix.  First, if the prefix is not found,
	 * it is automatically added to the connection table.  If a prefix
	 * is found, nothing happens.
	 * @param newPrefix is the new/replacement prefix
	 * definition.
	 */
	public static void addPrefix(JmriConnection newPrefix) {
		for (JmriConnection iter : Managers) {
			if (iter.matches(newPrefix)) {
				return;
			}
		}
		Managers.add(newPrefix);
	}

	/**
	 * searches the list of defined connections for one with
	 * the desired prefix, as seen by the IOSpec
	 * @param prefix is the manager prefix being searched for
	 * @return the JmriName of the manager if found or null
	 * if not found.
	 */
	public static JmriConnection findJMRIconnection(final String prefix) {
		for (JmriConnection iter : Managers) {
			if (iter.getCATSName().equals(prefix)) {
				return iter;
			}
		}
		return null;
	}

	/**
	 * is the method through which the object receives the text field.
	 *
	 * @param eleValue
	 *            is the Text for the Element's value.
	 *
	 * @return if the value is acceptable, then null; otherwise, an error
	 *         string.
	 */
	public String setValue(String eleValue) {
		MyClass = new String(eleValue);
		return null;
	}

	/**
	 * is the method through which the object receives embedded Objects.
	 *
	 * @param objName
	 *            is the name of the embedded object
	 * @param objValue
	 *            is the value of the embedded object
	 *
	 * @return null if the Object is acceptible or an error String if it is not.
	 */
	public String setObject(String objName, Object objValue) {
		return new String("A " + XML_TAG + " cannot contain an Element (" + objName + ").");

	}

	/**
	 * returns the XML Element tag for the XMLEleObject.
	 *
	 * @return the name by which XMLReader knows the XMLEleObject (the Element
	 *         tag).
	 */
	public String getTag() {
		return new String(XML_TAG);
	}

	/**
	 * tells the XMLEleObject that no more setValue or setObject calls will be
	 * made; thus, it can do any error checking that it needs.
	 *
	 * @return null, if it has received everything it needs or an error string
	 *         if something isn't correct.
	 */
	public String doneXML() {
		String result = null;
		if (MyPrefix == null) {
			result = new String("A JMRI connection is missing the JMRI system prefix.");
		}
		else {
			MyAlias = MyPrefix;
			//			MyType = MyPrefix.Type;
			if (MyClass != null) {
				// the next conditionals look for legacy CATS XML files with
				// CATS metering
				if (MyClass.equals(CATS_METERED_LIGHT_CLASS)) {
					MyAlias = "LT";
					Metered = true;
				} else if (MyClass.equals(CATS_METERED_SENSOR_CLASS)) {
					MyAlias = "LS";
					Metered = true;
				} else if (MyClass.equals(CATS_METERED_TURNOUT_CLASS)) {
					MyAlias = "LT";
					Metered = true;
				}

				// the next set of conditionals checks for later CATS metering
				// and for CATS IO Chains
				if (MyClass.equals(XML_METERED)) {
					Metered = true;
				}

				if (verifyConnection(MyAlias)) {
					addPrefix(this);
				} 
				else {
					result = new String(
							"JMRI System prefix " + MyPrefix + " is not defined in profile preferences");
				}
			}
		}
		return result;
	}

	/**
	 * registers a JmriConnectionFactory with the XMLReader.
	 */
	static public void init() {
		XMLReader.registerFactory(XML_TAG, new JmriConnectionFactory());
		Managers = new ArrayList<JmriConnection>();
		// reset();
//		List<jmri.SystemConnectionMemo> list = InstanceManager.getList(jmri.SystemConnectionMemo.class);
//		for (jmri.SystemConnectionMemo memo : list) {
//			System.out.println(memo.getSystemPrefix());
//		}
	}

	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(JmriConnection.class.getName());
}

/**
 * is a Class for capturing the JMRI connections defined in designer.
 */
class JmriConnectionFactory implements XMLEleFactory {

	/**
	 * is the JMRI Name prefix - the System prefix and device type.
	 */
	private String FullPrefix;

//	/**
//	 * is the type of object identified by Prefix.
//	 */
//	private JmriDeviceType JMRIType;

	/*
	 * tells the factory that an XMLEleObject is to be created. Thus, its
	 * contents can be set from the information in an XML Element description.
	 */
	public void newElement() {
		FullPrefix = "";
//		JMRIType = null;
	}

	/*
	 * gives the factory an initialization value for the created XMLEleObject.
	 *
	 * @param tag is the name of the attribute.
	 * 
	 * @param value is it value.
	 *
	 * @return null if the tag:value are accepted; otherwise, an error string.
	 */
	public String addAttribute(String tag, String value) {
		String resultMsg = null;

		if (JmriConnection.XML_PREFIX.equals(tag)) {
			FullPrefix = value;
		} 
		else if (JmriConnection.XML_TYPE.equals(tag) && (value != null)) {
//			if (JmriConnection.CHAIN.equals(value)) {
//				JMRIType = JmriDeviceType.Chain;
//			} else if (JmriConnection.MEMORY.equals(value)) {
//				JMRIType = JmriDeviceType.Memory;
//			} else if (JmriConnection.LIGHT.equals(value)) {
//				JMRIType = JmriDeviceType.Light;
//			} else if (JmriConnection.POWER.equals(value)) {
//				JMRIType = JmriDeviceType.Power;
//			} else if (JmriConnection.REPORTER.equals(value)) {
//				JMRIType = JmriDeviceType.Reporter;
//			} else if (JmriConnection.SENSOR.equals(value)) {
//				JMRIType = JmriDeviceType.Sensor;
//			} else if (JmriConnection.TURNOUT.equals(value)) {
//				JMRIType = JmriDeviceType.Turnout;
//			}
		} else {
			resultMsg = new String("A " + JmriConnection.XML_TAG + " XML Element cannot have a " + tag + " attribute.");
		}
		return resultMsg;
	}

	/*
	 * tells the factory that the attributes have been seen; therefore, return
	 * the XMLEleObject created.
	 *
	 * @return the newly created XMLEleObject or null (if there was a problem in
	 * creating it).
	 */
	public XMLEleObject getObject() {
		// return new JmriConnection(FullPrefix, null, JMRIType);
//		return new JmriConnection(FullPrefix, JMRIType);
		return new JmriConnection(FullPrefix);

	}
}
/* @(#)JmriConnection.java */
