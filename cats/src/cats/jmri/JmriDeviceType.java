/**
 * Name: JmriDeviceType.java
 *
 * What:
 * This enumeration classifes JMRI device type and is useful
 * for manipulating the type field of a name.
 */
package cats.jmri;

/**
 * This enumeration classifies a JMRI device type and is useful
 * for manipulating the type field of a name.
  * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2019, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public enum JmriDeviceType {
	Chain('C') {
		public Class getJmriClassManager() {
			return null;
		}
	},
	Light('L') {
		public Class getJmriClassManager() {
			return jmri.LightManager.class;
		}
	},
	Memory('M') {
		public Class getJmriClassManager() {
			return jmri.MemoryManager.class;
		}
	},
	Reporter('R') {
		public Class getJmriClassManager() {
			return jmri.ReporterManager.class;
		}
	},
	Route('O') {
		public Class getJmriClassManager() {
			return jmri.RouteManager.class;
		}
	},
	Sensor('S') {
		public Class getJmriClassManager() {
			return jmri.SensorManager.class;
		}
	},
	Turnout('T') {
		public Class getJmriClassManager() {
			return jmri.TurnoutManager.class;
		}
	};
	
	/**
	 * the letter designating the device type
	 */
	private final char Designator;
	
	/**
	 * the ctor
	 * @param letter is the designator
	 */
	JmriDeviceType(char letter) {
		Designator = letter;
	}
	
	/**
	 * maps a device to its identifier
	 * @return the identifier for a type
	 */
	public char toIdentifier() {
		return Designator;
	}
	
	/**
	 * returns the actual JMRI device type referenced by each member
	 * of this enum
	 * @return the class of the JMRI device managed
	 */
	public abstract Class getJmriClassManager();
	
	/**
	 * is a search method for converting a character to a type.
	 * It maps a character to a type.  Because 'R" is used for both
	 * Reporters and Routes, the first found (Reporter) will be
	 * returned.
	 * @param query is the type being asked for.  It must be uppercase for
	 * a match.
	 * @return the associated JMRI device type, if found, or null
	 * is there is no match
	 */
	static public JmriDeviceType fromIdentifier(char query) {
		for (JmriDeviceType j : JmriDeviceType.values()) {
			if (j.toIdentifier() == query) {
				return j;
			}
		}
		return null;
	}
}
/* @(#)JmriDeviceType.java */