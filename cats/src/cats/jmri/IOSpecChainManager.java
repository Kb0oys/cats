/**
 * Name: IOSpecChainManager.java
 * 
 * What:
 *   Manages the decoder chains.  The Chains are in a separate
 *   System and Name space.  It would be great to merge it into
 *   the JMRI name space, but I would have to add a new type
 *   or spoof an existing type.
 */
package cats.jmri;

import java.util.ArrayList;
import java.util.Iterator;

import cats.layout.items.IOSpecChain;


/**
 *   Manages the decoder chains.  The Chains are in a separate
 *   System and Name space.  It would be great to merge it into
 *   the JMRI name space, but I would have to add a new type
 *   or spoof an existing type. 
 *
 *   System names are "ICnnn", where nnn is the sensor number without padding.
 *   This code is derived from the JMRI LnSensorManager.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2006, 2010, 2011, 2016, 2020, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class IOSpecChainManager {
  
  ArrayList<IOSpecChain> Chains;
  
  /**
   * JMRI System Prefix for a Chain
   */
  private static final String PREFIX = "IC";
  
  /**
   * check for the existence of a Manager.
   *
   * @return the manager.  If one does not exist, create it.
   */
  static public IOSpecChainManager instance() {
    if (mInstance == null) {
      mInstance = new IOSpecChainManager();
    }
    return mInstance;
  }
  
  /**
   * is the singleton manager.
   */
  static private IOSpecChainManager mInstance = null;
  
  /**
   * constructor.
   */
  public IOSpecChainManager() {
    mInstance = this;
    Chains = new ArrayList<IOSpecChain>();
  }
 
  /**
   * searches the list of created DecoderChains for one with a matching
   * JMRI system name.
   * @param key the JMRI system name of the desired Chain
   * @return the DecoderChain, if found; or null, if not found.
   */
  public IOSpecChain findChainBySystem(String key) {
    String name = key.toUpperCase();
    IOSpecChain c;
    for (Iterator<IOSpecChain> iter = Chains.iterator(); iter.hasNext();) {
      c = iter.next();
      if (c.getName().equals(name)) {
        return c;
      }
    }
    return null;
  }
  
  /**
   * searches the list of created DecoderChains for one with a matching
   * JMRI user name.
   * @param key the JMRI user name of the desired Chain
   * @return the DecoderChain, if found; or null, if not found.
   */
  public IOSpecChain findChainByUser(String key) {
    String name = key.toUpperCase();
    IOSpecChain c;
    for (Iterator<IOSpecChain> iter = Chains.iterator(); iter.hasNext();) {
      c = iter.next();
      if (c.getName().equals(name)) {
        return c;
      }
    }
    return null;
  }
  
  
  /**
   * looks for an existing chain and if not found, creates a new one
   * @param sysName is the system name to look for/create
   * @param userName is the user name to look for/create
   * @return the IOSpecchain
   */
  public IOSpecChain newChain(String sysName, String userName) {
	  String systemName = sysName.toUpperCase();
	  if (log.isDebugEnabled()) {
		  log.debug("newChain:"
				  +( (systemName==null) ? "null" : systemName)
				  +";"+( (userName==null) ? "null" : userName));
	  }
	  if (systemName == null) {
		  log.error("SystemName cannot be null. UserName was "
				  +( (userName==null) ? "null" : userName));
		  return null;
	  }
	  // is system name in correct format?
	  if (!systemName.startsWith(""+PREFIX)) {
		  log.error("Invalid system name for chain: "+systemName
				  +" needed "+PREFIX);
		  return null;
	  }

	  // return existing if there is one
	  IOSpecChain c;
	  if ( (userName!=null) && ((c = findChainByUser(userName)) != null)) {
		  if (findChainBySystem(systemName)!=c)
			  log.error("inconsistent user ("+userName+") and system name ("+systemName+") results; userName related to ("+c.getName()+")");
		  return c;
	  }
	  if ( (c = findChainBySystem(systemName)) != null) {
		  if ((c.getUserName() == null) && (userName != null))
			  c.setUserName(userName);
		  else if (userName != null) log.warn("Found chain via system name ("+systemName
				  +") with non-null user name ("+userName+")");
		  return c;
	  }

	  // doesn't exist, make a new one
	  c = createNewChain(systemName.substring(2), userName);

	  // save in the maps
	  //  register(c);

	  return c;
  }
  
  /**
   * Internal method to invoke the factory, after all the
   * logic for returning an existing method has been invoked.
   * @param systemName is the JMRI-like system name of the chain
   * @param userName is the user name
   * @return new null
   */
  private IOSpecChain createNewChain(String systemName, String userName) {
    int addr = 0;
    boolean parseError = false;
    try {
      addr = Integer.parseInt(systemName);
    }
    catch (NumberFormatException nfe) {
      System.out.println("Illegal number for IOSpecChain " + systemName);
      log.warn("Illegal number for IOSpecChain " + systemName);
      parseError = true;
    }
    if (!parseError) {
      IOSpecChain i = new IOSpecChain(addr);
      i.setUserName(userName);
      Chains.add(i);
      return i;
    }
    return null;
  }
  
  static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(IOSpecChainManager.class.getName());
}
