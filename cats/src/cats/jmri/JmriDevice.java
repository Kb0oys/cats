/**
 * Name: JmriDevice.java
 *
 * What:
 * This class defines how to identify a JMRI device.  There
 * are two ways of identifying a JMRI device:
 * <ol>
 * <li>by a System Name (connection + type + address)</li>
 * <li>by a User Name for an existing device defined by a System Name (type + User Name)</li>
 * </ol>
 * A System Name is composed of three pieces:
 * <ul>
 * <li>the connection identifier (a letter followed by zero or more digits)</li>
 * <li>the JMRI device type designator (a single letter)</li>
 * <li>an address whose format is specific to the connection
 * </ul>
 * The connection identifier plus device type constitutes the JMRI System Prefix.
 * A User Name is optional.
 * <p>
 * JMRI developers prefer that JMRI devices be identified by a User Name, as it relies
 * less on JMRI underpinnings and is more flexible.  Identifying a device by User Name
 * requires two pieces:
 * <ul>
 * <li>the User Name (an arbitrary String)</li>
 * <li>knowing what type of JMRI device is being used</li>
 * </ul>
 * The type can be derived from the System Prefix.  Thus, a Prefix and User Name is
 * sufficient for finding a device.
 * <p>
 * Note that there are three fields and only two must be valid to create or identify a JMRI Device.
 * Thus, only two are needed when a device is instantiated, yet the third could be
 * added later.  Therefore, none of these fields are Java final.
 * <p>
 * Thus, a JMRI device can be identified to JMRI by any of:
 * <ol>
 * <li>System Prefix + Address</li>
 * <li>System Prefix + User Name</li>
 * <li>Device Type + User Name</li>
 * </ol>
 * I considered structuring this class as a wrapper around JmriPrefix, but rejected it.  A prefix
 * is more closely tied to the information pulled from the XML file.  It would be more accurate
 * to think of this class as a wrapper around a JMRI device type.  It is used not just for
 * finding the JMRI device, but for holding information common to clients of the device.
 * 
 */
package cats.jmri;

import java.util.ArrayList;
import java.util.EnumMap;

import cats.layout.items.IOSpec.LinkageCompletion;
import cats.layout.items.IOSpecChain;
import cats.layout.items.PtsEdge;
import jmri.NamedBean;

/**
 * This class defines how to identify a JMRI device.  There
 * are two ways of identifying a JMRI device:
 * <ol>
 * <li>by a System Name (connection + type + address)</li>
 * <li>by a User Name for an existing device defined by a System Name (type + User Name)</li>
 * </ol>
 * A System Name is composed of three pieces:
 * <ul>
 * <li>the connection identifier (a letter followed by zero or more digits)</li>
 * <li>the JMRI device type designator (a single letter)</li>
 * <li>an address whose format is specific to the connection
 * </ul>
 * The connection identifier plus device type constitutes the JMRI System Prefix.
 * A User Name is optional.
 * <p>
 * JMRI developers prefer that JMRI devices be identified by a User Name, as it relies
 * less on JMRI underpinnings and is more flexible.  Identifying a device by User Name
 * requires two pieces:
 * <ul>
 * <li>the User Name (an arbitrary String)</li>
 * <li>knowing what type of JMRI device is being used</li>
 * </ul>
 * The type can be derived from the System Prefix.  Thus, a Prefix and User Name is
 * sufficient for finding a device.
 * <p>
 * Note that there are three fields and only two must be valid to create or identify a JMRI Device.
 * Thus, only two are needed when a device is instantiated, yet the third could be
 * added later.  Therefore, none of these fields are Java final.
 * <p>
 * Thus, a JMRI device can be identified to JMRI by any of:
 * <ol>
 * <li>System Prefix + Address</li>
 * <li>System Prefix + User Name</li>
 * <li>Device Type + User Name</li>
 * </ol>
 * <p>
 * I considered structuring this class as a wrapper around JmriPrefix, but rejected it.  A prefix
 * is more closely tied to the information pulled from the XML file.  It would be more accurate
 * to think of this class as a wrapper around a JMRI device type.  It is used not just for
 * finding the JMRI device, but for holding information common to clients of the device.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2019, 2020, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class JmriDevice<T> {

	/**
	 * is the JMRI System Name - System Prefix + Address
	 */
	private String SystemName;

	/**
	 * is the JMRI device type
	 */
	private JmriDeviceType DeviceType;

	/**
	 * is the JMRI user name
	 */
	private String UserName;

	/**
	 * the JMRI device described by the information provided
	 */
	protected T MyDevice = null;

	/**
	 * is the JMRI connection to the layout
	 */
	private JmriConnection Connection;

	/**
	 * is a list of IOSpecs that were created before MyDevice was identified
	 */
	private ArrayList<LinkageCompletion> Deferred;
	
	/**
	 * is a list of Routes for devices used to move points, of all routes
	 * using the device.  This is needed for checking that it is safe to
	 * move the points and to tell the routes that the points are moving
	 * (to prevent overriding.  There is one ListArray for each direction.
	 */
	private ArrayList<PtsEdge.RouteReference> ThrowCommands;
	private ArrayList<PtsEdge.RouteReference> CloseCommands;
	
	/**
	 * are all of the JmriDevices encountered
	 */
	private static EnumMap<JmriDeviceType, ArrayList<JmriDevice>> Devices;

	/**
	 * the ctor.  Either prefix+address or prefix+userName is required to uniquely identify
	 * the decoder.
	 * @param systemName is the System Name
	 * @param type is the JMRI device type
	 * @param user is the user name from the XML file
	 * 
	 */
	protected JmriDevice(final String systemName, final JmriDeviceType type, final String user) {
		SystemName = systemName;
		UserName = user;
		DeviceType = type;
		Deferred = new ArrayList<LinkageCompletion>();
	}

	/**
	 * retrieves the System Name, which can be null
	 * @return the System Name
	 */
	public String getSystemName() {
		return SystemName;
	}
	
	/**
	 * returns the User name, which can be null
	 * @return the JMRI user name for the device
	 */
	public String getUserName() {
		return UserName;
	}
	
	/**
	 * gets the JMRI device type
	 * @return the device type, which could be null.
	 */
	public JmriDeviceType getType() {
		return DeviceType;
	}

	/**
	 * gets the actual JMRI device
	 * @return the JMRI device, which could be null
	 */
	public T getDevice() {
		return MyDevice;
	}

	/**
	 * tests if the JMRI interface is metered
	 * @return true only if the interface can be found and is metered.
	 */
	public boolean isMetered() {
		if (Connection != null) {
			return Connection.getMetered();
		}
		return false;
	}
	
	/**
	 * this method is invoked when MyDevice is set in a partially created
	 * JmriDevice to tell the IOSpecs that they can try to complete their
	 * linkage to the actual JMRI device.
	 */
	private void refreshIOSpecs() {
		for (LinkageCompletion l : Deferred) {
			l.linkUp();
		}
	}
	
	/**
	 * is a request for an IOSpec to be told when MyDevice has
	 * been discovered.
	 * @param l is the IOSpec wanting the alert
	 */
	public void deferLinking(LinkageCompletion l) {
		Deferred.add(l);
	}

	/**
	 * adds a PtsEdge to the list of points using the device.
	 * @param route is the PtsEdge and track number to add
	 * @param throwClose is true if the command is a Throw and false
	 * if it is a Close
	 */
	public void addRouteInfo(final PtsEdge.RouteReference route, final boolean throwClose) {
		ArrayList<PtsEdge.RouteReference> edges;
		if (route != null) {
			if (throwClose) {
				if (ThrowCommands == null) {
					ThrowCommands = new ArrayList<PtsEdge.RouteReference>(1);
				}
				edges = ThrowCommands;
			}
			else {
				if (CloseCommands == null ) {
					CloseCommands = new ArrayList<PtsEdge.RouteReference>(1);
				}
				edges = CloseCommands;
			}

			// this checks for duplicates
			for (PtsEdge.RouteReference p : edges) {
				if (route.equals(p)) {
					return;
				}
			}
			edges.add(route);
		}
	}
	
	/**
	 * retrieves the PtsEdges that use the Jmri Device for setting points
	 * @param throwClose is true if the command is a throw command and false
	 * if it is a Close command
	 * @return the list of PtsEdges, possibly null.
	 */
	public ArrayList<PtsEdge.RouteReference> getRoutes(final boolean throwClose) {
		return (throwClose) ? ThrowCommands : CloseCommands;
	}
	
	/***********************************************************************************
	 * these static methods implement the Singleton and central access point
	 ***********************************************************************************/

	/**
	 * searches for a discovered JMRI device, meeting one of the three criteria above.
	 * This is the common access point for creating a JmriDevice. It determines if enough
	 * information has been supplied to create or locate a device.  If the device has been
	 * seen before (either by JMRI, by a JMRI panel, or by CATS), it is returned.  Otherwise,
	 * if a System Name has been supplied, it will attempt to create the device. JmriDevice
	 * objects are singletons - only one object exists for each real device.
	 * <p>
	 * There is one other complication.  To support the legacy metered devices (ML, MS,
	 * and MT prefixes), the connection identity is checked for being an alias.
	 * @param prefix is a possibly null JMRI System Prefix
	 * @param address is a possibly null address
	 * @param type is a possibly null JMRI device type
	 * @param user is a possibly null User Name
	 * @return a JmriDevice is one can be found. If not, then if one can be created, it
	 * will be returned.  If one cannot be found or cannot be created, null will be returned.
	 */
	public static JmriDevice findDevice(final String prefix, final String address, final JmriDeviceType type,
			final String user) {
		String systemName = null;
		JmriConnection connection;
		String pre;  // the System Prefix, as known by JMRI
		JmriDeviceType t = type;
		JmriDevice userDev;	// a device found via type + User Name
		JmriDevice systemDev;	// a device found/created via Prefix + Address

		// the first check is for a valid System Name, which can be used to create a device
		if (prefix != null) {
			if ((connection = JmriConnection.findJMRIconnection(prefix)) != null) {
				pre = connection.getJMRIName();
			}
			else {
				pre = prefix;
			}
			t = JmriPrefix.extractDeviceType(pre);
			if (address != null) {
				systemName = pre + address;
			}
		}

		// the type must be known
		if (t != null) {
			//			  deviceList = Devices.get(t);
			// the first action is to locate a device that has already been seen, by
			// type + User Name.  If found and the System Name is missing and this System
			// Name has been defined, then the System Name is added
			if (user != null) {
				if ((userDev = findDeviceByUser(t, user)) != null) {
					if (systemName != null) {
						if ((systemDev = findDeviceBySystem(systemName, t)) != null) {
							if (userDev == systemDev) {
								// the JmriDevice has both definitions
								return systemDev;
							}
							// this is the most complicated case.  It happens when a device is defined by type + user Name
							// another is defined by Prefix + Address, and this definition has both; thus, linking the
							// the previous two.  We know that type + User Name is a place holder - it was created
							// before the System Name was known - so the System Name can now be added to it.
							userDev.SystemName = systemDev.SystemName;
							userDev.MyDevice = systemDev.MyDevice;
							userDev.Connection = systemDev.Connection;
							userDev.refreshIOSpecs();
							systemDev.UserName = userDev.UserName;
							return systemDev;
						}
						// found only a previous type + User Name, so it is incomplete, missing MyDevice
						// this is wasteful, but it creates a new JmriDevice, extracts MyDevice, then discards it
						if ((systemDev = createSystemDevice(systemName, t, user)) != null) {
							userDev.SystemName = systemDev.SystemName;
							userDev.MyDevice = systemDev.MyDevice;
							userDev.Connection = systemDev.Connection;
							userDev.refreshIOSpecs();
						};						
					}
					return userDev;
				}
				// this point is reached if no type + User Name is found, but there
				// could be a Prefix + Address
				if (systemName != null) {
					if ((systemDev = findDeviceBySystem(systemName, t)) != null) {
						systemDev.UserName = user;
						if (t.equals(JmriDeviceType.Chain)) {
							((IOSpecChain)systemDev.MyDevice).setUserName(user);
						}
						else {
							((NamedBean)systemDev.MyDevice).setUserName(user);
						}
						return systemDev;
					}
					// this point is reached when no existing device can be found by Prefix + Address
					// or type + User Name, so one must be created.  Since the System Name is known
					// the device should not be created as incomplete
					if ((systemDev = createSystemDevice(systemName, t, user)) != null) {
						Devices.get(t).add(systemDev);
						return systemDev;
					}					  
				}
				// we are almost out of options.  All that is known is the type + User Name.  A
				// new JmriDevice will be created as a place holder
				if ((userDev = createUserDevice(t, user)) != null)  {
					Devices.get(t).add(userDev);
					return userDev;
				}
			}
			else if (systemName != null) {  // all we have to work with is the System Name
				if ((systemDev = findDeviceBySystem(systemName, t)) != null) {
					return systemDev;
				}
				// the final resort is to create a new device by Prefix + Address
				if ((systemDev = createSystemDevice(systemName, t, null)) != null) {
					Devices.get(t).add(systemDev);
					return systemDev;
				}					  
			}
		}
		return null;
	}

	/**
	 * a ctor for constructing a JmriDevice from a JmriPrefix
	 * @param prefix the prefix
	 * @return the constructed (or existing) JMRI device
	 */
	public static JmriDevice findDevice(JmriPrefix prefix) {
		JmriDevice dev;
		if ((dev = findDevice(prefix.getPrefix(), prefix.getAddress(), prefix.getDeviceType(), prefix.getUserName()))
				!= null) {
			if (prefix.getSystemName() == null) {
				prefix.setSystemAddress(dev.SystemName);
			}
		}
		return dev;
	}

	/**
	 * attempts to locate a JmriDevice by type and user name.  If none is found, one is
	 * created.
	 * @param type is the JMRI device type.  It cannot be null.
	 * @param user is the JMRI User Name.  It cannot be null.
	 * @return an existing JmriDevice or null.
	 */
	private static JmriDevice findDeviceByUser(final JmriDeviceType type, final String user) {
		ArrayList<JmriDevice> deviceList;

		if (Devices == null) {
			Devices = new EnumMap<JmriDeviceType, ArrayList<JmriDevice>>(JmriDeviceType.class);
		}
		if ((deviceList = Devices.get(type)) == null) {
			deviceList = new ArrayList<JmriDevice>(1);
			Devices.put(type, deviceList);
		}
		for (JmriDevice dev : deviceList) {
			if (user.equals(dev.UserName)) {
				return dev;
			}
		}
		return null;
	}

	/**
	 * attempts to find a JmriDevice by System Name, system Prefix and address
	 * @param systemName is the desired JMRI System Name (Prefix + Address).  It must not be
	 * null.
	 * @param type is the type of the device
	 * @return the JmriDevice with the desired System Name.  If one cannot
	 * be found, null will be returned
	 */
	private static JmriDevice findDeviceBySystem(final String systemName, JmriDeviceType type) {
		ArrayList<JmriDevice> deviceList;

		if (Devices == null) {
			Devices = new EnumMap<JmriDeviceType, ArrayList<JmriDevice>>(JmriDeviceType.class);
		}
		if ((deviceList = Devices.get(type)) == null) {
			deviceList = new ArrayList<JmriDevice>(1);
			Devices.put(type, deviceList);
		}
		for (JmriDevice dev : deviceList) {
			if (systemName.equals(dev.SystemName)) {
				return dev;
			}
		}
		return null;	  
	}

	/**
	 * creates a JmriDevice from a type + User Name.  Typically, this is a place holder except
	 * JMRI will be queried for an actual device with that type and User Name.  If a matching device
	 * is not found, it could be created later, by CATS.
	 * @param type  is the type of device
	 * @param user is the JMRI User Name.
	 * @return a JmriDevice object describing the actual JMRI device
	 */
	private static JmriDevice createUserDevice(final JmriDeviceType type, final String user) {
		if (type.equals(JmriDeviceType.Chain)) {
			JmriDevice<IOSpecChain> chainDevice = new JmriChainDevice(null, type, user);
			IOSpecChain jmriChain = IOSpecChainManager.instance().findChainByUser(user);
			if (jmriChain != null) {
				chainDevice.MyDevice = jmriChain;
				chainDevice.SystemName = jmriChain.getName();
			}
			return chainDevice;
		}
		else {
			JmriDevice newDev = new JmriDevice(null, type, user);
			NamedBean jmriDev = CATSConnectionManager.findDeviceByUser(user, type.getJmriClassManager());
			if (jmriDev != null) {
				newDev.MyDevice = jmriDev;
				newDev.SystemName = jmriDev.getSystemName();
				if (jmriDev.getUserName() == null) {
					jmriDev.setUserName(user);
				}
			}
			return newDev;
		}
	}

	/**
	 * creates a JmriDevice from a Prefix + Address.  The User Name is optional.  A result of this
	 * operations is that the device is created in the appropriate JMRI device table.
	 * @param systemName is the JMRI System Name  It cannot be null.
	 * @param type is the JMRI device type.  It cannot be null. 
	 * @param user is the User Name as a String.  It can be null.
	 * @return a JmriDevice of the requested type or null.
	 */
	private static JmriDevice createSystemDevice(final String systemName, final JmriDeviceType type, final String user) {
		if (type.equals(JmriDeviceType.Chain)) {
			JmriDevice<IOSpecChain> newDev = new JmriChainDevice(systemName, type, user);
			IOSpecChain dev;
			if ((dev = IOSpecChainManager.instance().newChain(systemName, user)) != null) {
				newDev.MyDevice = dev;
				if (dev.getUserName() == null) {
					dev.setUserName(user);
				}
			}
			return newDev;
		}
		else {
			JmriDevice newDev = new JmriDevice(systemName, type, user);
			NamedBean dev;
			if ((dev = CATSConnectionManager.findDeviceBySystem(systemName, user, type.getJmriClassManager())) != null) {
				newDev.MyDevice = dev;
				if (dev.getUserName() == null) {
					dev.setUserName(user);
				}				  
			}
			return newDev;
		}
	}
}
/* @(#)JmriDevice.java */
