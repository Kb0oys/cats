
/**
 * Name: JmriPrefix.java
 *
 * What:
 * This class defines a JMRI System Name Prefix.  A Prefix is an
 * uppercase letter followed by zero or more digits, anding with
 * another uppercase letter.  The final uppercase letter identifies
 * a JMRI device type.
 * <p>
 * This class contains a parser for the above.
 */

package cats.jmri;

/**
 * This class defines a JMRI System Name Prefix.  A Prefix is an
 * uppercase letter followed by zero or more digits, ending with
 * another uppercase letter.  The final uppercase letter identifies
 * a JMRI device type.
 * <p>
 * This class contains a parser for the above.
 * </p>
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>
 * Copyright: Copyright (c) 2006, 2009, 2018, 2019, 2020, 2023
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */

public class JmriPrefix {
	/**
	 * the Prefix portion of a JMRI SystemName.  This is the connection
	 * ID plus the single character device type.
	 */
	private String Prefix;
	
	/**
	 * the JMRI Device type.  It can come from any of two sources:
	 * - the System Prefix (as translated by the JMRI connections, which may
	 * not be the same as Prefix)
	 * - as passed in during construction
	 */
	private final JmriDeviceType JmriType;
	
	/**
	 * the device address
	 */
	private String Address;
	
	/**
	 * the User Name
	 */
	private String UserName;
	
	/**
	 * the ctor.  Because of the possible valid combinations, any (or all)
	 * of these may be null; however, some combinations of parameters will
	 * not result in complete device identifiers.  So, methods have been provided
	 * to fill in missing fields.
	 * @param prefix is the JMRI System Prefix - connection + type letter
	 * @param address is the device's address.  It is optional.
	 * @param type is the type spelled out as a String - not the single letter
	 * @param user is the User Name.  It is optional.
	 */
	public JmriPrefix(final String prefix, final String address, final String type, final String user) {
		JmriConnection alias;
		Prefix = prefix;
		Address = address;
		if (prefix != null) {
			alias = JmriConnection.findJMRIconnection(Prefix);
			JmriType = extractDeviceType((alias == null) ? prefix : alias.getJMRIName());
		}
		else if (type != null) {
			JmriType = JmriDeviceType.valueOf(type);
		}
		else {  // this should never happen, but ...
			JmriType = JmriDeviceType.Sensor;
		}
		UserName = user;
	}
	/**
	 * a ctor for constructing a JmriPrefix from a JMRI System Name
	 * @param systemName the JMRI SystemName
	 */
	public JmriPrefix(final String systemName) {
		Prefix = extractSystemPrefix(systemName);
		if (Prefix != null) {
			Address = extractAddress(systemName);
			JmriType = extractDeviceType(Prefix);
		}
		else {
			Address = null;
			JmriType = null;
		}
		UserName = null;
	}
	
	/**
	 * this method sets the System Address.  Sometimes it is not
	 * known when a JmriPrefix is instantiated, but becomes known
	 * later.  This method handles that case.
	 * @param sysAddr is a String containing the System Address
	 */
	public void setSystemAddress(String sysAddr) {
		if (sysAddr != null) {
			Prefix = extractSystemPrefix(sysAddr);
			Address = extractAddress(sysAddr);
		}
	}
	
	/**
	 * retrieves the System Prefix - connection plus type.  If Prefix
	 * is null or the type is unknown, null is returned.  Do not rely
	 * on this value for identifying a JMRI device.  The value reflects
	 * what was found in the XML file, which may be later resolved
	 * when the JMRI device is found.
	 * @return the prefix - it can be null.
	 */
	public String getPrefix() {
		return Prefix;
	}
	
	/**
	 * retrieves the address.  Do not rely
	 * on this value for identifying a JMRI device.  The value reflects
	 * what was found in the XML file, which may be later resolved
	 * when the JMRI device is found.
	 * @return the address, which could be null.
	 */
	public String getAddress() {
		return Address;
	}
	
	/**
	 * retrieves the JMRI device type
	 * @return the device type.  It could be null.
	 */
	public JmriDeviceType getDeviceType() {
		return JmriType;
	}

	/**
	 * retrieves the User Name.  Do not rely
	 * on this value for identifying a JMRI device.  The value reflects
	 * what was found in the XML file, which may be later resolved
	 * when the JMRI device is found.
	 * @return the User Name.  It could be null.
	 */
	public String getUserName() {
		return UserName;
	}
	
	/**
	 * is used to retrieve the System Name.  The System Name is
	 * Prefix + Address. If either piece is missing, null is returned.
	 * Do not rely
	 * on this value for identifying a JMRI device.  The value reflects
	 * what was found in the XML file, which may be later resolved
	 * when the JMRI device is found.
	 * @return a System Name constructed from Prefix and Address.
	 */
	public String getSystemName() {
		if ((Prefix != null) && (Address != null)) {
			return Prefix + Address;
		}
		return null;
	}
	
	/**
	 * a utility routine to extract the JmriDeviceType from a potential
	 * JMRI System Name
	 * @param name is a possible JMRI System Name.  It is checked for being non-null and
	 * at least 2 characters long.
	 * @return the corresponding JmriDeviceType (if it exists); otherwise, null.
	 */
	public static JmriDeviceType extractDeviceType(final String name) {
		if ((name != null) && (name.length() > 1)) {
			return JmriDeviceType.fromIdentifier(name.charAt(name.length() - 1));
		}
		return null;
	}
	
	/**
	 * a utility routine to extract a valid JMRI System Prefix from a potential
	 * JMRI System Name or System Prefix.  When given a System Name with a
	 * syntactically valid System Prefix, it returns the System Prefix.  Thus, the
	 * caller can remove the prefix from the original name to isolate the address
	 * (whose format is connection specific).  This can be used as a system prefix
	 * by passing it the prefix and if the return value is the same as what was passed
	 * in, then the prefix is valid.  This routine does not check that a device
	 * actually exists - just that the string contains a String that parses
	 * correctly.
	 * @param name is a potential System Name or System Prefix.
	 * @return that portion of the name that is a valid System Prefix
	 * or null, if not.
	 */
	public static String extractSystemPrefix(final String name) {
		String copy;
		char letter;
		int length;
		int i = 0;
		StringBuffer p = new StringBuffer();
		
		if (name != null) {
			copy = name.trim();
			length = copy.length();
			if (length > 0) {
				// first isolate the connection ID
				letter = copy.charAt(0);
				if (Character.isUpperCase(letter)) {
					p.append(letter);
					i = 1;
					for (i = 1; i < length; ++i) {
						letter = copy.charAt(i);
						if (Character.isDigit(letter)) {
							p.append(letter);
						}
						else {
							break;
						}
					}
				}
				else {
					return null;
				}
				
				// second, pick out the device type
				if (i < length) {
					letter = copy.charAt(i++);
					if (Character.isUpperCase(letter)) {
						if (JmriDeviceType.fromIdentifier(letter) != null) {
							p.append(letter);
							return p.toString();
						}
					}
				}
			}
		}
		return null;		
	}
	
	/**
	 * extracts the address field from a potential JMRI System Name
	 * @param systemName is a potential JMRI System Name
	 * @return the address field (what remains after the Prefix
	 * is removed) or null if the Prefix is invalid or the System
	 * Name is nothing but Prefix.
	 */
	public static String extractAddress(final String systemName) {
		String prefix;
		if (systemName != null) {
			if (((prefix = extractSystemPrefix(systemName)) != null) && (systemName.length() > prefix.length())) {
				return systemName.substring(prefix.length());
			}
		}
		return null;
	}
	
	/**
	 * extracts the JMRI connection identifier from a potential JMRI System Name
	 * (or system Prefix).  It verifies that the system name is formatted properly.
	 * @param systemName is a potential JMRI System Name (or System Prefix)
	 * @return the connection part of the prefix, if a valid format, or null
	 * if not a valid format.
	 */
	public static String extractConnectionID(final String systemName) {
		String connection = extractSystemPrefix(systemName);
		if (connection != null) {
			return connection.substring(0, connection.length() - 1);
		}
		return null;
	}
}
/* @(#)JmriPrefix.java */
