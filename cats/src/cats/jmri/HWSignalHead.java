/*
 * Name: HWSignalHead
 * 
 * What:
 *   This file contains a SWSignalHead.  A SWSignalHead collects
 *   some JMRI Sensors or Turnouts into a group, so that they can be
 *   treated as a JMRI SignalHead.  This is an example of the Composition
 *   pattern.
 *   <p>
 *   The components need not be homogeneous.  For example, a Loconet
 *   Turnout could be mixed with a C/MRI Sensor.  The important thing
 *   is that the components reflect how the layout is wired.
 *   <p>
 *   This class is different from SWSignalHead in that the hardware
 *   that it talks to performs the timing for flashing.
 *   <p>
 *   I would like to use a JMRI SignalHead class (e.g. DoubleTurnoutSignalHead),
 *   but all of them are too restrictive.
 *   <ol>
 *   <li>it assumes all the components are JMRI Turnouts</li>
 *   <li>it assumes that CLOSED is off and THROWN is on</li>
 *   </ol>
 *   Because of this decision, a JMRI panel can be saved after CATS has loaded
 *   a layout and the panel reloaded into JMRI.  JMRI uses a schema to verify
 *   the XML and a CATS SignalHead does not exist in the schema; thus, the
 *   verification fails. There might be some way of looking into the IOSpecs
 *   of the decoder information being added and if they are all Turnouts and
 *   the "doCommand" is THROWN and the "undoCommand" is CLOSED, then
 *   punt to a standard JMRI SignalHead.  This seems too complicated for
 *   little gain, at this time.
 */
package cats.jmri;

import jmri.SignalHead;
import cats.layout.items.PhysicalSignal;

/**
 *   This file contains a SWSignalHead.  A SWSignalHead collects
 *   some JMRI Sensors or Turnouts into a group, so that they can be
 *   treated as a JMRI SignalHead.  This is an example of the Composition
 *   pattern.
 *   <p>
 *   The components need not be homogeneous.  For example, a Loconet
 *   Turnout could be mixed with a C/MRI Sensor.  The important thing
 *   is that the components reflect how the layout is wired.
 *   <p>
 *   This class is different from SWSignalHead in that the hardware
 *   that it talks to performs the timing for flashing.
 *   I would like to use a JMRI SignalHead class (e.g. DoubleTurnoutSignalHead),
 *   but all of them are too restrictive.
 *   <ol>
 *   <li>it assumes all the components are JMRI Turnouts</li>
 *   <li>it assumes that CLOSED is off and THROWN is on</li>
 *   </ol>
 *   Because of this decision, a JMRI panel can be saved after CATS has loaded
 *   a layout and the panel reloaded into JMRI.  JMRI uses a schema to verify
 *   the XML and a CATS SignalHead does not exist in the schema; thus, the
 *   verification fails. There might be some way of looking into the IOSpecs
 *   of the decoder information being added and if they are all Turnouts and
 *   the "doCommand" is THROWN and the "undoCommand" is CLOSED, then
 *   punt to a standard JMRI SignalHead.  This seems too complicated for
 *   little gain, at this time.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2008, 2010, 2016, 2019</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class HWSignalHead extends SWSignalHead {

  /**
   * constructs a HWSignalHead
   * @param systemName is the JMRI system name
   * @param userName is the external name known by the user
   */
  public HWSignalHead(String systemName, String userName) {
    super(systemName, userName);
  }

  /**
   * constructs a HWSignalHead
   * @param systemName is the JMRI system name
   */
  public HWSignalHead(String systemName) {
    super(systemName);
  }
  
  @Override
  public void setAppearance(int newAppearance) {
	  int oldAppearance = mAppearance;
	  mAppearance = newAppearance;

	  if (oldAppearance != newAppearance) {
		  updateOutput();

		  // notify listeners, if any
		  firePropertyChange("Appearance", new Integer(oldAppearance), new Integer(newAppearance));
	  }
  }


  @Override
  protected void updateOutput() {
	  int appearance = (mLit) ? mAppearance : SignalHead.DARK;
	  if (mLit) {
		  appearance = mAppearance;
	  }
	  else {
		  appearance = SignalHead.DARK;
	  }
	  if (CurrentDecoder != null) {
		  CurrentDecoder.sendUndoCommand();
	  }
	  CurrentDecoder = findSpec(appearance).Spec;
	  if (CurrentDecoder == null) {
		  String msg;
		  if (!sentWarning) {
			  if (getUserName() != null) {
				  msg = "SignalHead " + getUserName();
				  ;
			  }
			  else {
				  msg = "A SignalHead ";
			  }
			  log.warn(msg + " is missing the decoder definition for "
					  + PhysicalSignal.toCATS(appearance));
		  }
		  sentWarning = true;
	  }
	  else {
		  CurrentDecoder.sendCommand();
	  }
  }
  
  static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(HWSignalHead.class.getName());

}
/* @(#)HWSignalHead.java */
