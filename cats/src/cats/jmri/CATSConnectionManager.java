/**
 * Name: CATSConnectionManager.java
 * 
 * What:
 *    CATSConnectionManager is the doorway between CATS and JMRI.
 *    It handles requests for finding JMRI devices and creating new ones.
 */
package cats.jmri;

import jmri.InstanceManager;

import jmri.NamedBean;
import jmri.ProvidingManager;

import jmri.SensorManager;
import jmri.SignalHead;
import jmri.SignalHeadManager;

/**
 *    CATSConnectionManager is the doorway between CATS and JMRI.
 *    It handles requests for finding JMRI devices and creating new ones.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2006, 2009, 2011, 2018, 2020, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class CATSConnectionManager {

	/**
	 * the no argument ctor.  It creates the singleton.
	 */
	public CATSConnectionManager() {
	}


	/**
	 * handles error reporting.
	 * 
	 * @param msg is the error string.
	 */
	private static void errorOut(String msg) {
		System.out.println(msg);
		log.warn(msg);    
	}

	/**
	 * handles info reporting.
	 * 
	 * @param msg is the error string.
	 */
	private static void infoOut(String msg) {
		System.out.println(msg);
		log.info(msg);    
	}
	
	/**
	 * looks for a signal head manager for the hardware system.
	 * SignalHeads are named differently from other devices.  They
	 * tend to not have the two letter prefix.  They can be anything
	 * (including the two letter prefix).
	 * 
	 * @param name is either the system or user name of a head.
	 * 
	 * @return the SignalHead for the address.
	 */
	public static SignalHead findHead(String name) {
		SignalHeadManager shm = InstanceManager.getDefault(jmri.SignalHeadManager.class);
		if (shm != null) {
			return shm.getSignalHead(name);
		}
		return null;
	}

	/** 
	 * asks JMRI if there is a device of the requested type
	 * with the given user name
	 * @param user is the user name of the requested device
	 * @param manClass is the manager class of the manager managing that type of device
	 * @return a device if one is found and null if not.
	 * @param <T> is the JMRI device type of the connection
	 */
	public static <T> T findDeviceByUser(final String user, final Class manClass) {
		ProvidingManager manager;
		T dev = null;
		if (user != null) {
			manager = (ProvidingManager) InstanceManager.getDefault(manClass);
			if (manager == null) {
				errorOut("JMRI " + manClass.toString() + " manager was not found");
			}
			else if ((dev = (T) manager.getByUserName(user)) == null) {
				errorOut("JMRI " + manClass.toString() + " " + user + " not found");
			}
			return dev;
		}
		return null;
	}
	
	/**
	 * asks JMRI if there is a device with the given System
	 * Name of the requested type.  If there is no device, JMRI will create one for the
	 * System Name. If user name is not set, this method
	 * also sets the user name.
	 * @param system is the system name
	 * @param user is the optional user name
	 * @param manClass is the class of the manger managing that class of device
	 * @return a Light if one is found and null if not
	 */
	public static NamedBean findDeviceBySystem(final String system, final String user, final Class manClass) {
		ProvidingManager manager;
		NamedBean dev = null;
		if (system != null) {
			manager = (ProvidingManager) InstanceManager.getDefault(manClass);
			if (manager == null) {
				errorOut("JMRI " + manClass.toString() + " manager was not found");
			}
			else if ((dev = manager.provide(system)) == null) {
				errorOut("JMRI " + manager.getBeanTypeHandled()+ " " + system + " " + user + 
						" not found (should not happen)");
			}
			else if ((user != null) && (dev.getUserName() == null)) {
				dev.setUserName(user);
			}
			return dev;
		}
		return null;		
	}

	/**
	 * reads back the layout, if possible.  
	 */
	public static void readBack() {
		for (SensorManager sm : InstanceManager.getList(SensorManager.class)) {
			infoOut("Reading Sensor manager " + sm.getSystemPrefix());
			sm.updateAll();
		}
	}
	public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
			CATSConnectionManager.class.getName());

}
/* @(#)CATSConnectionManager.java */
