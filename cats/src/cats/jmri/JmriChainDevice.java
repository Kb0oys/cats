/**
 * Name: JmriDevice.java
 *
 * What:
 * This class extends JmriDevice to encompass decoder chains; thus is tightly coupled
 * to the decoder chain class.  What separates a decoder chain from a simple decoder
 * is that the chain has no hardware counterpart, but contains references to other decoders.
 * These references could be IOSpecs or JmriDevices - I need to decide which works best.
 * <p>
 * This class overrides the JmriDevice methods that deal directly with the embedded decoder
 * to account for the multiplicity of contained decoders.
 */
package cats.jmri;

import java.util.ArrayList;

import cats.layout.items.IOSpec;
import cats.layout.items.IOSpecChain;
import cats.layout.items.PtsEdge;
import cats.layout.items.RouteInfo;

/**
 *  * This class extends JmriDevice to encompass decoder chains; thus is tightly coupled
 * to the decoder chain class.  What separates a decoder chain from a simple decoder
 * is that the chain has no hardware counterpart, but contains references to other decoders.
 * These references could be IOSpecs or JmriDevices - I need to decide which works best.
 * <p>
 * This class overrides the JmriDevice methods that deal directly with the embedded decoder
 * to account for the multiplicity of contained decoders.
 * 
  * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2019, 2020, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class JmriChainDevice extends JmriDevice {

	/**
	 * is true when instantiated and set to false on first usage. True means that the
	 * routes list should be expanded to include subtended route lists.
	 */
	private boolean NeedsExpansion;
	/**
	 * the ctor.  Either prefix+address or prefix+userName is required to uniquely identify
	 * the decoder.
	 * @param systemName is the System Name
	 * @param type is the JMRI device type
	 * @param user is the user name from the XML file
	 * 
	 */
	protected JmriChainDevice(final String systemName, final JmriDeviceType type, final String user) {
		super(systemName, type, user);
		NeedsExpansion = true;
	}

	@Override
	public void addRouteInfo(final PtsEdge.RouteReference route, final boolean throwClose) {
		ArrayList<IOSpec> decoders = ((IOSpecChain) MyDevice).getDecoders();
		super.addRouteInfo(route, true);
		super.addRouteInfo(route, false);
		for (IOSpec spec : decoders) {
			spec.registerUser(route);
		}

	}
	
	@Override
	public ArrayList<RouteInfo> getRoutes(final boolean throwClose) {
		if (NeedsExpansion) {
			expandRouteList();
		}
		return super.getRoutes(throwClose);
	}
	
	/**
	 * is a method for adding the PtsEdges the decoders in the chain
	 * are involved in, to the chain's route list.
	 */
	private void expandRouteList() {
		ArrayList<IOSpec> decoders = ((IOSpecChain) MyDevice).getDecoders();
		ArrayList<PtsEdge.RouteReference> points;
		for (IOSpec spec : decoders) {
			points = spec.getRegisteredUsers();
			for (PtsEdge.RouteReference pts : points) {
				super.addRouteInfo(pts, true);
				super.addRouteInfo(pts, false);
			}
		}
		NeedsExpansion = false;
	}
	/**
	 * returns the last known state of the chain
	 * @return the last known state
	 */
	public Boolean getState() {
		return ((IOSpecChain) MyDevice).getKnownState();
	}
}
/* @(#)JmriChainDevice.java */
