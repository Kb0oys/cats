/* Name: DelayLineListener.java
 *
 * What:
 *   This class defines a DelayLine for slowing and metering commands
 *   sent down a layout connection.  It replaces the original MeteredLoconet
 *   connections (to avoid problem integrating into JMRI) and provides
 *   a metering mechanism for other connection schemes.
 *
 * Special Considerations:
 *   This class is application independent.
 */
package cats.jmri;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import cats.gui.Adjuster;
import cats.gui.CounterFactory;
import cats.gui.Sequence;
import cats.layout.DelayLineListener;
import cats.layout.DelayLine;
import cats.rr_events.EventInterface;

/**
 *   This class defines a DelayLine for slowing and metering commands
 *   sent down a layout connection.  It replaces the original MeteredLoconet
 *   connections (to avoid problem integrating into JMRI) and provides
 *   a metering mechanism for other connection schemes.
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * </p>
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2020
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public class MeteredConnection implements DelayLineListener<EventInterface>, PropertyChangeListener {

	/**
	 * the Singleton
	 */
	static private MeteredConnection Connection;
	
	/**
	 * the DelayLine
	 */
	static private DelayLine<EventInterface> Que;
	

	/**
	 * is the number of milliseconds to wait after sending a command
	 * before sending the next.  This meters the output so as to not
	 * overrun the network.
	 */
	private static Sequence FlowRate;

	/**
	 * pseudo-constructor
	 */
	static final public void init() {
		Connection = new MeteredConnection();
		if (FlowRate == null) {
			FlowRate = CounterFactory.CountKeeper.findSequence(
					CounterFactory.LNTAG);
			CounterFactory.CountKeeper.exposeSequence(CounterFactory.LNTAG);
			FlowRate.registerAdjustmentListener(Connection);
		}
		Que = new DelayLine<EventInterface>(10, Connection, FlowRate.getAdjustment());
	}
	
	/**
	 * retrieves the Singleton
	 * @return the Singleton
	 */
	static final public MeteredConnection instance() {
		if (Connection == null) {
			init();
		}
		return Connection;
	}

	/**
	 * enters a command into the metered queue
	 * @param comamnd is the action to send to the layout
	 */
	public void enqueue(EventInterface comamnd) {
		Que.append(comamnd);
	}
	
	@Override
	public void consume(EventInterface command) {
		if (command != null) {
			command.doIt();
		}
	}
	
	/**
	 * receives the notification that the delay between messages has changed.
	 * @param ce is the PropertyChangeEvent
	 */
	public void propertyChange(PropertyChangeEvent ce) {
		if (ce.getPropertyName().equals(Adjuster.CHANGE_TAG)) {
			Que.adjustTimeout(((Integer)ce.getNewValue()).intValue());
		}
	}
}
/* @(#)MeteredConnection.java */
