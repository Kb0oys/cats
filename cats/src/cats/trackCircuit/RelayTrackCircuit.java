package cats.trackCircuit;

/**
 *  This class relays Track Circuit codes from the Track Circuit in advance
 *  directly to the Track Circuit in approach.  It overloads the standard
 *  interfaces (receiveAspectChange()) to remain
 *  compatible.
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2016, 2017, 2018, 2019, 2022
 * </p>
 * <p>
 * Company:
 * </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class RelayTrackCircuit extends DefaultTrackCircuit {

	/**
	 * constructor
	 * @param identity is a String used to identify what the Track Circuit is associated with
	 */
	public RelayTrackCircuit(final String identity) {
		super(identity);
		TypeId = "Relay Track Circuit ";
	}
	
	@Override
	public void receiveAspectChange(int newAspect) {
		sendAspectChange(newAspect);
		super.receiveAspectChange(newAspect);
	}
}
/* @(#)RelayTrackCircuit.java */
