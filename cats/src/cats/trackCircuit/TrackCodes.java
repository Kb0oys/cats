/* Name TrackCodes.java
 *
 * What:
 *  This class holds an enumeration defining the information transmitted from a VitalLogic to
 *  the immediate VitalLogic in approach (in front of it - a train coming towards this
 *  VitalLogic will pass the one in approach first).
 *  <p>
 *  On the prototype, a VitalLogic device transmits its status information to the VitalLogic
 *  in approach through the rails, microwave, a pair of wires, etc.  A Track Circuit is
 *  essentially a battery across the rails, used for detecting the presence of a train.
 *  However, electrical pulses can be superimposed on top of the DC to send the status
 *  information to the VitalLogic in approach.
 *  <p>
 *  The most important information transmitted is the signal aspect, so that the approach
 *  VitalLogic can set caution aspects, as needed.  However, upon examining some documents
 *  from AnsaldoSTS (ex-US&S), I see that a code for tumble-down exists, which can be quite
 *  useful.  If these can be tied into something like a JMRI Reporter, then there is one
 *  more tool for linking CATS panels on separate computers.
 *  <p>
 *  This enum defines the track codes supported in CATS.
 */
package cats.trackCircuit;

/**
 *  This class holds an enumeration defining the information transmitted from a VitalLogic to
 *  the immediate VitalLogic in approach (in front of it - a train coming towards this
 *  VitalLogic will pass the one in approach first).
 *  <p>
 *  On the prototype, a VitalLogic device transmits its status information to the VitalLogic
 *  in approach through the rails, microwave, a pair of wires, etc.  A Track Circuit is
 *  essentially a battery across the rails, used for detecting the presence of a train.
 *  However, electrical pulses can be superimposed on top of the DC to send the status
 *  information to the VitalLogic in approach.
 *  <p>
 *  The most important information transmitted is the signal aspect, so that the approach
 *  VitalLogic can set caution aspects, as needed.  However, upon examining some documents
 *  from AnsaldoSTS (ex-US&S), I see that a code for tumble-down exists, which can be quite
 *  useful.  If these can be tied into something like a JMRI Reporter, then there is one
 *  more tool for linking CATS panels on separate computers.
 *  <p>
 *  This enum defines the track codes supported in CATS.
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2016, 2017
 * </p>
 * <p>
 * Company:
 * </p>
 * @author Rodney Black
 * @version $Revision$
 */

/*****************************************
 * 
 * All that I really should need to send are the current aspect
 * and tumbledown.  For if I know aspect of the signal in advance,
 * then I should be able to determine the speed.  Tumbledown is needed
 * to distinguish if a red is opposing signal or just stop.
 *
 */
public enum TrackCodes {
	APPROACH("APPROACH"),
	CLEAR("CLEAR"),
	DIVERGING("DIVERGING"),
	HALT("HALT"),
	TUMBLEDOWN("TUMBLEDOWN"),
	OPPOSING("OPPOSING"),
	OCCUPIED("OCCUPIED"),
	ADVANCEAPP("ADVANCEAPPROACH"),
//		// The following are the indexes of the track speeds
//	DEFAULTSPEED("DEFAULTSPEED"),
//	NORMALSPEED("NORMALSPEED"),
//	LIMITEDSPEED("LIMITEDSPEED"),
//	MEDIUMSPEED("MEDIUMSPEED"),
//	SLOWSPEED("SLOWSPEED"),
//	NOSPEED("NOSPEED"),
//	APPROACHSPEED("APPROACHSPEED")
	;
	
	/**
	 * the "code" for this status event
	 */
	private final String Mnemonic;
	
	/*
	 * the ctor
	 */
	TrackCodes(String code) {
		Mnemonic = code;
	}
	
	/**
	 * converts the Enum to a character string for transmission
	 * @return the String encoding of the Enum
	 */
	public String getCode() {
		return Mnemonic;
	}
	
	/**
	 * converts an encoding of a TrackCode to a TrackCode
	 * @param code is the String encoding of a TrackCode
	 * @return the corresponding TrackCode (or null, if there is
	 * no encoding)
	 */
	public static TrackCodes getTrackCode(String code) {
		for (TrackCodes tc : TrackCodes.values()) {
			if (tc.Mnemonic.equals(code)) {
				return tc;
			}
		}
		return null;
	}
	
//	/**
//	 * converts a TrackCodes speed code to an index into the
//	 * track speed table.
//	 * @param speed is a TrackCodes that should represent a Track speed
//	 * @return the index into the Track speed table of the speed.
//	 * If speed does not represent a TrackSpeed, then NOSPEED is returned.
//	 */
//	public static int enumToSpeed(TrackCodes speed) {
//		switch (speed) {
//		case DEFAULTSPEED: return Track.DEFAULT;
//		case NORMALSPEED: return Track.NORMAL;
//		case LIMITEDSPEED: return Track.LIMITED;
//		case MEDIUMSPEED: return Track.MEDIUM;
//		case SLOWSPEED: return Track.SLOW;
//		case APPROACHSPEED: return Track.APPROACH;
//		default:
//		}
//		return Track.NONE;
//	}
	
//	/**
//	 * determines the TrackCode value for a TrackSpeed and converts it
//	 * to the code String
//	 * @param speed is an index into the Track speed table
//	 * @return the encoding of the representative Track Code.
//	 * If there is none, NONE is returned.
//	 */
//	public static String speedToString(int speed) {
//		TrackCodes code = TrackCodes.NOSPEED;
//		if (Track.DEFAULT == speed) {
//			code = TrackCodes.DEFAULTSPEED;
//		}
//		else if (Track.NORMAL == speed) {
//			code = TrackCodes.NORMALSPEED;
//		}
//		else if (Track.LIMITED == speed) {
//			code = TrackCodes.LIMITEDSPEED;
//		}
//		else if (Track.MEDIUM == speed) {
//			code = TrackCodes.MEDIUMSPEED;
//		}
//		else if (Track.SLOW == speed) {
//			code = TrackCodes.SLOWSPEED;
//		}
//		else if (Track.APPROACH == speed) {
//			code = TrackCodes.APPROACHSPEED;
//		}
//		return code.getCode();
//	}
}
/* @(#)TrackCodes.java */

