/* Name TrackCircuitListener.java
 *
 * What:
 * An interface provided by any object (especially a TrackCircuit that is a proxy for other TrackCircuits)
 * that wants to know when a TrackCircuit has received a change from its adjacent TrackCircuit.  This is
 * a specialization of a PropertyListener.
 */

package cats.trackCircuit;

 /**
 * An interface provided by any object (especially a TrackCircuit that is a proxy for other TrackCircuits)
 * that wants to know when a TrackCircuit has received a change from its adjacent TrackCircuit.  This is
 * a specialization of a PropertyListener.
  * <p>
  * Title: CATS - Crandic Automated Traffic System
  * <p>
  * Description: A program for dispatching trains on Pat Lana's Crandic model
  * railroad.
  * <p>
  * Copyright: Copyright (c) 2016, 2017, 2018
  * </p>
  * <p>
  * Company:
  * </p>
  * @author Rodney Black
  * @version $Revision$
  */
public interface ProxyTrackCircuitListener extends TrackCircuitListener {

	/**
	 * retrieves the TrackCircuit that is being monitored
	 * @return the TrackCircuit being monitored
	 */
	public TrackCircuit getTrackCircuit();
	
	/**
	 * sets the TrackCircuit that receives changes from the proxy (or monitored TrackCircuit).
	 * @param approachCircuit is the TrackCircuit updated by changes in the proxy or monitored TrackCircuit
	 */
	public void setTrackCircuit(TrackCircuit approachCircuit);
}
/* @(#)ProxyTrackCircuitListener.java */
