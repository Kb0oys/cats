/* Name TrackCircuitListener.java
 *
 * What:
 * An interface provided by any object (especially a FieldSignal) that wants to know
 * when a TrackCircuit has received a change from it adjacent TrackCircuit.  This is
 * a specialization of a PropertyListener.
 */
package cats.trackCircuit;

/**
 * An interface provided by any object (especially a FieldSignal) that wants to know
 * when a TrackCircuit has received a change from its adjacent TrackCircuit.  This is
 * a specialization of a PropertyListener.
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2016, 2017, 2018
 * </p>
 * <p>
 * Company:
 * </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface TrackCircuitListener {

	/**
	 * informs the signal as to the state of the signal in advance	
	 * @param aspect is the aspect showing on the signal in advance
	 */
	public void setAdvanceAspect(final int aspect);
	
//	/**
//	 * the purpose of this interface - to be informed when a specific
//	 * TrackCircuit changed value.  The change is for a change in the aspect
//	 * of the advance signal.
//	 * @param newAspect is the indication of the advance Signal - see AspectMap.java
//	 */
//	public void AdvanceAppearanceChanged(int newAspect);
	
//	/**
//	 * A method to tell the TrackCircuit that the advance TrackSpeed has
//	 * changed.  The change is the index of the new speed into the speed table.
//	 * @param newSpeed is the index into the speed table of the new speed.
//	 */
//	public void AdvanceSpeedChanged(int newSpeed);
}
/* @(#)TrackCircuitListener.java */
