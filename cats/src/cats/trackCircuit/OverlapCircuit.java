/* Name OverlapCircuit.java
 *
 * What:
 *  Whereas most TrackCircuits are associated with a BlkEnd containing a Signal,
 *  this is on a plain Block end without a Signal, but whose neighbor is a signal.
 *  This is a situation where the TrackCircuit for travel in the opposite direction
 *  overlap this one.  It is needed to convert Tumbledown to Opposing, to prevent a
 *  head on collision.
 */
package cats.trackCircuit;

import cats.layout.AspectMap;

/**
 *  Whereas most TrackCircuits are associated with a BlkEnd containing a Signal,
 *  this is on a plain Block end without a Signal, but whose neighbor is a signal.
 *  This is a situation where the TrackCircuit for travel in the opposite direction
 *  overlaps this one.  It is needed to convert Tumbledown to Opposing, to prevent a
 *  head on collision.
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2016, 2018, 2019, 2022
 * </p>
 * <p>
 * Company:
 * </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class OverlapCircuit extends DefaultTrackCircuit {
	
	/**
	 * constructor
	 * @param identity is a way of identifying what the Track Circuit is associated with
	 */
	public OverlapCircuit(final String identity) {
		super(identity);
		TypeId = "Overlap Track Ciruit ";
	}
	
	@Override
	public void receiveAspectChange(int newAspect) {
//		if ((newAspect != -1) && (ApproachCircuit != null)) {
		if (ApproachCircuit != null) {
			AdvanceAppearance = newAspect;
			MyAspect = (newAspect == AspectMap.TUMBLEDOWN_INDEX) ?
				AspectMap.OPPOSING_INDEX : newAspect;
			ApproachCircuit.receiveAspectChange(MyAspect);
		}
	}
}
/* @(#)OverlapCircuit.java */
