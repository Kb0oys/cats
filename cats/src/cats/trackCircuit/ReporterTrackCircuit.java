package cats.trackCircuit;

import java.beans.PropertyChangeEvent;

import cats.layout.AspectMap;
import jmri.implementation.AbstractReporter;
/* Name TrackCircuit.java
 *
 * What:
 *  This class is a specialization of DefaultTrackCircuit.  Rather than sending
 *  events to and receiving events from software VitalLogic, it sends and receives
 *  events encoded as Strings from a JMRI Reporter.  The JMRI Reporter could
 *  encapsulate hardware VitalLogic or be distributed across a network or be
 *  controlled by Logix or ...
 *  <p>
 *  The events (TrackCodes) are encoded as Strings, defined in TrackCodes.
 *  @see cats.trackCircuit.TrackCodes.java
 */

/**
 *  This class is a specialization of DefaultTrackCircuit.  Rather than sending
 *  events to and receiving events from software VitalLogic, it sends and receives
 *  events encoded as Strings from a JMRI Reporter.  The JMRI Reporter could
 *  encapsulate hardware VitalLogic or be distributed across a network or be
 *  controlled by Logix or ...
 * <p>
 *  The events (TrackCodes) are encoded as Strings, defined in TrackCodes.
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2016, 2017, 2018, 2022
 * </p>
 * <p>
 * Company:
 * </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class ReporterTrackCircuit extends DefaultTrackCircuit implements java.beans.PropertyChangeListener {
	
	// the JMRI Reporter that provides TrackCodes
	private AbstractReporter XmitReporter = null;
	
	// the JMRI Reporter that is the recipient of TrackCodes
	private AbstractReporter RcvReporter = null;

	/**
	 * constructor
	 * @param identity is a String used to identify what the Track Circuit is associated with
	 * @param xmit is an AbstractReporter that sends to teh Track Circuit
	 * @param rcv is an Abstract Reporter that receives from the Track Circuit
	 */
	public ReporterTrackCircuit(final String identity, final AbstractReporter xmit, final AbstractReporter rcv) {
		super(identity);
		attachReporter(xmit);
		RcvReporter = rcv;
		TypeId = "Reporter Tarck Circuit ";
	}

	/**
	 * remembers the Reporter and registers to receive events
	 * @param proxy is the Reporter the track circuit is registering on
	 */
	public void attachReporter(AbstractReporter proxy) {
		XmitReporter = proxy;
		if (XmitReporter != null) {
			XmitReporter.addPropertyChangeListener(this);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		int codedEvent;
		Object event = arg0.getNewValue();
		if (event instanceof String) {
			codedEvent = Integer.valueOf((String) event);
//			if (codedEvent != null) {
//				switch (codedEvent) {
//				case APPROACH:
//				case CLEAR:
//				case DIVERGING:
//				case HALT:
//				case TUMBLEDOWN:
//				case ADVANCEAPP:
//					sendAspectChange(codedEvent);
//					break;

//				case DEFAULTSPEED:
//				case NORMALSPEED:
//				case LIMITEDSPEED:
//				case MEDIUMSPEED:
//				case SLOWSPEED:
//				case NOSPEED:
//				case APPROACHSPEED:
//					sendSpeedChange(TrackCodes.enumToSpeed(codedEvent));
////					break;
//				default:
//				}	
//			}
			if ((codedEvent > -1) && (codedEvent <= AspectMap.OPPOSING_INDEX)) {
				sendAspectChange(codedEvent);
			}
		}		
	}
	
	@Override
	public void receiveAspectChange(int newAspect) {
		if (RcvReporter != null) {
			RcvReporter.setReport(String.valueOf(newAspect));
		}
	}

//	@Override
//	public void receiveSpeedChange(int newSpeed) {
//		if (RcvReporter != null) {
//			RcvReporter.setReport(TrackCodes.speedToString(newSpeed));
//		}
//	}
}
/* @(#)ReporterTrackCircuit.java */
