/* Name TrackCircuit.java
 *
 * What:
 *  This is the interface to a class emulating a prototypical Track Circuit.  It is
 *  a node on a communications stream for forwarding appearance codes 
 *  to a TrackCircuit in approach.  It receives changes from the containing VitalLogic
 *  and passes changes to the containing VitalLogic from the TrackCircuit in advance.
 *  <p>
 *  TrackCircuits are contained by:
 *  <ul>
 *  <li>Intermediate Signals/li>
 *  <li>Control Points</li>
 *  <li>Switch Points</li>
 *  <li>Frogs Edges</li>
 *  <li>Block Edges whose neighbor has a TrackCircuit (this is to handle overlapping
 *  protection spans)</li>
 *  </ul>
 *  In its simplest form a TrackCircuit is an event distributor to Listeners.  However,
 *  the design goal is to allow TrackCircuit instances to be augmented with JMRI Reporters.
 *  This multiplies the power of the architecture:
 *  <ul>
 *  <li>the class contents could be replaced by actual hardware, in which case, these
 *  would form a communications network between hardware devices
 *  </li>
 *  <li>Logix could interact with TrackCircuits for things like crossing gates
 *  </li>
 *  <li>Reporters could be distributed across multiple computer, facilitating distributed,
 *  cooperating CATS panels</li>
 *  </ul>
 *  TrackCircuits exchange messages that are a single character long.  Each character
 *  is an encoding of a TrackCode.
 */
package cats.trackCircuit;

/**
 *  This is the interface to a class emulating a prototypical Track Circuit.  It is
 *  a node on a communications stream for forwarding appearance codes 
 *  to a TrackCircuit in approach.  It receives changes from the containing VitalLogic
 *  and passes changes to the containing VitalLogic from the TrackCircuit in advance.
 *  <p>
 *  TrackCircuits are contained by:
 *  <ul>
 *  <li>Intermediate Signals/li>
 *  <li>Control Points</li>
 *  <li>Switch Points</li>
 *  <li>Frogs Edges</li>
 *  <li>Block Edges whose neighbor has a TrackCircuit (this is to handle overlapping
 *  protection spans)</li>
 *  </ul>
 *  In its simplest form a TrackCircuit is an event distributor to Listeners.  However,
 *  the design goal is to allow TrackCircuit instances to be augmented with JMRI Reporters.
 *  This multiplies the power of the architecture:
 *  <ul>
 *  <li>the class contents could be replaced by actual hardware, in which case, these
 *  would form a communications network between hardware devices
 *  </li>
 *  <li>Logix could interact with TrackCircuits for things like crossing gates
 *  </li>
 *  <li>Reporters could be distributed across multiple computer, facilitating distributed,
 *  cooperating CATS panels</li>
 *  </ul>
 *  TrackCircuits exchange messages that are a single character long.  Each character
 *  is an encoding of a TrackCode.
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2018
 * </p>
 * <p>
 * Company:
 * </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface TrackCircuit {
	
	/**
	 * initializes the TrackCircuit to tell it where to forward changes.
	 * @param approach is the TrackCircuit in approach.
	 */
	public void setApproachTrackCircuit(TrackCircuit approach);
	
	/**
	 * remembers that a listener wants to be informed of changes in the adjacent
	 * TrackCircuit
	 * @param l is the listener registering for change notifications.  A check is
	 * made so that if it is already on the list, it is not added twice.
	 */
	public void addListener(TrackCircuitListener l);

	/**
	 * forget that a listener wants to be informed of changes.
	 * @param l is a listener requesting removal from the
	 * listener list.  A check is made so that if it is not
	 * on the list, nothing happens.
	 */
	public void removeListener(TrackCircuitListener l);

	/**
	 * handles sending an aspect TrackCode to the approach
	 * TrackCircuit
	 * @param newAspect is the TrackCode of the new aspect.
	 * Nothing is sent if the new aspect is null or the same as the old.
	 */
	public void sendAspectChange(int newAspect);
	
	/**
	 * the method through which a TrackCircuit receives a change in the
	 * signal aspect from its upstream adjacent TrackCircuit
	 * @param change is the change in aspect of the advance TrackCircuit.
	 * -1 is used to indicate that there is no advance TrackCircuit.
	 */
	public void receiveAspectChange(int change);

//	/**
//	 * handles sending a TrackSpeed to the approach TrackCircuit
//	 * @param newSpeed is the index into the speed table of the new Track Speed.
//	 * Nothing is sent if the new speed is undefined or the same as the old.
//	 */
//	public void sendSpeedChange(int newSpeed);
//	
//	/**
//	 * the method through which a TrackCircuit receives a change in the
//	 * TrackSpeed from its upstream adjacent TrackCircuit
//	 * @param change is the change in TrackSpeed of the advance TrackCircuit.
//	 * Though it should not be -1 (unknown), the method should guard against
//	 * that happening.
//	 */
//	public void receiveSpeedChange(int change);
	
	/**
	 * a method for retrieving the last TrackCode received
	 * regarding signal appearances from the advance TrackCircuit
	 * @return the last TrackCode received.  If none has
	 * ever been received, it will return -1.
	 */
	public int getLastAppearance();
	
//	/**
//	 * a method for retrieving the last TrackSpeed received
//	 * from the TrackCircuit in advance
//	 * @return the last TrackSpeed received.  If none has
//	 * ever been received, it will return NONE.
//	 */
//	public int getLastSpeed();
}
/* @(#)TrackCircuit.java */
