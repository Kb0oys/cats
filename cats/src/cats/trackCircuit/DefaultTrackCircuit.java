/* Name TrackCircuit.java
 *
 * What:
 *  This class is the simplest instance of a TrackCircuit.  It is a software
 *  implementation with no hardware connections.  It is an event distributor.
 */
package cats.trackCircuit;

import java.util.ArrayList;

/**
 *  This class is the simplest instance of a TrackCircuit.  It is a software
 *  implementation with no hardware connections.  It is an event distributor
 *  of TrackCodes.  The receive side from the advance circuit (implemented
 *  through receiveAspectChange()) is decoupled from the transmit side
 *  to the approach circuit (implemented through sendAspectChange()).  One
 *  or more of the listeners will connect the two sides.
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2016, 2017, 2018, 2022
 * </p>
 * <p>
 * Company:
 * </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class DefaultTrackCircuit implements TrackCircuit {
	
	/*
	 * a strang for identifying what te track circuit is associated with
	 */
	final protected String Identity;
	
	// the current Appearance presented by this TrackCircuit
	protected int MyAspect = -1;
	
	// the last Appearance TrackCode received from the adjacent TrackCircuit
	protected int AdvanceAppearance = -1;
	
	// the Track Circuit in approach, which receives changes in this Track Circuit
	protected TrackCircuit ApproachCircuit = null;
	
	// the list of listeners
	protected ArrayList<TrackCircuitListener> Listeners = new ArrayList<TrackCircuitListener>(1);
	
	protected String TypeId = "Default Track Circuit ";
	
	/**
	 * the constructor
	 * @param identity is a String used to identify what the Track Circuit is associated with
	 */
	public DefaultTrackCircuit(final String identity) {
		Identity = identity;
	}

	@Override
	public void setApproachTrackCircuit(TrackCircuit approach) {
//		if (approach == null) {
//			System.out.println("Clearing approach circuit for " + toString() + " aspect is " + MyAspect);
//		}
//		else {
//			System.out.println("Setting approach circuit for " + toString() + " to " + approach.toString() + " aspect is " + MyAspect);
//		}
		ApproachCircuit = approach;
		if (ApproachCircuit != null) {
			ApproachCircuit.receiveAspectChange(MyAspect);
		}
	};
	
	@Override
	public void addListener(TrackCircuitListener l) {
		if (!Listeners.contains(l)) {
			Listeners.add(l);
		}
	}

	@Override
	public void removeListener(TrackCircuitListener l) {
		if (Listeners.contains(l)) {
			Listeners.remove(l);
		}
	}

	@Override
	public void sendAspectChange(int newAspect) {
		if (ApproachCircuit == null) {
//			System.out.println(toString() + " changing from " + MyAspect + " to " + newAspect);
			MyAspect = newAspect;
		}
//		else if ((newAspect != -1) && (newAspect != MyAspect)) {
		else if (newAspect != MyAspect) {
//			System.out.println(toString() + " forwarding aspect " + newAspect + " to " + ApproachCircuit.toString());
			MyAspect = newAspect;
			ApproachCircuit.receiveAspectChange(newAspect);
		}
	}
	
	@Override
	public void receiveAspectChange(int newAspect) {
//		if (newAspect != -1) {
			AdvanceAppearance = newAspect;
			for (TrackCircuitListener tc : Listeners) {
				tc.setAdvanceAspect(newAspect);
			}
//		}
	}
	
	@Override
	public int getLastAppearance() {
		return AdvanceAppearance;
	}
	
	@Override
	public String toString() {
		return TypeId + ((Identity == null) ? "" : Identity);
	}
}
/* @(#)DefaultTrackCircuit.java */
