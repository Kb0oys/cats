/* Name: Train.java
 *
 * What:
 *   This class contains the information about a train.
 */
package cats.trains;

import cats.apps.Crandic;
import cats.automatedTests.TestMaker;
import cats.common.Constants;
import cats.common.DebugBits;
import cats.common.Sides;
import cats.crew.*;
import cats.jmri.OperationsTrains;
import cats.layout.FontList;
import cats.layout.Logger;
import cats.gui.crew.CrewPicker;
import cats.gui.CtcFont;
import cats.gui.frills.FrillLoc;
import cats.gui.frills.TrainFrill;
import cats.gui.jCustom.ColorFinder;
import cats.gui.jCustom.FontFinder;
import cats.gui.jCustom.JListDialog;
import cats.gui.Screen;
import cats.gui.store.TimeSpec;
import cats.gui.TrainLabel;
import cats.layout.ColorList;
import cats.layout.FastClock;
import cats.layout.items.BlkEdge;
import cats.layout.items.Block;
import cats.layout.items.SecEdge;
import cats.layout.items.Section;
import cats.layout.store.FieldPair;
import cats.layout.store.GenericRecord;
import cats.layout.store.StoredObject;
import cats.layout.xml.*;

import java.awt.event.MouseEvent;
import java.awt.event.KeyEvent;
import java.awt.Point;
import java.util.BitSet;
import java.util.Iterator;

import javax.swing.JPanel;

import org.jdom2.Element;
/**
 *  This class contains the information about a train.
 * <p>Title: CATS - Computer Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2010, 2011, 2012, 2014, 2015, 2020, 2021, 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class Train
implements StoredObject, XMLEleObject {
	/**
	 * is the tag for identifying a Train in the XML file.
	 */
	public static final String XML_TAG = "TRAIN";

	/**
	 * is the property tag for picking out the Train's name.
	 */
	public static final String TRAIN_NAME = "TRAIN_NAME";

	/**
	 * is the property tag for picking out the Train's identity.
	 */
	public static final String TRAIN_SYMBOL = "TRAIN_SYMBOL";

	/**
	 * is the property tag for picking out the lead engine.
	 */
	public static final String ENGINE = "ENGINE";

	/**
	 * is the property tag for picking out the transponding flag.
	 */
	public static final String TRANSPONDING = "TRANSPONDING";

	/**
	 * is the property tag for picking out the Caboose.
	 */
	public static final String CABOOSE = "CABOOSE";

	/**
	 * is the property tag for picking out the Crew.
	 */
	public static final String CREW = "CREW";

	/**
	 * is the property tag for the time the crew goes on duty.
	 */
	public static final String ONDUTY = "ONDUTY";

	/**
	 * is the property tag for the time the train departs.
	 */
	public static final String DEPARTURE = "DEPARTURE";

	/**
	 * is the property tag for the TrainStat Font of the train.
	 */
	public static final String FONT = "FONT";

	/**
	 * is the property tag for the length of the train
	 */
	public static final String LENGTH = "LENGTH";

	/**
	 * is the property tag for the weight of the train
	 */
	public static final String WEIGHT = "WEIGHT";

	/**
	 * is the property tag for the number of cars in the train
	 */
	public static final String CARS = "CARS";

	/**
	 * is the property tag for the auto-terminate in Operations flag
	 */
	public static final String AUTOTERMINATE = "AUTOTERMINATE";

	/**
	 * is the property tag for the label background enable flag
	 */
	public static final String LABELBACKGROUND = "LABELBACKGROUND";

	/**
	 * is the property tag for the working status of the train
	 */
	public static final String WORKING_STATUS = "STATUS";

	/**
	 * is the property tag on the hidden value for the Station
	 */
	private static final String STATION = "STATION";

	/**
	 * is the property tag on the hidden value for the train's Location
	 */
	private static final String LOCATION = "LOCATION";

	/**
	 * is the String used to identify a Train as unassigned.
	 */
	public static final String UNASSIGNED = "UNASSIGNED";

	/**
	 * is the pop up selection to change the crew on a Train.
	 */
	private static final String CHANGE_CREW = "Change Crew";

	/**
	 * is the pop up selection to tie down the Train.
	 */
	private static final String TIE_DOWN = "Tie Down Train";

	/**
	 * is the pop up selection to rerun the Train.
	 */
	private static final String RERUN_TRAIN = "Run Train again";

	/**
	 * is the pop up selection to remove the Train from the panel.
	 */
	private static final String REMOVE_TRAIN = "Remove Train";

	/**
	 * is the automated test tag on moving a train
	 */
	/**
	 * is for generating the pop up items.
	 */
	private static final String[] MenuItems = {
		TIE_DOWN,
		RERUN_TRAIN,
		REMOVE_TRAIN,
		CHANGE_CREW
	};

	/**
	 * Strings that describe the status of the train
	 */
	/**
	 * indicates that the Train has not run or is running
	 */
	public static final String TRAIN_ACTIVE = "TRAIN_ACTIVE";

	/**
	 * indicates that the Train has been tied down
	 */
	public static final String TRAIN_TIED_DOWN = "TRAIN_TIED_DOWN";

	/**
	 * indicates that the train has terminated and been removed
	 */
	public static final String TRAIN_TERMINATED = "TRAIN_TERMINATED";

	/**
	 * indicates that the train is being rerun
	 */
	public static final String TRAIN_RERUN = "TRAIN_RERUN";

	/**
	 * is the automated test tag on moving a train via the mouse
	 */
	public static final String TRAIN_MOVED = "TRAIN_MOVED";
	
	/*
	 * The following identify bits for selecting Trains.  They are not private
	 * because the Roster uses them for constructing selection masks.
	 */
	/**
	 * is set when the Train is created.
	 */
	static final int CREATED_BIT = 0;

	/**
	 * is set when the Train is Positioned on the layout.
	 */
	static final int POSITIONED_BIT = 1;

	/**
	 * is set when the Train has a crew assigned to it.  This bit could be
	 * set in conjunction with other bits.
	 */
	static final int ASSIGNED_BIT = 2;

	/**
	 * is set when the Train has the focus - will move in response to the
	 * keyboard.
	 */
	static final int FOCUS_BIT = 3;

	/**
	 * is set when the Train has completed its work, but still trips a
	 * detector on the layout.
	 */
	static final int TIEDUP_BIT = 4;

	/**
	 * is set when the Train has completed its work and is removed from
	 * the layout - is not detecting.
	 */
	static final int REMOVED_BIT = 5;

	/**
	 * is the number of bits used by the above.
	 */
	static final int STATE_BITS = 6;

	/**
	 * selects Trains in the Created State only.
	 */
	static BitSet Created;

	/**
	 * selects Trains in the Created and Positioned States only.
	 */
	static BitSet Unassigned;

	/**
	 * selects the Trains being run.
	 */
	static BitSet Active;

	/**
	 * selects the Trains that have completed their runs.
	 */
	static BitSet Done;

	/**
	 * selects Train in the Removed state only.
	 */
	static BitSet Removed;

	/**
	 * locates the current background color for the train label
	 */
	static ColorFinder BackgroundColor;

	/**
	 * is the Crew assigned to the Train.
	 */
	private Crew MyCrew;

	/**
	 * is the lead engine on the train.
	 */
	private String LeadEngine;

	/**
	 * is the state of the Train.
	 */
	private BitSet TrainState;

	/**
	 *  is the Label showing the Train's identity on the screen.
	 */
	private TrainFrill TrainLab;

	/**
	 * is the Section where the Train is located.
	 */
	private Section Location;

	/**
	 * is the SecEdge that the train is sitting on.
	 */
	private SecEdge Side;

	/**
	 * is the name of the station where the train sits.
	 */
	private String Station;

	/**
	 * is the GenericRecord defining the attributes of a Train that can
	 * be read from an XML file or edited.
	 */
	private GenericRecord TrainFields;

	/**
	 * is the time that the crew went on duty.
	 */
	private int TimeOnDuty;

	/**
	 * is the last Block visited by the train.  This is used in preventing unintentional
	 * reverse movements.
	 */
	private Block LastBlock;
	
	/**
	 * constructs a Train, with a given set of initial values.
	 */
	public Train() {
		TrainState = new BitSet(STATE_BITS);
		TrainState.set(CREATED_BIT);
		TimeOnDuty = TimeSpec.UNKNOWN_TIME;
		LeadEngine = new String(" ");
	}

	/**
	 * is the method used to give an Object its description.
	 *
	 * @param description is the GenericRecord describing the Object.
	 */
	public void linkDescription(GenericRecord description) {
		String value;
		TrainFields = description;
		value = new String((String) description.findValue(CREW)).trim();
		if (!value.equals("")) {
			crewChange(value, description.findPair(ONDUTY), description.findPair(DEPARTURE));
		}
		value = new String((String) description.findValue(ENGINE)).trim();
		if (!value.equals("")) {
			engineChange(value);
		}
		value = new String((String) description.findValue(GenericRecord.STATUS)).trim();
		if (!value.equals(GenericRecord.UNCHANGED)) {
			statusChange(value);
		}
	}

	/**
	 * is invoked to tell the Object that the description has been updated.  Several special conditions
	 * are handled in a Train record.
	 * 1. an engine change could requiring changing the label
	 * 2. a crew change could require changing the scheduled departure time and the on duty time
	 *
	 * @param description is the updated GenericRecord describing the Object.
	 */
	public void updateDescription(GenericRecord description) {
		String fieldTag;
		FieldPair newPair;
		FieldPair oldPair;
		FieldPair depart;
		FieldPair adjust;
		GenericRecord broadcastRec = new GenericRecord();
		broadcastRec.add(TrainFields.findPair(TRAIN_SYMBOL));
			// The crew change needs to be done first, because it can update ONDUTY and DEPARTURE
		;
		if (((newPair = description.findPair(CREW)) != null) && ((oldPair = TrainFields.findPair(CREW)) != null)) {
			if (!((String) oldPair.FieldValue).equals(newPair.FieldValue)) {
				if ((depart = description.findPair(DEPARTURE)) == null) {
					depart = TrainFields.findPair(DEPARTURE);
				}
				if ((adjust = description.findPair(ONDUTY)) == null) {
					adjust = TrainFields.findPair(ONDUTY);
				}
				crewChange((String) newPair.FieldValue, adjust, depart);
			}
		}
		for (Iterator<FieldPair> iter = description.iterator(); iter.hasNext(); ) {
			newPair = iter.next();
			fieldTag = newPair.FieldTag;
			oldPair = TrainFields.findPair(fieldTag);
			if (oldPair != null) {
				if (!newPair.FieldValue.equals(oldPair.FieldValue)) {
					// need to keep the key for the change broadcast
//					if (CREW.equals(fieldTag)) {
//						crewChange((String) newPair.FieldValue);
//						broadcastRec.add(new FieldPair(ONDUTY, TimeSpec.convertMinutes(getOnDuty())));
//						TrainFields.replacePair(newPair);
//						broadcastRec.add(newPair);
//					}
//					else if (ENGINE.equals(fieldTag)) {
					if (ENGINE.equals(fieldTag)) {
						engineChange((String) newPair.FieldValue);
						TrainFields.replacePair(newPair);
						broadcastRec.add(newPair);
					}
					else if (TRAIN_SYMBOL.equals(fieldTag)) {
						TrainFields.replacePair(newPair);
						changeLabel(TrainLabel.TrainLabelType.getFlagValue());
						broadcastRec.add(newPair);
					}
					else if (LABELBACKGROUND.equals(fieldTag)) {
						TrainFields.replacePair(newPair);
						if (TrainLab != null) {
							TrainLab.changeOpacity(getOpacity());
						}
						broadcastRec.add(newPair);
					}
					else if (GenericRecord.STORED_OBJECT.equals(fieldTag)) {
						newPair.FieldValue = this;
					}
					else {
						TrainFields.replacePair(newPair);
						broadcastRec.add(newPair);
					}
				}
			}
		}
		// the record must have more than the key field
		if (broadcastRec.size() > 1) {
			TrainStore.TrainKeeper.broadcastChange(broadcastRec);
		}
	}

	/**
	 * is invoked to tell the Object that it is being deleted.  It should
	 * clean up any associations it has with other Objects, then remove its
	 * description from its Store.
	 *
	 * For a Train, it is never called because Trains are never deleted.
	 */
	public void destructor() {
	}

	/**
	 * returns the name of the Train.
	 *
	 * @return the name of the Train.
	 */
	public String getName() {
		return new String( (String) TrainFields.findValue(TRAIN_NAME));
	}

	/**
	 * changes the name of the Train.
	 *
	 * @param name is the new name.
	 */
	public void setName(String name) {
		TrainFields.replaceValue(TRAIN_NAME, name);
	}

	/**
	 * returns the timetable identity of the Train.
	 *
	 * @return the identity of the Train.
	 */
	public String getSymbol() {
		return new String( (String) TrainFields.findValue(TRAIN_SYMBOL));
	}

	/**
	 * returns the lead engine.
	 *
	 * @return the lead engine.
	 */
	public String getEngine() {
		return new String( (String) TrainFields.findValue(ENGINE));
	}

	/**
	 * sets the Engine number.
	 *
	 * @param engine is the engine number.  It can be null.
	 */
	public void setEngine(String engine) {
		TrainFields.replaceValue(ENGINE, new String(engine));
		LeadEngine = new String(engine);
		if (TrainLabel.TrainLabelType.getFlagValue()) {
			changeLabel(true);
		}
	}

	/**
	 * is called from the editor and network to change the lead
	 * locomotive
	 * @param newEngine is the sting that identifies the new engine.
	 * It can be null.
	 */
	public void engineChange(String newEngine) {
		if (!LeadEngine.equals(newEngine)) {
			setEngine(newEngine);
		}    
	}

	/**
	 * returns the Caboose number.
	 *
	 * @return the caboose decoder address.
	 */
	public String getCaboose() {
		return new String( (String) TrainFields.findValue(CABOOSE));
	}

	/**
	 * sets the caboose decoder address.
	 *
	 * @param caboose is the transponding decoder address of the caboose.
	 */
	public void setCaboose(String caboose) {
		TrainFields.replaceValue(CABOOSE, new String(caboose));
	}
	/**
	 * sets the time at which the crew went on duty.  The algorithm for computing
	 * the on duty time is based on the recipes contained in the adjustment and
	 * schedule Strings.  If not absolute times they are converted to absolute times
	 * and the results are stored back into the ONDUTY and DEPARTURE fields.
	 * In addition, TimeOnDuty is updated to the computed departure time.
	 * 
	 * @param adjustment is the recipe for adjusting the scheduled departure time
	 * @param schedule is the recipe for computing the scheduled departure time
	 */
	private void setOnDuty(FieldPair adjustment, FieldPair schedule) {
//		String crewTime;
//		if (TimeOnDuty == TimeSpec.UNKNOWN_TIME) {
//			crewTime = ( (String) TrainFields.findValue(ONDUTY)).trim();
//			if (crewTime.length() == 0) {
//				TimeOnDuty = TimeSpec.currentTime();
//			}
//			else {
//				TimeOnDuty = TimeSpec.convertString(crewTime, getDeparture());
//			}
//		}
//		else {
//			TimeOnDuty = TimeSpec.currentTime();
//		}
		/** next iteration from the above **/
//		FieldPair onDutyPair = TrainFields.findPair(ONDUTY);
//		String onDutyTime;
//		FieldPair schedPair = TrainFields.findPair(DEPARTURE);
//		String schedTime;
//		// this determines the scheduled time and makes it absolute in TrainFields
//		if (schedPair == null) {
//			schedTime = getDeparture("");
//		}
//		else {
//			schedTime = getDeparture((String) schedPair.FieldValue);
//			schedPair.FieldValue = schedTime;
//		}
//		if (onDutyPair == null) {
//			onDutyTime = adjustOnDuty("", schedTime);
//		}
//		else {
//			onDutyTime = adjustOnDuty((String) onDutyPair.FieldValue, schedTime);
//			onDutyPair.FieldValue = onDutyTime;
//		}
//		TimeOnDuty = TimeSpec.toMinutes(onDutyTime);
		String resolved = getDeparture(schedule);
		TrainFields.findPair(DEPARTURE).FieldValue = resolved;
		resolved = adjustOnDuty(adjustment, schedule);
		TrainFields.findPair(ONDUTY).FieldValue = resolved;
		TimeOnDuty = TimeSpec.toMinutes(resolved);
	}

	/**
	 * is called to find out when the crew went on duty.
	 *
	 * @return the number of minutes past midnight for when the crew went on duty
	 * on the train.  If crew has not been assigned to the Train,
	 * TimeSpec.UNKNOW_TIME is returned.
	 */
	public int getOnDuty() {
		return TimeOnDuty;
	}

	/**
	 * Determines when the crew went on duty for this Train.  This gets kind of complicated.
	 * 1. a departure time is computed from the departure parameter:
	 *   a. if departure is an empty string, then there is no scheduled departure time,
	 *     so the current time will be used
	 *   b. if departure begins with + or -, then the schedule departure is relative
	 *       to the time the operating session began
	 *   c. if departure is absolute, then the departure time is that absolute time
	 * 2. the departure time is adjusted to account for crew checking out the train,
	 *   running from the initial terminal, etc.
	 *   a. if adjustment is an empty string, then there is no adjustment.  The departure
	 *     time is used.
	 *   b. if adjustment begins with + or -, then the adjustment is relative to
	 *     the departure time.  Generally, a + is in the future, so nonsensical
	 *   c. finally, adjustment is the departure time.
	 *   
	 * @param adjustment is the local adjustment to the scheduled departure time (2. above)
	 * @param departure is the scheduled departure time (1. above)
	 * 
	 * @return the departure time, adjusted by the above
	 */
	public String adjustOnDuty(final String adjustment, final String departure) {
//		String trainTime = getDeparture(departure);
//		String crewTime = ( (String) TrainFields.findValue(ONDUTY)).trim();
//		if (crewTime.length() == 0) {
//			departure = trainTime;
//		}
//		else if (((crewTime.charAt(0) == '-') || (crewTime.charAt(0) == '+'))) {
//			departure = TimeSpec.convertMinutes(TimeSpec.convertString(
//					crewTime, trainTime));
//		}
//		else {
//			departure = crewTime;
//		}
//		TrainFields.replaceValue(ONDUTY, departure);
		String trainTime;
		String schedule = getDeparture(departure);
		if (schedule.equals("")) {
			schedule = TimeSpec.getCurrentTime();
		}
		if ((adjustment == null) || ((trainTime = adjustment.trim()).length() == 0)) {
			trainTime = schedule;
		}
		else {
			trainTime = TimeSpec.convertMinutes(TimeSpec.convertString(trainTime,
					schedule));			
		}
		return trainTime;
	}

	/**
	 * Tis method determines the time the crew when on the clock on a train,
	 * like the method adjustOnDuty(String, String).  Unlike its twin, this method
	 * takes 2 parameters containing the adjustment and schedule descriptions.
	 * The method extracts them, determines the absolute time for each, and replaces
	 * the field values.
	 * @param adjustment is an ONDUTY field containing the recipe for adjusting
	 * the scheduled departure time
	 * @param departure is a DEPARTURE field containing the recipe for computing
	 * when the train is scheduled to leave
	 * @return the computed on duty time
	 */
	public String adjustOnDuty(FieldPair adjustment, FieldPair departure) {
		String trainTime;
		String schedule = getDeparture(departure);
		if (adjustment == null) {
			return adjustOnDuty("", schedule);
		}
		trainTime = adjustOnDuty((String) adjustment.FieldValue, schedule);
		adjustment.FieldValue = trainTime;
		return trainTime;
	}

	/**
	 * is called to get the scheduled departure time.  The value returned will
	 * be an absolute
	 * TimeSpec.  Though the departure time may have been read as a relative
	 * time, this method will convert it to absolute (using the operating session
	 * start time as a base).
	 *
	 * @param departure is the adjustment to the operating session start time
	 * 
	 * @return absolute departure time, which can be
	 *   1. relative to the start of the operating session
	 *   2. a specified time
	 *   3. the current time (departure is the empty String)
	 */
	public String getDeparture(final String departure) {
		String trainTime;
		if ((departure == null) || ((trainTime = departure.trim()).length() == 0)) {
//			trainTime = TimeSpec.getCurrentTime();
			trainTime = "";
		}
		else {
			trainTime = TimeSpec.convertMinutes(TimeSpec.convertString(trainTime,
					FastClock.TheClock.getStartTime()));
		}
		return trainTime;
	}

	/**
	 * is called to convert the recipe for determining when a train is scheduled
	 * to depart to an absolute time.  Unlike getDeparture(String) it extracts
	 * the recipe from a FieldPair, converts it to an absolute time, and
	 * writes it back to the FieldPair.
	 * @param departure is a FeldPair containing the recipe for computing the
	 * scheduled departure time
	 * @return the result of determining the departure time
	 */
	public String getDeparture(FieldPair departure) {
		String trainTime;
		if (departure == null) {
			return "";
		}
		trainTime = getDeparture((String) departure.FieldValue);
		departure.FieldValue = trainTime;
		return trainTime;
	}

	/**
	 * Assigns (or removes) a crew from a train.  The focus is on the data structures
	 * describing the assignment.  A side affect is determining the time on duty.
	 * This method assumes the crew is unassigned when invoked as it does not
	 * remove the crew from any previous assignment.
	 * 
	 * This method broadcasts updates to the crew, departure, and schedules.
	 * @param crew is the new crew, which can be null
	 * @param adjustment is a recipe for adjusting the train's scheduled departure time
	 * @param schedule is a recipe for determining the train's scheduled departure time
	 */
	public void setCrew(final Crew crew, FieldPair adjustment, FieldPair schedule) {
		FieldPair pair = TrainFields.findPair(CREW);
		GenericRecord broadcastRec = new GenericRecord();
		broadcastRec.add(TrainFields.findPair(TRAIN_SYMBOL));
		MyCrew = crew;
		if (MyCrew == null) {
			TrainState.clear(ASSIGNED_BIT);
			if (TrainState.get(FOCUS_BIT)) {
				TrainState.clear(FOCUS_BIT);
				TrainStore.TrainKeeper.clrFocus();
			}
			pair.FieldValue = "";
			TrainFields.findPair(DEPARTURE).FieldValue = "";
			TimeOnDuty = TimeSpec.UNKNOWN_TIME;
		}
		else {
			TrainState.set(ASSIGNED_BIT);
			pair.FieldValue = MyCrew.getCrewName();
			setOnDuty(adjustment, schedule);
		}
		setLabel();
		if (Location != null) { // see if the Train has been placed on the layout
			Location.getTile().requestUpdate();
		}
		broadcastRec.add(pair);
		broadcastRec.add(TrainFields.findPair(DEPARTURE));
		pair = TrainFields.findPair(ONDUTY);
		pair.FieldValue = TimeSpec.convertMinutes(getOnDuty());
		broadcastRec.add(pair);
		TrainStore.TrainKeeper.broadcastChange(broadcastRec);    		
	}
	
	/**
	 * assigns the Crew to the Train.  It does not unassign any current
	 * crew assignment.  It just sets up the assignment.  It does not make
	 * any method calls to a crew.  Thus, the caller is responsible for
	 * unassigning any crew and registering the train with the new crew.
	 *
	 * @param crew is the crew.  It could be null.
	 *
	 * @see cats.crew.Crew
	 */
	public void setCrew(Crew crew) {
		setCrew(crew, TrainFields.findPair(ONDUTY), TrainFields.findPair(DEPARTURE));
	}

	/**
	 * replaces the Crew that is assigned to the Train.  This is different
	 * from setCrew() in that if a Crew is currently assigned to the Train,
	 * then setTrain(null) is called on that Crew, to clear the existing
	 * assignment.
	 *
	 * @param crew is the new crew assignment.  It could be null.
	 * @param adjust is the FieldPair containing the recipe for how to adjust the departure time
	 * @param depart is the FieldPair containing the recipe for how to determine the scheduled departure time
	 *
	 * @see cats.crew.Crew
	 */
	public void replaceCrew(final Crew crew, FieldPair adjust, FieldPair depart) {
		if (crew!= MyCrew) {
			if (MyCrew != null) {
				MyCrew.setTrain(null);
			}
			setCrew(crew, adjust, depart);
			if (crew != null) {
				if (crew.getTrain() != null) {
					crew.getTrain().setCrew(null);
				}
				crew.setTrain(this);
			}
		}
	}

	/**
	 * replaces the Crew that is assigned to the Train.  This is different
	 * from setCrew() in that if a Crew is currently assigned to the Train,
	 * then setTrain(null) is called on that Crew, to clear the existing
	 * assignment.
	 *
	 * @param crew is the new crew assignment.  It could be null.
	 * */
	public void replaceCrew(final Crew crew) {
		replaceCrew(crew, TrainFields.findPair(ONDUTY), TrainFields.findPair(DEPARTURE));
	}

	/**
	 * is called from the editor or network to change the crew
	 * running the Train.
	 * @param crewName is the name of the new Crew (which could be empty or
	 * null)
	 * @param adjust is the FieldPair containing the recipe for how to adjust the departure time
	 * @param depart is the FieldPair containing the recipe for how to determine the scheduled departure time
	 */
	public void crewChange(final String crewName, FieldPair adjust, FieldPair depart) {
		Crew newCrew = Callboard.Crews.findCrew(crewName);
		if ( (newCrew != null) && !newCrew.getExtraFlag()) {
			newCrew = null;
		}
		if (MyCrew != newCrew) {
			replaceCrew(newCrew, adjust, depart);
		}   
	}

	/**
	 * returns the Crew assigned to the Train.
	 *
	 * @return the Crew.  It could be null.
	 *
	 * @see cats.crew.Crew
	 */
	public Crew getCrew() {
		return MyCrew;
	}

//	/**
//	 * set the Transponding flag.
//	 *
//	 * @param trans is true if the lead locomotive has transponding and
//	 * false if it does not.
//	 */
//	public void setTransponding(Boolean trans) {
//		TrainFields.replaceValue(TRANSPONDING, trans);
//	}
//
//	/**
//	 * returns the Transponding flag.
//	 *
//	 * @return true if the lead locomotive reports its engine number and
//	 * false if it doesn't.
//	 */
//	public Boolean getTransponding() {
//		return (Boolean) TrainFields.findValue(TRANSPONDING);
//	}

	/**
	 * returns the AutoTerminate flag.
	 *
	 * @return true if the Operations should be told when the
	 * train terminates
	 */
	public Boolean getAutoTerminate() {
		return (Boolean) TrainFields.findValue(AUTOTERMINATE);
	}

	/**
	 * returns the background opaque/transparency flag on the label.
	 *
	 * @return true if the Operations should be told when the
	 * train terminates
	 */
	public Boolean getOpacity() {
		return (Boolean) TrainFields.findValue(LABELBACKGROUND);
	}

	/**
	 * is a predicate for asking if a Train is in a certain state.
	 *
	 * @param selector is the Train state being searched for.  If any one
	 * of the bits in the BitSet are set, then it matches.
	 *
	 * @param filter is the inverse of selector.  If any one of the bits
	 * in the BitSet are set, then it is rejected.
	 *
	 * @return a Vector containing all Trains whose state is selector.
	 */
	boolean selectTrain(BitSet selector, BitSet filter) {
		if ( ( (selector == null) || (TrainState.intersects(selector))) &&
				( (filter == null) || ! (TrainState.intersects(filter)))) {
			return true;
		}
		return false;
	}

	/**
	 * is invoked to determine the last (not current Block) the Train came from.
	 * It may be null;
	 * @return the prior Block the Train visited.
	 */
	public Block getLastBlock() {
		return LastBlock;
	}
	
	/**
	 * is a query to ask if the Train has not been removed.
	 *
	 * @return true if the Train has not been positioned or is still on
	 * the layout.
	 */
	public boolean isNotRemoved() {
		return selectTrain(null, Removed);
	}

	/**
	 * is a query to determine if a Train has completed its run.
	 *
	 * @return true if it has been terminated or tied up.
	 */
	public boolean hasRun() {
		return selectTrain(Done, null);
	}

	/**
	 * returns the TrainLabel so that the Section can add it to the
	 * GridTile.
	 *
	 * @return TrainLab
	 */
	public TrainFrill getIcon() {
		return TrainLab;
	}

	/**
	 * moves a Train to the TiedDown state.  This method should be called
	 * when the Train has completed its work, but still occupies a Block
	 * on the layout.
	 */
	public void tieDown() {
		if (!TrainState.get(TIEDUP_BIT)) {
			logState(Constants.TIEDDOWN_TAG);
			replaceCrew(null);
			if (TrainState.get(POSITIONED_BIT)) {
				TrainState.clear();
				TrainState.set(TIEDUP_BIT);
				TrainState.set(POSITIONED_BIT);
				setLabel();
				Location.getTile().requestUpdate();
			}
			else {
				TrainState.clear();
				TrainState.set(TIEDUP_BIT);
			}
			if (TrainFields != null) {
				TrainFields.setStatus(Train.TRAIN_TIED_DOWN);      
			}
			if (getAutoTerminate() && OperationsTrains.instance().isEnabled()) {
				OperationsTrains.instance().terminateTrain(getSymbol());
			}
		}
		recordTestCase(TIE_DOWN);
	}

	/**
	 * moves a Train to the Removed state.  This method should be called
	 * only if the Train is no longer on the Layout.
	 */
	public void remove() {
		if (!TrainState.get(REMOVED_BIT)) {
			logState(Constants.TERMINATED_TAG);
			replaceCrew(null);
			TrainState.clear();
			TrainState.set(REMOVED_BIT);
			if (Location != null) {
				Location.addTrain(null, true);
				Location = null;
				Station = null;
			}
			if (TrainLab != null) {
				TrainLab.removeFrill();
			}
			if (TrainFields != null) {
				TrainFields.setStatus(Train.TRAIN_TERMINATED);
			}
			TrainLab = null;
			if (getAutoTerminate()  && OperationsTrains.instance().isEnabled()) {
				OperationsTrains.instance().terminateTrain(getSymbol());
			}
			LastBlock = null;
		}
		recordTestCase(REMOVE_TRAIN);
	}

	/**
	 * moves Train from Removed or Tied Up State to the Created state.
	 */
	public void rerun() {
		if (TrainState.get(TIEDUP_BIT) || TrainState.get(REMOVED_BIT)) {
			TrainState.clear(TIEDUP_BIT);
			TrainState.clear(REMOVED_BIT);
			if (!TrainState.get(POSITIONED_BIT)) {
				TrainState.set(CREATED_BIT);
			}
			if (TrainFields != null) {
				TrainFields.setStatus(GenericRecord.UNCHANGED);
			}
			setLabel();
			logState(Constants.RERUN_TAG);
		}
		recordTestCase(RERUN_TRAIN);
	}

	/**
	 * is called from the gui or network to change the status of a Train
	 * @param status is a String identifying the Trains's new status
	 */
	public void statusChange(String status) {
		if (null != status) {
			if (status.equals(Constants.TERMINATED_STATE)) {
				remove();
			}
			else if (status.equals(Constants.TIED_DOWN_STATE)) {
				tieDown();
			}
			else if (status.equals(Constants.WAITING_STATE)) {
				rerun();
			}
		}
	}

	/**
	 * prints some key properties of the Train.
	 * 
	 * @param logTag is the tag to use in the log file
	 */
	private void logState(String logTag) {
		String info = new String(Constants.QUOTE + getSymbol() + Constants.QUOTE + 
				Constants.FS + TrainState.toString() + Constants.FS + Constants.QUOTE);
		if (Location == null) {
			info = info.concat("unknown" + Constants.QUOTE + Constants.FS);
		}
		else {
			info = info.concat(Logger.formatLocation(Location.getCoordinates(), null) + Constants.QUOTE +
					Constants.FS + Location.hasTrain() + Constants.FS);
		}
		if (TrainLab == null) {
			info = info.concat("label lost");
		}
		else {
			info = info.concat("label exists");
		}
		TrainStore.TrainKeeper.broadcastTimestamp(logTag, info);
	}

	/**
	 * is a query for if the Train is working.
	 *
	 * @return true if the Train has a crew assigned and is positioned on the
	 * layout; otherwise, return false.
	 */
	public boolean isActive() {
		return (TrainState.get(ASSIGNED_BIT) && TrainState.get(POSITIONED_BIT));
	}

	/**
	 * tells the Train to set its color to indicate if the arrow keys move it
	 * or not.
	 *
	 * @param moves is true if it gets the arrow keys and false if it does not.
	 */
	public void setFocus(boolean moves) {
		if (moves) {
			TrainState.set(FOCUS_BIT);
			setLabel();
			Location.getTile().requestUpdate();
		}
		else {
			TrainState.clear(FOCUS_BIT);
			if (isActive()) {
				setLabel();
				Location.getTile().requestUpdate();
			}
		}
	}

	/**
	 * is called when the dispatcher presses a mouse button
	 * on top of the Train's label.  If the left button is used, then the
	 * label can be moved.  If the right button is used, then a popup menu
	 * is generated.
	 *
	 * @param me is the MouseEvent recording the button push/release.
	 *
	 * @see java.awt.event.MouseEvent
	 */
	public void trainButton(MouseEvent me) {
		int mods = me.getModifiers();
		Point p;
		if ( (mods & MouseEvent.BUTTON3_MASK) != 0) {
			TrainStore.TrainKeeper.singleTrainEdit(this);
		}
		else if ((mods & MouseEvent.BUTTON2_MASK) != 0) {
			trainMenu(me.getPoint());      
		}
		else if ( (mods & MouseEvent.BUTTON1_MASK) != 0) {
			p = me.getPoint();
			moveTrain(Screen.DispatcherPanel.locatePt(p));
			recordTestCase(p);
		}
	}

	/**
	 * presents the Menu of operations on the train.
	 *
	 * @param location is the location of the Mouse on the screen.
	 *
	 * @see cats.gui.jCustom.JListDialog
	 */
	public void trainMenu(Point location) {
		String menu[];
		Crew newCrew;
		int selection;
		if (hasRun()) {
			menu = new String[2];
		}
		else {
			menu = new String[3];
			menu[2] = MenuItems[3];
		}
		if (TrainState.get(TIEDUP_BIT)) {
			menu[0] = MenuItems[1];
		}
		else {
			menu[0] = MenuItems[0];
		}
		menu[1] = MenuItems[2];

		if ( (selection = JListDialog.select(setToolTip(), menu, "Train Operation",
				location))
				>= 0) {

			switch (CtcFont.findString(menu[selection], MenuItems)) {
			case 0: // Tie Down was selected
				tieDown();
				break;

			case 1: // Rerun Train was selected.
				rerun();
				break;

			case 2: // Remove Train was selected
				remove();
				break;

			default: // Change crew was selected
				newCrew = CrewPicker.pickOne(Callboard.Crews.getExtras(),
						"Crew Change", location);
				if (newCrew != null) {
					replaceCrew(newCrew);
				}
			}
		}
	}

	/**
	 * selects what to use for the label on the panel.
	 * 
	 * @param useEngine is true to use the train's symbol
	 * and false to use the engine.
	 */
	protected void changeLabel(boolean useEngine) {
		if (TrainLab != null) {
			String engine = getEngine();
			if (useEngine && (engine != null) && (engine.length() != 0)) {
				TrainLab.changePresentation(engine, getOpacity());
			}
			else {
				TrainLab.changePresentation(getSymbol(), getOpacity());
			}
			if (Location != null) {
				Location.getTile().requestUpdate();
			}
		}
	}

	/**
	 * sets the text, foreground color, and font size of the JLabel.
	 */
	private void setLabel() {
		String font;
		if (TrainLab != null) {
			if (TrainState.get(TIEDUP_BIT)) {
				font = FontList.TIEDUP;
			}
			else if (TrainState.get(FOCUS_BIT)) {
				font = FontList.SELECTED;
			}
			else if (TrainState.get(ASSIGNED_BIT)) {
				font = FontList.ACTIVE;
			}
			else {
				font = FontList.ONCALL;
			}
			TrainLab.setPalette(font);
		}
	}

	/**
	 * sets up the ToolTip that contains the Train's name and crew
	 *
	 * This sets the tool tip, but hides TrainLabels from mouse actions,
	 * such as button pushes and drag.
	 *
	 * @return the Strings containing information about the Train
	 */
	private String[] setToolTip() {
		String[] tip = new String[3];
		String s;
		if (TrainLab != null) {
			if (getName() != null) {
				tip[0] = new String(getName());
			}
			else {
				tip[0] = "";
			}
			if (getEngine() != null) {
				tip[1] = "engine is " + getEngine();
			}
			else {
				tip[1] = "";
			}
			if ( (MyCrew != null) && ( (s = MyCrew.getCrewName()) != null)) {
				tip[2] = "crew is " + s;
			}
			else {
				tip[2] = "";
			}
		}
		return tip;
	}

	/**
	 * positions the Train as a result of selecting a location in a Block
	 * menu.
	 *
	 * @param location are the coordinates on the screen of the mouse at
	 * the time the right button was pushed.
	 *
	 * @see java.awt.Point
	 */
	public void positionTrain(Point location) {
		positionTrain(Screen.DispatcherPanel.locatePt(location));
	}
	
	/**
	 * positions the Train when the section where its position is known.
	 *
	 * @param section is the section where the train label will be placed
	 */	
	public void positionTrain(Section section) {
		String label = getEngine();
		if (!TrainLabel.TrainLabelType.getFlagValue() ||
				(label == null) || (label.length() == 0)) {
			label = getSymbol();
		}
		TrainLab = new TrainFrill(label, getOpacity(), FrillLoc.newFrillLoc("UPCENT"),
				new FontFinder(FontList.ONCALL, FontList.FONT_TRAIN), BackgroundColor);
		setLabel();
		TrainLab.setOwner(this);
		if (section != null) {
			LastBlock = null;
			Side = null;
			Location = section;
			Station = Location.addTrain(this, true);
			TrainState.clear(CREATED_BIT);
			TrainState.set(POSITIONED_BIT);
			recordMove(null, null, Side, Location);
		}
	}

	/**
	 * moves the Train to the next Block in the direction of an arrow key.
	 *
	 * @param arrow is an arrow key.
	 *
	 * @see java.awt.event.KeyEvent
	 */
	public void moveTrain(int arrow) {
		int direction;
		SecEdge exitEdge;
		SecEdge enterEdge = null;
		SecEdge newEdge;
		switch (arrow) {
		case KeyEvent.VK_RIGHT:
			direction = Sides.RIGHT;
			break;

		case KeyEvent.VK_LEFT:
			direction = Sides.LEFT;
			break;

		case KeyEvent.VK_DOWN:
			direction = Sides.BOTTOM;
			break;

		case KeyEvent.VK_UP:
			direction = Sides.TOP;
			break;

		default:
			direction = -1;
		}
		if (TestMaker.isRecording()) {
			TestMaker.waitTestCase(new String("Touch " + Sides.EDGENAME[direction] + " Arrow"));
		}
		if (direction >= 0) {
			if ( (exitEdge = Location.getPath(Side, direction)) != null) {
//				if ( (enterEdge = exitEdge.getNeighbor()) != null) {
//					while ( (enterEdge != null) && !enterEdge.getSection().hasTrain()) {
//						newLoc = enterEdge.getSection();
//						if ( ( (exitEdge = enterEdge.traverse()) == null) ||
//								exitEdge.isBlock()) {
//							break;
//						}
//						enterEdge = exitEdge.getNeighbor();
//					}
					if (((enterEdge = exitEdge.getNeighbor()) != null) && !enterEdge.getSection().hasTrain()) {
						newEdge = enterEdge;
					}
					else {
						newEdge = exitEdge;
					}
					while (!exitEdge.isBlock()) {
						if (((enterEdge = exitEdge.getNeighbor()) == null) || enterEdge.getSection().hasTrain()) {
							break;
						}
						newEdge = enterEdge;;
						if ((exitEdge = enterEdge.traverse()) == null) {
							break;
						}
					}
					/**************
					 * There are a bunch of ways out of the above loop:
					 * - enterEdge = null -> use newLoc
					 * - found a Section with a Train -> use newLoc
					 * - exitEdge = null -> use newLoc
					 * - exitEdge is a BlkEdge -> try to use the neighbor Section
					 */
					if ((enterEdge != null) && !enterEdge.getSection().hasTrain() && (exitEdge != null)){
						if (((enterEdge = exitEdge.getNeighbor()) != null) && !enterEdge.getSection().hasTrain()) {
							newEdge = enterEdge;
						}
					}
//					if ( (Location != newLoc) && !newLoc.hasTrain() && (exitEdge != null)) {
//						advanceTrain(enterEdge);
//					}
//				}
					if (Location != newEdge.getSection()) {
						advanceTrain(newEdge);
					}
			}
		}
	}

	/**
	 * moves the Train to a new Section.
	 *
	 * @param sec is the new Section.  If null, nothing happens.
	 */
	public void moveTrain(Section sec) {
		if (sec != null) {
			recordMove(Side, Location, null, sec);
			if (Location != null) {
				if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
					log.info(Constants.LABEL_MOVE + "drag and drop " + getSymbol() + " from " + Location.toString() + " to " + sec);
				}
				LastBlock = (Side == null) ? null : Side.getBlock();
				Location.addTrain(null, true);
				Location = sec;
				Side = null;
				Station = Location.addTrain(this, true);
			}
		}
	}

	/**
	 * moves the train to a new Section.
	 * 
	 * @param where is a String created by the Logger
	 * @see cats.layout.Logger
	 * 
	 * @return null if the String parsed without error or a String describing the error
	 */
	public String moveTrain(String where) {
		int x;
		int xpos;
		int y;
		int ypos;
		Section sec;
		if (where.charAt(0) == '(') {
			xpos = where.indexOf(',');
			if (xpos > 1) {
				x = Integer.parseInt(where.substring(1, xpos));
				ypos = where.indexOf(')');
				if (ypos > (xpos + 1)) {
					y = Integer.parseInt(where.substring(xpos + 1, ypos));
					sec = Screen.DispatcherPanel.locateSection(x, y);
					if (sec != null) {                         
						// positionTrain must be called the first time the Train
						// is placed on the layout to initialize the Train's internal
						// data
						if (getIcon() == null) {
							// There seems to be a bug in Screen.locatePt() in which
							// the upper left corner of a Section is not found.
							Point p = sec.getTile().getClip().getLocation();
							++p.x;
							++p.y;
							positionTrain(p);
						}
						else {
							moveTrain(sec);
						}
						if ((where.length() > (ypos + 1)) && (where.charAt(ypos + 1) == ':')) {
							advanceTrain(sec.getEdge(Integer.parseInt(where.substring(ypos + 2))));
						}
						return null;
					}
					return "unknown location: (" + x + "," + y + ")";
				}
			}
		}
		return "badly formatted train location";
	}

	/**
	 * is invoked in response to a TrainStat message (in other words, from a train
	 * moving on another CATS screen), to move a train from one possible station to another).
	 * It is "possible" because the from and to locations may be coordinates and not stations.
	 * @param fromStation is where the train starts - either a Station or coordinates
	 * @param toStation is where the train ends - either a Station or blank.
	 */
	public void moveTrain(final String fromStation, final String toStation) {
		Block blk;
		BlkEdge blkEdge;
		if ((fromStation != null) && (fromStation.length() > 0) && (fromStation.charAt(0) != '(')) {  // only Station names should be processed
			if (fromStation.equals(Station)) {  // the train moved to a different CATS, so just disable the icon
				if (TrainState.get(FOCUS_BIT)) {
					TrainStore.TrainKeeper.clrFocus();
				}
				if (Location != null) {
					setFocus(false);
					Location.addTrain(null, false);
					Location = null;
					Station = null;
					TrainState.set(POSITIONED_BIT);
				}
				if (TrainLab != null) {
					TrainLab.removeFrill();
				}
				TrainLab = null;
			}
		}

		if ((toStation != null) && (toStation.length() > 0) && (toStation.charAt(0) != '(')) { // only Station names should be processed
			if (!toStation.equals(Station)) {  // this checks for an echo
				if (((blk = Block.findStation(toStation)) != null) && ((blkEdge = blk.findClearRoute()) != null)) {
					positionTrain(blkEdge.getSection());
				}
			}
		}
	}

	/**
	 * advances the train to a SecEdge.
	 *
	 * @param edge is the SecEdge through which the train entered the
	 * Section.
	 */
	public void advanceTrain(SecEdge edge) {
		boolean newBlock = true;
		if (edge != null) {
			recordMove(Side, Location, edge, edge.getSection());
			if (Location != null) {
				if (Side != null) {
					newBlock = Side.getBlock() != edge.getBlock();
				}
				Location.addTrain(null, newBlock);
				Location = edge.getSection();
				LastBlock = (Side == null) ? null : Side.getBlock();
				Side = edge;
				Station = Location.addTrain(this, newBlock);
			}
		}
	}

	/**
	 * records a Train's movement.  There are two sets of parameters for
	 * both of the location the train departs from and the location the
	 * train arrives at.  One of the parameters is the SecEdge the
	 * train is on.  If it is not null, it is used for obtaining a location
	 * description.  If the SecEdge is null, then the method looks at the
	 * other parameter, the Section.  If both are null, then the string
	 * "unknown" is used.
	 *
	 * @param departure is the SecEdge the train begins in (it can be null).
	 * @param depart is the Section the train departs from (it can be null).
	 *    recordMove looks at it, if departure is null.
	 * @param arrival is the SecEdge the train ends up in (it can be null).
	 * @param arrive is the Section the train ends up in (it can be null).
	 *    recordMove looks at it if arrival is null.
	 *
	 */
	private void recordMove(SecEdge departure, Section depart, SecEdge arrival,
			Section arrive) {
		String movement;
		Point dest = null;
		String destEdge = null;
		String station;

		movement = new String(Constants.QUOTE + getSymbol() + Constants.QUOTE_FS);
		if ( (departure != null) && ( (station = departure.identify()) != null)) {
			movement = movement.concat(station);
		}
		else if (depart != null) {
			station = depart.getStation();
			if (station == null) {
				movement = movement.concat(depart.toString());
			}
			else {
				movement = movement.concat(station);
			}
		}
		else {
			movement = movement.concat(Constants.UNKNOWN);
		}
		movement = movement.concat(Constants.QUOTE + Constants.FS_STRING + "to" + Constants.FS + Constants.QUOTE);
		if ( (arrival != null) && ( (station = arrival.identify()) != null)) {
			movement = movement.concat(station);
			dest = arrival.getSection().getCoordinates();
			destEdge = String.valueOf(arrival.getEdge());
			if (OperationsTrains.instance().isEnabled()) {
				OperationsTrains.instance().moveTrain(getSymbol(), station);
			}
		}
		else if (arrive != null) {
			station = arrive.getStation();
			if (station == null) {
				movement = movement.concat(arrive.toString());
			}
			else {
				movement = movement.concat(station);
				if (OperationsTrains.instance().isEnabled()) {
					OperationsTrains.instance().moveTrain(getSymbol(), station);
				}
			}
			dest = arrive.getCoordinates();
		}
		else {
			movement = movement.concat(Constants.UNKNOWN);
		}
		if (dest == null) {
			movement = movement.concat(Constants.QUOTE);
		}
		else {
			movement = movement.concat(Constants.QUOTE_FS + Logger.formatLocation(dest, destEdge) + Constants.QUOTE);
		}
		TrainStore.TrainKeeper.broadcastTimestamp(Constants.MOVE_TAG, movement);
	}

	/**
	 * creates a test case for doing something with a train
	 * @param definition is what is being done with the train
	 */
	private void recordTestCase(String definition) {
		if (TestMaker.isRecording()) {
			Element trigger = new Element(XML_TAG);
			trigger.setAttribute(TestMaker.DELAY, "");
			trigger.setAttribute(TRAIN_SYMBOL, getSymbol());
			trigger.setAttribute("ACTION", definition);
			TestMaker.recordTrigger(trigger, "Delaying for " + definition + " on train " + getSymbol());
		}
	}
	
	/**
	 * creates a test case for moving a train label with the mouse
	 * @param p locates the Section where the train moves to
	 */
	private void recordTestCase(Point p) {
		if (TestMaker.isRecording()) {
			Element trigger = new Element(XML_TAG);
			trigger.setAttribute(TestMaker.DELAY, "");
			trigger.setAttribute(TRAIN_SYMBOL, getSymbol());
			trigger.setAttribute("ACTION", TRAIN_MOVED);
			trigger.setAttribute(Section.X_TAG, String.valueOf(p.x));
			trigger.setAttribute(Section.Y_TAG, String.valueOf(p.y));
			TestMaker.recordTrigger(trigger, "Delaying for moving Train " + getSymbol());
		}		
	}

	  /**
	   * takes a part and processes an autotest trigger from a Train
	   * @param element is the description of the Train event
	   * @return an error String if there is a problem or null if there is not
	   */
	  public static String processTrainElement(Element element) {
		  String symbol = element.getAttributeValue(TRAIN_SYMBOL);
		  String event = element.getAttributeValue("ACTION");
		  Train t;
		  if (symbol == null) {
			  return "missing train symbol";
		  }
		  if (event == null) {
			  return "missing train action";
		  }
		  t = TrainStore.TrainKeeper.getTrain(symbol);
		  if (t == null) {
			  return "could not find a train named " + symbol;
		  }
		  if (TIE_DOWN.equals(event)) {
			  t.tieDown();
		  }
		  else if (RERUN_TRAIN.equals(event)) {
			  t.rerun();
		  }
		  else if (REMOVE_TRAIN.equals(event)) {
			  t.remove();
		  }
		  else if (TRAIN_MOVED.equals(event)) {
			    int x = Integer.parseInt(element.getAttributeValue(Section.X_TAG));
			    int y = Integer.parseInt(element.getAttributeValue(Section.Y_TAG));
			    t.trainButton(new MouseEvent(new JPanel(), 0, 0, MouseEvent.BUTTON1_MASK, x, y, 1, false));
		  }
		  else return event + " is an unknown train action";
		  return null;
	  }

	  @Override
	public String setValue(String eleValue) {
		return new String("A " + XML_TAG + " cannot contain a text field ("
				+ eleValue + ").");
	}

	@Override
	public String setObject(String objName, Object objValue) {
		return new String("A " + XML_TAG + " cannot contain an Element ("
				+ objName + ").");
	}

	@Override
	public String getTag() {
		return new String(XML_TAG);
	}

	@Override
	public String doneXML() {
		return null;
	}

	/**
	 * registers a TrainFactory with the XMLReader.
	 */
	static public void init() {
		//    XMLReader.registerFactory(XML_TAG, new TrainFactory());
		Created = new BitSet(Train.STATE_BITS);
		Created.set(Train.CREATED_BIT);
		Unassigned = new BitSet(Train.STATE_BITS);
		Unassigned.set(Train.CREATED_BIT);
		Unassigned.set(Train.POSITIONED_BIT);
		Active = new BitSet(Train.STATE_BITS);
		Active.set(Train.POSITIONED_BIT);
		Done = new BitSet(Train.STATE_BITS);
		Done.set(Train.TIEDUP_BIT);
		Done.set(Train.REMOVED_BIT);
		Removed = new BitSet(Train.STATE_BITS);
		Removed.set(Train.REMOVED_BIT);
		BackgroundColor = new ColorFinder(ColorList.LABELBACKGROUND);
	}

	/**
	 * is invoked to return the working status of the train.
	 * @return
	 * <ul>
	 * <li> TERMINATED_STATE
	 * <li> TIED_DOWN_STATE
	 * <li> WORKING_STATE
	 * <li> WAITING_STATE
	 * </ul>
	 */
	public String getTrainStatus() {
		String status;
		if (TrainState.get(REMOVED_BIT)) {
			status = Constants.TERMINATED_STATE;
		}
		else if (TrainState.get(TIEDUP_BIT)) {
			status = Constants.TIED_DOWN_STATE;
		}
		else if (TrainState.get(ASSIGNED_BIT)) {
			status = Constants.WORKING_STATE;
		}
		else {
			status = Constants.WAITING_STATE;
		}
		return status;
	}

	/**
	 * is invoked to retrieve internal values, formatted in a String
	 * as tag=value subStrings.  The derived class decides which
	 * values (and tags) it wants to expose.
	 * @return a String containing "tag=value" substrings.
	 */
	public String getHiddenValues() {
		String status = getTrainStatus();
		String contents;
		contents = new String(WORKING_STATUS + FieldPair.SEP + Constants.QUOTE + status + Constants.QUOTE);
		if (MyCrew != null) {
			contents = contents.concat(Constants.FS_STRING + CREW + FieldPair.SEP + Constants.QUOTE +
					MyCrew.getCrewName() + Constants.QUOTE);
		}
		if (Station != null) {
			contents = contents.concat(Constants.FS_STRING + STATION + FieldPair.SEP + Constants.QUOTE +
					Station + Constants.QUOTE);
		}
		if (Side != null) {
			contents = contents.concat(Constants.FS_STRING + LOCATION + FieldPair.SEP + Constants.QUOTE +
					Logger.formatLocation(Side.getSection().getCoordinates(), String.valueOf(Side.getEdge()))
					+ Constants.QUOTE);
		}
		else if (Location != null) {
			contents = contents.concat(Constants.FS_STRING + LOCATION + FieldPair.SEP + Constants.QUOTE +
					Logger.formatLocation(Location.getCoordinates(), null) + Constants.QUOTE);     
		}
		return contents;
	}
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Train.class.getName());
}
/* @(#)Train.java */
