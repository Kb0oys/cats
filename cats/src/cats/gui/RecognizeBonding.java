/**
 * Name: RecognizeBonding.java
 * 
 * What:
 *   This file contains a boolean for how CATS handles "dark" territory - Blocks
 *   without occupancy detectors.  Originally, CATS did not distinguish between
 *   dark and detected Blocks when setting routes or determining signal indications.
 *   This permitted CATS to work as a magnet board similar to how it works as a
 *   dispatcher panel, allowing the user to add detection after getting CATS going.
 *   <p>
 *   However, with the introduction of Restricting signals protecting end of track
 *   and unbonded track, it is necessary to identify unbonded track.  Rather than
 *   give up the magnet board functionality, this boolean provides the user a way
 *   to specify how he wants unbonded track to be handled.  False (the default to
 *   be compatible with older behavior) means that detection does not enter into
 *   route creation and signal indication determination.  True means that an
 *   undetected Block is treated as "end of track" for the purposes of signals
 *   and route creation.
 *   
 * Special Considerations:
 *   This flag is established when the panel is loaded and cannot be changed
 *   afterwards; therefore, it does not appear on any CATS menu.
 */
package cats.gui;

/**
 *   This file contains a boolean for how CATS handles "dark" territory - Blocks
 *   without occupancy detectors.  Originally, CATS did not distinguish between
 *   dark and detected Blocks when setting routes or determining signal indications.
 *   This permitted CATS to work as a magnet board similar to how it works as a
 *   dispatcher panel, allowing the user to add detection after getting CATS going.
 *   <p>
 *   However, with the introduction of Restricting signals protecting end of track
 *   and unbonded track, it is necessary to identify unbonded track.  Rather than
 *   give up the magnet board functionality, this boolean provides the user a way
 *   to specify how he wants unbonded track to be handled.  False (the default to
 *   be compatible with older behavior) means that detection does not enter into
 *   route creation and signal indication determination.  True means that an
 *   undetected Block is treated as "end of track" for the purposes of signals
 *   and route creation.
 *   
 * Special Considerations:
 *   This flag is established when the panel is loaded and cannot be changed
 *   afterwards; therefore, it does not appear on any CATS menu.
 * 
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
public class RecognizeBonding extends BooleanGui {
    /**
     * is the tag for identifying a StartTrainStat Object in the XMl file.
     */
    static final String XMLTag = "BONDINGLABEL";
    
    /**
     * is the label on the JCheckBoxMenuItem
     */
    static final String Label = "Recognize Unbonded Track";
    
    /**
     * is the singleton.
     */
    public static RecognizeBonding BondingFlag;

   /**
     * constructs the factory.
     */
    public RecognizeBonding() {
      super(Label, XMLTag, false);
      BondingFlag = this;
    }
}
/* @(#)RecognizeBonding.java */
