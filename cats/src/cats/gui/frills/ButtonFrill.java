/* Name: ButtonFrill.java
 *
 * What:
 * a class for painting buttons on Tiles.  In designer, it is just a place holder for
 * the real thing in CATS.  Its function is just to show what the CATS panel will look like.
 */
package cats.gui.frills;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import cats.gui.DispPanel;
import cats.gui.MouseUser;
import cats.layout.items.ButtonItem;

/**
 * a class for painting buttons on Tiles.  In designer, it is just a place holder for
 * the real thing in CATS.  Its function is just to show what the CATS panel will look like.
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class ButtonFrill extends ImageFrill implements MouseUser {
	
	/**
	 * the Button logic
	 */
	private final ButtonItem MyButton;
	
	/**
	 * the primary image
	 */
	private Image PrimaryImage;
	
	/**
	 * the alternate image
	 */
	private Image AlternateImage;
	
	/**
	 * a flag where true means the images should be sized to fit in a Grid Tile.
	 * False means do not adjust the size.
	 */
	private final boolean ScaledToFit;
	
	/**
	 * the constructor.  The upper left corner of the image is drawn in
	 * the upper left corner of the GridTile.
	 *
	 * @param button is ButtonItem providing the logic over the image
	 * @param primary is the path to the file containing the Primary Image.
	 * @param alternate is the path to the file containing the Alternate image
	 * @param fitToGrid is true to scale the active image to fit in a grid tile
	 * 	and false to laeve it as its native size
	 */
	public ButtonFrill(final ButtonItem button, final String primary, final String alternate, final boolean fitToGrid) {
		super(primary);
		File f;

		MyButton = button;
		try {
			f = new File(alternate);
			AlternateImage = ImageIO.read(f);
		}
		catch (IOException err) {
			log.warn("Failure to open Alternate Image file " + alternate);
			System.out.println("Failure to open Alternate Image " + alternate);		  
		}
		if (MyImage == null) {
			if (AlternateImage != null) {
				MyImage = AlternateImage;
			}
		}
		else {
			PrimaryImage = MyImage;
			if (AlternateImage == null) {
				AlternateImage = PrimaryImage;
			}
		}
		ScaledToFit = fitToGrid;
	}

	@Override
	 public void decorate(Graphics g) {
		 if (ScaledToFit) {
			 if (MyImage != null) {
				 g.drawImage(MyImage, Bounds.x, Bounds.y, Bounds.width, Bounds.height, DispPanel.ThePanel);
			 }
		 }
		 else {
			 super.decorate(g);			 
		 }
	 }
	
	/**
	 * changes the image between primary and alternate
	 * @param selectPrimary is true to display the primary image and false to display the
	 * alternate image
	 */
	public void setImage(final boolean selectPrimary) {
		if (selectPrimary) {
			if (MyImage == AlternateImage) {
				MyImage = PrimaryImage;
			}
		}
		else if (MyImage == PrimaryImage) {
			MyImage = AlternateImage;
		}
	}

	@Override
	public MouseUser mouseDown(MouseEvent event) {
		if (Bounds.contains(event.getPoint())) {
			return this;
		}
		return null;
	}
	
	  @Override
	  public boolean mousePush(MouseEvent event) {
		  return false;
	  }
	  
	@Override
	public boolean finishMouse(MouseEvent event) {
		MyButton.flipState(event);
		return true;
	}
}
/* @(#)ButtonFrill.java */
