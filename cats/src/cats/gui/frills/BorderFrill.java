/* Name: BorderFrill.java
 *
 * What:
 *   This file defines a Singleton class for painting borders on JLabels.  According to
 *   "Swing, second edition" by Robinson and Vorobiev, Swing Borders are not Swing
 *   Components; therefore, one Border instance can service multiple Swing Cpmponents.
 *   This instance does that for CATS frills derived from JLabels.
 */
package cats.gui.frills;

import javax.swing.border.LineBorder;

import cats.gui.jCustom.ColorFinder;
import cats.layout.ColorList;

/**
  *   This file defines a Singleton class for painting borders on JLabels.  According to
 *   "Swing, second edition" by Robinson and Vorobiev, Swing Borders are not Swing
 *   Components; therefore, one Border instance can service multiple Swing Cpmponents.
 *   This instance does that for CATS frills derived from JLabels.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class BorderFrill {

	/**
	 * the Singleton
	 */
	static private LineBorder BorderManager;
	
	/**
	 * the color of the border
	 */
	static private ColorFinder BorderColor;
	
	/**
	 * this creates and accesses the Singleton
	 * @return the LineBorder Singleton
	 */
	static public LineBorder getBorderManager() {
		if (BorderManager == null) {
			BorderColor = new ColorFinder(ColorList.LABELBORDER);
			BorderManager = new LineBorder(BorderColor.getColor(), 1, true);
		}
		return BorderManager;
	}
	
	/**
	 * changes the color on the BorderManager
	 */
	static public void rebuildBorderManager() {
		BorderManager = null;
	}
}
/* @(#)BorderFrill.java */
