/* Name: LabelFrill.java
 *
 * What:
 *   This file defines a class for displaying the local switch unlocked
 *   symbol.
 */
package cats.gui.frills;

import cats.common.UniCode;
import cats.gui.jCustom.FontFinder;
import cats.layout.FontList;

/**
 *   This file defines a class for displaying the local switch unlocked
 *   symbol.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class LockFrill extends LabelFrill {

	/**
	 * a lookup table for determining where to place the lock symbol, based
	 * on the tracks that for the points and the edge on which they meet.
	 * 
	 * The first dimension is the edge number (0 = right, 1 = bottom, 2 = left, 3 = top).
	 * The second dimension is the lowest numbered route track.
	 * The third dimension is the next highest numbered route track.
	 * 
	 * Note that this is a sparse array.  Some combinations are impossible.
	 * For example, a route track can not be the same edge as the points edge.
	 * The top edge cannot be the lowest numbered route edge because there
	 * must be at least two route edges.  There are only three possibilities for each
	 * Points edge.
	 * 
	 * I might be able to simplify this and key only on the points edge.
	 */
//	static private final String[][][] LockPosition = {
//			{ // right edge
//				{},  // first route is right edge - cannot be
//				{    // first route is bottom edge
//					null,		// second route is right edge - cannot be the same as the points edge
//					null,		// second route is bottom edge - cannot be same as first edge
//					"UPRIGHT",	// second route is left
//					"UPRIGHT"	// second route is top
//				},  
//				{    // first route is left edge
//					null,		// second route is right edge - cannot be
//					null,		// second route is bottom edge - already used above
//					null,		// second route is left edge - cannot be the same as first edge
//					"UPRIGHT"	// second edge is top - the only choice
//				},  
//				{}   // first route is top edge - cannot be - need a second route
//			},
//			{  // bottom edge
//				{	// first route is right edge
//					null,		// second route is right edge - cannot be the same as the first edge
//					null,		// second route is bottom - cannot be the same as the points edge
//					"LOWRIGHT",	// second route is left
//					"LOWRIGHT"	// second route is top
//				},  
//				{},  // first route is bottom edge - cannot be the same as the points edge
//				{  // first route is left edge
//					null,		// second route is right edge - already used above
//					null,		// second route is bottom edge - cannot be the same as the points edge
//					null,		// second route is left edge - cannot be the same as first edge
//					"LOWRIGHT"	// second route is top - the only choice
//				},
//				{}   // first route is top edge - cannot be	- need a second route		
//			},
//			{  // left edge
//				{  // first route is right edge
//					null,		// second route is right edge - cannot be the same as the first edge
//					"LOWLEFT",	// second route is bottom edge
//					null,		// second route is left edge - cannot be the same as the points edge
//					"LOWLEFT"	// second route is top edge
//				},
//				{  // first route is bottom edge
//					null,		// second route is left edge- - already used above
//					null,		// second route is bottom edge - cannot be the same as the first edge
//					null,		// second route is left edge - cannot be the same as the points edge
//					"LOWLEFT"	// second route is top edge - the only choice
//				},
//				{},  // first route is left edge - cannot be the same as the points edge
//				{}   // first route is top edge - cannot be	- need a second route			
//			},
//			{ // top edge
//				{  // first route is right edge
//					null,		// second route is right edge - cannot be the same as the first edge
//					"UPLEFT",	// second route is bottom
//					"UPLEFT",	// second route is left
//					null		// second route is top - cannot be the same as the points edge
//				},
//				{  // first route is bottom edge
//					null,		// second route is right edge- - already used above
//					null,		// second route is bottom - cannot be the same as the first edge
//					"UPLEFT",	// second route is left edge
//					null		// second route is top - cannot be the same as the points edge
//				},
//				{},  // first route is left edge - cannot be - need a second route
//				{}   // first route is top edge - cannot be - need a second route
//			}
//	};
	static private final String[] LockPosition = {
			"UPRIGHT",		// points are on the right edge
			"LOWRIGHT",		// points are on the bottom edge
			"LOWLEFT",		// points are on the left edge
			"UPLEFT"		// points are on the top edge
	};
	
	/**
	 * the ctor
	 * @param edge is the edge of the grid where the points are formed
	 */
	public LockFrill(int edge) {
		super(UniCode.CHECKMARK, toFrillLoc(edge), new FontFinder(FontList.UNLOCK_ICON));
	}
	
	/**
	 * determines where the lock symbol should be displayed in the Grid,
	 * based on the tracks that form the SwitchPoints
	 * @param edge is the edge of the Grid where the points meet
	 * @return a FrillLoc locating the lock symbol
	 */
	static private final FrillLoc toFrillLoc(int edge) {
		return FrillLoc.newFrillLoc(LockPosition[edge]);
	}
	
	/**
	 * displays the lock symbol
	 * @param value is true to display the lock and false to turn it off
	 */
	public void showLock(boolean value) {
		MyLabel.setVisible(value);
	}
}
/* @(#)LabelFrill.java */