/*
 * Name: InitRefresh.java
 * 
 * What:
 *  InitRefresh is a Singleton object with a boolean value.  It is set
 *  to true if CATS should try to read the JMRI values of IOSpecs being
 *  monitored when CATS starts up.  One initialization problem is that
 *  if JMRI starts before CATS, then JMRI never tells CATS the value of
 *  IOSPec until it changes.  If CATS starts first (or the layout is
 *  powered up after CATS), then JMRI sees values change from "UNKNOWN"
 *  to something and tells CATS.  This flag should be set when
 *  JMRI has completed its initialization before CATS is up.  This is
 *  a flag because CATS can have bad values if it initializes first.
 *   
 * Special Considerations:
 */
package cats.gui;

/**
 *  InitRefresh is a Singleton object with a boolean value.  It is set
 *  to true if CATS should try to read the JMRI values of IOSpecs being
 *  monitored when CATS starts up.  One initialization problem is that
 *  if JMRI starts before CATS, then JMRI never tells CATS the value of
 *  IOSPec until it changes.  If CATS starts first (or the layout is
 *  powered up after CATS), then JMRI sees values change from "UNKNOWN"
 *  to something and tells CATS.  This flag should be set when
 *  JMRI has completed its initialization before CATS is up.  This is
 *  a flag because CATS can have bad values if it initializes first.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class InitRefresh 
extends BooleanGui {
	  /**
	   * is the tag for identifying a Compression Object in the XMl file.
	   */
	  static final String XMLTag = "REFRESHTAG";
	  
	  /**
	   * is the label on the JCheckBoxMenuItem
	   */
	  static final String Label = "Initial Refresh";

	  /**
	   * is the singleton.
	   */
	  public static InitRefresh TheRefreshType;

	  /**
	   * constructs the factory.
	   */
	  public InitRefresh() {
	    super(Label, XMLTag, false);
	    TheRefreshType = this;
	  }
	}
	/* @(#)InitRefresh.java */

