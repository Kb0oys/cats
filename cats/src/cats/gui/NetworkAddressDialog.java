/**
 * Name: NetworkAddressDialog
 * 
 * What:
 * This class creates a JPanel for entering information on the network connection from
 * CATS to a JMRI instance running operations.
 *   
 * Special Considerations:
 */
package cats.gui;

import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cats.gui.jCustom.AcceptDialog;
import cats.network.AbstractClient;
import cats.network.OperationsClient;

/**
 * This class creates a JPanel for entering information on the network connection from
 * CATS to a JMRI instance running operations.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2011</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class NetworkAddressDialog extends JPanel{

	/**
	 * constants for this class
	 */
	/**
	 * the text tag for the name of the computer running Operations.
	 */
	private static String IP_HOSTNAME = " Host Name:";

	/**
	 * the text tag for the IP address of the computer running Operations.
	 */
	private static String IP_ADDRESS = " IP Address:";

	/**
	 * the text tag for the IP port address on the Operations computer.
	 */
	private static String SERVER_PORT = " port:";

	/**
	 * the text tag for the connection box
	 */
	private static String CONNECTION = "Connect to ";

	/**
	 * the text tag for the refresh box
	 */
	private static String REFRESH = "Refresh from ";

	/**
	 * is the JTextField for the host name of the computer running
	 * CATS.
	 */
	JTextField HostName;

	/**
	 * is the JTextField for IP address of the computer running CATS.
	 */
	JTextField IPAddress;

	/**
	 * is the JTextField for the telnet port on the computer running CATS.
	 */
	JTextField ServerPort;

	/**
	 * is the JCheckBox for the connection state
	 */
	JCheckBox ConnectBox;

	/**
	 * is the JCheckBox for requesting refresh from server
	 */
	JCheckBox RefreshBox;

	/**
	 * is the constructor.
	 * 
	 * @param addr contains the network information
	 * @param client is a String identifying the client
	 */
	private NetworkAddressDialog(final AbstractClient addr, final String client) {
		JPanel name = new JPanel(new FlowLayout());
		JPanel address = new JPanel(new FlowLayout());
		JPanel rPort = new JPanel(new FlowLayout());
		String str;

		name.add(new JLabel(client + IP_HOSTNAME));
		str = addr.getHostName();
		if (str == null) {
			HostName = new JTextField(15);
		}
		else {
			HostName = new JTextField(str,
					(str.length() < 15) ? 15 : str.length());
		}
		name.add(HostName);

		address.add(new JLabel(client + IP_ADDRESS));
		str = addr.getIPAddress();
		if (str == null) {
			IPAddress = new JTextField(15);
		}
		else {
			IPAddress = new JTextField(str, 15);
		}
		address.add(IPAddress);

		rPort.add(new JLabel(client + SERVER_PORT));
		str = addr.getServerPort();
		if (str == null) {
			ServerPort = new JTextField(5);
		}
		else
		{
			ServerPort = new JTextField(str, 5);
		}
		rPort.add(ServerPort);

		ConnectBox = new JCheckBox(CONNECTION + client +":");
		ConnectBox.setSelected(addr.getConnected());

		RefreshBox = new JCheckBox(REFRESH);
		RefreshBox.setSelected(false);

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(name);
		add(address);
		add(rPort);
		add(ConnectBox);
		if (addr.getConnected()) {
			add(RefreshBox);
		}
	}

	/**
	 * creates the JDialog, displays it, and tells the user what closing
	 * button was pushed.
	 *
	 * @param addr contains the network address information.  It must not be null.
	 * @param title is the name of the service
	 *
	 * @return true if the user pushed the Accept button or false if the
	 * user pushed the Cancel button.
	 */
	static public boolean select(AbstractClient addr, String title) {
		NetworkAddressDialog nad = new NetworkAddressDialog(addr, title);
		while (AcceptDialog.select(nad, title + " IP Address")) {
			if ((nad.IPAddress.getText() != null) && !nad.IPAddress.getText().trim().equals("") &&
					(OperationsClient.toInetAddress(nad.IPAddress.getText()) == null)) {
				JOptionPane.showMessageDialog(nad, "Improperly formatted IP address",
						"", JOptionPane.ERROR_MESSAGE);
			}
			else if (addr.isValidServerPort(nad.ServerPort.getText()) == OperationsClient.INVALID_PORT) {
				JOptionPane.showMessageDialog(nad, "Illegal IP port number on " + title + " computer",
						"", JOptionPane.ERROR_MESSAGE);
			}
			else {
				return addr.setAll(nad.HostName.getText(),
						nad.IPAddress.getText(),
						nad.ServerPort.getText(),
						nad.ConnectBox.isSelected(),
						nad.RefreshBox.isSelected());
			}
		}
		return false;
	}    

}
/* @(#)NetworkAddressDialog.java */