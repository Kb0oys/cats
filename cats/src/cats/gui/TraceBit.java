/* Name: TraceBit.java
 *
 * What:
 * This file contains the TraceBit class.  Each instantiation has
 * a bit in the global Details BitSet and a JMenuItem for changing the state of the bit.
 *
 * Special Considerations:
 */
package cats.gui;

import java.awt.event.ActionEvent;

import cats.apps.Crandic;
import cats.common.DebugBits;

/**
 * This file contains the TraceBit class.  Each instantiation has
 * a bit in the global Details BitSet and a JMenuItem for changing the state of the bit.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2021, 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class TraceBit extends TraceFlag {

	/**
	 * the index of the bit
	 */
	private final int BitIndex;

	/**
	 * constructs the flag as a JCheckBox.
	 * @param label is the menu label for the flag in the Trace pulldown
	 * @param tag is the XML tag.  This is not used, but is needed by
	 * the BooleanGui parent type.
	 * @param bit is the index of the bit Details.
	 */
	public TraceBit(final String label, final String tag, final int bit) {
		super(label, tag);
		BitIndex = bit;
		setFlagValue(Crandic.Details.get(bit));
	}

	@Override
	/**
	 * is the ActionListener for setting or clearing the flag.
	 */
	public void actionPerformed(ActionEvent ae) {
		if (getState()) {
			Crandic.Details.set(BitIndex);
		}
		else {
			Crandic.Details.clear(BitIndex);
		}
	}
	
	static public void init() {
		TraceFactory.Tracer.createTraceItem("Vital Logic Locks", "VITALLOGICLOCKBIT", DebugBits.VITALLOGICLOCKBIT);
		TraceFactory.Tracer.createTraceItem("GUI Locks", "GUILOCKBIT", DebugBits.GUILOCKBIT);
		TraceFactory.Tracer.createTraceItem("GUI Route Life Cycle", "GUIROUTEBIT", DebugBits.GUIROUTEBIT);
		TraceFactory.Tracer.createTraceItem("Code Line Traffic", "CODELINEBIT", DebugBits.CODELINEBIT);
		TraceFactory.Tracer.createTraceItem("Signal Icons and Masts", "SIGNALBIT", DebugBits.SIGNALBIT);
		TraceFactory.Tracer.createTraceItem("Traffic Sticks", "TRAFFICSTICKBIT", DebugBits.TRAFFICSTICKBIT);
		TraceFactory.Tracer.createTraceItem("CTC Routes", "CTCCREATIONBIT", DebugBits.CTCCREATIONBIT);
		TraceFactory.Tracer.createTraceItem("Route DAG", "ROUTENODEBIT", DebugBits.ROUTENODEBIT);
		TraceFactory.Tracer.createTraceItem("Route Stacking", "ROUTESTACKING", DebugBits.ROUTESTACKING);
		TraceFactory.Tracer.createTraceItem("Automated Testing", "AUTOMATEDTESTSBIT", DebugBits.AUTOMATEDTESTSBIT);
		TraceFactory.Tracer.createTraceItem("Mouse Selection", "MOUSEBIT", DebugBits.MOUSEBIT);
		TraceFactory.Tracer.createTraceItem("Track Color", "TRACKCOLORBIT", DebugBits.TRACKCOLORBIT);
		TraceFactory.Tracer.createTraceItem("XML Reader", "XMLBIT", DebugBits.XMLBIT);
		TraceFactory.Tracer.createTraceItem("Managed Queue", "QUEUEBIT", DebugBits.QUEUEBIT);
		TraceFactory.Tracer.createTraceItem("TrainStat Processing", "TRAINSTATBIT", DebugBits.TRAINSTATBIT);
		TraceFactory.Tracer.createTraceItem("Transponding Messages", "TRANSPONDINGBIT", DebugBits.TRANSPONDINGBIT);
		TraceFactory.Tracer.createTraceItem("Debug Flag", "DEBUGFLAG", DebugBits.DEBUGFLAG);
	}
}
/* @(#)TraceBit.java */
