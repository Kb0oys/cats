/*
 * Name: RouteManagerModel.java
 *
 * What:
 *  This class provides a JTableModel for editing the list of stacked routes.
 *  It provides the following operations on the stack:
 *  <ol>
 *  <li>delete a route</li>
 *  <li>rearrange the order of routes</li>
 *  </ol>
 *  <p>
 *  it does not support editing route names or adding a route.
 *  </p>
 *  <p>
 *  It builds on the AbstractManagerTableModel
 *  </p>
 *  
 */
package cats.gui.jCustom;


import java.util.Vector;

import javax.swing.table.AbstractTableModel;

/**
 *  This class provides a JTableModel for editing the list of stacked routes.
 *  It provides the following operations on the stack:
 *  <ol>
 *  <li>delete a route</li>
 *  <li>rearrange the order of routes</li>
 *  </ol>
 *  <p>
 *  it does not support editing route names or adding a route.
 *  </p>
 *  <p>
 *  It builds on the AbstractManagerTableModel
 *  </p>
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class RouteManagerModel extends AbstractTableModel {

	/**
	 * are the column headings - only one is needed, but the super-class requires an array
	 */
	final private String ColNames[] = {
			"Route"
	};

	/**
	 * stacked route anchors
	 */
	private Vector Contents;
	
	/**
	 * list of stacked routes to delete
	 */
	private Vector DeletedRoutes;
	
	/**
	 * the ctor
	 * @param routeList is the Vector of stacked route anchors
	 * @param deleteList is a Vector, initially empty, which will be filled by the JTable as they
	 * are deleted
	 */
	public RouteManagerModel(Vector routeList, Vector<?> deleteList) {
		Contents = routeList;
		DeletedRoutes = deleteList;
	}

	/**
	 * implements interchanging a row on the JTable with the row below it.
	 * Contents[Contents.size()] is the first index outside of the table (due t0 zero indexing)
	 * Contents[Contents.size() - 1] is the index of the last (bottom) row
	 * @param topRow is the index into the table contents of the top (lower numbered) row
	 */
	public void interchangeRows(final int topRow) {
		Object temp;
		if ((topRow >= 0) && (topRow < (Contents.size() - 1))) {
			temp = Contents.elementAt(topRow + 1);
			Contents.set(topRow + 1, Contents.elementAt(topRow));
			Contents.set(topRow, temp);
		}
	}
	
	/**
	 * deletes the route at a particular row
	 * @param row is the index of the route anchor
	 */
	public void deleteRoute(final int row) {
		DeletedRoutes.add(Contents.elementAt(row));
		Contents.remove(row);
		fireTableRowsDeleted(row, row);
	}
	
	@Override
	public Object getValueAt(int row, int col) {
		if ((col != 0) || (row >= Contents.size())){
			return null;
		}
		return Contents.elementAt(row).toString();
	}

	@Override
	public void setValueAt(Object val, int row, int col) {
		// should never be executed
	}

	@Override
	public int getColumnCount() {
		return 1;
	}

	@Override
	public int getRowCount() {
		return Contents.size();
	}
	
	@Override
	public boolean isCellEditable(int row, int col) {
		return false;
	}
	
	@Override
	public String getColumnName(final int col) {
		if (col < ColNames.length) {
			return ColNames[col];
		}
		else {
			return "";
		}
	}
}
/* @(#)RouteManagerModel.java */
