/**
 * Name: CATSOptionPane
 * 
 * What:
 * This is a Java Swing object which functions much like a
 * JOptionPane.  It is distinguished from a JOptionPane by
 * supporting a Java Point parameter to locate at a desired
 * place on the panel, which JOptionPane does not do.
 */
package cats.gui.jCustom;

import java.awt.Point;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import cats.gui.WindowFinder;

/**
 * This is a Java Swing object which functions much like a
 * JOptionPane.  It is distinguished from a JOptionPane by
 * supporting a Java Point parameter to locate at a desired
 * place on the panel, which JOptionPane does not do.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
*/
public class CATSOptionPane {
	
	/**
	 * a static method that expands the functionality of JOtionPane.showXXX y adding a position parameter
	 * so that the pane can be placed somewhere other than the center of the panel.  It prints a message
	 * and waits to be closed.  It accepts no selection input.
	 * @param cursorLoc is where to place the pane.  Typically, at the cursor.
	 * @param message is a single message to be printed.
	 * @param type is the JOption type of the pane
	 * @param title is the title on the dialog
	 */
	static public void showMessage(final Point cursorLoc, final String message, final int type, final String title) {
		JOptionPane pane = new JOptionPane(message, type);
	    Point offset = WindowFinder.getLocation().getLocationOnScreen();
	    int x = Math.max(40, cursorLoc.x + offset.x - pane.getSize().width);
	    int y = Math.max(40, cursorLoc.y + offset.y - pane.getSize().height);
	    JDialog dialog = pane.createDialog(title);
	    dialog.setLocation(x, y);
	    dialog.setVisible(true);
	    dialog.dispose();
	}
	
	/**
	 * the showMessage method tailored for an Error JOptionPane
	 * @param cursorLoc is where to place the pane.  Typically, at the cursor.
	 * @param message is a single message to be printed.
	 * @param title is the title on the dialog
	 */
	static public void showErrorMessage(final Point cursorLoc, final String message, final String title) {
		showMessage(cursorLoc, message, JOptionPane.ERROR_MESSAGE, title);
	}
	
	/**
	 * the showMessage method tailored for an Information JOptionPane
	 * @param cursorLoc is where to place the pane.  Typically, at the cursor.
	 * @param message is a single message to be printed.
	 * @param title is the title on the dialog
	 */
	static public void showInformationMessage(final Point cursorLoc, final String message, final String title) {
		showMessage(cursorLoc, message, JOptionPane.INFORMATION_MESSAGE, title);
	}
	
	/**
	 * the showOptionDialog adjusted for allowing screen positioning
	 * @param cursorLoc where on the screen to position the dialog
	 * @param message the text body of the dialog
	 * @param type the JOptionPane dialog type
	 * @param title the title of the dialog
	 * @param values an array of option buttons
	 * @param defaultValue the default button
	 * @return CLOSED_OPTION (if nothing selected) or the index of the option selected
	 */
	static public int showDialogMessage(final Point cursorLoc, final String message, final int type, final String title, final Object values[], final Object defaultValue) {
		JOptionPane pane = new JOptionPane(message, type, JOptionPane.YES_NO_OPTION, null, values, defaultValue);
		Point offset = WindowFinder.getLocation().getLocationOnScreen();
		int x = Math.max(40, cursorLoc.x + offset.x - pane.getSize().width);
		int y = Math.max(40, cursorLoc.y + offset.y - pane.getSize().height);
		JDialog dialog = pane.createDialog(title);
		dialog.setLocation(x, y);
		dialog.setVisible(true);
		Object selectedValue = pane.getValue();
		if (selectedValue == null) {
			return JOptionPane.CLOSED_OPTION;
		}
		for (int counter = 0, maxCounter = values.length;
				counter < maxCounter; counter++) {
			if (values[counter].equals(selectedValue)) {
				return counter;
			}
		}
		return JOptionPane.CLOSED_OPTION;
	}
	
	/**
	 * tailors showDialogMessage to have options "RETRY" and "CANCEL"
	 * @param cursorLoc is the coordinates of the mouse cursor on the screen
	 * @param message is the body of the text on the dialog
	 * @param title is the title of the dialog
	 * @return either CLOSED_OPTION or OK_OPTION
	 */
	static public int showDialog_RETRY_OR_CANCEL(final Point cursorLoc, final String message, final String title) {
		Object options[] = {"Retry", "Cancel"};
		int result = showDialogMessage(cursorLoc, message, JOptionPane.OK_CANCEL_OPTION, title, options, options[0]);
		if (result == 0) {
			result = JOptionPane.OK_OPTION;
		}
		else {
			result = JOptionPane.CANCEL_OPTION;
		}
		return result;
	}
}
