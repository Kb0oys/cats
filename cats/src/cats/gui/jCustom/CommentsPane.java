/* Name: CommentsPane.java
 *
 * What:
 *  This Dialog is for requesting a test case description
 */
package cats.gui.jCustom;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *  This Dialog is for requesting a test case description
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2011</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version 1.0
 */
public class CommentsPane extends JPanel {
  /**
   * is the field for accepting the description.
   */
  private JTextArea CommentsField = new JTextArea(2, 40);

  /**
   * the ctor
   */
  public CommentsPane() {
    add(new JLabel("Enter test case description"));
    add(CommentsField);
  }
  
  /**
   * is the factory to construct a CommentsDialog.
   * 
   *@param testNumber is the number of the test associated with the comment
   * @return the comments if anything was entered, or null
   */
  public static String getComments(int testNumber) {
    CommentsPane pane = new CommentsPane();
    String comments;
    if (AcceptDialog.select(pane, "comments for test case " + String.valueOf(testNumber))) {
      comments = pane.CommentsField.getText().trim();
      if ((comments != null) && (comments.length() != 0)) {
        return comments;
      }
    }
    return null;
  }
}
/* @(#)CommentsPane.java */