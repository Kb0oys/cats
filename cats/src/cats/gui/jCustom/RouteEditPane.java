/* Name: RouteEditPane.java
 *
 * What:
 *  This file contains the class definition for a RouteEditPane object.
 *  A RouteEditPane constructs a JTable within a ScrollPane for
 *  adding, deleting, and editing the stacked route list (maintained by the
 *  RouteListManager).
 *  <p>
 *  It is tightly coupled to a definition of AbstractManagerTableModel.
 */
package cats.gui.jCustom;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *  This file contains the class definition for a RouteEditPane object.
 *  A RouteEditPane constructs a JTable within a ScrollPane for
 *  adding, deleting, and editing the stacked route list (maintained by the
 *  RouteListManager).
 *  <p>
 *  It is tightly coupled to a definition of AbstractManagerTableModel.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2022</p>
 * @author Rodney Black
 * @version $Revision$
 */
public class RouteEditPane extends JPanel {

	/**
	 * is the Button for interchanging the selected row with the one above it.
	 */
	protected JButton UpButton = new JButton("Move Route Up");

	/**
	 * is the Button for interchanging the selected row with the one below it.
	 */
	protected JButton DownButton = new JButton("Move Route Down");

	/**
	 * is the Button for removing a Route Anchor at the selection point.
	 */
	private final JButton DelButton = new JButton("Delete Route");

	/**
	 * is the JTable that holds the Routes
	 */
	private JTable Table = new JTable();

	/**
	 * is the TableModel that controls the editing.
	 */
	private RouteManagerModel EditModel;
	/**
	 * the index of the selected row in the table
	 */
	private int SelectedRow = -1;

	/**
	 * constructs the RouteEditPane.
	 * @param model is the TableModel controlling the editing.
	 */
	public RouteEditPane(RouteManagerModel model) {
		JPanel movers = new JPanel();
		EditModel = model;
		Table.setModel(model);
		Table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		Table.getSelectionModel().addListSelectionListener(new
				ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				//Ignore extra messages.
				if (!e.getValueIsAdjusting()) {
					ListSelectionModel lsm = (ListSelectionModel)e.getSource();
					if (lsm.isSelectionEmpty()) {
						UpButton.setEnabled(false);
						DownButton.setEnabled(false);
						DelButton.setEnabled(false);
					}
					else {
						SelectedRow = lsm.getMinSelectionIndex();
						UpButton.setEnabled(SelectedRow > 0);
						DownButton.setEnabled(SelectedRow < (EditModel.getRowCount() - 1));
						DelButton.setEnabled(true);
					}
				}
			}
		});
		UpButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				Table.editingCanceled(null);
				--SelectedRow;
				EditModel.interchangeRows(SelectedRow);
				Table.setRowSelectionInterval(SelectedRow, SelectedRow);
				UpButton.setEnabled(SelectedRow > 0);
			}
		});
		DownButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
					Table.editingCanceled(null);
					EditModel.interchangeRows(SelectedRow);
					++SelectedRow;
					Table.setRowSelectionInterval(SelectedRow, SelectedRow);
					DownButton.setEnabled(SelectedRow < (EditModel.getRowCount() - 1));
			}
		});
		DelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				Table.editingCanceled(null);
				EditModel.deleteRoute(SelectedRow);
				UpButton.setEnabled(false);
				DownButton.setEnabled(false);
				DelButton.setEnabled(false);
				validate();
				repaint();
			}
		});
		movers.setLayout(new BoxLayout(movers, BoxLayout.Y_AXIS));
		UpButton.setEnabled(false);
		DownButton.setEnabled(false);
		DelButton.setEnabled(false);
		movers.add(DelButton);
		movers.add(UpButton);
		movers.add(DownButton);
		JScrollPane jsp = new JScrollPane(Table);
		add(jsp);
		add(movers);
	}

	/**
	 * is the factory to construct an edit table.
	 *
	 * @param model is the TableModel used to direct the editing.
	 * @param location is where on the screen the table should be printed
	 * @return true if the user asked that the Store be updated.
	 */
	public static boolean editList(RouteManagerModel model, final Point location) {
		return AcceptDialog.select(new RouteEditPane(model), "Edit Routes", location);
	}   
}
/* @(#)RouteEditPane.java */
