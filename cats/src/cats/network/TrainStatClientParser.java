/* Name: TrainStatClientParser.java
 *
 * What:
 *   This file parses messages received from a CATS TrainStat server and updates its
 *   view of the trains, crew, and jobs, accordingly.  It is based on the equivalent
 *   parser in the TrainStat application.
 *  
 *  Special Considerations:
 */
package cats.network;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import cats.apps.Crandic;
import cats.common.Constants;
import cats.common.DebugBits;
import cats.common.VersionList;
import cats.crew.Callboard;
import cats.jobs.JobStore;
import cats.layout.items.Block;
import cats.layout.replay.ReplayHandler;
import cats.layout.store.AbstractStore;
import cats.layout.store.FieldPair;
import cats.layout.store.GenericRecord;
import cats.trains.Train;
import cats.trains.TrainStore;



/**
 *   This file parses messages received from a CATS TrainStat server and updates its
 *   view of the trains, crew, and jobs, accordingly.  It is based on the equivalent
 *   parser in the TrainStat application.
 * 
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class TrainStatClientParser {
      
    /**
     * Parses a status update, received from CATS.
     * @param line is the status update
     */
    static public void parseLine(String line){
        String tag;
        String result = null;
        StringTokenizer tokens;
        tokens = new StringTokenizer(line, Constants.FS_STRING);
        if (Crandic.Details.get(DebugBits.TRAINSTATBIT)) {
        	System.out.println("TrainStatClientParser: " + line);
        }
        TrainStatClient.instance().setUpdateBroadcast(false);
        try {
            tag = tokens.nextToken();
            if (Constants.ASSIGN_TAG.equals(tag)) {
//                result = processAssign(tokens);
            }
            else if (Constants.TRAINSTAT.equals(tag)) {
                result = processCreate(tokens);
            }
            else if (Constants.FINISH_TAG.equals(tag)) {
                result = processFinish();
            }
            else if (Constants.MOVE_TAG.equals(tag)) {
                result = processMove(tokens);
            }
            else if (Constants.RERUN_TAG.equals(tag)) {
                result = processRerun(tokens);
            }
            else if (Constants.TERMINATED_TAG.equals(tag)) {
                result = processTerminated(tokens, Constants.TERMINATED_STATE);
            }
            else if (Constants.TIEDDOWN_TAG.equals(tag)) {
                result = processTerminated(tokens, Constants.TIED_DOWN_STATE);
            }
            else if (Constants.OOS_TAG.equals(tag)) {
                result = processOOS(tokens);
            }
            else if (Constants.TNT_TAG.equals(tag)) {
                result = processTNT(tokens);
            }
            else if (Constants.ADD_TO_STORE.equals(tag)) {
                result = processAddRecord(tokens);
            }
            else if (Constants.REMOVE_FROM_STORE.equals(tag)) {
                result = processDeleteRecord(tokens);
            }
            else if (Constants.CHANGE_STORE.equals(tag)) {
                result = processChangeRecord(tokens);
            }
            else if (Constants.DISCONNECT.equals(line)) {
            	result = "TrainStat server disconnected socket";
            }
            else {
                result = "Unrecognized TrainStat message <" + line + ">";
            }
            if (result != null) {
                System.out.println(tag + " " + result);
                log.info(tag + " " + result);
            }
        }
        catch (NoSuchElementException nsee) {
            System.out.println("Short TrainStat message <" + line + ">");
        }
        TrainStatClient.instance().setUpdateBroadcast(true);
    }
    
    /**
     * Assign a crew to a train.
     * @param tokens is a tokenized String, containing the crew
     * being assigned and the train being assigned to.
     * @return null if everything went well or an error message.
     */
//    private String processAssign(StringTokenizer tokens) {
//      MsgStructs.Crew_Assignment_Struct s = structs.new Crew_Assignment_Struct();
//      
//      try {
//        // extract the timestamp
//        s.timeStamp = tokens.nextToken();
//        
//        // extract the crew name
//        s.crewName = fetchQuotedString(tokens);
//        
//        // extract the assignment
//        s.running = tokens.nextToken();
//        
//        if (Constants.REASSIGNED.equals(s.running)) {
//            return null;
//        }
//        else if (Constants.ASSIGNMENT.equals(s.running)) { // job assignment
//            return null;
//        }
//        else if (Constants.RUNNING.equals(s.running)){  // train assignment      
//          if (!Constants.NOTHING.equals(s.job)) {
//              s.job = fetchQuotedString(tokens);
//          }
//          TrainStore.instance().changeCrew(s);
//        }
//        else {
//            return I18N.getString(Constants.UNRECOGNIZED_TAG);
//        }
//      }
//      catch (NoSuchElementException nsee) {
//        return I18N.getString(Constants.SHORT_MSG);
//      }
//      return null;
//    }

    /**
     * The first record.  It contains the TrainStat's format identifier.
     * @param tokens is a tokenized String, containing the version of the messages.
     * @return null if everything went well or an error message if
     * the version is bad.
     */
    static private String processCreate(StringTokenizer tokens) {
        String s;
        try {
            s = tokens.nextToken();
            if (!Constants.VERSION_TAG.equals(s)) {
                return "Unrecognized TrainStat tag:" + s;
            }
            if (VersionList.STATUS_VERSION != Integer.parseInt((s = tokens.nextToken()))) {
                return "Possibly incompatible version of TrainStat server: " + s;
            }
        }
        catch (NoSuchElementException nsee) {
            return "Short TrainStat creation message";
        }
        return null;
    }

    /**
     * All log records have been read.  Cleanup.
     * @return always null.
     */
    static private String processFinish() {
        return null;
    }

    /**
     * move a train.
     * @param tokens is a tokenized String, containing the train,
     * where it is coming from and where it is going to.
     * @return null if everything went well or an error message.
     */
    static private String processMove(StringTokenizer tokens) {
        String s = null;
        String tStamp = ReplayHandler.getTimeStamp(tokens);
        String fromStation;
        if (tStamp == null) {
          return "A move record is too short";
        }
        
        try {
          // Extract the Train
          s = ReplayHandler.fetchQuotedString(tokens);
          Train train = TrainStore.TrainKeeper.getTrain(s);
          if (train == null) {
            return "Unknown train: " + s;
          }
          
          // extract departure location
          fromStation = ReplayHandler.fetchQuotedString(tokens);
          
          // extract "to"
          s = tokens.nextToken();
          if (!"to".equals(s)) {
            return "Badly formatted movement record";
          }
          
          // extract arrival location.  This is the Station or coordinates.
          s = ReplayHandler.fetchQuotedString(tokens);
          train.moveTrain(fromStation, s);
        }
        catch (NoSuchElementException nsee) {
          return "Truncated move record.";
        }
        return null;    
    }

    /**
     * Rerun a train.
     * @param tokens is a tokenized String, containing the train
     * being rerun.
     * @return null if everything went well or an error message.
     */
    static private String processRerun(StringTokenizer tokens) {
        String train;
        Train t;
        String tStamp = ReplayHandler.getTimeStamp(tokens);
        if (tStamp == null) {
          return "A train rerun record is too short";
        }
        try {
          // Extract the Train
          train = ReplayHandler.fetchQuotedString(tokens);
          t = TrainStore.TrainKeeper.getTrain(train);
          if (t == null) {
            t = TrainStore.TrainKeeper.createTrain(train);
          }
          t.rerun();
        }
        catch (NoSuchElementException nsee) {
          return "Truncated rerun record.";
        }
        return null;
    }

    /**
     * Terminate a train.
     * @param tokens is a tokenized String, containing the train
     * being terminated.
     * @params status is either "TERMINATED" or "TIED_DOWN_STATE"
     * @return null if everything went well or an error message.
     */
    static private String processTerminated(StringTokenizer tokens, String status) {
        String train;
        Train t;
        String tStamp = ReplayHandler.getTimeStamp(tokens);
        if (tStamp == null) {
          return "A train terminated record is too short.";
        }
        try {
          // Extract the Train
          train = ReplayHandler.fetchQuotedString(tokens);
          t = TrainStore.TrainKeeper.getTrain(train);
          if (t != null) {
            if (Constants.TERMINATED_TAG.equals(status)) {
              t.remove();
            }
            else {
              t.tieDown();
            }
          }
        }
        catch (NoSuchElementException nsee) {
          return "Truncated terminated record.";
        }
        return null;
    }

    /**
     * Process Track Authority being added or removed from a block.
     * @param tokens is a tokenized String, containing the 
     * name of the block and its sense.
     * @return null if everything went well or an error message.
     */
    static private String processTNT(StringTokenizer tokens) {
        String blockName;
        String sense;
        Block b;
        boolean add = false;
        String tStamp = ReplayHandler.getTimeStamp(tokens);
        if (tStamp == null) {
          return "A Track Authority record is too short.";
        }
        try {
          // Extract the on or off name
          sense = tokens.nextToken();
          if (Constants.ADD_MARKER.equals(sense)){
            add = true;
          }
          else if (Constants.REMOVE_MARKER.equals(sense)){
            add = false;
          }
          else {
            return "Unknown Track Authority action " + sense;
          }
          blockName = ReplayHandler.fetchQuotedString(tokens);
          if ((b = Block.findBlock(blockName)) != null) {
            if (add) {
            	b.setTNT();
            }
            else {
            	b.clearTNT();
            }
          }
        }
        catch (NoSuchElementException nsee) {
          return "Truncated Track Authority record.";
        }
        return null;
    }

    /**
     * Mark or remove a block as OOS.
     * @param tokens is a tokenized String, containing the name of the
     * block taken out of service or put back in service.
     * @return null if everything went well or an error message.
     */
    static private String processOOS(StringTokenizer tokens) {
        String blockName;
        String sense;
        Block b;
        boolean add = false;
        String tStamp = ReplayHandler.getTimeStamp(tokens);
        if (tStamp == null) {
          return "An OOS record is too short.";
        }
        try {
          // Extract the on or off name
          sense = tokens.nextToken();
          if (Constants.ADD_MARKER.equals(sense)){
            add = true;
          }
          else if (Constants.REMOVE_MARKER.equals(sense)){
            add = false;
          }
          else {
            return "Unknown OOS action " + sense;
          }
          blockName = ReplayHandler.fetchQuotedString(tokens);
          ReplayHandler.fetchQuotedString(tokens);
          if ((b = Block.findBlock(blockName)) != null) {
            if (add) {
            	b.setOOS();
            }
            else {
            	b.clearOOS();
            }
          }
        }
        catch (NoSuchElementException nsee) {
          return "Truncated OOS record.";
        }
        return null;    
    }

    /**
     * process a record add message.  For the record descriptions, the
     * FIELD_KEY descriptor is not added because it is used in CATS
     * to describe the class of object each record represents.  In the
     * data records, the FIELD_KEY pair is removed because it is the
     * link back to the object represented.
     * @param tokens are the named fields in the record to be added
     * @return null if everything went well or an error message.
     */
    static private String processAddRecord(StringTokenizer tokens) {
    	String storeName;
    	GenericRecord updates = new GenericRecord();
    	try {
    		// Extract the timestamp
    		tokens.nextToken();

    		// Extract the store
    		storeName = tokens.nextToken();
    		ReplayHandler.toFieldPairs(tokens, updates);
    		if (TrainStore.TrainKeeper.getDataID().equals(storeName)) {
    			TrainStore.TrainKeeper.changeRecord(updates);            }
    		else if (TrainStore.TrainKeeper.getFieldID().equals(storeName)) {
    			TrainStore.TrainKeeper.changeFieldInfo(updates);
    		}
    		else if (JobStore.JobsKeeper.getDataID().equals(storeName)) {
    			JobStore.JobsKeeper.changeRecord(updates);
    		}
    		else if (JobStore.JobsKeeper.getFieldID().equals(storeName)) {
    			JobStore.JobsKeeper.changeFieldInfo(updates);          
    		}
    		else if (Callboard.Crews.getDataID().equals(storeName)) {
    			Callboard.Crews.changeRecord(updates);
    		}
    		else if (Callboard.Crews.getFieldID().equals(storeName)) {
    			Callboard.Crews.changeFieldInfo(updates);
    		}
    		else {
    			return "Unregocnized store: " + storeName;
    		}
    	}
    	catch (NoSuchElementException nsee) {
    		return "Short add message";
    	}
    	return null;
    }
    
    /**
     * process a record removal message
     * @param tokens are the names of the fields
     * @return null if everything went well or an error message
     */
    static private String processDeleteRecord(StringTokenizer tokens) {
        String storeName;
        GenericRecord del = new GenericRecord();
        AbstractStore store = null;
        FieldPair element;
        try {
          // Extract the timestamp
          tokens.nextToken();
          
          // Extract the store
          storeName = tokens.nextToken();
          ReplayHandler.toFieldPairs(tokens, del);
          if (TrainStore.TrainKeeper.getDataID().equals(storeName)) {
            store = TrainStore.TrainKeeper;
          }
          else if (TrainStore.TrainKeeper.getFieldID().equals(storeName)) {
            return "Unexpected change to Train presentation format";
          }
          else if (JobStore.JobsKeeper.getDataID().equals(storeName)) {
            store = JobStore.JobsKeeper;
          }
          else if (JobStore.JobsKeeper.getFieldID().equals(storeName)) {
            return "Unexpected change to Jobs presentation format";          
          }
          else if (Callboard.Crews.getDataID().equals(storeName)) {
            store = Callboard.Crews;
          }
          else if (Callboard.Crews.getFieldID().equals(storeName)) {
            return "Unexpected change to Crew presentation format";
          }
          else {
        	  return "unknown data store";
          }
          element = del.elementAt(0);
          del = store.findRecord(element.FieldTag, (String) element.FieldValue);
          if (null != del) {
            store.delRecord(del);
          }
        }
        catch (NoSuchElementException nsee) {
          return "short delete record";
        }
        return null;
    }
    
    /**
     * process a record change message.  A record change message is
     * formatted as:
     * <ol>
     * <li>"Changed" keyword, flushed by caller.
     * <li>object type from CATS XML file ("Train")
     * <li>record identifier
     * <li>field identifier
     * <li>field value
     * </ol> 
     * @param tokens are the names of the fields
     * @return null if everything went well or an error message
     */
    static private String processChangeRecord(StringTokenizer tokens) {
        String storeName;
        GenericRecord updates = new GenericRecord();
        AbstractStore store = null;
        try {
            // Extract the timestamp
            tokens.nextToken();
            
            // Extract the store
            storeName = tokens.nextToken();
            ReplayHandler.toFieldPairs(tokens, updates);
            if (TrainStore.TrainKeeper.getDataID().equals(storeName)) {
              store = TrainStore.TrainKeeper;
            }
            else if (JobStore.JobsKeeper.getDataID().equals(storeName)) {
              store = JobStore.JobsKeeper;
            }
            else if (Callboard.Crews.getDataID().equals(storeName)) {
              store = Callboard.Crews;
            }
            else {
              return "unknown store: " + storeName;
            }
            store.changeRecord(updates);
        }
        catch (NoSuchElementException nsee) {
            return "short change record";
        }
        return null;
    }

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
        TrainStatClientParser.class.getName());

}
/* @(#)TrainStatClientParser.java */
