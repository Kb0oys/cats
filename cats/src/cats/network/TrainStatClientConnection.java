/* Name: TrainStatClientConnection.java
 *
 * What:
 *   This file manages the network connection between a CATS TrainStat application
 *   and CATS TrainstatClient.
 *  
 *  Special Considerations:
 */
package cats.network;

import java.net.Socket;

/**
 *   This file manages the network connection between a CATS TrainStat application
 *   and CATS TrainstatClient.
 * 
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2020, 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class TrainStatClientConnection  extends AbstractConnection {

	/**
	 * is the ctor
	 * @param s is the socket forming the connection to the remote
	 * end
	 */
	TrainStatClientConnection(Socket s) {
		super(s, "TrainStatClient");
	}

static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TrainStatClientConnection.class.getName());

@Override
protected void processMessage(String msg) {
	TrainStatClientParser.parseLine(msg);	
}

}
/* @(#)TrainStatClientConnection.java */
