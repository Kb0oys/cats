/* Name: OperationsClient.java
 *
 * What:
 *   This file defines the information needed for establishing a network 
 *   connection between this computer running CATS and
 *   another computer running CATS, acting as the TrainStat servr.
 *  
 *  Special Considerations:
 */
package cats.network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import cats.layout.store.AbstractStoreWatcher;

/**
 *   This file defines the information needed for establishing a network 
 *   connection between this computer running CATS and
 *   another computer running CATS, acting as the TrainStat server.
 * 
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class TrainStatClient extends AbstractClient {

	/**
	 * the XML tag for the TrainStatClient configured in designer
	 */
	private static final String TRAINSTAT_TAG = "TSS_CLIENT";

	/**
	 * is the Singleton that contains the TrainStat Server IP information
	 */
	private static TrainStatClient TrainStatNetwork;

	/**
	 * is the transmitter half of the connection
	 */
	private static NetworkStoreWatcher Watcher;

	/**
	 * true to transmit updates to the TrainStat Server.
	 * False to block updates due to echo.
	 */
	private static boolean AllowUpdates;

	/**
	 * the ctor
	 */
	public TrainStatClient() {
		super(TRAINSTAT_TAG, NetworkProtocol.DEFAULT_PORT);
		Watcher = new NetworkStoreWatcher();
	}

	/**
	 * is the single point for accessing the Network Address.
	 * 
	 * @return the Network Address.
	 */
	public static TrainStatClient instance() {
		if (TrainStatNetwork == null) {
			TrainStatNetwork = new TrainStatClient();
		}
		return TrainStatNetwork;
	}

	@Override
	protected void disableNetwork() {
		if (isEnabled()) {
			Watcher.unregister();
			MyConnection.interrupt();
			MyConnection = null;
		}
	}

	@Override
	protected void establishConnection(InetAddress address, int port) {
		Socket sock;
		try {
			sock = new Socket(address, port);
			MyConnection = new TrainStatClientConnection(sock);
			MyConnection.addListener(this);
			AllowUpdates = true;
			Watcher.register();
		}
		catch (IOException e) {
			MyConnection = null;
			log.warn("Failed to establish network connection to " +
					address + ": " + e.getMessage());
		}
	}

	@Override
	protected void establishConnection(String address, int port) {
		Socket sock;
		try {
			sock = new Socket(address, port);
			MyConnection = new TrainStatClientConnection(sock);
			MyConnection.addListener(this);
			AllowUpdates = true;
			Watcher.register();
		}
		catch (IOException e) {
			MyConnection = null;
			log.warn("Failed to establish netwrok connection to " +
					address + ": " + e.getMessage());
		}
	}
	
	/**
	 * enables/disables broadcasting changes received from a TrainStat Server.
	 * When enabled, all changes in any of the CATS data is broadcast back
	 * to the TrainStat server.  When disabled, nothing is sent back to the
	 * TrainStat server.  The reason to disable broadcasts is to prevent an echo storm
	 * where the TrainStat server changes something, the client makes the changes,
	 * broadcasts them back to the server, which makes the same changes, broadcasts
	 * the changes, etc.  This storm normally does not happen because the change code
	 * changes on ly things that are different.  But, if something changes quickly
	 * (e.g. the crew is replaced on a train and the former crew is assigned to
	 * a different train), the crew and train could be changed twice in
	 * quick succession.
	 * @param enable is normally true; however, it is set to false when
	 * processing a TrainStat message.
	 */
	public void setUpdateBroadcast(final boolean enable) {
		AllowUpdates = enable;
	}

	/**
	 * The next class is private to TrainStatServer.  It is the bridge
	 * between the Stores and the network.  It registers itself with the
	 * Stores and forwards all messages to the TrainStat clients.
	 */
	private class NetworkStoreWatcher extends AbstractStoreWatcher {

		/**
		 * This method takes any messages constructed by the AbstractStoreWatcher
		 * and passes them to the network distributor.
		 * 
		 * @param msg is a message from an AbstractStore
		 */
		protected void forward(String msg) {
			if (AllowUpdates) {
				transmitMessage(msg);
			}
		}
	}

}
/* @(#)TrainStatClient.java */
