/* Name: OperationsClient.java
 *
 * What:
 *   This file defines the information needed for establishing a network 
 *   connection between this computer running CATS and
 *   the computer running a service.  They can be the same computer,
 *   in which case, the local host IP address can be used.  The service
 *   could be Operations or TrainStat.
 *  
 *  Special Considerations:
 */
package cats.network;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.StringTokenizer;

import cats.jmri.OperationsTrains;
import cats.layout.xml.XMLEleFactory;
import cats.layout.xml.XMLEleObject;
import cats.layout.xml.XMLReader;

/**
 *   This file defines the information needed for establishing a network 
 *   connection between this computer running CATS and
 *   the computer running a service.  They can be the same computer,
 *   in which case, the local host IP address can be used.  The service
 *   could be Operations or TrainStat.
 * 
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public abstract class AbstractClient implements XMLEleObject, Cloneable,
ConnectionListener {

	/**
	 * is the tag for identifying a Client in the XML file.
	 * Each client needs one of these.
	 */
	final String XML_TAG;

	/**
	 * is the XML attribute tag on the hostname
	 */
	static final String HOST_TAG = "HOSTNAME";

	/**
	 * is the XML attribute tag on the IP address
	 */
	static final String IP_TAG = "IPADDRESS";

	/**
	 * is the XML attribute tag on the Remote port
	 */
	static final String REMOTE_TAG = "REMOTE_PORT";

	/**
	 * is the XML attribute tag on the Local port
	 */
	static final String LOCAL_TAG = "LOCAL_PORT";

	/**
	 * is the XML attribute tag on connect at start up
	 */
	static final String CONNECT_TAG = "CONNECT";

	/**
	 * the default port to use on the Server computer.  If the port value
	 * is blank in the XML and the GUI, then this value is used.  There should
	 * be one of these for each client.
	 */
	private int REMOTE_PORT;

	/**
	 * flags an invalid TCP/IP port number
	 */
	public static final int INVALID_PORT = -1;

	/**
	 * is the default value for the server address - the local host.
	 * If no host name has been specified, this is the IP address of last resort.
	 */
	private final String LOCALHOST = "127.0.0.1";

	/**
	 * is the message handler that receives updates from the server.  It must be static
	 * because it's value carries over from one incarnation of this class to
	 * the next.
	 */
	protected AbstractConnection MyConnection;

	/**
	 * The internal data being configured
	 */
	/**
	 * the IP address.  The default value is blank, meaning, use the localhost.
	 */
	private String IPAddress = "";

	/**
	 * the hostname.  The default value is blank, meaning, use the localhost.
	 */
	private String Hostname = "";

	/**
	 * the port to use on the Operations computer.  The default is defined above.
	 */
	private String RemotePort = "";

	/**
	 * a boolean indicating the user's desire to connect to Operations or not.  This
	 * does not reflect the status of the actual connection.  That is contained
	 * in the value of MyConnection.
	 */
	private boolean Connected = false;

	/**
	 * the ctor
	 * @param tag is the tag in the XML file
	 * @param defaultPort is the default port number on the server
	 */
	public AbstractClient(final String tag, final int defaultPort) {
		Connected = false;
		XML_TAG = tag;
		REMOTE_PORT = defaultPort;
		MyConnection = null;
		XMLReader.registerFactory(tag, new ServerConnectionFactory(this));
	}

	/**
	 * retrieves the network name of the server
	 * @return the network name of the computer that this client
	 * will attempt to connect to.  It may be null.
	 */
	public String getHostName() {
		if (Hostname != null) {
			return new String(Hostname);
		}
		return "";
	}

	/**
	 * retrieves the IP address of the server
	 * @return the IP address of the server.
	 * Null may be returned.
	 */
	public String getIPAddress() {
		if (IPAddress != null) {
			return new String(IPAddress);
		}
		return "";
	}

	/**
	 * retrieves the port number on the server
	 * @return the port number on the server that will be
	 *  contacted for setting up a session.  It may be null.
	 */
	public String getServerPort() {
		if (RemotePort != null) {
			return new String(RemotePort);
		}
		return "";
	}

	/**
	 * retrieves the numeric value of the string containing
	 * the port on the server.  If null, then
	 * the default port.
	 * @return the numeric value of the port number
	 */
	private int ServerPort() {
		if ((RemotePort == null) || RemotePort.equals("")) {
			return REMOTE_PORT;
		}
		return Integer.parseInt(RemotePort);
	}

	/**
	 * is called to retrieve the current value of the Connected flag.  The flag controls
	 * if the connection should be established when CATS starts or not.
	 * @return the Connected flag.
	 */
	public boolean getConnected() {
		return Connected;
	}

	/**
	 * converts an IP address in String format to one in
	 * InetAddress format.  If the String is null, blank,
	 * or not dotted hex, the local address is returned.  
	 * @param address is the String form of the address
	 * @return the InetAddress form of an IP address.
	 */
	private InetAddress convertAddress(String address) {
		InetAddress a = null;
		if ((address == null) || (address.trim().equals("")) ||
				((a = toInetAddress(address)) == null)) {
			address = new String();
			try {
				a = InetAddress.getByName(LOCALHOST);
			} catch (UnknownHostException e) {
				log.fatal("Ill-formed IP address");
				e.printStackTrace();
			}
		}
		return a;
	}

	/**
	 * converts a String to an InetAddress.  If the String
	 * is not formatted for IPv4, null is returned.
	 * @param addr is a String representation of an IP address
	 * @return the IP address of the String or null if it is invalid
	 */
	public static InetAddress toInetAddress(String addr) {
		InetAddress ia = null;
		byte [] result = new byte[4];
		int partial;
		int octet = 0;
		StringTokenizer tokens = new StringTokenizer(addr, ".");
		while (tokens.hasMoreTokens()) {
			try {
				partial = Integer.parseInt(tokens.nextToken());
			}
			catch (NumberFormatException nfe) {
				return null;
			}
			if ((partial < 0) || (partial > 255)) {
				return null;
			}
			if (octet > 3) {
				return null;
			}
			result[octet++] = (byte) partial;
		}
		if (octet != 4) {
			return null;
		}
		try {
			ia = InetAddress.getByAddress(result);
		} catch (UnknownHostException e) {
			return null;
		}
		return ia;
	}
	
    /**
     * checks a string for being a valid port, using
     * isValidPort().  If the String is null or empty.
     * the default port is returned; otherwise,
     * the results of isValidPort are returned.
     * @param port is the String representation of an IP port
     * @return numeric representation of the port if the String
     * is valid or the default port if not.
     */
    public int isValidServerPort(String port) {
        if ((port == null) || (port.trim().equals(""))) {
            return REMOTE_PORT;
        }
        return isValidPort(port);
    }
    
	/**
	 * checks a String for being a valid IP port.  To be
	 * valid, the String must be all numeric characters,
	 * greater than or equal to 0, and less than 65536.
	 * @param port is the String being verified.
	 * @return If the String is valid, then the port
	 * number; otherwise, -1.
	 */
	public static int isValidPort(String port) {
		int result;
		try {
			result = Integer.parseInt(port);
		}
		catch (NumberFormatException nfe) {
			return INVALID_PORT;
		}
		if ((result >=0) && (result < 65536)) {
			return result;
		}
		return INVALID_PORT;
	}

	/**
	 * is called to set all the local data.
	 * @param hostname is the new name of the server.
	 * @param ipaddress is the new IP address of the server.
	 * @param serverPort is the port to contact on the server.
	 * @param enabled is true to set up the connection when CATS starts up.
	 * @param refresh is true to force CATS to refresh the status from the server.
	 * 
	 * @return true if any field changed; false if no field changed
	 */
	public boolean setAll(String hostname, String ipaddress, String serverPort,
			boolean enabled, boolean refresh) {
		boolean changed = false;
		hostname = hostname.trim();
		if (!Hostname.equals(hostname)) {
			changed = true;
			Hostname = hostname;
		}
		if (!IPAddress.equals(ipaddress) && 
				(ipaddress.equals("") || (toInetAddress(ipaddress) != null))) {
			changed = true;
			IPAddress = ipaddress;
		}
		if (!RemotePort.equals(serverPort) && 
				(serverPort.equals("") ||(isValidPort(serverPort) != INVALID_PORT))) {
			changed = true;
			RemotePort = serverPort;
		}
		if (Connected != enabled) {
			changed = true;
			Connected = enabled;
		}
		if (refresh && OperationsTrains.instance().isEnabled()) {
			OperationsTrains.instance().refreshTrains();
		}
		return changed;
	}

	/**
	 * this method attempts to make the real connection the
	 * same as the administered connection.
	 */
	public void reconcileConnection() {
		if (Connected) {
			enableNetwork();
		}
		else {
			disableNetwork();
		}
	}

	/**
	 * if the network is not running, the parameters (address
	 * and port) are passed to the network handler and it is
	 * enabled.
	 */
	private void enableNetwork() {
		disableNetwork();
		if (getIPAddress() == null) {
			establishConnection(getHostName(), ServerPort());
		}
		else {
			establishConnection(convertAddress(IPAddress), ServerPort());
		}
	}

	/**
	 * if the network is running, it is shutdown.
	 */
	protected abstract void disableNetwork();

	/**
	 * establishes a connection to a CATS server with a particular
	 * IP address.  The caller should be sure that a connection is not
	 * up before calling this method.
	 * @param address is the IP address
	 * @param port is the port on the CATS server to connect to.
	 */
	protected abstract void establishConnection(InetAddress address, int port);

	/**
	 * establishes a connection to a CATS server with a particular
	 * Internet name.  The caller should be sure that a connection is not
	 * up before calling this method.
	 * @param address is the name of the CATS server
	 * @param port is the port on the CATS server to connect to.
	 */
	protected abstract void establishConnection(String address, int port);

	/**
	 * is invoked to inquire about the status of the network
	 * connection
	 * @return true if the connection is up and false if it is not.
	 */
	public boolean isEnabled() {
		return MyConnection != null;
	}

	/**
	 * handles the call back when the server drops the connection.
	 * @param connection is the JMRI connection
	 */
	public void connectionDropped(AbstractConnection connection) {
		MyConnection = null;
	}

	/**
	 * sends a message to CATS.  If there is no connection, the message is dropped.
	 * @param msg is the fully constructed message to send
	 * @return true if the connection has been established; false if there is no connection.
	 */
	public boolean transmitMessage(String msg) {
		if (isEnabled()) {
			MyConnection.sendMessage(msg);
		}
		return false;
	}

	/*
	 * is the method through which the object receives the text field.
	 *
	 * @param eleValue is the Text for the Element's value.
	 *
	 * @return if the value is acceptable, then null; otherwise, an error
	 * string.
	 */
	public String setValue(String eleValue) {
		return new String("A " + XML_TAG + " cannot contain a text field ("
				+ eleValue + ").");
	}

	/*
	 * is the method through which the object receives embedded Objects.
	 *
	 * @param objName is the name of the embedded object
	 * @param objValue is the value of the embedded object
	 *
	 * @return null if the Object is acceptable or an error String
	 * if it is not.
	 */
	public String setObject(String objName, Object objValue) {
		return new String("A " + XML_TAG + " cannot contain an Element ("
				+ objName + ").");
	}

	/*
	 * returns the XML Element tag for the XMLEleObject.
	 *
	 * @return the name by which XMLReader knows the XMLEleObject (the
	 * Element tag).
	 */
	public String getTag() {
		return new String(XML_TAG);
	}

	/*
	 * tells the XMLEleObject that no more setValue or setObject calls will
	 * be made; thus, it can do any error checking that it needs.
	 *
	 * @return null, if it has received everything it needs or an error
	 * string if something isn't correct.
	 */
	public String doneXML() {
		return null;
	}

	/**
	 * is a Class known only to the AbstractClient class for creating a AbstractClient
	 * from an XML document.
	 */
	class ServerConnectionFactory
	implements XMLEleFactory {

		/**
		 * the prototype
		 */
		private AbstractClient Client;

		/**
		 * the fields in AbstractClient
		 */
		private String Hostname;
		private String IPAddress;
		private String RemotePort;
		private boolean Connected;

		/**
		 * the ctor
		 * @param client is the prototype
		 */
		public ServerConnectionFactory(AbstractClient client) {
			Client = client;
		}

		/*
		 * tells the factory that an XMLEleObject is to be created.  Thus,
		 * its contents can be set from the information in an XML Element
		 * description.
		 */
		public void newElement() {
			Hostname = Client.getHostName();
			IPAddress = Client.getIPAddress();
			RemotePort = Client.getServerPort();
			Connected = Client.getConnected();
		}

		/*
		 * gives the factory an initialization value for the created XMLEleObject.
		 *
		 * @param tag is the name of the attribute.
		 * @param value is it value.
		 *
		 * @return null if the tag:value are accepted; otherwise, an error
		 * string.
		 */
		public String addAttribute(String tag, String value) {
			String resultMsg = null;
			if (tag.equals(HOST_TAG)) {
				Hostname = value;
			}
			else if (tag.equals(IP_TAG)) {
				IPAddress = value;
			}
			else if (tag.equals(REMOTE_TAG)) {
				if (isValidPort(value) != INVALID_PORT) {
					RemotePort = value;
				}
			}
			else if (tag.equals(LOCAL_TAG)) {  // kept for backwards compatibility
			}
			else if (tag.equals(CONNECT_TAG)) {
				Connected = true;
			}
			else {
				resultMsg = new String("A " + Client.XML_TAG +
						" XML Element cannot have a " + tag +
						" attribute.");
			}
			return resultMsg;
		}

		/*
		 * tells the factory that the attributes have been seen; therefore,
		 * return the XMLEleObject created.
		 *
		 * @return the newly created XMLEleObject or null (if there was a problem
		 * in creating it).
		 */
		public XMLEleObject getObject() {
			if (Client.setAll(Hostname, IPAddress, RemotePort, 
					Connected, true)) {
				Client.reconcileConnection();
			}
			return Client;
		}
	}

	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AbstractClient.class.getName());

}
/* @(#)AbstractClient.java */

