/* Name: TestMaker.java
 *
 * What:
 *   This is a recorder of events and results.  Thus, it captures tests and their consequences.
 *   The resulting recording can be run through TestRunner to provide automated testing.
 *   <p>
 *   By necessity, the design of record/replay is convoluted and my be hard to understand.
 *   <ul>
 *   <li>
 *   The test cases and expected results are stored in an XML element.  The first content
 *   of the element is the trigger, which describes what action generated the state change.
 *   The rest of the contents is the state of the layout prior to the trigger.  This is
 *   non-intuitive, but necessary because the state change due to an action need not happen
 *   immediately, but may be queued for later execution.  This means that it is very
 *   difficult to determine when all the repercussions have settled down.  Thus, the record
 *   strategy is to see the trigger, capture the current state, record both in XML, and
 *   finally, execute the trigger.
 *   </li>
 *   <li>
 *   The TestRunner (replay) reads recorded test cases from the XML file.  It extracts the
 *   trigger from each XML element and simulates that on the CATS core logic. The TestMaker
 *   code is utilized during playback (testing) to regenerate the initial test (as an
 *   XML element).  The regeneration is passed to the TestRunner and compared against
 *   the recorded XML.  In addition, the test file can be updated, as the regeneration
 *   can be recorded.
 *   </li>
 *   <li>
 *   the TestRunner mechanism runs in its own Java thread.  This has been difficult
 *   to tune because some of CATS runs on the Swing dispatcher thread.  Through experimentation,
 *   I tried passing the replay triggers back through the RREvent queue, but that generated
 *   some Swing invalid state faults.  I found the best thing was to enter the CATS event
 *   handling on the TestRunner thread.  Since the test case is essentially rerun on the
 *   TestRuuner thread, the testing code need not have any sophisticated synchronization -
 *   it is simply: execute trigger, TestMaker picks up at the trigger, TestMaker regenerates
 *   the test case, TestMaker return the regenerated test case back to TestRunner for analysis.
 *   However, life is not that simple.  To exercise the user input, the state is captured
 *   just before the trigger is executed.  This may be after the user has touched keys,
 *   selected menu options, etc.  A good test should also check the user interaction.  Thus,
 *   the replay picks up before the trigger condition.  Since the replay freezes at the user
 *   input, the runner thread needs to wait for the user input without also freezing the
 *   input capture thread.  Thus, the runner communicates with the maker through a synchronized
 *   producer/consumer variable where the runner simulates the trigger and waits for an
 *   update from the maker.  This also allows the recording to contain a comment based
 *   no what the user did to get to the trigger point.  The comment is displayed during
 *   playback so that the tester knows what buttons to push and reactions to look for.
 *   </li>
 *   </ul>
 */

package cats.automatedTests;

import java.awt.Component;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.EnumMap;
import java.util.Enumeration;
import java.util.Iterator;

import javax.swing.JOptionPane;

import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;

import cats.common.VersionList;
import cats.gui.Screen;
import cats.gui.jCustom.CommentsPane;
import cats.layout.items.Block;
import cats.layout.items.Section;

/**
 *   This is a recorder of events and results.  Thus, it captures tests and their consequences.
 *   The resulting recording can be run through TestRunner to provide automated testing.
 *   <p>
 *   By necessity, the design of record/replay is convoluted and my be hard to understand.
 *   <ul>
 *   <li>
 *   The test cases and expected results are stored in an XML element.  The first content
 *   of the element is the trigger, which describes what action generated the state change.
 *   The rest of the contents is the state of the layout prior to the trigger.  This is
 *   non-intuitive, but necessary because the state change due to an action need not happen
 *   immediately, but may be queued for later execution.  This means that it is very
 *   difficult to determine when all the repercussions have settled down.  Thus, the record
 *   strategy is to see the trigger, capture the current state, record both in XML, and
 *   finally, execute the trigger.
 *   </li>
 *   <li>
 *   The TestRunner (replay) reads recorded test cases from the XML file.  It extracts the
 *   trigger from each XML element and simulates that on the CATS core logic. The TestMaker
 *   code is utilized during playback (testing) to regenerate the initial test (as an
 *   XML element).  The regeneration is passed to the TestRunner and compared against
 *   the recorded XML.  In addition, the test file can be updated, as the regeneration
 *   can be recorded.
 *   </li>
 *   <li>
 *   the TestRunner mechanism runs in its own Java thread.  This has been difficult
 *   to tune because some of CATS runs on the Swing dispatcher thread.  Through experimentation,
 *   I tried passing the replay triggers back through the RREvent queue, but that generated
 *   some Swing invalid state faults.  I found the best thing was to enter the CATS event
 *   handling on the TestRunner thread.  Since the test case is essentially rerun on the
 *   TestRuuner thread, the testing code need not have any sophisticated synchronization -
 *   it is simply: execute trigger, TestMaker picks up at the trigger, TestMaker regenerates
 *   the test case, TestMaker return the regenerated test case back to TestRunner for analysis.
 *   However, life is not that simple.  To exercise the user input, the state is captured
 *   just before the trigger is executed.  This may be after the user has touched keys,
 *   selected menu options, etc.  A good test should also check the user interaction.  Thus,
 *   the replay picks up before the trigger condition.  Since the replay freezes at the user
 *   input, the runner thread needs to wait for the user input without also freezing the
 *   input capture thread.  Thus, the runner communicates with the maker through a synchronized
 *   producer/consumer variable where the runner simulates the trigger and waits for an
 *   update from the maker.  This also allows the recording to contain a comment based
 *   no what the user did to get to the trigger point.  The comment is displayed during
 *   playback so that the tester knows what buttons to push and reactions to look for.
 *   </li>
 *   </ul>
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2011, 2014, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class TestMaker {
  
  /**
   * is the tag for the XML document.
   */
  static final String DocumentTag = "DOCUMENT";
  
  /**
   * is the first element, which sets the test environment
   */
  static final String ParametersTag = "PARAMETERS";

  /**
   * is the XML tag for identifying the Version of CATS used in
   * creating the XML file.  It identifies the format.
   */
  static final String VersionTag = "VERSION";

  /**
   * is the XML tag for identifying the test detail level.  The use of
   * this is really confusing and should be thought out.
   */
  static final String DetailLevel = "DETAILS";

  /**
   * is the value of DetailLevel for many details
   */
  static final String High = "HIGH";
  
  /**
   * is the value of DetailLevel for basic details
   */
  static final String Basic = "BASIC";
  
  /**
   * is the XML tag for identifying the name of the layout file
   */
  static final String LayoutTag = "LAYOUT";

  /**
   * is the XML tag for the test case Element
   */
  static final String TestCase = "TESTCASE";
  
  /**
   * is the XML attribute for the test case number
   */
  static final String CaseNumber = "NUMBER";

  /**
   * is the XML attribute tag for the test case description
   */
  static final String Comments = "COMMENTS";

  /**
   * is the XML element tag for identifying the trigger element
   */
  static final String Trigger = "TRIGGER";
  
  /**
   * is the trigger XML tag to simply wait for user input
   */
  static public final String WAIT = "WAIT";
  
  /**
   * is the trigger XML tag to delay before being replayed
   */
  static public final String DELAY = "DELAY";
 
  
  /**
   * the levels of information stored in test cases
   */
  enum TEST_LEVELS {
    NO_TESTING,
//    NO_COMMENTS,
    SIMPLE_TESTS,
    DETAILED_TESTS
  };

  /**
   * map each test level to a String
   */
  private final EnumMap<TEST_LEVELS, String> LevelNames = new EnumMap<TEST_LEVELS, String>(TEST_LEVELS.class);
  
  /**
   * the name of the output file
   */
  private final String OutName;
  
  /**
   * is the Root of the XML tree
   */
  private Element RecordingRoot = new Element(ParametersTag);

  /**
   * is the file stream for the test log
   */
  private PrintStream OutStream = null;

  /**
   * the number of the last test being executed
   */
  private static int TestNumber = 0;
 
//  /**
//   * is the current test level
//   */
//  private static TEST_LEVELS CurrentLevel = TEST_LEVELS.NO_TESTING;

  /**
   * is the recording level.
   */
  private static TEST_LEVELS RecordingLevel = TEST_LEVELS.NO_TESTING;

  /**
   * are the comments for the current test case, as read in by the TestRunner
   */
  private static String RecordedComments;
  
  /**
   * is the Singleton
   */
  private static TestMaker TheMaker;
  
  /**
   * is the latest test case, encoded as XML.  This is also the communications
   * interface in a producer/consumer relationship.  TestMaker produces test cases
   * and TestRunner consumes them.  The producer does not wait for consumption.
   * It just generates test cases at the user's whim.  Thus, it is possible
   * for test cases to be lost, but this should not happen.  The consumer also
   * stimulates the layout, which control how often test cases are generated.
   * The consumer triggers execution of a test case only after it has completely
   * consumed the last test case.
   */
  public static Element LatestTestCase;
  
//  /**
//   * is the synchronization point.  The producer (TestMaker) sets it to true
//   * when there are test results to consume and the consumer (TestRunner) sets
//   * it to false after it has consumed the test case.
//   */
//  public static Boolean TestResults = false;
  
  /**
   * a two argument ctor
   * @param outFile is where to store the automated test
   * @param details is the amount of details to include in the XML file
   */
  TestMaker(String outFile, TEST_LEVELS details) {
    OutName = outFile;
    LevelNames.put(TEST_LEVELS.NO_TESTING, "NO TESTING");
//    LevelNames.put(TEST_LEVELS.NO_COMMENTS, "NO COMMENTS");
    LevelNames.put(TEST_LEVELS.SIMPLE_TESTS, Basic);
    LevelNames.put(TEST_LEVELS.DETAILED_TESTS, High);
    RecordingRoot.setAttribute(VersionTag, VersionList.CATS_VERSION);
    RecordingRoot.setAttribute(DetailLevel, LevelNames.get(details));
    if (outFile != null) {
    	if (backupTests(new File(OutName))) {
    		try {
    			OutStream = new PrintStream(new FileOutputStream(OutName));
    			OutStream.print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    			OutStream.print("<" + DocumentTag + ">");
//    			CurrentLevel = RecordingLevel = details;
    			RecordingLevel = details;
    		}
    		catch (FileNotFoundException except) {
    			JOptionPane.showMessageDialog( (Component)null,
    					"Error creating " + OutName + " check the backup",
    					"File Error",
    					JOptionPane.ERROR_MESSAGE);
    			return;
    		}
    	}
    	else {
    		return;
    	}
    }
  }
  
  /**
   * creates the Singleton TestMaker
   * @param fileName is the name to store the test cases in
   */
  public static final void createMaker(String fileName) {
    if (TheMaker == null) {
      TheMaker = new TestMaker(fileName, TEST_LEVELS.SIMPLE_TESTS);
    }
  }

  /**
   * creates the Singleton TestMaker for a TestRunner.  This is invoked to update
   * a test sequence.  The old sequence is run by TestRunner.  The triggers are
   * recorded by TestRunner.  The states are recorded by TestRuuner; however, when
   * TestRunner finishes, the user can continue to add more testcases.
   * <p>
   * The test level is initialized to the level from the original test cases so
   * that it is recorded properly in the XML file.  It is then changed to NO_TESTING
   * so that the layout events do not trigger any recording.
   * @param fileName is the name of the file to store the renewal
   * @param details is the level of details to record
   */
  public static final void renewMaker(String fileName, TEST_LEVELS details) {
    if (TheMaker == null) {
      TheMaker = new TestMaker(fileName, details);
//      CurrentLevel = RecordingLevel = TEST_LEVELS.NO_TESTING;
//      RecordingLevel = TEST_LEVELS.NO_TESTING;
      RecordingLevel = details;
    }    
  }
  
  /**
   * sets the name of the layout file to use
   * @param fileName is the name of the layout file
   */
  public static void setLayout(String fileName) {
    if (TheMaker != null) {
      TheMaker.RecordingRoot.setAttribute(LayoutTag, fileName);
      TheMaker.saveTest(TheMaker.RecordingRoot);
    }
  }

  /**
   * Dynamically changes the level of state being recorded.  It is intended to
   * be invoked by TestRunner to change the level from NO_TESTING to the level
   * in the test sequence being executed, when the tests are complete.
   * <p>
   * WARNING: the level is set in the Document element, which has already been written
   * at this point.  Thus, detailLevel should be the same value.
   * @param detailLevel is the new control over what is recorded
   * @param caseNumber is the number of the next test case
   */
  public static void setDetails(TEST_LEVELS detailLevel, int caseNumber) {
//    CurrentLevel = RecordingLevel = detailLevel;
	RecordingLevel = detailLevel;
    TestNumber = caseNumber;
  }

  /**
   * sets the comments attribute.  This is set by theg TestRunner, using the comments
   * from the recorded test case
   * @param comments are the comments in the test case being executed
   */
  public static void setComments(String comments) {
	  RecordedComments = new String(comments);
  }
  
//  /**
//   * is invoked to inform the TestMaker that a processing for a trigger is about
//   * to begin.  This allows the TestMaker to ignore other triggers until the processing
//   * is complete.
//   * @return true if the TestMaker will start ignoring other trigger events; false, if there
//   * is no change in satte.
//   */
//  public static boolean triggerPreamble() {
//    if ((TheMaker != null) && (CurrentLevel != TEST_LEVELS.NO_TESTING)) {
//      CurrentLevel = TEST_LEVELS.NO_TESTING;
//      System.out.println("Preparing for test case " + String.valueOf(TestNumber));
//      return true;
//    }
//    return false;
//  }
 
//  /**
//   * is called to enable the trigger, if there is recording
//   */
//  public static void cancelTrigger() {
//    if ((TheMaker != null) && (RecordingLevel != TEST_LEVELS.NO_TESTING)) {
//      CurrentLevel = RecordingLevel;
//    }
//  }
  
  /**
   * is the meat of the class.  It is called whenever a state changing event
   * (test) happens.  The method starts a new Test XML element.  It records the
   * trigger for the state change, then polls the layout for state.  Because
   * of delays in processing events, CATS should call this method before executing
   * the event; thus, the state of CATS just prior to the event is captured.
   * @param trigger describes the test.
   * @param comment is displayed by TestRunner prior to replaying the test case
   */
  public static void recordTrigger(Element trigger, String comment) {
	  if (RecordingLevel != TEST_LEVELS.NO_TESTING) {
		  //      Element testCase = new Element(TestCase);
		  Element child;
		  String description;
		  //      synchronized(TestResults) {
		  LatestTestCase = new Element(TestCase);
		  //      testCase.setAttribute(CaseNumber, String.valueOf(++TestNumber));
		  LatestTestCase.setAttribute(CaseNumber, String.valueOf(++TestNumber));
		  if (comment == null) {
			  if (RecordedComments == null) {
				  if ((TheMaker.OutStream != null) && (description = CommentsPane.getComments(TestNumber)) != null) {
					  //        testCase.setAttribute(Comments, description);
					  LatestTestCase.setAttribute(Comments, description);
				  }
			  }
			  else {
				  LatestTestCase.setAttribute(Comments, RecordedComments);
			  }
		  }
		  else {
			  LatestTestCase.setAttribute(Comments, comment);
		  }
		  child = new Element(Trigger);
		  child.setContent(trigger);
		  //      testCase.setContent(child);
		  LatestTestCase.setContent(child);
		  //      try {
		  //        Thread.sleep(1000);
		  //      } catch (InterruptedException e) {
		  //        System.out.println("Caught interrupt exception while waiting to capture state");
		  //        e.printStackTrace();
		  //      }
		  //      captureStateChanges(RecordingLevel == TEST_LEVELS.SIMPLE_TESTS, testCase);
		  captureStateChanges(RecordingLevel == TEST_LEVELS.SIMPLE_TESTS, LatestTestCase);
		  //      TheMaker.saveTest(testCase);
		  TheMaker.saveTest(LatestTestCase);
		  //    	  TestResults = true;
		  //    	  TestResults.notify();
		  //      }
		  //      CurrentLevel = RecordingLevel;
		  TestResultsSync.SyncPoint.setResults(LatestTestCase);
	  }
  }

  /**
   * collects all the state changes due to an event into an XML Element
   * @param basic is true to capture only basic state changes; false to capture detailed
   * state changes
   * @param stateChanges is the XML Element that will contain the changes
   */
  static void captureStateChanges(boolean basic, Element stateChanges) {
    Element child;
    // get state information of each Section, passing in Basic or High
    for (Enumeration<Section> sec = Screen.DispatcherPanel.getSections(); sec.hasMoreElements(); ) {
      if ((child = sec.nextElement().getState(basic)) != null) {
        stateChanges.addContent(child);
      }
    }
    
    // get state information of each Block
    for (Iterator<Block> blk = Block.getBlockIterator(); blk.hasNext(); )
      if ((child = blk.next().getBlockState(basic)) != null) {
        stateChanges.addContent(child);
      }
  }

  /**
   * creates a test case which causes TestRunner to do nothing but wait
   * for user input. TestRunner waits on TestMaker to generate a new test
   * case.  Thus, the user can do things that do not record with impunity.
   * However, the next action that forces a recording should be the one
   * that matches the testcase being waited on.
   * @param comment is a prompt to tell the user what to do to end the wait
   */
  public static void waitTestCase(String comment) {
	  Element trigger = new Element(Trigger);
	  LatestTestCase = new Element(TestCase);
	  LatestTestCase.setAttribute(CaseNumber, String.valueOf(++TestNumber));
	  LatestTestCase.setAttribute(Comments, comment);
	  trigger.setContent(new Element(WAIT));
	  LatestTestCase.setContent(trigger);
	  captureStateChanges(RecordingLevel == TEST_LEVELS.SIMPLE_TESTS, LatestTestCase);
	  TheMaker.saveTest(LatestTestCase);
	  TestResultsSync.SyncPoint.setResults(LatestTestCase);
  }
  
  /**
   * takes an existing file and attempts to change its name
   * to the same name with ".bak" appended.  If that file
   * also exists, it is deleted.
   * 
   * @param existing is the file to be backed up.
   * @return true if the operation succeeded and false if it
   * failed.
   */
  public static boolean backupTests(File existing) {
      String newName = new String(existing.getAbsolutePath() + ".bak");
      File duplicate = new File(newName);
      if (existing.length() != 0) {
          if (duplicate.exists()) {
              if (!duplicate.isFile() || !duplicate.delete()) {
                  JOptionPane
                  .showMessageDialog( (Component)null,
                          newName
                          + " could not be deleted",
                          "Deletion Error",
                          JOptionPane.ERROR_MESSAGE);
                  return false;              
              }
          }
          // At this point, there is no old backup, so rename the
          // file.
          existing.renameTo(duplicate);
      }
      return true;
  }

  /**
   * dumps the formatted test case to the output file
   * @param testCase is either the test parameters or a complete test case
   * (trigger plus state changes)
   * @return true if the XML is written OK.
   */
  private boolean saveTest(Element testCase) {
    XMLOutputter fmt = new XMLOutputter();
    if (OutStream == null) {
    	return true;
    }
    fmt.setFormat(org.jdom2.output.Format.getPrettyFormat());
    try {
      fmt.output(testCase, OutStream);
      return true;
    }
    catch (java.io.FileNotFoundException nfExcept) {
      JOptionPane.showMessageDialog( (Component)null,
          "FileNotFound error writing file " + OutName + " " + nfExcept.getLocalizedMessage(),
          "File Error",
          JOptionPane.ERROR_MESSAGE);
    }
    catch (java.io.IOException ioExcept) {
      JOptionPane.showMessageDialog( (Component)null,
          "IO exception writing file " + OutName + " " + ioExcept.getLocalizedMessage(),
          "File Error",
          JOptionPane.ERROR_MESSAGE);
    }   
    return false;
  }

//  /**
//   * is an external entry into TestMaker, used by TestRunner, for updating the
//   * test results.
//   * @param testCase is the testcase being updated in XML format.  It includes the trigger,
//   * as well as the state changes due to the trigger.
//   * @return true if testcases are not being recorded or the write succeeded; false, if there
//   * is a write failure.
//   */
//  static boolean writeTestCase(Element testCase) {
//    if (TheMaker != null) {
//      return TheMaker.saveTest(testCase);
//    }
//    return true;
//  }
  
  /**
   * The test generation is done, so close things down.
   */
  public static void closeRecording() {
    if ((TheMaker != null) && (TheMaker.OutStream != null)){
      TheMaker.OutStream.print("</" + DocumentTag + ">");
      TheMaker.OutStream.close();
    }
  }

  /**
   * is a predicate for asking if triggers and state are being written to a file.
   * @return true if TestMaker is active and false if not
   */
  public static boolean isRecording() {
    return TheMaker != null;
  }
}
/* @(#)TestMaker.java */