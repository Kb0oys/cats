/* Name: AutoTestXML.java
 *
 * What:
 *  This parses test cases created by TestMaker and invokes methods
 *  in TestRunner to repeat those tests.
 */
package cats.automatedTests;

import java.io.File;
import java.io.IOException;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.jdom2.Element;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *  This parses test cases created by TestMaker and invokes methods
 *  in TestRunner to repeat those tests.
 *  <p>
 *  This class uses a SAX parser on the XML input, so that the test cases are
 *  processed as they are read in.  Two XML entities are read in:
 *  <ol>
 *  <li> the test parameters which specify the layout to use and the level of
 *  state changes recorded
 *  <li> a sequence of test cases.
 *  </ol>
 * <p>
 * Much of this design comes from examples in "Java and XML" by Brett McLaughlin, published
 * by O'Reilly
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2011</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version 1.0
 */
public class AutoTestXML extends DefaultHandler {
  /**
   * a String to hold SAX parser error messages
   */
 static String ErrorMsg;
 
 /**
  * line counter of the last line reported by the SAX parser
  */
 static private int LastLine = 0;  // last line located in document.
 
 /**
  * the COntent of an Element
  */
 private String TextVal = new String(); // The text value being constructed.
 
 /**
  * the cursor that walks through the XML document
  */
 private static Locator MyLocator;
 
 /**
  * the TestRunner that want the parsed XML document
  */
private static TestRunner MyReceiver;

/**
 * the XML tree
 */
private static Stack<Element> xmlTree = new Stack<Element>();

 /**
  * creates the parser and gets it rolling.
  *
  * @param file is the file containing the XML description.
  * @param runner is the TestRunner that wants the parsed document
  *
  * @return null if the file was parsed successfully; otherwise, return
  *         a String describing the error.
  */
 public static String parseDocument(File file, TestRunner runner) {
   AutoTestXML SAXEventHandler = new AutoTestXML();
   SAXParserFactory parserFactory = SAXParserFactory.newInstance();
   org.xml.sax.XMLReader xmlReader = null;
   MyReceiver = runner;
   try {
     SAXParser saxParser = parserFactory.newSAXParser();
     xmlReader = saxParser.getXMLReader();
   }
   catch (Exception e) {
     return e.getMessage();
   }

   xmlReader.setContentHandler(SAXEventHandler);
   xmlReader.setErrorHandler(SAXEventHandler);

   try {
     String path = file.getAbsolutePath();
     xmlReader.parse("file:" + path);
   }
   catch (SAXException saxE) {
     ErrorMsg = new String("At line " + LastLine + ": " + saxE.getMessage());
   }
   catch (IOException ioE) {
     ErrorMsg = new String("At line " + LastLine + ": " + ioE.getMessage());
   }
   return ErrorMsg;
 }
 /**
  * Provides a reference to <code>Locator</code>, which provides
  * information about where in a document callbacks occur.
  * <p>
  * @param locator <code>Locator</code> object is tied to callback
  * process.
  */
 public void setDocumentLocator(Locator locator) {
   MyLocator = locator;
 }

 /**
  * This indicates the start of a Document parse - this precedes
  * all callbacks in all SAX Handlers with the sole exception
  * of <code>{@link #setDocumentLocator(Locator)}</code>, which CATS
  * does not use
  */
 public void startDocument() {
   MyReceiver.startDocument();
 }

 /**
  * This indicates the end of a Document parse - this occurs after
  * all callbacks in all SAX Handlers.
  */
 public void endDocument() {
   MyReceiver.endDocument();
 }

 /**
  * <p>
  * This reports the occurrence of an actual XML Element.  It includes
  * the Element's attributes with the exception of XML vocabulary
  * specific attributes, such as
  * <code>xmlns:[namespace prefix]</code> and
  * <code>xsi:schemaLocation</code>
  * </p>
  * @param namespaceURI <code>String</code> namespace URI this Element
  * is associated with or an empty <code>String</code>
  * @param localName <code>String</code> name of Element (with no
  * namespace prefix, if one is present)
  * @param rawName <code>String</code> XML 1.0 version of Element name:
  * {namespaceprefic]:[localName]
  * @param attributes <code>Attributes</code> list for this Element
  */
 public void startElement(String namespaceURI, String localName,
     String rawName, Attributes attributes) {
   Element element = new Element(rawName);
   if (MyLocator != null) {
     LastLine = MyLocator.getLineNumber();
   }
   if (attributes != null) {
     int numberAttributes = attributes.getLength();
     for (int loopindex = 0; loopindex < numberAttributes; ++loopindex) {
       element.setAttribute(attributes.getQName(loopindex),
           attributes.getValue(loopindex));
     }
   }
   xmlTree.push(element);
 }
 
 /**
  * <p>
  * Indicates the end of an Element(<code>&lt;/[element name]&gt;</code> is reached.
  * Note that the parser does not distinguish between empty
  * elements and non-empty Elements, so this occurs uniformly.
  * </p>
  * 
  * @param namespaceURI <code>String</code> namespace URI this Element
  * is associated with or an empty <code>String</code>
  * @param localName <code>String</code> name of Element (with no
  * namespace prefix, if one is present)
  * @param rawName <code>String</code> XML 1.0 version of Element name:
  * {namespaceprefic]:[localName]
  */
 public void endElement(String namespaceURI, String localName,
 String rawName) {
   Element element = xmlTree.pop();
   Element parent;
   TextVal = TextVal.trim();
   if (TextVal.length() > 0) {
     element.addContent(TextVal);
     TextVal = new String();
   } 
   
   if (!xmlTree.empty()) {
     parent = xmlTree.peek();
     if (parent.getName().equals(TestMaker.DocumentTag)) {
       MyReceiver.endElement(element, LastLine);
     }
     else {
       xmlTree.peek().addContent(element);
     }
   }
 }

 /**
  * This reports character data (within an Element)
  * 
  * @param textChars <code>char[]</code> character array with character data
  * @param textStart <code>int</code> index into array where data starts
  * @param textLength <code>int</code> number of characters in the array
  */
 public void characters(char textChars[], int textStart, int textLength) {
   if (MyLocator != null) {
     LastLine = MyLocator.getLineNumber();
   }
   TextVal = TextVal.concat(new String(textChars, textStart, textLength));
 }
}
/* @(#)AutoTestXML.java */