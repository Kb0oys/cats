/* Name: TestResultsSync.java
 *
 * What:
 *   This class defines a communications mechanism between TestMaker and TestRunner for
 *   the latter to tell the former when the test case has been generated.
 *
 * Special Considerations:
 *   This class is application independent.
 */
package cats.automatedTests;

import org.jdom2.Element;

/**
 *   This class defines a communications mechanism between TestMaker and TestRunner for
 *   the latter to tell the former when the test case has been generated.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2014, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class TestResultsSync {
	
	/**
	 * the singleton
	 */
	public static final TestResultsSync SyncPoint = new TestResultsSync();
	
	/**
	 * the results of the last test
	 */
	private Element LastResults;
	
	/**
	 * the ctor
	 */
	public TestResultsSync() {
		
	}
	
	/**
	 * the producer method that is called by TestMaker
	 * @param results is the test case as an XML element
	 */
	public synchronized void setResults(Element results) {
		LastResults = results.clone();
		notify();
	}
	
	public synchronized Element getResults() {
		Element results;
		while (LastResults == null) {
			try {
				wait();
			} catch (InterruptedException e) {
				return null;
			}
		}
		results = LastResults;
		LastResults = null;
		return results;
	}
}
/* @(#)TestResultsSync.java */