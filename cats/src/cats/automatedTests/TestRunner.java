/* Name: TestRunner.java
 *
 * What:
 *   A class for re-executing an automated test.
 *   <p>
 *   This class re-executes a pre-recorded automated test and checks the results
 *   against the test.
 */
package cats.automatedTests;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.jdom2.Attribute;
import org.jdom2.Content;
import org.jdom2.Element;

import cats.apps.Crandic;
import cats.common.VersionList;
import cats.layout.Logger;
import cats.layout.items.Block;
import cats.layout.items.CatsReporter;
import cats.layout.items.IOSpec;
import cats.layout.items.Section;
import cats.trains.Train;

/**
 *   A class for re-executing an automated test.
 *   <p>
 *   This class re-executes a pre-recorded automated test and checks the results
 *   against the test.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2011, 2014, 2015, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class TestRunner {

  /**
   * the detail level of the test case
   */
  private TestMaker.TEST_LEVELS DetailLevel;
  
  /**
   * the name of the new file containing test cases.
   */
  private final String NewOut;

  /**
   * is a flag for requiring an Enter after printing the commentary on each test case
   * before executing it
   */
  private final boolean SingleStep;

  /**
   * is the case number of the last test case executed
   */
  private String CaseNumber;
  
  /**
   * constructs the automated test executor class
   * @param inFile is the name of the XML file holding the pre-recorded tests
   * @param outFile is the name of an XML file for holding updated versions of the automated tests
   * @param singleStep is true to single step (pause) through each test case
   */
  public TestRunner(String inFile, String outFile, boolean singleStep) {
    String errReport;
    File tests = new File(inFile);
	  byte[] buffer = new byte[10];

    NewOut = outFile;
    if ((outFile != null) && inFile.equals(outFile) && TestMaker.backupTests(tests)) {
    	tests = new File (new String(inFile + ".bak"));
    }
    SingleStep = singleStep;
    System.out.println("Touch enter to begin testing");
	  try {
		  System.in.read(buffer);
	  } catch (IOException e) {
		  // just ignore
	  }
    if ((errReport = AutoTestXML.parseDocument(tests, this)) != null) {
      JOptionPane.showMessageDialog( (Component)null,
          errReport,
          "Test Case Error",
          JOptionPane.ERROR_MESSAGE);
    }
  }
  
  /**
   * constructs the automated test executor class, without updating the test results
   * @param inFile is the name of the XML file holding the pre-recorded tests
   * @param singleStep is true to single step (pause) through each test case
   */
  public TestRunner(String inFile, boolean singleStep) {
    this(inFile, null, singleStep);
  }

  /**
   * takes a part and processes the first element in the XML file - the Parameter
   * element.
   * @param element is a Document element
   * @param lineNo is the line number of the </Document>
   */
  @SuppressWarnings("boxing")
  private void processParameterElement(Element element, int lineNo) {
    String version = element.getAttributeValue(TestMaker.VersionTag);
    String details = element.getAttributeValue(TestMaker.DetailLevel);
    final String layout = element.getAttributeValue(TestMaker.LayoutTag);
    double v;
    if ((version == null) || (version.length() == 0)) {
      log.warn("Missing version on test file!!");
    }
    else {  //compare the file version against this version of CATS
      v = new Double(version).doubleValue();
      if (v > new Double(VersionList.CATS_VERSION)) {
        log.warn("Test version is newer than CATS version");
      }
    }
    
    DetailLevel = TestMaker.TEST_LEVELS.DETAILED_TESTS;
    if ((details == null) || (details.length() == 0)) {
      log.warn("Missing details level in test file - assuming High");
    }
    else if (details.equals(TestMaker.Basic)) {
      DetailLevel = TestMaker.TEST_LEVELS.SIMPLE_TESTS;
    }
    else if (!details.equals(TestMaker.High)) {
      log.warn("Unknown detail level in tests: " + details);
    }
    
    if ((layout == null) || (layout.length() == 0)) {
      log.fatal("Missing layout XML file name - testing terminated!!!!!");
    }
//    else {
//      if (NewOut != null) {
//        TestMaker.renewMaker(NewOut, DetailLevel);
//      }
//      try {
//        SwingUtilities.invokeAndWait(new Runnable() {
//          public void run() {
//            Screen.init(new File(layout));
//          }
//        });
//      } catch (InterruptedException e) {
//        log.warn("Interrupted exception in creating Dispatcher Panel");
//        e.printStackTrace();
//      } catch (InvocationTargetException e) {
//        log.error("Error creating Dispatcher Panel");
//        e.printStackTrace();
//      }
//    }
    TestMaker.renewMaker(NewOut, DetailLevel);
    TestMaker.setLayout(layout);
  }

  /**
   * processes the XML element for a test case
   * @param element is a TestCase element
   * @param lineNo is the line number of the end of the element
   */
  private void processTestCase(Element element, int lineNo) {
	  String comment = element.getAttributeValue(TestMaker.Comments);
	  String result;
	  Element child;
	  Iterator<Content> action = element.getContent().iterator();
	  //    Element newTestCase = new Element(TestMaker.TestCase);
	  CaseNumber = element.getAttributeValue(TestMaker.CaseNumber);
	  //    newTestCase.setAttribute(TestMaker.CaseNumber, CaseNumber);
	  //    newTestCase.setAttribute(TestMaker.Comments, comment);
	  Element triggerElement;
//	  RREvent replayAction;
	  Element results;
	  byte[] buffer = new byte[10];
	  if ((comment == null) || (comment.length() == 0)) {
		  log.info("test case " + CaseNumber);
		  TestMaker.setComments(null);
	  }
	  else {
		  log.info("test case " + CaseNumber + ":" + comment);
		  TestMaker.setComments(comment);
	  }
	  if (SingleStep) {
		  try {
			  System.in.read(buffer);
		  } catch (IOException e) {
			  // just ignore
		  }
	  }
	  if (action.hasNext()) {
		  child = (Element) action.next();
		  //      newTestCase.addContent((Element) child.clone());
		  if (child.getName().equals(TestMaker.Trigger)) {
			  triggerElement = (Element) child.getContent(0);
			  if ((triggerElement.getAttribute(TestMaker.DELAY) != null) && !SingleStep) {
		          try {
		              Thread.sleep(5000);
		            } catch (InterruptedException e) {
		              log.error("Caught interrupt exception while waiting to capture state changes");
		              e.printStackTrace();
		            }		  				  
			  }
			  if (triggerElement.getName().equals(Section.XML_TAG)) {
				            result = Section.processSectionElement(triggerElement);
			  }
			  else if (triggerElement.getName().equals(Block.XML_TAG)) {
				  result = Block.processBlockElement(triggerElement);
			  }
			  else if (triggerElement.getName().equals(IOSpec.XML_TAG)) {
				  result = IOSpec.processIOSpecTrigger(triggerElement);
			  }
			  else if (triggerElement.getName().equals(Train.XML_TAG)) {
				  result = Train.processTrainElement(triggerElement);
			  }
			  else if (triggerElement.getName().equals(TestMaker.WAIT)) {
				  result = null;
			  }
			  else if (triggerElement.getName().equals(CatsReporter.XML_TAG)) {
				  result = CatsReporter.processReporterElement(triggerElement);
			  }
			  else {
				  result = new String("Unknown test case trigger (" + triggerElement.getName() + ")");
			  }
			  if (result != null) {
				  log.error("Error at line " + String.valueOf(lineNo) + ": " + result);
			  }
			  else { // check results
				  //          try {
				  //            Thread.sleep(1000);
				  //          } catch (InterruptedException e) {
				  //            log.error("Caught interrupt exception while waiting to capture state changes");
				  //            e.printStackTrace();
				  //          }
				  //          TestMaker.captureStateChanges(DetailLevel == TestMaker.TEST_LEVELS.SIMPLE_TESTS, newTestCase);
				  //          if ((result = compareElements(element, "", newTestCase)) != null) {
				  //            log.error("test case " + CaseNumber + " at line " + String.valueOf(lineNo) + " failed: " + result);
				  //          }
				  //        }
				  //        TestMaker.writeTestCase(newTestCase);
				  //        	synchronized(TestMaker.TestResults) {
				  //        		while (!TestMaker.TestResults) {
				  //        			try {
				  //        				TestMaker.TestResults.wait();
				  //        			} catch (InterruptedException e) {
				  //        				log.error("Caught interrupt exception while waiting to compare results");
				  //        				e.printStackTrace();
				  //        			}
				  //        		}
				  results = TestResultsSync.SyncPoint.getResults();
//				  results = (Element) TestMaker.LatestTestCase.clone();
				  if ((result = compareElements(element, "", results)) != null) {
					  log.error("test case " + CaseNumber + " at line " + String.valueOf(lineNo) + " failed: " + result);
				  }
				  //        		TestMaker.TestResults = false;
				  //        	}
			  }
		  }
		  else {
			  log.error("Missing trigger for test case ending at line " + String.valueOf(lineNo));
		  }
	  }
	  else {
		  log.error("Empty test case ends at " + String.valueOf(lineNo));
	  }
  }

  /**
   * recursively compares two XML Elements.  If they are not identical (including the order
   * of contents), they are not the same.
   * @param element1 is one XML Element to compare
   * @param path is a String indicating where elements are in the tree
   * @param element2 is the other XML Element being compared
   * @return an error String on mismatch or nothing on match
   */
  private String compareElements(Element element1, String path, Element element2) {
    String explanation = null;
    List<Attribute> attributes1 = element1.getAttributes();
    List<Attribute> attributes2 = element2.getAttributes();
    List<Content> elements1 = element1.getContent();
    List<Content> elements2 = element2.getContent();
    String attrName;
    String attrValue;
    Attribute attr;
    Content child1;
    Content child2;
    String childResult;
    int contents = Math.min(elements1.size(), elements2.size());
    if (element1.getName().equals(element2.getName())) {

      // This checks that the attributes are the same
      if (attributes1.size() == attributes2.size()) {
        for (int index = 0; index < attributes1.size(); ++index) {
          attrName = attributes1.get(index).getName();
          attrValue = attributes1.get(index).getValue();
          attr = element2.getAttribute(attrName);
          if (attr == null) {
            explanation = new String("Missing attribute (" + attrName + "): ");
          }
          else if (!attrName.equals(TestMaker.Comments) && !attrValue.equals(attr.getValue())) {
            explanation = new String("Attribute values for " + attrName + " do not match (" + attrValue
                + ", " + attr.getValue() + "): ");
          }
        }
      }
      else {
        explanation = new String("Different number of attributes (" + String.valueOf(attributes1.size())
            + ", " + String.valueOf(attributes2.size()) + "): ");
      }

      // This checks the contents
      for (int index = 0; index < contents; ++index) {
        child1 = elements1.get(index);
        child2 = elements2.get(index);
        if (Element.class.equals(child1.getClass()) && Element.class.equals(child2.getClass())) {
          childResult = compareElements((Element) child1, path + String.valueOf(index) + " ", (Element) child2);
          if (childResult != null) {
            explanation = new String("Element " + String.valueOf(index) + " ("  + element1.getName()
                + ")->"+ childResult);
          }
        }
        else if (org.jdom2.Text.class.equals(child1.getClass()) && org.jdom2.Text.class.equals(child1.getClass())) {
          if (!child1.getValue().equals(child2.getValue())) {
            explanation = new String("Different text values");
          }
        }
        else {
          explanation = new String("Different content for Element number " + String.valueOf(index));
          break;
        }
      }
      if ((elements1.size() != elements2.size()) && (explanation == null)) {
        explanation = new String("Different number of state changes (" + String.valueOf(elements1.size())
            + ", " + String.valueOf(elements2.size()) + "): ");
      }
    }
    else {
      explanation = new String("State names do not match: " + element1.getName() + "," + element2.getName());
    }
 
    return explanation;
//    if (explanation == null) {
//      return null;
//    }
//    return new String(explanation + path + element1.getName() + " and " + element2.getName());
  }
  /*********************************************************************************
   * The following handle the SAX events
   *********************************************************************************/
  
  /**
   * is called when the XML Document parsing begins
   */
  public void startDocument() {
  }
  
  /**
   * is called to signal that the XML document is processed, so the testing is complete
   */
  public void endDocument() {
    int result = JOptionPane.showConfirmDialog( (Component)null,
        "Testing is complete.  Continue running?",
        "Choose yes or no",
        JOptionPane.YES_NO_OPTION
    );
    if (result ==JOptionPane.NO_OPTION) {
      Logger.finishUp();
      TestMaker.closeRecording();
      Crandic.terminateSession();
    }
    if (TestMaker.isRecording()) {
      TestMaker.setDetails(DetailLevel, Integer.parseInt(CaseNumber));
    }
  }
  
  /**
   * processes the XML tree (test parameters or a test case)
   * @param element is the parameters element or a test case
   * @param lineNo is the line number in the XML file of the end of the element
   */
  public void endElement(Element element, int lineNo) {
    String name = element.getName();
    if (name.equals(TestMaker.DocumentTag)) {
      // do nothing
    }
    else if (name.equals(TestMaker.ParametersTag)) {
      processParameterElement(element, lineNo);
    }
    else if (name.equals(TestMaker.TestCase)) {
      processTestCase(element, lineNo);
    }
    else {
      log.error("Unknown test case element name (" + name +
          ") ending at line " + String.valueOf(lineNo));
    }
  }
  static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TestRunner.class.getName());
}
/* @(#)TestRunner.java */