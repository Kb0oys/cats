/* Name: DebugBits.java
 *
 * What:
 *   These are constants that refer to indices in the Details BitSet for controlling
 *   print messages to assist in tracing through code execution for real time debugging.
 */
package cats.common;

/**
 *   These are constants that refer to indices in the Details BitSet for controlling
 *   print messages to assist in tracing through code execution for real time debugging.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2021, 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class DebugBits {

	/**
	 * trace VitalLogic lock changes
	 */
	static public final int VITALLOGICLOCKBIT = 0;
	
	/**
	 * trace GUI lock changes 
	 */
	static public final int GUILOCKBIT = VITALLOGICLOCKBIT + 1;
	
	/**
	 * trace changes in GUI state involving routes
	 */
	static public final int GUIROUTEBIT = GUILOCKBIT + 1;
	
	/**
	 * trace codeline messages
	 */
	static public final int CODELINEBIT = GUIROUTEBIT + 1;
	
	/**
	 * trace signal changes
	 */
	static public final int SIGNALBIT = CODELINEBIT + 1;
	
	/**
	 * trace Vital Logic traffic stick changes
	 */
	static public final int TRAFFICSTICKBIT = SIGNALBIT + 1;
	
	/**
	 * trace CTC route creation
	 */
	static public final int CTCCREATIONBIT = TRAFFICSTICKBIT + 1;
	
	/**
	 * trace RouteNode operations
	 */
	static public final int ROUTENODEBIT = CTCCREATIONBIT + 1;
	
	/**
	 * trace route stacking
	 */
	static public final int ROUTESTACKING = ROUTENODEBIT + 1;
	
	/**
	 * trace automated tests
	 */
	static public final int AUTOMATEDTESTSBIT = ROUTESTACKING + 1;
	
	/**
	 * trace mouse events
	 */
	static public final int MOUSEBIT = AUTOMATEDTESTSBIT + 1;
	
	/**
	 * trace track color changes
	 */
	static public final int TRACKCOLORBIT = MOUSEBIT + 1;
	
	/**
	 * trace XML
	 */
	static public final int XMLBIT = TRACKCOLORBIT + 1;
	
	/**
	 * trace managed queue
	 */
	static public final int QUEUEBIT = XMLBIT + 1;
	
	/**
	 * trace TrainStat 
	 */
	static public final int TRAINSTATBIT = QUEUEBIT + 1;
	
	/**
	 * trace transponding
	 */
	static public final int TRANSPONDINGBIT = TRAINSTATBIT + 1;
	
	/**
	 * special purpose debug flag
	 */
	static public final int DEBUGFLAG = TRANSPONDINGBIT + 1;
	
	/**
	 * the number of bits in the BitSet (number of defined flags)
	 */
	static public final int DEBUGBITS = DEBUGFLAG + 1;
}
/* @(#)DebugBits.java */
