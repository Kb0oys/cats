/* Name: Sides.java
 *
 * What:
 *   This class holds some constants for referring to specific sides
 *   of a Section or GridTile.
 */

package cats.common;

/**
 * A class for holding constants for referring to specific sides
 * of a Section or GridTile.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class Sides {
  /**
   * is the array of Strings for identifying a Side (Edge).  These should all
   * be uppercase (because the mapping method converted candidates to upper
   * case) and in the same order of the constants.
   */
  public static final String EDGENAME[] = {
      "RIGHT",
      "BOTTOM",
      "LEFT",
      "TOP"
  };

  /**
   * identifies the Edge on the right.
   */
  public static final int RIGHT = 0;

  /**
   * identifies the Edge on the bottom.
   */
  public static final int BOTTOM = 1;

  /**
   * identifies the Edge on the left.
   */
  public static final int LEFT = 2;

  /**
   *   identifies the Edge on the top.
   */
  public static final int TOP = 3;

  /**
   * is a lookup table for specifying which Track in the adjoining
   * Section matches up with a Track.  The Tracks are identified by
   * the SecEdge that the other end terminates on.  For example, suppose
   * this SecEdge is 0 (RIGHT) and the track is "LOWERSLASH".  Then, the
   * Track is identified as 1 (it terminates on the BOTTOM edge - 1).
   * The "key" (index) of the table is the identity of a Track in this
   * SecEdge and the value is the identity of the complementary Track
   * in the adjoining SecEdge.
   */
  static public final int[] COMPLEMENT = {
    Sides.LEFT,
    Sides.TOP,
    Sides.RIGHT,
    Sides.BOTTOM
  };

  /**
   * locates which Side is being referenced.
   *
   * @param side is possibly the name of a Side.
   *
   * @return the value of the Side or NOT_FOUND if invalid.
   */
  public static int findSide(String side) {
    return Prop.findString(side, EDGENAME);
  }
}
