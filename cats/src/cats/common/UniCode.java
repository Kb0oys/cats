/*
 * Name: UniCode.java
 * 
 * What:
 *   This file contains non-ASCII constants - UTF-8.
 *   They are collected here because they are non-ASCII
 *   and some editors and compilers may have problems with them.
 *   
 * Special Considerations:
 */
package cats.common;

/**
 * This file contains non-ASCII constants - UTF-8
 * 
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
*/
public class UniCode {
	/**
	 * the checkmark symbol
	 */
	public static final String CHECKMARK = "√";
}
/* @(#)UniCode.java */