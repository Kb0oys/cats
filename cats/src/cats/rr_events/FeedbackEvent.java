/* Name: VerifyEvent.java
 *
 * What:
 *   This class is a command (see the Command design pattern) to RouteInfo
 *   to remind it that it needs to send a feedback message to its PtsVitalLogic.
 *   the feedback cannot be executed in line because of the logic flow.  The delay
 *   also simulates the prototype where requests take awhile to receive responses.
 */
package cats.rr_events;

import cats.layout.items.RouteInfo;

/**
 *   This class is a command (see the Command design pattern) to RouteInfo
 *   to remind it that it needs to send a feedback message to its PtsVitalLogic.
 *   the feedback cannot be executed in line because of the logic flow.  The delay
 *   also simulates the prototype where requests take awhile to receive responses.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class FeedbackEvent extends RREvent {

	/**
	 * the RouteInfo who wants to handle the queued event
	 */
	private final RouteInfo Requester;
	
	/**
	 * the ctor
	 * @param who is the RouteInfo that will send a feedback report
	 * to its VitalLogic
	 */
	public FeedbackEvent(RouteInfo who) {
		Requester = who;
	}
	
	@Override
	public void doIt() {
		Requester.sendFeedback();
	}
}
/* @(#)FeedbackEvent.java */