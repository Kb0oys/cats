/* Name: EventInterface.java
 *
 * What:
 *   This interface is for encapsulating actions as objects (the command
 *   design pattern).  It has a single method - doit() - which fires off the action.
 *
 * Special Considerations:
 *   This class is application independent.
 */
package cats.rr_events;

/**
 *   This interface is for encapsulating actions as objects (the command
 *   design pattern).  It has a single method - doit() - which fires off the action.
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * </p>
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2020
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public interface EventInterface {

	/**
	 * the purpose of the interface - to execute an action
	 */
	public void doIt();
}
/* @(#)EventInterface.java */
