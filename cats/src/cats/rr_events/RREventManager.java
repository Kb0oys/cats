/* Name: RREventManager.java
 *
 * What:
 *   This file contains a class that becomes the thread that receives
 *   RREvents from a queue and executes them.
 */
package cats.rr_events;

import cats.layout.AsyncDelayLine;
import cats.layout.DelayLineListener;
import cats.layout.ManagedQueue;

/**
 *   This file contains a class that becomes the thread that receives
 *   RREvents from a queue and executes them.  All objects placed in the
 *   queue must be derived from RREvent, so that the doIt() method exists
 *   and can be executed.
 *   @see cats.rr_events.RREvent
 *   @see cats.layout.Queue
 *   @see java.lang.Runnable
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class RREventManager implements DelayLineListener<RREvent> {

	/**
	 * is the event consumer constructor.
	 */
	public RREventManager() {
		EventHandler = new AsyncDelayLine<>("RREventManager", 10, 0, Thread.NORM_PRIORITY, this);
		EventQue = EventHandler.Que;
	}

	/**
	 * a FIFO for holding RREvent objects.
	 */
	public static ManagedQueue EventQue;

	/**
	 * the asynchronous queue.  No delay should be used.
	 */
	public static AsyncDelayLine<RREvent> EventHandler;

	@Override
	public void consume(RREvent item) {
		item.doIt();
	}

	/**
	 * returns the state of the Thread processing events
	 * @return the consumer Thread state
	 */
	public Thread.State getQueueThreadState() {
		return EventHandler.getThreadState();
	}
	
	/**
	 * constructs a stack dump of the CATS event processing thread - the consumer Thread
	 * @return the stack dump as a String
	 */
	public String consumerStackDump() {
		final StringBuilder dump = new StringBuilder();
		final StackTraceElement[] traceElements = EventHandler.getStackFrames();
		for (final StackTraceElement traceElement : traceElements) {
			dump.append("\n        at ");
			dump.append(traceElement);
		}
		dump.append("\n");
		return dump.toString();
	}
}
/* @(#)RREventManager.java */