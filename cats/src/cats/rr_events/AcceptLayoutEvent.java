/* Name: AcceptLayoutEvent.java
 *
 * What:
 *   This class is a command (see the Command design pattern) to a layout event
 *   processor to consume an event from the layout.
 */
package cats.rr_events;

import cats.layout.DecoderObserver;

/**
 *   This class is a command (see the Command design pattern) to a layout event
 *   processor to consume an event from the layout.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class AcceptLayoutEvent extends RREvent {

	/**
	 * the DecoderObserver which will process the event
	 */
	private final DecoderObserver Observer;
	
	/**
	 * the ctor
	 * @param observer is the DecoderObserver which will process the event
	 */
	public AcceptLayoutEvent(DecoderObserver observer) {
		Observer = observer;
	}
	
	@Override
	public void doIt() {
		Observer.acceptIOEvent();
	}
}
/* @(#)AcceptLayoutEvent.java */