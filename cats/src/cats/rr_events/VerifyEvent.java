/* Name: VerifyEvent.java
 *
 * What:
 *   This class is a command (see the Command design pattern) to SwitchPoints
 *   to verify that the points can be moved to a track.  The request is queued and
 *   not executed inline because it could be executed in the listener call stack of
 *   an AbstractTurnout, which causes problems if the action is to countermand the
 *   Turnout changing state.
 */
package cats.rr_events;

import cats.layout.items.IOSpec;

/**
 *   This class is a command (see the Command design pattern) to SwitchPoints
 *   to verify that the points can be moved to a track.  The request is queued and
 *   not executed inline because it could be executed in the listener call stack of
 *   an AbstractTurnout, which causes problems if the action is to countermand the
 *   Turnout changing state.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2009, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class VerifyEvent extends RREvent {

  /**
   * is the decoder to send the command to
   */
  private final IOSpec Who;
  
  /**
   * is true to move the points into place and false to
   * move the the other direction or reset the decoder.
   */
  private final boolean Operation;
  
  /**
   * the ctor.
   * @param who is the decoder being changed
   * @param operation is true when the points move into place and false
   * when the points move out of place.
   */
  public VerifyEvent(final IOSpec who, final boolean operation) {
    Who = who;
    Operation = operation;
  }
  
  /**
   * Performs the command encapsulated by this object.
   */
  public void doIt() {
	  if (Operation) {
		  System.out.println(Who.dumpContents());
		  Who.sendCommand();
	  }
	  else {
		  System.out.println(Who.dumpContents());
		  Who.sendUndoCommand();
	  }
  }
}
/* @(#)VerifyEvent.java */
