/* Name: CountermandEvent.java
 *
 * What:
 *   This class is a command (see the Command design pattern) to RoueInfo
 *   to force the points to move to a track.  The request is queued and
 *   not executed inline because it could be executed in the listener call stack of
 *   an AbstractTurnout, which causes problems if the action is to countermand the
 *   Turnout changing state.
 */
package cats.rr_events;

import cats.layout.items.RouteInfo;

/**
 *   This class is a command (see the Command design pattern) to SwitchPoints
 *   to force the points to move to a track.  The request is queued and
 *   not executed inline because it could be executed in the listener call stack of
 *   an AbstractTurnout, which causes problems if the action is to countermand the
 *   Turnout changing state.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2009, 2014</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class CountermandEvent extends RREvent {

	  /**
	   * are the SwitchPoints that receive the request.
	   */
	  private final RouteInfo Who;
	  
	  public CountermandEvent(RouteInfo info) {
		  Who = info;
	  }
	  
	  @Override
	  public void doIt() {
		  Who.forceAlignment();
	  }
}
/* @(#)CountermandEvent.java */