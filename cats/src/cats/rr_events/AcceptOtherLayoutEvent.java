/* Name: AcceptOtherLayoutEvent.java
 *
 * What:
 *   This class is a command (see the Command design pattern) to a layout event
 *   processor to consume the opposite of the registered layout event.
 */
package cats.rr_events;

import cats.layout.BiDecoderObserver;

/**
 *   This class is a command (see the Command design pattern) to a layout event
 *   processor to consume the opposite of the registered layout event.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class AcceptOtherLayoutEvent  extends RREvent {

	/**
	 * the DecoderObserver which will process the event
	 */
	private final BiDecoderObserver Observer;
	
	/**
	 * the ctor
	 * @param observer is the DecoderObserver which will process the event
	 */
	public AcceptOtherLayoutEvent(BiDecoderObserver observer) {
		Observer = observer;
	}
	
	@Override
	public void doIt() {
		Observer.acceptOtherIOEvent();
	}
}
/* @(#)AcceptOtherLayoutEvent.java */