/* Name: NormalizeTurnouts.java
 *
 * What:
 *   This class is a command (see the Command design pattern) generated in response
 *   to a dispatcher request to move all dispatcher controlled turnouts to the
 *   Normal route.  Often this is done at startup.  It runs on the worker thread
 *   because it has a tendency to lock up the CATS JRE - even in simulation mode.
 *   The lock up is not deterministic.  Sometimes it works.
 */
package cats.rr_events;

import cats.layout.items.PtsEdge;

/**
 *   This class is a command (see the Command design pattern) generated in response
 *   to a dispatcher request to move all dispatcher controlled turnouts to the
 *   Normal route.  Often this is done at startup.  It runs on the worker thread
 *   because it has a tendency to lock up the CATS JRE - even in simulation mode.
 *   The lock up is not deterministic.  Sometimes it works.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class NormalizeTurnouts extends RREvent {
	
	@Override
	public void doIt() {
		PtsEdge.setAllNormal();
	}
}
/* @(#)AcceptLayoutEvent.java */