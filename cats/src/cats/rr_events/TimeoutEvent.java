/* Name: TimeoutEvent.java
 *
 * What:
 * This class is a timeout event.  It contains:
 * <ol>
 * <li>a counter of the number of seconds to wait (timeout value)</li>
 * <li>an object that wants to receive the timeout</li>
 * <li>a free flag, set when this object is registered with MasterClock and
 * cleared when it is delivered back to the observer.
 * </ol>
 * This object is registered with MasterClock.  Every time MasterClock "ticks"
 * (once a second), it decrements the count by one.  When the count reaches 0,
 * MasterClock unregisters this ovject and queues it as a RREvent, which
 * gets delivered to the observer at some later time.  This forms an alarm.
 * <p>
 * Note that the free flag is needed because there is a short period of time
 * between the count being set to zero and the observer being notified
 * that the count has counted down.
 */
package cats.rr_events;

import cats.layout.TimeoutObserver;

/**
 * This class is a timeout event.  It contains:
 * <ol>
 * <li>a counter of the number of seconds to wait (timeout value)</li>
 * <li>an object that wants to receive the timeout</li>
 * <li>a free flag, set when this object is registered with MasterClock and
 * cleared when it is delivered back to the observer.
 * </ol>
 * This object is registered with MasterClock.  Every time MasterClock "ticks"
 * (once a second), it decrements the count by one.  When the count reaches 0,
 * MasterClock unregisters this ovject and queues it as a RREvent, which
 * gets delivered to the observer at some later time.  This forms an alarm.
 * <p>
 * Note that the free flag is needed because there is a short period of time
 * between the count being set to zero and the observer being notified
 * that the count has counted down.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2019</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class TimeoutEvent
    extends RREvent {

  /**
   * is the object receiving the timeout.
   */
  private TimeoutObserver Who;

  /**
   * the number of seconds to wait
   */
  public int Count;
  
  /**
   * a free flag, indicating that the TimeoutEvent is in use in either
   * the MasterClock list or the RREvent queue.
   */
  public boolean EventFree = true;
  
  /**
   * is the constructor.  There is an assumption in all of this that
   * because messages may be sent to multiple recipients and the messages
   * are not being copied, that no recipient is going to change a message's
   * contents.
   *
   * @param who is the Object where the message will be delivered.
   * @param duration is how long (in seconds) the initial timeout should be.
   * Since the Count field is global, it can be changed later.
   */
  public TimeoutEvent(TimeoutObserver who, final int duration) {
    Who = who;
    Count = duration;
  }

  /*
   * Performs the command encapsulated by this object.
   */
  public void doIt() {
	  EventFree = true;
	  Who.acceptTimeout();
  }
}
/* @(#)TimeoutEvent.java */