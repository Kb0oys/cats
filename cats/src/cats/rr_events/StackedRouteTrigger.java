/* Name: StackedRouteTrigger.java
 *
 * What:
 *   This class queues a trigger to A stacked route anchor to trigger it to walk
 *   its DAG, looking for a clear path from the anchor to the exit.
 */
package cats.rr_events;

import cats.layout.ctc.StackOfRoutes;

/**
 *   This class queues a Train selection/direction key push.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class StackedRouteTrigger extends RREvent {

//	/**
//	 * the RouteNOde anchoring the stacked route
//	 */
//	private final RouteNode Anchor;
//	
//	public StackedRouteTrigger(final RouteNode anchor) {
//		Anchor = anchor;
//	}
//	
//	@Override
//	public void doIt() {
//		Anchor.routeCheck();
//	}
	public StackedRouteTrigger() {
		// nothing to do except wait
	}
	
	@Override
	public void doIt() {
		StackOfRoutes.examineRoute();
	}
}
/* @(#)StackedRouteTrigger.java */
