/*
 * Name: Main
 * 
 * What:
 * the starting point for a driver for running tests on CATS code.
 */

package cats.apps;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * the starting point for a driver for running tests on CATS code.
 *
 * <p>Copyright: Copyright (c) 2013, 2014, 2015</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class TestGUI extends JFrame {

    /**
     * the default directory for layout files under Windows 8.1
     */
//    private static final String DefaultLayoutsWindows = "C:\\Documents and Settings\\All Users\\Documents\\Workspace\\cats\\APB-Tests";
    private static final String DefaultLayoutsWindows = "C:\\Users\\Rodney\\git\\CATS Source\\cats\\APB-Tests";

    /**
     * the default directory for layout files under Linux
     */
    private static final String DefaultLayoutsLinux = "/home/rwb/CATSTests";

    /**
     * the active one of the above
     */
    private static String DefaultLayouts;
    
    /**
     * the name of the shell script for running CATS under Linux
     */
    static final String CATS_CSH = "/home/rwb/JMRI/cats.csh";

    /**
     * the suffix on a layout file
     */
    protected static final String LAYOUT_SUFFIX = ".xml";
    
    /**
     * the default directory name of where logs are stored
     */
    private static final String LogName = "logs";
    
    /**
     * the default directory name of where test results are stored.
     * One test run's results are the next test run's test cases.
     */
    private static final String ResultsName = LogName;
    
//    /**
//     * is the command line option for requesting a test output file
//     */
//    private static final String TESTMAKER = "-TESTMAKER=";
//
//    /**
//     * is the command line option for requesting that a set of automated tests
//     * be run
//     */
//    private static final String TESTRUNNER = "-TESTRUNNER=";
//
//    /**
//     * is the command line option for singlestepping through a test plan.
//     */
//    private static final String SINGLESTEP = "-SINGLESTEP";
//
//    /**
//     * is the command line option for requesting details on state changes
//     */
//    private static final String DETAILS = "-DETAILS";
    
    /**
     * the working JFrame
     */
    static final TestGUI TestFrame = new TestGUI();

//    /**
//     * test complete
//     */
//    private static final int TEST_COMPLETE = 0;
//    
//    /**
//     * an error code for a null or missing XML file name
//     */
//    private static final int NO_FILE = -1;
//
//    /**
//     * cannot create a file from this name
//     */
//    private static final int BAD_NAME = -2;
    
//    /**
//     * the arguments
//     */
//    private static String Arguments[];
    
    /**
     * the current directory for layouts
     */
    private String CurrentLayouts;
    
    /**
     * the label on the layouts button
     */
    private JLabel LayoutsLabel = new JLabel("Layouts folder");
    
    /**
     * the button for selecting the folder containing the layouts
     */
    private JButton LayoutsButton = new JButton();
    
    /**
     * the current directory for logs
     */
//    private String CurrentLogs = DefaultLayouts + "\\" + LogName;
    private String CurrentLogs;
    
    /**
     * the label on the logs button
     */
    private JLabel LogsLabel = new JLabel("Logs folder");
    
    /**
     * the button for selecting the folder containing the logs
     */
    private JButton LogsButton = new JButton();
    
    /**
     * the current directory for logs
     */
//    private String CurrentResults = DefaultLayouts + "\\" + ResultsName;
    private String CurrentResults;
    
    /**
     * the label on the results button
     */
    private JLabel ResultsLabel = new JLabel("Results folder");
    
    /**
     * the button for selecting the folder containing the results
     */
    private JButton ResultsButton = new JButton();

    /**
     * the checkbox for generating a log
     */
    private JCheckBox LogsBox = new JCheckBox("Generate Cases");
    
    /**
     * the checkbox for running a test against an existing test case
     */
    private JCheckBox TestBox = new JCheckBox("Run Tests");
    
    /**
     * the checkbox for singlestepping through a previous test
     */
    private JCheckBox SingleStepBox = new JCheckBox("Single Step");
    
    /**
     * the checkbox for adding details
     */
    private JCheckBox DetailsBox = new JCheckBox("Add Details");
    
    /**
     * the button for selecting the test cases
     */
    private JButton CasesButton = new JButton("Test Cases");
    
    public TestGUI() {
        super("test driver");
        String osname = System.getProperty("os.name");
        if (osname == null) {
          throw new IllegalArgumentException("no os.name");
        }
        if (osname.toLowerCase().indexOf("windows") != -1) {
          DefaultLayouts = DefaultLayoutsWindows;                             
        }
        else {
          DefaultLayouts = DefaultLayoutsLinux;
        }
        CurrentLayouts = DefaultLayouts;
        CurrentLogs = new String(DefaultLayouts + File.separatorChar + LogName);
        CurrentResults = new String(CurrentLogs);
        LayoutsButton.setText(CurrentLayouts);
        LogsButton.setText(CurrentLogs);
        ResultsButton.setText(CurrentResults);
        JPanel layoutsPanel = new JPanel();
        JPanel logsPanel = new JPanel();
        JPanel resultsPanel = new JPanel();
        JPanel boxesPanel = new JPanel();
        
        LayoutsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String selection = findDirectory(CurrentLayouts);
                if (selection != null) {
                    LayoutsButton.setText(selection);
                    CurrentLayouts = selection;
                    CurrentLogs = selection + "\\" + LogName;
                    LogsButton.setText(CurrentLogs);
                    CurrentResults = selection + "\\" + ResultsName;
                    ResultsButton.setText(CurrentResults);

                }
            }
        });
        LogsButton.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent arg0) {
                String selection = findDirectory(CurrentLogs);
                if (selection != null) {
                    LogsButton.setText(selection);
                    CurrentLogs = selection;
                }
           }
        });
        ResultsButton.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent arg0) {
                String selection = findDirectory(CurrentResults);
                if (selection != null) {
                    ResultsButton.setText(selection);
                    CurrentResults = selection;
                }
           }
        });
        
        CasesButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {
                Object[] layouts = LayoutSelector.getLayouts(CurrentLayouts);
                if (layouts != null) {
                    for (Object s : layouts) {
                        System.out.println("Testing: " + s);
                    }
                }
            }
        });
        layoutsPanel.add(LayoutsLabel);
        layoutsPanel.add(LayoutsButton);
        add(layoutsPanel);
        logsPanel.add(LogsLabel);
        logsPanel.add(LogsButton);
        add(logsPanel);
        resultsPanel.add(ResultsLabel);
        resultsPanel.add(ResultsButton);
        add(resultsPanel);
        add(CasesButton);
        boxesPanel.setLayout(new GridLayout(2,2));
        boxesPanel.add(LogsBox);
        boxesPanel.add(TestBox);
        boxesPanel.add(SingleStepBox);
        boxesPanel.add(DetailsBox);
        add(boxesPanel);
     }
    
    /**
     * This method wraps the Swing file selection dialog.  It is used to identify
     * a directory.  If the user does not select a directory, it returns nothing.
     * If the user does select a directory, it returns a full path String to the
     * directory.  The user can create a new directory by entering a name in the
     * filter field.
     * @return the full path to a directory or null.
     */
    private String findDirectory(String startingPath) {
        int result;
        JFileChooser chooser = new JFileChooser(new File(startingPath));
        File fileobj;
        String directory;
        chooser.setDialogTitle("Select directory:");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        result = chooser.showOpenDialog(null);
        if (result != JFileChooser.APPROVE_OPTION) {
            System.out.println("No directory selected");
            JOptionPane.showMessageDialog(null, "No directory was selected");
            return null;
        }
        fileobj = chooser.getSelectedFile();
        directory = fileobj.getPath();
        if (fileobj.exists()) {
            if (fileobj.isDirectory()) {
                return directory;
            }
            System.out.println(directory + " is not a directory");
            JOptionPane.showMessageDialog(null, directory + " is not a directory");
            return null;
        }
        if (fileobj.mkdirs()) {
            fileobj.setExecutable(true, false);
            fileobj.setReadable(true, false);
            fileobj.setWritable(true, false);
            return directory;
        }
        System.out.println(directory + " cannot be created");
        JOptionPane.showMessageDialog(null, directory + " cannot be created");
        return null;
    }

    /**
     * creates a file in a specified directory.  The path to the file is the directory
     * name suffixed with a munged file name.  The file name is munged by appending
     * the suffix string.  If the directory does not exist, it
     * is created.  If the file does not exist, it is created.
     * @param dirName is a path to the directory containing the file
     * @param fileName is the file name without the ".xml"
     * @param suffix is a string that replaces the ".xml" on the filename
     * @return the name of the file, if the parent exists
     */
    private String createFile(final String dirName, final String fileName, final String suffix) {
        File directory = new File(dirName);
        String newName = new String(fileName + suffix);
        if (directory.exists()) {
            if (!directory.isDirectory()) {
                System.out.println(directory + " is not a directory");
                JOptionPane.showMessageDialog(null, directory + " is not a directory");
                return null;
            }
        }
        else if (directory.mkdirs()) {
            directory.setExecutable(true, false);
            directory.setReadable(true, false);
            directory.setWritable(true, false);
        }
        else {
            System.out.println(directory + " could not be created");
            JOptionPane.showMessageDialog(null, directory + " could not be created");
            return null;
        }
//        newFile = new File(directory, newName);
//        if (!newFile.exists()) {
//            try {
//                newFile.createNewFile();
//            } catch (IOException e) {
//                System.out.println(newName + " could not be created");
//                JOptionPane.showMessageDialog(null, newName + " could not be created");
//                return null;
//            }
//        }
//        return newFile; 
        return new File(directory, newName).getAbsolutePath();
    }
    
    /**
     * This method constructs the argument string for CATS.  The argument String
     * is an array of Strings, one element for each argument, as CATS might receive
     * it from a shell.
     * <p>
     * It needs the name of an XML file that describes the layout.  It also looks at
     * the buttons and command line for parameters.  It fills them in.  It constructs
     * the names of the results files from the base name of the XML file.
     * @param layout is the file containing the layout description.
     * @return the argument array if it can be created; null, if there is a problem
     */
    String[] createArgs(String layout) {
        File cats = new File(CurrentLayouts + "\\" + layout);
//        String command = new String(CATS_BATCH);
        String argv[] = new String[5];
        String tmp[];
        
        int argc = 0;
//        String temp;
//        Process p;
        if (layout == null) {
          System.out.println("No layout file name was provided");
          JOptionPane.showMessageDialog(null, "No layout file name was provided");
            return null;
        }
//        String path = cats.getParent();
        String baseName;
        
        if ((layout.length() > LAYOUT_SUFFIX.length()) && layout.endsWith(LAYOUT_SUFFIX)) {
            baseName = layout.substring(0, layout.length() - LAYOUT_SUFFIX.length());
        }
        else {
          System.out.println(layout + " is not the name of a layout file");
          JOptionPane.showMessageDialog(null, layout + " is not the name of a layout file");
           return null;
        }
        if (!cats.canRead() || !cats.isFile()) {
          System.out.println(layout + " cannot be read");
          JOptionPane.showMessageDialog(null, layout + " cannot be read");
           return null;
        }
        if (LogsBox.isSelected()) {
            argv[argc++] = new String(Crandic.TESTMAKER + 
                    createFile(CurrentLogs, baseName, LogName + LAYOUT_SUFFIX));;
        }
        if (TestBox.isSelected()) {
            // the file name is the same format because a log becomes a test generator
          argv[argc++] = new String(Crandic.TESTRUNNER + 
                    createFile(CurrentResults, baseName, LogName + LAYOUT_SUFFIX));
           
        }
        if (DetailsBox.isSelected()) {
          argv[argc++] = new String(Crandic.DETAILS);
        }
        if (SingleStepBox.isSelected()) {
          argv[argc++] = new String(Crandic.SINGLESTEP);
        }
        argv[argc++] = new String(cats.getAbsolutePath());
        tmp = new String[argc];
        System.arraycopy(argv, 0, tmp, 0, argc);
        return tmp;
    }
    
    public static void main(String args[]) {
//        Arguments = args;
        TestFrame.setLayout(new BoxLayout(TestFrame.getContentPane(), BoxLayout.Y_AXIS));
        TestFrame.pack();
        TestFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        TestFrame.setVisible(true);
    }
}
/* @(#)TestGUI.java */