/* Name: TestThread.java
 *
 * What:
 *   This class is a Thread separate from the Swing dispatch thread for running CATS.
 *   It is necessary because invokeAndWait() cannot be run on the Swing dispatch
 *   thread.  The design is taken directly from the Java SE 1.6 doc page for invokeAndWait().
 */
package cats.apps;

import cats.automatedTests.TestRunner;

/**
 *   This class is a Thread separate from the Swing dispatch thread for running CATS.
 *   It is necessary because invokeAndWait() cannot be run on the Swing dispatch
 *   thread.  The design is taken directly from the Java SE 1.6 doc page for invokeAndWait().
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class TestThread implements Runnable {
	private final String InputName;
	private final String OutputName;
	private boolean SingleStep;

	public TestThread(String inName, String outName, boolean single) {
		InputName = inName;
		OutputName = outName;
		SingleStep = single;
	}
	final Runnable doTest = new Runnable() {
		public void run() {
			new TestRunner(InputName, OutputName, SingleStep);
		}
	};

	@Override
    public void run() {
        try {
//            SwingUtilities.invokeAndWait(doTest);
        	new TestRunner(InputName, OutputName, SingleStep);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Finished on " + Thread.currentThread());
	}
}
