
/*
 * Name: LayoutSelector
 * 
 * What:
 * A class that presents a list of all files in a directory.  It prompts the user
 * to select anywhere from one to all and returns a list of the files selected.
 */
package cats.apps;

import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

/**
 * A class that presents a list of all files in a directory.  It prompts the user
 * to select anywhere from one to all and returns a list of the files selected.
 *
 * <p>Copyright: Copyright (c) 2013</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class LayoutSelector extends JDialog {

  /**
   * the arguments to CATS
   */
  String Argv[];

  /**
   * the Swing JList that displays the files in the directory
   */
  private JList FileList;

  /**
   * the JButton to clear all selections
   */
  private JButton ClearAll = new JButton("Clear All");

  /**
   * the JButton to select all
   */
  private JButton SelectAll = new JButton("Select All");

  /*
   * the JButton to cancel
   */
  private JButton Cancel = new JButton("Cancel");

  /**
   * the JButton for done
   */
  private JButton Execute = new JButton("Execute");

  /**
   * the list of files in the directory
   */
  private String[] FileNames;
  
  /**
   * the ctor
   * @param directory is the directory from which to select files
   */
  public LayoutSelector(File directory) {
    super((Frame) null, "select layout(s)", false);
    setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
    if (directory.isDirectory()) {
      JPanel buttonPanel = new JPanel();
      FileNames = directory.list(new xmlFilter());
      FileList = new JList(FileNames);
      FileList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
      add(FileList);
      Cancel.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent arg0) {
          FileList.clearSelection();
          dispose();
        }               
      });
      Execute.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent arg0) {
          int result = -1;
          int[] selections;
          if (!FileList.isSelectionEmpty()) {
        	  selections = FileList.getSelectedIndices();
            for (int i = 0; i < selections.length; ++i) {
              if ((Argv = TestGUI.TestFrame.createArgs(FileNames[selections[i]])) != null) {
                //                            Thread t = new Thread() {
                  //                              public void run() {
                //                                runCATS();
                //                              }
                //                            };
                //                            t.start();
                //                            try {
                //                              t.join();
                //                            } catch (InterruptedException e) {
                //                              System.out.println(s + " interrupted");
                //                              e.printStackTrace();
                //                            }
                String osname = System.getProperty("os.name");
                if (osname == null) {
                  throw new IllegalArgumentException("no os.name");
                }
                if (osname.toLowerCase().indexOf("windows") != -1) {
                  Crandic.main(Argv);                             
                }
                else {
                  String args[] = new String[Argv.length + 1];
                  System.arraycopy(Argv, 0, args, 1, Argv.length);
                  args[0] = new String(TestGUI.CATS_CSH);
                  try {
                    Process p = Runtime.getRuntime().exec(args);
                    result = p.waitFor();
                  } catch (InterruptedException e) {
                    System.out.println("Process interrupted");
                    e.printStackTrace();
                  } catch (IOException e) {
                    System.out.println("Process IOException");
                    e.printStackTrace();
                  }
                  System.out.println(FileNames[i] + " ended with code " + result);
                }
              }
            }
          }
        }               
      });
      ClearAll.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent arg0) {
          FileList.clearSelection();
        }
      });
      SelectAll.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent arg0) {
          int size = FileList.getModel().getSize();
          FileList.setSelectionInterval(0, size - 1);
        }                              
      });
      buttonPanel.setLayout(new GridLayout(2,2));
      buttonPanel.add(ClearAll);
      buttonPanel.add(SelectAll);
      buttonPanel.add(Cancel);
      buttonPanel.add(Execute);
      add(buttonPanel);
      pack();
      setVisible(true);
    }
    else {
      System.out.println(directory.getName() + " is not a directory");
      JOptionPane.showMessageDialog(null, directory.getName() + " is not a directory");
    }
  }

  /**
   * determine which files have been selected and returns their names
   * in an array
   * @param dirName is the directory containing the layout files
   * @return the names of the files selected
   */
  static public String[] getLayouts(String dirName) {
    File directory = new File(dirName);
    LayoutSelector sel = new LayoutSelector(directory);
    if ((sel.FileList != null) && !sel.FileList.isSelectionEmpty()) {
    	int[] selections = sel.FileList.getSelectedIndices();
    	String[] fileNames = new String[selections.length];
    	for (int i = 0; i < selections.length; ++i) {
    		fileNames[i] = sel.FileNames[i];
    	}
    	return fileNames;
    }
    return null;
  }

  /*************************************************************************************
   * an internal class for filtering file names
   **************************************************************************************/
  private class xmlFilter implements FilenameFilter {

    public boolean accept(File arg0, String arg1) {
      String candidate = new String(arg1);
      candidate.toLowerCase();
      if (candidate.endsWith(TestGUI.LAYOUT_SUFFIX)) {
        return true;
      }
      return false;
    }

  }
}
/* @(#)LayoutSelector.java */