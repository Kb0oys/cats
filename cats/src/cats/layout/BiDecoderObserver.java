/*
 * Name: BiDecoderObserver.java
 *
 * What:
 *   This file contains an interface required by all Objects which register
 *   with an IOSpec to be notified when the decoder fires.  It accepts both
 *   Throw and Close events
 */
package cats.layout;

/**
 *   This file contains an interface required by all Objects which register
 *   with an IOSpec to be notified when the decoder fires.  It accepts both
 *   Throw and Close events
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface BiDecoderObserver extends DecoderObserver {

  /**
   * is the interface through which the IOSpec delivers the other notification.
   */
  public void acceptOtherIOEvent();
}
/* @(#)BiDecoderObserver.java */
