/* Name: TurnoutTestScheduler.java
 *
 * What:
 * This class is a timeout event. It's purpose is to wait for a short
 * period of time, then set all the points to a particular state.  Running
 * the code can lock up CATS/JMRI/Java. Since the lockups appear random,
 * it is possible that the thread running the test could be a factor.
 * This class moves the test off the GUI thread.
 */
package cats.layout;

import cats.layout.items.PtsEdge;
import cats.rr_events.TimeoutEvent;

/**
 * This class is a timeout event. It's purpose is to wait for a short
 * period of time, then set all the points to a particular state.  Running
 * the code can lock up CATS/JMRI/Java. Since the lockups appear random,
 * it is possible that the thread running the test could be a factor.
 * This class moves the test off the GUI thread.
 * <p>Title: CATS - Computer Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class TurnoutTestScheduler implements TimeoutObserver {

	/**
	 * the desired state to place the points in
	 */
	boolean SetNormal;
	
	public TurnoutTestScheduler(final Boolean testDirection) {
		MasterClock.MyClock.setTimeout(new TimeoutEvent(this, 1));
		SetNormal = testDirection;
	}
	
	@Override
	public void acceptTimeout() {
		PtsEdge.testThrown(SetNormal);
	}
}
/* @(#)TurnoutTestScheduler.java */
