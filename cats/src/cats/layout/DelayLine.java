/* Name: DelayLine.java
 *
 * What:
 *   This class defines a Queue of any type, which can be shared by multiple
 *   threads (implying that operations are synchronized for exclusive access).
 *   It is built on a linked list.  The elements in the linked list are reused,
 *   rather than "new" whenever something is added.  So, the resource pool is
 *   itself a linked list.  The new allocator is invoked only when the
 *   resource pool is empty.
 *<p>
 *	It adds a delay from being added to the queue until removed, in addition to
 *	the synchronizing of things being added.  The delay value can be adjusted in
 *	mid-stream.
 *<p>
 *	A "push" (rather than "pull") delivery algorithm is used.  This means
 *	a single consumer registers with the queue and the consumer is told
 *	when something arrives rather than the consumer calling a receive
 *	method and waiting for something to arrive.
 *
 * Special Considerations:
 *   This class is application independent.
 */
package cats.layout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

/**
 *   This class defines a Queue of any type, which can be shared by multiple
 *   threads (implying that operations are synchronized for exclusive access).
 *   It is built on a linked list.  The elements in the linked list are reused,
 *   rather than "new" whenever something is added.  So, the resource pool is
 *   itself a linked list.  The new allocator is invoked only when the
 *   resource pool is empty.
 *<p>
 *	It adds a delay from being added to the queue until removed, in addition to
 *	the synchronizing of things being added.  The delay value can be adjusted in
 *	mid-stream.
 *<p>
 *	A "push" (rather than "pull") delivery algorithm is used.  This means
 *	a single consumer registers with the queue and the consumer is told
 *	when something arrives rather than the consumer calling a receive
 *	method and waiting for something to arrive.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class DelayLine<T> {
	// is the total number of elements in use.  It is just a way of monitoring
	// the density of traffic
	protected int QueueSize = 0;
	
	// is the delay line
	protected final LinkedList MyQueue = new LinkedList();
	
	// is the free pool of Elements, that are reused.  This removes the need
	// to create and free Elements
	protected final ExtensibleLinkedList Pool = new ExtensibleLinkedList();
	
	// is the destination of content, after being delayed
	protected final DelayLineListener Consumer;
	
	  /**
	   * is the LastTimeout.  It is remembered so that if it changes, the timer needs to be reset.
	   */
	  protected int LastTimeout = 0;
	  
	  /**
	   * is the current timeout
	   */
	  protected int ActiveTimeout;
	  
	  /**
	   * is a timer for how long to hold something in milliseconds
	   */
	  protected Timer TransmissionDelay;

	/**
	 *  The DelayLine constructor
	 *  @param initialLength is the number of items in the resource
	 *  pool to begin with
	 *  @param consumer is the destination for the elements
	 *  @param initialTimeout is how long to hold things in the queue in milliseconds
	 */
	public DelayLine(int initialLength, final DelayLineListener consumer, final int initialTimeout) {
		Consumer = consumer;
		ActiveTimeout = initialTimeout;
		QueueSize = initialLength;
		for (; initialLength > 0; --initialLength) {
			Pool.add(new Element(null));
		}
	    TransmissionDelay = new Timer(0, new ActionListener() {
	        public void actionPerformed(ActionEvent ae) {
	      	  receive();
	        }
	      });
	      TransmissionDelay.setRepeats(false);
	}

	/**
	 *  This method adds an item to the Queue.  It first tries
	 *  to get an unused Element from the pool and use it.  If
	 *  the pool is empty, it allocates a new Element.
	 *
	 * @param s is the item to be added to the Queue
	 */
	public synchronized void append(T s) {
		Element e;
		  if (MyQueue.isEmpty()) {
			  if (LastTimeout != ActiveTimeout) {
				  LastTimeout = ActiveTimeout;
				  TransmissionDelay.setDelay(ActiveTimeout);
				  TransmissionDelay.setInitialDelay(ActiveTimeout);
			  }
			  if (LastTimeout == 0) {
				  Consumer.consume(s);
			  }
			  else {
				  e = Pool.remove();
				  e.Content = s;
				  MyQueue.add(e);
				  TransmissionDelay.setRepeats(true);
				  TransmissionDelay.start();
			  }
		  }
		  else {
			  e = Pool.remove();
			  e.Content = s;
			  MyQueue.add(e);
		  }
	}

	/**
	 * Takes something off the queue and passes it to the consumer.
	 * Removing something from the queue is divorced from telling the
	 * consumer because the queue manipulation needs to be
	 * synchronized, while consuming should not.
	 */
	protected void receive() {
		T received = get();
		if (received != null) {
			Consumer.consume(received);
		}
	}
	
	/**
	 * This method pulls the oldest item off the Queue.  If
	 * there is one, the Element is returned to the free pool,
	 * and its contents sent to the consumer.
	 *
	 * @return The contents of the oldest item on the Queue
	 */
	protected synchronized T get() {
		Element e = MyQueue.remove();
		if (e != null) {
			T item = e.Content;
			e.Content = null;
			e.Next = null;
			Pool.add(e);
			if (MyQueue.isEmpty()) {
				TransmissionDelay.stop();
			}
			return item;
		}
		return null;
	}

	/**
	 * a predicate for determining if the queue is empty,
	 * to avoid blocking
	 * @return true if there is an element in the queue
	 */
	public synchronized boolean isEmpty() {
		return MyQueue.isEmpty();
	}

	/**
	 * changes the delay when the queue is next emptied.  It cannot be
	 * changed immediately because changing to 0 would strand anything
	 * lef tin the queue.
	 * @param newTimeout is the new time out in milliseconds
	 */
	public void adjustTimeout(final int newTimeout) {
		ActiveTimeout = newTimeout;
	}
	
	/****************************************************************
	 * inner classes
	 ****************************************************************/
	/****************************************************************
	 *   An inner class object for providing the linkage between
	 *   entries in a Queue.
	 ****************************************************************/
	public class Element {
		Element Next;
		public T Content;

		/**
		 *  The constructor.  It creates a wrapper around the
		 *   value queued, containing the linkages that form the Queue
		 *
		 * @param   s is the thing being put in the Queue
		 */
		public Element(T s) {
			Content = s;
			Next = null;
		}
	}
	/****************************************************************
	 *   end of the Element class
	 ****************************************************************/

	/****************************************************************
	 *   An inner class that is an unsynchronized linked list
	 ****************************************************************/
	protected class LinkedList {
		Element Head = null; /* where items are removed */
		Element Tail = null; /* where items are added */

		/**
		 * adds an item to the end of the linked list
		 * @param e is the item to add
		 */
		public void add(Element e) {
			if (Tail == null) {
				Head = e;
			}
			else {
				Tail.Next = e;
			}
			e.Next = null;
			Tail = e;
		}

		/**
		 * returns the first item in the linked list
		 * @return the first item, if the list is not empty;
		 * else it returns null
		 */
		public Element remove() {
			Element ele = Head;
			if (Head != null) {
				Head = Head.Next;
				if (Head == null) {
					Tail = null;
				}
				return ele;
			}
			log.warn("Attempt to remove Element from empty LinkedList");
			return null;
		}

		/**
		 * a predicate for determining if the LinkedList is empty
		 * @return true if there is an element in the linked list
		 */
		public boolean isEmpty() {
			return Head == null;
		}
	}
	/****************************************************************
	 *   end of the LinkedList class
	 ****************************************************************/
	/****************************************************************
	 *   An inner class that creates new Elements when empty
	 ****************************************************************/
	protected class ExtensibleLinkedList extends LinkedList {
		
		@Override
		public Element remove() {
			Element e;
			if (Head == null) {
				++QueueSize;
				return new Element(null);
			}
			e = Head;
			Head = Head.Next;
			if (Head == null) {
				Tail = null;
			}
			return e;			
		}
	}
	/****************************************************************
	 *   end of the LinkedList class
	 ****************************************************************/
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
			DelayLine.class.getName());
}
/* @(#)DelayLine.java */

