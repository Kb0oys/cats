/* Name: AsyncDelayLine.java
 *
 * What:
 *   This class defines a Queue of any type, which can be shared by multiple
 *   threads (implying that operations are synchronized for exclusive access).
 *   It is built on a linked list.  The elements in the linked list are reused,
 *   rather than "new" whenever something is added.  So, the resource pool is
 *   itself a linked list.  The new allocator is invoked only when the
 *   resource pool is empty and something is to be added to the queue.
 *<p>
 *	It adds a delay from being added to the queue until removed, in addition to
 *	the synchronizing of things being added.  The delay can be adjusted in
 *	mid-stream.  
 *<p>
 *	Unlike the DelayLine class, which uses a simple timer
 *  to implement the delay, this class runs the consumer asynchronously
 *  on an independent thread.  It uses the Java wait() call for the delay.
 *  Thus, it uses a "pull" mechanism to get things off the queue.  It also has
 *  a "flush" method to pull everyhting off the queue.
 */
package cats.layout;

/**
  *   This class defines a Queue of any type, which can be shared by multiple
 *   threads (implying that operations are synchronized for exclusive access).
 *   It is built on a linked list.  The elements in the linked list are reused,
 *   rather than "new" whenever something is added.  So, the resource pool is
 *   itself a linked list.  The new allocator is invoked only when the
 *   resource pool is empty and something is to be added to the queue.
 *<p>
 *	It adds a delay from being added to the queue until removed, in addition to
 *	the synchronizing of things being added.  The delay can be adjusted in
 *	mid-stream.  
 *<p>
 *	Unlike the DelayLine class, which uses a simple timer
 *  to implement the delay, this class runs the consumer asynchronously
 *  on an independent thread.  It uses the Java wait() call for the delay.
 *  Thus, it uses a "pull" mechanism to get things off the queue.  It also has
 *  a "flush" method to pull everything off the queue.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class AsyncDelayLine<T> implements Runnable {
		/**
		 * Is the number of milliseconds to delay between successive events
		 */
		private int Delay;

		/**
		 * a FIFO for holding the items
		 */
		public final ManagedQueue<T> Que;

		/**
		 * The thread that removes the items from the queue.
		 */
		private final Thread OutputThread;
		
		private final DelayLineListener<T> Consumer;
		
		/**
		 * The class constructor.
		 * 
		 * @param threadName
		 *            is the name of the asynchronous thread
		 * @param initialSize
		 *            is the starting size of the queue
		 * @param initialDelay
		 *            is the starting delay value in milliseconds
		 * @param priority
		 * 			  is the priority of the thread
		 * @param consumer
		 * 			  is the object consuming the items from the DelayLine
		 */
		public AsyncDelayLine(final String threadName, final int initialSize, final int initialDelay, final int priority,
				final DelayLineListener<T> consumer) {
			Que = new ManagedQueue<T>(initialSize);
			Delay = initialDelay;
			Consumer = consumer;

			OutputThread = new Thread(this);
			OutputThread.setName(threadName);
			OutputThread.start();
			OutputThread.setPriority(priority);
		}

		/*
		 * Name: void run()
		 *
		 * What: This is the Thread
		 *
		 * Inputs: There are none
		 *
		 * Returns: None
		 *
		 * Special Considerations:
		 */
		public void run() {
			T item;
			while (true) {
				item = Que.get();
				Consumer.consume(item);
				if (Delay != 0) {
					try {
						Thread.sleep(Delay);
					} catch (InterruptedException ignore) {
					}
				}
			}
		}

		/**
		 * sets the delay between sending messages.
		 *
		 * @param msecDelay
		 *            is the delay in milliseconds
		 */
		public void setMsgDelay(int msecDelay) {
			Delay = msecDelay;
		}

		/**
		 * queues an item for the delay line
		 * 
		 * @param item
		 *            is the thing being delayed
		 */
		public void queueItem(final T item) {
			Que.append(item);
		}
		
		/**
		 * is a query for the state of the thread
		 * @return the thread state
		 */
		public Thread.State getThreadState() {
			return OutputThread.getState();
		}
		
		/**
		 * retrieves the stack of the event processing (consumer) thread as frames
		 * in an array
		 * @return the Consumer Thread stack trace
		 */
		public StackTraceElement[] getStackFrames() {
			return OutputThread.getStackTrace();
		}
}
/* @(#)AsyncDelayLine.java */
