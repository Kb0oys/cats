/* Name: RefreshScheduler.java
 *
 * What:
 * This class is a timeout event. It's purpose is to wait for a short
 * period of time, then read back all JMRI devices, updating their CATS
 * equivalents.  The reason for the delay is that performing the
 * read back immediately after the panel is initialized sometimes locks
 * up so tightly that the CATS process must be manually killed.  Running
 * the same code from a menu pull down does not seem to have similar
 * issues.  Since the lockups appear random, it is possible that the
 * thread the read backs are running on could be a factor.  This class
 * both delays the read back activity and moved it off the startup thread.
 */
package cats.layout;

import java.awt.Point;

import cats.layout.ctc.StackOfRoutes;
import cats.rr_events.TimeoutEvent;

public class StackedRouteViewer  implements TimeoutObserver {
	
	/**
	 * the upper left corner of the display window
	 */
	private final Point L;
	
	/**
	 * ctor
	 * @param location is a point on the screen to anchor the displayed window
	 */

	public StackedRouteViewer(final Point location) {
		L = location;
		MasterClock.MyClock.setTimeout(new TimeoutEvent(this, 1));
	}
	
	@Override
	public void acceptTimeout() {
		StackOfRoutes.displayRoutes(L);;
	}
}
/* @(#)StackedRouteViewer.java */
