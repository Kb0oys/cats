/* Name: RouteWalker.java
 *
 * What:
 *   This class implements an Enumerator for traversing the <B>blocks</B>
 *   from an initial BlkEdge to the first CPEdge (signal) that would
 *   be seen by a train moving away from the BlkEdge.  It enumerates the
 *   BlkEdges in approach; thus, it visits the approach BlkEdges.  There are two possible
 *   results:
 *   <ol>
 *   <li> it finds an approach BlkEdge (or derivative) whose neighbor is a CPEdge</li>
 *   <li> it runs out of BlkEdge in approach (e.g. edge of layout,
 *   points lined the wrong way, etc.)
 *   </ol>
 *   <p>
 *   The purpose of this class is for identifying the opposing signals for
 *   a train moving away from the BlkEdge upto the next signal the train sees.
 */
package cats.layout.items;

import java.util.Enumeration;

import cats.layout.items.EdgeInterface.EDGE_TYPE;

/**
 *   This class implements an Enumerator for traversing the <B>blocks</B>
 *   from an initial BlkEdge to the first CPEdge (signal) that would
 *   be seen by a train moving away from the BlkEdge.  It enumerates the
 *   BlkEdges in approach; thus, it visits the approach BlkEdges.  There are two possible
 *   results:
 *   <ol>
 *   <li> it finds an approach BlkEdge (or derivative) whose neighbor is a CPEdge</li>
 *   <li> it runs out of BlkEdge in approach (e.g. edge of layout,
 *   points lined the wrong way, etc.)
 *   </ol>
 *   <p>
 *   The purpose of this class is for identifying the opposing signals for
 *   a train moving away from the BlkEdge upto the next signal the train sees.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2015, 2016, 2018</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class RouteWalker implements Enumeration {

	/**
	 * the next SecEdge to be handed out
	 */
	private AbstractTrackEdge nextEdge = null;

	public RouteWalker(AbstractTrackEdge origin) {
		nextEdge = getApproachEdge(origin);
	}

	@Override
	public boolean hasMoreElements() {
		return nextEdge != null;
	}

	@Override
	public Object nextElement() {
		AbstractTrackEdge thisEdge = nextEdge;
		SecEdge neighbor;
		if ((nextEdge != null) && ((neighbor = nextEdge.getNeighbor()) != null)
				&& !neighbor.getEdgeType().equals(EDGE_TYPE.CP_EDGE)) {
			nextEdge = thisEdge.getApproachEdge();
		}
		else {
			nextEdge = null;
		}
		return thisEdge;
	}

	/**
	 * this method locates the next BlkEdge in approach
	 * @param origin is the BlkEdge to start with
	 * @return the BlkEdge in approach, which could be null
	 */
	private AbstractTrackEdge getApproachEdge(AbstractTrackEdge origin) {
		if (origin != null) {
			return origin.getApproachEdge();
		}
		return null;
	}
}
/* @(#)RouteWalker.java */
