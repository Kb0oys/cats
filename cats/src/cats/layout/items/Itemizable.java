/*
 * Name: Itemizable.java
 *
 * What:
 *   This file contains an interface required by all sub-components of
 *   a Section.
 */
package cats.layout.items;

import org.jdom2.Element;

import cats.gui.GridTile;
import cats.layout.xml.XMLEleObject;

/**
 * defines the interface for Objects that are sub-components of a Section.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
 public interface Itemizable extends XMLEleObject {

   /**
    * tells the sub-component where its Section is, so that the sub-component
    * can register itself and retrieve anything else it needs from the Section.
    *
    * @param sec is the Section containing the sub-component.
    *
    * @see Section
    */
   public void addSelf(Section sec);

   /**
    * asks the sub-component to install itself on the painting surface.
    *
    * @param tile is the GridTile to install a frill on.
    *
    * @see cats.gui.GridTile
    */
   public void install(GridTile tile);

   /**
    * asks the sub-component if it has anything to paint on the Screen.
    *
    * @return true if it does and false if it doen't.
    */
   public boolean isVisible();
   
   /**
    * asks the sub-component to form any state changes since the last snap shot
    * into an XML Element.  This is used for recording the state changes for
    * establishing automated tests and in checking state changes when running
    * automated tests.
    * 
    * @param basic is true if only basic state changes are requested and false
    * for more detailed state information
    * 
    * @return an XML Element containing the changes since the last snapshot or
    * null if there were none
    */
   public Element dumpState(boolean basic);
 }
 /* @(#)Itemizable.java */