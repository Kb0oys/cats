/* Name: SignalEdge.java
 *
 * What:
 *   This interface specializes a TrackEdge for those edges that are at the end a sequence of
 *   Blocks, grouped together - an edge containing a signal or points.  The purpose is
 *   to connect signals to their approach signals.
 */
package cats.layout.items;

/**
 *   This class specializes a TrackEdge for those edges that are at the end a sequence of
 *   Blocks, grouped together - an edge containing a signal or points.  The purpose is
 *   to connect signals to their approach signals.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2013, 2018, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface SignalEdge {
  
/**
 * this method initiates probing for the signal in approach of this one.
 * It follows the chain of TrackEdges in approach until one containing
 * a signal is encountered.  That TrackEdge is returned and becomes
 * the approach signal.
 */
public void discoverApproachSignal();

}
/* @(#)SignalEdge.java */