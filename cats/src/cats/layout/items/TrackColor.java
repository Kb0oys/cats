/* Name TrackColor.java
 *
 * What:
 *  This class holds an enumeration defining the reasons and colors for painting tracks.
 *  The defined reasons are:
 *  <ul>
 *  <li>dark territory - no detector has been defined for the track</li>
 *  <li>idle track - there are detectors, but nothing is happening with the track</li>
 *  <li>occupied - track occupnacy is been detected or forced</li>
 *  <li>track and time - the dispatcher has granted a train permission to use the track</li>
 *  <li>out of service - the track has been taken out of service for maintenance</li>
 *  <li>reserved - the track has been reserved for a train to move over it</li>
 *  </ul>
 *  A track may hold multiple reasons; thus, the reasons are contained in an EnumSet.
 *  They are ordered by priority (lowest being the highest priority).  Consequently,
 *  color selection is done by finding the first TrackColor in the EnumSet.
 *  <p>
 *  A TrackColor Enum is associated with a Color reference.  So, after the highest
 *  priority reason is found, the associated color reference is picked up.
 */
package cats.layout.items;

import cats.layout.ColorList;

/*
 *  This class holds an enumeration defining the reasons and colors for painting tracks.
 *  The defined reasons are:
 *  <ul>
 *  <li>dark territory - no detector has been defined for the track</li>
 *  <li>idle track - there are detectors, but nothing is happening with the track</li>
 *  <li>occupied - track occupancy is been detected or forced</li>
 *  <li>track and time - the dispatcher has granted a train permission to use the track</li>
 *  <li>out of service - the track has been taken out of service for maintenance</li>
 *  <li>reserved - the track has been reserved for a train to move over it</li>
 *  <li>occupied and warrant - the track is both occupied and has track and time or out of service
 *  </ul>
 *  A track may hold multiple reasons; thus, the reasons are contained in an EnumSet.
 *  They are ordered by priority (lowest being the highest priority).  Consequently,
 *  color selection is done by finding the first TrackColor in the EnumSet.
 *  <p>
 *  A TrackColor Enum is associated with a Color reference.  So, after the highest
 *  priority reason is found, the associated color reference is picked up.
 * Title: CATS - Crandic Automated Traffic System
 * </p>
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2014, 2015, 2018
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public enum TrackColor {
	// the order is important, so be careful about changing it!
	OCC_TNT(ColorList.OCC_WARRANT),
	TRACK_N_TIME(ColorList.LOCAL),
	OUT_OF_SERVICE(ColorList.OOSERVICE),
	OCCUPIED(ColorList.OCCUPIED),
	ROUTE(ColorList.RESERVED),
	WARRANT(ColorList.DTC),
	IDLE(ColorList.EMPTY),
	DARK_TERRITORY(ColorList.DARK);
	
	private final String ColorRef;
	
	/**
	 * the ctor that associates a Color reference with each reason
	 * @param color is the color reference
	 */
	TrackColor(String color) {
		ColorRef = color;
	}
	
	/**
	 * retrieves the color reference associated with the condition
	 * @return the color reference
	 */
	public String getColor() {
		return new String(ColorRef);
	}
}
/* @(#)TrackColor.java */
