/* Name: EdgeInterface.java
 *
 * What:
 *   This interface lists the methods that track end on
 *   a section edge must use.  It is the base interface
 *   that TrackEdge extends.
 */

package cats.layout.items;

import org.jdom2.Element;

/**
 *   This interface lists the methods that tracks ending on
 *   a section edge must use.  It is the base interface
 *   that TrackEdge extends.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014, 2016, 2018</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface EdgeInterface {
	/**
	 * the kinds of Section edges.  Each derived class must
	 * be one of the following.  To simplify comparisons, the
	 * order is important.  So, careful testing must be done
	 * if the order is changed!
	 */
	public enum EDGE_TYPE {
			// the first group are internal to a Track Sequence
		SECTION_EDGE,
		UNKNOWN_EDGE,
			// the next group delimit a Track Sequence
		INTERNAL_EDGE,
		CROSSING_EDGE,
		FROG_EDGE,
		POINTS_EDGE,
		CP_EDGE,
		ABSTRACT_EDGE,
			// the next group delimit a detection Block
		BLOCK_EDGE,
		XBLOCK_EDGE,
		DEFAULT_EDGE,
			// the last group are block edges with signals
		INTERMEDIATE_EDGE,
		OS_EDGE
	}
	
	/**
	 * returns the SecEdge sharing the edge.
	 *
	 * @return the SecEdge that shares the edge.
	 *
	 */
	public SecEdge getNeighbor();
	
	/**
	 * identifies what kind of edge the edge is, rather than use
	 * the Java "instance of".  Though the latter is safer, the test
	 * must be asked for each candidate, when it is sometimes it
	 * is cleaner to use a switch statement.
	 * @return the edge type
	 */
	public EDGE_TYPE getEdgeType();

	/**
	 * Is a method in SecEdge to set the Block that it belongs to
	 * 
	 * @param block is the Block identity being propagated.
	 */
	public void setBlock(Block block);

	/**
	 * sets the Section, for upward references
	 * 
	 * @param sec is the Section containing the Edge.
	 */
	public void setSection(Section sec);

	/**
	 * comes from SecEdge
	 *
	 * @param trk is the SecEdge at the other end.
	 *
	 * @return false because there is only one route through the edge.
	 * Returning true will be misleading because it implies that the
	 * edge can be switched.
	 */
	public boolean setDestination(Track trk);

	/**
	 * records the state specific to a derived SecEdge.
	 * @param basic is true to record only basic state information.  It is false
	 * to record detailed state information.
	 * @return an Element, if the state has changed since the last snapshot; otherwise,
	 * null.
	 */
	public Element dumpDerivativeState(boolean basic);
}
/* @(#)EdgeInterface.java */