/* Name: OSEdge.java
 *
 * What:
 *   This class is the container for all the information about an OS
 *   Section Edge - a specialization of PtsEdge that implemants a
 *   set of SwitchPoints, under dispatcher control.
 */

package cats.layout.items;

import java.util.ArrayList;

import cats.gui.DecoderInterlock;
import cats.layout.codeLine.CodePurpose;
import cats.layout.codeLine.Codes;

/**
 * "is a" SecEdge which represents switch points - an edge on which
 * multiple tracks (routes) terminate - under dispatcher control.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2009, 2010, 2011, 2012, 2018, 2021, 2024</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class OSEdge
    extends PtsEdge {

  /**
   * constructs a SecEdge with only its Edge identifier.
   *
   * @param edge identifies the side of the GridTile the SecEdge is on.
   * @param shared describes the shared edge, if it is not adjacent.
   *    A null value means it is adjacent.
   * @param points are describes the routes which terminate on the PtsEdge.
   */
  public OSEdge(int edge, Edge shared, SwitchPoints points) {
    super(edge, shared, points);
  }

	@Override
	public EdgeInterface.EDGE_TYPE getEdgeType() {
		return EdgeInterface.EDGE_TYPE.OS_EDGE;
	}

	/**
	 * used by the dispatcher to request that the points be moved.  Since the
	 * dispatcher controls OS points, this eventually moves the points.
	 *
	 * @param trk identifies the requested aligned Track.  It must be between
	 * 0 and 3.
	 * @return true if the points are aligned or can be aligned to the requested
	 * Track.  False, if the points cannot be moved.
	 */
	public boolean requestAlignment(final int trk) {
		if (MyTracks[trk] != null) {
			return requestAlignment(MyTracks[trk]);
		}
		return false;
	}
	
	/**
	 * used by the dispatcher to request that the points be moved.  Since the
	 * dispatcher controls OS points, this eventually moves the points.
	 *
	 * @param trk identifies the requested aligned Track.  It must exist in MyTracks.
	 * @return true if the points are aligned or can be aligned to the requested
	 * Track.  False, if the points cannot be moved.
	 */
	public boolean requestAlignment(final Track trk) {
		int edge = trk.getDestination(MyEdge);
		ArrayList<RouteReference> list = SharedDecoders[edge];
		if ((list == null) || !DecoderInterlock.TheInterlockType.getFlagValue()) {
			if (!isDispatcherControlAllowed()) {
				return false;
			}
			prepareForMovement(edge);
		}
		else {
			// the first thing is to do a safety check of all sharing points
			for (RouteReference r : list) {
				if (!r.Edge.isDispatcherControlAllowed()) {
					return false;
				}
			}
			for (RouteReference r : list) {
				r.Edge.prepareForMovement(r.TrackNumber);
			}
		}
		CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.SWITCHPOSITION, String.valueOf(trk.getDestination(MyEdge)));
		return true;
	}

  static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
      OSEdge.class.getName());
}
/* @(#)OSEdge.java */
