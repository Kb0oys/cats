/* Name: RouteEdge.java
 *
 * What:
 *   This interface is for those SecEdges that compose a Route (CPEdge, PtsEdge,
 *   FrogEdge, XBlkEdge, and Xedge).  The edges are used for finding the ending
 *   signal in a Route and for constructing the Route.
 */
package cats.layout.items;

import java.util.BitSet;
import java.util.EnumSet;

import cats.layout.ctc.RouteNode;

/**
 *   This interface is for those SecEdges that compose a Route (CPEdge, PtsEdge,
 *   FrogEdge).  The edges are used for finding the ending
 *   signal in a Route and for constructing the Route.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2020, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface RouteEdge {

	/**
	 * the status of the Edge in searching for the ending signal to the route
	 */
	public enum RouteSearchStatus {
		UNSEARCHED,	// tracks in advance to the Edge have not been traversed
		SEARCHING,	// tracks in advance are being searched (needed for loop detection)
		DEADEND,	// been searched without finding the desired Signal
		SUCCESS		// destination found on this path
	}
	
	/**
	 * tells the edge that a search is beginning on its advance tracks.  The
	 * searchID is used for determining if this Route Edge has been encountered
	 * in this search generation.
	 * 1. If the desired CPEdge is found in the advance tracks (either as an
	 *  opposing CPEdge or as the next CPEdge in the same direction of travel),
	 *  the search status is set to SUCCESS and null is returned.
	 * 2. If there is no CPEdge found in advance, the search status is
	 * 	set to DEADEND and null is returned.
	 * 3. Finally, if the desired CPEdge is not found as an opposing edge and
	 * 	is not the next CPEdge, the search status is set to SEARCHING and the
	 * 	next CPEdge is returned, so that the search can continue. 
	 * @param destination is the CPEdge in advance of this RouteEdge being
	 * searched for
	 * @param searchID is the search generation number, used to detect out of date results
	 * @return null if the search ends at this RouteEdge and non-null for
	 * the next Route Edge in advance
	 */
	public RouteEdge startSearch(final CPEdge destination, final int searchID);
	
	/**
	 * queries the ID of the last search.  It is used to determine if the status
	 * is stale or not.  If the ID is the same as the current search ID, then
	 * the status is valid; otherwise, the ID will be updated, which sets the status
	 * to USEARCHED.
	 * @return the ID of the last search
	 */
	public int getSearchID();
	
	/**
	 * sets the search status
	 * @param status is the new search status
	 */
	public void setSearchStatus(final RouteSearchStatus status);
	
	/**
	 * queries the edge for status of the last search
	 * @return the status
	 */
	public RouteSearchStatus getSearchStatus();
	
	/**
	 * creates a RouteNode appropriate for the enclosed RouteEdge.
	 * This makes the RouteEdge a RouteNode factory.  If the RouteNode
	 * already exists, this must just return it.
	 * @param nextNode is the next RouteNode in the DAG of nodes
	 * @param searchID is the a search identifier.  It is incremented on each search.
	 * @return a tailored RouteNode
	 */
	public RouteNode createNode(final RouteNode nextNode, final int searchID);
	
	/**
	 * queries the edge for the search node
	 * @return the search node, which may be null;
	 */
	public RouteNode getNode();
	
	/**
	 * removes a RouteNode from the RouteEdge's search link.  If
	 * the search link contains the node, the search link is cleared;
	 * otherwise, nothing happens.
	 * @param node is a node possibly in the search link.  It should not be null.
	 */
	public void releaseNode(final RouteNode node);
	
	/**
	 * this method sets/clears the StackedRoute flag, which is used by the TrackEdge
	 * to inform the StackOfRoutes that a RouteEdge on a potential route had
	 * a blocking condition clear.
	 * @param stackWaiting is true to indicate that clearing locks should trigger
	 * StackOfRoutes and false to indicate the RouteEdge is not in a potential
	 * route.
	 * @param NodeSet is the set of all RouteEdges.  Registering a node for stacking involves
	 * setting the node's index in the set.  NodeSet must not be null.
	 */
	public void registerStackNode(final boolean stackWaiting, BitSet NodeSet);

	/**
	 * traverses tracks from the RouteEdge over tracks in advance, looking for
	 * certain blocking conditions.  The LocalLocks on each TrackEdge encountered
	 * are tested for the blocking condition.  Traversal stops at the next
	 * RouteEdge or end of track.
	 * @param blockingConditions is an EnumSet of GuiLocks, containing the locks
	 * of interest.
	 * @return true if any TrackEdge Local Locks contains any of blockingConditions;
	 * otherwise, it returns false.
	 */
	public boolean isRouteSegmentBlocked(EnumSet<GuiLocks> blockingConditions); 
	
	
	/**
	 * retrieves the number of times the associated Block has received
	 * an occupancy report
	 * @return a count of occupancy reports
	 */
	public long getOccupancyCount();
	
	/**
	 * retrieves the index into a Route BitSet of the node
	 * @return the index
	 */
	public int getRouteStatusIndex();

}
/* @(#)RouteEdge.java */
