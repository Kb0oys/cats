/* Name: CatsReporter.java
 *
 * What:
 *  This is a class for holding the information about a JMRI Reporter.
 *  It is based on the CATS Detector class.
 **/
package cats.layout.items;

import java.util.ArrayList;

import org.jdom2.Element;

import cats.automatedTests.TestMaker;
import cats.layout.xml.XMLEleFactory;
import cats.layout.xml.XMLEleObject;
import cats.layout.xml.XMLReader;

/**
 *  This is a class for holding the information about a JMRI Reporter.
 *  It is based on the CATS Detector class.
 * <ul>
 * <li>its IOSpec
 * <li>if the IOSpec reports an occupied or unoccupied event
 * </ul>
 * This class is needed only for tagging an IOSpec read from an XML document.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2012, 2015</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class CatsReporter implements XMLEleObject {
  /**
   * is the XML tag.
   */
  private String XmlTag;

  /**
   * is the XML tag for identifying a Reporter stimulus in a test recording
   */
  public static final String XML_TAG = "CATSREPORTER";
  
  /**
   * is the XML tag in a test case for the username on the CatsReporter
   */
  private static final String USERNAME = "USERNAME";
  
  /**
   * is the XML tag in a test case for the value to write into a reporter
   */
  private static final String VALUE = "VALUE";
  
  /**
   * is the ReporterSpec being wrapped.
   */
  private ReporterSpec MySpec;

  /**
   * the list of CatsReporters created
   */
  private static ArrayList<CatsReporter> ReporterKeeper = new ArrayList<CatsReporter>();
  
  /**
   * the constructor.
   *
   * @param tag is the XML tag for identifying what the ReporterSpec is for.
   */
  public CatsReporter(String tag) {
    XmlTag = tag;
    ReporterKeeper.add(this);
  }

  /**
   * returns the ReporterSpec encapsulated.
   *
   * @return the ReporterSpec.  null is not valid.
   */
  public ReporterSpec getSpec() {
    return MySpec;
  }

  /**********************************************************************************
   * the following are used in automated testing
   **********************************************************************************/
  /**
   * searches for a CatsReporter by its user name.
   * @param userName is the user name field
   * @return the reporter (if found) or null
   */
  private static CatsReporter findReporter(String userName) {
	  for (CatsReporter reporter : ReporterKeeper) {
		  if (reporter.getSpec().getReporter().getUserName().equals(userName)) {
			  return reporter;
		  }
	  }
	  return null;
  }
  
  
  /**
   * records a test case for writing to a CatsReporter
   * @param userName is the user name on the reporter
   * @param value is the value written to the reporter
   */
  public static void recordTestCase(String userName, String value) {
	  if (TestMaker.isRecording()) {
		  Element testCase = new Element(XML_TAG);
		  testCase.setAttribute(USERNAME, userName);
		  testCase.setAttribute(VALUE, value);
		  TestMaker.recordTrigger(testCase, "set reporter " + userName + " to " + value);
	  }
  }
  
  /**
   * is invoked to write to a Reporter to simulate a test stimulus
   * @param element describes the Reporter and what to write
   * @return null if the Reporter can be found and an error string if not
   */
  public static String processReporterElement(Element element) {
	  String userName = element.getAttribute(USERNAME).getValue();
	  CatsReporter reporter = findReporter(userName);
	  if (reporter == null) {
		  return new String ("could not find CatsReporter with user name " + userName);
	  }
	  reporter.getSpec().getReporter().setReport(element.getAttributeValue(VALUE));
	  return null;
  }
  
  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    return new String(XmlTag + " XML elements cannot not have text values ("
                      + eleValue + ").");
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptable or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    String resultMsg = null;
    if (ReporterSpec.XML_TAG.equals(objName)) {
      MySpec = (ReporterSpec) objValue;
    }
    else {
      resultMsg = new String(objName + " is not a valid embedded object in a"
                             + XmlTag + " XML element.");
    }
    return resultMsg;
  }

  /**
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return XmlTag;
  }

  /**
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error checking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    if (MySpec == null) {
      return new String("Missing " + ReporterSpec.XML_TAG + " element in a " +
                        XmlTag + " XML element.");
    }
    return null;
  }

  /**
   * registers a DetectorFactory with the XMLReader.
   * @param tag is the XML name for the class of Detector.
   */
  static public void init(String tag) {
    XMLReader.registerFactory(tag, new ReporterFactory(tag));
  }

}

/**
 * is a Class known only to the CatsReporter class for creating Reporters from
 * an XML document.
 */
class ReporterFactory
    implements XMLEleFactory {

  /**
   * is the XML element tag to register and apply to objects created.
   */
  private String ReporterTag;

  /**
   * is the factory constructor.
   *
   * @param tag is the XML element tag for Reporters created by this
   * factory.
   */
  public ReporterFactory(String tag) {
    ReporterTag = tag;
  }

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    return new String(ReporterTag + " XML elements cannot have an attribute.");
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    return new CatsReporter(ReporterTag);
  }

}
/* @(#)CatsReporter.java */