/* Name: CatsSignalSystem.java
 *
 * What:
 *   This class is a JMRI SignalSystem (used in conjunction with JMRI
 *   SignalMasts) for CATS.  It extends the JMRI DefaultSignalSystem.
 *   <p>
 *   It does not need all the features of the JMRI SignalSystem (e.g.
 *   images).  Those unnecessary features are left undefined or defaulted.
 *   <p>
 *   One of these is needed for each unique set of appearance names.
 *   In JMRI, it connects the physical signal attributes to the panel signal
 *   presentation.  It seems to be used for holding the speed protected by
 *   a SignalMast.  It is needed because the JMRI SystemName of a SignalMast
 *   contains the SignalSystem name and the appearance map name.
 */
package cats.layout.items;

import java.util.ArrayList;
import javax.swing.JOptionPane;

import cats.layout.AspectMap;
import jmri.managers.DefaultSignalSystemManager;

/**
 *   This class is a JMRI SignalSystem (used in conjunction with JMRI
 *   SignalMasts) for CATS.  It extends the JMRI DefaultSignalSystem.
 *   <p>
 *   It does not need all the features of the JMRI SignalSystem (e.g.
 *   images).  Those unnecessary features are left undefined or defaulted.
 *   <p>
 *   One of these is needed for each unique set of appearance names.
 *   In JMRI, it connects the physical signal attributes to the panel signal
 *   presentation.  It seems to be used for holding the speed protected by
 *   a SignalMast.  It is needed because the JMRI SystemName of a SignalMast
 *   contains the SignalSystem name and the appearance map name.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class CatsSignalSystem extends jmri.implementation.DefaultSignalSystem {
	
	/**
	 * the root of the SignalSystem name
	 */
	static public final String CatsName = "CATS";
	
	/**
	 * the key for finding the speed
	 */
	static public final String SPEED = "speed";
	
	/**
	 * a counter for generating unique SignalSystem names
	 */
	static private int UniqueID = 1;
	
	/**
	 * the registry of CatsSignalSystems
	 */
	static private ArrayList<CatsSignalSystem> Systems = new ArrayList<CatsSignalSystem>(1);
	
	/**
	 * the aspect names in the SignalSystem.  There is one entry for each aspect
	 * the CATS uses.
	 */
	private String Keys[];

	private CatsSignalSystem(String systemName) {
		this(systemName, systemName);
	}

	/**
	 * 
	 * @param systemName the JMRI system name for the SignalSystem
	 * @param userName the JMRI user name, which is the same as the system name
	 */
	private CatsSignalSystem(String systemName, String userName) {
		super(systemName, userName);
		Systems.add(this);
//		DefaultSignalSystemManager dssm = (DefaultSignalSystemManager)jmri.InstanceManager.signalSystemManagerInstance();
		DefaultSignalSystemManager dssm = (DefaultSignalSystemManager)jmri.InstanceManager.getDefault(jmri.SignalSystemManager.class);
		if (dssm == null) {
			JOptionPane.showMessageDialog(null, "JMRI DefaultSignalSystemManager does not exist");
			log.fatal("JMRI DefaultSignalSystemManager does not exist");
		}
		else {
			dssm.register(this);
		}
	}

    /**
     * searches the known CatsSignalSystems for one with matching keys
     * @param keys is the array of aspect keys of an SignalAppearanceMap
     * @return an existing CatsSignalSystem, if it exists
     */
    static public CatsSignalSystem getSignalSystem(final String keys[]) {
    	boolean match;
    	for (CatsSignalSystem css : Systems) {
    		match = true;
    		for (int a = 0; a < css.Keys.length; a++) {
    			if (!keys[a].equals(css.Keys[a])) {
    				match = false;
    				break;
    			}
    		}
    		if (match ) {
    			return css;
    		}
    	}
    	return null;
    }
    
    /**
     * searches the registry for an existing SignalSystem with the same keys
     * as requested.  If one does not exist, it is created and the aspects
     * and speeds are added.  Aspects and speeds are position sensitive.  Their
     * index in the speed array determines the speed.
     * @param keys is the list of aspect names in the system
     * @return a CatsSignalsystem that meets the requirements
     */
    static public CatsSignalSystem createSignalSystem(final String keys[]) {
    	boolean match;
    	String name;
    	CatsSignalSystem css;
//    	CatsSignalSystem css = getSignalSystem(keys);
//    	if (css != null) {
//    		return css;
//    	}
    	name = CatsName + (UniqueID++);
    	css = new CatsSignalSystem(name, name);
    	css.Keys = keys;

    	for (int speed = 0; speed < AspectMap.Speed.length; speed++) {
    		// this checks for duplicates
    		match = false;
    		for (int s = 0; s < speed; s++) {
    			if (keys[s].equals(keys[speed])) {
    				match = true;
    				break;
    			}
    			if (!match) {
    				css.setProperty(keys[speed], SPEED, AspectMap.Speed[speed]);
    			}
    		}
    	}
    	return css;
    }
    
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CatsSignalSystem.class.getName());
}
/* @(#)CatsSignalSystem.java */
