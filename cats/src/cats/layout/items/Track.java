/* Name: Track.java
 *
 * What:
 *   This class is the container for all the information about a piece of
 *   Track.  A piece of Track (track segment) does not have a counterpart in
 *   the prototype.  IT CATS, it performs two functions:
 *   <ul>
 *   <li>it ties the two ends together, which could be vital circuits,
 *   relaying to them the changes in the enclosing Block
 *   <li>provides an icon on the panel, which can be painted and selected
 *   </ul>
 */

package cats.layout.items;

import cats.apps.Crandic;
import cats.common.Constants;
import cats.common.DebugBits;
import cats.common.Prop;
import cats.common.Sides;
import cats.gui.DirectionArrow;
import cats.gui.GridTile;
import cats.gui.frills.rails.*;
import cats.layout.ColorList;
import cats.layout.vitalLogic.InternalXVitalLogic;
import cats.layout.xml.*;

import java.awt.Point;
import java.util.EnumSet;

import org.jdom2.Element;

/**
 *   This class is the container for all the information about a piece of
 *   Track.  A piece of Track (track segment) does not have a counterpart in
 *   the prototype.  IT CATS, it performs two functions:
 *   <ul>
 *   <li>it ties the two ends together, which could be vital circuits,
 *   relaying to them the changes in the enclosing Block
 *   <li>provides an icon on the panel, which can be painted and selected
 *   </ul>
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2009, 2011. 2012, 2014, 2015, 2020, 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class Track
implements XMLEleObject {
  /**
   *   The XML tag for recognizing the Track description.
   */
  public static final String XML_TAG = "TRACK";

  /**
   * The XML attribute tag for identifying the authorized speed.
   */
  public static final String SPEED_TAG = "SPEED";

  /**
   * The 4 authorized speeds for a Track segment, ordered by fastest
   * to slowest.  It is also used in determining a signal's indication.
   */
  public static final String[] TrackSpeed = {
      "Default",
      "Normal",
      "Limited",
      "Medium",
      "Slow",
      "None",
      "Approach"
  };

  /**
   * the index of "Default"
   */
  public static final int DEFAULT = 0;

  /**
   * the index of "Normal"
   */
  public static final int NORMAL = 1;

  /**
   * the index of "Limited"
   */
  public static final int LIMITED = 2;

  /**
   * the index of "Medium"
   */
  public static final int MEDIUM = 3;

  /**
   * the index of "Slow"
   */
  public static final int SLOW = 4;

  /**
   * the index of "None"
   */
  public static final int NONE = 5;
  
  /**
   * the index of "Approach" speed
   */
  public static final int APPROACH = 6;

  /**
   * Though not in the list, a signal may be showing Restricting.
   */
  public static final int RESTRICTING = TrackSpeed.length;
  
  /**
   * Though not in the list, a signal may be showing Stop and Proceed.
   */
  public static final int STOP_AND_GO = RESTRICTING + 1;
  /**
   * Though not in the list, a signal may be showing STOP.
   */
  public static final int  STOP = STOP_AND_GO + 1;

  /**
   * the speed through the Track
   */
  private int MySpeed;
  
  /**
   * The names of the 6 type of Tracks
   */
  public static final String TrackName[] = {
    "VERTICAL", // TOP-BOTTOM
    "HORIZONTAL", // RIGHT-LEFT
    "UPPERSLASH", // LEFT-TOP
    "LOWERSLASH", // RIGHT-BOTTOM
    "UPPERBACKSLASH", // RIGHT-TOP
    "LOWERBACKSLASH" // LEFT-BOTTOM
  };

  /**
   * the SecEdges that are the ends of each of the above Track Types.
   */
  protected static final int Termination[][] = {
    {
      Sides.BOTTOM, Sides.TOP}
    , {
      Sides.RIGHT, Sides.LEFT}
    , {
      Sides.LEFT, Sides.TOP}
    , {
      Sides.RIGHT, Sides.BOTTOM}
    , {
      Sides.RIGHT, Sides.TOP}
    , {
      Sides.BOTTOM, Sides.LEFT}
  };

  /**
   * The index of vertical track.
   */
  public static final int VERTICAL = 0;

  /**
   * The index of horizontal track.
   */
  public static final int HORIZONTAL = 1;

  /**
   * is the Name of the kind of Track.
   */
  protected String TrackString;

  /**
   * the kind of Track.
   */
  protected int TrackType;

  /**
   * the SecEdges at each end of the Track
   */
  protected SecEdge[] TrackEnds;

  /**
   * the TrackEdges protecting a crossing.  These are not real SecEdges,
   * but virtual ones, appearing only on the TrackEdge chain.  These are not
   * instantiated on every Track, but only those involved in crossings.
   * A better architecture is to derive an Xtrack object containing just
   * the internal edges, but the XML structure makes it hard to identify
   * a crossing until after the Tracks are instantiated, then it is hard
   * to replace the non-Xtracks.
   */
  private InternalXEdge InternalEdge[];

  /**
   * the Block that the Track belongs to.
   */
  protected Block TrackBlock;

  /**
   * the GridTile where the Track is shown.
   */
  private GridTile TrkTile;

  /**
   * the Frill that shows the Track.
   */
  private RailFrill TrackFrill;

  /**
   * the conditions that force the color of the track.  There will always be DARK_TERRITORY.
   */
  private EnumSet<TrackColor> ColorState = EnumSet.of(TrackColor.DARK_TERRITORY);
  
  /*************************************************************************
   * the following are used primarily for automated testing
   *************************************************************************/
  /**
   * the name of the last color used to paint the track
   */
  private String LastColor;
  
  /**
   * the name of the last color recorded
   */
  private String RecordedLastColor;
  
  /**
   * the state last recorded
   */
  private EnumSet<TrackColor> RecordedTrkState;
  
  /**
   * constructs the Track from scratch.
   *
   * @param speed is the timetable track speed.
   */
  public Track(int speed) {
    TrackEnds = new SecEdge[2];
    MySpeed = speed;
  }

  /**
   * returns the prescribed track speed.
   *
   * @return the authorized maximum speed over the track.
   */
  public int getSpeed() {
    return MySpeed;
  }

  /**
   * sets the prescribed track speed.  This is used to provide a speed
   * when the "default" value is read in.
   *
   * @param speed is the new speed.
   */
  public void setSpeed(int speed) {
    if ( (speed > 0) && (speed < TrackSpeed.length)) {
      MySpeed = speed;
    }
  }

  /**
   * is called to determine which of two speeds is slower.
   *
   * @param speed1 is one speed
   * @param speed2 is the other
   *
   * @return the slower of the two, based on the Speed constants, above.
   */
  public static int getSlower(int speed1, int speed2) {
    if ( (speed1 < DEFAULT) || (speed1 > STOP)) {
      return speed2;
    }
    if ( (speed2 < DEFAULT) || (speed2 > STOP)) {
      return speed1;
    }
    return (speed1 < speed2) ? speed2 : speed1;
  }

  /**
   * returns the Section that the Track is in
   * @return the Section the the first non-null end is in
   */
  public Section getSection() {
	  if ((TrackEnds[0] != null)  && (TrackEnds[0].getSection() != null)) {
		  return TrackEnds[0].getSection();
	  }
	  else if ((TrackEnds[1] != null)  && (TrackEnds[1].getSection() != null)) {
		  return TrackEnds[1].getSection();
	  }
	  return null;
  }
  
  /**
   * returns the Track's ends.
   * @return an a 2 element array, listing which SecEdges the
   * track touches.
   */
  public int[] getTrackEnds() {
    return Termination[TrackType];
  }

  /**
   * returns the SecEdge on one end of the Track.  The SecEdge is normally
   * static (fixed at creation time), except for points.  The edge changes
   * based on the points alignment.
   *
   * @param e is the index of the grid edge
   *
   *  @return the SecEdge currently associated with one end of the
   *  track or null, if it cannot be found.
   */
  public SecEdge getEnd(int e) {
    if (TrackEnds[0].getEdge() == e) {
      return TrackEnds[0];
    }
    if (TrackEnds[1].getEdge() == e) {
      return TrackEnds[1];
    }
    return null;
  }

  /**
   * finds which end (of the two) the SecEdge desired is.
   *
   * @param edge should be one of the two Terminations.
   *
   * @return the index to the end or -1 if it isn't one of the two.
   */
  private int findEdge(SecEdge edge) {
    if (TrackEnds[0] == edge) {
      return 0;
    }
    else if (TrackEnds[1] == edge) {
      return 1;
    }
    return -1;
  }

  /**
   * determines if the caller is one termination of the track.  If so,
   * it returns the other end.
   * @param caller is the SecEdge wanting to know the edge
   * at the other end.
   *
   * @return the SecEdge at the other end, if the caller is one termination;
   * otherwise, null.
   *
   * @see cats.layout.items.SecEdge
   */
  public SecEdge getDestination(SecEdge caller) {
    int index = caller.getEdge();
    if (TrackEnds[0].getEdge() == index) {
      return TrackEnds[1];
    }
    if (TrackEnds[1].getEdge() == index) {
      return TrackEnds[0];
    }
    return null;
  }

  /**
   * determines if the caller is one termination of the track.  If so,
   * it returns the Edge of the other end.
   * @param caller is the Edge wanting to know the Edge
   * at the other end.
   *
   * @return the Edge at the other end, if the caller is one termination;
   * otherwise, -1.
   *
   * @see cats.layout.items.SecEdge
   */
  public int getDestination(int caller) {
    if (TrackEnds[0].getEdge() == caller) {
      return TrackEnds[1].getEdge();
    }
    if (TrackEnds[1].getEdge() == caller) {
      return TrackEnds[0].getEdge();
    }
    return -1;
  }
  
  /**
   * determines if the caller is one termination of the track.  If so,
   * it returns the Side of the other termination.
   * @param caller is the SecEdge wanting to know where the
   * other end is.
   *
   * @return the Side of the other end, if it exists.
   *
   * @see cats.common.Sides
   */
  public int getTermination(SecEdge caller) {
    if (TrackEnds[0] == caller) {
      return Termination[TrackType][1];
    }
    if (TrackEnds[1] == caller) {
      return Termination[TrackType][0];
    }
    return -1;
  }

  /**
   * determines which SecEdge is attached to the "other" end of the Track.
   * This is similar to getDestination except, not only must the "other"
   * end exist, but if it is a PtsEdge, the points must be aligned for
   * this Track.  Thus, a search cannot be made by comparing the SecEdge, but
   * by comparing the edge number.
   * @param caller is the edge where traversal begins.
   *
   * @return the SecEdge at the other end, if the caller is one termination
   * and it is aligned for the Edge; otherwise, return null;
   *
   * @see cats.layout.items.SecEdge
   */
  public SecEdge traverse(SecEdge caller) {
    int index = caller.getEdge();
    int other = -1;
    if (TrackEnds[0].getEdge() == index) {
      other = 1;
    }
    else if (TrackEnds[1].getEdge() == index) {
      other = 0;
    }
    if ( (other != -1) && !TrackEnds[other].isFoulingEdge()) {
      return TrackEnds[other];
    }
    return null;
  }

  /**
   * is called to change the gap on one end of the track because the
   * points are fouled.
   * @param edge is the the SecEdge - the one being fouled or cleared
   * @param fouled is true if the edge is fouling or false if it is clear
   */
  public void setGap(int edge, boolean fouled) {
    if (TrackFrill != null) {
      TrackFrill.setGap(edge, fouled);
	  TrkTile.requestUpdate();
	  GridTile.doUpdates();
    }
  }
  
  /**
   * possibly changes the Track's color by adding or removing a condition.
   * @param reason is the reason (color)
   * @param addRemove is true to add a reason or false to remove a reason
   */
  public void alterColor(TrackColor reason, boolean addRemove) {
	  if (addRemove) {
		  ColorState.add(reason);
	  }
	  else {
		  ColorState.remove(reason);
	  }
	  updateTrackColor();
  }

  /**
   * is invoked to determine the highest priority condition on the track
   * and update its color, if needed.
   */
  private void updateTrackColor() {
	  String priorityColor = ColorList.DARK;
	  if (TrackFrill != null) {
		  if ((ColorState.contains(TrackColor.TRACK_N_TIME) || ColorState.contains(TrackColor.OUT_OF_SERVICE))
				  && ColorState.contains(TrackColor.OCCUPIED)) {
			  priorityColor = TrackColor.OCC_TNT.getColor();
		  }
		  else {
			  for (TrackColor tc : ColorState) {
				  priorityColor = tc.getColor();
				  break;
			  }
		  }
		  if (!priorityColor.equals(LastColor)) {
				/******************  DEBUG code ******************************/
				if (Crandic.Details.get(DebugBits.TRACKCOLORBIT)) {
					System.out.println("Track " + toString() + " changed from " + LastColor + " to " + priorityColor);			
				}
				/****************** end DEBUG code ******************************/
			  LastColor = priorityColor;
			  TrackFrill.setColor(LastColor);
			  TrkTile.requestUpdate();
			  GridTile.doUpdates();
		  }
	  }	  
  }
  
  /**
   * sets the SecEdges on the ends of the Track.  The edges are set to
   * line up as in Termination, for ease of later reference.
   *
   * @param edge1 is one edge.
   * @param edge2 is the other
   */
  public void setEdges(SecEdge edge1, SecEdge edge2) {
    boolean valid = false;
    int edge = edge1.getEdge();
    if (Termination[TrackType][0] == edge) {
      TrackEnds[0] = edge1;      
      if (Termination[TrackType][1] == edge2.getEdge()) {
        valid = true;
      }
      TrackEnds[1] = edge2;
    }
    else if (Termination[TrackType][1] == edge) {
      TrackEnds[1] = edge1;
      if (Termination[TrackType][0] == edge2.getEdge()) {
        valid = true;
      }
      TrackEnds[0] = edge2;
    }
    else {
      TrackEnds[0] = edge1;
      TrackEnds[1] = edge2;
    }
    edge1.setTrack(this);
    edge2.setTrack(this);

    if (InternalEdge != null) {
    	// this cross connects the crossing edge and hard edge
    	// it must be called after setTrack, because the latter sets Peer
	  InternalEdge[0].Peer = TrackEnds[0];
	  TrackEnds[0].Peer = InternalEdge[0];
	  InternalEdge[1].Peer = TrackEnds[1];
	  TrackEnds[1].Peer = InternalEdge[1];
    }

    if (!valid) {
      Point pt = edge2.getSection().getCoordinates();
      log.warn("One end for a Track in Section (" + pt.x + "," +
          pt.y + ") does not terminate on the correct edge.");
    }
  }

  /**
   * replaces the SecEdge at one end of the track.  This method is
   * invoked by Double Crossings to replace a XEdge with one of its
   * component SecEdges.
   * @param oldEdge is the SecEdge (XEdge) being replaced
   * @param newEdge is the replacement SecEdge
   */
  public void replaceEdge(SecEdge oldEdge, SecEdge newEdge) {
    if (TrackEnds[0] == oldEdge) {
      TrackEnds[0] = newEdge;
    }
    else if (TrackEnds[1] == oldEdge) {
      TrackEnds[1] = newEdge;
    }
    else {
      log.warn("Double Crossing is malformed");
    }
    TrackEnds[0].setTrack(this);
    TrackEnds[1].setTrack(this);
//    oldEdge.unsetTrack(TrkState);
  }

  /**
   * sets the Block that encompasses the Track.  A side affect of telling
   * the Track what Block it is, is that it registers itself with that
   * Block.
   *
   * @param block is the Block.
   */
  public void setBlock(Block block) {
    if (TrackBlock == null) {
      TrackBlock = block;
      TrackEnds[0].setBlock(block);
      TrackEnds[1].setBlock(block);
      if (InternalEdge != null) {
    	  InternalEdge[0].setBlock(block);
    	  InternalEdge[1].setBlock(block);
      }
      block.registerTrack(this);
      if (!block.isDarkTerritory()) {
    	  ColorState.add(TrackColor.IDLE);
    	  updateTrackColor();
      }
    }
    else if (TrackBlock != block) {
      Point pt = TrackEnds[0].getSection().getCoordinates();
      log.warn("A track in Section (" + pt.x + "," + pt.y +
      ") has multiple Block definitions.");
    }
  }

  /**
   * adds or removes the traffic arrowhead.
   *
   * @param arrow is true if the arrowhead should be added and false if it
   * should be removed.
   * @param edge is the BlkEdge making the request.
   */
  public void setArrow(boolean arrow, SecEdge edge) {
    if (DirectionArrow.TheArrowType.getFlagValue() && (TrackFrill != null)) {
      int e = findEdge(edge);
      if (arrow) {
        TrackFrill.setDOT(Termination[TrackType][e]);
      }
      else {
        TrackFrill.setDOT(RailFrill.NO_DOT);
      }
      TrkTile.requestUpdate();
      GridTile.doUpdates();
    }
  }

  /**
   * clears the history of any associated signals.
   */
  public void clrTrkHistory() {
    if (TrackEnds[0] != null) {
      TrackEnds[0].clrEdgeHistory();
    }
    if (TrackEnds[1] != null) {
      TrackEnds[1].clrEdgeHistory();
    }
  }

  /**
   * returns the Block the Track is contained in.
   *
   * @return the Block.  It should not be null, but if the layout is not
   * configured correctly, it will be.
   */
  public Block getBlock() {
    return TrackBlock;
  }

  /**
   * adds the icon for the Track to the GridTile
   * @param tile is the Tile being decorated.
   */
  public void install(GridTile tile) {
    TrkTile = tile;
    if (tile != null) {
      switch (TrackType) {
      case 0:
        TrackFrill = new VertRailFrill();
        break;

      case 1:
        TrackFrill = new HorzRailFrill();
        break;

      case 2:
        TrackFrill = new UpperSlashFrill();
        break;

      case 3:
        TrackFrill = new LowerSlashFrill();
        break;

      case 4:
        TrackFrill = new UpperBackFrill();
        break;

      case 5:
        TrackFrill = new LowerBackFrill();
        break;

      default:
        Point pt = TrackEnds[0].getSection().getCoordinates();
        log.warn("Unknown Track type in Section (" + pt.x + "," +
            pt.y + ").");
      }
      if (TrackFrill != null) {
        TrackFrill.setTrack(this);
        tile.addFrill(TrackFrill);
        if ((TrackEnds[0] != null) && TrackEnds[0].isFoulingEdge()) {
          TrackFrill.setGap(Termination[TrackType][0], true);
        }
        if ((TrackEnds[1] != null) && TrackEnds[1].isFoulingEdge()) {
          TrackFrill.setGap(Termination[TrackType][1], true);
        }
        updateTrackColor();
      }
    }
  }
  
  /********************************************************************************
   * the following implement crossings.
   ********************************************************************************/
  /**
   * creates half a crossing, internal to the Track.
   */
  public void createCrossing() {
	  // 1. create the crossing InternalXEdges
	  InternalEdge = new InternalXEdge[2];
	  InternalEdge[0] = new InternalXEdge(Termination[TrackType][0]);
	  InternalEdge[1] = new InternalXEdge(Termination[TrackType][1]);
	  
	  // 2. make them mutual adjacent
	  InternalEdge[0].setJoint(InternalEdge[1]);
	  InternalEdge[1].setJoint(InternalEdge[0]);
	  
	  // 3. set their tracks for traversal across the Section
	  InternalEdge[0].setDestination(this);
	  InternalEdge[1].setDestination(this);
	  
	  // 4. create the crossing Vital Logic
	  InternalEdge[0].MyLogic = new InternalXVitalLogic("Xing0", InternalEdge[0].getEdge());
	  InternalEdge[1].MyLogic = new InternalXVitalLogic("Xing1", InternalEdge[1].getEdge());
	  
	  // cannot cross link the edges until after the Section doneXML() fires
  }

  /**
   * completes the crossing by telling the crossing edges
   * about the crossing tracks
   * @param t is the crossing track
   */
  public void setCrossTrack(Track t) {
		InternalEdge[0].setBlockingLogic(t.InternalEdge);
		InternalEdge[1].setBlockingLogic(t.InternalEdge);	  
  }
  
  /**
   * prints the Track state to the console
   * @param leadin is aString for identifying which track is being dumped
   */
  public void dumpState(String leadin) {
    System.out.println(leadin + ColorState.toString());
  }

  /**
   * asks the Track to format any state changes since the last snap shot
   * into an XML Element.  This is used for recording the state changes for
   * establishing automated tests and in checking state changes when running
   * automated tests.
   * 
   * @param basic is true if only basic state changes are requested and false
   * for more detailed state information
   * 
   * @return an XML Element containing the changes since the last snapshot or
   * null if there were none
   */
  public Element dumpTrackState(boolean basic) {
    Element trkState = new Element(XML_TAG);
    boolean changed = false;
    if ((LastColor != null) && !LastColor.equals(RecordedLastColor)) {
      trkState.setAttribute("COLOR", LastColor);
      RecordedLastColor = LastColor;
      changed = true;
    }
    if (!basic && !ColorState.equals(RecordedTrkState)) {
      RecordedTrkState = ColorState.clone();
      trkState.addContent(RecordedTrkState.toString());
      changed = true;
    }
    if (changed) {
      trkState.setAttribute("TYPE", TrackString);
      return trkState;
    }
    return null;
  }

  @Override
  public String toString() {
	  SecEdge section;
	  if (((section = TrackEnds[0]) != null) && (section.getSection() != null)) {
		  return section.getSection().toString() + ":" + TrackName[TrackType];
	  }
	  return TrackName[TrackType];
  }
  
  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    TrackString = eleValue;
    return null;
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    String resultMsg = new String("A " + XML_TAG +
        " cannot contain an Element ("
        + objName + ").");
    return resultMsg;
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error checking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    String resultMsg = null;
    TrackType = Prop.findString(TrackString, TrackName);
    if (TrackType == Constants.NOT_FOUND) {
      resultMsg = new String(TrackType +
          " is not a valid XML text element for "
          + XML_TAG + ".");
    }
    return resultMsg;
  }

  /**
   * registers a TrackFactory with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new TrackFactory());
  }

  static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
      Track.class.getName());

}

/**
 * is a Class known only to the Track class for creating Tracks from
 * an XML document.
 */
class TrackFactory
implements XMLEleFactory {

  /**
   * is the authorized speed across the track.
   */
  private int Speed;

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    Speed = Track.DEFAULT;
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;
    if (Track.SPEED_TAG.equals(tag)) {
      if ( (Speed = Prop.findString(value, Track.TrackSpeed)) ==
        Constants.NOT_FOUND) {
        Speed = 0;
        resultMsg = new String(tag + " is not a recognized track speed.");
      }
    }
    else {
      resultMsg = new String(tag + " is not a valid XML Attribute for a " +
          Track.XML_TAG);
    }
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    return new Track(Speed);
  }
}
/* @(#)Track.java */