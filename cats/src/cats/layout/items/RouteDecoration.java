/**
/* Name: RouteDecoration.java
 *
 * What:
 * is a description of how to decorate/undecorate the tracks on the GUI
 * for displaying a route.
 * <p>
 * It has two pieces:
 * <ol>
 * <li>a color</li>
 * <li>a flag indicating if the color should be added or removed from the track</li>
 * </ol>
 */
package cats.layout.items;

/**
 * is a description of how to decorate/undecorate the tracks on the GUI
 * for displaying a route.
 * <p>
 * It has two pieces:
 * <ol>
 * <li>a color</li>
 * <li>a flag indicating if the color should be added or removed from the track</li>
 * </ol>
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2009, 2010, 2011, 2012, 2014, 2018</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public enum RouteDecoration {
	SET_ROUTE(new Attributes(TrackColor.ROUTE, true)),
	CLEAR_ROUTE(new Attributes(TrackColor.ROUTE, false)),
	SET_DTC_ROUTE(new Attributes(TrackColor.WARRANT, true)),
	CLEAR_DTC_ROUTE(new Attributes(TrackColor.WARRANT, false)),
	OCCUPY_ROUTE(new Attributes(TrackColor.OCCUPIED, true)),
	UNOCCUPY_ROUTE(new Attributes(TrackColor.OCCUPIED, false));
	
	RouteDecoration(Attributes attributes) {
		MyAttributes = attributes; 
	}
	
	/**
	 * the instantiation of the attribute
	 */
	private Attributes MyAttributes;
	
	/**
	 * the attribute accessor methods
	 */
	
	/**
	 * returns the track color
	 * @return the track color
	 */
	public TrackColor getTrackColor() {
		return MyAttributes.Color;
	}
	
	/**
	 * retrieves if the color should be added or removed
	 * @return true if it should be added
	 */
	public boolean getOnOff() {
		return MyAttributes.OnOff;
	}
}
/**
 * an internal container for the attributes of each decoration
 */
class Attributes {
	// the color of the track for this state
	final public TrackColor Color;
	// the action for this state - true to add the color and false to remove it
	final public boolean OnOff;
	
	public Attributes(TrackColor color, boolean onOff) {
		Color = color;
		OnOff = onOff;
	}
}

