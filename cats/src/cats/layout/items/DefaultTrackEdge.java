/* Name: DefaultTrackEdge.java
 *
 * What:
 *   This class is derived from AbstractTrackEdge by fleshing out the 
 *   abstract methods.  It also adds the codeline and some static
 *   variables for recording the things created.
 */
package cats.layout.items;

import java.util.ArrayList;

import cats.apps.Crandic;
import cats.common.Constants;
import cats.common.DebugBits;
import cats.layout.Discipline;
import cats.layout.codeLine.CodeMessage;
import cats.layout.codeLine.CodePurpose;
import cats.layout.codeLine.Codes;
import cats.layout.codeLine.packetFactory.EdgePacketFactory;
import cats.layout.codeLine.packetFactory.VitalPacketFactory;
import cats.layout.vitalLogic.BasicVitalLogic;
import cats.layout.vitalLogic.LogicLocks;

/**
 *   This class is derived from AbstractTrackEdge by fleshing out the 
 *   abstract methods.  It also adds the codeline and some static
 *   variables for recording the things created.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014, 2015, 2016, 2018, 2019, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class DefaultTrackEdge extends AbstractTrackEdge {

	/**
	 * the Code Line for talking to the VitalLogic
	 */
	protected VitalPacketFactory CodeLineEntry;

	/**************************************************************************
	 * the handlers for changes in field locks
	 **************************************************************************/

//	/**
//	 * the current direction of travel for a train on this edge
//	 */
//	protected DirectionOfTravel TrainDirection = DirectionOfTravel.NONE;

	/**
	 * a list of all TrackEdges
	 */
	private static ArrayList<TrackEdge> TrackEdgeKeeper = new ArrayList<TrackEdge>();

	/**
	 * a list of all SignalEdges
	 */
	private static ArrayList<SignalEdge> SignalEdgeKeeper = new ArrayList<SignalEdge>();

	public DefaultTrackEdge(int edge, Edge shared) {
		super(edge, shared);
		TrackEdgeKeeper.add(this);
	}

	@Override
	public boolean isBlkEdge() {
		return false;
	}

	/**
	 * is called to set a lock in VitalLogic through the code line.  This method
	 * constructs and sends the message, but does not record the event.  The Vital
	 * Logic will respond with a message that sets the internal lock.
	 * @param lock is the LogicLock being changed
	 * @param polarity is true to set the lock and false to clear it
	 */
	public void constructVitalLogicEvent(LogicLocks lock, boolean polarity) {
		CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.fromLogicLock(lock),
				(polarity) ? Constants.TRUE : Constants.FALSE);
	}

	/**
	 * is called to set a lock in VitalLogic through the code line.  Because no acknowledge
	 * is expected from the VitalLogic, the internal lock is set as well.
	 * @param lock is the LogicLock being changed
	 */
	public void setVitalLogicEvent(GuiLocks lock) {
		CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.fromGuiLock(lock), Constants.TRUE);
		setEdgeLock(lock);
	}

	/**
	 * is called to clear a lock in VitalLogic through the code line.  Because no acknowledge
	 * is expected from the VitalLoigc, the internal lock is cleared as well.
	 * @param lock is the LogicLock being changed
	 */
	public void clearVitalLogicEvent(GuiLocks lock) {
		CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.fromGuiLock(lock), Constants.FALSE);
		clearEdgeLock(lock);
	}

	/**************************************************************************
	 * the implementation of TrackEdge
	 **************************************************************************/
	@Override
	public BasicVitalLogic createVitalLogic() {
		return new BasicVitalLogic(new EdgePacketFactory(this), toString());
	}

	@Override
	public void discoverAdvanceVitalLogic() {
//		SecEdge neighbor = getNeighbor();
//		SecEdge nextEdge = Peer;
//		int speed = Track.NORMAL;
		// since "this" is a DefaultTrackEdge (and not FrogEdge or PtsEdge) the neighbor must
		// be a DefaultTrackEdge (TrackEdge) and not a FrogEdge or PtsEdge; thus, NeighborEdge
		// is the same as neighbor.
		NeighborEdge = (AbstractTrackEdge) getNeighbor();
		MyLogic.setIdentity(toString());
		MyLogic.tailorDiscipline(MyBlock.getDiscipline());
//		if ((neighbor != null) && !neighbor.MyBlock.getDiscipline().equals(Discipline.UNDEFINED)) {
//			NeighborEdge = (AbstractTrackEdge)neighbor;
//			MyLogic.setNeighborLogic(NeighborEdge.getVitalLogic());
//		}
		if ((NeighborEdge != null) && !NeighborEdge.MyBlock.getDiscipline().equals(Discipline.UNDEFINED)) {
			MyLogic.setNeighborLogic(NeighborEdge.getVitalLogic());
		}
		else {
			NeighborEdge = null;
			MyLogic.setNeighborLogic(null);
		}
		if (!MyBlock.getDiscipline().equals(Discipline.UNDEFINED)) {
//			do {
//				TrackSpan.add(nextEdge.Destination);
//				speed = Track.getSlower(speed, nextEdge.Destination.getSpeed());
//				if (nextEdge instanceof AbstractTrackEdge) {  // the normal case - the next TrackEdge in the opposite direction
//					OppositeEnd = (AbstractTrackEdge) nextEdge;
//					MyLogic.setPeerLogic(OppositeEnd.getVitalLogic());
//					nextEdge = ((TrackEdge)nextEdge).nextTrackEdge();
//					if ((nextEdge != null) && (nextEdge instanceof TrackEdge)) {
//						((TrackEdge) nextEdge).setApproachEdge(this);
//					}
//					nextEdge = null;
//				}
//				else {  // see if the neighbor is switch points
//					nextEdge = nextEdge.getNeighbor();
//					if (nextEdge != null) {
//						if (nextEdge instanceof PtsEdge) {
//							OppositeEnd = (AbstractTrackEdge) nextEdge;
//							MyLogic.setPeerLogic(OppositeEnd.getVitalLogic());
//							((TrackEdge) nextEdge).setApproachEdge(this);
//							nextEdge = null;
//						}
//						else {
//							nextEdge = nextEdge.Peer;
//						}
//					}
//				}
//			} while (nextEdge != null);
			MyLogic.setSpeed(trackSegmentAggregator(this));
		}
		else {
			MyLogic.setSpeed(Track.NORMAL);
		}
		if (OppositeEnd != null) {
			MyLogic.setPeerLogic(OppositeEnd.getVitalLogic());
		}
		CodeLineEntry = new VitalPacketFactory(MyLogic);	
	}

	/**
	 * This method is analogous to VitalLogicProbe, in that it is
	 * used to find the immediate signal in approach.  Since the linkages to
	 * the TrackEdge in approach are known, it traverses that chain to find
	 * the signal in approach, rather than running through all the SecEdges.
	 */
	public SignalEdge signalEdgeProbe() {
		if (ApproachEdge != null) {
			return ApproachEdge.signalEdgeProbe();
		}
		return null;
	}

	@Override
	public TrackEdge nextTrackEdge() {
		AbstractTrackEdge next = (AbstractTrackEdge) getNeighbor();
		if ((next != null) && !next.MyBlock.getDiscipline().equals(Discipline.UNDEFINED)) {
			return next;
		}
		return null;
	}

	/**
	 * retrieves the VitalLogic associated with the TrackEdge
	 * @return the VitalLogic
	 */
	public BasicVitalLogic getVitalLogic() {
		return MyLogic;
	}

//	@Override
//	public void setDirection(DirectionOfTravel dot) {
//		TrainDirection = dot;
//	}

	/**
	 * this method receives messages (responses and reports) from the field
	 * equipment and processes them
	 * @param response is the message from the field equipment
	 */
	public CodeMessage processFieldResponse(String response) {
		CodeMessage received = new CodeMessage(response);
		GuiLocks lock;
		/******************** DEBUG code *****************************/
		if (Crandic.Details.get(DebugBits.CODELINEBIT)) {
			System.out.println(toString() + " office received " + response);
		}
		/*************************************************************/
		if (received.Purpose.equals(CodePurpose.INDICATION)) {
			lock = Codes.toGuiLock(received.Lock);
			if (lock != null) {
				if (Boolean.parseBoolean(received.Value)) {
//					if (lock == GuiLocks.DETECTIONLOCK) {
//						if (!EdgeLocks.contains(lock)) {
//							MyBlock.setBlockDetectionOn();
//						}
//						// else do nothing- it has already been processed
//					}
//					else {
						setEdgeLock(lock);
//					}
				}
//				else if (lock == GuiLocks.DETECTIONLOCK) {
//					if (EdgeLocks.contains(lock)) {
//						MyBlock.setBlockDetectionOff();
//					}
//					// else do nothing- it has already been processed
//				}
				else {
					clearEdgeLock(lock);
				}
			}
		}
		return received;
	}

//	@Override
//	public void startFeeding() {
//		if (getVitalLogic() != null) {
//			getVitalLogic().primeApproach();
//		}		
//	}

	/**************************************************************************
	 *  end of TrackEdge implementation
	 **************************************************************************/

	/**************************************************************************
	 * The following are used to assist in linking the TrackEdges (and corresponding
	 * VitalLogics) together.
	 **************************************************************************/
	/**
	 * is a method for adding a TrackEdge that is not a descendant of DefaultTrackEdge
	 * to the list of TrackEdge objects.
	 * @param edge is the TrackEdge
	 */
	public static void addTrackEdge(TrackEdge edge) {
		TrackEdgeKeeper.add(edge);
	}

	/**
	 * is a method for adding a SignalEdge
	 * to the list of SignalEdge objects.
	 * @param edge is the SignalEdge
	 */
	public static void addSignalEdge(SignalEdge edge) {
		SignalEdgeKeeper.add(edge);
	}

	/**
	 * walks through the list of TrackEdges and SignalEdge to connect each to the one in approach,
	 * so it knows where to feed its state.
	 */
	public static void connectAllVitalLogic() {
		for (TrackEdge edge : TrackEdgeKeeper) {
			edge.discoverAdvanceVitalLogic();
		}
		for (TrackEdge edge : TrackEdgeKeeper) {
			edge.setHandlers();
			edge.getVitalLogic().configureLockProcessing();
		}
		for (SignalEdge edge : SignalEdgeKeeper) {
			edge.discoverApproachSignal();
		}
//		PtsEdge.initializePoints();
	}

	/**
	 * walks through the list of TrackEdges and requests that they trigger the associated
	 * VitalLogic to pass their state to the approach logic.
	 */
	public static void primeAllVitalLogic() {
		for (TrackEdge edge: TrackEdgeKeeper) {
			edge.startFeeding();
		}
	}
}
/* @(#)DefaultTrackEdge.java */