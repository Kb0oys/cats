/* Name: BlkEdge.java
 *
 * What:
 *   This class is the container for all the information about one
 *   of a Section's Edges which "is a" Block boundary.
 */

package cats.layout.items;

import cats.layout.Discipline;
import cats.layout.TrafficStickRelay;
import cats.layout.codeLine.packetFactory.EdgePacketFactory;
import cats.layout.vitalLogic.BasicVitalLogic;
import cats.layout.vitalLogic.BlkVitalLogic;
import cats.trains.Train;

import java.awt.Point;
import java.util.Enumeration;

/**
 * is a Block boundary, without a visible Signal.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2009, 2010, 2011, 2012, 2015, 2016, 2018, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class BlkEdge extends DefaultTrackEdge {

	/**
	 * is the Traffic Stick Relay, for determining the direction that a train
	 * is moving through the block
	 */
	protected TrafficStickRelay Tsr = new TrafficStickRelay();

	/**
	 * constructs a BlkEdge with only its Edge identifier.
	 *
	 * @param edge identifies the side of the GridTile the SecEdge is on.
	 * @param shared describes the shared edge, if it is not adjacent.
	 *    A null value means it is adjacent.
	 * @param block is the Block definition.  If it is null, then the
	 *   a default block is created, which may be overwritten during the
	 *   bind process.
	 */
	public BlkEdge(int edge, Edge shared, Block block) {
		super(edge, shared);
		if (block == null) {
			MyBlock = Block.BlockHolder;
		}
		else {
			block.registerEdge(this);
			MyBlock = block;
		}
	}

	@Override
	public EdgeInterface.EDGE_TYPE getEdgeType() {
		return EdgeInterface.EDGE_TYPE.BLOCK_EDGE;
	}
	
	/**
	 * is used to determine if a SecEdge is a Block boundary.
	 *
	 * @return true if it is and false if it is not.
	 */

	public boolean isBlock() {
		return true;
	}

	/**
	 * is used to determine if a SecEdge is on a HomeSignal or not.
	 *
	 * @return true if it is and false if it is not.  The boundaries of
	 * ABS Blocks are always Control Points because ABS does not support
	 * route reservations.
	 */
	public boolean allowsRoute() {
		return false;
	}

	/**
	 * is used to add Tracks to their Blocks.  In the XML file, only one
	 * SecEdge in a Block contains the Block definition.  This method is called
	 * for that Block, which sets the rest of the Track registration process
	 * in motion.  It tells each Track with a termination on the SecEdge
	 * which block the Track belongs to.  The Track, in turn, tells both termination
	 * ends which Block they belong to.  In addition to propagating the Block
	 * identity to the other end(s), it will echo back and try to tell this
	 * SecEdge which Block it is in.  Thus, the other form of getBlock()
	 * must stop the recursion.
	 */
	public void setBlock() {
		propagateBlock(MyBlock);
	}

	/**
	 * is called from a Track to tell its termination ends which Blocks
	 * they are in.  There are several cases to consider:
	 * <ol>
	 * <li> if the SecEdge isn't a Block boundary, then the Block identity
	 *      must be sent to all tracks terminating on the SecEdge and to
	 *      the shared SecEdge (if it exists) in a linked Section.
	 * <li> if the SecEdge is a Block boundary, then the propagating
	 *      is complete, because the shared SecEdge is in a different
	 *      Block.
	 * <li> if the SecEdge is a Block boundary and block is the same
	 *      as MyBlock, then the method call is an "echo" from a Track.
	 * <li> if the SecEdge is a Block boundary, and the block is not the
	 *      same, then if MyBlock does not define a discipline, then block
	 *      is the real Block, so it replaces MyBlock.
	 * <li> if the SecEdge is a Block boundary and the block is not the
	 *      same and MyBlock does define a discipline, then there is a
	 *      conflict.  The Blocks is defined differently at two ends.
	 * </ol>
	 * One other case can be ignored (MyBlock has a defined discipline and
	 * block doesn't) because Blocks with an undefined discipline are never
	 * entered into BlockKeeper.
	 * <p>
	 * setHandlers() is called in this method because the linkage to the
	 * neighbor must be established so that the correct handler can
	 * be selected.
	 *
	 * @param block is the Block identity being propagated.
	 */
	public void setBlock(Block block) {
		if (MyBlock != block) {
			if (MyBlock == Block.BlockHolder) {
				MyBlock = block;
				MyBlock.addBlockEdge(this);
//				setHandlers();
			}
			else {
				Point p = MySection.getCoordinates();
				log.warn("Side " + MyEdge + " in section " + p.x + "," + p.y +
						" has a second definition for its Block.");
			}
		}
		else if (MyBlock != null) {
			MyBlock.addBlockEdge(this);
//			setHandlers();
		}
	}

//	/**
//	 * is invoked to record a route through the block (and only the Block) protected by
//	 * this BlkEdge.  This method assumes that a path exists from one
//	 * edge on the Block to a far edge.  It checks the fouling flag
//	 * before beginning, to ensure that is true.
//	 * @param route is the CTCRoute of which the BlockRoute is a piece of.
//	 * For APB, it could be null!
//	 * @return a BlockRoute that describes the route (entry BlkEdge and
//	 * turnout settings) through the Block.  If there is no clear
//	 * path through the Block, null is returned.
//	 */
//	public BlockRoute captureRoute(CTCRoute route) {
//		SecEdge entry = this;
//		SecEdge egress;
//		BlockRoute myRoute;
//		myRoute = new BlockRoute(this, route);
//		for (egress = this.Peer; entry != null; ) {
//			if (egress.isTurnout()) {
//				myRoute.addTurnOut((PtsEdge) entry);
//			}
//			else if (egress.isBlock()) {
//				entry = null;
//			}
//			else {
//				entry = egress.getNeighbor();
//				if (entry != null) {
//					if (entry.isTurnout()) {
//						myRoute.addTurnOut((PtsEdge) entry);
//					}
//					egress = entry.Peer;
//				}
//			}
//		}
//		return myRoute;
//	}

//	/**
//	 * is called when the TrackEdge receives the occupancy indication
//	 * from the VitalLogic to determine the direction that a triggering
//	 * train might be going.  Unlike Chubb's algorithm, determining
//	 * which direction a train is traveling is hard.  There are two
//	 * principle problems: occupancy may not be solid (wheels bounce,
//	 * some cars have no detection, etc.) and a train may change direction.
//	 * This code assumes that things go as planned - an occupancy on
//	 * an entry edge is expected.  Any unexpected reports are due
//	 * to detection unreliability.  Reversing direction is picked up
//	 * when the unoccupancy is reported.
//	 * <p>
//	 * Occupancy is not reported to all edges simultaneously.  One edge
//	 * will be told before the other.  The edges do not know which is
//	 * first.  So, there is little interaction with the opposite edge.
//	 * <p>
//	 * It may not be possible to determine which edge a train is
//	 * entering from - either because both are candidates or neither
//	 * are candidates. 
//	 * @return true if the train probably entered the Block through
//	 * this edge and false if not.
//	 */
//	protected void determineDOT() {
//		switch (TrainDirection) {
//		case NONE:
//			if (isLockSet(LogicLocks.APPROACHLOCK)) {
//				TrainDirection = DirectionOfTravel.ENTRY;
//			}
//			else {
//				TrainDirection = DirectionOfTravel.EXIT;
//				if (MyBlock.getDiscipline().equals(Discipline.APB)) {
//					setEdgeLock(LogicLocks.TUMBLEDOWN);
//				}
//			}
//
//			//$FALL-THROUGH$
//		case ENTRY:
//		case EXIT:
//			// assume a hiccup
//		}
//	}

//	/**
//	 * this method is called when occupancy disappears from a BlkEdge.
//	 * That means a train has either departed or the detector misfired.
//	 * The method assumes the former.
//	 */
//	protected void removeDOT() {
//		TrainDirection = DirectionOfTravel.NONE;
//		if (MyBlock.getDiscipline().equals(Discipline.APB)) {
//			clearEdgeLock(LogicLocks.TUMBLEDOWN);
//			clearEdgeLock(LogicLocks.OPPOSINGSIGNALLOCK);
//		}
//	}

	/**
	 * searches for a Train in the protected block.  A Train is associated
	 * with a Section.  The way the search works is that it traverses the tracks,
	 * beginning at the BlkEnd, checking each Section, until it finds end of
	 * track or BlkEdge.
	 * @return if found, a Train; otherwise, null.
	 */
	public Train newSearch() {
		SecEdge edge = this;
		do {
			if (edge.MySection.hasTrain()) {
				return edge.MySection.getTrain();
			}
			// This crosses to the other side of the Section
			if ((edge = edge.traverse()) != null) {
				// This crosses into the adjacent Section.
				// A FrogeEdge skips over the Associated PtsEdge and even handles fouling.
				edge = edge.getNeighbor();
				if ((edge != null) && (edge instanceof PtsEdge)) {
					// skips over the PtsEdge to the lined FrogEdge (if any)
					edge = edge.Peer;
				}
			}
		} while ((edge != null)  && !(edge instanceof BlkEdge));
		return null;
	}
	
	/**
	 * searches for a Train in the neighbor Block.
	 *
	 * @return the first Train found or null.
	 */
	public Train trainSearch() {
		if ( (Joint != null) && ( (Joint.getBlock()) != null)) {
//			return ( (BlkEdge) Joint).findTrain();
			return ( (BlkEdge) Joint).newSearch();
		}
		return null;
	}

	/**
	 * is called to search for a Train in a Block, beginning at this BlkEdge.
	 * It follows the Track connections until it encounters a Train or the other
	 * side of the Block.
	 * @return a Train, if on can be found and null if not.
	 */
	public Train findTrain() {
		Section s;
		for (Enumeration<SecEdge> e = makeRoute();
				e.hasMoreElements(); ) {
			s = e.nextElement().MySection;
			if (s.hasTrain()) {
				return s.getTrain();
			}	
		}
		return null;
	}

	/**
	 * is called when occupancy is removed on a Block.  The goal is to
	 * move a train from the Block to an adjoining Block, through this edge.
	 * Thus, it first looks for a train in the owning block.  If found, it checks
	 * to see if the neighbor edge exists, and if so, if the neighbor block
	 * is able to accept the train.
	 */
	public void removeTrain() {
		Train t = findTrain();
		if ((t != null) && (Joint != null) && !Joint.MySection.hasTrain()) {
			t.advanceTrain(Joint);
		}
	}

	/**
	 * is called to determine if the adjacent Block (and only the Block) has a train
	 * which could be entering this block through this block edge.
	 *
	 * @return true if the track circuit adjacent to this one shows
	 * occupied.  If there is no adjacent track circuit, return true.
	 */
	public boolean neighborOccupied() {
		BlkEdge neighbor = (BlkEdge) getNeighbor();
		if ((neighbor != null) && neighbor.EdgeLocks.contains(GuiLocks.DETECTIONLOCK) && !neighbor.EdgeLocks.contains(GuiLocks.ENTRYLOCK)) {
			return true;
		}
		return false;
	}

	/**
	 * creates an Enumeration for traversing the exit edges in a route through
	 * a Block.  The Tracks can be found by looking at the exit edge Destination.
	 *
	 * @return an enumeration that returns the exit edge of the rail in each
	 * Section crossed.
	 */
	public Route makeRoute() {
		return new Route();
	}

	/**
	 * test if conflicting traffic has been set in the protected block(s)
	 * @return true if it has been or false if not.
	 */
	public boolean isTrafficBlocked() {
//		return CommonLocks.contains(LogicLocks.CONFLICTINGSIGNALLOCK) || CommonLocks.contains(LogicLocks.OPPOSINGSIGNALLOCK) ||
//				EdgeLocks.contains(LogicLocks.CONFLICTINGSIGNALLOCK) || EdgeLocks.contains(LogicLocks.OPPOSINGSIGNALLOCK);
		return EdgeLocks.contains(GuiLocks.CONFLICTINGSIGNALLOCK) || EdgeLocks.contains(GuiLocks.EXITLOCK);
	}

	/**
	 * tests if a reservation has been made through the SecEdge and if so,
	 * if it is the entrance into the track circuit.  An entry edge is indicated by the Traffic
	 * Stick (DirectionOfTravel) lock being set in the LocalState.
	 * @return true if the BlkEdge is the entrance into the track circuit for a reservation
	 */
	public boolean isEntranceEdge() {
//		return CommonLocks.contains(LogicLocks.TRAFFICLOCK) || EdgeLocks.contains(LogicLocks.TRAFFICLOCK);
		return EdgeLocks.contains(GuiLocks.ENTRYLOCK);
	}

	/**
	 * is a surrogate for setting the exit arrow.  This should be performed in
	 * the OpposingSignalLockIndication, but the BlkEdge object is needed and
	 * I forgot the syntax.
	 * @param isExit is true to set the arrow and false to clear it.
	 */
	private void setExitEdge(boolean isExit) {
		Destination.setArrow(isExit, this);
	}

	@Override
	public boolean isBlkEdge() {
		return true;
	}

	/**
	 * these methods tailors the Lock receivers for the specific edge
	 */

	@Override
	public void setHandlers() {
		/**
		 * The GuiLocks are:
		 * DETECTIONLOCK
		 * CONFLICTINGSIGNALLOCK
		 * SWITCHUNLOCK
		 * APPROACHLOCK
		 * TIMELOCK
		 * TRAFFICLOCK
		 * ENTRYLOCK
		 * EXITLOCK
		 * LOCALCONTROLLOCK
		 * CALLONLOCK
		 * WARRANTLOCK
		 * FLEET
		 * CROSSING
		 * EXTERNALLOCK
		 **/

//		ExitTrafficIndication = new BlockExitIndicationHandler();
//		EntryTrafficIndication = new EntryIndicationHandler();
//		DetectionIndication = new BlkDetectionDecorator(new DetectionLockIndicationHandler());
//		ApproachIndication = new ApproachIndicationHandler();
//		if (!MyBlock.getDiscipline().equals(Discipline.ABS) && !MyBlock.getDiscipline().equals(Discipline.UNDEFINED)) {
//			DetectionIndication = new PropagationDecorator(DetectionIndication);
//		}
		if (!MyBlock.getDiscipline().equals(Discipline.ABS) && !MyBlock.getDiscipline().equals(Discipline.UNDEFINED)) {
			DetectionIndication = new MergeDecorator(
					new PropagationDecorator(
							new RouteCheckerDecorator(
									new BasicLockIndicationHandler(GuiLocks.DETECTIONLOCK))));
		}
		else {
			DetectionIndication = new BasicLockIndicationHandler(GuiLocks.DETECTIONLOCK); 
		}
		DetectionIndication = new BlkDetectionDecorator(
				new DetectionDecorator(DetectionIndication));
		ConflictingSignalIndication = new MergeDecorator(
				new PropagationDecorator(
						new BasicLockIndicationHandler(GuiLocks.CONFLICTINGSIGNALLOCK)));
		SwitchUnlockIndication = new MergeDecorator(
				new PropagationDecorator(
						new RouteCheckerDecorator(
								new BasicLockIndicationHandler(GuiLocks.SWITCHUNLOCK))));
		ApproachIndication = new EdgeLocksDecorator(
				new ApproachIndicationHandler());
		// TIMELOCK should never be set or cleared
		// TRAFFICLOCK should never be set or cleared
		if ((MyBlock.getDiscipline() == Discipline.UNDEFINED) || (MyBlock.getDiscipline() == Discipline.ABS)) {
			EntryTrafficIndication = new BasicLockIndicationHandler(GuiLocks.ENTRYLOCK);
		}
		else {
			EntryTrafficIndication = new MergeDecorator(
					new BlockEntryDecorator(new RouteCheckerDecorator(
							new BasicLockIndicationHandler(GuiLocks.ENTRYLOCK))));
		}
		ExitTrafficIndication = new MergeDecorator(
				new BlockExitDecorator(
						new RouteCheckerDecorator(
								new BasicLockIndicationHandler(GuiLocks.EXITLOCK))));
		LocalControlLock = new MergeDecorator(
				new PropagationDecorator(
						new RouteCheckerDecorator(
								new BasicLockIndicationHandler(GuiLocks.LOCALCONTROLLOCK))));
		// CALLONLOCK
		CallOnIndication = new BasicLockIndicationHandler(GuiLocks.CALLONLOCK);
		// WARRANTLOCK should never be set or cleared
		// FLEETLOCK should never be set or cleared
		// CROSSINGLOCK should never be set or cleared
		PreparationIndication = new ProtectionZoneDecorator(
				new BasicLockIndicationHandler(GuiLocks.PREPARATION));
		ExternalIndication = new MergeDecorator(
				new PropagationDecorator(
						new RouteCheckerDecorator(
								new BasicLockIndicationHandler(GuiLocks.EXTERNALLOCK))));

////		if ((getNeighbor() == null) || (getNeighbor() instanceof CPEdge)) {
////			TravelIndication = new TrafficDirectionIndication();
////		}
////		else {
////			TravelIndication = new PropagationDecorator(new TrafficDirectionIndication());
////		}
//		switch (MyBlock.getDiscipline()) {
//		case ABS:
//		case APB:
//		case CTC:
//			DetectionIndication = new BlkDetectionIndication();
////			EntryTrafficIndication = new BlkEntryTrafficIndication();
////			ExitTrafficIndication = new BlkExitTrafficIndication();
////			ExitTrafficIndication = new TrafficDirectionIndication();
//			break;
//
//		default: // DTC
//			DetectionIndication = new DTCBlkDetectionIndication();
////			EntryTrafficIndication = new BlkEntryTrafficIndication();
////			ExitTrafficIndication = new BlkExitTrafficIndication();
////			ExitTrafficIndication = new TrafficDirectionIndication();
//		}
	}

	/**************************************************************************
	 *  lock indication handlers
	 **************************************************************************/
//	/**
//	 * the Detection Lock handler.  It doesn't do very much because detection
//	 * is more of a Block level activity, rather than an edge activity.  
//	 * Specifically, it does not paint the tracks because not every SecEdge
//	 * in an interlocking gets colored on occupancy.
//	 *
//	 * It should be propagated to to all ends of the protection zone so that
//	 * so that the requirements on setting and clearing routes can be met.
//	 */
//	protected class BlkDetectionIndication extends DefaultLockIndicationHandler {
//		public BlkDetectionIndication() {
//			super(GuiLocks.DETECTIONLOCK);
//		}
//
//		@Override
//		public void requestLockSet() {
//			if (NeighborEdge != null) {
//				NeighborEdge.setEdgeLock(GuiLocks.APPROACHLOCK);
//			}
//			Tsr.setOccupy();
//		}
//
//		@Override
//		public void requestLockClear() {
//			if (NeighborEdge != null) {
//				NeighborEdge.clearEdgeLock(GuiLocks.APPROACHLOCK);
//			}
//			Tsr.clearOccupy();
//			if (!CommonLocks.contains(GuiLocks.FLEET)) {
//				clearEdgeLock(GuiLocks.ENTRYLOCK);
//				clearEdgeLock(GuiLocks.EXITLOCK);
//			}
//		}
//	}

	/**
	 * the Detection Lock decorator for BlkEdges.  It doesn't do very much
	 * because detection is more of a Block level activity, rather than an
	 * edge activity.  Specifically, it does not paint the tracks because
	 * not every SecEdge in an interlocking gets colored on occupancy.
	 *
	 * It should be propagated to to all ends of the protection zone so that
	 * the requirements on setting and clearing routes can be met.
	 */
	protected class BlkDetectionDecorator extends AbstractHandlerDecorator {
		public BlkDetectionDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {
			BlkEdge.this.MyBlock.setBlockDetectionOn();
			if (NeighborEdge != null) {
				NeighborEdge.setEdgeLock(GuiLocks.APPROACHLOCK);
			}
			Tsr.setOccupy();
			Handler.requestLockSet();
		}

		@Override
		public void requestLockClear() {
			BlkEdge.this.MyBlock.setBlockDetectionOff();
			if (NeighborEdge != null) {
				NeighborEdge.clearEdgeLock(GuiLocks.APPROACHLOCK);
			}
			Tsr.clearOccupy();
			Handler.requestLockClear();
		}

		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
		}

		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
		}
	}
	
	/**
	 * the Approach Lock handler for BlkEdges.  It doesn't do very much
	 * because approach locks affect signals and traffic sticks.
	 */
	protected class ApproachIndicationHandler extends BasicLockIndicationHandler {
		public ApproachIndicationHandler() {
			super(GuiLocks.APPROACHLOCK);
		}

		@Override
		public void requestLockSet() {
			Tsr.setApproach();
			EdgeLocks.add(WatchedLock);
		}

		@Override
		public void requestLockClear() {
			Tsr.clearApproach();
			EdgeLocks.remove(WatchedLock);
		}
	}
	
//	/**
//	 * the BlockExit handler.  It turns on and off the direction arrow in
//	 * response to an EXITLOCK.
//	 *
//	 */
//	protected class BlockExitIndicationHandler extends DefaultLockIndicationHandler {
//		public BlockExitIndicationHandler() {
//			super(GuiLocks.EXITLOCK);
//		}
//
//		@Override
//		public void requestLockSet() {
//				setExitEdge(true);
//		}
//
//		@Override
//		public void requestLockClear() {
//				setExitEdge(false);
//		}
//	}
	/**
	 * the BlockExit decorator.  It turns on and off the direction arrow in
	 * response to an EXITLOCK.  It should be decorated with a MergeDecorator.
	 *
	 */
	protected class BlockExitDecorator extends AbstractHandlerDecorator {
		public BlockExitDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {
			Handler.requestLockSet();
			setExitEdge(true);
			setNeighborEntry();
		}

		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
			setExitEdge(false);
			clearNeighborEntry();
		}

		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
			setExitEdge(true);
			setNeighborEntry();
		}

		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
			setExitEdge(false);
			clearNeighborEntry();
		}

		/**
		 * checks the neighbor to see if the route
		 * should be propagated via changing its EntryLock
		 */
		private void setNeighborEntry() {
			if ((NeighborEdge != null) && !(NeighborEdge instanceof CPEdge) && 
					(NeighborEdge.getBlock().getDiscipline() != Discipline.UNDEFINED) && (NeighborEdge.getBlock().getDiscipline() != Discipline.ABS)) {
				NeighborEdge.setCommonLock(GuiLocks.ENTRYLOCK);
			}
		}

		/**
		 * checks the neighbor to see if the route
		 * should be cleared via changing its EntryLock
		 */
		private void clearNeighborEntry() {
			if ((NeighborEdge != null) && !(NeighborEdge instanceof CPEdge)) {
				NeighborEdge.clearCommonLock(GuiLocks.ENTRYLOCK);
			}
		}
	}

//	/**
//	 * the ExitTrafficLock handler.  For a BlkEdge, it colors the track
//	 * for a route.
//	 */
//	protected class BlkExitTrafficIndication extends AbstractLockIndicationHandler {
//		public BlkExitTrafficIndication() {
//			super(GuiLocks.EXITLOCK);
//		}
//
//		@Override
//		public void requestLockSet() {
//			if (!TrafficLocks.contains(LogicLocks.ROUTELOCK) && !CommonLocks.contains(LogicLocks.ROUTELOCK)
//					&& (Joint != null)) {
//				((BlkEdge) Joint).setCommonLock(LogicLocks.SIGNALINDICATIONLOCK);
//			}
////			setEdgeLock(LogicLocks.DIRECTIONOFTRAVEL);
////			setEdgeLock(LogicLocks.OPPOSINGSIGNALLOCK);
//		}
//
//		@Override
//		public void requestLockClear() {
//			if (!TrafficLocks.contains(LogicLocks.ROUTELOCK) && !CommonLocks.contains(LogicLocks.ROUTELOCK)
//					&& (Joint != null)) {
//				((BlkEdge) Joint).clearCommonLock(LogicLocks.SIGNALINDICATIONLOCK);
//			}
////			clearEdgeLock(LogicLocks.DIRECTIONOFTRAVEL);
////			clearEdgeLock(LogicLocks.OPPOSINGSIGNALLOCK);
//		}
//
//		@Override
//		public void advanceLockSet() {
//			if (!TrafficLocks.contains(LogicLocks.ROUTELOCK) && !EdgeLocks.contains(LogicLocks.ROUTELOCK)
//					&& (Joint != null)) {
//				((BlkEdge) Joint).setCommonLock(LogicLocks.SIGNALINDICATIONLOCK);
//			}
//		}
//
//		@Override
//		public void advanceLockClear() {
//			if (!TrafficLocks.contains(LogicLocks.ROUTELOCK) && !EdgeLocks.contains(LogicLocks.ROUTELOCK)
//					&& (Joint != null)) {
//				((BlkEdge) Joint).clearCommonLock(LogicLocks.SIGNALINDICATIONLOCK);
//			}
//		}
//		
//		@Override
//		public void blockLock() {
//			if ((EdgeLocks.contains(WatchedLock) || CommonLocks.contains(WatchedLock)) &&
//					(Joint != null)) {
//				((BlkEdge) Joint).clearCommonLock(WatchedLock);
//			}
//		}
//		
//		@Override
//		public void unblockLock() {
//			if ((EdgeLocks.contains(WatchedLock) || CommonLocks.contains(WatchedLock)) &&
//					(Joint != null)) {
//				((BlkEdge) Joint).setCommonLock(LogicLocks.SIGNALINDICATIONLOCK);
//			}
//		}
//	}

//	/**
//	 * the EntryTrafficLock handler.  For a BlkEdge, it colors the track
//	 * for a route, if there is no exit.  For a CPEdge, it also changes the signal icon's color.
//	 */
//	protected class BlkEntryTrafficIndication extends AbstractLockIndicationHandler {
//		public BlkEntryTrafficIndication() {
//			super(GuiLocks.ENTRYLOCK);
//		}
//
//		@Override
//		public void requestLockSet() {
//			// this is not needed because exittraffic is set by occupancy.  see BlkVitalLogic
////			if (OppositeEnd != null) {
////				OppositeEnd.setEdgeLock(LogicLocks.EXITTRAFFICLOCK);
////			}
//		}
//
//		@Override
//		public void requestLockClear() {
//			if (OppositeEnd != null) {
//				OppositeEnd.clearEdgeLock(GuiLocks.EXITLOCK);
//			}
//		}
////		@Override
////		public void advanceLockSet() {
////			if (OppositeEnd != null) {
////				OppositeEnd.setCommonLock(LogicLocks.ROUTELOCK);
////			}
////		}
////
////		@Override
////		public void advanceLockClear() {
////			if (OppositeEnd != null) {
////				OppositeEnd.clearCommonLock(LogicLocks.ROUTELOCK);
////			}
////		}
////		
////		@Override
////		public void blockLock() {
////		}
////		
////		@Override
////		public void unblockLock() {
////		}
//	}


//	/**
//	 * the ExitTrafficLockHandler for a DTC BlkEdge.  The only way to remove
//	 * the bread crumb trail is by clicking on the icon.
//	 */
//	protected class DTCBlkExitTrafficIndication extends AbstractLockIndicationHandler {
//		public DTCBlkExitTrafficIndication() {
//			super(GuiLocks.EXITLOCK);
//		}
//
//
//		@Override
//		public void requestLockSet() {
//			paintTrackSequence(RouteDecoration.SET_ROUTE);
//			paintTrackSequence(RouteDecoration.SET_DTC_ROUTE);
////			if ((Joint != null) && (((BlkEdge) Joint).OppositeEnd == null)) {
////				((BlkEdge) Joint).setCommonLock(LogicLocks.SIGNALINDICATIONLOCK);
////			}	
//		}
//
//		@Override
//		public void requestLockClear() {
//			paintTrackSequence(RouteDecoration.CLEAR_ROUTE);
//			paintTrackSequence(RouteDecoration.CLEAR_DTC_ROUTE);
////			if ((Joint != null) && (((BlkEdge) Joint).OppositeEnd == null)) {
////				((BlkEdge) Joint).clearCommonLock(LogicLocks.SIGNALINDICATIONLOCK);
////			}		  
//		}
//
////		@Override
////		public void advanceLockSet() {
////			paintTracks(TrackColor.ROUTE, true);
////			paintTracks(TrackColor.WARRANT, true);
////			if ((Joint != null) && (((BlkEdge) Joint).OppositeEnd == null)) {
////				((BlkEdge) Joint).setCommonLock(LogicLocks.SIGNALINDICATIONLOCK);
////			}	
////		}
////
////		@Override
////		public void advanceLockClear() {
////			paintTracks(TrackColor.ROUTE, false);
////			paintTracks(TrackColor.WARRANT, false);
////			if ((Joint != null) && (((BlkEdge) Joint).OppositeEnd == null)) {
////				((BlkEdge) Joint).clearCommonLock(LogicLocks.SIGNALINDICATIONLOCK);
////			}		  
////		}
////		
////		@Override
////		public void blockLock() {
////			if ((EdgeLocks.contains(WatchedLock) || CommonLocks.contains(WatchedLock)) &&
////					(Joint != null)) {
////				((BlkEdge) Joint).clearCommonLock(WatchedLock);
////			}
////		}
////		
////		@Override
////		public void unblockLock() {
////			if ((EdgeLocks.contains(WatchedLock) || CommonLocks.contains(WatchedLock)) &&
////					(Joint != null)) {
////				((BlkEdge) Joint).setCommonLock(LogicLocks.SIGNALINDICATIONLOCK);
////			}
////		}
//	}

//	/**
//	 * the EntryTrafficLock handler for DTC.  For a BlkEdge, it colors the track
//	 * for a route, if there is no exit.  For a CPEdge, it also changes the signal icon's color.
//	 */
//	protected class DTCBlkEntryTrafficIndication extends BlkEntryTrafficIndication {
//
//		@Override
//		public void requestLockSet() {
//			super.requestLockSet();
//			paintTrackSequence(RouteDecoration.SET_DTC_ROUTE);
//		}
//
//		@Override
//		public void requestLockClear() {
//			super.requestLockClear();
//			paintTrackSequence(RouteDecoration.CLEAR_DTC_ROUTE);
//		}
//
////		public void advanceLockSet() {
////			super.advanceLockSet();
////			paintTracks(TrackColor.WARRANT, true);
////		}
////
////		@Override
////		public void advanceLockClear() {
////			super.advanceLockClear();
////			paintTracks(TrackColor.WARRANT, false);
////		}
//	}


//	/**
//	 * the Detection Lock Handler for a DTC block.  It removes the ROUTE
//	 * color on unoccupancy, uncovering the warrant color.
//	 */
//	protected class DTCBlkDetectionIndication extends BlkDetectionIndication {
//		@Override
//		public void requestLockClear() {
//			super.requestLockClear();
//			paintTrackSequence(RouteDecoration.CLEAR_ROUTE);
//		}
//
////		@Override
////		public void advanceLockClear() {
////			super.advanceLockClear();
////			paintTracks(TrackColor.ROUTE, false);
////		}
//	}

//	/**
//	 * the MaintenanceLock handler (LocalControlLock handler).  It does not propagate passed
//	 * the BlkEdge.  It filters all DirectionOfTravel, OpposingSignal, and Exit locks so that they
//	 * do not propagate; however, it does not remove them so that they regenerate when maintenance
//	 * is removed.  It does remove the exit arrow. 
//	 *
//	 */
//	protected class MaintenanceIndication extends DefaultLockIndicationHandler {
//		public MaintenanceIndication() {
//			super(GuiLocks.LOCALCONTROLLOCK);
//		}
//
//		@Override
//		public void requestLockSet() {
//			if (!CommonLocks.contains(LogicLocks.MAINTENANCELOCK)) {
//			}
//		}
//
//		@Override
//		public void requestLockClear() {
//			if (!CommonLocks.contains(LogicLocks.MAINTENANCELOCK)) {
//			}
//		}
//
//		@Override
//		public void advanceLockSet() {
//			if (!EdgeLocks.contains(LogicLocks.MAINTENANCELOCK)) {
//			}
//		}
//
//		@Override
//		public void advanceLockClear() {
//			if (!EdgeLocks.contains(LogicLocks.MAINTENANCELOCK)) {
//			}
//		}
//	}
	
//	/**************************************************************************
//	 * the route entry handler.  It colors the track green in response to
//	 * an ENTRYLOCK.
//	 **************************************************************************/
//	protected class EntryIndicationHandler extends DefaultLockIndicationHandler {
//		public EntryIndicationHandler() {
//			super(GuiLocks.ENTRYLOCK);
//		}
//		
//		@Override
//		public void requestLockSet() {
//			paintTrackBlock(RouteDecoration.SET_ROUTE);
//		}
//		
//		@Override
//		public void requestLockClear() {
//			paintTrackBlock(RouteDecoration.CLEAR_ROUTE);
//		}
//	}	
//	/***************************************************************************
//	 * end of EntryIndicationHandler
//	 **************************************************************************/
	
	/**
	 * the BlockEntry decorator.  It colors the track.
	 * It should be decorated with a MergeDecorator.
	 */
	protected class BlockEntryDecorator extends AbstractHandlerDecorator {
		public BlockEntryDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {
			Handler.requestLockSet();
//			paintTrackBlock(RouteDecoration.SET_ROUTE);
			paintTrackSequence(RouteDecoration.SET_ROUTE);
			setOppositeExit();
		}

		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
//			paintTrackBlock(RouteDecoration.CLEAR_ROUTE);
			paintTrackSequence(RouteDecoration.CLEAR_ROUTE);
			clearOppositeExit();
		}

		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
//			paintTrackBlock(RouteDecoration.SET_ROUTE);
			paintTrackSequence(RouteDecoration.SET_ROUTE);
			setOppositeExit();
		}

		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
//			paintTrackBlock(RouteDecoration.CLEAR_ROUTE);
			paintTrackSequence(RouteDecoration.CLEAR_ROUTE);
			clearOppositeExit();
		}

		/**
		 * checks the neighbor to see if the route
		 * should be propagated via changing its EntryLock
		 */
		private void setOppositeExit() {
			if (OppositeEnd != null) {
				OppositeEnd.setCommonLock(GuiLocks.EXITLOCK);
			}
		}

		/**
		 * checks the neighbor to see if the route
		 * should be cleared via changing its EntryLock
		 */
		private void clearOppositeExit() {
			if (OppositeEnd != null) {
				OppositeEnd.clearCommonLock(GuiLocks.EXITLOCK);
			}
		}
	}

	/**************************************************************************
	 *  end of lock indication handlers
	 **************************************************************************/

	/**************************************************************************
	 * the implementation of TrackEdge
	 **************************************************************************/
	@Override
	public BasicVitalLogic createVitalLogic() {
		return new BlkVitalLogic(new EdgePacketFactory(this), toString());
	}

	/**************************************************************************
	 * the end of TrackEdge
	 **************************************************************************/

	/**
	 * An inner class for providing an Enumeration over the track sections
	 * in a route through the Block.
	 */
	private class Route
	implements Enumeration<SecEdge> {

		/**
		 * the next edge to be given out.
		 */
		private SecEdge NextEdge = null;

		/**
		 * is true if the route through NextEdge is not aligned
		 */
		private boolean Fouling = false;

		/**
		 * The constructor.
		 */
		public Route() {
			NextEdge = traverse();
		}

		public boolean hasMoreElements() {
			return NextEdge != null;
		}

		/**
		 * returns the next Section exit edge, in the path being taken through
		 * the Block.  There are multiple enumeration termination conditions:
		 * 1. if NextEdge is a BlkEdge, then we have reached the Block boundary
		 * 2. if NextEdge has no neighbor, then we have reached the Block
		 *    boundary.
		 * 3. if NextEdge is a PtsEdge and the route from the path into the
		 *    Section is fouling, then we have reached the Block boundary.
		 */
		public SecEdge nextElement() {
			if (hasMoreElements()) {
				SecEdge exitEdge = NextEdge;
				SecEdge entryEdge;
				NextEdge = null;
				if (!Fouling && !exitEdge.isBlock()) {
					if ( (entryEdge = exitEdge.getNeighbor()) != null) {
						NextEdge = entryEdge.traverse();
						if (NextEdge == null) {
							if (entryEdge.Destination != null) {
								NextEdge = entryEdge.Destination.getDestination(entryEdge);
							}
							Fouling = true;
						}
					}
				}
				return exitEdge;
			}
			return null;
		}
	}

	/**************************************************************************
	 *  lock indication handlers
	 **************************************************************************/

	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
			BlkEdge.class.getName());
}
/* @(#)BlkEdge.java */