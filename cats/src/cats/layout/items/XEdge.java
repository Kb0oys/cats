/* Name: XEdge.java
 *
 * What:
 *   This class is a SecEdge for one half of a double crossover.  It is adjacent
 *   to a twin that forms the other half.
 *   <p>
 *   A XEdge is part PtsEdge in that two tracks terminate on it and part
 *   BlkEdge in that it forms a Block boundary.  However, unlike PtsEdge, there
 *   is no preferred (normal) route; there are no moving points; and it can
 *   (actually should) be a Block boundary.
 *   <p>
 *   The design is that it is a composition of two BlkEdge derivatives.  The
 *   XEdge handles the basic track drawing and the BlkEdges handle signaling,
 *   reservations, occupancy, etc.
 */
package cats.layout.items;

import org.jdom2.Element;

import cats.common.Constants;
import cats.common.Sides;
import cats.layout.xml.XMLEleFactory;
import cats.layout.xml.XMLEleObject;
import cats.layout.xml.XMLReader;

/**
 *   This class is a SecEdge for one half of a diamond.  It is adjacent
 *   to a twin that forms the other half.
 *   <p>
 *   An XEdge is part PtsEdge in that two tracks terminate on it and part
 *   BlkEdge in that it forms a Block boundary.  However, unlike PtsEdge, there
 *   is no preferred (normal) route; there are no moving points; and it can
 *   (actually should) be a Block boundary.
 *   <p>
 *   The design is that it is a composition of two BlkEdge derivatives.  The
 *   XEdge handles the basic track drawing and the BlkEdges handle signaling,
 *   reservations, occupancy, etc.
 * 
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2009, 2010, 2011, 2014, 2018, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class XEdge extends SecEdge {

  /**
   * is the tag for identifying a XEdge Object in the XML file.
   */
  static final String XML_TAG = "CROSSINGEDGE";

  /**
   * the state of a track from across the Section that may terminate on the SecEdge:
   * <ol>
   * <li>
   * NO_TRACK means no track from the identified edge terminates on the SecEdge
   * <li>
   * NO_BLOCK means a track from the identified edge terminates on the SecEdge
   * and it does not have a block boundary
   * <li>
   * BLOCK means that a track from the identified edge terminates on the SecEdge
   * and it is a block boundary
   * </ol>
   */
  /**
   * Constants for identifying the above
   */
  private enum BLOCKSTATE {
    NO_TRACK,
    NO_BLOCK,
    BLOCK
  };

  /**
   * the composing section edges.  Only two are instantiated.  Four
   * are reserved to facilitate finding the two.
   */
  private XTrackEdge MySecEdges[] = new XTrackEdge[Sides.EDGENAME.length];

  /**
   * the state of each of the four possible tracks.
   */
  private BLOCKSTATE[] TrackEnd = {
      BLOCKSTATE.NO_TRACK,
      BLOCKSTATE.NO_TRACK,
      BLOCKSTATE.NO_TRACK,
      BLOCKSTATE.NO_TRACK
  };

  /**
   * ctor
   *
   * @param edge identifies the side of the GridTile the SecEdge is on.
   * It will always be -1 on construction and filled in later through
   * a call to completeEdge.
   * @param shared describes the shared edge, if it is not adjacent.
   *    A null value means it is adjacent.  It should always be null.
   */
  public XEdge(int edge, Edge shared) {
    super(edge, shared);
  }

  @Override
  public EdgeInterface.EDGE_TYPE getEdgeType() {
	  return EdgeInterface.EDGE_TYPE.CROSSING_EDGE;
  }
	
  /**
   * sets up the SecEdges that each track terminates on.
   * 
   * @param edge is the number of the edge SecEdge is on.
   */
  public void completeEdge(int edge) {
	  MyEdge = edge;
	  for (int i = 0; i < TrackEnd.length; ++i) {
		  switch (TrackEnd[i]) {
		  case BLOCK:
			  MySecEdges[i] = new XBlkEdge(MyEdge, MyBlock);
			  MySecEdges[i].setVitalLogic();
			  break;

		  case NO_BLOCK:
			  MySecEdges[i] = new InternalXEdge(MyEdge);
			  MySecEdges[i].setVitalLogic();
			  break;

		  default:
		  }
	  }
  }

  /**
   * is called from a Track to tell its termination ends which Blocks
   * they are in.  There are several cases to consider:
   * <ol>
   * <li> if the SecEdge isn't a Block boundary, then the Block identity
   *      must be sent to all tracks terminating on the SecEdge and to
   *      the shared SecEdge (if it exists) in a linked Section.
   * <li> if the SecEdge is a Block boundary, then the propagating
   *      is complete, because the shared SecEdge is in a different
   *      Block.
   * <li> if the SecEdge is a Block boundary and block is the same
   *      as MyBlock, then the method call is an "echo" from a Track.
   * <li> if the SecEdge is a Block boundary, and the block is not the
   *      same, then if MyBlock does not define a discipline, then block
   *      is the real Block, so it replaces MyBlock.
   * <li> if the SecEdge is a Block boundary. and the block is not the
   *      same and MyBlock does define a discipline, then there is a
   *      conflict.  The Blocks is defined differently at two ends.
   * </ol>
   * One other case can be ignored (MyBlock has a defined discipline and
   * block doesn't) because Blocks with an undefined discipline are never
   * entered into BlockKeeper.
   *
   * @param block is the Block identity being propagated.
   */
  public void setBlock(Block block) {
    super.setBlock(block);
    if (MyBlock != null) {
      for (int i = 0; i < MySecEdges.length; ++i) {
        if (MySecEdges[i] != null) {
          MySecEdges[i].setBlock(block);
        }
      }
    }
  }

  /**
   * tells the sub-component where its Section is, so that the sub-component
   * can register itself and retrieve anything else it needs from the Section.
   *
   * @param sec is the Section containing the sub-component.
   *
   * @see Section
   */
  public void addSelf(Section sec) {
    super.addSelf(sec);
    for (int i = 0; i < MySecEdges.length; ++i) {
      if (MySecEdges[i] != null) {
        MySecEdges[i].setSection(sec);
      }
    }    
  }

  /**
   * binds this SecEdge to its mate.  For an XEdge to exist,
   * we know:
   * <ul>
   * <li> its neighbor exists
   * <li> its neighbor is adjacent
   * <li> its neighbor is also an XEdge
   * </ul>
   * Consequently, there are fewer conditions to consider than
   * for a generic SecEdge.
   */
  public void bind() {
    Section nSec;
    XEdge nEdge = null;
    XTrackEdge crossing[] = new XTrackEdge[2];
    int edges[] = new int[2];
    int counter = 0;
    if (Joint == null) {
      nSec = findAdjacentSection(MySection.getCoordinates(), MyEdge);
      Joint = nSec.getEdge(findOtherEdge(MyEdge));
      if (Joint == null) {
        log.warn("Diamond at " + nSec.getCoordinates() + " is incorrectly formed.");
      }
      else {
        nEdge = (XEdge) Joint;
        for (int edge = 0; edge < MySecEdges.length; ++edge) {
          if (MySecEdges[edge] != null) {
        	  edges[counter++] = edge;
          }
        }
        MySecEdges[edges[0]].bindTo(nEdge.MySecEdges[cats.common.Sides.COMPLEMENT[edges[0]]]);
        MySecEdges[edges[1]].bindTo(nEdge.MySecEdges[cats.common.Sides.COMPLEMENT[edges[1]]]);
        crossing[0] = MySecEdges[edges[1]];
        crossing[1] = ((XTrackEdge) MySecEdges[edges[1]].getNeighbor());
        MySecEdges[edges[0]].setBlockingLogic(crossing);
        crossing = new XTrackEdge[2];
        crossing[0] = MySecEdges[edges[0]];
        crossing[1] = ((XTrackEdge) MySecEdges[edges[0]].getNeighbor());
        MySecEdges[edges[1]].setBlockingLogic(crossing);
        // set neighbor cross tracks?
//        nEdge.bind();
      }
    }
    // else it has already been bound
  }

  @Override
  public boolean setDestination(Track trk) {
    int oppositeSide = trk.getTermination(this);
    MySecEdges[oppositeSide].setDestination(trk);
    trk.replaceEdge(this, (SecEdge) MySecEdges[oppositeSide]);
    return false;
  }

  /**
   * is called once during initialization to prime the flow of state downstream.
   */
  public void startFeeding() {
    for (int i = 0; i < MySecEdges.length; ++i) {
      if (MySecEdges[i] != null) {
        MySecEdges[i].startFeeding();
      }
    }    
  }

  /**
   * sets the state of one of the tracks that could terminate on the SecEdge
   * @param tEnd is the track identifier (edge of the other end of the track)
   * @param state is one of the values from above
   * @return null if there is no problem or a message if there is
   */
  public String setTrackEnd(int tEnd, String state) {
    try {
      TrackEnd[tEnd] = BLOCKSTATE.valueOf(state);
    }
    catch(IllegalArgumentException iae) {
      return new String("Unknown XEdge track identifier: " + state);
    }
    catch (NullPointerException npe) {
      return new String("Unknown XEdge track identifier: " + state);
    }
    return null;
  }

  @Override
  public Element dumpDerivativeState(boolean basic) {
	  Element state = new Element(XML_TAG);
	  Element child;
	  boolean changed = false;
	  for (int i = 0; i < MySecEdges.length; ++i) {
		  if (MySecEdges[i] != null) {
			  child = MySecEdges[i].dumpDerivativeState(basic);
			  if (child != null) {
				  state.addContent(child);
				  changed = true;
			  }
		  }
	  }
	  return (changed) ? state : null;
  }

  @Override
  public String setValue(String eleValue) {
	  return new String(XML_TAG + " XML Elements do not have Text fields ("
			  + eleValue + ").");
  }

  @Override
  public String setObject(String objName, Object objValue) {
	  return new String(XML_TAG + " XML Elements do not have embedded objects ("
			  + objName + ").");
  }

  @Override
  public String getTag() {
	  return new String(XML_TAG);
  }

  @Override
  public String doneXML() {
	  return null;
  }

  /**
   * registers a DoubleXEdgeFactory with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new DoubleXEdgeFactory());
  }

//  /*****************************************************************************/
//  /**
//   * The interface that the SecEdge and BlkEdge forms of the composite edge
//   * must adhere to
//   */
//  private interface XEdgeInterface {
////    /**
////     * aligns the SecEdge for the calling Track.  If the perpendicular
////     * track has a reservation, then this edge should be set to not aligned.
////     *
////     * @param fouled is true if the cross track has a reservation or false
////     * if it does not.
////     */
////    public void setFouled(boolean fouled);
//
//    /**
//     * Is a method in SecEdge
//     * 
//     * @param block is the Block identity being propagated.
//     */
//    public void setBlock(Block block);
//
//    /**
//     * sets the Section, for upward references
//     * 
//     * @param sec is the Section containing the Edge.
//     */
//    public void setSection(Section sec);
//
//    /**
//     * binds half of a double crossover to the other half.
//     * @param edge is the other half.
//     */
//    public void bindTo(XEdgeInterface edge);
//
//    /**
//     * comes from SecEdge
//     *
//     * @param trk is the SecEdge at the other end.
//     *
//     * @return false because there is only one route through the edge.
//     * Returning true will be misleading because it implies that the
//     * edge can be switched.
//     */
//    public boolean setDestination(Track trk);
//
//    /**
//     * comes from SecEdge
//     */
//    public void startFeeding();
//
//    /**
//     * records the state specific to a derived SecEdge.
//     * @param basic is true to record only basic state information.  It is false
//     * to record detailed state information.
//     * @return an Element, if the state has changed since the last snapshot; otherwise,
//     * null.
//     */
//    public Element dumpDerivativeState(boolean basic);
//  }

  /*****************************************************************************/

//  /**
//   * a composing BlkEdge.  It has two virtual signals - the usual one that is
//   * the gate for entering the block and a second that is the gate for crossing
//   * the perpendicular track.  The latter is usually open (allowing a train to
//   * cross into the next SecEdge), unless the perpendicular track is busy.
//   */
////  private class XBlkEdge extends BlkEdge implements XEdgeInterface {
//  private class XBlkEdge extends BlkEdge implements XTrackEdge {
//
//    public XBlkEdge(int edge, Edge shared, Block block) {
//      super(edge, shared, block);
//    }
//
////    /**
////     * aligns the SecEdge for the calling Track.  If the perpendicular
////     * track has a reservation, then this edge should be set to not aligned.
////     *
////     * @param fouled is true if the cross track has a reservation or false
////     * if it does not.
////     */
////    public void setFouled(boolean fouled) {
////      if (fouled) {
////        if (Peer != null) {
////          Peer.ConflictingSignalLock.requestLockSet();
////        }
////      }
////      else {
////        if (Peer != null) {
////          Peer.ConflictingSignalLock.requestLockClear();
////        }
////      }
////      // Should this XBlkEdge also tell its peer?
////    }
//
//    public void setSection(Section sec) {
//      MySection = sec;
//    }
//
//    //    /**
//    //     * receives the state of the Track.
//    //     * For a BlkEdge, it must account for the state of the Block
//    //     * and direction of travel of a Train.
//    //     *
//    //     * @param state is the Track's state.
//    //     *
//    //     * @see cats.layout.items.Track
//    //     * @see cats.layout.items.Indication
//    //     */
//    //    public void setState(int state) {
//    //      super.setState(state);
//    //      foulXing(TrkBlkState != Block.TRK_IDLE, this);
//    //    }
//    /**
//     * recomputes the state of the Track.  This merges the track state,
//     * the local state, and the peer state
//     */
//    //    public void setEdgeState() {
//    //      super.setEdgeState();
//    //      foulXing(EnumerationLocks.isSharedBlocking(MergedState.getCktState().getLocks()), this);
//    //    }
//
//    /**
//     * binds half of a double crossover to the other half.
//     * @param edge is the other half.
//     */
////    public void bindTo(XEdgeInterface edge) {
//    public void bindTo(XTrackEdge edge) {
//     if (Joint == null) {
//        Joint = (XBlkEdge) edge;
//        if (Peer != null) {
////          if (Peer.FeederState == UnbondedCircuit) {
////            Peer.FeederState = BlockEdgeCircuit;
////          }
//        }
//        edge.bindTo(this);
//      }
//    }
//
//    //    /**
//    //     * is called from a SecEdge across the Section, looking for the next signal
//    //     * in the direction of travel.  This SecEdge inserts the ExitSignal and
//    //     * continues the probe using it.
//    //     *
//    //     * @param rte is the Track of the path taken to get across the Section.
//    //     * It is needed by the OSSec for identifying which of the routes is
//    //     * being probed.
//    //     *
//    //     * @param holder is a place to put the slowest speed found in the
//    //     * direction of travel.
//    //     *
//    //     * @return the next Signal encountered in the direction of travel.  It
//    //     * may be null.
//    //     */
//    //    public Signal neighborProbe(Track rte, Indication holder) {
//    //      Signal feeder = super.neighborProbe(rte, ExitIndication);
//    //      if (feeder != null) {
//    //        feeder.setPredecesor(ExitSignal);
//    //      }
//    //      holder.setProtectedSpeed(Track.getSlower(ExitIndication.getProtSpeed(),
//    //          holder.getProtSpeed()));
//    //      return ExitSignal;
//    //    } 
//    /**
//     * is called once during initialization to prime the flow of state downstream.
//     */
//    public void startFeeding() {
//      super.startFeeding();
//    }
//
//    /**
//     * records the state specific to a derived SecEdge.
//     * @param basic is true to record only basic state information.  It is false
//     * to record detailed state information.
//     * @return an Element, if the state has changed since the last snapshot; otherwise,
//     * null.
//     */
//    public Element dumpDerivativeState(boolean basic) {
//      return super.dumpDerivativeState(basic);
//    }
//  }
//  /*****************************************************************************/
//  /**
//   * a composing SecEdge.  It has one virtual signal - the usual one that is
//   * the gate for crossing the perpendicular track.  It is usually open (allowing a
//   * train to cross into the next SecEdge), unless the perpendicular track is busy.
//   */
//  private class XSecEdge extends SecEdge implements XEdgeInterface {
//
//    //    /**
//    //     * the signal to prevent a train from leaving the Section.
//    //     */
//    //    private Signal ExitSignal = new Signal();
//
//    //    /**
//    //     * the indication on that Signal.
//    //     */
//    //    private Indication ExitIndication = new Indication(Track.NORMAL, Block.TRK_RESERVED);
//
//    public XSecEdge(int edge, Edge shared) {
//      super(edge, shared);
//      //      ExitSignal.HomeSignal = false;
//    }
//
////    /**
////     * aligns the SecEdge for the calling Track.  If the perpendicular
////     * track has a reservation, then this edge should be set to not aligned.
////     *
////     * @param fouled is true if the cross track has a reservation or false
////     * if it does not.
////     */
////    public void setFouled(boolean fouled) {
////            Destination.alignTrkEnd(this, !fouled);
////            ExitIndication.setCondition((fouled) ? Block.TRK_FOULED : Block.TRK_RESERVED);
////            ExitSignal.trackState(ExitIndication);
////            if (fouled && !LocalState.contains(EnumerationLocks.ConflictingSignalLock)) {
////              setLocalLock(EnumerationLocks.ConflictingSignalLock);
////            }
////            else if (LocalState.contains(EnumerationLocks.ConflictingSignalLock)) {
////              clearLocalLock(EnumerationLocks.ConflictingSignalLock);
////            }
////      if (fouled) {
////        if (Peer != null) {
////          Peer.setLocalLock(EnumerationLocks.ConflictingSignalLock);
////        }
////      }
////      else {
////        if (Peer != null) {
////          Peer.ConflictingSignalLock.requestLockClear();
////        }
////      }
////    }
//
//    public void setSection(Section sec) {
//      MySection = sec;
//    }
//
//    //    /*
//    //     * receives the state of the Track.
//    //     * For a BlkEdge, it must account for the state of the Block
//    //     * and direction of travel of a Train.
//    //     *
//    //     * @param state is the Track's state.
//    //     *
//    //     * @see cats.layout.items.Track
//    //     * @see cats.layout.items.Indication
//    //     */
//    //    public void setState(int state) {
//    //      super.setState(state);
//    //      foulXing(TrkBlkState != Block.TRK_IDLE, this);
//    //    }
//    //    /**
//    //     * recomputes the state of the Track.  This merges the track state,
//    //     * the local state, and the peer state
//    //     */
//    //    public void setEdgeState() {
//    //      super.setEdgeState();
//    //      foulXing(EnumerationLocks.isSharedBlocking(MergedState.getCktState().getLocks()), this);
//    //    }
//
//    /**
//     * binds half of a double crossover to the other half.
//     * @param edge is the other half.
//     */
//    public void bindTo(XEdgeInterface edge) {
//      if (Joint == null) {
//        Joint = (XSecEdge) edge;
//        if (Peer != null) {
////          if (Peer.FeederState == UnbondedCircuit) {
////            Peer.FeederState = BlockEdgeCircuit;
////          }
//        }
//        edge.bindTo(this);
//      }
//    }
//
//    //    /**
//    //     * is called from a SecEdge across the Section, looking for the next signal
//    //     * in the direction of travel.  This SecEdge inserts the ExitSignal and
//    //     * continues the probe using it.
//    //     *
//    //     * @param rte is the Track of the path taken to get across the Section.
//    //     * It is needed by the OSSec for identifying which of the routes is
//    //     * being probed.
//    //     *
//    //     * @param holder is a place to put the slowest speed found in the
//    //     * direction of travel.
//    //     *
//    //     * @return the next Signal encountered in the direction of travel.  It
//    //     * may be null.
//    //     */
//    //    public Signal neighborProbe(Track rte, Indication holder) {
//    //      Signal feeder = super.neighborProbe(rte, ExitIndication);
//    //      if (feeder != null) {
//    //        feeder.setPredecesor(ExitSignal);
//    //      }
//    //      holder.setProtectedSpeed(Track.getSlower(ExitIndication.getProtSpeed(),
//    //          holder.getProtSpeed()));
//    //      return ExitSignal;
//    //    } 
//    /**
//     * is called once during initialization to prime the flow of state downstream.
//     */
//    public void startFeeding() {
//      super.startFeeding();
//    }
//
//    /**
//     * records the state specific to a derived SecEdge.
//     * @param basic is true to record only basic state information.  It is false
//     * to record detailed state information.
//     * @return an Element, if the state has changed since the last snapshot; otherwise,
//     * null.
//     */
//    public Element dumpDerivativeState(boolean basic) {
//      return super.dumpDerivativeState(basic);
//    }
//  }

  static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
      XEdge.class.getName());

}

/**
 * is a Class known only to the XEdge class for creating DoubleXEdges
 * from an XML document.
 */
class DoubleXEdgeFactory
implements XMLEleFactory {

  /**
   * is the XEdge created from the XML file
   */
  private XEdge NewEdge;

  /**
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    NewEdge = new XEdge(-1, null);
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    //    String resultMsg = null;
    //    int trk;
    //    int state;
    //    if ((trk = Sides.findSide(tag)) == Constants.NOT_FOUND) {
    //      resultMsg = "Unknown XEdge track identifier: " + tag;
    //    }
    //    else if ((state = Prop.findString(value, XEdge.TRACKSTATE)) == Constants.NOT_FOUND){
    //      resultMsg = "Unknown condition for XEdge track: " + value;
    //    }
    //    else {
    //      NewEdge.setTrackEnd(trk, state);
    //    }
    String resultMsg = null;
    int trk;
    if ((trk = Sides.findSide(tag)) == Constants.NOT_FOUND) {
      resultMsg = "" + tag;
    }
    else {
      resultMsg = NewEdge.setTrackEnd(trk, value);
    }
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    return NewEdge;
  }
}
/* @(#)XEdge.java */