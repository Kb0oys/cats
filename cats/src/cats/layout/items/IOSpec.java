/**
 * Name: IOSpec.java
 *
 * What:
  * is a class for holding the information needed to trigger something
 * on the railroad or identifying an event from the railroad.
 * <p>
 * Todo: Verify that objects defined by PanelPro can be accessed.
 * <p>
 * This class implements the facade design pattern by hiding the type
 * of the various inputs from the railroad.
 */
package cats.layout.items;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.jdom2.Element;

import cats.apps.Crandic;
import cats.common.Constants;
import cats.common.DebugBits;
import cats.jmri.JmriDevice;
import cats.jmri.JmriDeviceType;
import cats.jmri.JmriPrefix;
import cats.jmri.MeteredConnection;
import cats.layout.BiDecoderObserver;
import cats.layout.DecoderObserver;
import cats.layout.xml.XMLEleFactory;
import cats.layout.xml.XMLEleObject;
import cats.layout.xml.XMLReader;
import cats.rr_events.AcceptLayoutEvent;
import cats.rr_events.AcceptOtherLayoutEvent;
import cats.rr_events.EventInterface;
import jmri.JmriException;
import jmri.Light;
import jmri.Memory;
import jmri.Reporter;
import jmri.Route;
import jmri.Sensor;
import jmri.Turnout;
import jmri.beans.PropertyChangeProvider;

/**
 * is a class for holding the information needed to trigger something
 * on the railroad or identifying an event from the railroad.
 * <p>
 * Todo: Verify that objects defined by PanelPro can be accessed.
 * <p>
 * This class implements the facade design pattern by hiding the type
 * of the various inputs from the railroad.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2006, 2009, 2012, 2018, 2020, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class IOSpec
implements XMLEleObject {

	/**
	 * is the tag for identifying an IOSpec in the XMl file.
	 */
	public static final String XML_TAG = "IOSPEC";

	/**
	 * is the attribute tag for identifying the decoder address.
	 */
	static final public String DECADDR = "DECADDR";

	/**
	 * is the attribute tag for identifying the Loconet message type.
	 */
	static final String JMRIPREFIX = "JMRIPREFIX";

	/**
	 * is the XML tag for the UserName attribute.
	 */
	public static final String USER_NAME = "USER_NAME";

	/**
	 * is the XML tag for the JMRI device type
	 */
	public static final String JMRIDEVICE = "JMRIDEVICE";

	/**
	 * is the text string for a Throw command.
	 */
	public static final String THROW_TEXT = "throw";

	/**
	 * is the text string for a Close command.
	 */
	public static final String CLOSE_TEXT = "close";

	/**
	 * is the XML tag for explicitly sending a command when exiting
	 * a state.
	 */
	public static final String EXIT_CMD = "EXIT_CMD";

	/**
	 * is the XML tag for the delay between commands.
	 */
	public static final String DELAY = "DELAY";

	/**
	 * is the XML tag in the test results recording for the state
	 */
	private static final String STATE = "STATE";

	/**
	 * is the kind of trigger or event.  True is a "throw" and false is
	 * a "close".
	 */
	private boolean ThrowCmd;

	/**
	 * is the number of milliseconds to wait after starting the
	 * chain before starting the next command in the parent chain.
	 */
	private int Delay;

	/**
	 * is the JMRI system name/username broken into components for locating the JMRI device
	 */
	protected final JmriPrefix Prefix;

	/**
	 * is the JMRI Device this IOSPec attaches to
	 */
	private JmriDevice Device;
	
	/**
	 * is the throw/close state of the IOSpec, as picked up from the XML
	 */
	private String State;

	/**
	 * is true if a command should be sent when the state is exited.
	 * Some devices (e.g. SE8C) have mutually exclusive states (e.g one
	 * command turns all LEDS off except for the green) so the active
	 * output is turned off when the state changes.  Other devices have
	 * a state for each output, so one output must be deactivated before
	 * the next is activated.  ExitCmd should be false for the former class
	 * of devices and true for the latter.
	 */
	private boolean ExitCmd = false;

	/**
	 * is the object that is controlled by the IOSpec.
	 */
	private IOSpecAdapter MyAdapter;

	/**
	 * is the object that wants to be notified when the event happens.
	 */
	private DecoderObserver Observer;

	/**
	 * is the object that wants to be notified when the event is removed.
	 */
	private BiDecoderObserver UnObserver;

	/**
	 * is a sendCommand() as an object
	 */
	private EventInterface DoCommand = new SendDo();

	/**
	 * is a sendUndoCommand() as an object
	 */
	private EventInterface UndoCommand = new SendUndo();

	/**
	 * is the strategy pattern for queuing a sendCommand()
	 */
	private EventInterface SendCommand = DoCommand;


	/**
	 * is the strategy pattern for queuing a sendUndoCommand()
	 */
	private EventInterface SendUndoCommand = UndoCommand;

	/**
	 * is the list of DeviceMonitors that have been created.  This list
	 * is needed when "refresh screen" is requested.
	 */
	static private ArrayList<DeviceMonitor> MonitorList = new ArrayList<DeviceMonitor>();

	/**
	 * the constructor for the XML reader.
	 *
	 * @param prefix is the JMRI prefix
	 */
	public IOSpec(final JmriPrefix prefix) {
		Prefix = prefix;
		Device = JmriDevice.findDevice(prefix);
		ThrowCmd = true;  // default value that is overriden by body of XML
	}

	/**
	 * replaces the Delay value.
	 * 
	 * @param d is the new delay in milliseconds.
	 */
	public void setDelay(int d) {
		Delay = d;
	}

	/**
	 * retrieves the Delay value.
	 * 
	 * @return the delay in milliseconds after the previous command
	 *  in a chain.
	 */
	public int getDelay() {
		return Delay;
	}

	/**
	 * sends a command to the decoder to set it to the state specified.
	 */
	public void sendCommand() {
		//    MyAdapter.doCommand(ThrowCmd);
		SendCommand.doIt();
	}

	/**
	 * sends the deactivate command (if it exists).
	 */
	public void sendUndoCommand() {
		if (ExitCmd) {
			//      MyAdapter.doCommand(!ThrowCmd);
			SendUndoCommand.doIt();
		}
	}

	/**
	 * sets the flag designating if a command should be sent when the
	 * active state is exited or not.
	 * @param flag is true if a command should be sent when the active
	 * state is turned off and false when a command should not be sent.
	 */
	public void setExitFlag(boolean flag) {
		ExitCmd = flag;
	}

//	/**
//	 * retrieves the associated JmriDevice
//	 * @return Device, which could be null
//	 */
//	@Deprecated
//	public JmriDevice getDevice() {
//		return Device;
//	}
//	
//
	/**
	 * retrieves the decoder's name, which is used to identify it to the
	 * control structure.  If one has not been defined, MS will be used.
	 *
	 * @return the name of the decoder.
	 */
	private String getName() {
		if (Device != null) {
			return Device.getSystemName();
		}
		return Prefix.getSystemName();
	}

	/**
	 * returns the throw polarity of the command
	 * @return true if the command is a throw and false if it is a close
	 */
	public boolean getThrow() {
		return ThrowCmd;
	}
	
	/**
	 * registers a listener with the appropriate JMRI device.
	 * This method uses the facade pattern to make disparate objects look
	 * the same.
	 *
	 * @param listener is the object which wants to know when the state of
	 * the Sensor or Turnout changes.
	 */
	public void registerListener(DecoderObserver listener) {
		Observer = listener;
		if (Device != null) {
			switch (Device.getType()) {
			
			case Chain:
				new DeviceMonitor<IOSpecChain, Boolean> (listener, Boolean.valueOf(ThrowCmd), IOSpecChain.STATE_TAG);
				break;

			case Light:
				new DeviceMonitor<Light, Integer> (listener, (State.equals(THROW_TEXT) ? Light.OFF : Sensor.ON),
						"KnownState");
				break;

			case Memory:
				new DeviceMonitor<Memory, String> (listener, (State.equals(THROW_TEXT) ? "1" : "0"),
						"value");
				break;

			case Reporter:
				new DeviceMonitor<Reporter, Integer> (listener, (State.equals(THROW_TEXT) ? 1 : 0),
						"lastReport");
				break;

			case Sensor:
				new DeviceMonitor<Sensor, Integer> (listener, (State.equals(THROW_TEXT) ? Sensor.INACTIVE : Sensor.ACTIVE),
						"KnownState");
				break;

			case Turnout:
				new DeviceMonitor<Turnout, Integer> (listener, (State.equals(THROW_TEXT) ? Turnout.THROWN : Turnout.CLOSED),
						"KnownState");
				break;

			default:
			}
		}
	}

	/**
	 * registers a listener for when the event ceases
	 * @param listener wants to know when the event goes away
	 */
	public void registerOtherListener(BiDecoderObserver listener) {
		UnObserver = listener;
	}

	/**
	 * registers the PtsEdge as a possible shared user of the JMRI device
	 * @param points is the non-null information for a route through a PtsEdge
	 */
	public void registerUser(final PtsEdge.RouteReference points) {
		if (Device != null) {
			Device.addRouteInfo(points, ThrowCmd);
		}
	}
	
	/**
	 * retrieves the PtsEdges that use the device for moving points
	 * @return the list of devices registered with the JmriDevice.  It
	 * is unlikely, but it could be null.
	 */
	public ArrayList<PtsEdge.RouteReference> getRegisteredUsers() {
		if (Device != null) {
			return Device.getRoutes(ThrowCmd);
		}
		return null;
	}
	
	/**
	 * compares two IOSpecs for equality
	 *
	 * @param test is the IOSpec being tested for being equal.
	 *
	 * @return true if the commands and addressees are the same.
	 */
	public boolean sameAs(IOSpec test) {
		if ((Device == null) || (Device.getDevice() == null)) {
			return ((test == null) || (test.Device == null) || (test.Device.getDevice() == null));
		}
		if ((test == null) || (test.Device == null)) {
			return false;
		}
		return ((Device.getDevice() == test.Device.getDevice()) && (ThrowCmd == test.ThrowCmd));
	}

	/**
	 * generates an IOSpec that is a complement (changes Throw to Close and vice versa)
	 * @return an IOSpec that does the opposite of the original 
	 */
	public IOSpec createComplement() {
		IOSpec complement = new IOSpec(Prefix);
		if (THROW_TEXT.equals(State)) {
			complement.State = CLOSE_TEXT;
		}
		else {
			complement.State = THROW_TEXT;
		}
		complement.setDelay(Delay);
		complement.setExitFlag(ExitCmd);
		complement.doneXML();
		return complement;
	}
	
	/**
	 * dumps the contents of the IOSpec to the system console
	 * @return a formatted string containing the IOSpec name
	 */
	public String dumpContents( ) {
		String sysName = (getName() == null) ? "unknown" : getName();
		String userName;
		String cmd = (ThrowCmd) ? "throw" : "close";
		String state;
		if (Device == null) {
			state = userName = "unknown";
		}
		else {
			userName = Device.getUserName();
			state = MyAdapter.getDeviceState();
		}
		return new String("System name=" + sysName + " User name=" +
		 userName + " trigger=" + cmd + " state=" + state);
	}
	
	/**
	 * is called to replay the event that triggered a test case.
	 * @param trigger is the Element that describes the event
	 * @return an error message if the event could not be recreated; null,
	 * if it can be recreated
	 */
	public static String processIOSpecTrigger(Element trigger) {
		String prefix = trigger.getAttributeValue(JMRIPREFIX);
		String address = trigger.getAttributeValue(DECADDR);
		String user = trigger.getAttributeValue(USER_NAME);
		JmriDeviceType type;
		String state = trigger.getAttributeValue(STATE);
		JmriDevice device;
		String userIdentity;
		if ((prefix == null) || (prefix.length() == 0)) {
			return new String("Missing JMRI prefix on IOSpec trigger");
		}
		if ((address == null) || (address.length() == 0)) {
			return new String("Missing decoder address on IOSpec trigger");
		}
		if ((state == null) || (state.length() == 0)) {
			return new String ("Missing state for IOSPec " + prefix + address);
		}
		if ((type = JmriPrefix.extractDeviceType(prefix)) == null) {
			return new String("Cannot match " + prefix + " to a JMRI type");
		}
		if ((device = JmriDevice.findDevice(prefix, address, null, user)) == null) {
			if (user == null) {
				userIdentity = "";
			}
			else {
				userIdentity = new String(" User Name " + user);
			}
			return new String("Cannot locate JMRI device with System Name " + prefix + address +
					userIdentity);
		}
		switch(type) {
		case Light:
			Light light = (Light)device.getDevice();
			if (light == null) {
				return new String("No Light found for IOSpec " + prefix + address);
			}
			light.setState(Integer.parseInt(state));
			break;
		case Memory:
			Memory memory = (Memory) device.getDevice();
			if (memory == null) {
				return new String("No Memory found for IOSpec " + prefix + address);
			}
			memory.setValue(state);
			break;
		case Reporter:
			Reporter reporter = (Reporter) device.getDevice();
			if (reporter == null) {
				return new String("No Reporter found for IOSpec " + prefix + address);
			}
			try {
				reporter.setState(Integer.parseInt(state));
			} catch (NumberFormatException e) {
				e.printStackTrace();
				return new String("Reporter number format exception in " + prefix + address);
			} catch (JmriException e) {
				e.printStackTrace();
				return new String("Reporter exception in " + prefix + address);
			}
			break;
		case Sensor:
			Sensor sensor = (Sensor) device.getDevice();
			if (sensor == null) {
				return new String("No Sensor found for IOSpec " + prefix + address);
			}
			try {
				sensor.setKnownState(Integer.parseInt(state));
			} catch (NumberFormatException e) {
				e.printStackTrace();
				return new String("Sensor number format exception in IOSpec " + prefix + address);
			} catch (JmriException e) {
				e.printStackTrace();
				return new String("Sensor exception in IOSpec " + prefix + address);
			}
			break;
		case Turnout:
			Turnout turnout = (Turnout) device.getDevice();
			if (turnout == null) {
				return new String("No Turnout found for IOSpec " + prefix + address);
			}
			try {
				turnout.setState(Integer.parseInt(state));
			} catch (NumberFormatException e) {
				e.printStackTrace();
				return new String("Turnout number format exception in IOSpec " + prefix + address);
			} catch (JmriException e) {
				e.printStackTrace();
				return new String("Turnout exception in IOSpec " + prefix + address);
			}
			break;
		default:
			return new String(prefix + address + " is not implemented as an input.");
		}
		return null;
	}
	
	/**
	 * is invoked in response to "refresh screen".  It steps through the list of
	 * Device Monitors asking each to fetch the state of the JmriDevice being monitored
	 * (the hardware value, as known by JMRI) and send it to the object(s) monitoring
	 * the device.
	 */
	static public void refreshScreen() {
//		if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
//			log.info(Constants.REFRESH_SCREEN + "Starting refreshScreen at " + java.time.ZonedDateTime.now());
//		}
		for (DeviceMonitor monitor: MonitorList) {
			monitor.refreshListener();
		}
//		if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
//			log.info(Constants.REFRESH_SCREEN + "Ending refreshScreen at " + java.time.ZonedDateTime.now());
//		}
	}

	/*********************************************************
	 *   the following inner classes are for outputs
	 *********************************************************/  

	/**
	 * the command pattern: encapsulating the doCommand as an object
	 * so that it can be queued in a metered connection
	 *
	 */
	private class SendDo implements EventInterface {
		public void doIt() {
			MyAdapter.doCommand(ThrowCmd);
		}
	}

	/**
	 * the command pattern: encapsulating the doCommand as an object
	 * so that it can be queued in a metered connection
	 *
	 */
	private class SendUndo implements EventInterface {
		public void doIt() {
			MyAdapter.doCommand(!ThrowCmd);
		}
	}

	/**
	 * the strategy pattern for queuing a sendCommand
	 */
	private class QueDoCommand implements EventInterface {
		public void doIt() {
			MeteredConnection.instance().enqueue(DoCommand);
		}
	}

	/**
	 * the strategy pattern for queuing a sendUndoCommand
	 */
	private class QueUndoCommand implements EventInterface {
		public void doIt() {
			MeteredConnection.instance().enqueue(UndoCommand);
		}
	}

	/**
	 * is a private interface that provides the facade for the various
	 * kinds of DCC objects on the layout.
	 */
	private interface IOSpecAdapter {

		/**
		 * is for output devices.
		 *
		 * @param position is the setting of the light/turnout/sensor
		 */
		public void doCommand(boolean position);
		
		/**
		 * retrieves the actual state of the JMRI device.
		 * @return the state as a String.  This is device type dependent.
		 */
		public String getDeviceState();

	}

	/**
	 * is a public interface for registering with a JmriDevice.  It is used
	 * when the decoder specification is incomplete (the JMRI device was
	 * not found) at the time that the Adapter or Monitor was created.  When the
	 * JmriDevice has found an adapter, it will complete the linkage.
	 */
	public interface LinkageCompletion{
		
		/**
		 * is a call back to the JmriDevice to get the actual JMRI device.
		 */
		public void linkUp();
	}
	
	/**
	 * is an abstract inner class for setting JMRI devices.  It handles the generic
	 * Adapter operations and the concrete classes handle talking to the hardware.
	 */
	private abstract class Adapter<T>
	implements IOSpecAdapter, LinkageCompletion {

		/**
		 * is the JMRI device.
		 */
		protected T JmriDev;

		/**
		 * is the constructor.
		 */
		public Adapter() {
			if (Device != null) {
				JmriDev = (T) Device.getDevice();
				if (JmriDev == null) {
					Device.deferLinking(this);
				}
			}
		}

		public abstract void doCommand(boolean position);
		
		@Override
		public String getDeviceState() {
			if (JmriDev == null) {
				return "unknown state";
			}
			else {
				return getState();
			}
		}
		
		/**
		 * fetches the device dependent state 
		 * @return the state as a String
		 */
		protected abstract String getState();
		
		public void linkUp() {
			JmriDev = (T) Device.getDevice();
			if ((Prefix.getSystemName() == null) && (Device != null)) {
				Prefix.setSystemAddress(Device.getSystemName());
			}
		}
	}

	
	/**
	 * is an inner class for executing a Chain of commands.  Note that
	 * there is no undo command.
	 */
	private class ChainAdapter extends Adapter<IOSpecChain> {

		@Override
		public void doCommand(boolean position) {
			if (JmriDev != null) {
				if (position) {
					JmriDev.sendUndoCommand();
				}
				else {
					JmriDev.sendCommand();
				}
			}
		}
		
		@Override
		protected String getState() {
			return (JmriDev.getKnownState()) ? "true (close)" :
				"false (throw)";
		}
	}

	/**
	 * is an inner class for setting lights.
	 */
	private class LightAdapter extends Adapter<Light> {

		@Override
		public void doCommand(boolean position) {
			if (JmriDev != null) {
				JmriDev.setState(position ? Light.OFF : Light.ON);
			}
		}
		
		@Override
		protected String getState() {
			if (JmriDev.getKnownState() == Light.OFF) {
				return "off (throw)";
			}
			else if (JmriDev.getKnownState() == Light.ON) {
				return "on (close)";
			}
			return "unknown";
		}
	}

	/**
	 * is an inner class for setting memory events.
	 */
	private class MemoryAdapter extends Adapter<Memory> {

		@Override
		public void doCommand(boolean position) {
			if (JmriDev != null) {
				try {
					JmriDev.setState(position ? 1 : 0);
				} catch (JmriException e) {
					System.out.println("JMRI exception in setting Memory "
							+ getName());
				}
			}
		}
		
		@Override
		protected String getState() {
			if (JmriDev.getState() == 1) {
				return "1 (throw)";
			}
			else if (JmriDev.getState() == 0) {
				return "0 (close)";
			}
			return "unknown";
		}
	}

	/**
	 * is an inner class for setting Reporter events.
	 */
	private class ReporterAdapter extends Adapter<Reporter> {

		@Override
		public void doCommand(boolean position) {
			if (JmriDev != null) {
				try {
					JmriDev.setState(position ? 1 : 0);
				} catch (JmriException e) {
					System.out.println("JMRI exception in setting Reporter "
							+ getName());
				}
			}
		}
		
		@Override
		protected String getState() {
			if (JmriDev.getState() == 1) {
				return "1 (throw)";
			}
			else if (JmriDev.getState() == 0) {
				return "0 (close)";
			}
			return "unknown";
		}
	}

	/**
	 * is an inner class for setting Routes.
	 */
	private class RouteAdapter extends Adapter<Route> {

		@Override
		public void doCommand(boolean position) {
			if (JmriDev != null) {
				JmriDev.setRoute();
			}
		}
		
		@Override
		protected String getState() {
			return ((Integer) JmriDev.getState()).toString();
		}
	}

	/**
	 * is an inner class for setting sensors.
	 */
	private class SensorAdapter extends Adapter<Sensor> {

		@Override
		public void doCommand(boolean position) {
			if (JmriDev != null) {
				try {
					JmriDev.setKnownState(position ? Sensor.INACTIVE : Sensor.ACTIVE);
				}
				catch (jmri.JmriException je) {
					String except = "Caught JmriException sending a command to " +
							JmriDev.getSystemName();
					System.out.println(except);
				}
			}
		}
		
		@Override
		protected String getState() {
			if (JmriDev.getKnownState() == Sensor.INACTIVE) {
				return "inactive (throw)";
			}
			else if (JmriDev.getKnownState() == Sensor.ACTIVE) {
				return "active (close)";
			}
			return "unknown";
		}
	}

	/**
	 * is an inner class for setting Turnouts.
	 */
	private class TurnoutAdapter extends Adapter<Turnout> {
		
		@Override
		public void doCommand(boolean position) {
			if (JmriDev != null) {
//				System.out.println("Setting turnout " + JmriDev.getSystemName() + " to " + 
//						(position ? "throw" : "close"));
				JmriDev.setCommandedState(position ? jmri.Turnout.THROWN : jmri.Turnout.CLOSED);
			}
		}
		
		@Override
		protected String getState() {
			if (JmriDev.getKnownState() == jmri.Turnout.THROWN) {
				return "thrown (throw)";
			}
			else if (JmriDev.getKnownState() == jmri.Turnout.CLOSED) {
				return "closed (close)";
			}
			return "unknown";
		}
	}
	
	/*********************************************************
	 *   the following inner classes are for inputs
	 *********************************************************/  

	private class DeviceMonitor<DeviceType extends PropertyChangeProvider, ValueType> implements PropertyChangeListener, LinkageCompletion {
		/**
		 * the actual JMRI device, as a PropertyChangeProvider
		 */
		protected DeviceType JmriDev;
		
		/**
		 * is the state of the monitored device that triggers an
		 * event to the listener.
		 */
		final private ValueType Trigger;

		/**
		 * is the last known JMRI state of the Device
		 */
		private ValueType LastValue = null;

		/**
		 * is the tag in the PropertyChangeEvent that is being watched
		 */
		final private String PropertyTag;
		
		/**
		 * is the constructor.
		 *
		 * @param listener is the CATS object which wants to be told
		 * when the device's state changes to a particular value.
		 * @param trigger is the value that triggers further action.
		 * @param tag is the tag to look for in the PropertyChanged event
		 */
		public DeviceMonitor(final DecoderObserver listener, final ValueType trigger,
				final String tag) {
			Trigger = trigger;
			PropertyTag = tag;
			if (Device != null) {
				if ((JmriDev = (DeviceType) Device.getDevice()) != null) {
					JmriDev.addPropertyChangeListener(this);
				}
				else {
					Device.deferLinking(this);
				}
				MonitorList.add(this);
			}
		}

		/**
		 * receives the change from the railroad, sees if it is the change
		 * that is being watched for, and sends it to the listener.
		 *
		 * @param evt is describes the JMRI object that changed.
		 */
		public void propertyChange(PropertyChangeEvent evt) {
			if (evt.getPropertyName().equals(PropertyTag)) {
				ValueType LastValue = (ValueType) evt.getNewValue();
				if (LastValue.equals(Trigger)) {
//					Observer.acceptIOEvent();
					new AcceptLayoutEvent(Observer).queUp();
				}
				else if (UnObserver != null){
//					UnObserver.acceptOtherIOEvent();
					new AcceptOtherLayoutEvent(UnObserver).queUp();
				}
			}
		}
		
		/**
		 * completes the JMRI System Prefix for a device name
		 */
		public void linkUp() {
			JmriDev = (DeviceType) Device.getDevice();
			JmriDev.addPropertyChangeListener(this);
			if ((Prefix.getSystemName() == null) && (Device != null)) {
				Prefix.setSystemAddress(Device.getSystemName());
			}
		}
		/**
		 * queries the state of the device as known by JMRI and sends that state to
		 * the CATS observer; thus, refreshing CATS' view of the hardware
		 */
		public void refreshListener( ) {
			Object newValue = null;
			if (Device != null) {
				if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
					log.info(Constants.REFRESH_SCREEN + "Retrieving state of " + JmriDev.toString());
				}
				switch(Device.getType()) {
				case Chain:
					newValue = ((IOSpecChain) JmriDev).getKnownState();
					break;
				case Light:
					newValue = ((Light) JmriDev).getKnownState();
					break;
				case Memory:
					newValue = ((Memory) JmriDev).getValue();
					break;
				case Reporter:
					newValue = ((Reporter) JmriDev).getCurrentReport();
					break;
				case Route:
					newValue = ((Route) JmriDev).getState();
					break;
				case Sensor:
					newValue = ((Sensor) JmriDev).getKnownState();
					break;
				case Turnout:
					newValue = ((Turnout) JmriDev).getKnownState();
				}
				if (newValue != null) {
					if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
						log.info(Constants.REFRESH_SCREEN + "Updating Listeners for  " + JmriDev.toString());
					}
					propertyChange(new PropertyChangeEvent(Device, PropertyTag, LastValue, newValue));
				}
				if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
					log.info(Constants.REFRESH_SCREEN + "Done with refreshing " + JmriDev.toString());
				}
			}
		}
	}
	
	/******************************************************
	 * End of the private classes.
	 ******************************************************/

	/*
	 * is the method through which the object receives the text field.
	 *
	 * @param eleValue is the Text for the Element's value.
	 *
	 * @return if the value is acceptable, then null; otherwise, an error
	 * string.
	 */
	public String setValue(String eleValue) {
		State = new String(eleValue);
		return null;
	}

	/*
	 * is the method through which the object receives embedded Objects.
	 *
	 * @param objName is the name of the embedded object
	 * @param objValue is the value of the embedded object
	 *
	 * @return null if the Object is acceptable or an error String
	 * if it is not.
	 */
	public String setObject(String objName, Object objValue) {
		return new String("A " + XML_TAG + " cannot contain an Element ("
				+ objName + ").");
	}

	/*
	 * returns the XML Element tag for the XMLEleObject.
	 *
	 * @return the name by which XMLReader knows the XMLEleObject (the
	 * Element tag).
	 */
	public String getTag() {
		return new String(XML_TAG);
	}

	/*
	 * tells the XMLEleObject that no more setValue or setObject calls will
	 * be made; thus, it can do any error checking that it needs.
	 *
	 * @return null, if it has received everything it needs or an error
	 * string if something isn't correct.
	 */
	public String doneXML() {
		String resultMsg = null;
		if (Prefix != null) {
			switch (Prefix.getDeviceType()) {
			case Chain: {
				MyAdapter = new ChainAdapter(); 
				break;
			}
			case Light: {
				MyAdapter = new LightAdapter();  
				break;
			} 
			case Memory: {
				MyAdapter = new MemoryAdapter();
				break;
			}
			case Reporter: {
				MyAdapter = new ReporterAdapter();
				break;
			}
			case Route: {
				MyAdapter = new RouteAdapter();
				break;
			}
			case Sensor: {
				MyAdapter = new SensorAdapter();
				break;
			}
			case Turnout: {
				MyAdapter = new TurnoutAdapter();
				break;
			}
			default: {
				String id;
				if (Prefix.getSystemName() != null) {
					id = Prefix.getSystemName();
				}
				else if (Prefix.getUserName() != null) {
					id = Prefix.getUserName();
				}
				else {
					id = "unknown";
				}
				resultMsg = new String(id + " is not implemented as an output.");
			}

			}
			if ((Device != null) && Device.isMetered()) {
				SendCommand = new QueDoCommand();
				SendUndoCommand = new QueUndoCommand();
			}
		}
		else {
			resultMsg = new String("There is not enough information to identify a decoder");
		}
		if (THROW_TEXT.equals(State)) {
			ThrowCmd = true;
		}
		else if (CLOSE_TEXT.equals(State)) {
			ThrowCmd = false;
		}
		else {
			resultMsg = new String("An " + XML_TAG + " cannot contain a text field ("
					+ State + ").");
		}
		return resultMsg;
	}

	/**
	 * registers an IOSpecFactory with the XMLReader.
	 */
	static public void init() {
		XMLReader.registerFactory(XML_TAG, new IOSpecFactory());
		new LockedDecoders();
	}

	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
			IOSpec.class.getName());
}

/**
 * is a Class known only to the IOSpec class for creating IOSpecs from
 * an XML document.
 */
class IOSpecFactory
implements XMLEleFactory {

	/**
	 * is the address to send the command to.
	 */
	private String Recipient;

	/**
	 * is the JMRI name prefix
	 */
	private String Prefix;

	/**
	 * is a flag for holding the exit command.
	 */
	boolean ExitFlag;

	/**
	 * is the JMRI user name
	 */
	private String JMRIUserName;

	/**
	 * is the string denoting the JMRI device type
	 */
	private String JMRIType;

	/**
	 * is used when this IOSpec is in a chain to specify the number
	 * of milliseconds to wait before executing the next link in
	 * the chain.
	 */
	int Delay;

	/*
	 * tells the factory that an XMLEleObject is to be created.  Thus,
	 * its contents can be set from the information in an XML Element
	 * description.
	 */
	public void newElement() {
		Recipient = null;
		ExitFlag = false;
		Prefix = null;
		JMRIUserName = null;
		Delay = 0;
		JMRIType = null;
	}

	/*
	 * gives the factory an initial value for the created XMLEleObject.
	 *
	 * @param tag is the name of the attribute.
	 * @param value is it value.
	 *
	 * @return null if the tag:value are accepted; otherwise, an error
	 * string.
	 */
	public String addAttribute(String tag, String value) {
		String resultMsg = null;

		if (IOSpec.DECADDR.equals(tag)) {
			Recipient = value.trim();
			if (Recipient.isEmpty()) {
				Recipient = null;
			}
		}
		else if (IOSpec.JMRIPREFIX.equals(tag)) {
			Prefix = value;
		}
		else if (IOSpec.DELAY.equals(tag)) {
			try {
				int addr = Integer.parseInt(value);
				Delay = addr;
			}
			catch (NumberFormatException nfe) {
				System.out.println("Illegal delay for " + IOSpec.XML_TAG
						+ "XML Element:" + value);
			}        
		}
		else if (IOSpec.USER_NAME.equals(tag)) {
			JMRIUserName = new String(value);
		}
		else if (IOSpec.JMRIDEVICE.equals(tag)) {
			JMRIType = new String(value);
		}
		else if (IOSpec.EXIT_CMD.equals(tag)) {
			if (Constants.TRUE.equals(value)) {
				ExitFlag = true;
			}
		}
		else {
			resultMsg = new String("A " + IOSpec.XML_TAG +
					" XML Element cannot have a " + tag +
					" attribute.");
		}
		return resultMsg;
	}

	/*
	 * tells the factory that the attributes have been seen; therefore,
	 * return the XMLEleObject created.
	 *
	 * @return the newly created XMLEleObject or null (if there was a problem
	 * in creating it).
	 */
	public XMLEleObject getObject() {
		IOSpec spec = new IOSpec(new JmriPrefix(Prefix, Recipient, JMRIType, JMRIUserName));
		spec.setExitFlag(ExitFlag);
		spec.setDelay(Delay);
		return spec;
	}
}
/* @(#)IOSpec.java */