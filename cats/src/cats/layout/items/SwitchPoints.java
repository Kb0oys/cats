/* Name: SwitchPoints.java
 *
 * What:
 * contains the information about Switch Points:
 * <ol>
 *    <li> the command to turn on the locked light.
 *    <li> the command to turn off the locked light.
 *    <li> the command to turn on the unlocked light.
 *    <li> the command to turn off the unlocked light.
 *    <li> the report when the turnout is locally unlocked.
 *    <li> the report when the turnout is locally locked.
 *    <li> the requests to move the turnout to each route.
 *    <li> the commands to move the turnout to each route.
 *    <li> the reports of the turnout reaching each route.
 *    <li> the reports of the turnout moving away from each route.
 * </ol>
 * No distinction is made between input messages and output messages.
 * So, if that changes in the future, this class will undergo some
 * major changes.
 */

package cats.layout.items;

import java.util.ArrayList;

import cats.common.Constants;
import cats.common.Sides;
import cats.layout.BiDecoderObserver;
import cats.layout.DecoderObserver;
import cats.layout.vitalLogic.LogicLocks;
import cats.layout.vitalLogic.PtsVitalLogic;
import cats.layout.xml.*;
/**
 * contains the information about Switch Points:
 * <ol>
 *    <li> the command to turn on the locked light.
 *    <li> the command to turn off the locked light.
 *    <li> the command to turn on the unlocked light.
 *    <li> the command to turn off the unlocked light.
 *    <li> the report when the turnout is locally unlocked.
 *    <li> the report when the turnout is locally locked.
 *    <li> the requests to move the turnout to each route.
 *    <li> the commands to move the turnout to each route.
 *    <li> the reports of the turnout reaching each route.
 *    <li> the reports of the turnout moving away from each route.
 * </ol>
 * No distinction is made between input messages and output messages.
 * So, if that changes in the future, this class will undergo some
 * major changes.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2009, 2012, 2014, 2020, 2024</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class SwitchPoints
implements XMLEleObject {

	/**
	 * is the tag for identifying a SwitchPoints Object in the XMl file.
	 */
	static final String XML_TAG = "SWITCHPOINTS";

	/**
	 * is the tag for identifying one of the messages in the XML file.
	 */
	static final String MSG_TAG = "POINTSMSG";

	/**
	 * is the tag for labeling the SwitchPoints as a spur or not.
	 */
	static final String SPUR_TAG = "SPUR";

	/**
	 * this table contains two loosely related items:
	 * <ol>
	 * <li>
	 *     the labels for each IOSPEC as they appear on dialogs
	 * <li>
	 *     the tag for each IOSpec in the XML file
	 * </ol>
	 * The reason they are combined in this table is to reinforce that there
	 * is a string of both uses for each IOSPEC.
	 * <p>
	 * LOCKOFFCMD and UNLOCKOFFCMD have been deprecated and should be removed.
	 */
	public static final String[][] MessageName = {
		{
			"Turn Locked Light On", "LOCKONCMD"}
		, {
			"Turn Locked Light Off", "LOCKOFFCMD"}
		, {
			"Turn Unlocked Light On", "UNLOCKONCMD"}
		, {
			"Turn Unlocked Light Off", "UNLOCKOFFCMD"}
		, {
			"Turnout Unlocked Report", "UNLOCKREPORT"}
		, {
			"Turnout Locked Report", "LOCKREPORT"}
	};

	/**
	 * the index into MessageName of the XML tag.
	 */
	public static final int MSGXML = 1;

	/**
	 * symbolic names for indexing each of the above
	 */
	/**
	 * a symbolic name for the index of the commands for turning on the
	 * "Locked" light.  This has been revised to be used with a Controlled
	 * Electrical Lock.  A Controlled Electrical Lock is a manual switch
	 * which must be unlocked by the dispatcher for hand operation.
	 */
	public static final int LOCKONCMD = 0;

	/**
	 * a symbolic name for the index of the commands for turning off the
	 * "Locked" light.
	 */
	public static final int LOCKOFFCMD = 1;

	/**
	 * a symbolic name for the index of the commands for turning off the
	 * "UnLocked" light.  This has been revised to be used with an Automatic
	 * Electrical Lock.  A Automatic Electrical Lock is a manual switch
	 * which must be unlocked by the field crew before it can be hand operated;
	 * thus, this function is closely related to a LOCKREPORT.  CATS receives
	 * the unlock request and when conditions permit, it acknowledges the
	 * request by sending the "lock off" command.
	 */
	public static final int UNLOCKONCMD = 2;

	/**
	 * a symbolic name for the index of the commands for turning off the
	 * "Unlocked" light.
	 */
	public static final int UNLOCKOFFCMD = 3;

	/**
	 * a symbolic name for the index of the report sent when the turnout
	 * is unlocked.
	 */
	public static final int UNLOCKREPORT = 4;

	/**
	 * a symbolic name for the index of the report sent when the turnout
	 * is locked.
	 */
	public static final int LOCKREPORT = 5;

	/**
	 * the IOSpecs for each of the above.  Some of these entries
	 * can be null.  For example, if there are no reports of the
	 * turnout's position, then the last four will be null.  Others may
	 * be null because the function is duplicated.  For example,
	 * turning the lock light on may turn the unlock light off, but
	 * both are included to handle the layout where different decoders
	 * control the lock and unlock lights.
	 */
	private IOSpec[] Decoders = new IOSpec[MessageName.length];

	/**
	 * the list of requests, commands, and reports for each route.
	 */
	private RouteInfo[] Routes = new RouteInfo[Sides.EDGENAME.length];

//	/** 
//	 * the list of PtsVitalLogic that share decoders with this SwitchPoints
//	 */
//	private SharingDecoder[][] SharingLogic;
	
	/**
	 * the PtsVitalLogic that oversees these SwitchPoints
	 */
	private PtsVitalLogic PtsLogic;

//	/**
//	 * the PtsEdge (including OSEdge) that contains this SwitchPoints
//	 */
//	private PtsEdge GuiEdge;
	
//	/**
//	 * the local lock symbol
//	 */
//	private LockFrill LockSymbol;
	
	/**
	 * is a flag for indicating the SwitchPoints can aligned by the dispatcher
	 * or not.  true means they are a spur (cannot be moved by the dispatcher)
	 * and false means they are completely under dispatcher control.
	 */
	private boolean Spur = false;

	/**
	 * records the last lock/unlock report
	 */
	private boolean Locked;

	/**
	 * is true if Lock/unlock reports have been implemented.
	 */
	private boolean LockReports;

	/**
	 * The index of the Normal route.
	 */
	int NormalRoute;

	/**
	 * The index of the last route selected.  It is needed to refresh the
	 * layout from a test mode.
	 */
	private int LastRoute;

	/**
	 * the constructor.
	 *
	 * @param spur is true if the SwitchPoints are for a Spur and false if
	 * they are for an OS section.
	 */
	public SwitchPoints(boolean spur) {
		Spur = spur;
		NormalRoute = -1;
	}
	
	/**********************************************************************************
	 * This first set of methods are used in configuring points.
	 **********************************************************************************/
	/**
	 * tells the SwitchPoints where the PtsLogic (VitalLogic)
	 * is
	 * @param logic is the VitalLogic
	 */
	public void setPtsLogic(PtsVitalLogic logic) {
		PtsLogic = logic;
		for (int rt = 0; rt < Routes.length; ++rt) {
			if (Routes[rt] != null) {
				Routes[rt].setVitalLogic(PtsLogic);
			}
		}
	}

//	/**
//	 * tells the SwitchPoints where the containing PtsEdge is
//	 * @param gui the containing Gui PtsEdge
//	 */
//	public void setGuiEdge(PtsEdge gui) {
//		GuiEdge = gui;
//		if ((Decoders[UNLOCKREPORT] != null) || (Decoders[LOCKREPORT] != null)) {
//			LockSymbol = new LockFrill(GuiEdge.MyEdge);
//			LockSymbol.showLock(false);
//			GuiEdge.EdgeTile.addFrill(LockSymbol);
//		}
//	}
	
	/**
	 * fetches the Edge opposite of the SwitchPoints edge where the
	 * Normal route terminates.
	 *
	 * @return NormalRoute.  It may not be set, if no Route has Normal
	 * set.
	 */
	public int getNormal() {
		return NormalRoute;
	}

	/**
	 * returns the Spur flag.
	 *
	 * @return true if the SwitchPoints cannot be aligned by the dispatcher
	 * or false if they can.
	 */
	public boolean getSpur() {
		return Spur;
	}

	/**
	 * retrieves the SwitchPoints that share common decoders with this SwitchPoints
	 * @return all the SwitchPoints that have registered their Command decoder with JmriDevice.
	 */
	public ArrayList<PtsEdge.RouteReference>[] getSharedRoutes() {
		ArrayList<PtsEdge.RouteReference>[] shared = new ArrayList[PtsEdge.MAX_ROUTES];
		IOSpec decoder;
//		boolean sharing = false;
		for (int i = 0; i < PtsEdge.MAX_ROUTES; ++i) {
			if ((Routes[i] != null) && ((decoder = Routes[i].getRouteCmd()) != null)) {
				shared[i] = decoder.getRegisteredUsers();
			}
		}
		
//		// convert and prune the list of shared decoders
//		SharingLogic = new SharingDecoder[PtsEdge.MAX_ROUTES][];
//		for (int i = 0; i < PtsEdge.MAX_ROUTES; ++i) {
//			if ((shared[i] != null) && (shared[i].size() > 1)) {
//				sharing = true;
//				SharingLogic[i] = new SharingDecoder[shared[i].size()];
//				for (int ref = 0; ref < shared[i].size(); ++ref) {
//					SharingLogic[i][ref] = new SharingDecoder(shared[i].get(ref));
//				}
//			}
//		}
//		if (!sharing) {
//			SharingLogic = null;
//		}
		return shared;
	}
	
	/**
	 * retrieves the IOSpec that moves the points to a track
	 * @param side is the index of the desired track.  It must bein range (0..MAX_ROUTES)
	 * @return the IOSpec that makes the move.  If there is no IOSPec, it will be null
	 */
	public IOSpec getRouteCmd(final int side) {
		if (Routes[side] != null) {
			return Routes[side].getRouteCmd();
		}
		return null;
	}

	/**
	 * retrieves the decoder command for controlling the lock/unlock
	 * lights.
	 *
	 * @param light is the command for the light being requested.
	 *
	 * @return the IOSpec for the requested light.  It can be null if there
	 * is no command for the light.
	 */
	public IOSpec getLockCmds(int light) {
		if ( (light >= 0) && (light < Decoders.length)) {
			return Decoders[light];
		}
		return null;
	}

	/**
	 * returns if the switch points have feedback defined.
	 * @return true if the layout reports when the points move and
	 * false if it does not.
	 */
	public boolean hasFeedback()
	{
		RouteInfo rtInfo;
		if (Routes != null) {
			for (int rt = 0; rt < Routes.length; rt++) {
				if (((rtInfo = Routes[rt]) != null) &&
						( rtInfo.getFeedback() != RouteInfo.FEEDBACK.NO_FEEDBACK)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * a method to determine how many feedback events will be generated when the points move
	 * from one route to another.  There are only three possibilities (as per the Digitrax Big Book of DCC):
	 * 1. none (toRoute does not report when the points are in place
	 * 2. one (toRoute does report when points are in place, but fromRoute does not report when points move out of place)
	 * 3. two (toRoute does report when points are in place and fromRoute does report when points move out of place)
	 * Note that not all point movements in a given SwitchPoints have to return the same results.  It all depends
	 * upon which feedback is defined on each alignment.
	 * @param fromRoute is the idex of the track the points begin aligned on
	 * @param toRoute is the index of the track the points are moving to
	 * @return NO_FEEDBACK, POSITIVE_FEEDBACK, or EXACT_FEEDBACK of the track combinations.
	 */
	public RouteInfo.FEEDBACK getfeedbackEventCount(final int fromRoute, final int toRoute) {
		if (Routes == null){ 
			return RouteInfo.FEEDBACK.NO_FEEDBACK;
		}
		if ((Routes[toRoute] == null) || Routes[toRoute].getFeedback().equals(RouteInfo.FEEDBACK.NO_FEEDBACK)) {
			return RouteInfo.FEEDBACK.NO_FEEDBACK;
		}
		else if ((Routes[fromRoute] == null) || !Routes[fromRoute].getFeedback().equals(RouteInfo.FEEDBACK.EXACT_FEEDBACK)) {
			return RouteInfo.FEEDBACK.POSITIVE_FEEDBACK;
		}
		return RouteInfo.FEEDBACK.EXACT_FEEDBACK;
	}
	
	/**********************************************************************************
	 * The next group of methods are used to run the panel
	 **********************************************************************************/
	/**
	 * is a request to line the points to a route.  This method will immediately
	 * deny the request if they are not an OS (dispatcher controlled).  Otherwise,
	 * it will delegate the safety checking to the requested route.
	 * @param route is the index of the Track (the SecEdge on the far end) of the
	 * requested route.  Since it is called only from PtsVitalLogic and PtsVitalLogic
	 * has already sanity checked the route, this method assumes that route is
	 * between 0 and 3.
	 * @return true if the request can be honored and false if not.
	 */
	public boolean requestRoute(int route) {
		if (Spur) {
			return false;
		}
		if (Routes[route] == null) {
			if (PtsLogic.checkDispatcherLocks()) {
				return PtsLogic.receiveFeedback(route, true);
			}
		}
		else if (Routes[route].requestAlignment()) {
			LastRoute = route;
			return true;
		}
		return false;
	}

//	/**
//	 * is a request to line the points to a route in response to local
//	 * request.  This is different from a dispatcher request because
//	 * the switch unlocked state is the opposite.  For a dispatcher originated
//	 * request, a locally unlocked switch will deny the request.  For
//	 * a local request, a locally unlocked switch (if defined) must
//	 * be unlocked.  If not defined, then the unlocked switch flag is ignored.
//	 * @param route is the index of the Track (the SecEdge on the far end) of the
//	 * requested route.  Since it is called only from PtsVitalLogic and PtsVitalLogic
//	 * has already sanity checked the route, this method assumes that route is
//	 * between 0 and 3.
//	 * @return true if the request can be honored and false if not.
//	 */
//	public boolean requestLocalRoute(int route) {
//		if (Routes[route].requestLocalAlignment()) {
//			LastRoute = route;
//			return true;
//		}
//		return false;
//	}

//	/**
//	 * retrieves a list of decoders.
//	 *
//	 * @param route is the side of the frog end of the points.
//	 *
//	 * @return the RouteInfo describing that leg of the turnout, which
//	 * may be null.
//	 */
//	public RouteInfo getRouteList(int route) {
//		if ( (Routes != null) && (route >= 0) && (route < Routes.length)) {
//			return Routes[route];
//		}
//		return null;
//	}

	/**
	 * sets the points to a particular alignment.
	 *
	 * @param rt indexes the decoder commands to move the turnout(s).
	 * It must be between 0 and 3.  Out of correspondence is invalid.
	 */
	public void selectRoute(int rt) {
		RouteInfo rtInfo;
		IOSpec decoder;

		LastRoute = rt;
		if ( (Routes != null) && ( (rtInfo = Routes[rt]) != null) &&
				( (decoder = rtInfo.getRouteCmd()) != null)) {
			decoder.sendCommand();
		}
		else { // no decoder to trigger, so the layout will not generate feedback.
			
		}
	}

	/**
	 * sets the points to their last known alignment.
	 */
	public void restorePoints() {
		selectRoute(LastRoute);
	}

	//  /**
	//   * locks or unlocks the decoder commands.  When locked, commands that would
	//   * move the points are placed on the black list.  Unlocking removes them
	//   * from the list.
	//   * @param rt is the route which is being protected.
	//   * @param locked is true to lockout the other commands and true to remove
	//   * the lock
	//   */
	//  public void setRouteLock(int rt, boolean locked) {
	//    RouteInfo rtInfo;
	//    IOSpec decoder;
	//    if ((Routes != null) && (rt >= 0)  && (rt < Routes.length)) {
	//      for (int r = 0; r < Routes.length; ++r) {
	////        if ((rt != r) && ((rtInfo = Routes[r]) != null) && 
	//        if (((rtInfo = Routes[r]) != null) && 
	//            ((decoder = rtInfo.getRouteCmd()) != null)) {
	//          if (locked) {
	//            decoder.lockOutCommand();
	//          }
	//          else {
	//            decoder.unlockOutCommand();
	//          }
	//        }
	//      }
	//    }
	//  }

	//  /**
	//   * reports if it is safe to send the command to set the points
	//   * to a route.
	//   * @param rt  is the route.
	//   * @return true if the decoder command has been put on the 
	//   * black list and false if it has not.
	//   */
	//  public boolean isRouteLocked(int rt) {
	//    RouteInfo rtInfo;
	//    IOSpec decoder;
	//    if ((Routes != null) && (rt >= 0)  && (rt < Routes.length)) {
	//      if (((rtInfo = Routes[rt]) != null) && 
	//          ((decoder = rtInfo.getRouteCmd()) != null)) {
	//        return decoder.isLockedOut();
	//      }
	//    }
	//    return false;
	//  }

	/**
	 * tests that the decoders in a route are working by putting all of them
	 * in a particular state.
	 * 
	 * @param state is the state to send to all decoders in all routes.
	 */
	public void testRoutes(boolean state) {
		RouteInfo rtInfo;
		IOSpec decoder;

		if (Routes != null) {
			for (int rt = 0; rt < Routes.length; rt++) {
				if (((rtInfo = Routes[rt]) != null) &&
						( (decoder = rtInfo.getRouteCmd()) != null)) {
					if (decoder.getThrow() == state) {
						decoder.sendCommand();
					}
					else {
						decoder.sendUndoCommand();
					}
				}
			}
		}
	}

//	/**
//	 * checks that the points are supposed to be aligned to a particular
//	 * track.  If they are not, it moves them to the last commanded position.
//	 * This has the effect of countermanding any unauthorized local command
//	 * (when the points report in place movement.
//	 * 
//	 * @param trk is the index of the route the points are in (or commanded
//	 * to be in)
//	 */
//	public void verifyMovement(int trk) {
//		if (trk != LastRoute) {
//			log.debug("Restoring " + trk + " to " + LastRoute);
//			restorePoints();
//		}
//		else {
//			log.debug("No correction");
//		}
//	}

//	/**
//	 * is called when the points are moving from a position.  If the trk being
//	 * moved from is the last commanded position, then it moves them back.
//	 * This has the effect of countermanding any unauthorized local command
//	 * (when the points report release movement).
//	 * @param trk
//	 */
//	public void verifyNoMovement(int trk) {
//		if (trk == LastRoute) {
//			restorePoints();
//		}
//	}

	//  /**
	//   * registers the routes with the TurnoutHelper so that ganged turnouts are not thrown
	//   * when one is locked in position and movement is allowed on unselected turnouts.
	//   */
	//  public void registerRoutes(PtsEdge edge) {
	//    ArrayList<IOSpec> allFeedback = new ArrayList<IOSpec>();
	//    ArrayList<IOSpec> expected;
	//    IOSpec spec;
	//    
	//    // collect all feedback
	//    for (int rt = 0; rt < Routes.length; ++rt) {
	//      if (Routes[rt] != null) {
	//        if ((spec = Routes[rt].getRouteReport(RouteInfo.SELECTEDREPORT)) != null) {
	//          allFeedback.add(spec);
	//        }
	//        if ((spec = Routes[rt].getRouteReport(RouteInfo.NOTSELECTEDREPORT)) != null) {
	//          allFeedback.add(spec);
	//        }
	//      }
	//    }
	//    
	//    // register the feedback for each route
	//    for (int rt = 0; rt < Routes.length; ++rt) {
	//      if (Routes[rt] != null) {
	//        // this is points moving into place
	//        expected = new ArrayList<IOSpec>(2);
	//        if ((spec = Routes[rt].getRouteReport(RouteInfo.SELECTEDREPORT)) != null) {
	//          expected.add(spec);
	//        }
	//        // this is points moving out of place for all other routes
	//        for (int r = 0; r < Routes.length; ++r) {
	//          if ((Routes[r] != null) && (r != rt)) {
	//            if ((spec = Routes[r].getRouteReport(RouteInfo.NOTSELECTEDREPORT)) != null) {
	//              expected.add(spec);
	//            }            
	//          }
	//        }
	//        TurnoutHelper.registerTurnoutCommand(Routes[rt].getCmdName(), Routes[rt].getRouteCmd(), edge, expected, allFeedback);
	//      }
	//    }    
	//  }

	/*
	 * is the method through which the object receives the text field.
	 *
	 * @param eleValue is the Text for the Element's value.
	 *
	 * @return if the value is acceptable, then null; otherwise, an error
	 * string.
	 */
	public String setValue(String eleValue) {
		return new String("A " + XML_TAG + " cannot contain a text field ("
				+ eleValue + ").");
	}

	/*
	 * is the method through which the object receives embedded Objects.
	 *
	 * @param objName is the name of the embedded object
	 * @param objValue is the value of the embedded object
	 *
	 * @return null if the Object is acceptable or an error String
	 * if it is not.
	 */
	public String setObject(String objName, Object objValue) {
		String resultMsg = null;
		SwitchMessage msg;
		RouteInfo newRoute;

		if (MSG_TAG.equals(objName)) {
			msg = (SwitchMessage) objValue;
			if ( (msg.Function >= 0) && (msg.Function < MessageName.length)) {
				Decoders[msg.Function] = msg.Message;
			}
			else {
				resultMsg = new String("A " + XML_TAG + " found an invalid message:"
						+ msg.Function);
			}
		}
		else if (RouteInfo.XML_TAG.equals(objName)) {
			newRoute = (RouteInfo) objValue;
			Routes[newRoute.getRouteId()] = newRoute;
			if (newRoute.getNormal()) {
				NormalRoute = newRoute.getRouteId();
				LastRoute = NormalRoute;
			}
		}
		else {
			resultMsg = new String("A " + XML_TAG + " cannot contain an Element ("
					+ objName + ").");
		}
		return resultMsg;
	}

	/*
	 * returns the XML Element tag for the XMLEleObject.
	 *
	 * @return the name by which XMLReader knows the XMLEleObject (the
	 * Element tag).
	 */
	public String getTag() {
		return new String(XML_TAG);
	}

	/*
	 * tells the XMLEleObject that no more setValue or setObject calls will
	 * be made; thus, it can do any error checking that it needs.
	 *
	 * @return null, if it has received everything it needs or an error
	 * string if something isn't correct.
	 */
	public String doneXML() {
		BiDecoderObserver observer;
		IOSpec decoder;
		IOSpec[] localCtrl = new IOSpec[PtsEdge.MAX_ROUTES];
		boolean toggle;
		if ((NormalRoute == -1) && (Routes != null)) {
			return new String("The Normal Route has not been defined for Points.");
		}

		// Though it may not be correct, start out with all fascia lock
		// indicators in the "locked" position.  It would be best to set
		// them to the current state, but the state cannot be read on some
		// systems (e.g. Loconet).
		//	if ( (decoder = MyPoints.getLockCmds(SwitchPoints.LOCKONCMD)) != null) {
		//		decoder.sendCommand();
		//	}
		//
		//	if ( (decoder = MyPoints.getLockCmds(SwitchPoints.UNLOCKONCMD)) != null) {
		//		decoder.sendUndoCommand();
		//	}
		//
		//	// register an observer for the Unlock event
		//	if ( (decoder = MyPoints.getLockCmds(SwitchPoints.UNLOCKREPORT)) != null) {
		//		new UnLockReceiver(decoder);
		//	}
		//
		//	// register an observer for the Lock event
		//	if ( (decoder = MyPoints.getLockCmds(SwitchPoints.LOCKREPORT)) != null) {
		//		new LockReceiver(decoder);
		//		//        PadLocked = true;
		//	}
		if (Decoders[LOCKONCMD] != null) {
			Decoders[LOCKONCMD].sendCommand();
		}
		if (Decoders[UNLOCKONCMD] != null) {
			Decoders[UNLOCKONCMD].sendUndoCommand();
		}
		if (Decoders[UNLOCKREPORT] != null) {
			observer = new UnLockReceiver();
			Decoders[UNLOCKREPORT].registerListener(observer);
			if (Decoders[LOCKREPORT] != null) {
				Decoders[LOCKREPORT].registerListener(new LockReceiver());
			}
			else {
				Decoders[UNLOCKREPORT].registerOtherListener(observer);
			}
		}
		else if (Decoders[LOCKREPORT] != null) {
			observer = new LockReceiver();
			Decoders[LOCKREPORT].registerListener(observer);
			Decoders[LOCKREPORT].registerOtherListener(observer);
		}
		for (int align = 0; align < PtsEdge.MAX_ROUTES; ++align) {
			if ( Routes[align] != null) {
				// register an observer for the local control requesting movement
				if ( (decoder = Routes[align].getRouteReport(RouteInfo.ROUTEREQUEST)) != null) {
					localCtrl[align] = decoder;
				}
			}
		}
		for (int rte = 0; rte < PtsEdge.MAX_ROUTES; ++rte) {
			toggle = false;
			if (localCtrl[rte] != null) {
				for (int r = rte + 1; r < PtsEdge.MAX_ROUTES; ++r) {
					if ( (localCtrl[r] != null) && (localCtrl[rte].sameAs(localCtrl[r]))) {
						toggle = true;
						localCtrl[r] = null;
					}
				}
				if (toggle) {
					new ToggleReceiver(localCtrl[rte]);
				}
				else {
					new LocalReceiver(rte, localCtrl[rte]);
				}
			}
		}

		return null;
	}

	/**
	 * registers a SwitchPointsFactory with the XMLReader.
	 */
	static public void init() {
		XMLReader.registerFactory(XML_TAG, new SwitchPointsFactory());
		XMLReader.registerFactory(MSG_TAG, new SwitchMessageFactory());
		RouteInfo.init();
	}

//	/**
//	 * is an inner class for holding the identity of PtsVitalLogic and a track index for
//	 * shared decoders.
//	 */
//	private class SharingDecoder {
//		// the PtsVitalLogic using the decoder
//		public final PtsVitalLogic SharingLogic;
//		
//		// the track index
//		public final int SharingTrack;
//		
//		// the constructor extracts the information from a RouteReference
//		public SharingDecoder (final PtsEdge.RouteReference shared) {
//			SharingLogic = (PtsVitalLogic) shared.Edge.getVitalLogic();
//			SharingTrack = shared.TrackNumber;
//		}
//	}
	
	/**
	 * is an inner class for receiving requests for local movement of the points.
	 * It has a one-to-one relationship with a single alignment of the points.
	 */
	private class LocalReceiver
	implements DecoderObserver {

		/**
		 * is the Track being controlled.
		 */
		private int TrkControlled;

		/**
		 * is the constructor.
		 *
		 * @param c is the track being controlled.
		 * @param decoder is the IOSpec describing the messages that the receiver
		 * is looking for.
		 */
		public LocalReceiver(int c, IOSpec decoder) {
			TrkControlled = c;
			decoder.registerListener(this);
		}

		/**
		 * is the interface through which the MsgFilter delivers the RREvent.
		 * This routine probably should tell the enclosing Section that the
		 * alignment has changed.  Not doing so means that the dispatcher may
		 * have to click multiple times to make a change.
		 */
		public void acceptIOEvent() {
			if (!LockReports || !Locked) {
				PtsLogic.requestLocalMovement(TrkControlled);
			}
		}
	}

	/**
	 * is an inner class for receiving requests for local movement of the points.
	 * It is a toggle report so that every time it is received, the points move.
	 */
	private class ToggleReceiver
	implements DecoderObserver {

		int CurrentTrk;
		/**
		 * is the constructor.
		 *
		 * @param decoder is the IOSpec describing the messages that the receiver
		 * is looking for.
		 */
		public ToggleReceiver(IOSpec decoder) {
			decoder.registerListener(this);
		}

		/**
		 * is the interface through which the MsgFilter delivers the RREvent.
		 * This routine probably should tell the enclosing Section that the
		 * alignment has changed.  Not doing so means that the dispatcher may
		 * have to click multiple times to make a change.
		 */
		public void acceptIOEvent() {
			if (!LockReports || !Locked) {
				do {
					if ( (++CurrentTrk) == PtsEdge.MAX_ROUTES) {
						CurrentTrk = 0;
					}
				}
				while (Routes[CurrentTrk] == null);
				PtsLogic.requestLocalMovement(CurrentTrk);
			}
		}
	}


	/**
	 * is an inner class for receiving reports that the manual controls on
	 * the switch machine have been unlocked.
	 */
	private class UnLockReceiver
	implements BiDecoderObserver {

		public UnLockReceiver() {
		}

		@Override
		public void acceptIOEvent() {
			Locked = false;
//			PtsLogic.SwitchUnLock.requestLockClear();
			PtsLogic.clearLocalLock(LogicLocks.SWITCHUNLOCK);
//			GuiEdge.clearEdgeLock(GuiLocks.SWITCHUNLOCK);
//			LockSymbol.showLock(false);
		}

		@Override
		public void acceptOtherIOEvent() {
			Locked = true;
//			PtsLogic.SwitchUnLock.requestLockSet();
			PtsLogic.setLocalLock(LogicLocks.SWITCHUNLOCK);
//			GuiEdge.setEdgeLock(GuiLocks.SWITCHUNLOCK);
//			LockSymbol.showLock(true);
		}
	}

	/**
	 * is an inner class for receiving reports that the manual controls on
	 * the switch machine have been locked.
	 */
	private class LockReceiver
	implements BiDecoderObserver {

		/**
		 * is the constructor.
		 *
		 * @param decoder is the IOSpec describing the messages that the receiver
		 * is looking for.
		 */
		public LockReceiver() {
		}

		@Override
		public void acceptIOEvent() {
			Locked = true;
//			PtsLogic.SwitchUnLock.requestLockSet();
			PtsLogic.setLocalLock(LogicLocks.SWITCHUNLOCK);
//			GuiEdge.setEdgeLock(GuiLocks.SWITCHUNLOCK);
//			LockSymbol.showLock(true);
		}

		@Override
		public void acceptOtherIOEvent() {
			Locked = false;
//			PtsLogic.SwitchUnLock.requestLockClear();
			PtsLogic.clearLocalLock(LogicLocks.SWITCHUNLOCK);
//			GuiEdge.clearEdgeLock(GuiLocks.SWITCHUNLOCK);
//			LockSymbol.showLock(false);
		}
	}

	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SwitchPoints.class.getName());
}

/**
 * is an inner class for constructing messages.
 */
class SwitchMessage
implements XMLEleObject {

	/**
	 * is the function of the IOSpec.
	 */
	public int Function = -1;

	/**
	 * is the IOSpec.
	 */
	public IOSpec Message;

	/**
	 * is the String for capturing the message.
	 */
	private String MsgName = null;

	/*
	 * is the method through which the object receives the text field.
	 *
	 * @param eleValue is the Text for the Element's value.
	 *
	 * @return if the value is acceptable, then null; otherwise, an error
	 * string.
	 */
	public String setValue(String eleValue) {
		MsgName = new String(eleValue);
		return null;
	}

	/*
	 * is the method through which the object receives embedded Objects.
	 *
	 * @param objName is the name of the embedded object
	 * @param objValue is the value of the embedded object
	 *
	 * @return null if the Object is acceptible or an error String
	 * if it is not.
	 */
	public String setObject(String objName, Object objValue) {
		String resultMsg = null;
		if (IOSpec.XML_TAG.equals(objName)) {
			Message = (IOSpec) objValue;
		}
		else {
			resultMsg = new String("A " + SwitchPoints.MSG_TAG +
					" cannot contain an Element ("
					+ objName + ").");
		}
		return resultMsg;

	}

	/*
	 * returns the XML Element tag for the XMLEleObject.
	 *
	 * @return the name by which XMLReader knows the XMLEleObject (the
	 * Element tag).
	 */
	public String getTag() {
		return new String(SwitchPoints.MSG_TAG);
	}

	/*
	 * tells the XMLEleObject that no more setValue or setObject calls will
	 * be made; thus, it can do any error chacking that it needs.
	 *
	 * @return null, if it has received everything it needs or an error
	 * string if something isn't correct.
	 */
	public String doneXML() {
		for (int i = 0; i < SwitchPoints.MessageName.length; ++i) {
			if (SwitchPoints.MessageName[i][SwitchPoints.MSGXML].equals(MsgName)) {
				Function = i;
				return null;
			}
		}
		return new String("A " + SwitchPoints.MSG_TAG +
				" cannot contain a text field ("
				+ MsgName + ").");
	}
}

/**
 * is a Class known only to the SwitchPoints class for creating SwitchPoints
 * from an XML document.
 */
class SwitchPointsFactory
implements XMLEleFactory {

	/**
	 * is true if a "Spur=true" attribute is found.
	 */
	boolean FoundSpur;

	/*
	 * tells the factory that an XMLEleObject is to be created.  Thus,
	 * its contents can be set from the information in an XML Element
	 * description.
	 */
	public void newElement() {
		FoundSpur = false;
	}

	/*
	 * gives the factory an initialization value for the created XMLEleObject.
	 *
	 * @param tag is the name of the attribute.
	 * @param value is it value.
	 *
	 * @return null if the tag:value are accepted; otherwise, an error
	 * string.
	 */
	public String addAttribute(String tag, String value) {
		String resultMsg = null;

		if (SwitchPoints.SPUR_TAG.equals(tag)) {
			if (Constants.TRUE.equals(value)) {
				FoundSpur = true;
			}
		}
		else {
			resultMsg = new String("A " + SwitchPoints.XML_TAG +
					" XML Element cannot have a " + tag +
					" attribute.");
		}
		return resultMsg;
	}

	/*
	 * tells the factory that the attributes have been seen; therefore,
	 * return the XMLEleObject created.
	 *
	 * @return the newly created XMLEleObject or null (if there was a problem
	 * in creating it).
	 */
	public XMLEleObject getObject() {
		return new SwitchPoints(FoundSpur);
	}
}

/**
 * is a Class known only to the SwitchPoints class for creating messages
 * from an XML document.
 */
class SwitchMessageFactory
implements XMLEleFactory {

	/*
	 * tells the factory that an XMLEleObject is to be created.  Thus,
	 * its contents can be set from the information in an XML Element
	 * description.
	 */
	public void newElement() {
	}

	/*
	 * gives the factory an initialization value for the created XMLEleObject.
	 *
	 * @param tag is the name of the attribute.
	 * @param value is it value.
	 *
	 * @return null if the tag:value are accepted; otherwise, an error
	 * string.
	 */
	public String addAttribute(String tag, String value) {
		String resultMsg = null;

		resultMsg = new String("A " + SwitchPoints.MSG_TAG +
				" XML Element cannot have a " + tag +
				" attribute.");
		return resultMsg;
	}

	/*
	 * tells the factory that the attributes have been seen; therefore,
	 * return the XMLEleObject created.
	 *
	 * @return the newly created XMLEleObject or null (if there was a problem
	 * in creating it).
	 */
	public XMLEleObject getObject() {
		return new SwitchMessage();
	}
}
/* @(#)SwitchPoints.java */