/* Name: CatsSignalMast.java
 *
 * What:
 *   This class is a bridge between a JMRI SIgnalMast and CATS vision of
 *   a physical signal - one on the layout.  Its purpose is to allow CATS
 *   to use JMRI SignalMast objects (e.g. DCCSignalMast), and at the same
 *   time, support CATS unique functions (e.g. hardware testing).
 *   <p>
 *   There is one of these for each physical signal.  They make the
 *   abstract physical signal concrete.
 */
package cats.layout.items;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Element;
//import org.python.jline.internal.Log;

import jmri.NamedBeanHandle;
import jmri.SignalAppearanceMap;
import jmri.SignalHead;
import jmri.SignalMast;
import jmri.implementation.AbstractSignalHead;
import jmri.implementation.DefaultSignalAppearanceMap;
import jmri.implementation.SignalHeadSignalMast;
import jmri.implementation.VirtualSignalMast;
import jmri.managers.DefaultSignalMastManager;
import cats.apps.Crandic;
import cats.common.Constants;
import cats.common.DebugBits;
import cats.common.Prop;
import cats.gui.TraceFactory;
import cats.gui.TraceFlag;
import cats.layout.AspectMap;
import cats.layout.SignalTemplate;
import cats.layout.TemplateStore;
import cats.layout.xml.XMLEleFactory;
import cats.layout.xml.XMLEleObject;
import cats.layout.xml.XMLReader;

/**
 *   This class is a bridge between a JMRI SIgnalMast and CATS vision of
 *   a physical signal - one on the layout.  Its purpose is to allow CATS
 *   to use JMRI SignalMast objects (e.g. DCCSignalMast), and at the same
 *   time, support CATS unique functions (e.g. hardware testing).
 *   <p>
 *   There is one of these for each physical signal.  They make the
 *   abstract physical signal concrete.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2009, 2011, 2012, 2014, 2017, 2018, 2020, 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class PhysicalSignal implements XMLEleObject, PropertyChangeListener {
	/**
	 * is the tag for identifying a PhysicalSignal in the XMl file.
	 */
	static final String XML_TAG = "PHYSIGNAL";

	/**
	 * is the attribute tag in an automated test Element for the indication rule
	 * (Appearance)
	 */
	private static final String RULE_TAG = "RULE";

	/**
	 * is the attribute tag in an automated test Element for the lit value
	 */
	private static final String LIT_TAG = "LIT";

	/**
	 * is the JMRI System Prefix for a SignalHeadSignalMast
	 */
	private static final String JMRI_SIGNALHEAD_PREFIX = "IF$shsm:";
	
	/**
	 * is the JMRI System Prefix for a VirtualSignalMast
	 */
	private static final String JMRI_VIRTUAL_PREFIX = "IF$vsm:";
	
	/**
	 * is the base part of a Signal Mast name, specific for CATS
	 */
	private static final String CATS_BASE = "Cats";
	
	/**
	 * is the list of all physical signals.
	 */
	private static List<PhysicalSignal> MastKeeper = new ArrayList<PhysicalSignal>();

	/**
	 * is the list of all SignalMasts formed from SignalHeads
	 */
	private static List<SignalHeadSignalMast> SignalHeadMasts = new ArrayList<SignalHeadSignalMast>();

	/**
	 * is where to find the value of the trace flag.
	 */
	private static TraceFlag TraceSignal = null;

	/**
	 * is a table for translating CATS signal presentation names to JMRI
	 * presentation names.
	 */
	private static final String AppearanceName[] = {
		"off",
		"green",
		"yellow",
		"red",
		"flashing green",
		"flashing yellow",
		"flashing red",
		"lunar",
		"flashing lunar",
		"horizontal",
		"diagonal",
		"vertical"
	};

	/**
	 * is the JMRI presentation value (@see jmri.SignalHead) that
	 * corresponds to each String.
	 */
	private static final int JMRIAppearance[] = {
		SignalHead.DARK,
		SignalHead.GREEN,
		SignalHead.YELLOW,
		SignalHead.RED,
		SignalHead.FLASHGREEN,
		SignalHead.FLASHYELLOW,
		SignalHead.FLASHRED,
		SignalHead.LUNAR,
		SignalHead.FLASHLUNAR,
		SignalHead.RED,
		SignalHead.YELLOW,
		SignalHead.GREEN
	};

	/**
	 * is a marker for meaning that CATS has not defined an appearance
	 * with the name.
	 */
	public static final int NO_SUCH_APPEARANCE = 0xff;

	/**
	 * is a counter for constructing unique CATS SignalMast names
	 */
	private static int NextMastNumber = 1;
	
	/**
	 * is a counter for constructing unique VirtualSignalMast names
	 */
	private static int VirtualMastNumber = 1;
	
	/**
	 * is a counter for constructing unique SignalHead user names
	 */
	private static int NextHeadNumber = 1;
	
	  //	/**
//	 * is (temporarily) the number of heads in the Signal.  This field
//	 * should come from the Signal Type object.
//	 */
//	private int Heads = 1;

	/**
	 * is the SignalTemplate that describes the SignalAppearanceMap keys
	 */
	private SignalTemplate Template;

	/**
	 * is the commands to the decoder.
	 */
	private AspectTable DecoderList;

//	/**
//	 * describes which Aspects are supported.  This can be null, if the
//	 * user defined the SignalMast in JMRI.
//	 */
//	private AspectMap MyMap;
	
	/**
	 * is true if the signal logic is using 4 aspect (advance) signals
	 */
	private boolean AdvanceSignals = false;
	
	/**
	 * is true if the signal logic is using approach lighting
	 */
	private boolean ApproachLighting = false;
	
	/**
	 * is true if CATS should not set aspects, but Hold/Unhold aspects
	 */
	private boolean HoldAspects = false;
	
	/**
	 * is the Rule that the last commands were sent for.
	 */
	private int LastRule = -1;

	/**
	 * is a flag set to true when in test mode.
	 */
	private static boolean SignalTest = false;

	/**
	 * is a flag set to true when the Signal is lit.
	 */
	private boolean SignalLit = true;
	
	/**
	 * is the JMRI User Name of the containing mast
	 */
	public String SignalName = "";

	/**
	 * is the JMRI SignalMast wrapped by this PhysicalSignal
	 */
	private SignalMast EmbeddedMast;
	
	/**
	 * is the containing SecSignal.  It is the link to the GUI icon
	 */
	private SecSignal SectionSignal;

	/************************************************************************
	 * the following are used in automated testing for determining if the state
	 * has changed or not.
	 ************************************************************************/
	/**
	 * the recorded value of LastRule
	 */
	private int RecordedLastRule = -1;

	/**
	 * the recorded value of SignalLit
	 */
	private boolean RecordedSignalLit = true;

	public PhysicalSignal() {
		EmbeddedMast = null;
	}

	/**
	 * is a predicate for determining if 4 aspect (Advance) signal
	 * aspects are being used
	 * @return true if they are and false if not
	 */
	public boolean isUsingAdvance() {
		return AdvanceSignals;
	}

	/**
	 * is a predicate for determining if the signal has approach lighting.
	 * By default, approach lighting means that the detection block in
	 * approach is occupied.  This should be generalized to use any
	 * IOSpec.
	 * @return true if the signal uses approach lighting
	 */
	public boolean isUsingApproachLighting() {
		return ApproachLighting;
	}
	
	/**
	 * is a predicate for determining if the signal is using logic external
	 * to CATS for setting aspects.
	 * @return true if external logic determines aspects
	 */
	public boolean isUsingCATSHold() {
		return HoldAspects;
	}
	
	/**
	 * retrieves the user defined trigger for approach lighting, if
	 * there is one.
	 * @return the trigger or null
	 */
	public IOSpec getApproachTrigger() {
		if (DecoderList != null) {
			return DecoderList.getApproachTrigger();
		}
		return null;
	}
	
	/**
	 * retrieves the name of the SignalTemplate.
	 *
	 * @return the name of the SignalTemplate so that it can be located
	 * in the TemplateStore.
	 */
	public String getTemplateName() {
		return new String(Template.getName());
	}

	/**
	 * sets the name of the SignalTemplate.
	 *
	 * @param template is the name of the SignalTemplate
	 */
	public void setTemplateName(String template) {
		Template = TemplateStore.SignalKeeper.find(template);
		if (Template == null) {
			org.apache.log4j.Logger.getLogger(PhysicalSignal.class.getName()).
			warn("Signal template " + template + " is not defined");
		}
		else {
			//		MyMap = TemplateStore.SignalKeeper.find(TemplateName).getAspectMap();
			AdvanceSignals = Template.getAspectMap().getAdvanced();
			ApproachLighting = Template.getAspectMap().getApproachLight();
			HoldAspects = Template.getAspectMap().getHoldOnly();
		}
	}

	//    /**
	//     * sets the decoder command list.
	//     *
	//     * @param dlist is the new list.
	//     *
	//     * @see AspectTable
	//     */
	//    public void setCommandTable(AspectTable dlist) {
	//      JmriDeviceList = dlist;
	//    }

	//  /**
	//   * changes the Signal on the layout.  Note: this routine needs to be
	//   * more robust.  It assumes that an aspect has been defined for each
	//   * indication that the signal will be asked to display.
	//   *
	//   * @param indication is the message to be conveyed by the Signal.
	//   */
	//  void setAspect(CompositeState indication) {
	//      int rule = (MyMap.getAdvanced()) ? indication.getAdvancedIndication() : indication.getIndication();
	//      setAspectByRule(rule);
	//  }

	//    /**
	//     * changes the Signal on the layout to the aspect associated with a particular
	//     * rule
	//     * @param rule is the index into the AspectMap.Map (signal template) for the desired aspect
	//     */
	//    public void setAspectByRule(int rule) {
	//  	    // Just in case an indication is not defined, find one more restrictive
	//  	    // that is defined
	//  	    for (; MyMap.getPresentation(rule, 0) == null; ++rule) {
	//  	    }
	//  	    if (!SignalTest && (rule != LastRule)) {
	//  		    /******************** DEBUG code *****************************/
	//  	  	  if (Crandic.Details) {
	//  	  	      String name = (SignalName == null) ? "unamed signal" : SignalName;
	//  	  	      if (LastRule > -1) {
	//  	  	    	  System.out.println(name + " changed from " + AspectMap.IndicationNames[LastRule][0] +
	//  	  	    			  " to " + AspectMap.IndicationNames[rule][0]);
	//  	  	      }
	//  	  	      else {
	//  	  	    	  System.out.println(name + " initialized to " + AspectMap.IndicationNames[rule][0]);
	//  	  	      }
	//  	  	  }
	//  	  	    /*************************************************************/
	//  	      sendSignalCommands(rule);
	//  	      LastRule = rule;
	//  	    }	  
	//    }

	/**
	 * performs the vital task of sending the name of the desired appearance
	 * to the SignalMast
	 * @param rule is the CATS value of the signal rule - the index of the
	 * appearance name
	 */
	public void setAspect(int rule) {
		if ((rule != LastRule) && (rule >= 0) && (rule < AspectMap.IndicationNames.length)) {
			if (!SignalTest) {
				if (TraceSignal.getTraceValue() || Crandic.Details.get(DebugBits.SIGNALBIT)) {
					String name = (SignalName == null) ? "unamed signal" : SignalName;
					if (LastRule > -1) {
						System.out.println(name + " changed from " + Template.getAspectKey(LastRule) +
								" (" + appearanceToAspects(AspectMap.IndicationNames[LastRule][AspectMap.XML]) +
								") to " + Template.getAspectKey(rule) +
								" (" + appearanceToAspects(AspectMap.IndicationNames[rule][AspectMap.XML]) + ")");
					}
					else {
						System.out.println(name + " initialized to " + Template.getAspectKey(rule) +
								" (" + appearanceToAspects(AspectMap.IndicationNames[rule][AspectMap.XML]) + ")");
					}
				}
				if (EmbeddedMast != null) {
					if (isUsingCATSHold()) {
						EmbeddedMast.setHeld(rule == AspectMap.STOP_INDEX);
					}
					else {
						try {
							EmbeddedMast.setAspect(Template.getAspectKey(rule));
						}
						catch(IllegalArgumentException iae) {
							String message = "indication " + Template.getAspectKey(rule) + " is not defined for mast " + SignalName;
							log.warn(message);
						}
					}
				}
			}
			LastRule = rule;
		}
	}

//	/**
//	 * sends the commands to the PhysicalSignal
//	 *
//	 * @param rule is the signal rule (aspect) being shown
//	 *
//	 * @see cats.layout.AspectMap
//	 */
//	private void sendSignalCommands(int rule) {
//		String state;
//		SignalHead sh;
//		String msg;
//		if (JmriDeviceList != null) {
//			for (int head = 0; head < Heads; ++head) {
//				state = MyMap.getPresentation(rule, head);
//				sh = JmriDeviceList.getHead(head);
//				if (sh == null) {
//					if ((SignalName != null) && (SignalName.length() > 0)) {
//						msg = "Signal " + SignalName ;
//					}
//					else {
//						msg = "PhysicalSignal: a " + TemplateName + " signal";
//					}
//					log.warn(msg +  " is missing the decoder definitions for "
//							+ state);
//				}
//				else {
//					int appearance = toJMRI(state);
//					if (appearance == NO_SUCH_APPEARANCE) {
//						if ((SignalName != null) && (SignalName.length() > 0)) {
//							msg = "Signal " + SignalName;
//						}
//						else {
//							msg = "PhysicalSignal: " ;
//						}
//						log.warn(msg + " does not have a JMRI appearance for " +
//								state);
//					}
//					else {
//						sh.setAppearance(appearance);
//						if (TraceSignal.getTraceValue()) {
//							System.out.println(SignalName + " head " + (head + 1) +
//									" indication is " +
//									AspectMap.IndicationNames[rule][AspectMap.
//									                                LABEL] +
//									                                " color is " + state);
//						}
//					}
//				}
//			}
//		}
//	}

	/**
	 * turns the signal on or off for approach lighting.
	 * 
	 * @param lit is true for on and false for off.
	 */
	public void lightUpSignal(boolean lit) {
		//      SignalHead decoders;
		SignalLit = lit;
		//      if (MyMap.getApproachLight()) {
		//        if (JmriDeviceList != null) {
		//          for (int head = 0; head < Heads; ++head) {
		//            decoders = JmriDeviceList.getHead(head);
		//            if (decoders != null) {
		//              decoders.setLit(lit);
		//            }
		//          }
		//        }
		//      }
		if ((EmbeddedMast != null) && Template.getAspectMap().getApproachLight()) {
			EmbeddedMast.setLit(lit);
		}
	}
	
	/**
	 * refreshes the signals on the layout
	 */
	public void refresh() {
		if (EmbeddedMast != null) {
			if (LastRule == -1) {
//				EmbeddedMast.setAspect(AspectMap.IndicationNames[AspectMap.IndicationNames.length - 1][AspectMap.XML]);
				EmbeddedMast.setAspect(Template.getAspectKey(AspectMap.STOP_INDEX));
			}
			else {
//				EmbeddedMast.setAspect(AspectMap.IndicationNames[LastRule][AspectMap.XML]);
				EmbeddedMast.setAspect(Template.getAspectKey(LastRule));
			}
			lightUpSignal(SignalLit);
		}
	}

	//    /**
	//     * puts the PhysicalSignal in a test mode.  It stays in the test mode
	//     * until it the refresh() method is invoked.
	//     *
	//     * @param testInd is the Indication being tested.
	//     */
	//    public void testSignal(int testInd) {
	//    	SignalTest = true;
	//    	EmbeddedMast.setAspect(AspectMap.IndicationNames[testInd][AspectMap.XML]);
	//    	EmbeddedMast.setLit(true);
	//    }

//	/**
//	 * sets all the heads on a mast to a particular aspect.  If a head does
//	 * not support that presentation, then the presentation is ignored.
//	 *
//	 * @param aspect is the presentation being requested.  It is a string
//	 * describing the color or oreientation of the signal.
//	 */
//	private void testSignal(String aspect) {
//		SignalHead decoders;
//		if (JmriDeviceList != null) {
//			for (int head = 0; head < Heads; ++head) {
//				decoders = JmriDeviceList.getHead(head);
//				if (decoders != null) {
//					decoders.setLit(true);
//					decoders.setAppearance(toJMRI(aspect));
//					if (TraceSignal.getTraceValue()) {
//						System.out.println(SignalName + ", head " + head +
//								": color is " + aspect);
//					}
//				}
//			}
//		}
//	}

	/**
	 * tells the PhysicalSignal the name of the signal.  This code is kind of convoluted.
	 * It attempts to find an externally defined SignalMast in JMRI with the user name
	 * the method is called with.  If the SignalMast is found, then it is used as the
	 * CATS SignalMast, remembered in the list of Cats SignalMasts, and is queried to
	 * determine if it is using advance signals or not.  The latter operation is
	 * accomplished by trying to get the advance aspects for clear out of the AppearanceMap.
	 * If AppearanceMap has no advance aspects, then the AppearanceMap will throw an
	 * IllegalArgumentException, which CATS catches.
	 * <p>
	 * If no SignalMast with the requested User name has been defined, then CATS will create one
	 * with that naem from the enclosed SignalHeads, when requested.
	 * <p>
	 * Finally, if the name is null or empty, CATS will generate a unique name for later use.
	 * @param name is the name of the signal
	 */
	public void setName(String name) {
		//      SignalName = name;
		//      if (JmriDeviceList != null) {
		//        JmriDeviceList.setSignalName(SignalName);
		//        Heads = JmriDeviceList.getListCount();
		//      }
		SignalName = new String(name);
	}

	/**
	 * establishes the linkage to the containing SecSignal, for
	 * changing the icon
	 * @param sig is the containing SecSignal
	 */
	public void setSecSignal(final SecSignal sig) {
		SectionSignal = sig;
	}
	
	/**
	 * Translates the name of an appearance to the aspect
	 * Strings for that appearance on this SignalMast.
	 * The method accesses the associated AppearanceMap
	 * to assist in the translation.  It is possible
	 * that the appearance is not defined.
	 * @param appearance is the name of the appearance being requested
	 * @return a String of the aspects
	 */
	private String appearanceToAspects(String appearance) {
		StringBuilder aspectNames = new StringBuilder();
		SignalAppearanceMap map;
		int aspects[];
		int count = 0;
		if (EmbeddedMast != null) {
			map = EmbeddedMast.getAppearanceMap();
			if (map.checkAspect(appearance) && (map instanceof DefaultSignalAppearanceMap)) {
				aspects = ((DefaultSignalAppearanceMap)map).getAspectSettings(appearance);
				aspectNames.append(AbstractSignalHead.getDefaultStateName(aspects[0]));
				while ((++count) < aspects.length) {
					aspectNames.append(" over ");
					aspectNames.append(AbstractSignalHead.getDefaultStateName(aspects[count]));
				}
				return aspectNames.toString();
			}
		}
		return "";
	}

	/**
	 * If the SignalMast has not been defined, then this method
	 * converts the physical signal description to a JMRI SignalHeadSignalMast and
	 * adds the mast to the global mast and signal head mast tables.
	 * @return the SignalMast
	 */
	public SignalMast getSignalMast() {
//		DefaultSignalMastManager dsmm = (DefaultSignalMastManager)jmri.InstanceManager.signalMastManagerInstance();
		DefaultSignalMastManager dsmm = (DefaultSignalMastManager)jmri.InstanceManager.getDefault(jmri.SignalMastManager.class);
		String sSystemName = Template.getSignalSystemName() + ":" + Template.getName();
		if (dsmm == null) {
			log.fatal("JMRI SignalMastManager does not exist");
		}
		else {
			if ((SignalName == null) || SignalName.isEmpty()) {
				SignalName =  CATS_BASE + (NextMastNumber++);
				while (dsmm.getSignalMast(SignalName) != null) {
					SignalName = CATS_BASE + (NextMastNumber++);
				}
			}
			else {
//				EmbeddedMast = dssm.getByUserName(SignalName);
				if (Crandic.Details.get(DebugBits.SIGNALBIT)) {
					System.out.println("Looking for SignalMast with user name " + SignalName);
				}
				EmbeddedMast = dsmm.getSignalMast(SignalName);
				if (EmbeddedMast != null) {
					MastKeeper.add(this);
					EmbeddedMast.addPropertyChangeListener(this);
				}
			}
//			if ((EmbeddedMast == null) && (JmriDeviceList != null)) {
			if (EmbeddedMast == null) {

				StringBuilder mastName;
				String headName;
				Template.getAppearanceMap();
//				if (headCount == 0) {
				if ((DecoderList == null) || !DecoderList.hasSignalHeads()) {
					mastName = new StringBuilder(JMRI_VIRTUAL_PREFIX + sSystemName + "($"
							+ VirtualMastNumber++ + ")");
					EmbeddedMast = new VirtualSignalMast(mastName.toString(), SignalName);
				}
				else {
					DecoderList.setSignalName(SignalName);
					mastName = new StringBuilder(JMRI_SIGNALHEAD_PREFIX + sSystemName);
					for (int head = 0; head < DecoderList.getListCount(); head++) {
						headName = DecoderList.getHead(head).getUserName();
//						if (headName == null) {
//							org.apache.log4j.Logger.getLogger(PhysicalSignal.class.getName()).
//								warn("Signal mast " + SignalName + " head " + head + " missing a user name.");
//						}
//						else {
//							mastName.append("(" + headName + ")");
//						}
						if (headName == null) {
							org.apache.log4j.Logger.getLogger(PhysicalSignal.class.getName()).
							warn("Signal mast " + SignalName + " head " + head + " missing a user name.");
							headName = "$" + Integer.toString(NextHeadNumber++);
							DecoderList.getHead(head).setUserName(headName);
						}
						mastName.append("(" + headName + ")");
					}

					EmbeddedMast = new SignalHeadSignalMast(mastName.toString(), SignalName);
					if (EmbeddedMast != null) {
						SignalHeadMasts.add((SignalHeadSignalMast) EmbeddedMast);
					}
				}
				if (EmbeddedMast != null) {
					if (Crandic.Details.get(DebugBits.SIGNALBIT)) {
						System.out.println("Adding signal mast " + EmbeddedMast.getSystemName() +
								" (" + SignalName + " )");
					}
					MastKeeper.add(this);
					dsmm.register(EmbeddedMast);
				}
				else {
					log.warn("Signal mast " + mastName + "(" + SignalName +
							") could not be created from CATS definition");
				}
			}
		}
		return EmbeddedMast;
	}
	
	/*
	 * is the method through which the object receives the text field.
	 *
	 * @param eleValue is the Text for the Element's value.
	 *
	 * @return if the value is acceptable, then null; otherwise, an error
	 * string.
	 */
	public String setValue(String eleValue) {
		setTemplateName(eleValue);
		return null;
	}

	/*
	 * is the method through which the object receives embedded Objects.
	 *
	 * @param objName is the name of the embedded object
	 * @param objValue is the value of the embedded object
	 *
	 * @return null if the Object is acceptible or an error String
	 * if it is not.
	 */
	public String setObject(String objName, Object objValue) {
		String resultMsg = null;
		if (AspectTable.XML_TAG.equals(objName)) {
			DecoderList = (AspectTable) objValue;
		}
		else {
			resultMsg = new String("A " + XML_TAG + " cannot contain an Element ("
					+ objName + ").");
		}
		return resultMsg;
	}

	/*
	 * returns the XML Element tag for the XMLEleObject.
	 *
	 * @return the name by which XMLReader knows the XMLEleObject (the
	 * Element tag).
	 */
	public String getTag() {
		return new String(XML_TAG);
	}

	/*
	 * tells the XMLEleObject that no more setValue or setObject calls will
	 * be made; thus, it can do any error chacking that it needs.
	 *
	 * @return null, if it has received everything it needs or an error
	 * string if something isn't correct.
	 */
	public String doneXML() {
//		if (TemplateName != null) {
//			setTemplateName(TemplateName);
//		}
		return null;
	}

	/**
	 * registers a PhySignalFactory with the XMLReader.
	 */
	static public void init() {
		XMLReader.registerFactory(XML_TAG, new CatsSignalMastFactory());
		AspectTable.init();
		TraceSignal = TraceFactory.Tracer.createTraceItem("Signal Changes", "SIG_TRACE");

	}

	/**
	 * puts all signals in a test mode until the panel is refreshed.
	 *
	 * @param testMode is the Indication rule being tested.
	 */
	public static void signalTest(int testMode) {
		int count = 0;
		SignalTest = true;
		for (PhysicalSignal mast : MastKeeper) {
			mast.EmbeddedMast.setAspect(mast.Template.getAspectKey(testMode));
			mast.EmbeddedMast.setLit(true);

			if ( (++count) == 10) {
				count = 0;

				// Sleep a while so the serial port is not overwhelmed.
				try {
					Thread.sleep(5);
				}
				catch (InterruptedException ie) {
					break;
				}
			}
		}
	}

	/**
	 * puts all the signals in a test mode until the panel is refreshed.
	 *
	 * @param testMode is the color or orientation being tested.
	 */
	public static void signalTest(String testMode) {
		int count = 0;
		SignalHead head;
		//      for (Enumeration<PhysicalSignal> e = SignalKeeper.elements(); e.hasMoreElements(); ) {
		//        e.nextElement().testSignal(testMode);
		for (SignalHeadSignalMast sm : SignalHeadMasts) {
			List<NamedBeanHandle<SignalHead>> heads = sm.getHeadsUsed();
			for (NamedBeanHandle<SignalHead> handle : heads) {
				head = handle.getBean();
				head.setAppearance(toJMRI(testMode));
				head.setLit(true);
			}
			if ( (++count) == 10) {
				count = 0;

				// Sleep a while so the serial port is not overwhelmed.
				try {
					Thread.sleep(5);
				}
				catch (InterruptedException ie) {
					break;
				}
			}
		}
	}

	/**
	 * translates a CATS appearance String to a JMRI appearance constant
	 * @param appearance is the name of the appearance in CATS (see AppearanceName)
	 * @return the JMRI appearance constant that it corresponds to.  0xff means there
	 * is no translation.
	 */
	public static int toJMRI(String appearance) {
		int index = Prop.findString(appearance, AppearanceName);
		if (index == Constants.NOT_FOUND) {
			return NO_SUCH_APPEARANCE;
		}
		return JMRIAppearance[index];
	}

	/**
	 * searches for a JMRI appearance constant and returns the CATS
	 * name for the constant.
	 * 
	 * @param appearance is the JMRI appearance constant
	 * @return the matching CATS name.  A special string is returned
	 * if the constant cannot be found.
	 */
	public static String toCATS(int appearance) {
		int index =Prop.findInt(appearance, JMRIAppearance);
		if (index == Constants.NOT_FOUND) {
			return "Appearance not found";
		}
		return new String(AppearanceName[index]);
	}

	/**
	 * records the signal's state in an XML Element for automated testing.
	 * @param basic is true to record only basic state information.  It is false
	 * to record detailed state information.
	 * @return an Element, if the state has changed since the last snapshot; otherwise,
	 * null.
	 */
	public Element dumpSignalState(boolean basic) {
		boolean changed = false;
		Element newState = new Element(XML_TAG);
		if (LastRule != RecordedLastRule) {
			newState.setAttribute(RULE_TAG, String.valueOf(LastRule));
			RecordedLastRule = LastRule;
			changed = true;
		}
		if (SignalLit != RecordedSignalLit) {
			newState.setAttribute(LIT_TAG, String.valueOf(SignalLit));
			RecordedSignalLit = SignalLit;
			changed = true;
		}
		if (changed) {
			if (!basic && (SignalName != null)) {
				newState.setAttribute("NAME", SignalName);
			}
			return newState;
		}
		return null;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if ((evt != null) && "Aspect".equals(evt.getPropertyName())) {
			int key = Template.getAspectKey((String) evt.getNewValue());
			if (key != -1) {
				SectionSignal.setAspectforRule(key);
			}
		}
	}
	
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PhysicalSignal.class.getName());

}
/**
 * is a Class known only to the CatsSignalMast class for creating CatsSignalMasts from
 * an XML document.
 */
class CatsSignalMastFactory
implements XMLEleFactory {
  
  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
  }
  
  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;
    
    resultMsg = new String("A " + PhysicalSignal.XML_TAG +
        " XML Element cannot have a " + tag +
    " attribute.");
    return resultMsg;
  }
  
  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    return new PhysicalSignal();
  }
}
