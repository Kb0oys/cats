/* Name: FrogEdge.java
 *
 * What:
 *   This class is the container for all the information about one
 *   of the ends of a track that attaches to points. It is derived directly
 *   from AbstractTrackEdge and is used for forwarding information to its associated PtsEdge.
 *   <p>
 *   The FrogEdge may be fouled or aligned.  If aligned, the following are true:
 *   <ul>
 *   <li>Joint is not null
 *   <li>In this state, the FrogEdge operates as its parent class (AbstractTrackEdge), except
 *   it does not receive opposing traffic messages from its neighbor directly, but through
 *   the associated PtsEdge.  This should be transparent.  The only lock that the FrogEdge injects
 *   is CONFLICTINGSIGNAL.  Thus, no merging is performed except for CONFLICTINGSIGNAL.
 *   <li>Messages received from the Peer are forwarded through the PtsEdge.  No locks are
 *   injected.
 *   </ul>
 *   <p>
 *   If fouling, the following are true:
 *   <ul>
 *   <li>Joint is set to null, to prevent the state from being forwarded.
 *   <li>Messages received from Peer are queued, to be forwarded when the points move.
 *   </ul>
 *   Not all locks are forwarded when the poins move.  ENTRY and EXIT do not because
 *   when points move in the middle of a route, the route is cancelled.
 */
package cats.layout.items;

import java.util.EnumSet;

import cats.apps.Crandic;
import cats.common.DebugBits;
import cats.common.Sides;
import cats.layout.codeLine.CodeMessage;
import cats.layout.ctc.RouteNode;
import cats.layout.vitalLogic.BasicVitalLogic;
import cats.layout.vitalLogic.FrogVitalLogic;
import cats.layout.vitalLogic.PtsVitalLogic;

/**
 *   This class is the container for all the information about one
 *   of the ends of a track that attaches to points. It is derived directly
 *   from AbstractTrackEdge and is used for forwarding information to its associated PtsEdge.
 *   <p>
 *   The FrogEdge may be fouled or aligned.  If aligned, the following are true:
 *   <ul>
 *   <li>Joint is not null
 *   <li>In this state, the FrogEdge operates as its parent class (AbstractTrackEdge), except
 *   it does not receive opposing traffic messages from its neighbor directly, but through
 *   the associated PtsEdge.  This should be transparent.  The only lock that the FrogEdge injects
 *   are CONFLICTINGSIGNAL and SWITCHUNLOCK.  Thus, no merging is performed except for CONFLICTINGSIGNAL
 *   and SWITCHUNLOCK.
 *   <li>Messages received from the Peer are forwarded through the PtsEdge.  No locks are
 *   injected.
 *   </ul>
 *   <p>
 *   If fouling, the following are true:
 *   <ul>
 *   <li>Joint is set to null, to prevent the state from being forwarded.
 *   <li>Messages received from Peer are queued, to be forwarded when the points move.
 *   </ul>
 *   Not all locks are forwarded when the points move.  ENTRY and EXIT do not because
 *   when points move in the middle of a route, the route is cancelled.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2009, 2011, 2012, 2014, 2018, 2019, 2021, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class FrogEdge extends AbstractTrackEdge implements SignalEdge, RouteEdge {

	/**
	 * is the PtsEdge that multiplexes information from the approach side
	 */
	private PtsEdge Shadow;

//	/**
//	 * the ProxySignal in MyLogic
//	 */
//	private ProxySignal SignalLogic;

//	/**
//	 * the approach TrackEdge in the Opposing direction
//	 */
//	private AbstractTrackEdge OpposingApproachEdge;

	/**
	 * The slowest speed calculated by merging this speed with
	 * the AdvanceSpeed
	 */
	protected int MergedSpeed = Track.NORMAL;

	/*******************************************************************************
	 * the following are mirror images of the indication handling from the Peer, but
	 * from the PtsEdge
	 ********************************************************************************/

	/**
	 * the set of Edge Locks from the PtsEdge
	 */
	private EnumSet<GuiLocks> PtsEdgeLocks = EnumSet.noneOf(GuiLocks.class);

	/**
	 * the set of Common Locks from the PtsEdge
	 */
	private EnumSet<GuiLocks>	PtsCommonLocks = EnumSet.noneOf(GuiLocks.class);

//	/**
//	 * the set of Lock filters received from the Peer and sent through the Points
//	 */
//	private EnumSet<GuiLocks>	PtsFilters = EnumSet.noneOf(GuiLocks.class);
	
	/**
	 * the side on which the associated track terminates, used for identifying the FrogEdge
	 */
	private final int FrogIndex;

	/**
	 * is the preferred ctor
	 * @param shadow is the PtsEdge for handling information from the approach side.
	 * @param index is the integer identifying which side of the points the frog is on
	 */
	public FrogEdge(final PtsEdge shadow, final int index) {
		super(shadow.MyEdge, null);
		FrogIndex = index;
		MySection = shadow.MySection;
		Shadow = shadow;
		createVitalLogic();
		DefaultTrackEdge.addTrackEdge(this);

//		DefaultTrackEdge.addSignalEdge(this);
//		setHandlers();
	}

	@Override
	public EdgeInterface.EDGE_TYPE getEdgeType() {
		return EdgeInterface.EDGE_TYPE.FROG_EDGE;
	}
	
	/**
	 * forwards the block information to the PtsEdge so that it can distribute
	 * it down all the tracks.
	 */
	public void propagateBlock(Block block) {
		Shadow.propagateBlock(block);
	}

	/**
	 * tests if the SecEdge is fouling or not
	 * @return true if it is fouling and false if not.
	 */
	public boolean isFoulingEdge() {
		return Joint == null;
	}

	/**
	 * is invoked to change what the neighbor SecEdge is.  This is used by Points
	 * to change the fouling and non-fouling track.
	 * @param newJoint is the neighbor, for forwarding state in the opposite
	 * direction.  If null, then the track is fouling
	 */
	public void setJoint(SecEdge newJoint) {
		EnumSet<GuiLocks> transfer;
		Joint = newJoint;
		if (newJoint == null) {
			Destination.setGap(MyEdge, true);
//			for (GuiLocks l : CommonLocks) {
//				Shadow.clearPointsCommonLock(l);
//			}
//			for (GuiLocks l : EdgeLocks) {
//				Shadow.clearPointsEdgeLock(l);
//			}
//			Shadow.clearPtsFilters(PtsFilters);
			transfer = GuiLocks.intersection(CommonLocks, GuiLocks.GUISwitchLocks);
			for (GuiLocks lock : transfer) {
				Shadow.clearPointsCommonLock(lock);
			}
		}
		else {
			Destination.setGap(MyEdge, false);
//			Shadow.setPtsFilters(PtsFilters);
//			for (GuiLocks l : CommonLocks) {
//				Shadow.setPointsCommonLock(l);
//			}
			transfer = GuiLocks.intersection(CommonLocks, GuiLocks.GUISwitchLocks);
			for (GuiLocks lock : transfer) {
				Shadow.setPointsCommonLock(lock);
			}
//			for (GuiLocks l : EdgeLocks) {
//				Shadow.setPointsEdgeLock(l);
//			}
		}
	}

	@Override
	/**
	 * returns the SecEdge sharing the edge.  In this case, the neighbor
	 * is the PtsEdge that this FrogEdge overlays.  Finding its 
	 * Peer returns this FrogEdge (if the points are aligned for this route).
	 * Therefore, the "real" neighbor is the PtsEdge neighbor.
	 *
	 * @return the SecEdge that shares the edge.
	 *
	 */
	public SecEdge getNeighbor() {
		if (Joint != null) {
			return Joint.Joint;
		}
		return null;
	}

	/**************************************************************************
	 * the implementation of TrackEdge
	 **************************************************************************/
	@Override
	public boolean isBlkEdge() {
		return false;
	}

	/**
	 * This method starts the probe for the TrackEdge in approach; however, it is acting
	 * as a surrogate for the PtsEdge.  It probes in the direction of travel, rather than
	 * against it.  When the PtsEdge receives an update entering through the points end,
	 * the update is sent to the TrackEdge discovered through this probe.
	 */
	@Override
	public void discoverAdvanceVitalLogic() {
//		SecEdge nextEdge = Peer;
//		int speed = Track.NORMAL;
		MyLogic.setIdentity(toString());
		MyLogic.tailorDiscipline(MyBlock.getDiscipline());
//		do {
//			TrackSpan.add(nextEdge.Destination);
//			speed = Track.getSlower(speed, nextEdge.Destination.getSpeed());
//			if (nextEdge instanceof TrackEdge) {  // the normal case - the next TrackEdge in the opposite direction
////				OpposingApproachEdge = (AbstractTrackEdge) nextEdge;
////				MyLogic.setPeerLogic(OpposingApproachEdge.getVitalLogic());
//				OppositeEnd = (AbstractTrackEdge) nextEdge;
//				MyLogic.setPeerLogic(OppositeEnd.getVitalLogic());
//				nextEdge = ((TrackEdge)nextEdge).nextTrackEdge();
//				if ((nextEdge != null) && (nextEdge instanceof TrackEdge)) {
//					((TrackEdge) nextEdge).setApproachEdge(this);
//				}
//				nextEdge = null;
//			}
//			else {  // see if the neighbor is switch points
//				nextEdge = nextEdge.getNeighbor();
//				if (nextEdge != null) {
//					if (nextEdge instanceof PtsEdge) {
////						OpposingApproachEdge = (AbstractTrackEdge) nextEdge;
////						MyLogic.setPeerLogic(OpposingApproachEdge.getVitalLogic());
//						OppositeEnd = (AbstractTrackEdge) nextEdge;
//						MyLogic.setPeerLogic(OppositeEnd.getVitalLogic());
//						((TrackEdge) nextEdge).setApproachEdge(this);
//						nextEdge = null;
//					}
//					else {
//						nextEdge = nextEdge.Peer;
//					}
//				}
//			}
//		} while (nextEdge != null);		
		//		mergeAdvanceSpeed(AdvanceSpeed);
		MyLogic.setSpeed(trackSegmentAggregator(this));
		if (OppositeEnd != null) {
			MyLogic.setPeerLogic(OppositeEnd.getVitalLogic());
		}
		MyLogic.primeApproach();
//		if (Joint == null) {
//			setFrogEdgeLock(GuiLocks.CONFLICTINGSIGNALLOCK);
//		}
	}

	@Override
	public SignalEdge signalEdgeProbe() {
		return this;
	}

	@Override
	public TrackEdge nextTrackEdge() {
		return this;
	}

	/**
	 * retrieves the VitalLogic associated with the TrackEdge
	 * @return the VitalLogic
	 */
	public BasicVitalLogic getVitalLogic() {
		return MyLogic;
	}

	@Override
	public CodeMessage processFieldResponse(String response) { 
		// this should never be called for a FrogEdge
		return new CodeMessage(response);
	}

	@Override
	public void setApproachEdge(AbstractTrackEdge approach) {
//		if (approach != null) {
//			MyLogic.setApproachLogic(approach.getVitalLogic());
//		}
	}

	@Override
	public BasicVitalLogic createVitalLogic() {
		MyLogic = new FrogVitalLogic((PtsVitalLogic) Shadow.getVitalLogic(), toString());

//		MyLogic = new FrogVitalLogic(new EdgePacketFactory(this), (PtsVitalLogic) Shadow.getVitalLogic(), toString());
//		SignalLogic = new ProxySignal(new FrogCircuitRelay());
//		((FrogVitalLogic) MyLogic).setSignalHead(SignalLogic);
		return MyLogic;
	}

//	@Override
//	public void setDirection(DirectionOfTravel dot) {
//		// this should never be called for a FrogEdge
//	}

	@Override
	public boolean isLockSet(GuiLocks lock) {
		// This should never be called for a FrogEdge
		return false;
	}

//	@Override
//	public EnumSet<GuiLocks> getEdgeLocks() {
//		return EdgeLocks;
//	}
//	
//	@Override
//	public EnumSet<GuiLocks> getCommonLocks() {
//		return CommonLocks;
//	}
	
	/**
	 * gets the associated points so that they can be found, even if not
	 * lined for the Frog.
	 * @return Shadow
	 */
	public AbstractTrackEdge getPoints() {
		return Shadow;
	}
	
	/**
	 * gets the side of the frog's track
	 * @return the index into the points frog array containing this FrogEdge
	 */
	public int getIndex() {
		return FrogIndex;
	}
	
//	@Override
//	protected void setFilters(EnumSet<GuiLocks> filters) {
//		if (Joint != null) {
//			Shadow.setPtsFilters(filters);
//		}
//		PtsFilters.addAll(filters);
//	}

//	@Override
//	protected void clearFilters(EnumSet<LogicLocks> filters) {
//		if (Joint != null) {
//			Shadow.clearPtsFilters(filters);
//		}
//		PtsFilters.removeAll(filters);
//	}

//	/**
//	 * is the counterpart to setFilters, but for handling
//	 * a filter received through the Points
//	 * @param filters is the set of locks that are blocked
//	 */
//	protected void setFrogFilters(EnumSet<LogicLocks> filters) {
//		if (OpposingApproachEdge != null) {
//			OpposingApproachEdge.setFilters(filters);
//		}
//	}

	/**
	 * is the counterpart to clearFilters, but for handling
	 * a filter received through the Points
	 * @param filters is the set of locks that are blocked
	 */
//	protected void clearFrogFilters(EnumSet<LogicLocks> filters) {
//		if (OpposingApproachEdge != null) {
//			OpposingApproachEdge.clearFilters(filters);
//		}
//	}

//	@Override
//	protected void setCommonLock(LogicLocks lock) {
//		if (!CommonLocks.contains(lock)) {
//			if (Joint != null) {
//				Shadow.setPointsCommonLock(lock);
//			}
//			CommonLocks.add(lock);
//		}
//	}
//
//	@Override
//	protected void clearCommonLock(LogicLocks lock) {
//		if (CommonLocks.contains(lock)) {
//			if (Joint != null) {
//				Shadow.clearPointsCommonLock(lock);
//			}
//			CommonLocks.remove(lock);
//		}
//	}

//	@Override
//	protected void setEdgeLock(GuiLocks lock) {
//		if (!EdgeLocks.contains(lock)) {
//			if (Joint != null) {
//				Shadow.setPointsEdgeLock(lock);
//			}
//			EdgeLocks.add(lock);
//		}
//	}
//
//	@Override
//	protected void clearEdgeLock(GuiLocks lock) {
//		if (EdgeLocks.contains(lock)) {
//			if (Joint != null) {
//				Shadow.clearPointsEdgeLock(lock);
//			}
//			EdgeLocks.remove(lock);
//		}
//	}

	/**
	 * is the counterpart to setCommonLock(), as invoked
	 * from the Points edge
	 * @param lock is the lock being set
	 */
	protected void setFrogCommonLock(GuiLocks lock) {
		if (!PtsCommonLocks.contains(lock)) {
			if (OppositeEnd != null) {
				OppositeEnd.setCommonLock(lock);
			}
			EnumSet<GuiLocks> oldLocks = PtsCommonLocks.clone();
			PtsCommonLocks.add(lock);
			// the sense of entry/exit gets flipped going through the frog
			if (lock.equals(GuiLocks.EXITLOCK)) {
				paintTrackSequence(RouteDecoration.SET_ROUTE);
			}
			/******************  DEBUG code ******************************/
			if (Crandic.Details.get(DebugBits.GUILOCKBIT)) {
				System.out.println("Frog Edge " + toString() + " common locks were " + oldLocks + " now " + PtsCommonLocks.toString());			
			}
			/****************** end DEBUG code ******************************/
		}
	}

	/**
	 * is the counterpart to clearCommonLock(), as invoked
	 * from the Points edge
	 * @param lock is the lock being set
	 */
	protected void clearFrogCommonLock(GuiLocks lock) {
		if (PtsCommonLocks.contains(lock)) {
			if (OppositeEnd != null) {
				OppositeEnd.clearCommonLock(lock);
			}
			EnumSet<GuiLocks> oldLocks = PtsCommonLocks.clone();
			PtsCommonLocks.remove(lock);
			if (lock.equals(GuiLocks.EXITLOCK)) {
				paintTrackSequence(RouteDecoration.CLEAR_ROUTE);
			}
			/******************  DEBUG code ******************************/
			if (Crandic.Details.get(DebugBits.GUILOCKBIT)) {
				System.out.println("Frog Edge " + toString() + " common locks were " + oldLocks + " now " + PtsCommonLocks.toString());			
			}
			/****************** end DEBUG code ******************************/
		}
	}

	/**
	 * is the counterpart to setEdgeLock(), as invoked
	 * from the Points edge.  Except for CONFLICTINGSIGNALLOCK, neither the
	 * points nor frogs should call this method.  The points convert all of
	 * its EdgeLocks to CommonLocks.
	 * @param lock is the lock being set
	 */
	protected void setFrogEdgeLock(GuiLocks lock) {
		if (!PtsEdgeLocks.contains(lock)) {
			if (OppositeEnd != null) {
				OppositeEnd.setEdgeLock(lock);
			}
			EnumSet<GuiLocks> oldLocks = PtsEdgeLocks.clone();
			PtsEdgeLocks.add(lock);
			if (lock.equals(GuiLocks.EXITLOCK)) {
				paintTrackSequence(RouteDecoration.SET_ROUTE);
			}
			/******************  DEBUG code ******************************/
			if (Crandic.Details.get(DebugBits.GUILOCKBIT)) {
				System.out.println("Frog Edge " + toString() + " points edge locks were " + oldLocks + " now " + PtsEdgeLocks.toString());			
			}
			/****************** end DEBUG code ******************************/
		}
	}

	/**
	 * is the counterpart to clearEdgeLock(), as invoked
	 * from the Points edge.  Except for CONFLICTINGSIGNALLOCK, neither the
	 * points nor frogs should call this method.  The points convert all of
	 * its EdgeLocks to CommonLocks.
	 * @param lock is the lock being set
	 */
	protected void clearFrogEdgeLock(GuiLocks lock) {
		if (PtsEdgeLocks.contains(lock)) {
			if (OppositeEnd != null) {
				OppositeEnd.clearEdgeLock(lock);
			}
			EnumSet<GuiLocks> oldLocks = PtsEdgeLocks.clone();
			PtsEdgeLocks.remove(lock);
			if (lock.equals(GuiLocks.EXITLOCK)) {
				paintTrackSequence(RouteDecoration.CLEAR_ROUTE);
			}
			/******************  DEBUG code ******************************/
			if (Crandic.Details.get(DebugBits.GUILOCKBIT)) {
				System.out.println("Frog Edge " + toString() + " points edge locks were " + oldLocks + " now " + PtsEdgeLocks.toString());			
			}
			/****************** end DEBUG code ******************************/
		}
	}

	@Override
	public void startFeeding() {
		if (getVitalLogic() != null) {
			getVitalLogic().primeApproach();
		}
		if (Joint == null) {
			setFrogEdgeLock(GuiLocks.CONFLICTINGSIGNALLOCK);
		}
		else {
			clearFrogEdgeLock(GuiLocks.CONFLICTINGSIGNALLOCK);
		}
	}

	/**************************************************************************
	 *  end of TrackEdge implementation
	 **************************************************************************/
	
	@Override
	public void resetLocks() {
		super.resetLocks();
		if (PtsCommonLocks.contains(GuiLocks.ENTRYLOCK)) {
			clearFrogCommonLock(GuiLocks.ENTRYLOCK);
		}
		if (PtsCommonLocks.contains(GuiLocks.EXITLOCK)) {
			clearFrogCommonLock(GuiLocks.EXITLOCK);
		}
		paintTrackSequence(RouteDecoration.CLEAR_ROUTE);
	}

	@Override
	public void setHandlers () {
//		PtsLockHandlers.put(GuiLocks.EXITLOCK,  new FrogExitTraffic());
//		PtsLockHandlers.put(GuiLocks.DETECTIONLOCK, new FrogDetectionIndication());
//		DetectionIndication = new FrogDecorator(new DetectionLockIndicationHandler());
		/**
		 * The GuiLocks are:
		 * DETECTIONLOCK
		 * CONFLICTINGSIGNALLOCK
		 * SWITCHUNLOCK
		 * APPROACHLOCK
		 * TIMELOCK
		 * TRAFFICLOCK
		 * ENTRYLOCK
		 * EXITLOCK
		 * LOCALCONTROLLOCK
		 * CALLONLOCK
		 * WARRANTLOCK
		 * FLEET
		 * CROSSING
		 * EXTERNALLOCK
		 **/
		DetectionIndication = new FrogDecorator(				
				new BasicLockIndicationHandler(GuiLocks.DETECTIONLOCK));
		ConflictingSignalIndication = new FrogDecorator(
				new BasicLockIndicationHandler(GuiLocks.CONFLICTINGSIGNALLOCK));
		SwitchUnlockIndication = new FrogDecorator(
				new BasicLockIndicationHandler(GuiLocks.SWITCHUNLOCK));
		// APPROACHLOCK should never be set or cleared
		// TIMELOCK should never be set or cleared
		// TRAFFICLOCK should never be set or cleared
		EntryTrafficIndication = new FrogDecorator(
						new BasicLockIndicationHandler(GuiLocks.ENTRYLOCK));
		ExitTrafficIndication = new FrogDecorator(
				new BasicLockIndicationHandler(GuiLocks.EXITLOCK));
		LocalControlLock = new FrogDecorator(
				new BasicLockIndicationHandler(GuiLocks.LOCALCONTROLLOCK));
		// CALLONLOCK
		CallOnIndication = new FrogDecorator(
				new BasicLockIndicationHandler(GuiLocks.CALLONLOCK));
		// FLEETLOCK should never be set or cleared
		// CROSSINGLOCK should never be set or cleared
		PreparationIndication = new ForceLockDecorator(
				new BasicLockIndicationHandler(GuiLocks.PREPARATION));;
		ExternalIndication = new FrogDecorator(
				new BasicLockIndicationHandler(GuiLocks.EXTERNALLOCK));
	}

	/*****************************************************************************************
	 * The methods that implement SignalEdge
	 *****************************************************************************************/ 
	@Override
	public void discoverApproachSignal() {
//		SignalEdge approachSignal;
//		if (OpposingApproachEdge != null) {
//			approachSignal = OpposingApproachEdge.signalEdgeProbe();
//			if (approachSignal != null) {
//				((FrogVitalLogic) MyLogic).setAdvanceSignal(approachSignal.getSignalLogic());
//			}
//		}
	} 

//	@Override
//	public SignalVitalLogic getSignalLogic() {
//		return SignalLogic;
//	}
	
	/*****************************************************************************************
	 * end of the SignalEdge implementation
	 *****************************************************************************************/
	/*****************************************************************************************
	 * The methods that implement RouteEdge
	 *****************************************************************************************/ 
	
	/**
	 * This method is an analog to AbstractTrackEdge.getNextRoute().  Whereas the super method
	 * is used in the points->frog search direction, this method is used in the frog->points
	 * search direction.  If the points are not lined for this frog, then there is no physical
	 * "next RouteEdge".  In some cases, the caller wants to ignore the alignment and continue
	 *  as though the points were lined.  In that case, the method returns the PtsEdge
	 * 
	 * @param force if true, then ignore the current alignment and act as though the points are
	 * lined for the frog.
	 * @return the PtsEdge, as a RouteEdge
	 */
	public RouteEdge getNextRouteEdge(final boolean force) {
		if (!force && isFoulingEdge()) {  // Points lined to this Frog?
			return null;
		}
		else {
			return (RouteEdge) Joint;
		}
	}
	
	/**
	 * jumps from the FrogEdge to the PtsEdge, ignoring how the turnout is lined
	 * @return the corresponding PtsEdge
	 */
	public RouteEdge startPointsSearch() {
		if (!Shadow.isRoutable(this)  || (Shadow.isSearchFromPoints() && (Shadow.getSearchID() <= RouteSearchID) &&
				(Shadow.getSearchStatus().equals(RouteSearchStatus.SUCCESS) || Shadow.getSearchStatus().equals(RouteSearchStatus.SEARCHING)))) {
			return null;
		}
		return Shadow; 
	}

//	@Override
//	public boolean isRouteSegmentBlocked(final EnumSet<GuiLocks> blockingConditions) {
//		if (isFoulingEdge()) {
//			return true;
//		}
//		return ((AbstractTrackEdge)Shadow).isRouteSegmentBlocked(blockingConditions);
//	}
//
//	/**
//	 * a continuation of findAdvanceRouteEdge() when entering a turnout
//	 * from frog side.  Because the points CurrentTrk route index changes, all
//	 * this routine does is jump to the current frog and pick up findAdvanceRouteEdge.
//	 */
//	public void findAdvancePtsEdge() {
//		if (isFoulingEdge()) {
//			Shadow.findAdvanceRouteEdge();
//		}
//	}
//	
	@Override
	public RouteNode createNode(final RouteNode nextNode, final int searchID) {
		if ((SearchNode == null) || (SearchNode.getRouteNumber() != searchID)) {
//			SearchNode = new RouteNode(this, RouteSearchID, nextNode);
			SearchNode = RouteNode.newNode(this, searchID, nextNode);
			RouteSearchID = searchID;
		}
		return SearchNode;
	}

	@Override
	public void exitClearRoute() {
		resetLocks();
		if (Shadow != null) {
			Shadow.entryClearRoute();
		}
	}
	
	@Override
	public void entryPattern() {
		System.out.println("FrogEdge Entry: " + toString() + ":" + Sides.EDGENAME[FrogIndex]);
		super.entryPattern();
	}

	@Override
	public void exitPattern() {
		System.out.println("FrogEdge Exit: " + toString() + ":" + Sides.EDGENAME[FrogIndex]);
		if (Shadow != null) {
			Shadow.entryPattern();
		}
	}

	/*****************************************************************************************
	 * end of the RouteEdge implementation
	 *****************************************************************************************/
	/*****************************************************************************************
	 * Lock Indication Handlers for Frog Edges
	 *****************************************************************************************/
	/**************************************************************************
	 * FrogDecorator - this is a decorator on a Lock Handler to
	 * forward locks from in advance of the frog to the points.  The lock
	 * is forwarded to the same Map as the request.
	 **************************************************************************/
	protected class FrogDecorator extends AbstractHandlerDecorator {
		public FrogDecorator(LockIndicationHandler handler) {
			super(handler);
		}
		
		@Override
		public void requestLockSet() {
			Handler.requestLockSet();
			if (Joint != null) {
				((PtsEdge)Joint).setPointsEdgeLock(WatchedLock);
			}
		}

		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
			if (Joint != null) {
				((PtsEdge)Joint).clearPointsEdgeLock(WatchedLock);
			}
		}

		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
			if (Joint != null) {
				((PtsEdge)Joint).setPointsCommonLock(WatchedLock);
			}
		}

		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
			if (Joint != null) {
				((PtsEdge)Joint).clearPointsCommonLock(WatchedLock);
			}
		}		
	}
	/**************************************************************************
	 * end of FrogDecorator
	 **************************************************************************/

	/**************************************************************************
	 * ForceLockDecorator - this is a decorator on a Lock Handler to
	 * force locks from in advance of the frog to the points.  The lock
	 * is forwarded to the same Map as the request.  Points alignment is ignored.
	 **************************************************************************/
	protected class ForceLockDecorator extends AbstractHandlerDecorator {
		public ForceLockDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {
			Handler.requestLockSet();
			Shadow.setPointsEdgeLock(WatchedLock);
			if (Joint != null) {
				System.out.println("Frog joint " + FrogEdge.this.toString() + " is not null for " + WatchedLock);
			}
			else {
				System.out.println("Frog joint " + FrogEdge.this.toString() + " is null for " + WatchedLock);
			}
		}

		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
			Shadow.clearPointsEdgeLock(WatchedLock);
		}

		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
			Shadow.setPointsCommonLock(WatchedLock);
		}

		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
			Shadow.clearPointsCommonLock(WatchedLock);
		}		
	}
	/**************************************************************************
	 * end of ForceLockDecorator
	 **************************************************************************/
	/*****************************************************************************************
	 * end of Lock Indication Handlers
	 *****************************************************************************************/
}
/* @(#)FrogEdge.java */