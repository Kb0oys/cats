/* Name Block.java
 *
 * What:
 *  This class holds the information needed for a detection block.
 */
package cats.layout.items;

import cats.apps.Crandic;
import cats.automatedTests.TestMaker;
import cats.common.Constants;
import cats.common.DebugBits;
import cats.gui.jCustom.JListDialog;
import cats.gui.CtcFont;
import cats.gui.CounterFactory;
import cats.gui.GridTile;
import cats.gui.Sequence;
import cats.layout.DecoderObserver;
import cats.layout.Discipline;
import cats.layout.MasterClock;
import cats.layout.OccupancySpectrum;
import cats.layout.TimeoutObserver;
import cats.layout.store.AbstractStoreWatcher;
import cats.layout.vitalLogic.BasicVitalLogic;
import cats.layout.vitalLogic.LogicLocks;
import cats.layout.xml.*;
import cats.rr_events.TimeoutEvent;
import cats.trains.Train;
import cats.trains.TrainStore;

import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import jmri.Reportable;
import jmri.Reporter;

import org.jdom2.Element;

/**
 * contains the information needed for a track circuit (detection block):
 * <ul>
 * <li>the name of the track circuit
 * <li>the IOSpec of the detector
 * <li>the discipline used on the track circuit
 * <li>if the tracks composing the track circuit are hidden on the dispatcher panel or not
 * </ul>
 * <p>
 * The purpose of a Block is to hold the state of the track circuit and distribute it to
 * the Tracks and Edges contained within the circuit.  A Block does not have an analog on the prototype.
 * In CATS, it is a container for the Tracks and SecEdges that make up a detection block.  On
 * the prototype, there are logic circuits for the ends of the Block.  In CATS, the corresponding logic
 * is performed in the BlkEdge.
 * <p>
 * The states of a track circuit (and Track and Edge) are defined by the locks contained in trkState.
 * <ol>
 * <li>Traffic Lock means
 * <ul>
 * <li>a route has been reserved through the Block for an approaching train.
 * <li>the signal on the entry end is showing CLEAR or APPROACH
 * <li>the signal on the exit end is showing STOP
 * <li>the dispatcher cannot change the route (move any switch points)
 * <li>local controls are locked so that they cannot change the route
 * <li>turnout lock lights are on
 * <li>the reservation can be removed by the dispatcher (CTC)
 * </ul>
 * <li>Detection Lock means at least one train is present in the Block
 * <ul>
 * <li>all signals are STOP
 * <li>all tracks constituting the route on the panel are red
 * <li>the dispatcher cannot change the route (move any switch points)
 * <li>local controls are locked - maybe not
 * <li>the dispatcher can clear a reservation (CTC)
 * </ul>
 * <li>Track And Time Lock means the dispatcher has granted permission for crew to
 * perform local switching in the track circuit
 * <ul>
 * <li>the entry and exit signals are STOP AND PROCEED
 * <li>all tracks constituting the track circuit are purple
 * <li>the dispatcher cannot change the route
 * <li>local controls are unlocked
 * <li>turnout lock lights are off
 * <li>reservations are prohibited
 * </ul>
 * <li>Out Of Service Lock means that the dispatcher has taken the track out of service for
 * maintenance
 * <ul>
 * <li>all signals are STOP AND PROCEED
 * <li>all tracks constituting the track circuit are blue
 * <li>the dispatcher cannot change the route
 * <li>local controls are unlocked
 * <li>turnout lock lights are off
 * <li>reservations are prohibited
 * </ul>
 * <li>Switch Indication means that the leg of the route ends in a turnout aligned to
 * another direction. This state applies to pieces of a track circuit and not all Tracks
 * in the track circuit.
 * <ul>
 * <li>all signals feeding into the turnout are STOP
 * <li>the tracks on the partial route reflect the state of the track circuit, but are
 * not CLEAR
 * <li>depending upon the state, the dispatcher may throw turnouts
 * <li>depending upon the state, local control may be enabled
 * <li>depending upon the state, the turnout lock light may be off
 * <li>reservations are prohibited
 * </ul>
 * <li>Switch Unlocked Lock means that the local crew has unlocked a turnout.
 * <ul>
 * <li>all signals protecting the unlocked turnout are set to STOP
 * <li>the turnout is shown as unknown on the dispatcher panel
 * <li>the dispatcher cannot change the turnout
 * <li>the local crew can change the turnout
 * <li>the lock light is off
 * <li>reservations are prohibited
 * </ul>
 * <li>Opposing Signal Lock applies to only the signal protecting the exit from a route
 * <ul>
 * <li>the signal protecting the exit is STOP
 * <li>the reservation cannot be cleared from the exit end
 * <li>a new reservation cannot be set through the exit end
 * </ul>
 * <li>no locks means there is no reservation and no reasons why a reservation
 * cannot be made.
 * <ul>
 * <li>all signals are STOP when using CTC
 * <li>the signals on entry and exit are CLEAR when using ABS or APB
 * <li>all tracks constituting the route on the panel are white
 * <li>the dispatcher can change the route (move any switch points)
 * <li>local controls are unlocked when not CTC
 * <li>turnout lock lights are off when not CTC
 * <li>the dispatcher can make a reservation on a through route (CTC)
 * </ul>
 * </ol>
 * 
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * </p>
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2004, 2010, 2011, 2012, 2014, 2015, 2018, 2019,
 * 2020, 2022, 2023
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */

public class Block implements XMLEleObject {

	/**
	 * a list of all Blocks
	 */
	private static ArrayList<Block> BlockKeeper = new ArrayList<Block>();

	/**
	 * a permanent Block to handle the "unknown" Block.  It is a place holder
	 * that will be replaced by another Block definition down the track sequence.
	 */
	//  public static final Block BlockHolder = new Block(null);
	public static final Block BlockHolder = new Block(Discipline.UNDEFINED, null, null, true);

	/***********************************************************
	 * XML Tags
	 ***********************************************************/
	/**
	 * The XML tag for recognizing the Block description.
	 */
	public static final String XML_TAG = "BLOCK";

	/**
	 * The XML attribute tag for the signaling discipline.
	 */
	static final String DISCIPLINE = "DISCIPLINE";

	/**
	 * The XML attribute tag for the Block's name.
	 */
	static final String NAME = "NAME";

	/**
	 * The XML attribute tag for the Block's Station.
	 */
	static final String STATION = "STATION";

	/**
	 * The XML attribute tag for the Visible flag.
	 */
	static final String VISIBLE = "VISIBLE";

	/**
	 * The XML object tag for the occupied IOSpec.
	 */
	static final String OCCUPIED = "OCCUPIEDSPEC";

	/**
	 * The XML object tag for the unoccupied IOSpec.
	 */
	static final String UNOCCUPIED = "UNOCCUPIEDSPEC";

	/**
	 * The XML object tag for a JMRI Reporter that listens for
	 * Train movements
	 */
	static final String TRAINREPORTER = "TRAINREPORTER";


	/***********************************************************
	 * Menu constants
	 ***********************************************************/
	/**
	 * the pop up selection to occupy the Block.
	 */
	private static final String OCCUPY = "Occupy Block";

	/**
	 * the pop up selection to unoccupy the Block.
	 */
	private static final String UNOCCUPY = "Unoccupy Block";

	/**
	 * the pop up selection to add track and time.
	 */
	private static final String ADD_TNT = "Grant Track Authority";

	/**
	 * the pop up selection to remove track and time.
	 */
	private static final String REMOVE_TNT = "Remove Track Authority";

	/**
	 * the pop up selection to take a block out of service.
	 */
	private static final String ADD_OOS = "Grant Out of Service";

	/**
	 * the pop up selection to remove out of service.
	 */
	private static final String REMOVE_OOS = "Remove Out of Service";

	/**
	 * the pop up selection to position a train.
	 */
	private static final String ADD_TRAIN = "Position Train";

	/**
	 * the Labels for the Menu Items. The order of these is closely tied to the
	 * logic of blockMenu, so if they are changed, blockMenu will have to
	 * change.
	 */
	private static final String[] MenuItems = { OCCUPY, UNOCCUPY, ADD_TNT,
		REMOVE_TNT, ADD_OOS, REMOVE_OOS, ADD_TRAIN};
	
	/*********************************************************************
	 * mutually exclusive states for local control
	 *********************************************************************/
	private enum LOCAL_STATE {
		NO_LOCAL,
		TRACK_AUTHORITY,
		OUT_OF_SERVICE;
	}
	
	/*********************************************************************
	 * Object variables
	 *********************************************************************/
	/**
	 * the state of the track circuit.  It shadows the VitalLogic locks global to the
	 * TrackEdges in a Block.
	 */
	private EnumSet<GuiLocks> BlockLocksShadow = EnumSet.noneOf(GuiLocks.class);

	/**
	 * is the number of seconds to delay before considering an occupancy report
	 * to be stable.
	 */
	private static Sequence ActiveDebouncer;

	/**
	 * is the number of seconds to delay before considering an unoccupancy report
	 * to be stable.
	 */
	private static Sequence InactiveDebouncer;

	/**
	 * is the number of milliseconds to wait after sending a command before
	 * sending the next. This meters the output so as to not overrun the
	 * network.
	 */
	private static Sequence Restrictor;

	/**
	 * is the list of listeners
	 */
	private static LinkedList<AbstractStoreWatcher> Observers;

	/**
	 * the discipline over the Block
	 */
	private final Discipline MyDiscipline;

	/**
	 * the name of the Block.
	 */
	private final String BlockName;

	/**
	 * the name of the Station.
	 */
	private final String StationName;

	/**
	 * a flag, where true means the dispatcher panel should display the Block
	 * and false means it should be hidden.
	 */
	private final boolean Visible;

	/**
	 * remembers if the dispatcher has granted track authority or out of service
	 */
	private LOCAL_STATE LocalState = LOCAL_STATE.NO_LOCAL;
	
	/**
	 * the IOSpec for identifying when the block is occupied.
	 */
	private IOSpec Occupied;

	/**
	 * the IOSpec for identifying when the Block is unoccupied. It should be the
	 * opposite of Occupied, but is included for those cases when it isn't.
	 */
	private IOSpec Unoccupied;

	/**
	 * the JMRI Reporter for holding the symbol of the train occupying the block
	 */
	private TrainReport TrainReporter;

	/**
	 * is a list of all the Tracks enclosed in the Block.
	 */
	private ArrayList<Track> TrkList;

	/**
	 * is a list of all BlkEdges in the Block.
	 */
	private ArrayList<BlkEdge> EdgeList;

	/**
	 * is a list of all internal TrackEdges in the Block.  This is used
	 * for distributing Block level events (detection, OOS, and TnT) to points and crossings.
	 */
	private ArrayList<AbstractTrackEdge> InternalEdges;

	/**
	 * is the SecEdge(s) that the Block is defined in. It is needed as a starting
	 * point for assigning Tracks to the Block.
	 */
	private ArrayList<BlkEdge> Anchor;

	/**
	 * is the list of all VitalLogic that follow detection.  The preferred path is
	 * for the Occupied/Unoccupied IOSpecs to tell the VitalLogics about
	 * occupancy; however, the IOSpecs may not be defined.  So, the occupy menu option,
	 * train label move, and IOSpecs use this list.  This is BasicVitalLogic because
	 * the VitalLogic is added around the time it is instantiated, but the DetectionLock
	 * processor can change when the Block (and its discipline) is given to the
	 * BasicVitalLogic
	 */
	private ArrayList<BasicVitalLogic> DetectionListeners;
	
	/**
	 * is the number of train labels in the block.
	 */
	private int NumTrainLabels;

	/**
	 * the state of the detector
	 */
	private enum DETECTOR_STATE {
		BLOCK_OCCUPIED,		// detector reports that the block is occupied
		ACTIVE_FILTER,		// transition from unoccupied to occupied, waiting on filter timeout
		BLOCK_UNOCCUPIED,	// detector reports that the block is unoccupied
		INACTIVE_FILTER		// transition from occupied to unoccupied, waiting on filter timeout
	};
	
//	/**
//	 * is a detection debounce timer. The detector must report occupied at least
//	 * Debounce seconds before it is accepted. This is the timer that is the
//	 * gate.
//	 */
////	private Timer MyDebouncer;
//	private TimeoutEvent MyDebouncer;

	/**
	 * is the detector event filter
	 */
	private OccupancyFilter EventFilter = new OccupancyFilter();

//	/**
//	 * is the timeout value of the debounce timer. It is remembered so that if
//	 * the user changes the global value, the specific timer will be changed.
//	 */
//	private int LastTimeout;

//	/**
//	 * is true when waiting for the debounce timer to fire.  A true value means that
//	 * short occupancy pulses are being filtered out.
//	 */
//	private boolean Debouncing;

	/**
	 * is the timestamp of when the block occupied event was seen
	 */
	private long OccupyOn;

	//is the count of number of times occupancy was detected
	private long DetectionCounter;
	
//	/**
//	 * is the BlockRoute being executed.  Because there could be multiple,
//	 * non-intersecting routes being executed, it could be an array. 
//	 */
//	private RouteNode ActiveRoute;

//	/**
//	 * is the queue of BlockRoutes.
//	 */
//	private RouteNode MyRoutes;

	/*******************************************************************
	 * The following are the last recorded values of state variables - used
	 * for automated testing
	 *******************************************************************/
	/**
	 * The last recorded number of train labels
	 */
	private int RecordedNumTrainLabels;

	/**
	 * the name of the last Train recorded as being in the Block
	 */
	private String RecordedTrain;

	/**
	 * the constructor.
	 * @param discipline is the signaling discipline
	 * @param blkName is the name of the block
	 * @param station is an optional station name
	 * @param visible is true if the block is displayed
	 */
	public Block(Discipline discipline, String blkName, String station, boolean visible) {
		MyDiscipline = discipline;
		if (blkName != null) {
			BlockName = new String(blkName);
		}
		else {
			BlockName = null;
		}
		if (station != null) {
			StationName = new String(station);
		}
		else {
			StationName = null;
		}
		Visible = visible;
		TrkList = new ArrayList<Track>();
		EdgeList = new ArrayList<BlkEdge>();
		InternalEdges = new ArrayList<AbstractTrackEdge>();
		DetectionListeners = new ArrayList<BasicVitalLogic>();
		Anchor = new ArrayList<BlkEdge>(1);

		NumTrainLabels = 0;
		if (ActiveDebouncer == null) {
			ActiveDebouncer = CounterFactory.CountKeeper.findSequence(CounterFactory.OCCUPANCYDEBOUNCETAG);
		}
		if (InactiveDebouncer == null) {
			InactiveDebouncer = CounterFactory.CountKeeper.findSequence(CounterFactory.UNOCCUPANCYDEBOUNCETAG);
		}

		if (Restrictor == null) {
			Restrictor = CounterFactory.CountKeeper.findSequence(
					CounterFactory.REFRESHTAG);
		}
		if (Observers == null) {
			Observers = new LinkedList<AbstractStoreWatcher>();
		}
	}

	/**
	 * return the Block's discipline.
	 * 
	 * @return the signaling discipline over the track circuit, describing the rules for setting
	 *         signals and controlling activity
	 */
	public Discipline getDiscipline() {
		return MyDiscipline;
	}

	/**
	 * queries if the Block discipline requires a dispatcher (CTC or DTC) or not
	 * @return true if the Block uses CTC or DTC rules
	 */
	public boolean isDispatcherControlled() {
		return (MyDiscipline == Discipline.CTC) || (MyDiscipline == Discipline.DTC);
	}

	/**
	 * retrieves the name of the Block.
	 * 
	 * @return the name of the Block
	 */
	public String getBlockName() {
		if (BlockName != null) {
			return new String(BlockName);
		}
		return null;
	}

	/**
	 * return the Name of the Station the Block is at.
	 * @return the Station name.
	 */
	public String getStationName() {
		if (StationName != null) {
			return new String(StationName);
		}
		return null;
	}

	/**
	 * return the value of Visible.
	 * 
	 * @return true if the Block is to be shown; false if it is to be hidden.
	 */
	public boolean getVisible() {
		return Visible;
	}

	/**
	 * tells the caller if the Block is detected or not.
	 * 
	 * @return true if sensors have been defined for detecting block occupancy
	 *         and empty.
	 */
	public boolean isDarkTerritory() {
		return ((Occupied == null) && (Unoccupied == null));
	}

//	/**
//	 * tells the caller the decoder address of the occupancy detector.
//	 * 
//	 * @return the JMRI name of the detector reporting occupied or null
//	 * if there is none.
//	 */
//	public String getDetectorName() {
//		if (Occupied != null) {
//			return Occupied.getName();
//		}
//		return null;
//	}

//	/**
//	 * is a request to determine if a Block lock has been set
//	 * @param lock is the desired lock
//	 * @return true if it is set and false if not
//	 */
//	public boolean isLockSet(GuiLocks lock) {
//		return BlockLocksShadow.contains(lock);
//	}
	
	/**
	 * adds a Track to the list of enclosed Tracks.
	 * 
	 * @param trk
	 *            is a Track. It must register only once.
	 * 
	 * @see cats.layout.items.Track
	 */
	public void registerTrack(Track trk) {
		TrkList.add(trk);
	}

	/**
	 * tells the Block which SecEdge it encloses.
	 * 
	 * @param edge
	 *            is the SecEdge in which the Block is defined.
	 */
	public void registerEdge(BlkEdge edge) {
		Anchor.add(edge);
	}

	/**
	 * adds an internal TrackEdge to the Block
	 * @param edge is the AbstractTrackEdge included in the Block
	 */
	public void registerTrackEdge(AbstractTrackEdge edge) {
		InternalEdges.add(edge);
	}

	/**
	 * registers the DetectionLock processor for the TrackEdges
	 * that need to know about changes in occupancy
	 * @param edge is the TrackEdge containing the VitalLogic with
	 * the DetectionLock processor
	 */
	public void registerDetectionProcessor(TrackEdge edge) {
		BasicVitalLogic logic = edge.getVitalLogic();
		if (logic != null) {
			for (BasicVitalLogic v : DetectionListeners) {
				if (v == logic) {
					return;
				}
			}
			DetectionListeners.add(logic);		
		}
	}
	
	/**
	 * adds a BlkEdge to the list of BlkEdges on the boundary of the Block
	 * and its VitalLogic detection lock processor to the detection
	 * listener list.
	 * 
	 * @param newEdge
	 *            is the BlkEdge. It must be registered only once.
	 */
	public void addBlockEdge(BlkEdge newEdge) {
		EdgeList.add(newEdge);
		registerDetectionProcessor(newEdge);
	}

	/**
	 * presents the Menu of operations on the Block.
	 * 
	 * @param location
	 *            is the location of the Mouse on the screen.
	 * 
	 * @see cats.gui.jCustom.JListDialog
	 */
	public void blockMenu(Point location) {
		Vector<String> menu = new Vector<String>();
		Vector<Train> trains;
		Vector<String> names;
		Train t;
		String[] items;
		int selection;
		String title;

		if (getBlockName() != null) {
			title = new String("Block: " + getBlockName());
		} 
		else {
			title = new String("Block: unknown");
		}
		if (BlockLocksShadow.contains(GuiLocks.DETECTIONLOCK)) {
			menu.add(MenuItems[1]);
		} 
		else {
			menu.add(MenuItems[0]);
		}
		//    if (!FleetOn) {
		if (getTrkNTime()) {
			menu.add(MenuItems[3]);
		} 
		else if (getOOS()) {
			menu.add(MenuItems[5]);
		} 
		else {
			menu.add(MenuItems[2]);
			menu.add(MenuItems[4]);
		}
		//    }
		if (NumTrainLabels == 0) {
			menu.add(MenuItems[6]);
		}

		// convert to an array so the window is more compact.
		items = new String[menu.size()];
		for (int i = 0; i < menu.size(); ++i) {
			items[i] = menu.elementAt(i);
		}
		if ((selection = JListDialog.select(items, title, location)) >= 0) {
			switch (CtcFont.findString(items[selection], MenuItems)) {
			case 0: // Occupy was selected
				recordTestCase(location, "Occupy");
				setDetector();
				break;

			case 1:
				recordTestCase(location, "Unoccupy");
				clearDetector();
				break;

			case 2:
				recordTestCase(location, "Set Track Authority");
				setTNT();
				break;

			case 3:
				recordTestCase(location, "Clear Track Authority");
				clearTNT();
				break;

			case 4:
				recordTestCase(location, "Set Out of Service");
				setOOS();
				break;

			case 5:
				recordTestCase(location, "Clear Out of Service");
				clearOOS();
				break;

			case 6:
				trains = TrainStore.TrainKeeper.getCreated();
				names = new Vector<String>(trains.size());
				for (Enumeration<Train> e = trains.elements(); e
						.hasMoreElements();) {
					t = e.nextElement();
					names.add(t.getSymbol() + " " + t.getName());
				}
				if ((selection = JListDialog.select(names, "Select a Train",
						location)) >= 0) {
					t = trains.elementAt(selection);
					recordTestCase(location, "Position Train " + t.getSymbol() + " in ");
					t.positionTrain(location);
				}
				break;

			default:
			}
		}
	}
	
	/**
	 * adds or removes track authority or out of service.  The new state affects
	 * all embedded turnouts, and all block ends.  If a block end is not a signal
	 * and its peer is also not a signal, then the state change propagates to
	 * to the adjacent block.
	 * @param newState is the new local state
	 */
	private void changeLocalState(LOCAL_STATE newState) {
		// this code must be careful about recursion
//		LOCAL_STATE oldState = LocalState;
//		if (LocalState != newState) {
//			LocalState = newState;
//			for (PtsEdge p : PtsList) {
//				if (newState == LOCAL_STATE.NO_LOCAL) {
//					p.setDispatcherControl();
//				}
//				else {
//					p.removeDispatcherControl();
//				}
//			}
//			for (BlkEdge b : EdgeList) {
//				if (!(b instanceof IntermediateEdge) && 
//						(b.getNeighbor() != null) &&
//						!(b.getNeighbor() instanceof IntermediateEdge)) {
//					switch (newState) {
//					case TRACK_AUTHORITY:
//						b.getNeighbor().getBlock().setTNT();
//						break;
//					case OUT_OF_SERVICE:
//						b.getNeighbor().getBlock().setOOS();
//						break;
//					default:
//						switch (oldState) {
//						case TRACK_AUTHORITY:
//							b.getNeighbor().getBlock().clearTNT();
//							break;
//						case OUT_OF_SERVICE:
//							b.getNeighbor().getBlock().clearOOS();
//							break;
//						default:
//						}
//					}
//				}
//				else if (newState == LOCAL_STATE.NO_LOCAL) {
//					b.clearVitalLogicEvent(LogicLocks.LOCALLOCK);
//				}
//				else {
//					b.setVitalLogicEvent(LogicLocks.LOCALLOCK);
//				}
//			}
//		}
		for (AbstractTrackEdge e : InternalEdges) {
			if (newState == LOCAL_STATE.NO_LOCAL) {
//				p.setDispatcherControl();
				e.clearEdgeLock(GuiLocks.LOCALCONTROLLOCK);
			}
			else {
//				p.removeDispatcherControl();
				e.setEdgeLock(GuiLocks.LOCALCONTROLLOCK);
			}
		}
		for (BlkEdge b : EdgeList) {
			if (newState == LOCAL_STATE.NO_LOCAL) {
				b.clearVitalLogicEvent(GuiLocks.LOCALCONTROLLOCK);
			}
			else {
				b.setVitalLogicEvent(GuiLocks.LOCALCONTROLLOCK);
			}
		}
		LocalState = newState;
	}

	/**
	 * sets Track and Time for all tracks in the block
	 */
	public void setTNT() {
		changeLocalState(LOCAL_STATE.TRACK_AUTHORITY);
		for (Track t : TrkList) {
			t.alterColor(TrackColor.TRACK_N_TIME, true);
		}
}

	/**
	 * removes Track and Time for all tracks in the block
	 */
	public void clearTNT() {
		changeLocalState(LOCAL_STATE.NO_LOCAL);
		for (Track t : TrkList) {
			t.alterColor(TrackColor.TRACK_N_TIME, false);
		}
	}

	/**
	 * sets Out of Service for all tracks in the block
	 */
	public void setOOS() {
		changeLocalState(LOCAL_STATE.OUT_OF_SERVICE);
		for (Track t : TrkList) {
			t.alterColor(TrackColor.OUT_OF_SERVICE, true);
		}
	}

	/**
	 * removes Out of Service for all tracks in the block
	 */
	public void clearOOS() {
		changeLocalState(LOCAL_STATE.NO_LOCAL);
		for (Track t : TrkList) {
			t.alterColor(TrackColor.OUT_OF_SERVICE, false);
		}
	}

//	/**
//	 * broadcasts a change in a Track's state
//	 * @param tag is the tag on the lock, which is recorded
//	 */
//	private void setTrackState(String tag) {
//		broadcastUpdate(tag, Constants.ADD_MARKER + Constants.FS + Constants.QUOTE + BlockName + Constants.QUOTE);
//	}
//
//	/**
//	 * broadcasts a change in a Track's state
//	 * @param tag is the tag on the lock, which is recorded
//	 */
//	private void clrTrackState(String tag) {
//		broadcastUpdate(tag, Constants.REMOVE_MARKER + Constants.FS + Constants.QUOTE + BlockName + Constants.QUOTE);
//	}

	/**
	 * forces occupancy to be set (or cleared due to a train label
	 * moving).
	 * @param enable is true to set occupancy or false to clear it
	 */
	public void forceOccupancy(boolean enable) {
		if (enable) {
			setDetector();
		}
		else {
			clearDetector();
		}
	}
	
	/**
	 * processes the DetectionLock on event.  It has two functions:
	 * 1. paint the tracks for the route(s) thru the Block red
	 * 2. move train label(s) into the Block.
	 * This method may be invoked either by a detection sensor, the dispatcher
	 * forcing occupancy, or the dispatcher moving a train label into a Block
	 * without detectors.  The second function should probably not be done for the
	 * last; however, the GUI must tell the VitalLogic about the occupancy, which
	 * merges into the sensor control flow.  This is usually not a problem because
	 * of the selective filtering on where trains are searched for, there will not
	 * likely be any trains found.
	 */
	public void setBlockDetectionOn() {
		Train train = null;
		Train blockSafeTrain = null;
		Train occupiedTrain = null;
		BlkEdge blockSafeEdge = null;
		BlkEdge occupiedEdge = null;
		if (!BlockLocksShadow.contains(GuiLocks.DETECTIONLOCK)) {
			BlockLocksShadow.add(GuiLocks.DETECTIONLOCK);
			
			// this computes the traffic stick for all edges and
			// colors the track
			for (AbstractTrackEdge e: InternalEdges) {
//				p.setEdgeLock(GuiLocks.DETECTIONLOCK);
				e.setEdgeLock(GuiLocks.DETECTIONLOCK);
			}
			
			paintOccupancy();
			// look for a train to move.
			if (NumTrainLabels == 0) {
				for (BlkEdge b : EdgeList) {
					if ((b.isLockSet(GuiLocks.ENTRYLOCK) || b.isLockSet(GuiLocks.TRAFFICLOCK)) && ((train = b.trainSearch()) != null)) {
						train.advanceTrain(b);
						if (Crandic.Details.get(DebugBits.DEBUGFLAG)){
							log.info(Constants.LABEL_MOVE + "Train " + train.getSymbol() + " moved to " + BlockName + " due to CTC entry");
						}
						break;
					}
					else if ((train = b.trainSearch()) != null) {
						if (train.getLastBlock() == this) {
							if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
								log.info(Constants.LABEL_MOVE + "Train " + train.getSymbol() + " not moved to " + BlockName + " due to reverse move");
							}
						}
						else if (b.isBlockSafe()) {
							if (Crandic.Details.get(DebugBits.DEBUGFLAG)){
								log.info(Constants.LABEL_MOVE + "Train " + train.getSymbol() + " moved to " + BlockName + " due to clear path");
							}
							blockSafeTrain = train;
							blockSafeEdge = b;
						}
						else if (b.neighborOccupied()) {
							if (Crandic.Details.get(DebugBits.DEBUGFLAG)){
								log.info(Constants.LABEL_MOVE + "Train " + train.getSymbol() + " moved to " + BlockName + " due to occupied");
							}
							occupiedTrain = train;
							occupiedEdge = b;
						}
					}
				}
				if (blockSafeTrain != null) {
					blockSafeTrain.advanceTrain(blockSafeEdge);
				}
				else if (occupiedTrain != null) {
					occupiedTrain.advanceTrain(occupiedEdge);
				}
				else if (Crandic.Details.get(DebugBits.DEBUGFLAG) && (train == null)){
					log.info(Constants.LABEL_MOVE + "No train found for moving to Block " + BlockName);
				}

			}
			else if (Crandic.Details.get(DebugBits.DEBUGFLAG)){
				log.info(Constants.LABEL_MOVE + "No train moved to " + BlockName + " due to " + NumTrainLabels + " existing labels");
			}

//			for (BlkEdge b : EdgeList) {
//				if (b.isBlockSafe() && b.neighborOccupied() && ((train = b.trainSearch()) != null) && (NumTrainLabels == 0)) {
//					train.advanceTrain(b);
//					break;
//				}
//			}
////			for (BlkEdge b : EdgeList) {
////				if ((b.isLockSet(GuiLocks.ENTRYLOCK) || b.Tsr.isStickSet()) && b.isBlockSafe() && 
////						((train = b.trainSearch()) != null) && (NumTrainLabels == 0)) {
////					train.advanceTrain(b);
////				}
////			}
////			for (BlkEdge b : EdgeList) {
////				if (!b.isLockSet(GuiLocks.CONFLICTINGSIGNALLOCK)) {
//////					b.paintAllTracks(TrackColor.OCCUPIED, true);
//////					if (b.determineDOT() && ((train = b.trainSearch()) != null) && (NumTrainLabels == 0)) {
//////					if ((b.TrainDirection != DirectionOfTravel.EXIT) && ((train = b.trainSearch()) != null) && (NumTrainLabels == 0)) {
////					if (b.Tsr.isStickSet() && ((train = b.trainSearch()) != null) && (NumTrainLabels == 0)) {
////						train.advanceTrain(b);
////					}
////				}
////			}
//////			if (!foundOne) {
//////				for (BlkEdge b : EdgeList) {
//////					if (!b.isLockSet(LogicLocks.ConflictingSignalLock) && !b.isLockSet(LogicLocks.OpposingSignalLock)) {
//////						train = b.trainSearch();
//////						if (train != null) {
//////							train.advanceTrain(b);
//////						}
//////					}
//////				}		  
//////			}
//////			else {
//////				for (Track t : TrkList) {
//////					t.alterColor(TrackColor.OCCUPIED, true);
//////				}
//////			}
			GridTile.doUpdates();
		}
	}

	/**
	 * processes the DetectionLock off event.  For signaling, the primary
	 * function of the DetectionLock being cleared is to remove the direction
	 * of travel (if allowed - it may not be allowed if fleeting is in effect).
	 * A secondary function is to turn off the track occupancy lights on the
	 * dispatcher panel.
	 */
	public void setBlockDetectionOff() {
		if (BlockLocksShadow.contains(GuiLocks.DETECTIONLOCK)) {
			// this computes the traffic stick for all edges
			BlockLocksShadow.remove(GuiLocks.DETECTIONLOCK);
			for (BlkEdge b : EdgeList) {
				if (!b.isLockSet(GuiLocks.CONFLICTINGSIGNALLOCK)) {
					if (!b.Tsr.isStickSet() && b.isLockSet(GuiLocks.APPROACHLOCK)) {
						b.removeTrain();
					}
				}
			}
			
			// this should not be needed because all BlkEdges have Vital Logic registered with the Block
//			for (BlkEdge b : EdgeList) {
//				b.clearEdgeLock(GuiLocks.DETECTIONLOCK);
//			}
			
			for (AbstractTrackEdge e : InternalEdges) {
				e.clearEdgeLock(GuiLocks.DETECTIONLOCK);
			}
			for (Track t : TrkList) {
				t.alterColor(TrackColor.OCCUPIED, false);
			}
			GridTile.doUpdates();
		}
	}

	/**
	 * paints tracks red in response to occupancy.  It searches
	 * the BlkEnds for a clear path through the block (a clear
	 * path does not have a fouling SecEdge).  If one is found,
	 * all the tracks in the path are colored.  If one is
	 * not found, then all tracks are colored.
	 */
	public void paintOccupancy() {
		boolean foundRoute = false;
		if (BlockLocksShadow.contains(GuiLocks.DETECTIONLOCK)) {
			// start by clearing the occupancy from all tracks
			for (Track t : TrkList) {
				t.alterColor(TrackColor.OCCUPIED, false);
			}
			// try to color any tracks that have a clear path
			for (BlkEdge b: EdgeList) {
				if (b.isBlockSafe()) {
//				if (!b.EdgeLocks.contains(GuiLocks.CONFLICTINGSIGNALLOCK)) {
//					for (Enumeration<SecEdge> e = b.makeRoute(); e.hasMoreElements(); ) {
//						e.nextElement().Destination.alterColor(TrackColor.OCCUPIED, true);
//					}
					b.paintTrackBlock(RouteDecoration.OCCUPY_ROUTE);
					foundRoute = true;
				}
			}
			// if no tracks have a clear path, paint all occupied
			if (!foundRoute) {
				for (Track t : TrkList) {
					t.alterColor(TrackColor.OCCUPIED, true);
				}				
			}
		}
	}
	
	/**
	 * returns the Track and Time status.
	 * 
	 * @return true if the dispatcher has granted Track and Time.
	 */
	private boolean getTrkNTime() {
		return LocalState == LOCAL_STATE.TRACK_AUTHORITY;
	}

	/**
	 * returns the Out of Service status.
	 * 
	 * @return true if the Block is out of service.
	 */
	private boolean getOOS() {
		return LocalState == LOCAL_STATE.OUT_OF_SERVICE;
	}

//	/**
//	 * distributes a LogicLock to the VitalLogic in all of the Blocks
//	 * BlkEdges via the code line.  
//	 * @param event is the LogicLocks event to distribute
//	 * @param polarity is true for setting it and false for removing it
//	 */
//	private void distributeEvents(LogicLocks event, boolean polarity) {
//		for (BlkEdge b : EdgeList) {
//			b.constructVitalLogicEvent(event, polarity);
//		}
//	}
	
	/**
	 * steps through the list of DetectionLock processors for the
	 * Block, telling them that the block is occupied.  This bypasses
	 * the code line, but is actually a better simulation of the
	 * prototype because the stimulus to the GUI should come from
	 * the VitalLogic.
	 */
	public void setDetector() {
		if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
			log.info(Constants.OCCUPANCY_TRIGGER + "Setting detector for Block " + BlockName);
		}
		for (BasicVitalLogic vl : DetectionListeners) {
//			vl.DetectionLock.requestLockSet();
			vl.setLocalLock(LogicLocks.DETECTIONLOCK);
		}
	}

	/**
	 * steps through the list of DetectionLock processors for the
	 * Block, telling them that the block is unoccupied.  This bypasses
	 * the code line, but is actually a better simulation of the
	 * prototype because the stimulus to the GUI should come from
	 * the VitalLogic.
	 */
	public void clearDetector() {
		if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
			log.info(Constants.UNOCCUPANCY_TRIGGER + "Clearing detector for Block " + BlockName);
		}
		for (BasicVitalLogic vl : DetectionListeners) {
//			vl.DetectionLock.requestLockClear();
			vl.clearLocalLock(LogicLocks.DETECTIONLOCK);
		}
	}

	/**
	 * retrieves the number of times occupancy was detected
	 * @return the detection counter
	 */
	public long getDetectionCount() {
		return DetectionCounter;
	}
	
//	/**
//	 * is invoked to determine if a BlockRoute can be executed to set a 
//	 * route through the Block.  This method does not change anything, but
//	 * checks to see if a route is through the Block and if so, if it is this one.
//	 * This handles stacking of routes to keep them in FIFO order.
//	 * @param route contains the details of how any enclosed Points are set
//	 * @return true if it can be executed and false if not
//	 */
//	public boolean isRouteExecutable(BlockRoute route) {
//		return (ActiveRoute == null) || (ActiveRoute == route);
//	}

//	/**
//	 * is invoked to set the turnouts in a Block to the alignment
//	 * contained in BlockRoute.  If the turnouts are aligned (or
//	 * there are no turnouts), true is returned.  Otherwise, commands
//	 * are set to align the turnouts and false is returned.  A test is made that
//	 * no other BlockRoute is active.  If one is, false is returned, also.
//	 * @param route
//	 * @return true if the route is clear; otherwise, return false
//	 */
//	public boolean executeRoute(BlockRoute route) {
//		if (ActiveRoute == null) {
//			ActiveRoute = route;
//		}
//		if (ActiveRoute == route) {
//			return route.alignPoints();
//		}
//		return false;
//	}
			
//	/**
//	 * stacks a BlockRoute on the stack (queue)
//	 * @param route is the BlockRoute
//	 * @return true if there is no conflicting route ahead of the parameter
//	 */
//	public boolean stackUpRoute(BlockRoute route) {
//		return MyRoutes.queueUpRoute(route);
//	}
	
//	/**
//	 * adds the anchor to a chain of RouteNodes to the Block's stack of routes
//	 * @param node is the anchor (a node referencing a CPEdge in the DAG).  It must not be null.
//	 */
//	public void stackRoute(final RouteNode node) {
//		RouteNode link;
//		for (link = MyRoutes; link.getStackNode() != null; link = link.getStackNode()) {	
//			// empty body.  The loop looks for the last node in the chain.
//		}
//		link.setStackNode(node);		
//	}
//
//	/**
//	 * removes the route anchor from the stack
//	 * @param node is the anchor to remove.  It must not be null.  It should be in the
//	 * stack, but the code will handle it not being there.
//	 */
//	public void removeRoute(final RouteNode node) {
//		RouteNode link;
//		if (MyRoutes != null) {
//			if (MyRoutes == node) {
//				MyRoutes = node.getStackNode();
//				node.setStackNode(null);
//			}
//			else {
//				for (link = MyRoutes; (link != null) && (link.getStackNode() != node); link = link.getStackNode()) {
//				}
//				if (link != null) {
//					link.setStackNode(node.getStackNode());
//					node.setStackNode(null);
//				}
//			}
//		}
//	}
	
//	/**
//	 * if the stack of routes is not empty, the oldest is removed and returned.  If it
//	 * is empty, null is returned.
//	 * @return
//	 */
//	public RouteNode popRoute() {
//		RouteNode next = null;
//		if (MyRoutes != null) {
//			next = MyRoutes;
//			MyRoutes = next.getStackNode();
//			next.setStackNode(null);
//		}
//		return next;
//	}
//	
//	/**
//	 * is a query to determine if any routes are stacked across the block.
//	 * @return true if none are stacked (stack empty) and false if at least one
//	 * is stacked
//	 */
//	public boolean routestackEmpty() {
//		return MyRoutes == null;
//	}
//	
	/**
	 * If a TrainReporter has been defined tells the TrainReporter that a Train
	 * is in the Block.  The TrainReporter, then broadcasts the Train symbol to
	 * all listeners
	 * @param symbol is a Train symbol
	 */
	public void reportTrain(String symbol) {
		if (TrainReporter != null) {
			TrainReporter.setTrain(symbol);
		}
	}

	/**
	 * clears the Block's history, so that it can be refreshed
	 */
	private void clearHistory() {
		int delay;
		//    TrackCircuitState CurrentState = TrkState.copy();
		for (Track trk : TrkList) {
			trk.clrTrkHistory();
			// something is needed here to push all locks to the Track
		}
		//    TrkState.refreshLocks(CurrentState.getLocks());

		// Sleep a while so the serial port is not overwhelmed.
		if ((delay = Restrictor.getAdjustment()) != 0) {
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
			}
		}
	}

	/**
	 * increments the counter of trains in the block.
	 */
	public void incrementTrains() {
		NumTrainLabels = NumTrainLabels + 1;
	}

	/**
	 * decrements the counter of trains in the block.
	 */
	public void decrementTrains() {
		NumTrainLabels = NumTrainLabels - 1;
}

	/**
	 * looks at all the registered BlkEdges for one
	 * (there should be two) that has a clear path through the block.
	 * If the block is a self-contained loop, then there will be no
	 * path.
	 * 
	 * @return the first BlkEdge that is non-fouling.  If non are found,
	 * the last BlkEdge registered is returned.
	 */
	public BlkEdge findClearRoute() {
		BlkEdge b = null;
		for (Iterator<BlkEdge> edge = EdgeList.iterator(); edge.hasNext(); ) {
			b = edge.next();
			if (b.isBlockSafe()) {
				break;
			}
		}
		return b;
	}

	/**
	 * fetches the number of registered train Labels in the block
	 * @return NumTrainLabels
	 */
	public int getTrainCount() {
		return NumTrainLabels;
	}

	/**
	 * searches for the Block containing a particular Station name.
	 * 
	 * @param name is the name of a Station.  It must not be null.
	 * 
	 * @return the Block with that name, if found.  Otherwise,
	 * return null.
	 */
	public static Block findStation(String name) {
		Block blk;
		for (Iterator<Block> e = BlockKeeper.iterator(); e.hasNext();) {
			blk = e.next();
			if (name.equals(blk.StationName)) {
				return blk;
			}
		}
		return null; 
	}

	/**
	 * registers an observer for changes
	 * @param observer is the object interested in changes
	 */
	public static void registerObserver(AbstractStoreWatcher observer) {
		if (!Observers.contains(observer)) {
			Observers.add(observer);
		}
	}

	/**
	 * deregisters an observer
	 * @param observer is the object that is no longer interested
	 * in changes.
	 */
	public static void deregisterObserver(AbstractStoreWatcher observer) {
		Observers.remove(observer);
	}


//	/**
//	 * broadcasts a status update on a block to all observers
//	 * @param tag is a tag on the status
//	 * @param status is the status update
//	 */
//	private void broadcastUpdate(String tag, String status) {
//		String update = Logger.timeStamp(tag, status);
//		for (Iterator<AbstractStoreWatcher> iter = Observers.iterator(); iter.hasNext(); ) {
//			iter.next().broadcast(update);
//		}       
//	}

	/**
	 * creates a test case for a block menu pop-up
	 * @param where is where the pop-up should be positioned
	 * @param describes which of the choices was selected
	 */
	private void recordTestCase(Point where, String comment) {
		if (TestMaker.isRecording()) {
			Element testCase = new Element(XML_TAG);
			testCase.setAttribute(Section.X_TAG, String.valueOf(where.x));
			testCase.setAttribute(Section.Y_TAG, String.valueOf(where.y));
			testCase.setAttribute(NAME, BlockName);
			TestMaker.recordTrigger(testCase, (comment + " Block " + BlockName));  
		}
	}

	/**
	 * processes a test case involving the Block menu
	 * @param element is the test case (including the comment)
	 * @return null if the block can be found and an error message if not
	 */
	public static String processBlockElement(Element element) {
		int x = Integer.parseInt(element.getAttributeValue(Section.X_TAG));
		int y = Integer.parseInt(element.getAttributeValue(Section.Y_TAG));
		Block b = findBlock(element.getAttribute(NAME).getValue());
		if (b == null) {
			return new String("could not find Block " + element.getAttribute(NAME).getValue());
		}
		else {
			b.blockMenu(new Point(x, y));
		}
		return null;
	}

	/*****************************************************************************
	 * routines that involve the Block manager
	 *****************************************************************************/
	/**
	 * searches for the Block with a particular name.
	 * 
	 * @param name is the name of a Block it must not be null.
	 * 
	 * @return the Block with that name, if found.  Otherwise,
	 * return null.
	 */
	public static Block findBlock(String name) {
		if (name == null) {
			return BlockHolder;
		}
		for (Block blk : BlockKeeper) {
			if (name.equals(blk.BlockName)) {
				return blk;
			}
		}
		return null; 
	}

	/**
	 * walks through the list of Blocks to set the initial state of all the
	 * components of the Blocks.
	 */
	public static void startUp() {
		for (Block blk : BlockKeeper) {
			blk.clearHistory();
//			blk.MyRoutes = new RouteQueue(blk.EdgeList.size());
//			blk.MyRoutes = null;
		}
	}

	/**
	 * creates an Iterator over all Blocks
	 * @return the Iterator
	 */
	public static Iterator<Block> getBlockIterator() {
		return BlockKeeper.iterator();
	}

	/**
	 * walks through the list of Blocks (which are associated with SecEdges that
	 * have blocks). For each Block, it stimulates the SecEdge to tell all
	 * connected tracks to register themselves with the Block.
	 */
	public static void resolveBlocks() {
		BlockKeeper.remove(BlockHolder);
		for (Block blk : BlockKeeper) {
			for (BlkEdge edge : blk.Anchor) {
				edge.setBlock();
			}
		}
	}

	/*****************************************************************************
	 * automated test support - records a Block's state
	 *****************************************************************************/
	/**
	 * @param basic is true to record basic state and false to record details
	 * @return if any of the monitored state variables have changed since the 
	 * last snapshot, return an Element containing the changes; otherwise,
	 * return null.
	 */
	public Element getBlockState(boolean basic) {
		Element blkState = new Element(XML_TAG);
		boolean changed = false;
		Train train;
		Section sec;
		if (NumTrainLabels != RecordedNumTrainLabels) {
			blkState.setAttribute("TRAINS", String.valueOf(NumTrainLabels));
			RecordedNumTrainLabels = NumTrainLabels;
			changed = true;
		}
		if (NumTrainLabels > 0) {
			for (Track trk : TrkList) {
				if ((sec = trk.getSection()) != null) {
					if (((train = sec.getTrain()) != null) && !train.getName().equals(RecordedTrain)) {
						RecordedTrain = train.getName();
						blkState.setAttribute("TRAIN", RecordedTrain);
						changed = true;
						break;
					}
				}
			}
		}
		if (changed) {
			blkState.setAttribute("NAME", BlockName);
			return blkState;
		}
		return null;
	}

	/****************************************************************
	 * is an inner class for receiving occupied reports.
	 ****************************************************************/
	private class Occupier implements DecoderObserver {

		/**
		 * is the constructor.
		 * 
		 * @param decoder
		 *            is the IOSpec describing the message that the receiver is
		 *            looking for.
		 */
		public Occupier(IOSpec decoder) {
			decoder.registerListener(this);
		}

		/**
		 * is the interface through which the RREvent is delivered.
		 */
		public void acceptIOEvent() {
//			int global_timeout;
//			if ((global_timeout = Debouncer.getAdjustment()) == 0) {
//				if (OccupancySpectrum.instance().isRecording()) {
//					OccupyOn = Calendar.getInstance().getTimeInMillis();
//				}
//				else {
//					OccupyOn = 0;
//				}
//				setDetector();
//			}
//			else if (!Debouncing) {
//				if (OccupancySpectrum.instance().isRecording()) {
//					OccupyOn = Calendar.getInstance().getTimeInMillis();
//				}
//				else {
//					OccupyOn = 0;
//				}
//				if ((LastTimeout != global_timeout) && MyDebouncer.EventFree) {
//					LastTimeout = global_timeout;
////					MyDebouncer.setDelay(LastTimeout * 1000);
////					MyDebouncer.setInitialDelay(LastTimeout * 1000);
//					MyDebouncer.Count = LastTimeout;
//				}
////				MyDebouncer.start();
//				MasterClock.MyClock.setTimeout(MyDebouncer);
//				Debouncing = true;
//			}
			OccupancySpectrum recorder = OccupancySpectrum.instance();
			long newTimeStamp;
			long delta;
			if (recorder.isRecording()) {
				newTimeStamp = Calendar.getInstance().getTimeInMillis();
				delta = newTimeStamp - OccupyOn;
				recorder.classify(delta, false);
				OccupyOn = newTimeStamp;
				if (delta < 1000) {
					log.warn("Detector for Block " + BlockName + " has a detector inactive spike of less than one second");
				}
			}
			EventFilter.acceptActiveReport();
			++DetectionCounter;
		}
	}

	/**************************************************************
	 * is an inner class for receiving unoccupied reports.
	 **************************************************************/
	private class Unoccupier implements DecoderObserver {

		/**
		 * is the constructor.
		 * 
		 * @param decoder
		 *            is the IOSpec describing the message that the receiver is
		 *            looking for.
		 */
		public Unoccupier(IOSpec decoder) {
			decoder.registerListener(this);
		}

		/**
		 * is the interface through which the RREvent is delivered.
		 */
		public void acceptIOEvent() {
//			if (recorder.isRecording() && (OccupyOn != 0)) {
//				recorder.classify(Calendar.getInstance().getTimeInMillis() - OccupyOn);
//			}
//			if (Debouncing) {
////				MyDebouncer.stop();
//				MasterClock.MyClock.cancelTimer(MyDebouncer);
//				Debouncing = false;
//				OccupancyFilterCounter.instance().bumpCounter();
//			} else {
//				clearDetector();
//			}
			OccupancySpectrum recorder = OccupancySpectrum.instance();
			long newTimeStamp;
			long delta;
			if (recorder.isRecording()) {
				newTimeStamp = Calendar.getInstance().getTimeInMillis();
				delta = newTimeStamp - OccupyOn;
				recorder.classify(delta, true);
				OccupyOn = newTimeStamp;
				if (delta < 1000) {
					log.warn("Detector for Block " + BlockName + " has a detector active spike of less than one second");
				}
			}
			EventFilter.acceptInActiveReport();
		}
	}

	/************************************************************************
	 *  is an inner class for receiving train symbol reports
	 ***********************************************************************/
	private class TrainReport implements PropertyChangeListener {

		/**
		 * is the last train added to the Block.
		 */
		private String TrainSymbol = null;

		/**
		 * is the JMRI Reporter holding the train symbol in the block.  Since CATS does not write
		 * to the Reporter anymore, this may not be needed.
		 */
		private Reporter Transponder;

		/**
		 * is the constructor
		 */
		public TrainReport(Reporter rep) {
			Transponder = rep;
			if (Transponder != null) {
				Transponder.addPropertyChangeListener(this);
			}
		}

		/**
		 * remember the symbol of the last train added to the Block.  This is needed
		 * to avoid a recursion issue.
		 * @param symbol
		 */
		public void setTrain(String train) {
			String symbol = null;
			if (train != null) {
				symbol = new String(train);
				symbol.trim();
				if (symbol.length() == 0) {
					symbol = null;
				}
			}
			if (symbol == null) {
				symbol = "";
			}
			TrainSymbol = symbol;
//			Transponder.setReport(symbol);
		}

		public void propertyChange(PropertyChangeEvent arg0) {
			Object TranspondingMsg = arg0.getNewValue();
			String transId;
			String tSymbol = null;
//			String symbol = ((NamedBean) arg0.getNewValue()).getDisplayName();
			Train t = null;
			boolean positioned = false;
//			symbol = arg0.getNewValue().toString();
			if (TranspondingMsg == null) {
				// do nothing.  Maybe remove Train?
				return;
			}
			else if (TranspondingMsg instanceof Reportable) {	//This is taken from JMRI ReporterTableDataModel.java for the Reporter table
				transId = ((Reportable) TranspondingMsg).toReportString();
			}
			else {
				transId = TranspondingMsg.toString();
			}
			if (transId != null) {
				transId.trim();
				if (transId.length() == 0) {
					transId = null;
				}
			}
			if (transId == null) {
				if (Crandic.Details.get(DebugBits.TRANSPONDINGBIT)){
					log.info("Transponding Reporter for " + Block.this.BlockName + " received message empty message");
				}
			}
			else {
				t = TrainStore.TrainKeeper.getTrainByTransponder(transId);
				if (t != null) {
					tSymbol = t.getSymbol();
				}
				if (Crandic.Details.get(DebugBits.TRANSPONDINGBIT)){
					log.info("Transponding Reporter for " + Block.this.BlockName + " received message <" + transId + ">");
				}
			}
			if ((TrainSymbol == null) || !TrainSymbol.equals(tSymbol)) {
				CatsReporter.recordTestCase(Transponder.getUserName(), transId);
				if (tSymbol == null) {
					if (TrainSymbol != null) {
						// this should clear the Train from the edge.
					}
				}
				else if (!tSymbol.equals(TrainSymbol)){
//					t = TrainStore.TrainKeeper.getTrain(symbol);
					if (t != null) {
						// this should position the Train on an edge in the block
						for (BlkEdge b : EdgeList) {
							if (b.isEntranceEdge()) {
								if (t.getIcon() == null) {
									t.positionTrain(b.getSection());
								}
								else {
									t.moveTrain(b.getSection());
								}
								positioned = true;
								break;
							}
						}
						if (!positioned) {
							// there was no entry edge, so find the first unblocked edge
							for (BlkEdge b : EdgeList) {
								if (!b.isTrafficBlocked()) {
									if (t.getIcon() == null) {
										t.positionTrain(b.getSection());
									}
									else {
										t.moveTrain(b.getSection());
									}
									break;
								}
							}
						}
					}
				}
			}
		}
	}
	
//	/************************************************************************
//	 * an inner class for receiving debounce timeouts
//	 ************************************************************************/
//	private class TickReceiver implements TimeoutObserver {
//
//		@Override
//		public void acceptTimeout() {
////			setBlockDetectionOn();
////			for (BlkEdge b : EdgeList) {
////				b.setEdgeLock(GuiLocks.DETECTIONLOCK);
////			}
//			setDetector();
////			Debouncing = false;
////			GridTile.doUpdates();
//		}
//	}
	
	/************************************************************************
	 * an inner class that is a preprocessor on block detection events.  It
	 * tracks the state of the detector. if the hardware indicates a change in
	 * state, it can invoke a filter that holds the change for a short period of
	 * time while watching for a return to the previous state; thus, it filters
	 * out spikes or glitches, either in occupancy or unoccupancy.
	 ************************************************************************/
	private class OccupancyFilter implements TimeoutObserver {
		/**
		 * the state of the detector
		 */
		public DETECTOR_STATE DetectorState = DETECTOR_STATE.BLOCK_UNOCCUPIED;
		
		/**
		 * the timer for filtering out spikes and glitches
		 */
		private TimeoutEvent FilterTimer = new TimeoutEvent(this, 0);
		
		/**
		 * the detector cleared trigger to the filter state machine
		 */
		public void acceptActiveReport() {
			int global_timeout = ActiveDebouncer.getAdjustment();
			switch (DetectorState) {
			case BLOCK_UNOCCUPIED:	// stable state of detector reporting block vacant
				if (global_timeout == 0) {
					setDetector();
					DetectorState = DETECTOR_STATE.BLOCK_OCCUPIED;
				}
				else {
					if (FilterTimer.EventFree) {
						FilterTimer.Count = global_timeout;
					}
					MasterClock.MyClock.setTimeout(FilterTimer);
					DetectorState = DETECTOR_STATE.ACTIVE_FILTER;
				}
				break;
			case INACTIVE_FILTER:	// transition from occupied to unoccupied.  This event cancels the transition
				MasterClock.MyClock.cancelTimer(FilterTimer);
				DetectorState = DETECTOR_STATE.BLOCK_OCCUPIED;
				break;
			default:
			}
		}
		
		/**
		 * the detector fired trigger to the filter state machine
		 */
		public void acceptInActiveReport() {
			int global_timeout = InactiveDebouncer.getAdjustment();
			switch (DetectorState) {
			case BLOCK_OCCUPIED:	// stable state of detector reporting block occupied
				if (global_timeout == 0) {
					clearDetector();
					DetectorState = DETECTOR_STATE.BLOCK_UNOCCUPIED;
				}
				else {
					if (FilterTimer.EventFree) {
						FilterTimer.Count = global_timeout;
					}
					MasterClock.MyClock.setTimeout(FilterTimer);
					DetectorState = DETECTOR_STATE.INACTIVE_FILTER;
				}
				break;
			case ACTIVE_FILTER:	// transition from unoccupied to occupied.  This event cancels the transition
				MasterClock.MyClock.cancelTimer(FilterTimer);
				DetectorState = DETECTOR_STATE.BLOCK_UNOCCUPIED;
				break;
			default:
			}
		}
		
		@Override
		public void acceptTimeout() {
			switch (DetectorState) {
			case ACTIVE_FILTER:
				setDetector();
				DetectorState = DETECTOR_STATE.BLOCK_OCCUPIED;
				break;
			case INACTIVE_FILTER:
				clearDetector();
				DetectorState = DETECTOR_STATE.BLOCK_UNOCCUPIED;
				break;
			default:
			}
		}
	}
	/************************************************************************
	 * routines for constructing a Block from XML
	 ************************************************************************/

	/************************************************************************
	 * routines for constructing a Block from XML
	 ************************************************************************/
	/*
	 * is the method through which the object receives the text field.
	 * 
	 * @param eleValue is the Text for the Element's value.
	 * 
	 * @return if the value is acceptable, then null; otherwise, an error
	 * string.
	 */
	public String setValue(String eleValue) {
		return null;
	}

	/*
	 * is the method through which the object receives embedded Objects.
	 * 
	 * @param objName is the name of the embedded object @param objValue is the
	 * value of the embedded object
	 * 
	 * @return null if the Object is acceptible or an error String if it is not.
	 */
	public String setObject(String objName, Object objValue) {
		String resultMsg = null;
		ReporterSpec rep;
		Reporter reporter;
		String blkName;
		if (OCCUPIED.equals(objName)) {
			if (Occupied == null) {
				Occupied = ((Detector) objValue).getSpec();
//				Debouncing = false;
//				LastTimeout = Debouncer.getAdjustment();
//				MyDebouncer = new Timer(LastTimeout, new ActionListener() {
//					public void actionPerformed(ActionEvent ae) {
//						MyDebouncer.stop();
//						setBlockDetectionOn();
//						Debouncing = false;
//						GridTile.doUpdates();
//					}
//				});
//				MyDebouncer.setRepeats(false);
//				MyDebouncer = new TimeoutEvent(new TickReceiver(), LastTimeout);
				new Occupier(Occupied);
			}
		} else if (UNOCCUPIED.equals(objName)) {
			if (Unoccupied == null) {
				Unoccupied = ((Detector) objValue).getSpec();
				new Unoccupier(Unoccupied);
			}
		} else if (TRAINREPORTER.equals(objName)) {
			rep = ((CatsReporter) objValue).getSpec();
			if (rep != null) {
				if ((reporter = rep.getReporter()) == null) {
					if (BlockName != null) {
						blkName = "Block " + BlockName + " ";
					}
					else {
						blkName = "";
					}
					log.warn(blkName + "Transponding Reporter " + rep.toString() + " Could not be found");
				}
				else {
					TrainReporter = new TrainReport(reporter);
				}
			}
		}
		else {
			resultMsg = new String(XML_TAG + " XML elements cannot have "
					+ objName + " embedded objects.");
		}
		return resultMsg;
	}

	/*
	 * returns the XML Element tag for the XMLEleObject.
	 * 
	 * @return the name by which XMLReader knows the XMLEleObject (the Element
	 * tag).
	 */
	public String getTag() {
		return new String(XML_TAG);
	}

	/*
	 * tells the XMLEleObject that no more setValue or setObject calls will be
	 * made; thus, it can do any error checking that it needs. It should
	 * probably check that if an occupy IOSpec is listed that an unoccupy IOSpec
	 * is also listed.
	 * 
	 * @return null, if it has received everything it needs or an error string
	 * if something isn't correct.
	 */
	public String doneXML() {
		if ((Occupied == null) && (Unoccupied != null)) {
			new Occupier(Unoccupied.createComplement());
		}
		else if ((Occupied != null) && (Unoccupied == null)) {
			new Unoccupier(Occupied.createComplement());
		}
		return null;
	}

	/**
	 * registers a BlockFactory with the XMLReader.
	 */
	static public void init() {
		XMLReader.registerFactory(XML_TAG, new BlockFactory());
		Detector.init(OCCUPIED);
		Detector.init(UNOCCUPIED);
		CatsReporter.init(TRAINREPORTER);
		BlockKeeper.add(BlockHolder);
	}

	/**
	 * is a Class known only to the Block class for creating Blocks from an XML
	 * document.
	 */
	static class BlockFactory implements XMLEleFactory {

		/**
		 * the Block's name in an attribute.
		 */
		private String AttrName;

		/**
		 * the Block's Station name is an attribute.
		 */
		private String AttrStation;

		/**
		 * the Block's discipline in an attribute.
		 */
		//	private int AttrDiscipline;
		private Discipline AttrDiscipline = Discipline.UNDEFINED;

		/**
		 * the Block's visibility in an attribute.
		 */
		private boolean AttrVisible;

		/*
		 * tells the factory that an XMLEleObject is to be created. Thus, its
		 * contents can be set from the information in an XML Element description.
		 */
		public void newElement() {
			AttrName = null;
			AttrStation = null;
			AttrDiscipline = null;
			AttrVisible = true;
		}

		/*
		 * gives the factory an initialization value for the created XMLEleObject.
		 * 
		 * @param tag is the name of the attribute. @param value is it value.
		 * 
		 * @return null if the tag:value are accepted; otherwise, an error string.
		 */
		public String addAttribute(String tag, String value) {
			String resultMsg = null;
			if (Block.NAME.equals(tag)) {
				AttrName = value;
			}
			else if (Block.STATION.equals(tag)) {
				AttrStation = value;
			} else if (Block.DISCIPLINE.equals(tag)) {
				AttrDiscipline = Discipline.toDiscipline(value);
				if (AttrDiscipline == null) {
					resultMsg = new String(value + " is an invalid value for a "
							+ Block.XML_TAG + " XML element " + Block.DISCIPLINE
							+ " attribute.");
				}
			} else if (Block.VISIBLE.equals(tag)) {
				if (Constants.TRUE.equals(value)) {
					AttrVisible = true;
				} else if (Constants.FALSE.equals(value)) {
					AttrVisible = false;
				} else {
					resultMsg = new String(value + " is an invalid value for a "
							+ Block.XML_TAG + " XML element " + Block.VISIBLE
							+ " attribute.");
				}
			} else {
				
				resultMsg = new String(Block.XML_TAG + " XML elements do not have "
						+ tag + " attributes.");
			}
			return resultMsg;
		}

		/*
		 * tells the factory that the attributes have been seen; therefore, return
		 * the XMLEleObject created.
		 * 
		 * @return the newly created XMLEleObject or null (if there was a problem in
		 * creating it).
		 */
		public XMLEleObject getObject() {
			Block newBlock = findBlock(AttrName);
			if (newBlock == null) {
				newBlock = new Block(AttrDiscipline, AttrName, AttrStation, AttrVisible);
				BlockKeeper.add(newBlock);
			}
			// should check that all the Block attributes match up.
			return newBlock;
		}
	}
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Block.class.getName());
}
/* @(#)Block.java */