/* Name: ButtonItem.java
 *
 * What:
 * ButtonItem defines a class for placing a button on a CATS panel.  It
 * has the following components:
 * <ol>
 * <li>an IOSpec triggered by "pushing" the button or being notified of it being pushed</li>
 * <li>the image to display when the button is in the primary state</li>
 * <li>the image to display when the button is in the alternate state</li>
 * <li>a timer for implementing a momentary push button</li>
 * </ol>
 */
package cats.layout.items;

import java.awt.event.MouseEvent;

import org.jdom2.Element;

import cats.gui.GridTile;
import cats.gui.frills.ButtonFrill;
import cats.layout.BiDecoderObserver;
import cats.layout.xml.XMLEleFactory;
import cats.layout.xml.XMLEleObject;
import cats.layout.xml.XMLReader;
import cats.rr_events.VerifyEvent;

/**
 * ButtonItem defines a class for placing a button on a CATS panel.  It
 * has the following components:
 * <ol>
 * <li>an IOSpec triggered by "pushing" the button or being notified of it being pushed</li>
 * <li>the image to display when the button is in the primary state</li>
 * <li>the image to display when the button is in the alternate state</li>
 * <li>a timer for implementing a momentary push button</li>
 * </ol>
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class ButtonItem implements Itemizable, BiDecoderObserver {

	/**
	 * The XML tag for identifying a ButtonItem definition.
	 */
	static final String XML_TAG = "BUTTON";

	/**
	 * the XML tag for the trigger
	 */
	static final String XML_TRIGGER = "TRIGGER";

	/**
	 * the XML tag for the location of the primary image
	 */
	static final String XML_PRIMARY = "PRIMARY";

	/**
	 * the XML tag for the location of the alternate image
	 */
	static final String XML_ALTERNATE = "ALTERNATE";

	/**
	 * the XML tag for the delay
	 */
	static final String XML_DELAY = "DELAY";
	
	/**
	 * the XML tag for the display only (do not accept keystrokes) flag
	 */
	static final String XML_STATUS = "STATUS";
	
	/**
	 * the XML tag for scaling mages to fit in a Grid tile
	 */
	static final String XML_FIT = "FIT_TO_GRID";
	
	/**
	 * the maximum delay in seconds
	 */
	public static final int MAXIMUM_DELAY = 5;

	/**
	 * The ImageFrill for painting the current Image.
	 */
	private ButtonFrill MyFrill;

//	/**
//	 * The Section containing the ImageItem.
//	 */
//	private Section ButtonSection;

	/**
	 * The GridTile where the Image is painted.
	 */
	private GridTile ButtonTile;

	/**
	 * the IOSpec triggered/received
	 */
	private IOSpec Trigger;

	/**
	 * the image displayed when the primary IOSpec is selected
	 */
	private final String PrimaryImage;

	/**
	 * the image displayed when the alternate IOSpec is selected
	 */
	private final String AlternateImage;

	/**
	 * the number of seconds to wait after selecting the alternate before flipping
	 * back to the primary.  A value of 0 creates a latching button.  A non-zero
	 * value creates a momentary button (where the delay is the value in seconds)
	 */
	private final int MomentaryDelay;
	
	/**
	 * a flag indicating if the button can accept keystrokes or not.  When true, the
	 * button will not accept keystrokes.  It will only change images in response
	 * to events received from the layout; thus, is used for showing status.
	 */
	private final boolean StatusOnly;

	/**
	 * a flag indicating if the image should be scaled to the size of a Grid Tile.  True
	 * scales it and false leaves it at its native size.
	 */
	private final boolean FitToGrid;
	
	/**
	 * the state of the button.  True means the Primary Image is active; thus, when a
	 * mouse button release is received, a command is sent to display the alternate
	 * image.  Conversely, when the alternate image is active and a mouse button
	 * release is received, a command is sent ot display the primary image.
	 */
	private boolean PrimaryActive;
	
	/**
	 * the Timer used to implement a momentary button.  It provides the delay between
	 * button push and button reset.
	 */
	private javax.swing.Timer ResetTimer;
	
	/**
	 * the ctor
	 * @param primary is the file name of the primary image
	 * @param alternate is the file name of the alternate image
	 * @param delay is the value of the delay for momentary buttons
	 * @param status is true if the button shows only the status of a decoder
	 * @param fit is true to fit the images into a Grid Tile
	 */
	ButtonItem(final String primary, final String alternate, final int delay,
			final boolean status, boolean fit){
		PrimaryImage = primary;
		AlternateImage = alternate;
		MomentaryDelay = delay;
		StatusOnly = status;
		FitToGrid = fit;
		PrimaryActive = true;
	}
	
	/**
	 * is the mouse button event receiver.  It accepts only releases of
	 * mouse button1.  When it receives one, it flips the state of the button.
	 * @param event is the mouse button event
	 */
	public void flipState(final MouseEvent event) {
//		System.out.println("Button state = " + (PrimaryActive ? "Primary " : " Alternate ") + Trigger.dumpContents());
		if (!StatusOnly && ((event.getModifiers() & MouseEvent.BUTTON1_MASK) != 0) && (Trigger != null)) {
			if (PrimaryActive) {
				new VerifyEvent(Trigger, false).queUp();
				if (MomentaryDelay != 0) {
					if (!ResetTimer.isRunning()) {
						ResetTimer.start();
					}
				}
			}
			else {
				new VerifyEvent(Trigger, true).queUp();
			}		
		}
	}

	@Override
	public Element dumpState(boolean basic) {
		Element state = new Element(XML_TAG);
		state.setAttribute("Button State", (PrimaryActive ? "Primary" : "Alternate"));
		return null;
	}

	@Override
	public String setValue(String eleValue) {
		return null;
	}

	@Override
	public String setObject(String objName, Object objValue) {
		String resultMsg = null;
		if (XML_TRIGGER.equals(objName)) {
			Trigger = ((Detector) objValue).getSpec();
		}
		else {
			resultMsg = new String(XML_TAG + " XML elements cannot have " +
					objName + " embedded objects.");

		}
		return resultMsg;
	}

	@Override
	public String getTag() {
		return XML_TAG;
	}

	@Override
	public String doneXML() {
		return null;
	}

	@Override
	public void addSelf(Section sec) {
		sec.replaceButton(this);
//		ButtonSection = sec;
	}

	@Override
	public void install(GridTile tile) {
		if ( (MyFrill == null) && (PrimaryImage != null) && (AlternateImage != null)) {
			MyFrill = new ButtonFrill(this, PrimaryImage, AlternateImage, FitToGrid);
		}

		ButtonTile = tile;
		if (MyFrill != null) {
			ButtonTile.addFrill(MyFrill);
		}
		
		if (Trigger != null) {
			Trigger.registerListener(this);
			Trigger.registerOtherListener(this);
			Trigger.setExitFlag(true);
		}
		else {
			System.out.println("A CTC button is missing its decoder definition");
			log.warn("A CTC button is missing its decoder definition");
		}
		if ((Trigger != null) && (MomentaryDelay != 0)) {
			ResetTimer = new javax.swing.Timer(MomentaryDelay * 1000, new java.awt.event.ActionListener() {
		        public void actionPerformed(java.awt.event.ActionEvent e) {
		        	new VerifyEvent(Trigger, true).queUp();
		        	ResetTimer.stop();
		          }
		        });
//		      ResetTimer.setInitialDelay(0);
		      ResetTimer.setRepeats(false);
		}
//		acceptIOEvent();
	}

	@Override
	public boolean isVisible() {
		return true;
	}
	
	/**********************************************************
	 * the following two methods handle events from the layout
	 **********************************************************/
	@Override
	public void acceptIOEvent() {
		if (MyFrill != null) {
			MyFrill.setImage(true);
			ButtonTile.requestUpdate();
			GridTile.doUpdates();
		}
		System.out.println("Setting Primary active");
		PrimaryActive = true;
	}

	@Override
	public void acceptOtherIOEvent() {
		if (MyFrill != null) {
			MyFrill.setImage(false);
			ButtonTile.requestUpdate();
			GridTile.doUpdates();
		}
		System.out.println("Setting Alternate active");
		PrimaryActive = false;
	}

	/**
	 * registers a BlockFactory with the XMLReader.
	 */
	static public void init() {
		XMLReader.registerFactory(XML_TAG, new ButtonFactory());
		Detector.init(XML_TRIGGER);
	}
	
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
			ButtonItem.class.getName());
}

/**
 * is a Class known only to the ButtonItem class for creating Buttons from
 * an XML document.  Its purpose is to pick up the Image file name.
 */
class ButtonFactory
implements XMLEleFactory {

	/**
	 * these are the XML attributes
	 */
	private String PrimaryAttr;
	private String AlternateAttr;
	private int DelayAttr;
	private boolean StatusAttr;
	private boolean FitAttr;

	@Override
	public void newElement() {
		PrimaryAttr = null;
		AlternateAttr = null;
		DelayAttr = 0;
		StatusAttr = false;
		FitAttr = false;
	}

	@Override
	public String addAttribute(String tag, String value) {
		String resultMsg = null;
		if (ButtonItem.XML_PRIMARY.equals(tag)) {
			PrimaryAttr = value;
		}
		else if (ButtonItem.XML_ALTERNATE.equals(tag)) {
			AlternateAttr = value;
		}
		else if (ButtonItem.XML_DELAY.equals(tag)) {
			try {
				DelayAttr = Integer.parseInt(value);
			}
			catch (NumberFormatException e) {
				resultMsg = new String(value + " is an invalid delay for a " +
						ButtonItem.XML_TAG + " XML Element " + ButtonItem.XML_DELAY +
						" attribute");
			}
		}
		else if (ButtonItem.XML_STATUS.equals(tag)) {
			StatusAttr = Boolean.parseBoolean(value);
		}
		else if (ButtonItem.XML_FIT.equals(tag)) {
			FitAttr = Boolean.parseBoolean(value);
		}
		return resultMsg;
	}

	@Override
	public XMLEleObject getObject() {
		return new ButtonItem(PrimaryAttr, AlternateAttr, DelayAttr, StatusAttr, FitAttr);
	}

}

/* @(#)ButtonItem.java */
