/* Name: RouteInfo,java
 *
 * What;
 *  RouteInfo is a passive data structure for holding the information needed
 *  for aligning turnout points to a particular route.
 */
package cats.layout.items;

import java.util.ArrayList;

import cats.apps.Crandic;
import cats.common.Constants;
import cats.common.DebugBits;
import cats.common.Sides;
import cats.layout.DecoderObserver;
import cats.layout.vitalLogic.PtsVitalLogic;
import cats.layout.xml.*;
import cats.rr_events.CountermandEvent;
import cats.rr_events.FeedbackEvent;

/**
 * is a passive data structure for holding the information needed for
 * aligning turnouts to a particular route.   The data structure is an array
 * of Vector of IOSpec, with one entry for each of:
 * <ul>
 * <li>
 *   the request from a manual switch on the fascia for selecting the route.
 * <li>
 *   the command, sent to the stationary decoder, to select the route.
 * <li>
 *   the report from a sensor at the turnout, indicating that the turnout
 *   has been aligned for the route.
 * <li>
 *   the report from a sensor at the turnout, indicating that the turnout
 *   has not been aligned for the route.
 * </ul>
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2009, 2011, 2012, 2013, 2014, 2015</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class RouteInfo
    implements XMLEleObject {
	
	  static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(RouteInfo.class.getName());

  /**
   * is the tag for identifying a SwitchPoints Object in the XML file.
   */
  static final String XML_TAG = "ROUTEINFO";

  /**
   * is the attribute tag for identifying the route, by its other end.
   */
  static final String ROUTE_ID = "ROUTEID";

  /**
   * is the tag for identifying the Normal attribute.
   */
  static final String NORMAL_TAG = "NORMAL";

  /**
   * is the String for confirming that the route is the Normal route.
   */
  static final String ISNORMAL = "true";

  public enum FEEDBACK {NO_FEEDBACK, POSITIVE_FEEDBACK, EXACT_FEEDBACK};
  
  /**
   * is the feedback defined for this alignment
   */
  private FEEDBACK MyFeedback;
  
  /**
   * is a table of labels and XML tags for the lists of decoder information.
   */
  public static final String[][] RouteName = {
      {
      "Select Route Request", "ROUTEREQUEST"}
      , {
      "Route Selected Report", "SELECTEDREPORT"}
      , {
      "Route Unselected Report", "NOTSELECTEDREPORT"}
  };

  /**
   * is the association between a label and XML tag for the list
   * of commands for moving the switch points.
   */
  public static final String[] RouteCommand = {
      "Select Route Command", "ROUTECOMMAND"
  };

  /**
   * is an index into the above for selecting a label.
   */
  public static final int LAB_INDEX = 0;

  /**
   * is an index into the above for selecting an XML tag.
   */
  public static final int TAG_INDEX = 1;

  /**
   * the information about the route.
   */
  private Detector[] RouteSpecs = new Detector[RouteName.length];

  /**
   * is the decoder command for setting the route.
   */
  private IOSpec RouteCmd;

  /**
   * are indexes into the above.
   */
  public static final int ROUTEREQUEST = 0;
  /**
   * indexes the IOSpec for when the route is selected
   */
  public static final int SELECTEDREPORT = 1;
  /**
   * indexes the IOSPec for when the route is de-selected.
   */
  public static final int NOTSELECTEDREPORT = 2;

  /**
   * the SecEdge where the other end of the route terminates.
   */
  private int Destination;

  /**
   * is the PtsVitalLogic containing the RouteInfo
   */
  private PtsVitalLogic VitalLogic;
  
  /**
   * a true flag meaning that this route is the normal or through route.
   * Only one route in the SwitchPoints can be the normal route.
   */
  private boolean NormalRoute = false;

  /**
   * the constructor.
   *
   * @param dest is the name of the edge that the other end terminates on.
   */
  public RouteInfo(int dest) {
    Destination = dest;
  }

  /**
   * returns the Route identity.
   *
   * @return the Route identity.  It will be a valid integer between
   * 0 and Edge.EDGENAME.length.
   */
  public int getRouteId() {
    return Destination;
  }

  /**
   * tells the RouteInfo what VitalLogic it is a component of
   * @param logic is the enclosing VitalLogic
   */
  public void setVitalLogic(PtsVitalLogic logic) {
	  VitalLogic = logic;
  }
  
  /**
   * returns one of the decoder report lists.
   *
   * @param id is the index of the report requested.  If it refers to
   * a non-existent list, nothing happens.
   *
   * @return the list.  It can be null.
   */
  public IOSpec getRouteReport(int id) {
    if ( (id >= 0) && (id < RouteName.length) && (RouteSpecs[id] != null)) {
      return RouteSpecs[id].getSpec();
    }
    return null;
  }

  /**
   * returns the command for moving the points.
   *
   * @return the command.
   */
  public IOSpec getRouteCmd() {
    return RouteCmd;
  }

  /**
   * changes the NormalRoute flag.
   *
   * @param normal is true if the route is the normal route or false if
   * it is a diverging route through the turnout.
   */
  public void setNormal(boolean normal) {
    NormalRoute = normal;
  }

  /**
   * retrieves the NormalRoute flag.
   *
   * @return true if the route is the normal route through the turnout or
   * false if it is a diverging route.
   */
  public boolean getNormal() {
    return NormalRoute;
  }

  /**
   * is invoked to prod the RouteInfo to tell its VitalLogic
   * that a command has been sent to move the points to this route;
   * therefore the feedback should not be overridden
   */
  public void reserveRoute() {
	  VitalLogic.setNextRoute(Destination);
  }
  
  /**
   * is called by TurnoutHelper to ask the Route if the containing PtsVitalLogic
   * permits the points to be moved by the dispatcher to this alignment.
   * @return true if the points can be moved indirectly as a result of moving
   * other points and false if not
   */
  public boolean checkDispatcherSafety() {
	  return VitalLogic.checkDispatcherLocks();
  }

  /**
   * is called by TurnoutHelper to ask the Route if the containing PtsVitalLogic
   * permits the points to be moved under local control to this alignment.
   * @return true if the points can be moved indirectly as a result of moving
   * other points and false if not
   */
  public boolean checkLocalSafety() {
	  return VitalLogic.checkLocalLocks();
  }

  /**
   * Attempts to issue the command from the dispatcher to move the points on a turnout.  Before sending
   * the command, it runs a safety check on all turnouts that use the same command (because
   * they will move if the command is sent).  If any turnout should not be moved, the command
   * is not sent.  If all can be moved, the command is sent.
   * @return true if the command is sent and false if the command is not found or a turnout should not be moved.
   */
  private boolean runDispatcherCommand() {
    ArrayList<PtsEdge.RouteReference> routes;
    routes = (RouteCmd == null) ? null : RouteCmd.getRegisteredUsers();
    if (routes == null) {
    	return false;
    }
    for (PtsEdge.RouteReference info : routes) {
    	if (!info.Edge.isDispatcherControlAllowed()) {
    		return false;
    	}
    }
    // tell all turnouts affected by the command to expect a feedback message
    for (PtsEdge.RouteReference info : routes) {
    	info.Edge.prepareForMovement(info.TrackNumber);;
    }
    RouteCmd.sendCommand();        
    return true;
  }
  
  /**
   * steps through all turnouts that use the command to verify that they are in
   * a state where the points movement command can be sent.  If they are, the method
   * prepares the VitalLogic for the feedback so that it does not attempt
   * to reverse the operation.
   * @return true if all turnouts can be changed and false if any one cannot
   */
  private boolean runLocalCommand() {
	  ArrayList<PtsEdge.RouteReference> routes;
	  routes = (RouteCmd == null) ? null : RouteCmd.getRegisteredUsers();
	  if (routes == null) {
		  return false;
	  }
	  for (PtsEdge.RouteReference info : routes) {
		  if (!info.Edge.isLocalControlAllowed()) {
			  return false;
		  }
	  }
	  // tell all turnouts affected by the command to expect a feedback message
	  for (PtsEdge.RouteReference info : routes) {
		  info.Edge.prepareForMovement(info.TrackNumber);;
	  }
	  RouteCmd.sendCommand();        
	  return true;
  }
  

 /**
  * is a request to line the points to a route.  To service this request,
  * RouteInfo checks to see what is involved.  If the requested route
  * has a command defined, then the request is forwarded to the TurnoutHelper
  * (to verify that turnouts sharing the command - such as crossovers -
  * can also be lined).  If the requested route does not have a command defined,
  * then the requested can be honored immediately and the feedback (if present)
  * is generated.
  * 
  * @return true if the request can be honored and false if not.
  */
  public boolean requestAlignment() {
	  FeedbackEvent event; 
	  if (RouteCmd == null) {
		  if (VitalLogic.checkDispatcherLocks()) {
			  if (MyFeedback.equals(FEEDBACK.NO_FEEDBACK)) {
				  event = new FeedbackEvent(this);
				  event.queUp();			  
			  }
			  else if (RouteSpecs[SELECTEDREPORT] != null){
				  RouteSpecs[SELECTEDREPORT].getSpec().sendCommand();
			  }
			  else {
				  RouteSpecs[NOTSELECTEDREPORT].getSpec().sendCommand();
			  }
			  return true;			  
		  }
//	  } else if (TurnoutHelper.runDispatcherCommand(CommandName)) {
	  } else if (runDispatcherCommand()) {
		  if (MyFeedback.equals(FEEDBACK.NO_FEEDBACK)) {
			  event = new FeedbackEvent(this);
			  event.queUp();			  
		  }
		  return true;
	  }
	  return false;
  }
  
  /**
   * is a request to line the points to a route due to a local request.  To service this request,
   * RouteInfo checks to see what is involved.  If the requested route
   * has a command defined, then the request is forwarded to the TurnoutHelper
   * (to verify that turnouts sharing the command - such as crossovers -
   * can also be lined).  If the requested route does not have a command defined,
   * then the requested can be honored immediately and the feedback (if present)
   * is generated.
   * 
   * @return true if the request can be honored and false if not.
   */
  public boolean requestLocalAlignment() {
	  FeedbackEvent event; 
//	  if ((RouteCmd == null) || TurnoutHelper.runLocalCommand(CommandName)) {
	  if ((RouteCmd == null) || runLocalCommand()) {
		  if (MyFeedback.equals(FEEDBACK.NO_FEEDBACK)) {
			  event = new FeedbackEvent(this);
			  event.queUp();			  
		  }
		  return true;
	  }
	  return false;
  }
  
  /**
   * is called to alert the PtsVitalLogic that the points
   * have been aligned to this route.
   */
  public void sendFeedback() {
	  CountermandEvent override;
	  if (!VitalLogic.receiveFeedback(Destination, true)) {
		  override = new CountermandEvent(this);
		  override.queUp();
	  }
  }

  /**
   * is called to force the points to this alignment.  It is used
   * for reversing an unauthorized points move and for testing.
   */
  public void forceAlignment() {
	  if (RouteCmd != null) {
		  RouteCmd.sendCommand();
	  }
  }
  
  /**
   * returns the kind of feedback on the route
   * @return NO_FEEDBACK if there are no feedback reports
   * <p>POSITIVE_FEEDBACK if there is only one report
   * <p>EXACT_FEEDBACK if there are two reports
   */
  public FEEDBACK getFeedback() {
	  return MyFeedback;
  }
  
  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    return new String("A " + XML_TAG + " cannot contain a text field ("
                      + eleValue + ").");
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptable or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    String resultMsg = null;
    int field;
    for (field = 0; field < RouteName.length; ++field) {
      if (RouteName[field][TAG_INDEX].equals(objName)) {
        RouteSpecs[field] = (Detector) objValue;
        break;
      }
    }
    if (field == RouteName.length) {
      if (RouteCommand[TAG_INDEX].equals(objName)) {
        RouteCmd = ((Detector) objValue).getSpec();
//        CommandName = RouteCmd.createID();
//        RouteCmd.registerLock();
      }
      else {
        resultMsg = new String("A " + XML_TAG + " cannot contain an Element ("
                               + objName + ").");
      }
    }
    return resultMsg;
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error checking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
	  MyFeedback = FEEDBACK.POSITIVE_FEEDBACK;
	  if (((RouteSpecs[SELECTEDREPORT] == null) ||
			  (RouteSpecs[SELECTEDREPORT].getSpec() == null)) &&
			  ((RouteSpecs[NOTSELECTEDREPORT] == null) ||
					  (RouteSpecs[NOTSELECTEDREPORT].getSpec() == null))) {
		  MyFeedback = FEEDBACK.NO_FEEDBACK;
	  }
	  else if (((RouteSpecs[SELECTEDREPORT] != null) &&
			  (RouteSpecs[SELECTEDREPORT].getSpec() != null)) &&
			  ((RouteSpecs[NOTSELECTEDREPORT] != null) &&
					  (RouteSpecs[NOTSELECTEDREPORT].getSpec() != null))) {
		  MyFeedback = FEEDBACK.EXACT_FEEDBACK;
	  }
	  if ((RouteSpecs[SELECTEDREPORT] != null) && (RouteSpecs[SELECTEDREPORT].getSpec() != null)) {
		  new InPlaceReceiver(RouteSpecs[SELECTEDREPORT].getSpec());
	  }
	  if ((RouteSpecs[NOTSELECTEDREPORT] != null) && (RouteSpecs[NOTSELECTEDREPORT].getSpec() != null)) {
		  new ReleaseReceiver(RouteSpecs[NOTSELECTEDREPORT].getSpec());
	  }

	  return null;
  }

  /**
   * registers a RouteInfoFactory and the constituent ItemVectors
   * with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new RouteInfoFactory());
    for (int vec = 0; vec < RouteName.length; ++vec) {
      Detector.init(RouteName[vec][TAG_INDEX]);
    }
    Detector.init(RouteCommand[TAG_INDEX]);
  }
  
	/**
	 * is an inner class that reports on the points being moved into position.
	 */
	private class InPlaceReceiver implements DecoderObserver {

		/**
		 * is the constructor.
		 *
		 * @param c is the track being aligned.
		 * @param decoder is the IOSpec describing the messages that the receiver
		 * is looking for.
		 */

		public InPlaceReceiver(IOSpec decoder) {
			decoder.registerListener(this);
		}


		/*
		 * is the interface through which the MsgFilter delivers the RREvent.
		 */
		public void acceptIOEvent() {
			if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
				log.info("Moving Points into position");
			}
			sendFeedback();
		}
	}

	/**
	 * is an inner class that reports on the points being moved out of position.
	 */
	private class ReleaseReceiver implements DecoderObserver {


		/**
		 * is the constructor.
		 * 
		 *
		 * @param c is the track being aligned.
		 * @param decoder is the IOSpec describing the messages that the receiver
		 * is looking for.
		 */

		public ReleaseReceiver(IOSpec decoder) {
			decoder.registerListener(this);
		}


		/*
		 * is the interface through which the MsgFilter delivers the RREvent.
		 */
		public void acceptIOEvent() {
//			  CountermandEvent override;
//			  if (!VitalLogic.receiveFeedback(Destination, false)) {
//				  override = new CountermandEvent(RouteInfo.this);
//				  override.queUp();
//			  }
			// This probably should force the points back, but queueing
			// up a countermand causes the countermand to be overridden.
			// Eventually, the points will stop and the feedback at that time
			// will force the points
			if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
				log.info("Moving Points out of position");
			}
			VitalLogic.receiveFeedback(Destination, false);
		}
	}
}

/**
 * is a Class known only to the RouteInfo class for creating Routes
 * from an XML document.
 */
class RouteInfoFactory
    implements XMLEleFactory {

  /**
   * the index of the edge on which the route terminates.
   */
  int Term;

  /**
   * is true if the normal route attribute is seen.
   */
  boolean Normal;

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    Term = -1;
    Normal = false;
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;
    int edge;

    if (RouteInfo.ROUTE_ID.equals(tag)) {
      if ( (edge = Edge.toEdge(value)) != Constants.NOT_FOUND) {
        Term = edge;
      }
      else {
        resultMsg = new String(value + " is not a valid value for a " +
                               tag + " attribute.");
      }
    }
    else if (RouteInfo.NORMAL_TAG.equals(tag) &&
             RouteInfo.ISNORMAL.equals(value)) {
      Normal = true;
    }
    else {
      resultMsg = new String("A " + RouteInfo.XML_TAG +
                             " XML Element cannot have a " + tag +
                             " attribute.");
    }
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    RouteInfo route;
    if ( (Term >= 0) && (Term < Sides.EDGENAME.length)) {
      route = new RouteInfo(Term);
      route.setNormal(Normal);
      return route;
    }
    System.out.println("Missing edge attribute for a " + RouteInfo.XML_TAG
                       + " XML element.");
    return null;
  }
}
/* @(#)RouteInfo.java */