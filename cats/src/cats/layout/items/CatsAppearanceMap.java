/* Name: CatsSignalSystem.java
 *
 * What:
 *   This class is a JMRI SignalAppearanceMap (used in conjunction with JMRI
 *   SignalMasts) for CATS.  It extends the JMRI DefaultSignalAppearanceMap.
 *   <p>
 *   It does not need all the features of the JMRI AppearanceMap (e.g.
 *   images).  Those unnecessary features are left undefined or defaulted.
 *   <p>
 *   One of these objects is needed for each signal type (SignalTemplate).
 *   It translates appearance names (e.g. Clear) into aspect names (e.g. green)
 *   and JMRI SignalHead identifiers (e.g. SignalHead.GREEEN).
 *   <p>
 *   This class is needed because the JMRI SignalMast architecture permits
 *   the AppearanceMaps to be created only from XML files, while CATS defines
 *   the maps in SignalTemplates, XML fields within the layout description.
 */
package cats.layout.items;

import javax.swing.JOptionPane;

import cats.layout.SignalTemplate;
import jmri.implementation.DefaultSignalAppearanceMap;

/**
 *   This class is a JMRI SignalAppearanceMap (used in conjunction with JMRI
 *   SignalMasts) for CATS.  It extends the JMRI DefaultSignalAppearanceMap.
 *   <p>
 *   It does not need all the features of the JMRI AppearanceMap (e.g.
 *   images).  Those unnecessary features are left undefined or defaulted.
 *   <p>
 *   One of these objects is needed for each signal type (SignalTemplate).
 *   It translates appearance names (e.g. Clear) into aspect names (e.g. green)
 *   and JMRI SignalHead identifiers (e.g. SignalHead.GREEEN).
 *   <p>
 *   This class is needed because the JMRI SignalMast architecture permits
 *   the AppearanceMaps to be created only from XML files, while CATS defines
 *   the maps in SignalTemplates, XML fields within the layout description.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class CatsAppearanceMap extends DefaultSignalAppearanceMap {
	
	/**
	 * the two argument ctor
	 * @param systemName is the name of the SignalSystem for CATS
	 * @param userName is a JMRI user name for the object created
	 */
	private CatsAppearanceMap(String systemName, String userName) {
		super(systemName, userName);
		registerMap();
	}

//	private CatsAppearanceMap(String systemName) {
//		super(systemName);
//		registerMap();
//	}

	/**
	 * the public constructor of a CatsAppearanceMap.  The Java constructors are wrapped in
	 * this method to make the system robust.  This method verifies that non-blank Signal System
	 * and aspect map names are specified.  If that tests passes, it checks to see that no
	 * CatsAppearanceMap already exists with that name.  Finally, it creates a new CatsAppearanceMap
	 * with a proper JMRI System Name and the aspect map name as the user name.
	 * @param sigSystemName  The name of the Signal System.  Typically, this will be the name
	 * of the CATS Signal System.
	 * @param aspectName  The name of a CATS Signal Template.
	 * @return a CatsAppearanceMap that either already exists or is newly created.  If the system
	 * name is illegal, then null is returned.
	 */
	static public DefaultSignalAppearanceMap createMap(String sigSystemName, String aspectName) {
		String sysName;
		DefaultSignalAppearanceMap map = null;
		if ((sigSystemName == null) || (sigSystemName.trim().length() == 0) ||
				(aspectName == null) || (aspectName.trim().length() == 0)) {
			JOptionPane.showMessageDialog(null, "Warning: illegal Appearance Map specification - " +
				sigSystemName + " (Signal System) " + aspectName + " (Signal Template)");
		}
		sysName = buildSystemName(sigSystemName, aspectName);
		map  = DefaultSignalAppearanceMap.findMap(sysName);
		if (map == null) {
				map = new CatsAppearanceMap(sysName, aspectName);
		}
		return map;
	}
	
	/**
	 * constructs a JMRI system name for a SignalAppearanceMap from a Signal System identifier
	 * (note that this is a piece of the JMRI system name for a Signal System object) and a
	 * the name of an aspect map file.
	 * @param signalSystemName is the Signal System name
	 * @param aspectMapName is the name of an aspect map within that Signal System
	 * @return the properly constructed JMRI system name for the aspect map within the
	 * Signal System
	 */
	static public String buildSystemName(String signalSystemName, String aspectMapName) {
		return "map:" + signalSystemName+ ":" + aspectMapName;
	}
	
//	/**
//	 * searches the CatsAppearanceMap registry for a CatsAppearanceMap with a specific
//	 * Signal System and aspect map name
//	 * @param sigSystemName is the name of the Signal System
//	 * @param aspectName is the name of an aspect map within that system
//	 * @return a map, if one can be found or null
//	 */
//	static public CatsAppearanceMap findCatsMap(String sigSystemName, String aspectName) {
//		return CatsMaps.get(buildSystemName(sigSystemName, aspectName));
//	}
	
//	/**
//	 * Searches the CatsAppearanceMap registry for a CatsAppearanceMap with a specific
//	 * JMRI System Name
//	 * @param mapName is the JMRI System Name
//	 * @return the msp, if it csn be found or null if it cannot be found.
//	 */
//	static public CatsAppearanceMap findCatsMap(String mapName) {
//		return CatsMaps.get(mapName);
//	}
	
//	/**
//	 * searches for a DefaultSignlaAppearanceMap, first in the CATS registry and if not
//	 * found, then in the JMRI DefaultSignalAppearanceMap registry.  This order is crucial because
//	 * if the JMRI registry is searched and no matching entry is found, JMRI will create one.
//	 * Thus, the next search will find one.
//	 * @param sigSystemName is the nmae of a Signal System
//	 * @param aspectName is the name of an aspect map within that Signal System
//	 * @return a DefaultSignalAppearanceMap who fits the description.  null cannot be returned
//	 * because JMRI will create a map if none is found.
//	 */
//	static public DefaultSignalAppearanceMap findAppearanceMap(String sigSystemName, String aspectName) {
//		DefaultSignalAppearanceMap map = findCatsMap(sigSystemName, aspectName);
//		if (map == null) {
//			try {
//			map = getMap(sigSystemName, aspectName);
//			}
//			catch (IllegalArgumentException iae) {
//				JOptionPane.showMessageDialog(null, "warning: An signal appearance map for signal system "
//						+ sigSystemName + " and aspect map " + aspectName + " does not exist");
//			}
//		}
//		return map;
//	}
	
	/**
	 * fills in the contents of the CatsAppearanceMap from the SignalTemplate
	 * @param template is the template
	 */
	public void completeMap(SignalTemplate template) {
		aspectAttributeMap = template.getAspectMap().createAttributeMap(template.getAppearanceKeys());
		table = template.getAspectMap().createAspectMap(template.getAppearanceKeys());
		//specificMaps =
		//aspectRelationshipMap =
	}
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CatsAppearanceMap.class.getName());

}
/* @(#)CatsAppearanceMap.java */
