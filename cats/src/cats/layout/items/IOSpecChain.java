/* Name: IOSpecChain.java
 *
 * What:
 *  This is a class for holding multiple IOSpecs.  Though it may change as
 *  IOSPecs are added to it during the construction process, they will
 *  not be removed or changed.
 *  <p>
 *  The implementation would be pretty easy if the Chain contained only
 *  IOSpecs.  Including delays complicates things considerably.
 *  <ol>
 *  <li> the Chain should run in its own thread because the wait will block
 *  any thread that the wait is executing on.</li>
 *  <li>Because of the wait, another call could be made to the Chain
 *  or its mirror image, during the wait.  This means the Chain must be
 *  re-enterant.  In fact, perhaps all Chain must be re-entrant and
 *  sequenced in time</li>
 *  </ol>
 *  </p>
 *  <p>
 *  The Throw selection activates the Chain - executes each decoder command in the
 *  Chain.  The Close selection is more ambiguous.  It could execute the inverse
 *  of each decoder command, but that is often not the desired action.  Consider
 *  a yard ladder.  The Throw sets all the turnouts, except one to the ladder.
 *  The lone exception is set to a particular yard track.  If the Close is issued,
 *  all tracks except one are set to yard tracks and the one is set to the ladder.
 *  Thus, how to "undo" is probably situation dependent.  Therefore, when a Chain
 *  is commanded to Throw, all the decoders in the Chain will be commanded as defined
 *  and the Chain is "active".  If any decoder changes state, then the Chain is "inactive".
 *  This implies that the Chain listens for changes in its constituent decoders and
 *  sets its state from their values.
 *  </p>
 */
package cats.layout.items;

import cats.jmri.IOSpecChainManager;
import cats.layout.BiDecoderObserver;
import cats.layout.xml.*;
import cats.rr_events.EventInterface;
import jmri.beans.PropertyChangeSupport;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;


/**
 *  This is a class for holding multiple IOSpecs.  Though it may change as
 *  IOSPecs are added to it during the construction process, they will
 *  not be removed or changed.
 *  <p>
 *  The implementation would be pretty easy if the Chain contained only
 *  IOSpecs.  Including delays complicates things considerably.
 *  <ol>
 *  <li> the Chain should run in its own thread because the wait will block
 *  any thread that the wait is executing on.</li>
 *  <li>Because of the wait, another call could be made to the Chain
 *  or its mirror image, during the wait.  This means the Chain must be
 *  re-enterant.  In fact, perhaps all Chain must be re-entrant and
 *  sequenced in time</li>
 *  </ol>
 *  </p>
 *  <p>
 *  The Throw selection activates the Chain - executes each decoder command in the
 *  Chain.  The Close selection is more ambiguous.  It could execute the inverse
 *  of each decoder command, but that is often not the desired action.  Consider
 *  a yard ladder.  The Throw sets all the turnouts, except one to the ladder.
 *  The lone exception is set to a particular yard track.  If the Close is issued,
 *  all tracks except one are set to yard tracks and the one is set to the ladder.
 *  Thus, how to "undo" is probably situation dependent.  Therefore, when a Chain
 *  is commanded to Throw, all the decoders in the Chain will be commanded as defined
 *  and the Chain is "active".  If any decoder changes state, then the Chain is "inactive".
 *  This implies that the Chain listens for changes in its constituent decoders and
 *  sets its state from their values.
 *  </p>
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2003, 2020, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class IOSpecChain extends PropertyChangeSupport implements XMLEleObject {
  /**
   *   The XML tag for recognizing the Block description.
   */
  public static final String XML_TAG = "IOSPECCHAIN";
  
  /**
   * The JMRI prefix for an IOSpecChain
   */
  public static final String CHAIN_PREFIX = "IC";
  
  /**
   * is the String used in property changes for identifying a state change
   */
  public static final String STATE_TAG = "KnownState";
  
  /**
   * is the IOSpecChain instantiation counter for converting an
   * older XML file to the latest.  0 is reserved to be the
   * temporary address during a copy operation.
   */
  static private int Counter = 1;
  
  /**
   * is the address to send the trigger to.
   */
  private int DecAddr;

  /**
   * is the number of milliseconds to wait after starting the
   * chain before starting the next command in the parent chain.
   */
  private int Delay;
  
  /**
   * is the JMRI user name
   */
  private String UserName;
  
  /**
   * is the list of composite IOSpecs.
   */
  private ArrayList<IOSpec> Specs;

  /**
   * is the list of IOSpecs, with delay
   */
  private ArrayList<DoChainAction> ThrowList;
  
//  /**
//   * is the list of inverse IOSPecs, with delay
//   */
//  private ArrayList<EventInterface> CloseList;
  
  /**
   * is the last commanded state.
   */
  private boolean LastState = true;
  
//  /**
//   * is the delay timer
//   */
//  private javax.swing.Timer DelayTimer;
  
//  /**
//   * are the Objects that send the commands.
//   */
//  private CommandBot NormalCommand;  // sends the default command
//  private CommandBot UndoCommand;    // sends the Undo command
//  private CommandBot ForceCommand;   // sends the Force state command 

//  /**
//   * remembers which request is being made.
//   */
//  private CommandBot ExecutingCommand;
  
  /**
   * is the null parameter ctor.  It is called when an
   * anonymous IOSpecChain is encountered.  This happens
   * only when migrating from a version of CATS prior to
   * the designer 21 version.  This means if one is seen,
   * no non-anonymous IOSpecLists will be seen, so we can
   * sequentially number them, as found.
   */
  public IOSpecChain() {
    this(Counter++);
  }
  
  /**
   * is the ctor, when the "system address" (number) is
   * known.
   * 
   * @param addr is the system identifier
   */
  public IOSpecChain(int addr) {
    Specs = new ArrayList<IOSpec>();
    ThrowList = new ArrayList<DoChainAction>();
//    CloseList = new ArrayList<EventInterface>();
    DecAddr = addr;
   
//    NormalCommand = new CommandBot() {
//    	public void runCommand(IOSpec decoder) {
//    		decoder.sendCommand();
//    		firePropertyChange(STATE_TAG, state, true);
//    	}
//    };
//    
//    UndoCommand = new CommandBot() {
//      public void runCommand(IOSpec decoder) {
//        decoder.sendUndoCommand();
//		firePropertyChange(STATE_TAG, state, false);
//      }
//    };
//    
//    ForceCommand = new CommandBot() {
//      public void runCommand(IOSpec decoder) {
//        decoder.forceState(state);
//      }
//    };
  }
  
  /**
   * sends a command to the decoders to set them to their normal
   * state.
   */
  public void sendCommand() {
    runCommand(ThrowList);
    firePropertyChange(STATE_TAG, LastState, true);
    LastState = true;
  }
  
  /**
   * sends the deactivate command (if it exists).  This sets the
   * decoder to its other state.
   */
  public void sendUndoCommand() {
//    runCommand(CloseList);
    firePropertyChange(STATE_TAG, LastState, false);
    LastState = false;
  }
  
  /**
   * initiates running a sequence of commands.  If there is a delay
   * between any, it starts the timer.
   * 
   * @param bot is the object holding the command,
   */
//  private void runChain(CommandBot bot) {
//    ExecutingCommand = bot;
//    ExecutingCommand.nextDecoder = 0;
//    ExecutingCommand.runNext();
//  }
  private void runCommand(final ArrayList<DoChainAction> actions) {
	  for (EventInterface a : actions) {
		  ChainActionManager.EventQue.append(a);
	  }
  }

  /**
   * retrieves the decoder's name, which is used to identify it to the
   * control structure.
   *
   * @return the name of the decoder.
   */
  public String getName() {
    return new String(CHAIN_PREFIX + String.valueOf(DecAddr));
  }
  
  /**
   * retrieves the JMRI User Name.
   * 
   * @return the JMRI User Name fo rthe Chain.
   */
  public String getUserName() {
    return UserName;
  }
  
  /**
   * saves the JMRI User Name.
   * @param name is the User defined name.
   */
  public void setUserName(String name) {
    UserName = name;
  }
  
  /**
   * adds an additional command to the Chain.
   * 
   * @param spec is the command being added
   */
  public void addSpec(IOSpec spec) {
    Specs.add(spec);
  }
  
  /**
   * retrieves the IOSpec composing the chain
   * @return the IOSpecs added to the chain.  It may contain nothing,
   * but it will not be null.
   */
  public ArrayList<IOSpec> getDecoders() {
	  return Specs;
  }
  
  /**
   * determines the state of the chain.  If all the IOSpecs are
   * in their "active" state, then the chain is "active"; otherwise,
   * it is "inactive".  All listeners are informed when the state changes.
   */
  public void determineState( ) {
	  boolean newState;
	  newState = true;
	  for (DoChainAction d : ThrowList) {
		  if (!d.Active) {
			  newState = false;
			  break;
		  }
	  }
	  if (newState != LastState) {
		  firePropertyChange(new PropertyChangeEvent(this, STATE_TAG, LastState, newState));
		  LastState = newState;
	  }
  }

//  /**
//   * Locks the decoder command while the route is locked through a turnout so that
//   * no other decoder can move the points.
//   */
//  public void lockOutCommand() {
//    for (IOSpec spec : Specs) {
//      spec.lockOutCommand();      
//    }
//  }
//  
//  /**
//   * Unlocks the decoder command when a route through a turnout has cleared.  The points
//   * can again be moved.
//   */
//  public void unlockOutCommand() {
//	  for (IOSpec spec : Specs) {
//		  spec.unlockOutCommand();      
//	  }
//  }
//  
//  /**
//   * tests if the decoder command has been locked out.
//   * @return true if the command should not be sent. False if it is safe to send it.
//   */
//  public boolean isLockedOut() {
//	  for (IOSpec spec : Specs) {
//		  if (spec.isLockedOut()) {
//			  return true;
//		  }
//	  }
//	  return false;
//  }

//  /**
//   * registers the decoder with the LockedDecoder as a candidate for having the lock
//   * treatment.
//   */
//  public void registerLock() {
//    for (Enumeration<IOInterface> iter = Specs.elements(); iter.hasMoreElements(); ) {
//      iter.nextElement().registerLock();      
//    }
//  }

  /**
   * replaces the Delay value.
   * 
   * @param d is the new delay in milliseconds.
   */
  public void setDelay(int d) {
      Delay = d;
  }
  
  /**
   * retrieves the Delay value.
   * 
   * @return the delay in milliseconds after the previous command
   *  in a chain.
   */
  public int getDelay() {
      return Delay;
  }
  
  /**
   * return the last known state
   * @return true if the last command was "do" and false if it was "undo"
   */
  public boolean getKnownState() {
	  return LastState;
  }
  
//  /**
//   * Start the timer that delays before sending the next command
//   * 
//   * @param delay is the amount of time to delay in milliseconds
//   */
//  protected void startDelay(int delay) {
//    if (DelayTimer==null) {
//      DelayTimer = new javax.swing.Timer(delay, new java.awt.event.ActionListener() {
//        public void actionPerformed(java.awt.event.ActionEvent e) {
//          timeout();
//        }
//      });
//      DelayTimer.setInitialDelay(delay);
//      DelayTimer.setRepeats(false);
//    }
//    DelayTimer.start();
//  }
// 
//  /**
//   * is invoked when the timeout delay from the previous command
//   * in the chain has expired.  This is the trigger to send the
//   * next command in the chain.
//   */
//  private void timeout() {
//	  ExecutingCommand.runNext();
//  }

//  /**
//   * is borrowed from IOSPec to register the PtsEdge with all users of the Chain
//   * @param points is the PtsEdge that uses the Chain
//   */
//  public void registerUser(final PtsEdge points) {
//	  for (IOSpec spec : Specs) {
//		  spec.registerUser(points);      
//	  }
//  }

  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    return new String("An " + XML_TAG + " cannot have a text value (" +
        eleValue + ").");
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptable or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    String result = null;
    if (IOSpec.XML_TAG.equals(objName)) {
      Specs.add((IOSpec) objValue);
    }
    else {
      result = new String("An " + XML_TAG + " cannot contain a " +
          objName + ".");
    }
    return result;
  }
  
  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }
  
  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error checking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
	  /**
	   * creates list of throw commands
	   */
	  for (int index = 0; index < Specs.size(); ++index) {
		  ThrowList.add(new DoChainAction(Specs.get(index)));
	  }
	  
//	  /**
//	   * creates the list of Close commands
//	   */
//	  for (int index = 0; index < Specs.size(); ++index) {
//		  CloseList.add(new UndoChainAction(Specs.get(index)));
//	  }
	  
    return null;
  }
  
  /**
   * registers an IOSpecChainFactory with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new IOSpecChainFactory());
  }

/******************************************************************************
 * internal classes
 *****************************************************************************/
/**
 * is a Class known only to the IOSpecChain class that implements
 * the command design pattern.  This makes an object out of the request being
 * sent to the links in the chain.  It calls the sendCommand() method in
 * each IOSpec.
 */
  public class DoChainAction implements EventInterface, BiDecoderObserver  {
		
		/**
		 * the IOSpec command, encapsulated as an Object
		 */
		private final IOSpec Decoder;
		
		/**
		 * the last recorded state of the constituent IOSpec.  True means
		 * it is as specified (Throw or Closed).  False means it is the other
		 * polarity
		 */
		public boolean Active;
		
		/**
		 * is the ctor
		 * @param d is the IOSpecChain
		 */
		public DoChainAction(final IOSpec d) {
			Decoder = d;
			Decoder.registerListener(this);
			Decoder.registerOtherListener(this);
		}
		
		@Override
		public void doIt() {
			Decoder.sendCommand();
			ChainActionManager.ChainHandler.setMsgDelay(Decoder.getDelay());
		}

		@Override
		public void acceptIOEvent() {
//			System.out.println("Decoder " + Decoder.getDevice().getSystemName() + " in Chain " + UserName + " is active");
			Active = true;
			determineState();
		}

		@Override
		public void acceptOtherIOEvent() {
//			System.out.println("Decoder " + Decoder.getDevice().getSystemName() + " in Chain " + UserName + " is inactive");
			Active = false;
			determineState();
		}
	}

//  /**
//   * is a Class known only to the IOSpecChain class that implements
//   * the command design pattern.  This makes an object out of the request being
//   * sent to the links in the chain. It calls the sendUndoCommand() method in
//   * each IOSpec.
//   */
//
//  public class UndoChainAction implements EventInterface {
//		
//		/**
//		 * the IOSpec command, encapsulated as an Object
//		 */
//		private final IOSpec Decoder;
//		
//		/**
//		 * is the ctor
//		 * @param d is IOSpecChain
//		 */
//		public UndoChainAction(final IOSpec d) {
//			Decoder = d;
//		}
//		
//		@Override
//		public void doIt() {
//			Decoder.sendUndoCommand();
//			ChainActionManager.ChainHandler.setMsgDelay(Decoder.getDelay());
//		}
//	}

//  private abstract class CommandBot {
//    /**
//     * is the parameter for the forceState command.
//     */
//    public boolean state;
//    
//    /**
//     * is the index of the decoder that will be triggered next.
//     */
//    public int nextDecoder;
//    
//    /**
//     * is the specialization for each request.
//     * 
//     * @param decoder is the IOSpec of the decoder being triggered
//     */
//    public abstract void runCommand(IOSpec decoder);
//
//    /**
//     * walks through the Chain, inserting delays where needed.
//     */
//    public void runNext() {
//      IOSpec decoder;
//      while (nextDecoder < (Specs.size() - 1)) {
//        decoder = Specs.get(nextDecoder++);
//        runCommand(decoder);
//        if (decoder.getDelay() > 0) {
//          startDelay(decoder.getDelay());
//          return;
//        }
//      }
//      if (nextDecoder >= 0) {
//        runCommand(Specs.get(nextDecoder));
//      }
//    }
//  }
}

/**
 * is a Class known only to the IOSpecChain class for creating IOSpecChains from
 * an XML document.
 */
class IOSpecChainFactory
implements XMLEleFactory {
 
  /**
   * is the address to send the command to.
   */
  String Recipient;
  
  /**
   * is the User Name.
   */
  String UserName;

  /**
   * is the Delay field.
   */
  int Delay;

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    Recipient = null;
    UserName = null;
    Delay = 0;
  }
  
  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;
    if (IOSpec.DECADDR.equals(tag)) {
      Recipient = value;
    }
    else if (IOSpec.DELAY.equals(tag)) {
      try {
          int addr = Integer.parseInt(value);
          Delay = addr;
        }
        catch (NumberFormatException nfe) {
          System.out.println("Illegal delay for " + IOSpec.XML_TAG
                             + "XML Element:" + value);
        }        
  }
    else if (IOSpec.USER_NAME.equals(tag)) {
      UserName = value;
    }
    else {
      resultMsg = new String("A " + IOSpecChain.XML_TAG +
          " XML Element cannot have a " + tag +
      " attribute.");
    }
    return resultMsg;
  }
  
  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    IOSpecChain spec = IOSpecChainManager.instance().newChain(IOSpecChain.CHAIN_PREFIX + Recipient, UserName);
    spec.setDelay(Delay);
    return spec;
  }
}
/* @(#)IOSpecChain.java */

