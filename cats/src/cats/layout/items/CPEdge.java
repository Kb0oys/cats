/* Name: CPEdge.java
 *
 * What:
 *   This class is the container for all the information about one
 *   of a Section's Edges which "is a" Control Point boundary.
 */

package cats.layout.items;

import cats.apps.Crandic;
import java.awt.Point;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.jdom2.Element;

import cats.automatedTests.TestMaker;
import cats.common.Constants;
import cats.common.DebugBits;
import cats.gui.CounterFactory;
import cats.gui.GridTile;
import cats.gui.MouseUser;
import cats.gui.Screen;
import cats.gui.frills.SignalFrill;
import cats.gui.jCustom.CATSOptionPane;
import cats.gui.jCustom.JListDialog;
import cats.layout.ColorList;
import cats.layout.Discipline;
import cats.layout.MasterClock;
import cats.layout.StackedRouteViewer;
import cats.layout.TimeoutObserver;
import cats.layout.codeLine.CodeMessage;
import cats.layout.codeLine.CodePurpose;
import cats.layout.codeLine.Codes;
import cats.layout.codeLine.packetFactory.EdgePacketFactory;
import cats.layout.ctc.RouteCreator;
import cats.layout.ctc.RouteNode;
import cats.layout.ctc.RouteNodePts;
import cats.layout.ctc.RouteNodeStatus;
import cats.layout.ctc.StackOfRoutes;
import cats.layout.vitalLogic.BasicVitalLogic;
import cats.layout.vitalLogic.CPVitalLogic;
import cats.layout.vitalLogic.LogicLocks;
import cats.rr_events.TimeoutEvent;
 
/**
 * is a Control Point boundary.  A Control Point boundary is a Block boundary
 * with a visible signal.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2009, 2010, 2011, 2012, 2014, 2018, 2019, 2020, 2021, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class CPEdge extends IntermediateEdge implements RouteEdge {

	/**
	 * the labels for setting the menu items on right click.
	 * There is one of of these for each item in ButtonValues
	 */
	private static final String[] SET_MENU_ITEMS = {
			"Set N/X Route",
			"Stack Route",
			"Turn on Fleeting",
			"Grant Track Authority",
			"Take Out Of Service",
			"Set Call-on",
			"Reset Route",
			"View/Change Stacked Routes",
			""
	};

	/**
	 * the labels for canceling the menu items on right click.
	 * There is one of of these for each item in ButtonValues
	 */
	private static final String[] CANCEL_MENU_ITEMS = {
			"Cancel N/X Route",
			"Cancel Stack Route",
			"Cancel Fleeting",
			"Remove Track Authority",
			"Remove Out Of Service",
			"Cancel Call-on",
			"Reset Route",
			"View/Change Stacked Routes",
			""
	};

	/**
	 * the labels for indicating that the operation is not valid.
	 * There is one of of these for each item in ButtonValues
	 */
	private static final String[] DISABLE_MENU_ITEMS = {
			"N/X Route Disabled",
			"Stack Route Disabled",
			"Fleeting Disabled",
			"Track Authority Disabled",
			"Out Of Service Disabled",
			"Call-on Disabled",
			"Reset Route",
			"View/Change Stacked Routes",
			""
	};

	/**
	 * the kind of label to put on the right button menu
	 */
	private enum ButtonValues {
		ENABLE,		// the button is an option 
		CANCEL,		// the button is to cancel an option
		DISABLE		// the button is disabled
	}
	
	// indexes (I know, these could be Enums)
	private static final int NX_MENU = 0;
	private static final int PRECONDITION_MENU = NX_MENU + 1;
	private static final int FLEET_MENU = PRECONDITION_MENU + 1;
	private static final int TNT_MENU = FLEET_MENU + 1;
	private static final int OOS_MENU = TNT_MENU + 1;
	private static final int CALLON_MENU = OOS_MENU + 1;
	private static final int RESET_MENU = CALLON_MENU + 1;
	private static final int ROUTE_MENU = RESET_MENU + 1;
	private static final int LEFT_MOUSE = ROUTE_MENU + 1;

	/**
	 * a set of right menu label states.  There are probably better ways of doing
	 * this involving maps, but these are static and can be initialized when declared.
	 * 
	 * The state order is:
	 * 0 - ACTIVE, ROLLUP, PREPARATION, without an active route (N/X)
	 * 1 - IDLE with no local detection, but advance detection (possible call-on)
	 * 2 - IDLE with no locks
	 * 3 - IDLE with obstructed block
	 * 4 - FLEET, no N/X
	 * 5 - CALL-ON
	 * 6 - PENDING, CLEARING
	 * 7 - ACTIVE, ROLLUP, PREPARATION, with an active route (N/X)
	 * 8 - Fleet, N/X
	 * The button state order is: NX, Stack, Fleet, TnT,
	 *                            OOS, Call-on, Reset, Stack Edit, Left Button
	 */
	private static final ButtonValues[][] BUTTON_STATES = {
			// Active, Roll-up, Preparation (with N/X)
			{ButtonValues.DISABLE, ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.DISABLE, 
				ButtonValues.DISABLE, ButtonValues.DISABLE, ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.ENABLE},
			// Idle, global detection, no local detection
			{ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.DISABLE, ButtonValues.DISABLE,
				ButtonValues.DISABLE, ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.DISABLE},
			// Idle, no locks
			{ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.DISABLE, ButtonValues.DISABLE,
				ButtonValues.DISABLE, ButtonValues.DISABLE, ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.ENABLE},
			// Idle with locks
			{ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.DISABLE, ButtonValues.DISABLE,
				ButtonValues.DISABLE, ButtonValues.DISABLE, ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.ENABLE},
			// Fleet
			{ButtonValues.DISABLE, ButtonValues.ENABLE, ButtonValues.CANCEL, ButtonValues.DISABLE,
				ButtonValues.DISABLE, ButtonValues.DISABLE, ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.ENABLE},
			// Call-on
			{ButtonValues.DISABLE, ButtonValues.ENABLE, ButtonValues.DISABLE, ButtonValues.DISABLE,
				ButtonValues.DISABLE, ButtonValues.CANCEL, ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.ENABLE},
			// Pending, Clearing
			{ButtonValues.DISABLE, ButtonValues.ENABLE, ButtonValues.DISABLE, ButtonValues.DISABLE,
				ButtonValues.DISABLE, ButtonValues.DISABLE, ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.DISABLE},
			// ACTIVE, ROLLUP, PREPARATION, with an active route (N/X)
			{ButtonValues.CANCEL, ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.DISABLE,
					ButtonValues.DISABLE, ButtonValues.DISABLE, ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.ENABLE},
			// Fleet, N/X
			{ButtonValues.CANCEL, ButtonValues.ENABLE, ButtonValues.CANCEL, ButtonValues.DISABLE,
						ButtonValues.DISABLE, ButtonValues.DISABLE, ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.ENABLE},
			// OOS
			{ButtonValues.DISABLE, ButtonValues.ENABLE, ButtonValues.DISABLE, ButtonValues.DISABLE,
							ButtonValues.CANCEL, ButtonValues.DISABLE, ButtonValues.ENABLE, ButtonValues.ENABLE, ButtonValues.DISABLE}			
	};

	//	/**
	//	 * the state of the Control Point (Head) signal
	//	 */
	//	private enum SigValues {
	//		NO_ACTION,	// the dispatcher has initiated nothing
	//		PENDING,	// the dispatcher placed a request, that has not been answered, yet
	//		ACTIVE,		// the requested action has been acknowledged
	//		CLEARING	// the dispatcher has requested that the Action be cancelled
	//	}

	/**
	 * these are the states of the Control Point GUI
	 */
	private enum EdgeStates {
		IDLE,		// no route
		PENDING,	// a route or call-on has been requested and is waiting for Vital Logic confirmation
		ACTIVE,		// the requested route has been acknowledged; no FLEETING
		ROLL_UP,	// one or more trains are moving through the RouteZone, ACTIVE to IDLE
		FLEET,		// fleeting through the route is enabled; ACTIVE with FLEETING
		CLEARING,	// the dispatcher has cancelled the route or call-on and is waiting for Vital Logic confirmation
		CALLON,		// call-on is active
		PREPARATION	// a Route has been stacked, but is waiting for the nodes to not block and points to line
	}

	/**
	 * these are the steps in the state machine that moves points on a route
	 */
	private enum DELAY_STEPS {
		NO_DELAY,
		OCCUPANCY_DELAY,
		MOVEMENT_DELAY,
		CANCEL_DELAY
	}

	/**
	 * is the signal's icon on the panel
	 */
	private final PanelSignal Icon;

	/**
	 * is the color if the signal icon when there is no route
	 * in CTC or DTC.  It could be DARK (no physical signal) or
	 * NO_ROUTE (physical signal)
	 */
	private final String NoRoute;
	
	/**
	 * is true if using CTC or DTC
	 */
	private boolean DispatcherControlled = false;

	/**
	 * route state machine
	 */
	private final RouteStateMachine RouteMachine = new RouteStateMachine();
	
	/**
	 * the method to receive the second mouse click when defining an eXtended Route
	 */
	private final SecondClick RouteEnd;
	
	/**
	 * a state machine for moving points
	 */
	private final DelayedMovement DelaySwitch = new DelayedMovement();
	
	/**
	 * a shortcut for determining call-on eligibility
	 */
	private boolean CallOnCapable;
	
	//	/**
	//	 * fleeting state machine
	//	 */
	//	private final FleetStateMachine FleetMachine = new FleetStateMachine();
	//
	//	/**
	//	 * call-on state machine
	//	 */
	//	private final CallOnStateMachine CallOnMachine = new CallOnStateMachine();

	/**
	 * constructs a CPEdge with only its Edge identifier.
	 *
	 * @param edge identifies the side of the GridTile the SecEdge is on.
	 * @param shared describes the shared edge, if it is not adjacent.
	 *    A null value means it is adjacent.
	 * @param block is the Block definition.  If it is null, then the
	 *   a default block is created, which may be overwritten during the
	 *   bind process.
	 * @param blkSignal is a Signal.  It may not be null.
	 */
	public CPEdge(int edge, Edge shared, Block block, SecSignal blkSignal) {
		super(edge, shared, block, blkSignal);
		MySignal = blkSignal;
		NoRoute = (MySignal.getLayoutSignal() == null) ? ColorList.DARK : ColorList.NO_ROUTE;
		Icon = MySignal.getSigIcon();
		if (Icon != null) {
			Icon.protectEdge(this);
			Icon.setGUIPresentation(NoRoute, NoRoute);
		}
		RouteEnd = new SecondClick();
	}

	@Override
	public EdgeInterface.EDGE_TYPE getEdgeType() {
		return EdgeInterface.EDGE_TYPE.CP_EDGE;
	}

	@Override
	public void setBlock(Block block) {
		super.setBlock(block);
		switch (MyBlock.getDiscipline()) {
		case DTC:
		case CTC :
			DispatcherControlled = true;
			Icon.holdSignal(true);
			break;
		default:
		}
	}
	/**
	 * registers the SecEdge's icon on the painting surface.
	 *
	 * @param tile is where the Frills are painted.
	 *
	 * @see cats.gui.GridTile
	 */
	@Override
	public void install(GridTile tile) {
		EdgeTile = tile;
		if (EdgeTile != null) {
			MySignal.install(EdgeTile);
			EdgeTile.setSide(MyEdge, true);
			Icon.setGUIPresentation(NoRoute, NoRoute);
		}
	}

	/**
	 * handles a mouse button push when positioned over a Signal Icon.
	 *
	 * If the left mouse button is pushed, then it is interpreted as a request
	 * to set traffic through the Block, beginning at this CPEdge.  If the
	 * right button is pushed, then a menu is presented.
	 *
	 * @param me is the MouseEvent recording the button push/release.
	 *
	 * @see java.awt.event.MouseEvent
	 */
	public void edgeMouse(MouseEvent me) {
//		if (DispatcherControlled || (MyBlock.getDiscipline().equals(Discipline.APB))) {
		if (DispatcherControlled) {
			int mods = me.getModifiers();
			if ( (mods & MouseEvent.BUTTON3_MASK) != 0) {
				rightClick(me.getPoint());
			}
			else if ( (mods & MouseEvent.BUTTON1_MASK) != 0) {
				recordTestCase("Left click on signal");
				//				leftClick();
				if (isLeftMouseAllowed()) {
					RouteMachine.leftClick(me.getPoint());
				}
				else {
					if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
						log.info("RouteMachine State=" + RouteMachine.getLabelStates() + " CommonLocks="
								+ CommonLocks);
					}
					CATSOptionPane.showErrorMessage(me.getPoint(), "A route cannot be created due to conflicts.", "Route Conflict");
				}
			}
		}
		else {
			CATSOptionPane.showErrorMessage(me.getPoint(), "Left mouse clicks are accepted on only CTC and DTC control point signals", "No Mouse Clicks");
		}
	}

	/**
	 * context sensitive handling of a mouse left-click
	 */
	//	private void leftClick() {
	//		SigValues state = RouteMachine.getState();
	//		if (state.equals(SigValues.NO_ACTION) || state.equals(SigValues.CLEARING)) {
	//			RouteMachine.createEvent();
	//		}
	//		else {
	//			RouteMachine.cancelEvent();
	//		}
	//	}

	/**
	 * creates the context sensitive menu for a right click
	 * @param location is the location of the Mouse on the screen.
	 *
	 * @see cats.gui.jCustom.JListDialog
	 */
	private void rightClick(Point location) {
		String menu[] = new String [LEFT_MOUSE];
		boolean[] flags = new boolean[LEFT_MOUSE];
		//		boolean setActive[] = {true, true};
		int selection;
		int stateIndex = RouteMachine.getLabelStates();
		//		if (FleetMachine.getState().equals(SigValues.ACTIVE) || FleetMachine.getState().equals(SigValues.PENDING)) {
		//			menu.add(CancelMenuItems[0]);
		//			setActive[0] = false;
		//		}
		//		else {
		//			menu.add(SetMenuItems[0]);
		//		}
		if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
			System.out.println(toString() + "RouteMachine State=" + RouteMachine.getLabelStates() + " CommonLocks="
					+ CommonLocks);
		}
		for (int item = 0; item < LEFT_MOUSE; ++item) {
			if ((item == CALLON_MENU) && !CallOnCapable) {
				menu[item] = DISABLE_MENU_ITEMS[CALLON_MENU];
				flags[item] = false;
			}
			else if ((item == ROUTE_MENU) && StackOfRoutes.instance().isStackEmpty()) {
				menu[item] = DISABLE_MENU_ITEMS[item];
				flags[item] = false;
			}
			else {
				switch (BUTTON_STATES[stateIndex][item]) {
				case ENABLE:
					menu[item] = SET_MENU_ITEMS[item];
					flags[item] = true;
					break;

				case CANCEL:
					menu[item] = CANCEL_MENU_ITEMS[item];
					flags[item] = true;
					break;

				case DISABLE:
					menu[item] = DISABLE_MENU_ITEMS[item];
					flags[item] = false;
					break;
				}
			}
		}
		//		if (Icon.isLamp()) {
		//			if (CallOnMachine.getState().equals(SigValues.ACTIVE) || CallOnMachine.getState().equals(SigValues.PENDING)) {
		//				menu.add(CancelMenuItems[1]);
		//				setActive[1] = false;
		//			}
		//			else {
		//				menu.add(SetMenuItems[1]);
		//			}
		//		}
		if ((selection = JListDialog.select(menu, flags, "Traffic Operation", location)) >= 0) {
			if (BUTTON_STATES[stateIndex][selection].equals(ButtonValues.DISABLE)) {
				CATSOptionPane.showErrorMessage(location, "That route option is disabled at this time.", "No Option");
			}
			else {
				switch (selection) {
				case FLEET_MENU:
					if (BUTTON_STATES[stateIndex][selection].equals(ButtonValues.ENABLE)) {
						recordTestCase("Set Fleet", location);
						//					FleetMachine.createEvent();
						if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
							log.info(Constants.ROUTE_CREATION + toString() + " Enable Fleeting CommonLocks="
									+ CommonLocks);
						}
						if (ActiveRoute == null) {
							RouteMachine.fleetChange(true);
						}
						else {
							changeRouteFleeting(true);
						}
					}
					else {
						recordTestCase("Cancel Fleet", location);
						//						FleetMachine.cancelEvent();
						if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
							log.info(Constants.ROUTE_CREATION + toString() + " Cancel Fleeting");
						}
						if (ActiveRoute == null) {
							RouteMachine.fleetChange(false);
						}
						else {
							cancelExtendedRoute();
						}
					}
					break;
				case CALLON_MENU:
					if (BUTTON_STATES[stateIndex][selection].equals(ButtonValues.ENABLE)) {
						recordTestCase("Set Call-on", location);
						if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
							log.info(Constants.ROUTE_CREATION + toString() + " Enable Call on " + " CommonLocks="
									+ CommonLocks);
						}
						RouteMachine.callOnChange(true);
					}
					else {
						recordTestCase("Cancel Call-on", location);
						if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
							log.info(Constants.ROUTE_CREATION + toString() + " Cancel Call on");
						}
						RouteMachine.callOnChange(false);
					}
					break;
				case RESET_MENU:
					log.info(Constants.ROUTE_CREATION + "Edge " + toString() + " Reset EdgeLocks=" + EdgeLocks.toString() +
							" CommonLocks=" + CommonLocks.toString());
					if (ActiveRoute != null) {
//						if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
//							log.info(Constants.ROUTE_CREATION + toString() + " Reset CP" + " CommonLocks="
//									+ CommonLocks);
//						}
						RouteNode temp = ActiveRoute;
						for (RouteNode node = ActiveRoute; node != null; node = node.getControlNode()) {
							if (node.getRouteEdge() != null) {
								((CPEdge) node.getRouteEdge()).RouteMachine.resetRoute();
							}
						}
						temp.removeNodes(null);
					}
					else {
						RouteMachine.resetRoute();
					}
					break;
				case NX_MENU:
					if (ActiveRoute == null) {
						if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
							log.info(Constants.ROUTE_CREATION + "Create Extended route at " + toString() + " CommonLocks="
									+ CommonLocks);
						}
						RouteEnd.startXRoute(location, selection);
					}
					else {
						if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
							log.info(Constants.ROUTE_CREATION + "Cancel Extended route at " + toString());
						}
						cancelExtendedRoute();
					}
					break;
				case ROUTE_MENU:
					if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
						log.info(Constants.ROUTE_CREATION + "Showing route stack");
					}
					// StackOfRoutes.displayRoutes(location);
					new StackedRouteViewer(location);
					break;
				case PRECONDITION_MENU:
					if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
						log.info(Constants.ROUTE_CREATION + "Start route stacking at " + toString());
					}

					RouteEnd.startXRoute(location, selection);
					break;
				case TNT_MENU:
				case OOS_MENU:
					CATSOptionPane.showErrorMessage(location, "To Be Implemented", "Under Construction");
					//$FALL-THROUGH$
				default:
				}
			}
		}
	}

	/**
	 * is used to determine if a SecEdge is on a HomeSignal or not.
	 *
	 * @return true if it is and false if it is not.  The boundaries of
	 * ABS Blocks are always Control Points because ABS does not support
	 * route reservations.
	 */
	@Override
	public boolean allowsRoute() {
		return true;
	}

	/**
	 * Checks that a route can be created from this CPEdge to the next in advance
	 * by walking the track sequences, looking at entry edges for conditions that
	 * would block a route.  The walk does not change any turnouts.
	 * 
	 * Four cases trigger this code (all from "left click" in the RouteStateMachine).
	 * 1. left click on an idle signal.
	 * 2. a segment of an N/X Route.  This should be like a "left click".  The route
	 * DAG should have lined all the turnouts prior to getting here.
	 * 3. a stacked route verified that all turnouts were lined and everything looked
	 * good.
	 * 4. a left click on a stacked route (could be a Quick Route waiting for turnouts to
	 * feedback), which are a request to cancel the route.
	 * 
	 * In all cases, the state machine is in the IDLE state.
	 * @return true if there are no hazards and false if there are any
	 */
	private boolean checkRoute() {
		
//		EntryEdgeWalker walker = new EntryEdgeWalker();
//		EnumSet<GuiLocks> blockingLocks;
//		AbstractTrackEdge edge = walker.nextElement();
		AbstractTrackEdge edge = this;
		// DETECTIONLOCK is not checked in isRouteSegmentBlocked().
		if (EdgeLocks.contains(GuiLocks.DETECTIONLOCK) || CommonLocks.contains(GuiLocks.DETECTIONLOCK)) {
			if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
				log.info(Constants.ROUTE_CREATION + "Edge " + edge.toString() + " is blocked due to detection");
			}
			return false;
		}
		do {
			if (Crandic.Details.get(DebugBits.GUIROUTEBIT)) {
				System.out.println("Checking entry edge " + edge.toString() + " for safe route");
			}
//			blockingLocks = GuiLocks.intersection(edge.getCommonLocks(), GuiLocks.RouteBlocker);
//			if (!blockingLocks.isEmpty()) {
//				if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
//					System.out.println("Edge " + edge.toString() + " blocking due to common locks:" + edge.getCommonLocks());
//				}
//				return false;
//			}
//			blockingLocks = GuiLocks.intersection(edge.getEdgeLocks(), GuiLocks.RouteBlocker);
//			if (!blockingLocks.isEmpty()) {
//				if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
//					System.out.println("Edge " + edge.toString() + " blocking due to edge locks:" + edge.getEdgeLocks());
//				}
//				return false;
//			}
			if (edge.isRouteSegmentBlocked(GuiLocks.RouteBlocker)) {
				if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
					log.info(Constants.ROUTE_CREATION + "Edge " + toString() + " EdgeLocks=" + EdgeLocks.toString() +
							" CommonLocks=" + CommonLocks.toString());
				}
				return false;
			}
//			if (!walker.hasMoreElements()) {
//				break;
//			}
//			edge = walker.nextElement();
			edge = (AbstractTrackEdge) edge.NextRouteEdge;
			if (edge != null) {
				if ((edge instanceof PtsEdge) || (edge instanceof FrogEdge)) {
					edge = edge.NeighborEdge;
				}
			}
		} while ((edge != null) && !(edge instanceof CPEdge));
		return true;
	}
	
//	/**
//	 * moves the DAG for a route to the RouteStack on the RouteEdges.  This stacks
//	 * each RouteNode on its associated RouteEdge.  In addition, it expands the nodes
//	 * to create RouteNodes for all opposing CPEdges and stacks those, as well.
//	 */
//	private void finalizeRoute() {
//		AbstractTrackEdge edge = this;
//		RouteNode node = this.getNode();
//		RouteNode newNode;
//		do {
//			edge.stackNode(node);
//			if (node.getEntryFlag()) {
//				for (RouteEdge opposing : edge.OpposingSignalList) {
//					newNode = RouteNode.newNode(opposing, node.getRouteNumber(), null);
//					newNode.setEntryFlag(false);
//					newNode.setNodeStatus(node.getNodeStatus());
//					node.linkNode(newNode);
//					node = newNode;;
//				}
//			}
//			if ((node = node.getNextNode())== null) {
//				break;
//			}
//			edge = (AbstractTrackEdge) node.getRouteEdge();
//		} while (!edge.getEdgeType().equals(EDGE_TYPE.CP_EDGE));
//	}
	
//	/**
//	 * checks for a stacked route pending execution.  If there is one,
//	 * the route components are checked to see if there are no blocking locks
//	 * and that all points are lined.  If everything is ready, a stimulus is
//	 * queued to send a left mouse click to the CP edge
//	 */
//	private void checkStackedRoute() {
//		if (RouteMachine.isIdleState() && (RouteStack != null) && (RouteStack.getNodeStatus().equals(RouteNodeStatus.PENDING))) {
//			
//		}
//	}

	/**
	 * checks to see if the CPEdge state machine is in a state that allows
	 * a left mouse button push
	 * @return true if it is and false if it is not
	 */
	private boolean isLeftMouseAllowed( ) {
		return BUTTON_STATES[RouteMachine.getLabelStates()][LEFT_MOUSE].equals(ButtonValues.ENABLE);
	}
	
	/**************************************************************************
	 * the implementation of TrackEdge
	 **************************************************************************/
	
	@Override
	public BasicVitalLogic createVitalLogic() {
		MyVitalLogic = new CPVitalLogic(new EdgePacketFactory(this), toString(),
				MySignal.getLayoutSignal());
		return MyVitalLogic;
	}

	@Override
	public void discoverAdvanceVitalLogic() {
		super.discoverAdvanceVitalLogic();
		CallOnCapable = (OppositeEnd != null) && Icon.isLamp() && 
				((OppositeEnd instanceof OSEdge) || (OppositeEnd instanceof FrogEdge));
	}

	@Override
	public CodeMessage processFieldResponse(String response) {
		CodeMessage received = super.processFieldResponse(response);
		switch (received.Lock) {
		case ASPECT:
			updatePanelIcon(received.Value);
			break;

		case CALLONLOCK:
			RouteMachine.callOnResponse(received);
			break;
			//		case FLEET:
			//			FleetMachine.codeLineResponse(received);
			//			break;

		case SIGNALINDICATIONLOCK:
			RouteMachine.codeLineResponse(received);
			break;

		default:
		}
		return received;
	}

	/**
	 * determines what the GUI panel icon should look like.  The
	 * icon presentation gets complicated, depending upon the
	 * state of the VitalLogic.  For example, if it is CTC and there
	 * is no route through (either direction) the signal, it should be
	 * blank.  If the signal is Restricting, it could be painted different
	 * (e.g. half white).  If the request has been sent to change the signal,
	 * it could blink.
	 * @param indication is a String containing the index into
	 * AspectMap.IndicationType for the indication showing on the
	 * signal.
	 */
	private void updatePanelIcon(String indication) {
		MySignal.setAspectforRule(Integer.parseInt(indication));
	}
	/**************************************************************************
	 *  end of TrackEdge implementation
	 **************************************************************************/

	/**************************************************************************
	 *  RouteEdge specific methods for extended routes
	 **************************************************************************/

//	/**
//	 * receives the results of the second mouse selection
//	 * @param exitEdge is the results.  It could be null, indicating that the
//	 * eXtended Route operation should be cancelled
//	 */
//	private void secondMouseResults(final CPEdge exitEdge) {
//		if (exitEdge != null) {
//			JOptionPane.showMessageDialog((Component) null, "Clicked on Signal Icon",
//					"Success", JOptionPane.INFORMATION_MESSAGE);
//		}
//	}
	
	@Override
	public RouteNode createNode(final RouteNode nextNode, final int searchID) {
		if ((SearchNode == null) || (SearchNode.getRouteNumber() != searchID)) {
			SearchNode = RouteNode.newNode(this, searchID, nextNode);
			RouteSearchID = searchID;
		}
		return SearchNode;
	}

	/**
	 * creates an eXtended Route for immediate execution.  If a clear route
	 * can be found (including moving points), then the RouteNodes are
	 * tied to the RouteEdges on CPedges and PtsEdges.  The RouteNodes contain the kind of operation
	 * on each RouteEdge (clear or fleet).  The algorithm walks through the DAG,
	 * beginning at anchor for the "best" path to the end.  The "best" path is the one
	 * which has no blockage (occupied blocks and other locks) and the fewest number
	 * of turnouts to throw.
	 * 
	 * @param anchor is the root node of the DAG
	 */
	@SuppressWarnings("null")
	public void constructChain(final RouteNode anchor) {
		RouteEdge edge;
		RouteNode cpNode = anchor;
		RouteNodePts nextNode;
		if (Crandic.Details.get(DebugBits.ROUTENODEBIT)) {
			anchor.dumpDAG();
		}
		for (RouteNode node = anchor; node != null; node = node.getNextNode()) {
			edge = node.getRouteEdge();
//			edge.registerStackNode(false);
			//			if (((AbstractTrackEdge) edge).isStackEmpty()) {
			//				edge.stackNode(node);
			//				node.setControlNode(cpNode);
			node.setAnchorNode(anchor);
			node.setControlNode(cpNode);
			node.setNodeStatus(RouteNodeStatus.ACTIVE);
			if (edge instanceof CPEdge) {
				//				if (((CPEdge) edge).isLeftMouseAllowed() && ((CPEdge) edge).MyBlock.routestackEmpty()) {
				if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
					System.out.printf("%d Routing through CPEdge %s with blockages %d, diverging routes %d, fouling turnouts %d%n", 
						node.getRouteNumber() ,((CPEdge) edge).toString(), node.getBlockageCount(), node.getDivergingCount(), node.getComplexity());
				}
				cpNode.setControlNode(node);
				cpNode = node;
				cpNode.setControlNode(null);
			}
			else if (edge instanceof PtsEdge) {
				// facing points - this is where points get moved.  The metrics for deciding which
				// way to go must have been determined earlier for them to propagate to nodes
				// in approach
				nextNode = (RouteNodePts) node;
				if ((node = node.getNextNode()) != null) {
					node.setAnchorNode(anchor);
					node.setControlNode(cpNode);
					node.setNodeStatus(RouteNodeStatus.ACTIVE);
					((PtsEdge) edge).setNextTrak(((FrogEdge) node.getRouteEdge()).getIndex());
					if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
						System.out.printf("%d Routing through PtsEdge %s with blockages %d, diverging routes %d, fouling turnouts %d%n", 
								node.getRouteNumber() ,node.toString(), node.getBlockageCount(), node.getDivergingCount(), node.getComplexity());
						if (((FrogEdge) node.getRouteEdge()).isFoulingEdge()) {
							System.out.println("Scheduling " + ((PtsEdge)edge).toString() + " to move to " + ((FrogEdge) node.getRouteEdge()).getIndex());
						}
					}
				}
			}
			else if (edge instanceof FrogEdge) { // frogEdge
				if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) { 
					System.out.printf("%d Routing through FrogEdge %s %d%n", node.getRouteNumber(), edge.toString(),
							((FrogEdge) edge).getIndex());
				}
				nextNode = (RouteNodePts) node.getNextNode();
				nextNode.setAlignment(((FrogEdge) edge).getIndex());
				if (((FrogEdge) edge).isFoulingEdge()) {
					if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) { 
						System.out.println("Scheduling " + ((PtsEdge)nextNode.getRouteEdge()).toString() + " to move");
					}
					cpNode.setComplexity(cpNode.getComplexity() + 1);
				}
				//					nextNode.getRouteEdge().stackNode(nextNode);
				nextNode.setControlNode(cpNode);
				nextNode.setAnchorNode(anchor);
				nextNode.setNodeStatus(RouteNodeStatus.ACTIVE);
				node = nextNode;
			}
		}
	}
	
	/**
	 * Attempts to construct a chain from a DAG.  If successful, it walks that chain of
	 * CPNodes, creating a Quick Route for those that are ready and moving points on those
	 * that are not.
	 * @param anchor is the root node of the DAG
	 * @return true if a chain could be constructed and all the CPNodes committed.
	 */
	public boolean directRoute(final RouteNode anchor) {
		constructChain(anchor);
		for (RouteNode node = anchor; node != null; node = node.getControlNode()) {
			((CPEdge) (node.getRouteEdge())).ActiveRoute = node;
			if (node.countMislinedPoints() == 0) {
				((CPEdge) (node.getRouteEdge())).RouteMachine.startRoute();
			}
			else {
				node.setNodeStatus(RouteNodeStatus.PENDING);
				((CPEdge) (node.getRouteEdge())).DelaySwitch.startDelay();
			}
			if (node == node.getControlNode()) {
				break;
			}
		}
		return true;
	}
	
	/**
	 * Attempts to construct a chain from a DAG.  If successful, it walks that chain of
	 * CPNode, putting each on a timer to look for stability.
	 * 
	 * This algorithm is pretty weak and needs rework.  It first solidifies the chain,
	 * then checks for stability in the Blocks containing points.  It really should
	 * check check the stability of all Blocks on the chain and dissolve the chain
	 * if any Block is unstable.
	 * 
	 * @param anchor is the root node of the DAG
	 * @return true if a chain could be constructed and all the CPNodes committed
	 */
	public boolean deferredRoute(final RouteNode anchor) {
		constructChain(anchor);
		for (RouteNode node = anchor; node != null; node = node.getControlNode()) {
			((CPEdge) (node.getRouteEdge())).ActiveRoute = node;
			node.setNodeStatus(RouteNodeStatus.PENDING);
			((CPEdge) (node.getRouteEdge())).DelaySwitch.startDelay();
			if (node == node.getControlNode()) {
				break;
			}
		}
		return true;
	}
	
	/**
	 * steps through the route, telling each CPEdge to knock down the route
	 */
	private void cancelExtendedRoute() {
		for (RouteNode node = ActiveRoute; node != null; node = node.getControlNode()) {
			if (node.getRouteEdge() != null) {
				((CPEdge) (node.getRouteEdge())).RouteMachine.cancelRoute();
			}
		}
	}

	/**
	 * steps through the CPEdges in the route. telling each to either turn on or off
	 * fleeting.
	 * @param fleeting is true to turn on fleeting and false to turn off fleeting
	 */
	private void changeRouteFleeting(final boolean fleeting) {
		for (RouteNode node = ActiveRoute; node != null; node = node.getControlNode()) {
			if (node.getRouteEdge() != null) {
				((CPEdge) (node.getRouteEdge())).RouteMachine.fleetChange(fleeting);;
			}
		}		
	}
	
//	/**
//	 * since the CPEdge is the anchor for the route, it steps through the RouteNodes,
//	 * disconnecting them from the RouteEdge.  In addition, the oldest stacked
//	 * RouteNode becomes the ActiveRoute
//	 */
//	private void clearActiveRoute() {
//		RouteNode node = ActiveRoute;
//		do {
//			node.getRouteEdge().clearActiveRoute(node);
//			if ((node = node.getNextNode()) == null) {
//				break;
//			}
//		} while (!(node.getRouteEdge() instanceof CPEdge));
//		ActiveRoute.chainDisposal();
//	}

	/**************************************************************************
	 *  end of RouteEdge implementation
	 **************************************************************************/

//	/**
//	 * walks the tracks behind this CPEdge, up to the next CPEge (in direction of
//	 * travel), using current turnout alignments.
//	 * @return true if no blocking locks are found and false if any are found
//	 */
//	public boolean isRouteSafe() {
//		
//	}
	@Override
	public void resetLocks() {
		super.resetLocks();
		Icon.outOfCorrespondence(false);
	}

	@Override
	public void setHandlers() {
		//		super.setHandlers();
		//		DetectionIndication = new BlkDetectionDecorator(new DetectionLockIndicationHandler());
		//		switch (MyBlock.getDiscipline())
		//		{
		//		case DTC:
		//			WarrantIndication = new WarrantIndicationHandler();
		//			//$FALL-THROUGH$
		//		case CTC:
		//			TrafficIndication = new CPTrafficIndication();
		//			DetectionIndication = new CPDetectionDecorator(DetectionIndication);
		//			break;
		//			
		//		case APB:
		//			TrafficIndication = new CPTrafficIndication();
		//			DetectionIndication = new CPDetectionDecorator(DetectionIndication);
		//			break;
		//
		//		default:
		//		}
		//		FleetIndication = new FleetIndicationHandler();
		/**
		 * The GuiLocks are:
		 * DETECTIONLOCK
		 * CONFLICTINGSIGNALLOCK
		 * SWITCHUNLOCK
		 * APPROACHLOCK
		 * TIMELOCK
		 * TRAFFICLOCK
		 * ENTRYLOCK
		 * EXITLOCK
		 * LOCALCONTROLLOCK
		 * CALLONLOCK
		 * WARRANTLOCK
		 * FLEET
		 * CROSSING
		 * PREPARATION
		 * EXTERNALLOCK
		 **/

		switch (MyBlock.getDiscipline())
		{
		case DTC:
			WarrantIndication = new WarrantIndicationHandler();
			//$FALL-THROUGH$
		case CTC:
			//			DetectionIndication = new DetectionDecorator(
			DetectionIndication = new BlkDetectionDecorator(
					new CPDetectionDecorator(new RouteCheckerDecorator(
							new BasicLockIndicationHandler(GuiLocks.DETECTIONLOCK))));
			ConflictingSignalIndication = new MergeDecorator(
					new GeneralCPDecorator(
							new BasicLockIndicationHandler(GuiLocks.CONFLICTINGSIGNALLOCK)));
			//			ConflictingSignalIndication = new CommonLocksDecorator(
			//					new BasicLockIndicationHandler(GuiLocks.CONFLICTINGSIGNALLOCK));
			//			SwitchUnlockIndication = new CommonLocksDecorator(
			//					new GeneralCPDecorator(
			//							new BasicLockIndicationHandler(GuiLocks.SWITCHUNLOCK)));
			SwitchUnlockIndication = new RouteCheckerDecorator(
					new CommonLocksDecorator(
							new BasicLockIndicationHandler(GuiLocks.SWITCHUNLOCK)));
			ApproachIndication = new EdgeLocksDecorator(
					new ApproachIndicationHandler());
			TimeIndication = new RouteCheckerDecorator(
					new EdgeLocksDecorator(
							new BasicLockIndicationHandler(GuiLocks.TIMELOCK)));
			TrafficIndication = new EdgeLocksDecorator(
					new CPTrafficIndication());
			EntryTrafficIndication = new RouteCheckerDecorator(
					new EdgeLocksDecorator(
							new BlockEntryDecorator(
									new BasicLockIndicationHandler(GuiLocks.ENTRYLOCK))));
			ExitTrafficIndication = new MergeDecorator(
					new RouteCheckerDecorator(
							new BlockExitDecorator(
									new BasicLockIndicationHandler(GuiLocks.EXITLOCK))));
			//			LocalControlLock = new CommonLocksDecorator(
			//					new GeneralCPDecorator(
			//							new BasicLockIndicationHandler(GuiLocks.LOCALCONTROLLOCK)));
			LocalControlLock = new RouteCheckerDecorator(
					new CommonLocksDecorator(
							new BasicLockIndicationHandler(GuiLocks.LOCALCONTROLLOCK)));
			CallOnIndication = (OppositeEnd == null) ? new HalfCallOnIndication() : new CallOnIndication();
			CallOnIndication = new EdgeLocksDecorator(
					new RouteCheckerDecorator(
							CallOnIndication));
			FleetIndication = new EdgeLocksDecorator(
					new RouteCheckerDecorator(
							new BasicLockIndicationHandler(GuiLocks.FLEET)));
			// CROSSINGLOCK should never be set or cleared
			PreparationIndication = new ProtectionZoneDecorator(
					new BasicLockIndicationHandler(GuiLocks.PREPARATION));
			ExternalIndication = new MergeDecorator(
					new RouteCheckerDecorator(
							new GeneralCPDecorator(
									new BasicLockIndicationHandler(GuiLocks.EXTERNALLOCK))));
			break;

		case APB:
		case ABS:
		case UNDEFINED:
			DetectionIndication = new BlkDetectionDecorator(
					new MergeDecorator(
							new BasicLockIndicationHandler(GuiLocks.DETECTIONLOCK)));
			ConflictingSignalIndication = new CommonLocksDecorator(
					new BasicLockIndicationHandler(GuiLocks.CONFLICTINGSIGNALLOCK));
			SwitchUnlockIndication = new CommonLocksDecorator(
					new BasicLockIndicationHandler(GuiLocks.SWITCHUNLOCK));
			ApproachIndication = new EdgeLocksDecorator(
					new ApproachIndicationHandler());
			// TIMELOCK should never be set or cleared
			// TRAFFICLOCK should never be set or cleared
			// ENTRYLOCK should never be set or cleared
			// EXITLOCK should never be set or cleared
			LocalControlLock = new CommonLocksDecorator(
					new BasicLockIndicationHandler(GuiLocks.LOCALCONTROLLOCK));
			// CALLONLOCK should never be set or cleared
			// FLEETLOCK should never be set or cleared
			// CROSSINGLOCK should never be set or cleared
			// Preparation should never be set or cleared
			ExternalIndication = new MergeDecorator(
					new BasicLockIndicationHandler(GuiLocks.EXTERNALLOCK));
			break;
		default:
		}
	}

	/**************************************************************************
	 *  lock indication handlers
	 **************************************************************************/

	/**************************************************************************
	 * the Detection handler for a control point.  It clears a route when FLEET
	 * is not set and the last occupied block reports unoccupied.
	 **************************************************************************/
	protected class CPDetectionDecorator extends AbstractHandlerDecorator {
		public CPDetectionDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {
			//			if (!EdgeLocks.contains(GuiLocks.FLEET) && !CommonLocks.contains(GuiLocks.DETECTIONLOCK)) {
			//				RouteMachine.updateVitalLogic(false);
			//			}
			Handler.requestLockSet();
			RouteMachine.detectionChange(true);
		}

		@Override
		public void requestLockClear() {
			//			if (!EdgeLocks.contains(GuiLocks.FLEET)) {
			////				if (!CommonLocks.contains(GuiLocks.DETECTIONLOCK)) {
			////					CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.SIGNALINDICATIONLOCK, Constants.FALSE);
			////				}
			//				clearEdgeLock(GuiLocks.ENTRYLOCK);
			//			}
			Handler.requestLockClear();
			if (!EdgeLocks.contains(GuiLocks.FLEET)) {
				clearEdgeLock(GuiLocks.ENTRYLOCK);
			}
			clearEdgeLock(GuiLocks.EXITLOCK);
			RouteMachine.detectionChange(false);
		}

		@Override
		public void advanceLockSet() {
			//			if (!EdgeLocks.contains(GuiLocks.FLEET) && !EdgeLocks.contains(GuiLocks.DETECTIONLOCK)) {
			//				RouteMachine.updateVitalLogic(false);
			//			}
			Handler.advanceLockSet();
			RouteMachine.detectionChange(true);
		}

		@Override
		public void advanceLockClear() {
			//			if (!EdgeLocks.contains(GuiLocks.FLEET) && !EdgeLocks.contains(GuiLocks.DETECTIONLOCK)) {
			////				CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.SIGNALINDICATIONLOCK, Constants.FALSE);
			//				RouteMachine.cancelEvent();
			//			}
			Handler.advanceLockClear();
			RouteMachine.detectionChange(false);
		}
	}
	/**************************************************************************
	 *  end of CPDetectionDecorator
	 **************************************************************************/

	/**************************************************************************
	 * the Traffic handler for a control point.  It walks the route, setting/clearing
	 * entry and exit locks on all nodes and paints the track green. 
	 **************************************************************************/
	protected class CPTrafficIndication extends DefaultLockIndicationHandler {
		public CPTrafficIndication() {
			super(GuiLocks.TRAFFICLOCK);
		}

		@Override
		public void requestLockSet() {
			Icon.holdSignal(false);
			EdgeLocks.add(GuiLocks.TRAFFICLOCK);
			setEdgeLock(GuiLocks.ENTRYLOCK);
			//			TheRoute.reserveTrackEnds();
		}

		@Override
		public void requestLockClear() {
			Icon.holdSignal(DispatcherControlled);
			EdgeLocks.remove(GuiLocks.TRAFFICLOCK);
			if (!EdgeLocks.contains(GuiLocks.DETECTIONLOCK)) {
				clearEdgeLock(GuiLocks.ENTRYLOCK);
			}
			//			TheRoute.unreserveTrackEnds();
		}
	}
	/**************************************************************************
	 *  end of CPTrafficIndication
	 **************************************************************************/

	/**************************************************************************
	 * the Call-on handler for a control point.  It adjusts the icon to reflect
	 * Call-on and locks the opposite end of the track.
	 **************************************************************************/
	protected class CallOnIndication extends BasicLockIndicationHandler {
		public CallOnIndication() {
			super(GuiLocks.CALLONLOCK);
		}

		@Override
		public void requestLockSet() {
			Icon.holdSignal(false);
			OppositeEnd.setCommonLock(GuiLocks.CALLONLOCK);
			EdgeLocks.add(GuiLocks.CALLONLOCK);
		}

		@Override
		public void requestLockClear() {
			Icon.holdSignal(true);
			OppositeEnd.clearCommonLock(GuiLocks.CALLONLOCK);
			EdgeLocks.remove(GuiLocks.CALLONLOCK);
		}
	}
	/**************************************************************************
	 *  end of CallOnIndication
	 **************************************************************************/

	/**************************************************************************
	 * the half Call-on handler for a control point.  It only adjusts the icon to reflect
	 * Call-on.
	 **************************************************************************/
	protected class HalfCallOnIndication extends BasicLockIndicationHandler {
		public HalfCallOnIndication() {
			super(GuiLocks.CALLONLOCK);
		}

		@Override
		public void requestLockSet() {
			Icon.holdSignal(false);
			EdgeLocks.add(GuiLocks.CALLONLOCK);
		}

		@Override
		public void requestLockClear() {
			Icon.holdSignal(true);
			EdgeLocks.remove(GuiLocks.CALLONLOCK);
		}
	}
	/**************************************************************************
	 *  end of HalfCallOnIndication
	 **************************************************************************/

	/**************************************************************************
	 * Warrant lock handler for a DTC control point
	 **************************************************************************/
	protected class WarrantIndicationHandler extends DefaultLockIndicationHandler {

		public WarrantIndicationHandler() {
			super(GuiLocks.WARRANTLOCK);
		}

		@Override
		public void requestLockSet() {
			EdgeLocks.add(GuiLocks.WARRANTLOCK);
			paintTrackRoute(RouteDecoration.SET_DTC_ROUTE);
		}

		@Override
		public void requestLockClear() {
			EdgeLocks.remove(GuiLocks.WARRANTLOCK);
			paintTrackRoute(RouteDecoration.CLEAR_DTC_ROUTE);
		}
	}
	/**************************************************************************
	 *  end of WarrantIndicationHandler
	 **************************************************************************/

	//	/*************************************************************************
	//	 * Fleeting lock for a CPEdge.  The EdgeLocks lock vector is used on the initiating
	//	 * ("entry") end of a potential route.  The CommonLocks lock vector is used on the "exit"
	//	 * end.
	//	 * <p>
	//	 * Fleeting can be set on any CPEdge at any time - no safety checking is performed.
	//	 * It comes into affect only after a route is created, at which time the safety check
	//	 * on the route is made.  This means that there are two cases that the lock is distributed
	//	 * over the route - when fleeting is set and the route is already established and when the
	//	 * route is created and fleet is set.
	//	 * <p>
	//	 * Similarly, there are two cases when the fleet lock is turned off on the nodes in a route -
	//	 * when fleet is cleared and a route is active and when the created is cancelled by the
	//	 * dispatcher and fleet is set
	//	 */
	//	private class FleetIndicationHandler extends DefaultLockIndicationHandler {
	//
	//		public FleetIndicationHandler() {
	//			super(GuiLocks.FLEET);
	//		}
	//
	//		@Override
	//		public void requestLockSet() {
	//			EdgeLocks.add(GuiLocks.FLEET);
	////			TheRoute.reserveTrackEnds();
	////			TheRoute.setFleet(true);
	//		}
	//
	//		@Override
	//		public void requestLockClear() {
	//			EdgeLocks.remove(GuiLocks.FLEET);
	////			TheRoute.setFleet(false);
	//		}
	//		
	//		@Override
	//		public void advanceLockSet() {
	//			CommonLocks.add(GuiLocks.FLEET);
	//		}
	//		
	//		@Override
	//		public void advanceLockClear() {
	//			CommonLocks.remove(GuiLocks.FLEET);
	//		}
	//	}
	//	/**************************************************************************
	//	 *  end of FleetIndicationHandler
	//	 **************************************************************************/
	/*************************************************************************
	 * GeneralCPDecorator - a lock decorator that alerts the CPEdge when any
	 * lock is set in any BlkEdge in the Route Zone.
	 *************************************************************************/
	private class GeneralCPDecorator extends AbstractHandlerDecorator {

		public GeneralCPDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {
			Handler.requestLockSet();
			RouteMachine.lockChange(true);
		}

		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
			RouteMachine.lockChange(false);
		}

		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
			RouteMachine.lockChange(true);
		}

		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
			RouteMachine.lockChange(false);
		}
	}
	/**************************************************************************
	 *  end of GeneralCPDecorator
	 **************************************************************************/

//	/*************************************************************************
//	 * RouteStackDecorator - a lock decorator that alerts the CPEdge when any
//	 * lock that could block an eXtendedRoute is cleared.  The CPEdge then checks
//	 * if a stacked route is pending and could be activated. If so, the route
//	 * will be requested, as though the left mouse button was pushed.  This
//	 * decorator should be applied to any lock that can hold a route in the
//	 * stack.
//	 *************************************************************************/
//	private class RouteStackDecorator extends AbstractHandlerDecorator {
//
//		public RouteStackDecorator(LockIndicationHandler handler) {
//			super(handler);
//		}
//
//		@Override
//		public void requestLockSet() {
//			Handler.requestLockSet();
//		}
//
//		@Override
//		public void requestLockClear() {
//			Handler.requestLockClear();
//			checkStackedRoute();
//		}
//
//		@Override
//		public void advanceLockSet() {
//			Handler.advanceLockSet();
//		}
//
//		@Override
//		public void advanceLockClear() {
//			Handler.advanceLockClear();
//			checkStackedRoute();
//		}
//	}
	/**************************************************************************
	 *  end of RouteStackDecorator
	 **************************************************************************/
	/**************************************************************************
	 *  end of lock indication handlers
	 **************************************************************************/
	/***************************************************************************************
	 *        The following routines are for automated testing
	 ***************************************************************************************/
	/**
	 * creates a test case for when the dispatcher left mouse clicks on a signal icon
	 * @param comment is the expected action
	 */
	private void recordTestCase(String comment) {
		if (TestMaker.isRecording()) {
			Element testCase = MySection.createTrigger();
			testCase.setAttribute(SecEdge.EDGE, String.valueOf(MyEdge));
			//			testCase.setAttribute("State", State.toString());
			//			testCase.setAttribute("Event", Event.toString());
			testCase.addContent(new Element(Section.Button1));
			testCase.setAttribute(TestMaker.DELAY, "");
			TestMaker.recordTrigger(testCase, comment);
		}
	}

	/**
	 * creates a test case for a mouse event that generates a pop-up.
	 * @param comment describes the menu selection
	 * @param where is where the pop-up should be positioned
	 */
	private void recordTestCase(String comment, Point where) {
		if (TestMaker.isRecording()) {
			Element testCase = MySection.createTrigger();
			Element details = new Element(Section.Button3);
			testCase.setAttribute(SecEdge.EDGE, String.valueOf(MyEdge));
			details.setAttribute(Section.X_TAG, String.valueOf(where.x));
			details.setAttribute(Section.Y_TAG, String.valueOf(where.y));
			testCase.addContent(details);
			TestMaker.recordTrigger(testCase, comment);  
		}
	}

	/**
	 * handles replaying a trigger event.  This does nothing for any SecEdge
	 * except a CPEdge or a PtsEdge.  A CPEdge constructs a MouseEvent from the 
	 * Element and injects it into the code.
	 * 
	 * @param element is the XML Element that defines the trigger event
	 * @return an error message if the trigger cannot be replayed
	 */
	@Override
	public String replayEvent(Element element) {
		int x;
		int y;
		MouseEvent me;
		JPanel dummy = new JPanel();  // this is needed to prevent the MouseEvent ctor from crashing
		if (element.getName().equals(Section.Button1)) {
			me = new MouseEvent(dummy, 0, 0, MouseEvent.BUTTON1_MASK, 0, 0, 1, false);
		}
		else if (element.getName().equals(Section.Button3)) {
			x = Integer.parseInt(element.getAttributeValue(Section.X_TAG));
			y = Integer.parseInt(element.getAttributeValue(Section.Y_TAG));
			me = new MouseEvent(dummy, 0, 0, MouseEvent.BUTTON3_MASK, x, y, 1, false);      
		}
		else {
			return super.replayEvent(element);
		}
		edgeMouse(me);
		return null;
	}

	//	/**********************************************************************************************************
	//	 * Edge state machines
	//	 **********************************************************************************************************/
	//	private abstract class AbstractStateMachine {
	//		/**
	//		 * the current state
	//		 */
	//		protected SigValues EdgeState = SigValues.NO_ACTION;
	//		
	////		/**
	////		 * retrieve the value of the current state
	////		 * @return the current state
	////		 */
	////		public SigValues getState() {
	////			return EdgeState;
	////		}
	//		
	//		/**
	//		 * requests that the event be created
	//		 * @return true if the event can be created and false if not.
	//		 * Note that returning true may not mean that the event is active.
	//		 * Some communication with the Vital Logic may be needed for
	//		 * that to happen.
	//		 */
	//		abstract public boolean createEvent();
	//
	//		/**
	//		 * requests that the event be cancelled
	//		 * @return true if it can be cancelled and false if not
	//		 */
	//		abstract public boolean cancelEvent();
	//		
	//		/**
	//		 * take input from the layout
	//		 * @param response is the message received on the code line
	//		 */
	//		abstract public void codeLineResponse(CodeMessage response);
	//	}

	/*******************************************************************************************************
	 * left mouse click (route creation/destruction) state machine
	 *******************************************************************************************************/
	private class RouteStateMachine {

		//		/**
		//		 * is the state of the route in the CPVitalLogic
		//		 */
		//		private boolean FieldRoute = false;
		//		
		//		@Override
		//		public boolean createEvent() {
		//			switch (EdgeState) {
		//			case NO_ACTION:
		//				if (checkRoute()) {
		//					TheRoute.reserveExitEnds();  // to prevent an opposing route while in in a transition state
		//					Icon.outOfCorrespondence(true);
		//					updateVitalLogic(true);
		//					EdgeState = SigValues.PENDING;	// and wait for a response from teh VitalLogic
		//				}
		//				else {
		//					JOptionPane.showMessageDialog((Component) null, "Route cannot be created due to conflicts",
		//							"Denied Action", JOptionPane.INFORMATION_MESSAGE);
		//					return false;
		//				}
		//				break;
		//			case CLEARING:
		//				updateVitalLogic(true);
		//				EdgeState = SigValues.PENDING;
		//				break;
		//			default:
		//			}
		//			return true;
		//		}
		//
		//		@Override
		//		public boolean cancelEvent() {
		//			switch(EdgeState) {
		//			case ACTIVE:
		//				Icon.outOfCorrespondence(DispatcherControlled);
		//				//$FALL-THROUGH$
		//			case PENDING:
		//				if ((FleetMachine.getState() == SigValues.ACTIVE)  || FleetMachine.getState() == SigValues.PENDING) {
		//					FleetMachine.cancelEvent();
		//				}
		//				updateVitalLogic(false);
		//				EdgeState = SigValues.CLEARING;
		//				break;
		//			default:
		//			}
		//			return true;
		//		}
		//
		//		@Override
		//		public void codeLineResponse(CodeMessage response) {
		//			if (response.Lock.equals(Codes.SIGNALINDICATIONLOCK)) {
		//				if (response.Purpose.equals(CodePurpose.INDICATION)) {
		//					if (Boolean.valueOf(response.Value)) {
		//						if (EdgeState.equals(SigValues.PENDING)) {  // this is route confirmation from the field
		//							TrafficIndication.requestLockSet();
		//							if (MyBlock.getDiscipline().equals(Discipline.DTC)) {
		//								WarrantIndication.requestLockSet();
		//							}
		//							EdgeState = SigValues.ACTIVE;
		//							Icon.outOfCorrespondence(false);
		//							Icon.makeDark(false);
		//						}
		//						// else it can be ignored
		//					}
		//					else {
		//						FieldRoute = false;
		//						if (EdgeState.equals(SigValues.CLEARING)) {	// this is confirmation from the field
		//							// the next conditional distinguishes between a dispatcher
		//							// initiated cancel and an asynchronous termination due to
		//							// no more occupancy
		//							if (MyBlock.getDiscipline().equals(Discipline.DTC)) {
		//								WarrantIndication.requestLockClear();
		//							}
		//							TrafficIndication.requestLockClear();
		//							TheRoute = null;
		//							EdgeState = SigValues.NO_ACTION;
		//							Icon.makeDark(DispatcherControlled);
		//						}
		//						else if (EdgeState.equals(SigValues.ACTIVE)) {  // renew route when fleeting
		//							if (EdgeLocks.contains(GuiLocks.FLEET)) {
		//								updateVitalLogic(true);
		//							}
		//						}
		//					}
		//				}
		//			}
		////			else if (response.Purpose.equals(CodePurpose.DENY)) {
		////				if (EdgeState == SigValues.PENDING) {
		////					EdgeState = SigValues.NO_ACTION;
		////				}
		////				JOptionPane.showMessageDialog((Component) null, "Request for " + response.Lock + " Denied",
		////						"Denied Action", JOptionPane.INFORMATION_MESSAGE);
		////				Icon.outOfCorrespondence(false);
		////			}
		//		}
		//		
		//		/**
		//		 * this method keeps the associated Vital Logic up to date, so it can set the signals appropriately
		//		 * @param state is true if there is a route through the field equipment and false if not
		//		 */
		//		public void updateVitalLogic(boolean state) {
		//			if (FieldRoute != state) {
		//				CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.SIGNALINDICATIONLOCK, String.valueOf(state));
		//			}
		//			FieldRoute = state;
		//		}

		/**
		 * the current state
		 */
		private EdgeStates State = EdgeStates.IDLE;

		/**
		 * this is a safety net, resetting the Route state machine
		 */
		public void resetRoute() {
			if (EdgeLocks.contains(GuiLocks.TRAFFICLOCK) && (MyLogic != null)) {
				MyLogic.clearLocalLock(LogicLocks.TRAFFICLOCK);
			}
			State = EdgeStates.IDLE;
			CPEdge.this.ActiveRoute = null;
			TrafficIndication.requestLockClear();
			FleetIndication.requestLockClear();
			CallOnIndication.requestLockClear();
			Icon.outOfCorrespondence(false);
			CPEdge.this.entryClearRoute();
			killRoute();
			if (Crandic.Details.get(DebugBits.GUIROUTEBIT)) {
				System.out.println(CPEdge.this.toString() + ": Control Point reset");
			}
		}

		/**
		 * process a Left Mouse click
		 * @param location is the mouse location on the panel
		 */
		public void leftClick(final Point location) {
			EdgeStates oldState = State;
			switch (State) {
			case IDLE:
				if (checkRoute()) {
//					ActiveRoute = RouteCreator.getRouteFactory().quickRoute(CPEdge.this);
					startRoute();
				}
				else {
					CATSOptionPane.showErrorMessage(location, "A route cannot be created due to conflicts.", "Route Denied");
				}
				break;
			case PREPARATION:
				clearEdgeLock(GuiLocks.PREPARATION);
				//$FALL-THROUGH$
			case ACTIVE:
				cancelRoute();
				break;
			case ROLL_UP:
				if (!isRouteSegmentBlocked(GuiLocks.RouteBlocker)) {
					CATSOptionPane.showErrorMessage(location, "A route cannot be created due to conflicts.", "Route Denied");
				}
				else {
					CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.SIGNALINDICATIONLOCK, Constants.TRUE);
					Icon.outOfCorrespondence(true);
					//				TheRoute.reserveTrackEnds();
					//					setEdgeLock(GuiLocks.TRAFFICLOCK);
					State = EdgeStates.PENDING;
				}
				break;
			case FLEET:
				CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.SIGNALINDICATIONLOCK, Constants.FALSE);
				Icon.outOfCorrespondence(true);
				FleetIndication.requestLockClear();
				State =  (EdgeLocks.contains(GuiLocks.DETECTIONLOCK) || CommonLocks.contains(GuiLocks.DETECTIONLOCK)) ?
						EdgeStates.ROLL_UP : EdgeStates.CLEARING;
				break;
			case CALLON:
				CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.CALLONLOCK, Constants.FALSE);
				Icon.outOfCorrespondence(true);
				State = EdgeStates.CLEARING;
				break;
			case CLEARING:
			case PENDING:
			}
			if (Crandic.Details.get(DebugBits.GUIROUTEBIT) && !oldState.equals(State)) {
				System.out.println(CPEdge.this.toString() + ": Left Mouse click in Route State machine. old="
						+ oldState + " new=" + State);
			}
		}

		/**
		 * process a change in the fleet setting
		 * @param change is true if the Fleet menu selection was made and false if not
		 */
		public void fleetChange(boolean change) {
			EdgeStates oldState = State;
			switch (State) {
			case ACTIVE:
				if (change) {
					FleetIndication.requestLockSet();
					State = EdgeStates.FLEET;
				}
				break;
			case ROLL_UP:
				if (change) {
					CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.SIGNALINDICATIONLOCK, Constants.TRUE);
					Icon.outOfCorrespondence(true);
					FleetIndication.requestLockSet();
					//					TheRoute.reserveTrackEnds();
					setEdgeLock(GuiLocks.TRAFFICLOCK);
					State = EdgeStates.FLEET;
				}
				break;
			case FLEET:
				if (!change) {
					CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.SIGNALINDICATIONLOCK, Constants.FALSE);
					Icon.outOfCorrespondence(true);
					FleetIndication.requestLockClear();
					State =  (EdgeLocks.contains(GuiLocks.DETECTIONLOCK) || CommonLocks.contains(GuiLocks.DETECTIONLOCK)) ?
							EdgeStates.ROLL_UP : EdgeStates.ACTIVE;
				}
				break;
			case IDLE:
			case PENDING:
			case CLEARING:
			case CALLON:
			case PREPARATION:
			}
			if (Crandic.Details.get(DebugBits.GUIROUTEBIT)  && !oldState.equals(State)) {
				System.out.println(CPEdge.this.toString() + ": Fleet change in Route State machine. old="
						+ oldState + " new=" + State);
			}
		}

		/**
		 * process a change in the call on setting
		 * @param change is true if the Call On menu selection was made and false if not
		 */
		public void callOnChange(boolean change) {
			EdgeStates oldState = State;
			switch (State) {
			case IDLE:
				if (change && !EdgeLocks.contains(GuiLocks.DETECTIONLOCK)) {
					CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.CALLONLOCK, Constants.TRUE);
					Icon.outOfCorrespondence(true);
					State = EdgeStates.PENDING;	
//					RouteStack = RouteCreator.getRouteFactory().callonRoute(CPEdge.this);
//					if (Crandic.Details.get(DebugBits.ROUTENODEBIT)) {
//						RouteStack.dumpDAG();
//					}
				}
				break;
			case CALLON:
				if (!change) {
					CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.CALLONLOCK, Constants.FALSE);
					Icon.outOfCorrespondence(true);
					State = EdgeStates.CLEARING;					
				}
				break;
			case ACTIVE:
			case ROLL_UP:
			case FLEET:
			case PENDING:
			case CLEARING:
			case PREPARATION:
			}
			if (Crandic.Details.get(DebugBits.GUIROUTEBIT)  && !oldState.equals(State)) {
				System.out.println(CPEdge.this.toString() + ": Call On change in Route State machine. old="
						+ oldState + " new=" + State);
			}
		}

		/**
		 * process a detection lock change
		 * @param change is true if route detection turned on and false if lost
		 */
		public void detectionChange(boolean change) {
			EdgeStates oldState = State;
			switch (State) {
			case ROLL_UP:
				if (!change) {
					if (!EdgeLocks.contains(GuiLocks.DETECTIONLOCK) && !(CommonLocks.contains(GuiLocks.DETECTIONLOCK))) {
						killRoute();
					}
				}
				break;
			case ACTIVE:
				if (!change) {
					if (!EdgeLocks.contains(GuiLocks.DETECTIONLOCK) && !(CommonLocks.contains(GuiLocks.DETECTIONLOCK))) {
						State = EdgeStates.ROLL_UP;
					}					
				}
				break;
			case FLEET:
				if (!change) {
					if (!EdgeLocks.contains(GuiLocks.DETECTIONLOCK) && !(CommonLocks.contains(GuiLocks.DETECTIONLOCK))) {
						CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.SIGNALINDICATIONLOCK, Constants.TRUE);
					}					
				}
				break;
			case CALLON:
			case IDLE:
			case PENDING:
			case CLEARING:
			case PREPARATION:
			}
			if (Crandic.Details.get(DebugBits.GUIROUTEBIT)  && !oldState.equals(State)) {
				System.out.println(CPEdge.this.toString() + ": Detection change in Route State machine. old="
						+ oldState + " new=" + State);
			}
		}

		/**
		 * process a non-detection lock change
		 * @param change is true if any route non-detection turned on and no lock is on
		 */
		public void lockChange(boolean change) {
			EdgeStates oldState = State;
			switch (State) {
			case PENDING:
			case ACTIVE:
				if (change) {
					CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.SIGNALINDICATIONLOCK, Constants.FALSE);
					State = EdgeStates.CLEARING;
				}
				break;
			case ROLL_UP:
				if (change) {
					EntryTrafficIndication.requestLockClear();
					routeWrapUp();
				}
				break;
			case FLEET:
				if (change) {
					FleetIndication.requestLockClear();
					CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.SIGNALINDICATIONLOCK, Constants.FALSE);
					State = EdgeStates.CLEARING;
				}
				break;
			case IDLE:
			case CLEARING:
			case CALLON:
			case PREPARATION:
			}
			if (Crandic.Details.get(DebugBits.GUIROUTEBIT)  && !oldState.equals(State)) {
				System.out.println(CPEdge.this.toString() + ": Lock changes in Route State machine. old="
						+ oldState + " new=" + State);
			}
		}

		/**
		 * process a SignalIndication response from the Vital Logic
		 * @param response is the packet from the code line
		 */
		public void codeLineResponse(CodeMessage response) {
			boolean trafficState;
			EdgeStates oldState = State;
			if (response.Purpose.equals(CodePurpose.INDICATION)) {
				Icon.outOfCorrespondence(false);
				trafficState = Boolean.valueOf(response.Value);
				switch (State) {
				case IDLE:
				case CALLON:
					break;
				case PENDING:
					if (trafficState) {
						//						TheRoute.reserveTrackEnds();
						//						TrafficIndication.requestLockSet();
						//						Icon.makeDark(false);
						Icon.holdSignal(false);
						if (MyBlock.getDiscipline().equals(Discipline.DTC)) {
							WarrantIndication.requestLockSet();
						}
						State = (EdgeLocks.contains(GuiLocks.DETECTIONLOCK)) ? EdgeStates.ROLL_UP :
							EdgeStates.ACTIVE;
					}
					//					else {
					//						killRoute();
					//						State = EdgeStates.IDLE;						
					//					}
					break;
				case ACTIVE:
					if (!trafficState) {
						State = EdgeStates.ROLL_UP;
					}
					break;
				case ROLL_UP:
					if (trafficState) {
						State = EdgeStates.ACTIVE;
					}
					else {
						killRoute();
					}
					break;
				case CLEARING:
					if (MyBlock.getDiscipline().equals(Discipline.DTC)) {
						WarrantIndication.requestLockClear();
					}
					//					TrafficIndication.requestLockClear();
					//					Icon.makeDark(DispatcherControlled);
					if (DispatcherControlled) {
						Icon.holdSignal(true);
					}
					killRoute();
					break;
					
				case FLEET:
				case PREPARATION:
				}
			}
			if (Crandic.Details.get(DebugBits.GUIROUTEBIT)  && !oldState.equals(State)) {
				System.out.println(CPEdge.this.toString() + ": Vital logic signal indication response in Route State machine. old="
						+ oldState + " new=" + State);
			}
		}

		/**
		 * process a CallOn response from the Vital Logic
		 * @param response is the packet from the code line
		 */
		public void callOnResponse(CodeMessage response) {
			boolean trafficState;
			EdgeStates oldState = State;
			if (response.Purpose.equals(CodePurpose.INDICATION)) {
				Icon.outOfCorrespondence(false);
				trafficState = Boolean.valueOf(response.Value);
				switch (State) {
				case PENDING:
					if (trafficState) {
						//						TheRoute.reserveTrackEnds();
						CallOnIndication.requestLockSet();
						State = EdgeStates.CALLON;
					}
					break;
				case CALLON:
				case CLEARING:
					if (!trafficState) {
						CallOnIndication.requestLockClear();
						routeWrapUp();
					}
					break;
				case IDLE:
				case ACTIVE:
				case ROLL_UP:
				case FLEET:
				case PREPARATION:
					break;
				}
			}
			if (Crandic.Details.get(DebugBits.GUIROUTEBIT)  && !oldState.equals(State)) {
				System.out.println(CPEdge.this.toString() + ": Vital logic call on response in Route State machine. old="
						+ oldState + " new=" + State);
			}
		}

		/**
		 * based on the current state and locks, determines what button items are
		 * enabled, disabled, or can be cancelled
		 * 
		 * @return an index into the array of menu button label states
		 * 0: unknown state or Active, Roll-Up, or Preparation without an active route (N/X)
		 * 1: Idle, advance block is occupied, local is not
		 * 2: Idle, CommonLocks contains a lock other than Detection
		 * 3: Idle, attached block is occupied
		 * 4: Fleet active, no N/X
		 * 5: Call-on active
		 * 6: Signal is Pending or Clearing (in transition)
		 * 7: Active, Roll-Up, or Preparation with an active route (N/X)
		 * 8: Fleet active, N/X
		 */
		public int getLabelStates() {
			switch (State) {
			case IDLE:
				if (!GuiLocks.intersection(CommonLocks, GuiLocks.ObstructedBlock).isEmpty()) {
					return 3;
				}
				if (CommonLocks.contains(GuiLocks.DETECTIONLOCK)) {
					return (EdgeLocks.contains(GuiLocks.DETECTIONLOCK)) ? 3 : 1;
				}
				return 2;
			case PENDING:
			case CLEARING:
				return 6;
			case ACTIVE:
			case ROLL_UP:
			case PREPARATION:
				return (ActiveRoute == null) ? 0 : 7;
			case FLEET:
				return (ActiveRoute == null) ? 4 : 8;
			case CALLON:
				return 5;
			default:
			}
			return 0;
		}

		/**
		 * startRoute() is invoked to begin the process of creating a route, originating on this
		 * CP.  Because it does no state checking, it should be called after the caller has
		 * checked the state for validity.  It is called due to
		 * - left clicking on an IDLE signal
		 * - as one stage of an eXtended route
		 * - after the points are lined in an eXtended route
		 * - after the points are lined during pre-conditioning
		 */
		public void startRoute() {
			if (EdgeLocks.contains(GuiLocks.WARRANTLOCK)) {
				WarrantIndication.requestLockClear();
			}
			else {
				CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.SIGNALINDICATIONLOCK, Constants.TRUE);
				Icon.outOfCorrespondence(true);
				State = EdgeStates.PENDING;
			}			
		}

		/**
		 * cancelRoute() is invoked to cancel an active route (one not PENDING or CLEARING), originating
		 * on this CP.  It clears up to the next CP in advance.
		 */
		public void cancelRoute() {
			CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.SIGNALINDICATIONLOCK, Constants.FALSE);
			DelaySwitch.cancelTimer();
			if (EdgeLocks.contains(GuiLocks.DETECTIONLOCK) || CommonLocks.contains(GuiLocks.DETECTIONLOCK)) {
				// route is committed
				State = EdgeStates.ROLL_UP;
			}
			else {
				// route is being cancelled
				Icon.outOfCorrespondence(true);
				State = EdgeStates.CLEARING;
			}
			if (EdgeLocks.contains(GuiLocks.FLEET)) {
				FleetIndication.requestLockClear();
			}
		}
		
		/**
		 * wipes out all vestiges of the route. if it exists
		 */
		private void killRoute() {
//			if (ActiveRoute != null) {
				Icon.holdSignal(true);
//				ActiveRoute.unreserveTrackEnds();
				routeWrapUp();
//			}
		}
		
		/**
		 * finishes making the route active.
		 */
		private void routeWrapUp() {
			State = EdgeStates.IDLE;
//			CPEdge.this.clearActiveRoute(CPEdge.this.ActiveRoute);
			if (ActiveRoute!= null) {
				ActiveRoute.releaseEdge();
				ActiveRoute = null;
			}
			DelaySwitch.cancelTimer();
		}
		
		/**
		 * puts the Edge into a state where the delay timers are running that line
		 * turnouts.
		 */
		public void prepareRoute() {
			EdgeStates oldState = State;
			State = EdgeStates.PREPARATION;
			setEdgeLock(GuiLocks.PREPARATION);
			Icon.setGUIPresentation(ColorList.PENDING, ColorList.PENDING);
			Icon.outOfCorrespondence(true);
			if (Crandic.Details.get(DebugBits.GUIROUTEBIT)  && !oldState.equals(State)) {
				System.out.println(CPEdge.this.toString() + ": Delaying for moving points. old="
						+ oldState + " new=" + State);
			}
		}
		
		/**
		 *  takes the CP Edge out of the preparation state, cleaning up the GUI
		 */
		public void endPreparation() {
			EdgeStates oldState = State;
			State = EdgeStates.IDLE;
			clearEdgeLock(GuiLocks.PREPARATION);
			Icon.setGUIPresentation(NoRoute, NoRoute);
			Icon.outOfCorrespondence(false);
			if (Crandic.Details.get(DebugBits.GUIROUTEBIT)  && !oldState.equals(State)) {
				System.out.println(CPEdge.this.toString() + ": Finished delaying for moving points. old="
						+ oldState + " new=" + State);
			}
			
		}
		
		/**
		 * is a query to determine if the state machine is in the IDLE state or
		 * not.  If in the IDLE state, then a new operation can be initiated.
		 * @return true if the state machine is in the IDLE state
		 */
		public boolean isIdleState() {
			return State == EdgeStates.IDLE;
		}
	}

	//	/*******************************************************************************************************
	//	 * right mouse click (fleeting) state machine
	//	 * Some things to consider are:
	//	 * - a route must be created first - this handles the safety checking
	//	 * - can a request to fleet be denied?
	//	 * - can a request to cancel be denied?
	//	 * - a cancel request stimulates a cancel on the route
	//	 * - there are some assumptions on order implied - e.g. a route will not be cancelled while a fleet 
	//	 * request is pending
	//	 * - what happens on a fleet request in a route that is partially satisfied?
	//	 *******************************************************************************************************/
	//	private class FleetStateMachine extends AbstractStateMachine {
	//
	//		@Override
	//		public boolean createEvent() {
	//			switch (EdgeState) {
	//			case NO_ACTION:
	//				//$FALL-THROUGH$
	//			case CLEARING:
	////				CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.FLEET, Constants.TRUE);
	//				RouteMachine.updateVitalLogic(true);
	//				if (TheRoute != null) {
	//					EdgeState = SigValues.PENDING;
	//				}
	//				else {
	//					EdgeState = SigValues.ACTIVE;
	//				}
	//				break;
	//			default:
	//			}
	//			return true;
	//		}
	//
	//		@Override
	//		public boolean cancelEvent() {
	//			switch(EdgeState) {
	//			case ACTIVE:
	//				//$FALL-THROUGH$
	//			case PENDING:
	////				CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.FLEET, Constants.FALSE);
	//				if (TheRoute != null) {
	//					EdgeState = SigValues.CLEARING;
	//				}
	//				else {
	//					EdgeState = SigValues.NO_ACTION;
	//				}
	//				break;
	//			default:
	//			}
	//			return true;
	//		}
	//
	//		@Override
	//		public void codeLineResponse(CodeMessage response) {
	//			if (response.Lock.equals(Codes.FLEET)) {
	//				if (response.Purpose.equals(CodePurpose.INDICATION)) {
	//					if (Boolean.valueOf(response.Value)) {
	//						EdgeState = SigValues.ACTIVE;
	//						FleetIndication.requestLockSet();
	//						Icon.setFleeting(true);
	//					}
	//					else {
	//						EdgeState = SigValues.NO_ACTION;
	//						FleetIndication.requestLockClear();
	//						Icon.setFleeting(false);
	//					}
	//				}
	//				else if (response.Purpose.equals(CodePurpose.DENY)) {
	//					if (EdgeState == SigValues.PENDING) {
	//						EdgeState = SigValues.NO_ACTION;
	//					}
	//					else if (EdgeState == SigValues.CLEARING) {
	//						EdgeState = SigValues.ACTIVE;
	//					}
	//				}
	//			}
	//		}
	//	}

	//	/*******************************************************************************************************
	//	 * right mouse click (call-on) state machine
	//	 * Some things to consider are:
	//	 * - an exit lock must not be set
	//	 *******************************************************************************************************/
	//	private class CallOnStateMachine extends AbstractStateMachine {
	//
	//		@Override
	//		public boolean createEvent() {
	//			switch (EdgeState) {
	//			case NO_ACTION:
	//				break;
	//			case CLEARING:
	//				break;
	//			default:
	//			}
	//			return true;
	//		}
	//
	//		@Override
	//		public boolean cancelEvent() {
	//			switch(EdgeState) {
	//			case ACTIVE:
	//				//$FALL-THROUGH$
	//			case PENDING:
	//				CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.CALLONLOCK, Constants.FALSE);
	//				EdgeState = SigValues.CLEARING;
	//				break;
	//			default:
	//			}
	//			return true;
	//		}
	//
	//		@Override
	//		public void codeLineResponse(CodeMessage response) {
	//			if (response.Lock.equals(Codes.CALLONLOCK)) {
	//				if (response.Purpose.equals(CodePurpose.INDICATION)) {
	//					if (Boolean.valueOf(response.Value)) {
	//						EdgeState = SigValues.ACTIVE;
	//						Icon.setCallOn(true);
	//					}
	//					else {
	//						EdgeState = SigValues.NO_ACTION;
	//						Icon.setCallOn(false);
	//					}
	//				}
	//				else if (response.Purpose.equals(CodePurpose.DENY)) {
	//					if (EdgeState == SigValues.PENDING) {
	//						EdgeState = SigValues.NO_ACTION;
	//					}
	//					else if (EdgeState == SigValues.CLEARING) {
	//						EdgeState = SigValues.ACTIVE;
	//					}
	//				}
	//			}
	//		}
	//	}
	
	/***********************************************************************************
	 * method for receiving left or right mouse clicks for completing an eXtended Route.
	 * Two mouse steps are required to create an eXtended Route.
	 * 1. The user positions the mouse over a Signal Icon, uses the right mouse to
	 * 		pull up the menu, and selects an eXtended Route option.  This disables the
	 * 		general mouse processing in Screen.
	 * 2. The user positions the mouse over another Signal Icon and uses either mouse
	 * 		button to select the end of the eXtended Route.  This routine captures
	 * 		the second mouse click.
	 * Of course, there is a lot more to the procedure than above - how to handle
	 * a mouse click not over a Signal Icon, different actions for left and right
	 * clicks, maybe an abort key, etc.
	 ***********************************************************************************/
	private class SecondClick implements MouseUser {

		/**
		 * the CPEdge selected by the second mouse click.  It is captured on mouse down.
		 */
		private CPEdge EndRoute;
		
		/**
		 * the eXtended Route of RouteNodes as a path from this CPEdge to the requested one
		 */
		private RouteNode Anchor;
		
		/**
		 * the reason for constructing an eXtended Route
		 */
		private int XRouteReason;
		
		/**
		 * the ctor
		 */
		public SecondClick() {
			EndRoute = null;
		}
		
		@Override
		public boolean mousePush(MouseEvent event) {
			EndRoute = findSignal(event);
			return false;
		}
		
		@Override
		public boolean finishMouse(MouseEvent event) {
			EndRoute = findSignal(event);
			if ((EndRoute == null) || (EndRoute == CPEdge.this)) {
				if (CATSOptionPane.showDialog_RETRY_OR_CANCEL(event.getPoint(), "Exit signal not selected.  Retry?", "No Signal Selected") == JOptionPane.CANCEL_OPTION) {
//				if (JOptionPane.showConfirmDialog(null, "Exit signal not selected.  Retry?", "No Signal Selected", JOptionPane.OK_CANCEL_OPTION) ==
//						JOptionPane.CANCEL_OPTION) {
//					Icon.setGUIPresentation(NoRoute, NoRoute);
//					Icon.outOfCorrespondence(false);
//					Icon.holdSignal(false);
//					Icon.setDefaultBackground(true);
					if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
						log.info(Constants.ROUTE_CREATION + "Aborting extended route");
					}
					restoreIcon();
					return true;
				}
				else {
					if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
						log.info(Constants.ROUTE_CREATION + "Retry for second signal");
					}
					return false;
				}
			}
			else {
//				JOptionPane.showMessageDialog((Component) null, "Located an exit signal at " + endRoute.toString(),
//						"Extended Route Located", JOptionPane.INFORMATION_MESSAGE);
//				Icon.setGUIPresentation(NoRoute, NoRoute);
//				Icon.outOfCorrespondence(false);
//				Icon.setDefaultBackground(true);
				restoreIcon();
				Anchor = RouteCreator.getRouteFactory().DAGRouteSearch(CPEdge.this, EndRoute);
				if (Anchor == null) {
					if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
						log.info(Constants.ROUTE_CREATION + "No extended route found");
					}
					CATSOptionPane.showInformationMessage(event.getPoint(), "No Path Found", "Search Failure");
				}
				else {
//					Anchor.dumpDAG();
//					JOptionPane.showMessageDialog((Component) null, "DAG search found destination CPEdge",
//							"Search Success", JOptionPane.INFORMATION_MESSAGE);	
					// the DAG has been created, so now the nodes can be removed from the edge
					// search links
					if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
						System.out.println("DAG successfully constructed");
					}
					Anchor.disconnectSearchNodes();
					switch (XRouteReason) {
					case NX_MENU:
					case FLEET_MENU:
						if (Anchor.getBlockageCount() == 0) {
							// this should probably be queued for deferred execution
							if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
								log.info(Constants.ROUTE_CREATION + "Extended route created");
							}
							directRoute(Anchor);
						}
						else {
							if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
								log.info(Constants.ROUTE_CREATION + "Extended route blocked");
							}
							CATSOptionPane.showErrorMessage(event.getPoint(), "No unobstructed route found", "Search Result");
						}
						break;
						
					case PRECONDITION_MENU:
						if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
							log.info(Constants.ROUTE_CREATION + "Route stacked");
						}
						StackOfRoutes.instance().pushRoute(Anchor);
						CATSOptionPane.showInformationMessage(event.getPoint(), "Route " + Anchor.toString() + " Stacked", "Route Stacked");
						break;
					
					default:
						CATSOptionPane.showErrorMessage(event.getPoint(), "eXtended Route function not implemented", "Search Success");
					}	
				}
//				Anchor.removeNodes(null);
				return true;
			}
		}
		
		/**
		 * tries to determine if the mouse has been clicked over a signal icon.
		 * @param event is the mouse event generated by the click
		 * @return if over an icon, the CPEdge associated with the signal icon;
		 * else null
		 */
		private CPEdge findSignal(MouseEvent event) {
			if (EndRoute == null) {
				MouseUser dest;
				Section sec;
				sec = Screen.DispatcherPanel.locatePt(event.getPoint());
				if (sec != null) {
					dest = sec.getTile().startMouse(event);
					if (dest instanceof SignalFrill) {
						return ((SignalFrill) dest).getOwner();
					}
				}
			}
			return EndRoute;
		}
		
		/**
		 * a method for initializing the variables for creating an eXtended Route.  Several things
		 * happen:
		 * 1. mouse button pushes are intercepted by registering this object with the master mouse handler.
		 * If the handler is in the middle of processing mouse events, the intercept will be denied and
		 * an error pop-up presented.
		 * 2. the signal icon background will be changed for the operation
		 * 
		 * @param location is the location of the mouse cursor on the panel
		 * @param routeType is the type of eXtended Route being created.  It is also the index of the selection
		 * on the right mouse menu
		 */
		public void startXRoute(final Point location, final int routeType) {
			EndRoute = null;
			XRouteReason = routeType;
			if (Screen.DispatcherPanel.setExtendedRoute(this)) {
				Icon.holdSignal(true);
				Icon.setGUIPresentation(ColorList.XROUTE, ColorList.XROUTE);
				Icon.outOfCorrespondence(true);
//				Icon.setDefaultBackground(false);
			}
			else {
				CATSOptionPane.showErrorMessage(location, "The primary mouse button is in use at this time.", "Extended Route Denied");
			}
		}
		
		/**
		 * restores the signalIcon to reflect the signal's indication
		 */
		private void restoreIcon() {
			Icon.outOfCorrespondence(false);
			if (RouteMachine.isIdleState()) {
				Icon.setGUIPresentation(NoRoute, NoRoute);
			}
			else {
				Icon.holdSignal(false);
			}
		}
	}
	
	/***********************************************************************************
	 * A state machine for moving the points to the preferred route.  The preferred route
	 * is specified in a DAG.  All points on the route have the Alignment bit set in the
	 * associated RouteNodePts.  This state machine delays until all points in the route
	 * have not been occupied for awhile (safety check to avoid moving points under a train),
	 * then moves any points not lined correctly, and when all have been lined, creates
	 * a Quick Route.
	 ***********************************************************************************/
	private class DelayedMovement implements TimeoutObserver {
		
//		/**
//		 * a count of the number of occupancies that the enclosing Block has seen
//		 */
//		long OccupancyCounts;
		
		/**
		 * the state of he state machine
		 */
		DELAY_STEPS Step = DELAY_STEPS.NO_DELAY;
		
		TimeoutEvent TimeOutEvent = new TimeoutEvent(this, 1);
		
		public void startDelay() {
			if (ActiveRoute == null) {
				Step = DELAY_STEPS.NO_DELAY;
			}
			else {
//				OccupancyCounts = MyBlock.getDetectionCount();
				Step = DELAY_STEPS.OCCUPANCY_DELAY;
				TimeOutEvent.Count = CounterFactory.CountKeeper.findSequence(CounterFactory.TURNOUTSAFETYTAG).getAdjustment();
				MasterClock.MyClock.setTimeout(TimeOutEvent);
				RouteMachine.prepareRoute();
				if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
					System.out.println(CPEdge.this.toString() + " starting points delay");
				}
			}
		}
		
		/**
		 * cancels the timer.  Setting the state to NO_DELAY will disarm it when it times out.
		 */
		public void cancelTimer() {
			Step = DELAY_STEPS.CANCEL_DELAY;
		}

		@Override
		public void acceptTimeout() {
//			long newCount;
			if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
				System.out.println(CPEdge.this.toString() + " timeout in step " + Step);
			}
			if (ActiveRoute == null) {
				Step = DELAY_STEPS.NO_DELAY;
			}
			else {
				switch(Step) {
				case OCCUPANCY_DELAY:
//					newCount = MyBlock.getDetectionCount();
//					if ((OccupancyCounts != newCount) || EdgeLocks.contains(GuiLocks.DETECTIONLOCK) || CommonLocks.contains(GuiLocks.DETECTIONLOCK)) {
//						OccupancyCounts = newCount;
					if (!ActiveRoute.checkStability()) {
						TimeOutEvent.Count = CounterFactory.CountKeeper.findSequence(CounterFactory.TURNOUTSAFETYTAG).getAdjustment();
						MasterClock.MyClock.setTimeout(TimeOutEvent);
					}
					else if (ActiveRoute.countMislinedPoints() == 0) {
						RouteMachine.endPreparation();
						RouteMachine.startRoute();
						Step = DELAY_STEPS.NO_DELAY;
					}
					else {
						ActiveRoute.setPoints();
						Step = DELAY_STEPS.MOVEMENT_DELAY;
						TimeOutEvent.Count = CounterFactory.CountKeeper.findSequence(CounterFactory.TURNOUTSAFETYTAG).getAdjustment();
						MasterClock.MyClock.setTimeout(TimeOutEvent);
					}
					break;
				case MOVEMENT_DELAY:
					if (ActiveRoute.countMislinedPoints() == 0) {
						RouteMachine.endPreparation();
						RouteMachine.startRoute();
						Step = DELAY_STEPS.NO_DELAY;						
					}
//					else if (ActiveRoute.setPoints() == 0) {
//						RouteMachine.startRoute();
//						Step = DELAY_STEPS.NO_DELAY;
//					}
					else {
						TimeOutEvent.Count = CounterFactory.CountKeeper.findSequence(CounterFactory.TURNOUTSAFETYTAG).getAdjustment();
						MasterClock.MyClock.setTimeout(TimeOutEvent);
					}
					break;
				case CANCEL_DELAY:
					Step = DELAY_STEPS.NO_DELAY;
					RouteMachine.endPreparation();
					break;
				case NO_DELAY:
				}
			}
			
		}
	}
	static org.apache.log4j.Logger Log = org.apache.log4j.Logger.getLogger(CPEdge.class.getName());
}
/* @(#)CPEdge.java */
