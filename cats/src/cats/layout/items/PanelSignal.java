/* Name: PanelSignal.java
 *
 * What:
 *   This class is the container for the User Interface for a Signal's appearance
 *   on the dispatcher panel.  The UI has two pieces - the presentation of the 
 *   signal and a "hot spot" for providing commands to the signaling logic.
 *   <p>
 *   PanelSignals denote Control Points (CTC and DTC) or Head Signals (ABS or APB).
 */

package cats.layout.items;

import cats.apps.Crandic;
import cats.common.Constants;
import cats.common.DebugBits;
import cats.gui.CtcFont;
import cats.gui.GridTile;
import cats.gui.frills.FrillLoc;
import cats.gui.frills.SignalFrill;
import cats.gui.frills.LightFrill;
import cats.gui.frills.SemaphoreFrill;
import cats.layout.AspectMap;
import cats.layout.ColorList;
import cats.layout.MasterClock;
import cats.layout.TimeoutObserver;
import cats.layout.xml.*;
import cats.rr_events.TimeoutEvent;

import org.jdom2.Element;

/**
 *   This class is the container for the User Interface for a Signal's appearance
 *   on the dispatcher panel.  The UI has two pieces - the presentation of the 
 *   signal and a "hot spot" for providing commands to the signaling logic.
 *   <p>
 *   PanelSignals denote Control Points (CTC and DTC) or Head Signals (ABS or APB).
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2011, 2012, 2014, 2016, 2018, 2020, 2021, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class PanelSignal
implements XMLEleObject {
	/**
	 * is the tag for identifying a PanelSignal in the XMl file.
	 */
	static final String XML_TAG = "PANELSIGNAL";

	/**
	 * is the XML tag for identifying the Signal's location in the GridTile.
	 */
	static final String SIG_LOCATION = "SIGLOCATION";

	/**
	 * is the XML tag for identifying the Signal's orientation.
	 */
	static final String SIG_ORIENT = "SIGORIENT";

	/**
	 * is the XML tag for identifying the Signal's type.
	 */
	static final String SIG_PANEL_TYPE = "SIGPANTYPE";

	/**
	 * is the values of the Signal's type.
	 */
	static final String[] SIG_TYPE = {
		"LAMP1",
		"LAMP2",
		"LAMP3",
		"SEM1",
		"SEM2",
		"SEM3"
	};

	/**
	 * is a look up table for the number of heads corresponding to each of the
	 * above.
	 */
	static final int[] SIGNAL_HEADS = {
		1, 2, 3, 1, 2, 3
	};

	/**
	 * is a look up table for the kind of signal, corresponding to the above.
	 */

	static final boolean[] LAMP = {
		true, true, true, false, false, false
	};

//	/**
//	 * The colors for the signal icon, when it has a route through it or is ABS
//	 * or APB.
//	 */
//	private static final EnumMap<TrackCodes, String> IndicationColor = new EnumMap<TrackCodes,
//			String>(TrackCodes.class);
//	static {
//		IndicationColor.put(TrackCodes.HALT, ColorList.STOP);
//		IndicationColor.put(TrackCodes.APPROACH, ColorList.APPROACH);
//		IndicationColor.put(TrackCodes.DIVERGING, ColorList.CLEAR);
//		IndicationColor.put(TrackCodes.ADVANCEAPP, ColorList.APPROACH);
//		IndicationColor.put(TrackCodes.CLEAR, ColorList.CLEAR);
//	}

	/**
	 *  The GridTile where painting happens.
	 */
	private GridTile SignalTile;

	/**
	 *  The Frill for displaying the Signal Icon.
	 */
	private SignalFrill TheFrill;

	/**
	 *  The location of the Signal Icon in the GridTile.
	 */
	private FrillLoc MyLocation;

	/**
	 * The orientation of the Signal Icon.
	 */
	private int MyOrientation;

	/**
	 * The number of heads.
	 */
	private int MyHeadCount;

	/**
	 * The kind of Icon - true for light and false for semaphore.
	 */
	private boolean MyLamp;

	/**
	 * is a flag indicating if the icon should reflect the signal indication
	 * or the GUI state.  It is roughly equivalent to "holding" the signal
	 * at stop.
	 */
	private boolean SignalHeld;

//	/**
//	 * the On/OFF duty cycle in milliseconds
//	 */
//	private static final int HALF_CYCLE = 500;
//
//	/**
//	 * is a MyTimer that controls the on and off periods.
//	 */
//	private javax.swing.Timer MyTimer = null;

	/**
	 * is true if the icon is animated (flashing)
	 */
	private boolean Animated;
	
	/**
	 * the object used to include a time out request on the CATS timer chain
	 */
	private TimeoutEvent CatsTimeout;
	
	/**
	 * identifies the half cycle - true means in the blank phase
	 */
	private boolean Blank = false;

//	/**
//	 * is true if the fleeting is active - this changes the icon presentation
//	 */
//	private boolean Fleeting = false;
	
	/**
	 *  The CPEdge protected by the Signal.
	 */
	private CPEdge Protected;
	
	/**
	 * is the IndicationColor of the last color set
	 */
	private int LastIndication = -1;

	/**
	 * when the GUI is coloring the icon, the color of the head
	 */
	private String HeadColor;
	
	/**
	 * when the GUI is coloring the icon, the color of the base
	 */
	private String BaseColor;
	
	/**
	 * is the name of the last color used to paint the icon.  This is needed because
	 * the icon is given its initial color before it is installed.
	 */
	private String LastColor;

	/**
	 * is the last color recorded in a test case
	 */
	private String RecordedLastColor;

	/**
	 * constructs a PanelSignal, given no other information
	 */
	public PanelSignal() {
		MyLamp = true;
		SignalHeld = false;
		MyHeadCount = 1;
		CatsTimeout = new TimeoutEvent(new TickReceiver(), MasterClock.ONE_SECOND);
		Animated = false;
	}

	/**
	 * changes the location of the PanelSignal in its GridTile.
	 *
	 * @param loc is the new location.
	 *
	 * @see cats.gui.frills.FrillLoc
	 */
	public void setLocation(FrillLoc loc) {
		MyLocation = loc;
	}

	/**
	 * changes the Icon's orientation.
	 *
	 * @param orient is the new orientation.  UNKNOWN is valid.
	 */
	public void setOrient(int orient) {
		MyOrientation = orient;
	}

	/**
	 * retrieves the number of heads.
	 *
	 * @return the number of heads.  0 is invalid.
	 */
	public int getHeadCount() {
		return MyHeadCount;
	}

	/**
	 * retrieves the signal type
	 * @return return true if lamp and false if semaphore
	 */
	public boolean isLamp() {
		return MyLamp;
	}
	
	/** sets the type of Icon and number of heads.
	 *
	 * @param type is true for lights and false for semaphore.
	 * @param num is the number of heads (1-3).
	 */
	public void setParms(boolean type, int num) {
		MyLamp = type;
		MyHeadCount = num;
	}

	/**
	 * sets the PanelSignal to Dark.
	 * @param dark being true means the GUI controls the icon's appearance;
	 * false means the Signal controls the appearance
	 */
	public void holdSignal(boolean dark) {
		SignalHeld = dark;
		paintIcon();
	}

	/**
	 * tells the PanelSignal that a command has been sent to the VitalLogic
	 * and the response has not been received
	 * @param unsynced is true while the command is outstanding
	 */
	public void outOfCorrespondence(boolean unsynced) {
		// flashing works, but though dark overrides the type, the type is needed
		// for when dark is removed
		if (unsynced) {
			startFlash();
		}
		else {
			stopFlash();
		}
	}

	/**
	 * tells the PanelSignal what CPEdge it is protecting, so that it knows
	 * whose method to call when the user mouse clicks on the Icon.
	 * @param protect is the CPEdge being protected.
	 */
	public void protectEdge(CPEdge protect) {
		Protected = protect;
	}

	/**
	 * tells the PanelSignal where its Icon is shown.
	 *
	 * @param tile is the GridTile where painting happens.
	 *
	 * @see cats.gui.GridTile
	 */
	public void install(GridTile tile) {
		String finder = null;
		SignalTile = tile;
		if (tile != null) {
			if (MyLamp) {
				TheFrill = new LightFrill(MyLocation, MyOrientation, MyHeadCount);
			}
			else {
				TheFrill = new SemaphoreFrill(MyLocation, MyOrientation, MyHeadCount);
			}
			SignalTile.addFrill(TheFrill);
			if (SignalHeld) {
				finder = ColorList.NO_ROUTE;
			}
			else if (LastColor != null) {
				finder = LastColor;
			}
			else {
				finder = ColorList.NO_ROUTE;
			}
			if (Crandic.Details.get(DebugBits.SIGNALBIT)) {
				System.out.println("Setting icon for " + Protected.toString() + " to " + finder);
			}
			TheFrill.setSignalColor(finder);
			TheFrill.setOwner(Protected);
		}
	}

	/**
	 * sets the color of the icon based on the Signal's indication
	 * @param indication is the indication displayed by the Signal
	 */
	public void setIndicationType(int indication) {
		LastIndication = indication;
////		TrackCodes indicationType;
//		if (!Blank) {
//			if (SignalHeld) {
//				LastColor = ColorList.NO_ROUTE;
//			}
//			else {
////				if (indication == -1) {
////					indicationType = AspectMap.INDICATIONTYPE.HALT;
////				}
////				else {
////				indicationType = AspectMap.IndicationType[indication];
////				}
////				LastColor = IndicationColor.get(indicationType);
//				LastColor = AspectMap.IndicationNames[indication][AspectMap.ICON];
//			}
//			if (TheFrill != null) {
//				if (Fleeting) {
//					TheFrill.setSignalColor(LastColor, ColorList.NO_ROUTE);
//				}
//				else {
//					TheFrill.setSignalColor(LastColor);
//				}
//				SignalTile.requestUpdate();
//				GridTile.doUpdates();
//			}
//		}
		paintIcon();
	}

	/**
	 * sets the color of the icon's head and base.  If either is null, then
	 * the stored value is not changed.
	 * @param head is a ColorList String mnemonic for the Color of the head
	 * @param base is a ColorList String mnemonic for the Color of the base
	 */
	public void setGUIPresentation(final String head, final String base) {
		if (head != null) {
			HeadColor = head;
		}
		if (base != null) {
			BaseColor = base;
		}
		paintIcon();
	}
	
	/**
	 * a method that determines who is in control of the icon - the Signal or the GUI.
	 * It then paints the icon accordingly.
	 */
	private void paintIcon() {
		if (TheFrill != null) {
			if (SignalHeld) {
				LastColor = HeadColor;
				TheFrill.setSignalColor(HeadColor, BaseColor);
				if (Crandic.Details.get(DebugBits.SIGNALBIT)) {
					System.out.println("Setting icon for " + Protected.toString() + " to " + HeadColor + " and " + BaseColor);
				}
			}
			else {
				LastColor = AspectMap.IndicationNames[LastIndication][AspectMap.ICON];
				TheFrill.setSignalColor(LastColor);
				if (Crandic.Details.get(DebugBits.SIGNALBIT)) {
					System.out.println("Setting icon for " + Protected.toString() + " to " + LastColor);
				}
			}
			SignalTile.requestUpdate();
			GridTile.doUpdates();
		}
		else if (SignalHeld ){  // the PanelSignal has not been completely instantiated
			LastColor = HeadColor;
		}
		else if (LastIndication > -1) {
			LastColor = AspectMap.IndicationNames[LastIndication][AspectMap.ICON];
		}
	}
	
	/*
	 * Start the MyTimer that controls flashing
	 */
	private void startFlash() {
//		if (MyTimer==null) {
//			MyTimer = new javax.swing.Timer(HALF_CYCLE, new java.awt.event.ActionListener() {
//				public void actionPerformed(java.awt.event.ActionEvent e) {
//					timeout();
//				}
//			});
//			MyTimer.setInitialDelay(HALF_CYCLE);
//			MyTimer.setRepeats(true);
//		}
//		MyTimer.start();
		TheFrill.setBlank(Blank = false);
		Animated = true;
		MasterClock.MyClock.setTimeout(CatsTimeout);
	}

	/**
	 * invoked on a timeout.  This is what turns the light on or off.
	 */
	private void timeout() {
		if (Animated) {
			Blank = !Blank;
			TheFrill.setBlank(Blank);
			SignalTile.requestUpdate();
			GridTile.doUpdates();
		}
	}

	/*
	 * Stop the MyTimer that controls flashing.
	 *
	 * This is only a resource-saver; the actual use of 
	 * flashing happens elsewere
	 */
	private void stopFlash() {
//		if (MyTimer!=null) {
//			MyTimer.stop();
//		}
		Animated = false;
		Blank = false;
		TheFrill.setBlank(false);
		setIndicationType(LastIndication);
	}

//	/**
//	 * chooses the background color behind the icon.  This does not work because
//	 * of how CATS schedules screen updates.
//	 * @param defaultColor is true to change it to the background of the rest
//	 * of the layout and false to change it to the color denoting second
//	 * mouse button search
//	 */
//	public void setDefaultBackground( final boolean defaultColor) {
//		TheFrill.setDefaultBackground(defaultColor);
//		TheFrill.setBlank(true);
//		SignalTile.requestUpdate();
//		GridTile.doUpdates();
//		TheFrill.setBlank(Blank);
//		SignalTile.requestUpdate();
//		GridTile.doUpdates();		
//	}
//	/**
//	 * sets/clears fleeting and changes the icon's presentation
//	 * @param fleeting is true to change the icon's presentation to fleeting
//	 * and false to turn it off
//	 */
//	public void setFleeting(boolean fleeting) {
//		Fleeting = fleeting;
//		if (fleeting) {
//			TheFrill.setSignalColor(LastColor, ColorList.NO_ROUTE);
//		}
//		else {
//			TheFrill.setSignalColor(LastColor);
//		}
//	}
	
//	/**
//	 * sets/clears call-on and changes the icon's presentation
//	 * @param callOn is true to change the icon's presentation to show
//	 * call-on and false to turn it off
//	 */
//	public void setCallOn(boolean callOn) {
//		
//	}
	
	/**
	 * records the signal icon's state in an XML Element for automated testing.
	 * @param basic is true to record only basic state information.  It is false
	 * to record detailed state information.
	 * @return an Element, if the state has changed since the last snapshot; otherwise,
	 * null.
	 */
	public Element dumpIconState(boolean basic) {
		boolean changed = false;
		Element newState = new Element(XML_TAG);
		if (!LastColor.equals(RecordedLastColor)) {
			newState.setAttribute("COLOR", LastColor);
			RecordedLastColor = LastColor;
			changed = true;
		}
		return (changed) ? newState : null;
	}

	/************************************************************************
	 * an inner class for receiving phase timeouts for flashing the icon
	 ************************************************************************/
	private class TickReceiver implements TimeoutObserver {

		@Override
		public void acceptTimeout() {
			timeout();
			if (Animated) {
				MasterClock.MyClock.setTimeout(CatsTimeout);
			}
		}
	}

	/**
	 * registers a PanelSignalFactory with the XMLReader.
	 */
	static public void init() {
		XMLReader.registerFactory(XML_TAG, new PanelSignalFactory());
	}

	/*******************************************************************************
	 * the routines for constructing the PanelSignal from XML
	 *******************************************************************************/
	/*
	 * is the method through which the object receives the text field.
	 *
	 * @param eleValue is the Text for the Element's value.
	 *
	 * @return if the value is acceptable, then null; otherwise, an error
	 * string.
	 */
	public String setValue(String eleValue) {
		MyLocation = FrillLoc.newFrillLoc(eleValue);
		return null;
	}

	/*
	 * is the method through which the object receives embedded Objects.
	 *
	 * @param objName is the name of the embedded object
	 * @param objValue is the value of the embedded object
	 *
	 * @return null if the Object is acceptible or an error String
	 * if it is not.
	 */
	public String setObject(String objName, Object objValue) {
		return new String("A " + XML_TAG + " cannot contain an Element ("
				+ objName + ").");
	}

	/*
	 * returns the XML Element tag for the XMLEleObject.
	 *
	 * @return the name by which XMLReader knows the XMLEleObject (the
	 * Element tag).
	 */
	public String getTag() {
		return new String(XML_TAG);
	}

	/*
	 * tells the XMLEleObject that no more setValue or setObject calls will
	 * be made; thus, it can do any error checking that it needs.
	 *
	 * @return null, if it has received everything it needs or an error
	 * string if something isn't correct.
	 */
	public String doneXML() {
		return null;
	}
}

/**
 * is a Class known only to the PanelSignal class for creating PanelSignals from
 * an XML document.  Its purpose is to pick up the location of the SecSignal
 * in the GridTile, its orientation, and physical attributes on the layout.
 */
class PanelSignalFactory
implements XMLEleFactory {

	FrillLoc Location;
	int Orientation;
	boolean Lamp;
	int Heads;

	/*
	 * tells the factory that an XMLEleObject is to be created.  Thus,
	 * its contents can be set from the information in an XML Element
	 * description.
	 */
	public void newElement() {
		Location = null;
		Orientation = Constants.NOT_FOUND;
		Lamp = true;
		Heads = 1;
	}

	/*
	 * gives the factory an initialization value for the created XMLEleObject.
	 *
	 * @param tag is the name of the attribute.
	 * @param value is it value.
	 *
	 * @return null if the tag:value are accepted; otherwise, an error
	 * string.
	 */
	public String addAttribute(String tag, String value) {
		String resultMsg = null;

		if (PanelSignal.SIG_ORIENT.equals(tag)) {
			Orientation = Edge.toEdge(value);
		}
		else if (PanelSignal.SIG_LOCATION.equals(tag)) {
			Location = FrillLoc.newFrillLoc(value);
		}
		else if (PanelSignal.SIG_PANEL_TYPE.equals(tag)) {
			int type = CtcFont.findString(value, PanelSignal.SIG_TYPE);
			if (type != Constants.NOT_FOUND) {
				Lamp = PanelSignal.LAMP[type];
				Heads = PanelSignal.SIGNAL_HEADS[type];
			}
		}
		else {
			resultMsg = new String("A " + PanelSignal.XML_TAG +
					" XML Element cannot have a " + tag +
					" attribute.");
		}
		return resultMsg;
	}

	/*
	 * tells the factory that the attributes have been seen; therefore,
	 * return the XMLEleObject created.
	 *
	 * @return the newly created XMLEleObject or null (if there was a problem
	 * in creating it).
	 */
	public XMLEleObject getObject() {
		PanelSignal p = new PanelSignal();
		p.setLocation(Location);
		p.setOrient(Orientation);
		p.setParms(Lamp, Heads);
		return p;
	}
}
/* @(#)PanelSignal.java */