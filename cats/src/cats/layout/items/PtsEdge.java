/* Name: PtsEdge.java
 *
 * What:
 * A PtsEdge "is a" SecEdge which represents switch points - an edge on which
 * multiple tracks (routes) terminate.  It is the SecEdge known by the Section and
 * by the neighbor; however, it is not a TrackEnd on any Tracks.  The SecEdges on
 * TrackEnds are FrogEdges.  Thus, it receives messages from the neighbor and
 * distributes them to all the FrogEdges.  
 * <p>
 * Since a PtsEdge is not a TrackEnd on any Track, it will not receive a message from
 * its Peer.  Furthermore, since it has no "peer" at the other end of the track, the
 * Peer field will be used for holding the FrogEdge of the currently aligned track.
 */

package cats.layout.items;

import cats.apps.Crandic;
import cats.common.Constants;
import cats.common.DebugBits;
import cats.common.Sides;
import cats.gui.GridTile;
import cats.gui.frills.LockFrill;
import cats.layout.codeLine.CodeMessage;
import cats.layout.codeLine.CodePurpose;
import cats.layout.codeLine.Codes;
import cats.layout.codeLine.packetFactory.EdgePacketFactory;
import cats.layout.codeLine.packetFactory.VitalPacketFactory;
import cats.layout.ctc.RouteNode;
import cats.layout.ctc.RouteNodePts;
import cats.layout.vitalLogic.BasicVitalLogic;
import cats.layout.vitalLogic.FrogVitalLogic;
import cats.layout.vitalLogic.PtsVitalLogic;
import java.awt.Component;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JOptionPane;

import org.jdom2.Element;

/**
 * A PtsEdge "is a" SecEdge which represents switch points - an edge on which
 * multiple tracks (routes) terminate.  It is the SecEdge known by the Section and
 * by the neighbor; however, it is not a TrackEnd on any Tracks.  The SecEdges on
 * TrackEnds are FrogEdges.  Thus, it receives messages from the neighbor and
 * distributes them to all the FrogEdges.  
 * <p>
 * Since a PtsEdge is not a TrackEnd on any Track, it will not receive a message from
 * its Peer.  Furthermore, since it has no "peer" at the other end of the track, the
 * Peer field will be used for holding the FrogEdge of the currently aligned track.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2009, 2010, 2011, 2012, 2014, 2018, 2019, 2020, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class PtsEdge extends AbstractTrackEdge implements SignalEdge, RouteEdge {

	/**
	 * is all the GuiLocks, except CONFLICTINGSIGNALLOCK
	 */
	private static final EnumSet<GuiLocks> PTS_FILTER = EnumSet.complementOf(EnumSet.of(GuiLocks.CONFLICTINGSIGNALLOCK));
	
	/**
	 * is a list of all PtsEdges on the layout.
	 */
	private static Vector<PtsEdge> PtsKeeper = new Vector<PtsEdge>();

	/**
	 * defines the number of SecEdges at the other end of routes that
	 * terminate on this SecEdge.  There physically cannot be more
	 * than there are sides on a grid tile (4).  There will never be
	 * that many because the PtsEdge occupies a side.
	 */
	public static final int MAX_ROUTES = Sides.EDGENAME.length;

	/**
	 * the following are indices into the Feedback BitSets, identifying the
	 * feedback properties of each route.
	 */
	
	/**
	 * set means that a route has been defined
	 */
	protected final int HAS_ROUTE = 0;
	
	/**
	 * set means that either in place or release feedback has been defined
	 */
	protected final int HAS_FEEDBACK = HAS_ROUTE + 1;
	
	/**
	 * set means in place feedback has been defined
	 */
	protected final int HAS_IN_PLACE = HAS_FEEDBACK + 1;
	
	/**
	 * set means the in place feedback has the same decoder address as the command
	 */
	protected final int COUPLED_IN_PLACE = HAS_IN_PLACE + 1;
	
	/**
	 * set means the in place feedback has a different decoder address than the command
	 */
	protected final int UNCOUPLED_IN_PLACE = COUPLED_IN_PLACE + 1;
	
	/**
	 * set means release feedback has been defined
	 */
	protected final int HAS_RELEASE = UNCOUPLED_IN_PLACE + 1;
	
	/**
	 * is the number of bits (indices) used by the above
	 */
	protected final int FEEDBACK_BITS = HAS_RELEASE + 1;
	
	/**
	 * the bitsets for each route
	 */
	protected final BitSet Feedback[] = {
			new BitSet(FEEDBACK_BITS),
			new BitSet(FEEDBACK_BITS),
			new BitSet(FEEDBACK_BITS),
			new BitSet(FEEDBACK_BITS)
	};
	
	/**
	 * describes the routes through the points, how to sense the turnout
	 * position, how to throw the turnout, and how to set the lock light.
	 */
	protected SwitchPoints MyPoints;

	/**
	 * the Tracks terminating on this edge.  There can be a maximum of three
	 * tracks (one to each of the other edges), but the array contains four
	 * because each track is identified by the edge the other end terminates on.
	 * <p>
	 * The tracks are indexed by the number of the side at the other end.
	 */
	protected Track[] MyTracks = new Track[MAX_ROUTES];

	/**
	 * the FrogEdges, corresponding to the "Peers" for each track above
	 */
	private FrogEdge[] MyFrogs = new FrogEdge[MAX_ROUTES];

	/**
	 * the list of all RouteInfos that share a JmriDevice with this PtsEdge
	 */
	protected ArrayList<RouteReference>[] SharedDecoders;
	
	/**
	 * a flag for each route.  true means that a command exists
	 * to move the points to that route.
	 */
	protected final boolean[] HasCommand;
	
	/**
	 * the index of the requested track for an Extended Route
	 */
	private int ExtendedRouteTrk = Constants.OutofCorrespondence;
	
	/**
	 * the index of the currently selected track.  If it is the same as
	 * MyEdge, then the selection is not known.
	 */
	protected int CurrentTrk = MyEdge;

	/**
	 * the index of the last selected track, prior to a request to
	 * move the points.
	 */
	private int LastTrk = Constants.OutofCorrespondence;

	/**
	 * is a flag indicating if the points have been locked in the field.  If true, then
	 * the local crew must unlock them before requests to move them will be
	 * honored.  This flag reflects that state of the field lock and is changed
	 * by the "Turn FieldLocked Light On" and "Turn Unlocked Light Off" layout responses.
	 */
	protected boolean FieldLocked = true;

	/**
	 * the index of the Track with the Normal route through the points.
	 */
	private final int NormalRoute;

	/**
	 * is true if there is a listener for either the Lock or Unlock reply,
	 * indicating that CATS will be told when the field locks/unlocks
	 */
	private final boolean HasFieldLock;
	
	/**
	 * the local lock symbol
	 */
	private LockFrill LockSymbol;
	
	/**
	 * the associated VitalLogic
	 */
	private PtsVitalLogic PtsLogic;

//	/**
//	 * the ProxySignal in MyLogic.  This is the signal fed from the
//	 * approach side, counter to the normal flow of aspects.
//	 */
//	private ProxySignal SignalLogic;

	/**
	 * the Code Line for talking to the VitalLogic
	 */
	protected VitalPacketFactory CodeLineEntry;

	/*******************************************************************************
	 * the following are mirror images of the indication handling from the Peer, but
	 * from the current FrogEdge
	 ********************************************************************************/

	/**
	 * the set of Edge Locks from the Frog
	 */
	protected EnumSet<GuiLocks> FrogEdgeLocks = EnumSet.noneOf(GuiLocks.class);

	/**
	 * the set of Common Locks from the Frog
	 */
	protected EnumSet<GuiLocks>	FrogCommonLocks = EnumSet.noneOf(GuiLocks.class);
	
	/***********************************************************************
	 * used for path coverage when searching for an eXtended Route
	 ***********************************************************************/
	protected int SearchFrog;

	/**
	 * used for remembering direction search entered the points.  This needed for
	 * detecting reverse loops.  A successful search enters the PtsEdge
	 * from only one direction.
	 */
	private boolean PointsSearch;
	

	//	/**
//	 * a counter of the number of times occupancy was detected.  This is used
//	 * for determining the safety of moving point autonomously.
//	 */
//	protected long OccupancyCount;
	
	/***********************************************************************
	 * The following are used for detecting changes for automated testing
	 ***********************************************************************/
	/**
	 * copy of the alignment of the Points
	 */
	private int RecordedCurrentTrack = Constants.OutofCorrespondence;

	/**
	 * copy of the PadLocked state
	 */
	private boolean RecordedPadLock = false;

	/**
	 * copy of CurrentEdge
	 */
	private SecEdge RecordedPeer;

	/**
	 * constructs a SecEdge with only its Edge identifier.
	 *
	 * @param edge identifies the side of the GridTile the SecEdge is on.
	 * @param shared describes the shared edge, if it is not adjacent.
	 *    A null value means it is adjacent.
	 * @param points describes the routes which terminate on the PtsEdge.
	 */
	public PtsEdge(int edge, Edge shared, SwitchPoints points) {
		super(edge, shared);
		IOSpec decoder;
		MyPoints = points;
		NormalRoute = MyPoints.getNormal();
		PointsSearch = false;
		HasCommand = new boolean[MAX_ROUTES];
//		OccupancyCount = 0;
		if (points == null) { //they should always be not null.
			HasFieldLock = false;
		}
		else {
			// Though it may not be correct, start out with all fascia lock
			// indicators in the "locked" position.  It would be best to set
			// them to the current state, but the state cannot be read on some
			// systems (e.g. Loconet).
			if ( (decoder = MyPoints.getLockCmds(SwitchPoints.LOCKONCMD)) != null) {
				decoder.sendCommand();
			}

			HasFieldLock = (MyPoints.getLockCmds(SwitchPoints.UNLOCKREPORT) != null) ||
					(MyPoints.getLockCmds(SwitchPoints.LOCKREPORT) != null);
			
			// collects the JmriDevices used for moving points and registers the PtsEdge
			// with the IOSpec representing the decoder.  Registration goes through the IOSPec
			// rather than directly to the device so that Chains can register their components.
			for (int side = 0; side < MAX_ROUTES; ++side) {
				if ((decoder = MyPoints.getRouteCmd(side))!= null) {
					decoder.registerUser(new RouteReference(this, side));
					HasCommand[side] = !MyPoints.getSpur();
				}
				else {
					HasCommand[side] = false;
				}
			}

		}

		DefaultTrackEdge.addTrackEdge(this);
		DefaultTrackEdge.addSignalEdge(this);
		PtsKeeper.add(this);
	}

	@Override
	public EdgeInterface.EDGE_TYPE getEdgeType() {
		return EdgeInterface.EDGE_TYPE.POINTS_EDGE;
	}
	
	/*
	 * binds the edge to its mate.  It also converts the
	 * default speed to a real speed on each diverging route to "medium".
	 */
	public void bind() {
		super.bind();
		for (int trk = 0; trk < MyTracks.length; ++trk) {
			if (MyTracks[trk] != null) {
				if (trk != NormalRoute) {
					if (MyTracks[trk].getSpeed() == Track.DEFAULT) {
						MyTracks[trk].setSpeed(Track.MEDIUM);
					}
				}
			}
		}
	}
	
	@Override
	public void install(GridTile tile) {
		boolean sharing = false;
		super.install(tile);;
		if (MyPoints != null) {
			SharedDecoders = MyPoints.getSharedRoutes();
			// most decoders are not shared, so these can be pruned down
			for (int i = 0; i < MAX_ROUTES; ++i) {
				if ((SharedDecoders[i] != null) && (SharedDecoders[i].size() < 2)) {
					SharedDecoders[i] = null;
				}
				else {
					sharing = true;
				}
			}
			if (!sharing) {
				SharedDecoders = null;
			}
		}
		else {
			SharedDecoders = new ArrayList[MAX_ROUTES];
		}
		if (HasFieldLock) {
			LockSymbol = new LockFrill(MyEdge);
			LockSymbol.showLock(false);
			EdgeTile.addFrill(LockSymbol);			
		}
	}
	
	/**
	 * steps through the Tracks that have a termination on this SecEdge,
	 * telling them which Block they are members of.
	 */
	public void propagateBlock(Block block) {
		if ((block != null) && (block != MyBlock)) {
			MyBlock = block;
			for (int edge = 0; edge < MyTracks.length; ++edge) {
				if (MyTracks[edge] != null) {
					MyTracks[edge].setBlock(block);
				}
			}
			if (Joint != null) {
				Joint.propagateBlock(block);
			}
			block.registerTrackEdge(this);
			block.registerDetectionProcessor(this);
		}
	}

	/**
	 * remembers the Tracks that terminate on this SecEdge and returns true
	 * if the Track is the default route through the edge.
	 *
	 * @param trk is one of the Tracks terminating on the edge.
	 *
	 * @return true if the Track is on the Normal route.
	 */
	public boolean setDestination(Track trk) {
		FrogEdge internalEdge = new FrogEdge(this, trk.getTermination(this));
		boolean normal = false;
		int other = trk.getTermination(this);
		if (other >= 0) {
			MyTracks[other] = trk;
			MyTracks[other].replaceEdge(this, internalEdge);
			PtsLogic.setRouteLogic(other, (FrogVitalLogic) internalEdge.getVitalLogic());
			if ( (NormalRoute == other) || (Destination == null)) {
				normal = true;
				Destination = trk;
				CurrentTrk = other;
				Peer = NeighborEdge = internalEdge;
				internalEdge.NeighborEdge = this;
			}
		}
		return normal;
	}

	/**
	 * connects the SecEdge to its Track.  For a PtsEdge, this work is handled
	 * by setDestination; consequently, this method does nothing.
	 * @param trk is the Track that this is an edge on
	 */
	public void setTrack(Track trk) {
	}

	/**
	 * is invoked to retrieve the side the points are set to
	 * @return the index of the frog (grid side) the turnout is lined to
	 */
	public int getAlignment() {
		return CurrentTrk;
	}

	/**
	 * is used to determine if a SecEdge is a turnout
	 * @return true if it is either a spur or OS section and false if not
	 */
	public boolean isTurnout() {
		return true;
	}
	
	/**
	 * test if a frog is the Frog for the noraml route through the points
	 * @param edge is the FrogEdge being questioned
	 * @return true if it at the end of the normal route and false if it
	 * is a diverging route
	 */
	public boolean isNormalFrog(final FrogEdge edge) {
		return edge == MyFrogs[NormalRoute];
	}
	
	/**
	 * test if an Extended route can be set through the points.
	 * @param frog is the destination of the route through the points being tested
	 * @return true if either
	 * the points are lined for the requested route or it is not a spur. 
	 */
	public boolean isRoutable(final FrogEdge frog) {
		return ((CurrentTrk == frog.getIndex()) || !MyPoints.getSpur());
	}
	
	/**
	 * is called to determine if local control is allowed or not.  To be allowed,
	 * the local lock must be unlocked and
	 * 
	 * if CTC/DTC
	 * - have track and time or out of service
	 * - have no reservation
	 * - not be occupied without T&T or OOS
	 * The Block.determineState() method checks for T&T and OOS before
	 * occupancy, so indirectly results in this conditional behavior.
	 * 
	 * if ABS/APB
	 * - have no reservation
	 * - T&T or OOS is optional and not necessary
	 *
	 * For a Chain, this should be tested on each PtsEdge that could be affected
	 * by issuing the any of the commands in the Chain.
	 * 
	 * @return true if the local control is active or false if it is
	 * ignored because the position of the points are locked.
	 */
	protected boolean isLocalControlAllowed() {
		if (GuiLocks.intersection(EdgeLocks, GuiLocks.LocalSwitchBlocks).isEmpty() &&
				GuiLocks.intersection(CommonLocks, GuiLocks.CommonSwitchBlocks).isEmpty() &&
				GuiLocks.intersection(FrogEdgeLocks, GuiLocks.LocalSwitchBlocks).isEmpty()&&
				  GuiLocks.intersection(FrogCommonLocks, GuiLocks.CommonSwitchBlocks).isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * is a predicate for determining if the dispatcher can move the points.  To be
	 * allowed to move the points,
	 * - local control must be locked (if it exists),
	 * - no reservation,
	 * - no occupancy
	 * - no T&T or OOS
	 * 
	 * No distinction is made for ABS/APB Vs CTC/DTC because dispatcher control of
	 * ABS/APB is undefined.  CATS allows it, so will treat moving points similar
	 * to under CTC/DTC.
	 * 
	 * Though a spur can not be lined by the dispatcher, the dispatcher may lock/unlock
	 * the points.  The spur points may share a decoder address with OSEdge points.
	 * Consequently, this method must reside in PtsEdge, rather than OSEdge.
	 * 
	 * This test must be applied to every PtsEdge in a Chain that can be affected
	 * by commanding any decoder in the Chain.
	 * 
	 * @return true if the dispatcher can move the points
	 */
	protected boolean isDispatcherControlAllowed() {
//		if ((!HasFieldLock || FieldLocked) &&	// Either the file lock listener is undefined or is set to lock
//				(MyBlock.determineState() == Block.TRK_IDLE) &&	// handles TNT and OOS and occupancy
//				!MyPoints.isRouteLocked()) {	// route reservation check
		  if (GuiLocks.intersection(EdgeLocks, GuiLocks.EdgeSwitchBlocks).isEmpty() && 
				  GuiLocks.intersection(CommonLocks, GuiLocks.CommonSwitchBlocks).isEmpty() &&
				  GuiLocks.intersection(FrogEdgeLocks, GuiLocks.EdgeSwitchBlocks).isEmpty() &&
				  GuiLocks.intersection(FrogCommonLocks, GuiLocks.CommonSwitchBlocks).isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * this method tells all PtsEdges that share a decoder that a command is being sent to the
	 * layout to trigger the decoder and move the points.  Except when testing, the safety
	 * checks should be called prior to calling this method.
	 * @param trk is the track that the points will be moved to
	 */
	protected void prepareForMovement(final int trk) {
		changeLinkages(Constants.OutofCorrespondence);
		PtsLogic.setNextRoute(trk);
		if (!MyPoints.hasFeedback()) {
			PtsLogic.receiveFeedback(trk, true);
		}
	}
	
	/**
	 * is called once during initialization to prime the flow of state downstream.
	 */
	public void startFeeding() {
		if (PtsLogic != null) {
			PtsLogic.primeApproach();
			PtsLogic.setSelectedTrack(NormalRoute);
		}
	}

	/**
	 * is called to handle the details of moving the points.  If
	 * the current route is not OutOfCorrespondence, then it must
	 * be unlinked.  If the newRoute is not OutOfCorrespondence,
	 * then it must be linked in.
	 * @param newRoute is the index of the track that the points are now
	 * lined to.  It may be OutOfCorrespondence, meaning there is no
	 * lined route through these points (they may be in motion)
	 */
	protected void changeLinkages(int newRoute) {
		EnumSet<GuiLocks> transfer;
		if (newRoute != CurrentTrk) {
			if ((CurrentTrk != Constants.OutofCorrespondence) &&
					(Peer != null)) {
//				if (isLockSet(GuiLocks.ENTRYLOCK)) {
//					paintTrackSequence(RouteDecoration.CLEAR_ROUTE);
////					if (ActiveRoute != null) {
////						ActiveRoute.getRouteAnchor().trafficCleared();
////					}
//				}
//				else if (isLockSet(GuiLocks.EXITLOCK) && (ActiveRoute != null)) {
////					ActiveRoute.getRouteAnchor().trafficCleared();
//				}
				clearCommonLock(GuiLocks.ENTRYLOCK);
				clearCommonLock(GuiLocks.EXITLOCK);
				Peer.setJoint(null);  // this clears all the locks the Frog set
				((FrogEdge) Peer).NeighborEdge = null;
				((FrogEdge) Peer).setFrogEdgeLock(GuiLocks.CONFLICTINGSIGNALLOCK);
				transfer = GuiLocks.intersection(CommonLocks, GuiLocks.GUISwitchLocks);
				for (GuiLocks lock : transfer) {
					((FrogEdge)Peer).clearFrogCommonLock(lock);
				}
				transfer = GuiLocks.intersection(EdgeLocks, PTS_FILTER);
				for (GuiLocks lock : transfer) {
					((FrogEdge)Peer).clearFrogCommonLock(lock);
				}
//				((FrogEdge) Peer).clearFrogFilters(FrogFilters);
				LastTrk = CurrentTrk;
			}
			CurrentTrk = newRoute;
			if (newRoute == Constants.OutofCorrespondence) {
				Destination = null;
				Peer = null;
				NeighborEdge = null;
				setPointsEdgeLock(GuiLocks.CONFLICTINGSIGNALLOCK);
			}
			else {
				// the order on clearing the ConflictingSignalLock is critical -
				// the new FrogEdge may need to set it, again
				clearPointsEdgeLock(GuiLocks.CONFLICTINGSIGNALLOCK);
				Destination = MyTracks[CurrentTrk];
				NeighborEdge = (AbstractTrackEdge) (Peer = Destination.getEnd(MyEdge));
				Peer.setJoint(this);  // this sets all the locks from the Frog
				((FrogEdge)Peer).NeighborEdge = this;
				((FrogEdge)Peer).clearFrogEdgeLock(GuiLocks.CONFLICTINGSIGNALLOCK);
//				if (isLockSet(GuiLocks.ENTRYLOCK)) {
//					paintTrackSequence(RouteDecoration.SET_ROUTE);
//				}
				transfer = GuiLocks.intersection(CommonLocks, GuiLocks.GUISwitchLocks);
				for (GuiLocks lock : transfer) {
					((FrogEdge)Peer).clearFrogCommonLock(lock);
				}
				transfer = GuiLocks.intersection(EdgeLocks, PTS_FILTER);
				for (GuiLocks lock : transfer) {
					((FrogEdge)Peer).clearFrogCommonLock(lock);
				}
//				((FrogEdge) Peer).setFrogFilters(FrogFilters);
			}
			MyBlock.paintOccupancy();
			EdgeTile.requestUpdate();
		}
	}

	/**
	 * steps through the list of PtsEdges, setting them all to the 
	 * last known alignment.
	 */
	public static void restoreAlignment() {
		PtsEdge turnOut;
		for (Enumeration<PtsEdge> e = PtsKeeper.elements(); e.hasMoreElements(); ) {
			turnOut = e.nextElement();
			turnOut.MyPoints.restorePoints();
		}
	}


	/**
	 * steps through the list of PtsEdges, setting them all to a
	 * particular state.  The intent of this method is to test the turnouts
	 * on a layout.  If tested for throw, then closed, then
	 * if the layout is refreshed, CATS will know the positions
	 * of all turnouts.
	 * 
	 * @param state is true to throw them all or false to close
	 * them all.
	 */
	public static void testThrown(boolean state) {
		PtsEdge turnOut;
		for (Enumeration<PtsEdge> e = PtsKeeper.elements(); e.hasMoreElements(); ) {
			turnOut = e.nextElement();
			turnOut.testTurnout(state);
		}
	}

	/**
	 * positions all the turnout decoders controlled by the
	 * PtsEdge to a particular state.
	 * 
	 * @param state is the state being tested.  It is true
	 * for thrown and false for closed.
	 */
	public void testTurnout(boolean state) {
		if (!EdgeLocks.contains(GuiLocks.DETECTIONLOCK) && (MyPoints != null)) {
			MyPoints.testRoutes(state);
		}
	}

	/**
	 * positions all turnouts to the normal (straight thru) route.  If
	 * the turnout has feedback, stimulate the method that moves the
	 * points.  If it doesn't, then stimulate the method that moves
	 * the points and changes the graphics.
	 * <p>
	 * This does not move the points if the block is occupied
	 *
	 */
	public static void setAllNormal() {
		PtsEdge turnOut;
		for (Enumeration<PtsEdge> e = PtsKeeper.elements(); e.hasMoreElements(); ) {
			turnOut = e.nextElement();
			if (!turnOut.EdgeLocks.contains(GuiLocks.DETECTIONLOCK) && (turnOut.MyPoints != null)) {
				Log.warn(turnOut.toString() + " set to normal");
				if (turnOut.MyPoints.hasFeedback()) {
					turnOut.MyPoints.selectRoute(turnOut.NormalRoute);
				}
				else {
					turnOut.requestAlignment(turnOut.MyTracks[turnOut.NormalRoute]);

				}
			}
		}
	}

	/**
	 * steps through the list of PtsEdges, setting all the Lock lights.  The
	 * intent of this method is to initialize the lights on a layout that
	 * does not report state when the system is powered up or to test
	 * all the lights.
	 * <p>
	 * The actions here are a little confusing because there could be 2
	 * ways to turn off the existing light.  For example, to turn on
	 * the Lock light, the Unlock light should be turned off.  If the
	 * command to turn it off (UNLOCKOFF) is defined, it is sent.
	 * Alternatively, if the UNLOCKON has a cancel operation, it is sent.
	 * @param locked is true if the light is to show "locked"; false, if
	 * it is to show "unlocked".
	 */
	public static void testLockLights(boolean locked) {
		PtsEdge turnOut;
		IOSpec decoder;
		for (Enumeration<PtsEdge> e = PtsKeeper.elements(); e.hasMoreElements(); ) {
			turnOut = e.nextElement();
			if (locked) {
				if ( (decoder = turnOut.MyPoints.getLockCmds(SwitchPoints.UNLOCKONCMD)) != null) {
					decoder.sendUndoCommand();
				}
				if ( (decoder = turnOut.MyPoints.getLockCmds(SwitchPoints.LOCKONCMD)) != null) {
					decoder.sendCommand();
				}
			}
			else {
				if ( (decoder = turnOut.MyPoints.getLockCmds(SwitchPoints.LOCKONCMD)) != null) {
					decoder.sendUndoCommand();
				}
				if ( (decoder = turnOut.MyPoints.getLockCmds(SwitchPoints.UNLOCKONCMD)) != null) {
					decoder.sendCommand();
				}
			}
		}
	}

//	/**
//	 * steps through all the PtsEdges and sets their current linkages to the normal position.
//	 * This does not send commands to the layout.
//	 */
//	public static void initializePoints() {
//		PtsEdge turnOut;
//		for (Enumeration<PtsEdge> e = PtsKeeper.elements(); e.hasMoreElements(); ) {
//			turnOut = e.nextElement();
//			turnOut.PtsLogic.setSelectedTrack(turnOut.NormalRoute);
//		}
//	}
	/**
	 * is invoked to change what the neighbor SecEdge is.  This is used by Points
	 * to change the fouling and non-fouling track.
	 * @param newJoint is the neighbor, for forwarding state in the opposite
	 * direction.  If null, then the track is fouling
	 */
	public void setJoint(SecEdge newJoint) {
		super.setJoint(newJoint);
		//      MyTracks[NormalRoute].getEnd(MyEdge).Joint = newJoint;
		for (int trk = 0; trk < MyTracks.length; ++trk) {
			if (MyTracks[trk] != null) {
				//				if (trk == NormalRoute) {
				//					MyTracks[trk].getEnd(MyEdge).setJoint(this);
				//				}
				//				else {
				//					MyTracks[trk].getEnd(MyEdge).setJoint(null);
				//				}
				MyFrogs[trk] = (FrogEdge) MyTracks[trk].getEnd(MyEdge);
				MyFrogs[trk].setJoint((trk == NormalRoute) ? this : null);
			}
		}
	}

	@Override
	public Element dumpDerivativeState(boolean basic) {
		Element state = new Element("PtsEdge");
		boolean changed = false;
		if (CurrentTrk != RecordedCurrentTrack) {
			state.setAttribute("ALIGNED", String.valueOf(CurrentTrk));
			RecordedCurrentTrack = CurrentTrk;
			changed = true;
		}
		if (Peer != RecordedPeer) {
			if (Peer == null) {
				state.setAttribute("EDGE", "None");
			}
			else {
				state.setAttribute("EDGE", String.valueOf(Peer.getEdge()));
			}
			RecordedPeer = Peer;
			changed = true;
		}
		if (EdgeLocks.contains(GuiLocks.SWITCHUNLOCK) != RecordedPadLock) {
			state.setAttribute("LOCK", String.valueOf(EdgeLocks.contains(GuiLocks.SWITCHUNLOCK)));
			RecordedPadLock = EdgeLocks.contains(GuiLocks.SWITCHUNLOCK);
			changed = true;
		}
		return (changed) ? state : null;
	}

//	/**
//	 * is called to set dispatcher control in the VitalLogic through the code line.  Because no acknowledge
//	 * is expected from the VitalLoigc, the internal lock is cleared as well.
//	 */
//	public void setDispatcherControl() {
//		CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.MAINTENANCELOCK, Constants.FALSE);
//		EdgeLocks.remove(GuiLocks.LOCALCONTROLLOCK);
//	}
//
//	/**
//	 * is called to remove dispatcher control in the VitalLogic through the code line.  Because no acknowledge
//	 * is expected from the VitalLoigc, the internal lock is set as well.
//	 */
//	public void removeDispatcherControl() {
//		CodeLineEntry.createPacket(CodePurpose.REQUEST, Codes.MAINTENANCELOCK, Constants.TRUE);
//		EdgeLocks.add(GuiLocks.LOCALCONTROLLOCK);
//	}
//	
//	/**
//	 * is a method for propagating the AbstractTrackEdge cleanRoute() operation
//	 * through the frogs on a PtsEdge.
//	 */
//	public void clearFrogLocks() {
//		for (int i = 0; i < MyFrogs.length; ++i) {
//			if (MyFrogs[i] != null) {
//				MyFrogs[i].cleanRoute();
//			}
//		}
//	}

	@Override
	public void resetLocks() {
		super.resetLocks();
		if (FrogCommonLocks.contains(GuiLocks.ENTRYLOCK)) {
			clearPointsCommonLock(GuiLocks.ENTRYLOCK);
		}
		if (CommonLocks.contains(GuiLocks.EXITLOCK)) {
			clearPointsCommonLock(GuiLocks.EXITLOCK);
		}
	}
	
	/**
	 * sets the expected route field for propagating the PREPARATION lock.
	 * @param nextTrk is the proposed turnout alignment
	 */
	public void setNextTrak(final int nextTrk) {
		ExtendedRouteTrk = nextTrk;
	}
	
	/**************************************************************************
	 * the implementation of TrackEdge
	 **************************************************************************/
	@Override
	public BasicVitalLogic createVitalLogic() {
		PtsLogic = new PtsVitalLogic(new EdgePacketFactory(this), toString());
//		SignalLogic = new ProxySignal(new PointsCircuitRelay());
//		PtsLogic.setSignalHead(SignalLogic);
		if (MyPoints != null) { //they should always be not null.
			PtsLogic.setPoints(MyPoints);

			// Though it may not be correct, start out with all fascia lock
			// indicators in the "locked" position.  It would be best to set
			// them to the current state, but the state cannot be read on some
			// systems (e.g. Loconet).
//			setHandlers();
		}
		return PtsLogic;
	}

	/**
	 * The non-points version of this method tries to fill in three things:
	 * 1. protected speed (not needed for points because the frogs acquire the speed)
	 * 2. sets the "peer" for this vital logic  (not needed because the frogs acquire
	 * the "peer" in their probe)
	 * 3. tells the advance edge that this is the approach edge.  The frogs also do that.
	 * 
	 * However, there is one case not covered - points facing each other.  Thus, this method
	 * looks for that case and sets the approach edge. 
	 */
	@Override
	public void discoverAdvanceVitalLogic() {
		SecEdge nextEdge = getNeighbor();
//		SecEdge opposite = null;
		int speed = Track.NORMAL;
		PtsLogic.setIdentity(toString());
		PtsLogic.tailorDiscipline(MyBlock.getDiscipline());
//		while (nextEdge != null) {
//			if (nextEdge instanceof TrackEdge) {
//				((TrackEdge) nextEdge).setApproachEdge(this);
//				if (nextEdge instanceof PtsEdge) {
//					OppositeEnd = (AbstractTrackEdge) nextEdge;
//				}
//				else {
//					OppositeEnd = (AbstractTrackEdge) opposite;
//				}
//				nextEdge = null;
//			}
//			else {
//				speed = Track.getSlower(speed, nextEdge.Destination.getSpeed());
//				TrackSpan.add(nextEdge.Destination);
//				opposite = nextEdge.Peer;
//				if (opposite != null) {
//					nextEdge = opposite.getNeighbor();
//				}
//			}
//		}
//		CodeLineEntry = new VitalPacketFactory(PtsLogic);
//		PtsLogic.setSpeed(speed);
////		PtsLogic.primeApproach();  // this is two early
		if (nextEdge == null) {
			OppositeEnd = null;
		}
		else if( nextEdge instanceof PtsEdge) {
			((PtsEdge) nextEdge).setApproachEdge(this);
			OppositeEnd = (PtsEdge) nextEdge;
		}
		else {
			speed = trackSegmentAggregator(nextEdge);
		}
		CodeLineEntry = new VitalPacketFactory(PtsLogic);
		PtsLogic.setSpeed(speed);
	}

	@Override
	public void setApproachEdge(AbstractTrackEdge approach) {
//		if (approach == null) {
//			System.out.println("Setting points approach edge for " + toString() + "to null");
//		}
//		else {
//			System.out.println("Setting points approach edge for " + toString() + " to " + approach.toString());
//		}
		ApproachEdge = approach;
		if (ApproachEdge != null) {
			PtsLogic.setApproachLogic(ApproachEdge.getVitalLogic());
		}
	}

	@Override
	public SignalEdge signalEdgeProbe() {
		return this;
	}

	@Override
	public TrackEdge nextTrackEdge() {
		return this;
	}

	/**
	 * retrieves the VitalLogic associated with the TrackEdge
	 * @return the VitalLogic
	 */
	public BasicVitalLogic getVitalLogic() {
		return PtsLogic;
	}

	@Override
	public boolean isBlkEdge() {
		return false;
	}

//	@Override
//	public void setDirection(DirectionOfTravel dot) {
//		// this should never be called for a PtsEdge
//	}

	@Override
	public boolean isLockSet(GuiLocks lock) {
//		return CommonLocks.contains(lock) || EdgeLocks.contains(lock) || FrogCommonLocks.contains(lock) || FrogEdgeLocks.contains(lock);
		return EdgeLocks.contains(lock) || FrogEdgeLocks.contains(lock);
	}

	/**
	 * this method receives messages (responses and reports) from the field
	 * equipment and processes them
	 * @param response is the message from the field equipment
	 */
	public CodeMessage processFieldResponse(String response) {
		CodeMessage received = new CodeMessage(response);
		int track;
		/******************** DEBUG code *****************************/
		if (Crandic.Details.get(DebugBits.CODELINEBIT)) {
			System.out.println(toString() + " office received " + response);
		}
		/*************************************************************/
		if (received.Purpose.equals(CodePurpose.INDICATION)) {
			if (received.Lock == Codes.SWITCHPOSITION) {
				track = Integer.parseInt(received.Value);
				if (track == Constants.OutofCorrespondence) {
					changeLinkages(Constants.OutofCorrespondence);
				}
				else if ((track > Constants.OutofCorrespondence) && (track < MAX_ROUTES)
						&& (MyTracks[track] != null) && (CurrentTrk != track)) {
					if (CurrentTrk != Constants.OutofCorrespondence) {
						changeLinkages(Constants.OutofCorrespondence);
					}
					changeLinkages(track);
					MySection.setSelTrack(MyTracks[track]);
				}
			}
			else if (received.Lock == Codes.SWITCHUNLOCK) {
				if (Boolean.parseBoolean(received.Value)) {
					SwitchUnlockIndication.requestLockSet();
				}
				else {
					SwitchUnlockIndication.requestLockClear();
				}
			}
		} else if (received.Purpose.equals(CodePurpose.DENY)) {
			changeLinkages(LastTrk);
			JOptionPane.showMessageDialog((Component) null, "Request to move points is denied",
					"Denied Action", JOptionPane.INFORMATION_MESSAGE);

		}
		return received;
	}
	/**************************************************************************
	 *  end of TrackEdge implementation
	 **************************************************************************/

	/*****************************************************************************************
	 * Implementation of SignalEdge
	 *****************************************************************************************/
	@Override
	public void discoverApproachSignal() {
//		SignalEdge approachSignal;
//		if (ApproachEdge != null) {
//			approachSignal = ApproachEdge.signalEdgeProbe();
//			if (approachSignal != null) {
//				PtsLogic.setApproachSignal(approachSignal.getSignalLogic());
//			}
//		}
		if (ApproachEdge != null) {
			if (ApproachEdge.signalEdgeProbe() != null) {
				PtsLogic.connectTrackCircuits();
			}
		}
	}

//	@Override
//	public SignalVitalLogic getSignalLogic() {
//		return SignalLogic;
//	}

	/*****************************************************************************************
	 * End of SignalEdge implementation
	 *****************************************************************************************/
	/*****************************************************************************************
	 * The methods that implement RouteEdge
	 *****************************************************************************************/ 
	/**
	 * Jumps across the points to the current frog
	 * @return the Frog currently lined
	 */
	public RouteEdge startFrogSearch() {
		SearchStatus = RouteSearchStatus.SEARCHING;
		PointsSearch = true;
		if (CurrentTrk == Constants.OutofCorrespondence) {
			return null;
		}
		if (isRoutable(MyFrogs[CurrentTrk])) {
			return MyFrogs[CurrentTrk];
		}
		return null;
	}
	
	/**
	 * This method is an analog to AbstractTrackEdge.getNextRoute().  Whereas the super method
	 * is used in the frog->points search direction, this method is used in the points->frog
	 * search direction.  The frog can be selected.
	 * 
	 * @return the requested FrogEdge as a RouteEdge
	 */
	public RouteEdge getNextRouteEdge() {
		if (MyPoints.getSpur()) {
			return (CurrentTrk == NormalRoute) ? MyFrogs[NormalRoute] : null;
		}
		return (CurrentTrk == Constants.OutofCorrespondence) ? null : MyFrogs[SearchFrog];
	}
	
	/**
	 * simulates point movement to a specific frog.
	 * @param frog is the desired frog.  -1 means the current
	 * alignment (which could be OutofCorrespondence
	 */
	public void forceSearchFrog(final int frog) {	
		SearchFrog = (frog == Constants.OutofCorrespondence) ? CurrentTrk : frog;
	}
	
//	/**
//	 * clears the occupancy counter when an extended route is being constructed.
//	 * The algorithm is that the route under construction will clear the counter,
//	 * wait awhile, then check it.  If the counter is non-zero, then it might not
//	 * safe to move the points, so it will clear the counter and wait again.  If the
//	 * counter is 0 after clearing, then the route will move the points.
//	 */
//	public void clearOccupancyCounter() {
//		OccupancyCount = (EdgeLocks.contains(GuiLocks.DETECTIONLOCK)) ? 1 : 0;
//	}
//	
//	/**
//	 * is an indicator of train movement across the points.  If the points have not been
//	 * occupied since the last time OccupiedCount was zeroed, then the code assumes
//	 * the points can be moved without derailing a train.
//	 * @return
//	 */
//	public boolean safeToMove() {
//		return OccupancyCount == 0;
//	}
	
	/**
	 * retrieves the frogs so that a RouteCreator can walk through them
	 * @return the array of FrogEdges
	 */
	public FrogEdge[] getFrogs() {
		return MyFrogs;
	}
	
	@Override
	public RouteNode createNode(final RouteNode nextNode, final int searchID) {
		if ((SearchNode == null) || (SearchNode.getRouteNumber() != searchID)) {
//			SearchNode = new RouteNodePts(this, RouteSearchID, nextNode);
			SearchNode = RouteNodePts.newNode(this, searchID, nextNode);
			RouteSearchID = searchID;
		}
		return SearchNode;
	}
	
	/**
	 * adds a RouteNode for a frog to a points node
	 * @param frogNode is the RouteNode for the frog
	 * @param searchID is the search identifier, used to distinguish search iterations
	 * @param path is its frog position
	 */
	public void setFrogRouteNode(final RouteNode frogNode, final int searchID, final int path) {
		((RouteNodePts) createNode(null, searchID)).setFrogNode(frogNode, path);
	}
	
	/**
	 * is a query for determining which direction the search entered the PtsEdge.
	 * @return true if it came from the points direction and false if it came from the
	 * frog direction.
	 */
	public boolean isSearchFromPoints() {
		return PointsSearch;
	}
	
	/**
	 * method to mark entry into points from a FrogEdge
	 */
	public void resetSearchFlag() {
		PointsSearch = false;
	}
	
	@Override
	public void releaseNode(RouteNode node) {
		super.releaseNode(node);
		PointsSearch = false;
	}
	
	@Override
	protected boolean extendedIsEdgeBlocked(final EnumSet<GuiLocks> blockingConditions) {
		return !isDispatcherControlAllowed();
//		if (super.extendedIsEdgeBlocked(blockingConditions)) {
//			return true;
//		}
//		if (!GuiLocks.intersection(GuiLocks.CommonBlock, FrogCommonLocks).isEmpty()) {
//			if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
//				System.out.println("Edge " + toString() + " blocking on edge CommonLocks");
//			}
//			return true;
//		}
//		return false;
	}
	
	@Override
	public void exitClearRoute() {
		resetLocks();
		// This is one of many possible ways of continuing
		for (int frog = 0; frog < MAX_ROUTES; ++frog) {
			if (MyFrogs[frog] != null) {
				MyFrogs[frog].entryClearRoute();
			}
		}
	}

	@Override
	public void entryPattern() {
		System.out.println("PtsEdge Entry: " + toString());
		super.entryPattern();
	}

	@Override
	public void exitPattern() {
		System.out.println("PtsEdge Exit: " + toString());
		// This is one of many possible ways of continuing
		for (int frog = 0; frog < MAX_ROUTES; ++frog) {
			if (MyFrogs[frog] != null) {
				MyFrogs[frog].entryPattern();
			}
		}
	}

//	@Override
//	public boolean isRouteSegmentBlocked(final EnumSet<GuiLocks> blockingConditions) {
//		if (CurrentTrk == Constants.OutofCorrespondence) {
//			return true;
//		}
//		return ((AbstractTrackEdge) MyFrogs[CurrentTrk]).isRouteSegmentBlocked(blockingConditions);
//	}
//
//	/**
//	 * a continuation of findAdvanceRouteEdge() when entering a turnout
//	 * from the points side.  Because the CurrentTrk route index changes, all
//	 * this routine does is jump to the current frog and pick up findAdvanceRouteEdge.
//	 * @return the next RouteNode in advance of the current frog
//	 */
//	public void findAdvanceFrogEdge() {
//		if (CurrentTrk != Constants.OutofCorrespondence) {
//			MyFrogs[CurrentTrk].findAdvanceRouteEdge();
//		}
//	}
	/*****************************************************************************************
	 * end of the RouteEdge implementation
	 *****************************************************************************************/

//	@Override
//	protected void setFilters(EnumSet<LogicLocks> filters) {
//		if (Peer != null) {
//			((FrogEdge)Peer).setFrogFilters(filters);
//		}
//		FrogFilters.addAll(filters);
//	}
//
//	@Override
//	protected void clearFilters(EnumSet<LogicLocks> filters) {
//		if (Peer != null) {
//			((FrogEdge)Peer).clearFrogFilters(filters);
//		}
//		FrogFilters.removeAll(filters);
//	}

//	/**
//	 * is the counterpart to setFilters, but for handling
//	 * a filter received through the frog
//	 * @param filters is the set of locks that are blocked
//	 */
//	protected void setPtsFilters(EnumSet<LogicLocks> filters) {
//		if (ApproachEdge != null) {
//			ApproachEdge.setFilters(filters);
//		}			
//	}

//	/**
//	 * is the counterpart to clearFilters, but for handling
//	 * a filter received through the frog
//	 * @param filters is the set of locks that are blocked
//	 */
//	protected void clearPtsFilters(EnumSet<LogicLocks> filters) {
//		if (ApproachEdge != null) {
//			ApproachEdge.clearFilters(filters);
//		}			
//	}

//	@Override
//	public void setEdgeLock(GuiLocks lock) {
//		LockIndicationHandler handler;
//		if (!EdgeLocks.contains(lock)) {
//			EdgeLocks.add(lock);
//			if ((handler = lockToHandler(lock)) != null) {
//				handler.requestLockSet();
//			}			
//			if (Peer != null) {
//				((FrogEdge)Peer).setFrogEdgeLock(lock);
//			}
//		}		
//	}
//
//	@Override
//	public void clearEdgeLock(GuiLocks lock) {
//		LockIndicationHandler handler;
//		if (EdgeLocks.contains(lock)) {
//			EdgeLocks.remove(lock);
//			if ((handler = lockToHandler(lock)) != null) {
//				handler.requestLockClear();
//			}			
//			if (Peer != null) {
//				((FrogEdge)Peer).clearFrogEdgeLock(lock);
//			}
//		}		
//	}
//
//	@Override
//	public void setCommonLock(GuiLocks lock) {
//		LockIndicationHandler handler;
//		if (!CommonLocks.contains(lock)) {
//			CommonLocks.add(lock);
//			if ((handler = lockToHandler(lock)) != null) {
//				handler.advanceLockSet();
//			}			
//			if (Peer != null) {
//				((FrogEdge)Peer).setFrogCommonLock(lock);
//			}
//		}		
//	}
//
//	@Override
//	public void clearCommonLock(GuiLocks lock) {
//		LockIndicationHandler handler;
//		if (CommonLocks.contains(lock)) {
//			CommonLocks.remove(lock);
//			if ((handler = lockToHandler(lock)) != null) {
//				handler.advanceLockClear();
//			}			
//			if (Peer != null) {
//				((FrogEdge)Peer).clearFrogCommonLock(lock);
//			}
//		}		
//	}

	/**
	 * is the counterpart to setCommonLock(), as invoked
	 * from the Frog edge
	 * @param lock is the lock being set
	 */
	protected void setPointsCommonLock(GuiLocks lock) {
		if (!FrogCommonLocks.contains(lock) && !FrogEdgeLocks.contains(lock)) {
			if (ApproachEdge != null) {
				ApproachEdge.setCommonLock(lock);
			}			
			EnumSet<GuiLocks> oldLocks = FrogCommonLocks.clone();
			if (lock.equals(GuiLocks.EXITLOCK)) {
				paintTrackSequence(RouteDecoration.SET_ROUTE);
			}
			FrogCommonLocks.add(lock);
			/******************  DEBUG code ******************************/
			if (Crandic.Details.get(DebugBits.GUILOCKBIT)) {
				System.out.println("Points Edge " + toString() + "  frog common locks were " + oldLocks + " now " + FrogCommonLocks.toString());			
			}
			/****************** end DEBUG code ******************************/
		}
	}

	/**
	 * is the counterpart to clearCommonLock(), as invoked
	 * from the Frog edge
	 * @param lock is the lock being set
	 */
	protected void clearPointsCommonLock(GuiLocks lock) {
		if (FrogCommonLocks.contains(lock) && !FrogEdgeLocks.contains(lock)) {
			if (ApproachEdge != null) {
				ApproachEdge.clearCommonLock(lock);
			}			
			EnumSet<GuiLocks> oldLocks = FrogCommonLocks.clone();
			if (lock.equals(GuiLocks.EXITLOCK)) {
				paintTrackSequence(RouteDecoration.CLEAR_ROUTE);
			}
			FrogCommonLocks.remove(lock);
			/******************  DEBUG code ******************************/
			if (Crandic.Details.get(DebugBits.GUILOCKBIT)) {
				System.out.println("Points Edge " + toString() + " frog common locks were " + oldLocks + " now " + FrogCommonLocks.toString());			
			}
			/****************** end DEBUG code ******************************/
		}
	}

	/**
	 * is the counterpart to setEdgeLock(), as invoked
	 * from the Frog edge.  Note that operations on EdgeLocks are converted to operations
	 * on CommonLocks - to distinguish points changes from frog changes.
	 * @param lock is the lock being set
	 */
	protected void setPointsEdgeLock(GuiLocks lock) {
		if (!FrogCommonLocks.contains(lock) && !FrogEdgeLocks.contains(lock)) {
			if (ApproachEdge != null) {
				ApproachEdge.setCommonLock(lock);
			}			
			EnumSet<GuiLocks> oldLocks = FrogEdgeLocks.clone();
			if (lock.equals(GuiLocks.EXITLOCK)) {
				paintTrackSequence(RouteDecoration.SET_ROUTE);
			}
			FrogEdgeLocks.add(lock);
			/******************  DEBUG code ******************************/
			if (Crandic.Details.get(DebugBits.GUILOCKBIT)) {
				System.out.println("Points Edge " + toString() + " frog edge locks were " + oldLocks + " now " + FrogEdgeLocks.toString());			
			}
			/****************** end DEBUG code ******************************/
		}
	}

	/**
	 * is the counterpart to clearEdgeLock(), as invoked
	 * from the Frog edge.  Note that operations on EdgeLocks are converted to operations
	 * on CommonLocks - to distinguish points changes from frog changes.
	 * @param lock is the lock being set
	 */
	protected void clearPointsEdgeLock(GuiLocks lock) {
		if (!FrogCommonLocks.contains(lock) && FrogEdgeLocks.contains(lock)) {
			if (ApproachEdge != null) {
				ApproachEdge.clearCommonLock(lock);
			}			
			EnumSet<GuiLocks> oldLocks = FrogEdgeLocks.clone();
			if (lock.equals(GuiLocks.EXITLOCK)) {
				paintTrackSequence(RouteDecoration.CLEAR_ROUTE);
			}
			FrogEdgeLocks.remove(lock);
			/******************  DEBUG code ******************************/
			if (Crandic.Details.get(DebugBits.GUILOCKBIT)) {
				System.out.println("Points Edge " + toString() + " frog edge locks were " + oldLocks + " now " + FrogEdgeLocks.toString());			
			}
			/****************** end DEBUG code ******************************/
		}
	}


	@Override
	public void setHandlers () {
//		FrogLockHandlers.put(GuiLocks.EXITLOCK, new FrogExitTraffic());
//		DetectionIndication = new PtsDecorator(new DetectionLockIndicationHandler());
		/**
		 * The GuiLocks are:
		 * DETECTIONLOCK
		 * CONFLICTINGSIGNALLOCK
		 * SWITCHUNLOCK
		 * APPROACHLOCK
		 * TIMELOCK
		 * TRAFFICLOCK
		 * ENTRYLOCK
		 * EXITLOCK
		 * LOCALCONTROLLOCK
		 * CALLONLOCK
		 * WARRANTLOCK
		 * FLEET
		 * CROSSING
		 * PREPARATION
		 * EXTERNALLOCK
		 **/
//		DetectionIndication = new PtsDetectionDecorator(
//						new DetectionDecorator(
//								new BasicLockIndicationHandler(GuiLocks.DETECTIONLOCK)));
		DetectionIndication = new DetectionDecorator(
				new CommonLocksDecorator(
						new PtsDecorator(
								new BasicLockIndicationHandler(GuiLocks.DETECTIONLOCK))));
		ConflictingSignalIndication = new MergeDecorator(
				new PtsDecorator(
						new BasicLockIndicationHandler(GuiLocks.CONFLICTINGSIGNALLOCK)));
		SwitchUnlockIndication = new FieldLockDecorator(
				new MergeDecorator(
						new SwitchDecorator(
								new PtsDecorator(
										new BasicLockIndicationHandler(GuiLocks.SWITCHUNLOCK)))));
		// APPROACHLOCK should never be set or cleared
		// TIMELOCK should never be set or cleared
		// TRAFFICLOCK should never be set or cleared
		EntryTrafficIndication = new MergeDecorator(
				new PtsDecorator(
						new BasicLockIndicationHandler(GuiLocks.ENTRYLOCK)));
		ExitTrafficIndication = new MergeDecorator(
				new PtsDecorator(
						new BasicLockIndicationHandler(GuiLocks.EXITLOCK)));
		LocalControlLock = new CodeLineDecorator(
				new MergeDecorator(
						new PtsDecorator(
								new BasicLockIndicationHandler(GuiLocks.LOCALCONTROLLOCK))), Codes.MAINTENANCELOCK);
		// CALLONLOCK
		CallOnIndication = new MergeDecorator(
				new PtsDecorator(
						new BasicLockIndicationHandler(GuiLocks.CALLONLOCK)));
		// FLEETLOCK should never be set or cleared
		PreparationIndication = new ForwardDecorator(
						new BasicLockIndicationHandler(GuiLocks.PREPARATION));
		// CROSSINGLOCK should never be set or cleared
		ExternalIndication = new MergeDecorator(
				new PtsDecorator(
						new BasicLockIndicationHandler(GuiLocks.EXTERNALLOCK)));
}

	/*****************************************************************************************
	 * Lock Indication Handlers for Points Edges
	 *****************************************************************************************/
	/**************************************************************************
	 * PtsDecorator - this is a decorator on a Lock Handler to
	 * forward locks from in advance of the points to the Frog.  The lock
	 * is forwarded to the same Map as the request.
	 **************************************************************************/
	protected class PtsDecorator extends AbstractHandlerDecorator {

		/**
		 * the ctor
		 * @param lock is the LogicLocks element handled
		 */
		public PtsDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {			
			Handler.requestLockSet();
			if (Peer != null) {
				((FrogEdge) Peer).setFrogCommonLock(WatchedLock);
			}
		}

		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
			if (Peer != null) {
				((FrogEdge) Peer).clearFrogCommonLock(WatchedLock);
			}
		}

		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
			if (Peer != null) {
				((FrogEdge) Peer).setFrogCommonLock(WatchedLock);
			}
		}

		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
			if (Peer != null) {
				((FrogEdge) Peer).clearFrogCommonLock(WatchedLock);
			}
		}
	}
	/***************************************************************************
	 * end of PtsDecorator
	 **************************************************************************/
//	/**************************************************************************
//	 * PtsDetectionDecorator - it processes the side effects of a
//	 * block reporting occupied and unoccupied.  For a PtsEdge, it also monitors
//	 * occupancy to avoid throwing points under a train during route stacking.
//	 * 
//	 * It is derived from PtsDecorator because the EdgeLocks do not propagate to
//	 * the opposite ends of the Frog (the opposite end is in the same block).
//	 * Allowing the propagation will affect the CommonEdge flag (due to the frog)
//	 * and the EdgeLocks flag (due to the block).
//	 **************************************************************************/
//	protected class PtsDetectionDecorator extends PtsDecorator {
//
//		/**
//		 * the ctor
//		 */
//		public PtsDetectionDecorator(LockIndicationHandler handler) {
//			super(handler);
//		}
//
//		@Override
//		public void requestLockSet() {
//			Handler.requestLockSet();
//			if (RouteStack != null) {
//				((RouteNodePts) RouteStack).setOccupancy();
//			}
//			++OccupancyCount;
//		}
//		
//		@Override
//		public void requestLockClear() {
//			Handler.requestLockClear();
//			if (RouteStack != null) {
//				((RouteNodePts) RouteStack).clearOccupancy();
//			}
//		}
//		
//		@Override
//		public void advanceLockSet() {
//			super.advanceLockSet();
//			if (RouteStack != null) {
//				((RouteNodePts) RouteStack).setOccupancy();
//			}
//		}
//		
//		@Override
//		public void advanceLockClear() {
//			super.advanceLockClear();
//			if (RouteStack != null) {
//				((RouteNodePts) RouteStack).clearOccupancy();
//			}
//		}
//	}
//	/***************************************************************************
//	 * end of PtsDetectionDecorator
//	 **************************************************************************/
	
	/**************************************************************************
	 * SwitchDecorator - this is a decorator on a Lock Handler to account for local
	 * locks, primarily SWITCHUNLOCK.  It sets the EdgeLock in all the Frogs directly.
	 * 
	 **************************************************************************/
	protected class SwitchDecorator extends AbstractHandlerDecorator {

		/**
		 * the ctor
		 * @param lock is the LogicLocks element handled
		 */
		public SwitchDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {			
			Handler.requestLockSet();
			for (int i = 0; i < MyFrogs.length; ++i) {
				if (MyFrogs[i] != null) {
					MyFrogs[i].setEdgeLock(WatchedLock);
				}
			}
		}

		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
			for (int i = 0; i < MyFrogs.length; ++i) {
				if (MyFrogs[i] != null) {
					MyFrogs[i].clearEdgeLock(WatchedLock);
				}
			}
		}

		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
		}

		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
		}
	}
	/***************************************************************************
	 * end of SwitchDecorator
	 **************************************************************************/
	/**************************************************************************
	 * ForwardDecorator - this is a decorator on a Lock Handler to
	 * forward locks that change Lock maps.  CommonLocks are generated by the Opposite
	 * end TrackEdge; thus are forwarded through all Frogs as Edge Locks.  EdgeLocks
	 * are generated by the Frog; thus, are forwarded to the Opposite end as CommonLocks
	 * 
	 * This is intended primarily for PREPARATION locks.  They propagate through
	 * points in motion, to the intended route.
	 **************************************************************************/
	protected class ForwardDecorator extends EdgeLocksDecorator {		

		/**
		 * the ctor
		 * @param lock is the LogicLocks element handled
		 */
		public ForwardDecorator(LockIndicationHandler handler) {
			super(handler);
		}
		
		@Override
		public void requestLockSet() {
			Handler.requestLockSet();
			if (ExtendedRouteTrk != Constants.OutofCorrespondence) {
				MyFrogs[ExtendedRouteTrk].setFrogEdgeLock(WatchedLock);
			}
		}

		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
			if (ExtendedRouteTrk != Constants.OutofCorrespondence) {
				MyFrogs[ExtendedRouteTrk].clearFrogEdgeLock(WatchedLock);
				ExtendedRouteTrk = Constants.OutofCorrespondence;
			}
		}
		
		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
			if (ExtendedRouteTrk != Constants.OutofCorrespondence) {
				MyFrogs[ExtendedRouteTrk].setFrogCommonLock(WatchedLock);
			}
		}

		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
			if (ExtendedRouteTrk != Constants.OutofCorrespondence) {
				MyFrogs[ExtendedRouteTrk].clearFrogCommonLock(WatchedLock);
				ExtendedRouteTrk = Constants.OutofCorrespondence;
			}
		}
	}
	/***************************************************************************
	 * end of ForwardDecorator
	 **************************************************************************/
//	/**************************************************************************
//	 * ReverseDecorator - this is a decorator on a Lock Handler to
//	 * forward locks that change maps.  EdgeLocks are generated by a Frog
//	 * TrackEdge; thus are forwarded to the Opposite End as an Common Lock.  
//	 **************************************************************************/
//	protected class ReverseDecorator extends CommonLocksDecorator {
//
//		/**
//		 * the ctor
//		 * @param lock is the LogicLocks element handled
//		 */
//		public ReverseDecorator(LockIndicationHandler handler) {
//			super(handler);
//		}
//
//		@Override
//		public void advanceLockSet() {
//			Handler.advanceLockSet();
//			if (OppositeEnd != null) {
//				OppositeEnd.setEdgeLock(WatchedLock);
//			}
//		}
//
//		@Override
//		public void advanceLockClear() {
//			Handler.advanceLockClear();
//			if (OppositeEnd != null) {
//				OppositeEnd.clearEdgeLock(WatchedLock);
//			}
//		}
//	}
//	/***************************************************************************
//	 * end of ReverseDecorator
//	 **************************************************************************/
	/**************************************************************************
	 * FieldLockDecorator - this is a decorator on a Lock Handler to account for SWITCHUNLOCK,
	 * the physical lock controlled from the field.
	 * 
	 **************************************************************************/
	private class FieldLockDecorator extends AbstractHandlerDecorator {

		/**
		 * the ctor
		 */
		public FieldLockDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {
			LockSymbol.showLock(true);
			FieldLocked = true;
			Handler.requestLockSet();
		}
		
		@Override
		public void requestLockClear() {
			LockSymbol.showLock(false);
			FieldLocked = false;
			Handler.requestLockClear();
		}
		
		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
		}
		
		@Override
		public void advanceLockClear()
		{
			Handler.advanceLockClear();
		}
	}
	/***************************************************************************
	 * end of FieldLockDecorator
	 **************************************************************************/
	
	/**************************************************************************
	 * CodeLineDecorator - this is a decorator on a Lock Handler that generates
	 * code to the vital logic. methods send code only when the Edge lock is
	 * changed.  Request to change the Common Lock just passes on the operation.
	 **************************************************************************/
	private class CodeLineDecorator extends AbstractHandlerDecorator {

		/**
		 * the code sent to the code line
		 */
		private final Codes LogicCode;
		
		/**
		 * the ctor
		 * @param handlker is the lock handler being decorated
		 * @param code is the Code line code.  The code is always sent with a Request
		 * and true when the code is set and false when cleared.
		 */
		public CodeLineDecorator(final LockIndicationHandler handler, final Codes code) {
			super(handler);
			LogicCode = code;
		}

		@Override
		public void requestLockSet() {
			CodeLineEntry.createPacket(CodePurpose.REQUEST, LogicCode, Constants.TRUE);
			Handler.requestLockSet();
		}
		
		@Override
		public void requestLockClear() {
			CodeLineEntry.createPacket(CodePurpose.REQUEST, LogicCode, Constants.FALSE);
			Handler.requestLockClear();
		}
		
		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
		}
		
		@Override
		public void advanceLockClear()
		{
			Handler.advanceLockClear();
		}
	}
	/***************************************************************************
	 * end of CodeLineDecorator
	 **************************************************************************/

	/*****************************************************************************************
	 * end of Lock Indication Handlers
	 *****************************************************************************************/	
	/**
	 * is a container for describing a route- the PtsEdge and the track number.
	 */
	public class RouteReference {
		
		/**
		 * the PtsEdge
		 */
		public final PtsEdge Edge;
		
		/**
		 * the track number
		 */
		public final int TrackNumber;
		
		/**
		 * the ctor
		 * @param edge is the PtsEdge
		 * @param track is the track number
		 */
		RouteReference(final PtsEdge edge, final int track) {
			Edge = edge;
			TrackNumber = track;
		}
		
	}
	static org.apache.log4j.Logger Log = org.apache.log4j.Logger.getLogger(PtsEdge.class.getName());

}
/* @(#)PtsEdge.java */
