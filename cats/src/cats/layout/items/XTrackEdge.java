/* Name: XTrackEdge.java
 *
 * What:
 *   This interface adds methods to TrackEdge that are needed for one side
 *   of a crossing - a diamond
 */
package cats.layout.items;

import java.util.BitSet;
import java.util.EnumSet;

import cats.layout.vitalLogic.CrossingVitalLogic;

/**
 *   This interface adds methods to TrackEdge that are needed for one side
 *   of a crossing - a diamond
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface XTrackEdge extends TrackEdge {

	/**
	 * relays to the associated XVitalLogic the identity of the blocking
	 * XVitalLogic
	 * @param blocks is the blocking XVitalLogics
	 */
	public void setBlockingLogic(XTrackEdge blocks[]);

    /**
     * binds half of a double crossover to the other half.
     * @param edge is the other half.
     */
    public void bindTo(XTrackEdge edge);
    
    /**
     * is a work around for Java's lack of multiple
     * Inheritance.  It returns itself, but as a SecEdge
     * or derived class rather than a TrackEdge.
     * @return "this"
     */
    public SecEdge castEdge();
    
    /**
     * stimulates generation of the Vital Logic
     */
    public void setVitalLogic();
    
    /**
     * retrieves the associated VitalLogic
     * @return the VitalLogic used on the XTrackEdge
     */
    public CrossingVitalLogic getXVitalLogic();
    
	/**
	 * this is a way to invoke isEdgeBlocked() on the cross tracks without creating
	 * an infinite recursion.
	 * @param blockingConditions is the set of GUI locks which prevent a route from being created
	 * @return true if any of the locks are set in edges or common
	 */
	public boolean xIsEdgeBlocked(final EnumSet<GuiLocks> blockingConditions);
	
	/**
	 * this is a way to invoke activateStacking() on the cross tracks without creating
	 * an infinite recursion.
	 * @param alert is true to generate a DAG check when conditions clear and
	 * false to not
	 * @param NodeSet is the set of all RouteEdges.  Registering a node for stacking involves
	 * setting the node's index in the set.  NodeSet must not be null.
	 */
	public void xActivateStacking(final boolean alert, BitSet nodeSet);
}
/* @(#)XTrackEdge.java */