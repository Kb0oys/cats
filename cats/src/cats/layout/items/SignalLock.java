/* Name: SignalLock.java
 *
 * What:
 *   This class is an association between an EnumerationLock and an IOSpec.  It provides
 *   a way to store and retrieve the JMRI object that is the API into a Signal.
 */
package cats.layout.items;

import cats.layout.vitalLogic.LogicLocks;
import cats.layout.xml.XMLEleFactory;
import cats.layout.xml.XMLEleObject;
import cats.layout.xml.XMLReader;


/**
 *   This class is an association between an EnumerationLock and an IOSpec.  It provides
 *   a way to store and retrieve the JMRI object that is the API into a Signal.
 *   <p>
 * Title: CATS - Crandic Automated Traffic System
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2014, 2015, 2016, 2017, 2018
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public class SignalLock implements XMLEleObject{


  /**
   * is the XML tag on a Lock Element
   */
  static final String XML_TAG = "LOCK";
  
  /**
   * is the XML attribute tag for the Lock's name
   */
  static final String LOCK_NAME = "LOCKNAME";
  
  /**
   * is the EnumerationLock
   */
  private final LogicLocks MyLock;
  
  /**
   * is the IOSpec
   */
  private IOSpec MySpec;
  
  /**
   * the ctor
   * @param lock is the lock
   */
  public SignalLock(LogicLocks lock) {
      MyLock = lock;
  }

  /**
   * a ctor to assist in generating the XML
   * @param lock is the lock
   * @param spec is the IOSpec associated with the lock
   */
  public SignalLock(LogicLocks lock, IOSpec spec) {
      MyLock = lock;
      MySpec = spec;
  }
  
  /**
   * retrieves the Lock field
   * @return the EnumerationLock
   */
  public LogicLocks getLock() {
      return MyLock;
  }
  
  /**
   * sets the IOSpec
   * @param spec is the specification for the external Lock
   */
  public void setSpec(IOSpec spec) {
      MySpec = spec;
  }
  
  /**
   * retrieves the IOSpec for the Lock
   * @return the IOSpec
   */
  public IOSpec getSpec() {
      return MySpec;
  }

  public String setValue(String eleValue) {
      return new String("A " + XML_TAG + " cannot have a value");
  }

  public String setObject(String objName, Object objValue) {
      String resultMsg = null;
      if (IOSpec.XML_TAG.equals(objName)) {
          MySpec = (IOSpec) objValue;
      }
      else {
          resultMsg = new String("Expecting " + XML_TAG + " and received " + objName);
      }
      return resultMsg;
  }

  public String getTag() {
      return XML_TAG;
  }

  public String doneXML() {
      if (MySpec == null) {
          return new String("Missing IOSPecification for Lock " + MyLock.toString());
      }
      return null;
  }
  
  /**
   * registers a SignalLockFactory with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new SignalLockFactory());
  }
}

/**
* is a class known only to the SignalLock for creating a SignalLock from a
* Lock XML Element
*/
class SignalLockFactory implements XMLEleFactory {

  /**
   * is the name of the EnumerationLock
   */
  private String LockName = null;
  
  public void newElement() {
  }

  public String addAttribute(String tag, String value) {
      if (SignalLock.LOCK_NAME.equals(tag)) {
          LockName = value;
          LockName.trim();
          return null;
      }
      return new String(tag + " is not a valid attribute for a " + SignalLock.XML_TAG);
  }

  public XMLEleObject getObject() {
      if ((LockName != null) && !LockName.isEmpty()) {
          return new SignalLock(LogicLocks.valueOf(LockName));
      }
      return null;
  }
}
/* @(#)SignalLock.java */
