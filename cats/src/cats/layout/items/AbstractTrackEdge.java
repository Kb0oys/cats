/* Name: AbstractTrackEdge.java
 *
 * What:
 *   This class is derived from SecEdge by adding an implementation
 *   for part of the TrackEdge interface.  It defines some common
 *   fields and methods for using them. The focus is on common lock
 *   handling and traversing from one Track Sequence to teh next.
 */
package cats.layout.items;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.EnumSet;
import java.util.Enumeration;

import cats.apps.Crandic;
import cats.common.DebugBits;
import cats.layout.Discipline;
import cats.layout.codeLine.CodeMessage;
import cats.layout.ctc.RouteNode;
import cats.layout.ctc.StackOfRoutes;
import cats.layout.ctc.TrackEdgeCounter;
import cats.layout.items.RouteEdge.RouteSearchStatus;
import cats.layout.vitalLogic.BasicVitalLogic;
import cats.layout.vitalLogic.LogicLocks;

/**
 *   This class is derived from SecEdge by adding an implementation
 *   for part of the TrackEdge interface.  It defines some common
 *   fields and methods for using them. The focus is on common lock
 *   handling and traversing from one Track Sequence to teh next.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2015, 2016, 2018, 2019, 2020, 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
abstract public class AbstractTrackEdge extends SecEdge implements TrackEdge {

	/**
	 * the immediate TrackEdge in approach of this one.  The one that this
	 * one feeds state changes to.
	 */
	protected AbstractTrackEdge ApproachEdge;

	/**
	 * the SecEdge at the other end of the Track Sequence.  Unless the other end
	 * is end of track, this should be a TrackEdge
	 */
	protected AbstractTrackEdge OppositeEnd = null;

	/**
	 * the neighbor TrackEdge.  Typically, it is in the adjacent Grid Tile;
	 * however, with hyper-jumping, it may not be.  The "neighbor" is immutable
	 * (except for points and frogs); thus, rather than run through the computation
	 * of who the neighbor is on every traversal, it is stored here.  The method 
	 * SecEdge.getNeighbor() handles this at the SecEdge level. nextTrackEdge()
	 * builds on getNeighbor to consider the Discipline in building linkages.
	 */
	protected AbstractTrackEdge NeighborEdge = null;

	/**
	 * a counter of the number of stacked routes involving this TrackEdge.  If it is 0, then there are
	 * none and it does not trigger the RouteStack machine when a blocking condition happens.  If greater than
	 * 0, then the RouteStack machine is triggered when a blocking condition is cleared.  The RouteStack machine
	 * is triggered in event processing.
	 */
	protected int StackedRoute;
	
	/**
	 * the index into the BitSet for flagging this TrackEdge as a candidate for inclusion in a route
	 */
	protected final int RouteStatusIndex;
	
	/**
	 * the corresponding RouteNode in the active route running through the RouteEdge
	 */
	protected RouteNode ActiveRoute;
	
//	/**
//	 * the stack of RouteNoes waiting to be processed.  The first is either active or
//	 * waiting to become active.
//	 */
//	protected RouteNode RouteStack;
	
	/**
	 * the VitalLogic protecting entry into the edge
	 */
	protected BasicVitalLogic MyLogic;

	/**
	 * the Locks common to more than one edge
	 */
	protected EnumSet<GuiLocks> CommonLocks = EnumSet.noneOf(GuiLocks.class);

	/**
	 * the locks specific to this edge
	 */
	protected EnumSet<GuiLocks> EdgeLocks = EnumSet.noneOf(GuiLocks.class);
	
	/**
	 * the list of all tracks protected by this TrackEdge (i.e. the Tracks
	 * behind this TrackEdge, up to and including the next one in the opposite
	 * direction).  Because points and frogs are TrackEdge implementations,
	 * the tracks being protected are constant.
	 */
	protected ArrayList<Track> TrackSpan = new ArrayList<Track>(1);

//	/**
//	 * the locks which are blocked from being propagated
//	 */
//	EnumSet<LogicLocks> TrafficLocks = EnumSet.noneOf(LogicLocks.class);

//	/**
//	 * the locks which are currently blocked from changing the GUI
//	 */
//	EnumSet<LogicLocks> BlockedLocks = EnumSet.noneOf(LogicLocks.class);

//	/**
//	 * any of these locks will turn a signal red, preventing an exit from
//	 * its neighbor
//	 */
//	EnumSet<LogicLocks> BlockingTriggers = EnumSet.noneOf(LogicLocks.class);
	
	/***********************************************************************
	 * the following are for RouteEnds.  Not all derived classes from AbstractTrack
	 * implement RouteEnd, but those that do, use some of the same algorithms.
	 ***********************************************************************/
	
	/**
	 * the route identity.  This is needed for identifying track loops.
	 */
	protected int RouteSearchID = 0;
	
	/**
	 * the status of the route search
	 */
	protected RouteEdge.RouteSearchStatus SearchStatus = RouteSearchStatus.UNSEARCHED;
	
	/**
	 * the opposing CPEdges (Signals) between this RouteEdge and the next in advance
	 * in the same direction of travel.  These are searched in an attempt to find
	 * an exit signal for a route.  This member uses deferred creation.  It starts out
	 * null and remains so until a search is requested.  Then it is created to hold
	 * all opposing CPEdge found.  Since these are immutable (except PtsEdge), the
	 * list is retained between searches and need not be recreated.  The list can exist,
	 * but be empty.
	 */
	protected ArrayList<CPEdge> OpposingSignalList;
	
	/**
	 * the opposing CPEdges between this RouteNode and the next.  It is used for
	 * alerting those edges to tell StcakOfRoutes when locks change, to initiate
	 * scanning of the TopOfStack.
	 */
	protected ArrayList<AbstractTrackEdge> OpposingEdges;
	
	/**
	 * The BlkEdge between this TrackEdge and the next, in the direction of
	 * travel.  These are needed for accumulating blocking locks.  CommonLocks
	 * cannot be used because common locks reflects the locks between CPEdges
	 * based on the current points settings.  When exploring extended routes
	 * all paths through turnouts must be probed - not just the current
	 * alignment.
	 */
	protected ArrayList<AbstractTrackEdge> InternalEdges;
	
	/**
	 * the first RouteEnd discovered in advance of this RouteEnd.  As above,
	 * it has deferred creation.  But, since no advance RouteEnd may exist,
	 * null does not mean the search has been performed.
	 */
	protected RouteEdge NextRouteEdge = null;
	
	/**
	 * the RouteNode associated with a route search
	 */
	protected RouteNode SearchNode;
	
	/**************************************************************************
	 * the handlers for changes in field locks
	 **************************************************************************/
	protected LockIndicationHandler DetectionIndication;
	protected LockIndicationHandler ConflictingSignalIndication;
	protected LockIndicationHandler SwitchUnlockIndication;
	protected LockIndicationHandler ApproachIndication;
	protected LockIndicationHandler TimeIndication;
	protected LockIndicationHandler TrafficIndication;
	protected LockIndicationHandler EntryTrafficIndication;
	protected LockIndicationHandler ExitTrafficIndication;
	protected LockIndicationHandler LocalControlLock;
	protected LockIndicationHandler CallOnIndication;
	protected LockIndicationHandler WarrantIndication;
	protected LockIndicationHandler FleetIndication;
	protected LockIndicationHandler CrossingIndication;
	protected LockIndicationHandler PreparationIndication;
	protected LockIndicationHandler ExternalIndication;


	/**
	 * @param edge identifies the side of the GridTile the SecEdge is on.
	 * @param shared describes the shared edge, if it is not adjacent.
	 *    A null value means it is adjacent.
	 */
	public AbstractTrackEdge(int edge, Edge shared) {
		super(edge, shared);
		RouteStatusIndex = TrackEdgeCounter.getEdgeOrdinal();
	}

	@Override
	public EdgeInterface.EDGE_TYPE getEdgeType() {
		return EdgeInterface.EDGE_TYPE.ABSTRACT_EDGE;
	}
	
	@Override
	abstract public BasicVitalLogic createVitalLogic();

	@Override
	abstract public void discoverAdvanceVitalLogic();

	/**
	 * a method that walks the tracks in advance to a TrackEdge, looking for the next
	 * TrackEdge.  It performs the common activities for discoverAdvanceVitalLogic().
	 * It
	 * 1. adds all the tracks encountered to the TrackSpan (they are immutable and
	 * 	there are no points or frogs because they are after the next TrackEdge).
	 * 2. computes the minimum speed
	 * 3. sets OppositeEnd
	 * 4. attempts to set ApproachEdge on the TrackEdge in advance to this.
	 * 
	 * @param entryEdge is the TrackEdge where the work begins
	 * @return the slowest speed over the TrackSequence.
	 */
	protected int trackSegmentAggregator(SecEdge entryEdge) {
		SecEdge opposite = null;
		TrackEdge advanceEdge;
		int speed = Track.NORMAL;
		do {
			speed = Track.getSlower(speed, entryEdge.Destination.getSpeed());
			TrackSpan.add(entryEdge.Destination);
			opposite = entryEdge.Peer; 
			if (opposite instanceof TrackEdge) {  // could be any except PtsEdge
				OppositeEnd = (AbstractTrackEdge) opposite;
				if ((advanceEdge = OppositeEnd.nextTrackEdge()) != null) {
					advanceEdge.setApproachEdge(this);
				}
				return speed;
			}
			else if (((entryEdge = opposite.getNeighbor()) != null) &&
					(entryEdge instanceof PtsEdge)) {
				OppositeEnd = (AbstractTrackEdge) entryEdge;
				((TrackEdge) entryEdge).setApproachEdge(this);
				return speed;
			}
		} while (entryEdge != null);
		return speed;
	}
		
	@Override
	public void setApproachEdge(AbstractTrackEdge approach) {
//		if (approach == null) {
//			System.out.println("Setting approach edge for " + toString() + "to null");
//		}
//		else {
//			System.out.println("Setting approach edge for " + toString() + " to " + approach.toString());
//		}
		ApproachEdge = approach;
		if (approach != null) {
			MyLogic.setApproachLogic(approach.getVitalLogic());
		}
//		Discipline disc = MyBlock.getDiscipline();
//		Discipline other;
//		if (approach != null) {
//			if ((disc.equals(Discipline.ABS) || (disc.equals(Discipline.UNDEFINED)))) {
//				other = approach.MyBlock.getDiscipline();
//				if ((other.equals(Discipline.ABS)) || (other.equals(Discipline.UNDEFINED))) {
//					MyLogic.setApproachLogic(approach.getVitalLogic());
//				}
//			}
//			else {
//				MyLogic.setApproachLogic(approach.getVitalLogic());
//			}
//		}
	}

	@Override
	abstract public SignalEdge signalEdgeProbe();

	@Override
	abstract public TrackEdge nextTrackEdge();

	@Override
	public BasicVitalLogic getVitalLogic() {
		return MyLogic;
	}

	@Override
	abstract public boolean isBlkEdge();

//	@Override
//	abstract public void setDirection(DirectionOfTravel dot);

	@Override
	public boolean isLockSet(GuiLocks lock) {
//		return EdgeLocks.contains(lock) || CommonLocks.contains(lock);
		return EdgeLocks.contains(lock);
	}

	@Override
	abstract public CodeMessage processFieldResponse(String response);

	@Override
	public void startFeeding() {
		if (getVitalLogic() != null) {
			getVitalLogic().primeApproach();
		}		
	}

	@Override
	public void setHandlers() {
		DetectionIndication = new DefaultLockIndicationHandler(GuiLocks.DETECTIONLOCK);
		ConflictingSignalIndication = new DefaultLockIndicationHandler(GuiLocks.CONFLICTINGSIGNALLOCK);
		SwitchUnlockIndication = new DefaultLockIndicationHandler(GuiLocks.SWITCHUNLOCK);
		ApproachIndication = new DefaultLockIndicationHandler(GuiLocks.APPROACHLOCK);
		TimeIndication = new DefaultLockIndicationHandler(GuiLocks.TIMELOCK);
		TrafficIndication = new DefaultLockIndicationHandler(GuiLocks.TRAFFICLOCK);
		EntryTrafficIndication = new DefaultLockIndicationHandler(GuiLocks.ENTRYLOCK);
		ExitTrafficIndication = new DefaultLockIndicationHandler(GuiLocks.EXITLOCK);
		LocalControlLock = new DefaultLockIndicationHandler(GuiLocks.LOCALCONTROLLOCK);
		CallOnIndication = new DefaultLockIndicationHandler(GuiLocks.CALLONLOCK);
		WarrantIndication = new DefaultLockIndicationHandler(GuiLocks.WARRANTLOCK);
		FleetIndication = new DefaultLockIndicationHandler(GuiLocks.FLEET);
		CrossingIndication = new DefaultLockIndicationHandler(GuiLocks.CROSSING);
		PreparationIndication = new DefaultLockIndicationHandler(GuiLocks.PREPARATION);
		ExternalIndication = new DefaultLockIndicationHandler(GuiLocks.EXTERNALLOCK);
	}
	
	/**
	 * is called to identify the AbstractTrackEdge in approach
	 * of this edge - the one fed by this edge
	 * @return the approach AbstractTrackEdge, which could be null
	 */
	public AbstractTrackEdge getApproachEdge() {
		return ApproachEdge;
	}

	/**
	 * is called to traverse the track sequence, returning the AbstractTrackEdge
	 * at the far end, if there is one.
	 * @return  OppositeEnd, which could be null
	 */
	public AbstractTrackEdge getOppositeEdge() {
		return OppositeEnd;
	}
	
	/**
	 * retrieves the EdgeLocks
	 * @return the EdgeLocks
	 */
	public EnumSet<GuiLocks> getEdgeLocks() {
		return EdgeLocks;
	}
	
	/**
	 * retrieves the set of CommonLocks
	 * @return the CommonLocks
	 */
	public EnumSet<GuiLocks> getCommonLocks() {
		return CommonLocks;
	}
	
	/**
	 * sets a lock in the common set, which may also be processed by the
	 * specific lock handler
	 * @param lock is the lock being set
	 */
	public void setCommonLock(GuiLocks lock) {
		LockIndicationHandler handler;
		if (!CommonLocks.contains(lock)) {
			EnumSet<GuiLocks> oldLocks = CommonLocks.clone();
			if ((handler = lockToHandler(lock)) != null) {
				handler.advanceLockSet();
			}
//			CommonLocks.add(lock);
			/******************  DEBUG code ******************************/
			if (Crandic.Details.get(DebugBits.GUILOCKBIT) && !oldLocks.equals(CommonLocks)) {
				System.out.println("Edge " + toString() + " common locks were " + oldLocks + " now " + CommonLocks.toString());			
			}
			/****************** end DEBUG code ******************************/
		}		
	}

	/**
	 * clears a lock in the common set, which is merged with the
	 * edge specific locks
	 * @param lock is the lock set
	 */
	public void clearCommonLock(GuiLocks lock) {
		LockIndicationHandler handler;
		if (CommonLocks.contains(lock)) {
			EnumSet<GuiLocks> oldLocks = CommonLocks.clone();
			if ((handler = lockToHandler(lock)) != null) {
				handler.advanceLockClear();
			}
//			CommonLocks.remove(lock);
			/******************  DEBUG code ******************************/
			if (Crandic.Details.get(DebugBits.GUILOCKBIT) && !oldLocks.equals(CommonLocks)) {
				System.out.println("Edge " + toString() + " common locks were " + oldLocks + " now " + CommonLocks.toString());			
			}
			/****************** end DEBUG code ******************************/
		}		
	}

	@Override
	  public void addSelf(Section sec) {
		super.addSelf(sec);
		MyLogic = createVitalLogic();
	}
	
	/**
	 * sets a lock in the edge
	 * @param lock is the lock set
	 */
	public void setEdgeLock(GuiLocks lock) {
		LockIndicationHandler handler;
		if (!EdgeLocks.contains(lock)) {
			EnumSet<GuiLocks> oldLocks = EdgeLocks.clone();
			if ((handler = lockToHandler(lock)) != null) {
				handler.requestLockSet();
			}
//			EdgeLocks.add(lock);
			/******************  DEBUG code ******************************/
			if (Crandic.Details.get(DebugBits.GUILOCKBIT) && !oldLocks.equals(EdgeLocks)) {
				System.out.println("Edge " + toString() + " edge locks were " + oldLocks + " now " + EdgeLocks.toString());			
			}
			/****************** end DEBUG code ******************************/
		}		
	}

	/**
	 * clears a lock in the edge specific set, which is merged with the
	 * common edge locks
	 * @param lock is the lock set
	 */
	public void clearEdgeLock(GuiLocks lock) {
		LockIndicationHandler handler;
		if (EdgeLocks.contains(lock)) {
			EnumSet<GuiLocks> oldLocks = EdgeLocks.clone();
//			EdgeLocks.remove(lock);
			if ((handler = lockToHandler(lock)) != null) {
				handler.requestLockClear();
			}
			/******************  DEBUG code ******************************/
			if (Crandic.Details.get(DebugBits.GUILOCKBIT) && !oldLocks.equals(EdgeLocks)) {
				System.out.println("Edge " + toString() + " edge locks were " + oldLocks + " now " + EdgeLocks.toString());			
			}
			/****************** end DEBUG code ******************************/
		}		
	}

//	/**
//	 * sets filters on Locks for preventing them from propagating to
//	 * the neighbor or approach edge
//	 * @param filters is the set of locks that are blocked
//	 */
//	protected void setFilters(EnumSet<LogicLocks> filters) {
//		LockIndicationHandler handler;
//		for (LogicLocks lock : filters) {
//			if ((handler = lockToHandler(lock)) != null) {
//				handler.blockLock();
////				TrafficLocks.remove(lock);
//			}			
//		}
//	}
//
//	/**
//	 * removes filters on Locks that prevents them from propagating to
//	 * the neighbor or approach edge
//	 * @param filters is the set of locks that are blocked
//	 */
//	protected void clearFilters(EnumSet<LogicLocks> filters) {
//		LockIndicationHandler handler;
//		for (LogicLocks lock : filters) {
//			if ((handler = lockToHandler(lock)) != null) {
//				handler.unblockLock();
////				TrafficLocks.add(lock);
//			}			
//		}
//	}
	
	/**
	 * maps a LogicLock to its indication handler
	 * @param lock is the LogicLock
	 * @return its indication handler
	 */
	protected LockIndicationHandler lockToHandler(GuiLocks lock) {
		switch (lock) {
		case DETECTIONLOCK: return DetectionIndication;
		case CONFLICTINGSIGNALLOCK: return ConflictingSignalIndication;
//		case HOLDLOCK: return HoldIndication;
		case SWITCHUNLOCK: return SwitchUnlockIndication;
		case APPROACHLOCK: return ApproachIndication;
		case TIMELOCK: return TimeIndication;
		case TRAFFICLOCK: return TrafficIndication;
		case ENTRYLOCK: return EntryTrafficIndication;
		case EXITLOCK: return ExitTrafficIndication;
		case LOCALCONTROLLOCK: return LocalControlLock;
		case CALLONLOCK: return CallOnIndication;
		case WARRANTLOCK: return WarrantIndication;
		case FLEET: return FleetIndication;
		case CROSSING: return CrossingIndication;
		case PREPARATION: return PreparationIndication;
		case EXTERNALLOCK:
		default:
			break;
		}
		return null;
	}

	/**
	 * is a generic method for walking the tracks from this TrackEdge to the
	 * next in the advance (behind) direction.  For each track encountered,
	 * it changes the color mix active on that track.  This method covers all the
	 * the tracks connected to this edge.  This is called the Track Sequence
	 * in the documentation.
	 * @param decoration describes the color of the Track
	 */
	public void paintTrackSequence(RouteDecoration decoration) {
		SequenceWalker walker = new SequenceWalker();
		while (walker.hasMoreElements()) {
			walker.nextElement().alterColor(decoration.getTrackColor(), decoration.getOnOff());
		}		
	}

	/**
	 * walks the tracks protected by this BlkEdge (and the following
	 * TrackEdge, if it is in the same Block), changing the color mix
	 * active on those tracks.  This method covers all the tracks up to
	 * a matching BlkEdge, spanning turnouts.  The ending BlkEdge is returned
	 * because multiple Track Sequences might be traversed.
	 * @param decoration describes the color of the Track
         * @return the ending BlkEdge
	 */
	public AbstractTrackEdge paintTrackBlock(RouteDecoration decoration) {
		BlockWalker walker = new BlockWalker();
		while (walker.hasMoreElements()) {
			walker.nextElement().alterColor(decoration.getTrackColor(), decoration.getOnOff());
		}
		return walker.lastEdge();
	}

	/**
	 * colors the Tracks behind this AbstractTrackEdge to the next Control
	 * Point in the opposing direction - the route boundary.
	 * @param decoration describes the color of the Track
	 */
	public void paintTrackRoute(RouteDecoration decoration) {
		RouteWalker walker = new RouteWalker();
		while (walker.hasMoreElements()) {
			walker.nextElement().alterColor(decoration.getTrackColor(), decoration.getOnOff());
		}		
	}

	/**
	 * remembers the active route through the AbstractTrackEdge
	 * @param route is the route.  Null is valid.
	 */
	public void setRouteAnchor(final RouteNode route) {
		ActiveRoute = route;
	}

//	/**
//	 * is the "getter" for ActiveRoute
//	 * @return ActiveRoue, which may be null.
//	 */
//	public CTCRoute getRouteAnchor() {
//		return ActiveRoute;
//	}
	/**************************************************************************
	 * Though an AbstractTrackEdge does not implement the RouteEdge interface
	 * the following methods implement pieces of the interface.  They are
	 * here because they do similar things in implementers (CPEdge, FrogEdge,
	 * PtsEdge) and AbstractTrackEdge is the common ancestor.  However,
	 * CPEdge is not derived directly from AbstractTrackEdge, so some of the
	 * intermediate classes will have these methods, though they don't use them.
	 **************************************************************************/
	
	/**
	 * traverses tracks from the RouteEdge over tracks in advance, looking for
	 * certain blocking conditions.  The LocalLocks on each TrackEdge,
	 * in either direction of travel, encountered are tested for the blocking
	 * condition.  Traversal stops at the next RouteEdge or end of track.
	 * @param blockingConditions is an EnumSet of GuiLocks, containing the locks
	 * of interest.
	 * @return true if any TrackEdge Local Locks contains any of blockingConditions;
	 * otherwise, it returns false.
	 */
	public boolean isRouteSegmentBlocked(final EnumSet<GuiLocks> blockingConditions) {
		AbstractTrackEdge nextEdge = this;
//		if (OpposingSignalList == null) {
//			if (this instanceof PtsEdge) {
//				if (((PtsEdge) this).NeighborEdge != null) {
//					return ((PtsEdge) this).NeighborEdge.isRouteSegmentBlocked(blockingConditions);
//				}
//				else {
//					return false;
//				}
//			}
//			else if (this instanceof FrogEdge) {
//				if (((FrogEdge)this).isFoulingEdge()) {
//					return false;
//				}
//				else {
//					return NeighborEdge.isRouteSegmentBlocked(blockingConditions);
//				}
//			}
//			else {
//				NextRouteEdge = findNextRouteEdge();
//			}
//		}
		if (OpposingSignalList == null) {
			NextRouteEdge = findNextRouteEdge();
		}
		do {
			if ((nextEdge.getBlock().getDiscipline() == Discipline.UNDEFINED) ||
					(nextEdge.getBlock().getDiscipline() == Discipline.ABS)) {
				return false;
			}
			if (nextEdge.extendedIsEdgeBlocked(blockingConditions)) {
				return true;
			}
			nextEdge = nextEdge.OppositeEnd;
			if (nextEdge != NextRouteEdge) {
				if (nextEdge.isEdgeBlocked(GuiLocks.OppositeExtendedRouteBlock)) {
					return true;
				}
				nextEdge = nextEdge.NeighborEdge;
			}
		} while (nextEdge != NextRouteEdge);
		return false;
	}
	
	/**
	 * checks a TrackEdge for blocking conditions between it and the next TrackEdge in advance.
	 * Only EdgeLocks are examined because CommonLocks may also reflect locks past the next
	 * TrackEdge (i.e. on the other side of points).
	 * @param blockingConditions is the set of locks that would prevent a route from being created
	 * @return true if the TrackEdge contains a lock from the set
	 */
	protected boolean isEdgeBlocked(final EnumSet<GuiLocks> blockingConditions) {
		if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
			System.out.println("Edge " + toString() + " EdgeLocks=" + EdgeLocks.toString() +
					" CommonLocks=" + CommonLocks.toString());
		}
		if (!GuiLocks.intersection(blockingConditions, EdgeLocks).isEmpty()) {
			if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
				System.out.println("Edge " + toString() + " blocking on edge EdgeLocks");
			}
			return true;
		}
		return false;
	}
	
	/**
	 * checks both EdgeLocks and CommonLocks for blocking conditions
	 * @param blockingConditions is the set of locks that would prevent a route from being created
	 * @return true if the TrackEdge contains a lock from the set
	 */
	protected boolean extendedIsEdgeBlocked(final EnumSet<GuiLocks> blockingConditions) {
		if (isEdgeBlocked(blockingConditions)) {
			return true;
		}
		if (!GuiLocks.intersection(GuiLocks.CommonBlock, CommonLocks).isEmpty()) {
			if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
				System.out.println("Edge " + toString() + " blocking on edge CommonLocks");
			}
			return true;
		}
		return false;
	}
	
	/**
	 * this method begins at an AbstractTrackEdge and pursues all possible paths that can be
	 * reached from this edge, returning them in turn.  It considers all paths out of
	 * switch points.
	 */
//	public void cleanRoute() {
//		AbstractTrackEdge opposite;
//		AbstractTrackEdge edge = this;
//		while (edge != null) {
//			edge.resetLocks();
//			if ((opposite = edge.OppositeEnd) == null) {
//				edge = null;
//			}
//			else {
//				opposite.resetLocks();
//				switch (opposite.getEdgeType()) {
//				case POINTS_EDGE:
//				case OS_EDGE:
//					((PtsEdge) opposite).clearFrogLocks();
//					edge = null;
//					break;
//
//				case CP_EDGE:
//				case BLOCK_EDGE:
//				case XBLOCK_EDGE:
//				case INTERMEDIATE_EDGE:
//					if (((edge = opposite.NeighborEdge) == null) || edge.getEdgeType().equals(EDGE_TYPE.CP_EDGE)
//							|| edge.getBlock().getDiscipline().equals(Discipline.UNDEFINED)
//							|| edge.getBlock().getDiscipline().equals(Discipline.ABS)){
//						edge = null;
//					}
//					break;
//
//				case FROG_EDGE:
//					edge = ((FrogEdge) opposite).getPoints();
//					break;
//
//				default:
//					edge = null;
//				}
//			}
//		}
//	}

	/**
	 * This method uses the recursion version of the route traversal pattern to remove locks
	 * on TrackEdge,  This cleans up stray locks that could be preventing a route from
	 * being created.  The method is called when entering a TrackEdge.
	 */
	public void entryClearRoute() {
		resetLocks();
		if (OppositeEnd != null) {
			OppositeEnd.exitClearRoute();
		}
	}
	
	/**
	 * This method uses the recursion version of the route traversal pattern to remove locks
	 * on TrackEdge,  This cleans up stray locks that could be preventing a route from
	 * being created.  The method is called when exiting a TrackEdge.
	 */
	public void exitClearRoute() {
		resetLocks();
		if ((NeighborEdge != null) && (!NeighborEdge.getEdgeType().equals(EDGE_TYPE.CP_EDGE))){
			NeighborEdge.entryClearRoute();
		}
	}

	/**
	 * This method illustrates half of a two sequence pattern for how to handle traversing
	 * a route (a chain of TrackEdges from an entry CP to the next CP in advance).
	 * This half is used when entering a TrackEdge.  It's purpose is to apply the "strategy",
	 * and check for either an exit edge or end. 
	 * 
	 * Perhaps it could be implemented using a Strategy design pattern.  This illustration
	 * doesn't because it is so simple (creating the strategies can complicate things) and
	 * the ending criteria, and identifying the exit TrackEdge can vary.
	 * 
	 * It uses recursion.  A variation would use iteration, and then it would change from
	 * a method to a function.  Look at how FrogEdges and PtsEdge implement these when they
	 * the entry and exit edges.
	 */
	public void entryPattern() {
		System.out.println("AbstractTrackEdge Entry: " + toString());
		if (OppositeEnd != null) {
			OppositeEnd.exitPattern();
		}
	}
	
	/**
	 * This method illustrates the second half of the two sequence pattern to handle traversing
	 * a route.  It's purpose is to apply the "strategy" (which could be different from the one
	 * in the entry half) and check for and identify a next entry edge.
	 */
	public void exitPattern() {
		System.out.println("AbstractTrackEdge Exit: " + toString());
		if ((NeighborEdge != null) && (!NeighborEdge.getEdgeType().equals(EDGE_TYPE.CP_EDGE))){
			NeighborEdge.entryPattern();
		}
	}
	
	/**
	 * searches for the next RouteEdge in advance.  Since PtsEdges and FrogEdges
	 * are RouteEdges, either of them can satisfy the result, so there is no
	 * branching.  As opposing CPedges are encountered, they are added to
	 * the OppositeSignals array.
	 * 
	 * This algorithm looks like it jumps too far in the second major clause,
	 * but it doesn't.  Points and Frogs can appear only on the Opposite end
	 * of a Track.  They can never be a Neighbor, except to another Points or Frog.
	 * Thus, the next RouteEdge could be in either direction of travel.
	 * 
	 * @return the first RouteEdge in advance encountered.  There might not
	 * be one, in which case null is returned.
	 * 
	 */
	private RouteEdge findNextRouteEdge() {
		// this loop both searches for the next RouteEdge in advance and
		// adds opposing CPEdges to OpposingSignalList.
		OpposingSignalList = new ArrayList<CPEdge>();
		OpposingEdges = new ArrayList<AbstractTrackEdge>();
		InternalEdges = new ArrayList<AbstractTrackEdge>(1);
		InternalEdges.add(this);
		for (AbstractTrackEdge nextEdge = OppositeEnd; nextEdge != null; ) {
			if ((nextEdge instanceof FrogEdge) || (nextEdge instanceof PtsEdge)) {
				// this looks strange because NextRouteEdge maybe set to an
				// opposing direction SecEdge, but is needed to distinguish entering
				// a turnout from the frog side rather than the points side.
				return (RouteEdge) nextEdge;
			}
			else {
				if (nextEdge instanceof CPEdge) {
					OpposingSignalList.add((CPEdge) nextEdge);
				}
				else {
					OpposingEdges.add(nextEdge);
				}
				if ((nextEdge = nextEdge.NeighborEdge) != null) {
					if ((nextEdge.getBlock().getDiscipline() == Discipline.UNDEFINED) ||
							(nextEdge.getBlock().getDiscipline() == Discipline.ABS)) {
						return null;
					}
					else if (nextEdge instanceof CPEdge) {
						return (RouteEdge) nextEdge;
					}
					else {
						InternalEdges.add(nextEdge);
						nextEdge = nextEdge.OppositeEnd;
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * implements RouteEdge.startSearch()
	 * @param destination is the CPEdge in advance of this RouteEdge being
	 * searched for.
	 * @param searchID is the search generation number, used to detect out of date results
	 * @return null if the search ends at this RouteEdge and non-null for
	 * the next Route Edge in advance
	 */
	public RouteEdge startSearch(final CPEdge destination, final int searchID) {
		SearchStatus = RouteSearchStatus.SEARCHING;
		SearchNode = null;
		RouteSearchID = searchID;
		
		if (OpposingSignalList == null) {
			NextRouteEdge = findNextRouteEdge();
		}

		if (OppositeEnd != null) {
			OppositeEnd.RouteSearchID = searchID;
		}
		
		// since the list of opposing CPEdges has been constructed, the next
		// step is to search them for the desired CPEdge
		for (CPEdge e : OpposingSignalList) {
			if (e.equals(destination)) {
				SearchStatus = RouteSearchStatus.SUCCESS;
				if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
					System.out.println("Found destination signal " + destination);
				}
				return null;
			}
		}
		if (destination == NextRouteEdge) {
			SearchStatus = RouteSearchStatus.SUCCESS;
			if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
				System.out.println("Found destination signal " + destination);
			}
			return null;
		}
		if (NextRouteEdge == null) {
			SearchStatus = RouteSearchStatus.DEADEND;
		}
		return NextRouteEdge;
	}
	
	/**
	 * queries the ID of the last search.  It is used to determine if the status
	 * is stale or not.  If the ID is the same as the current search ID, then
	 * the status is valid; otherwise, the ID will be updated, which sets the status
	 * to USEARCHED.
	 * @return the ID of the last search
	 */
	public int getSearchID() {
		return RouteSearchID;
	}
	
	/**
	 * sets the search status
	 * @param status is the new search status
	 */
	public void setSearchStatus(final RouteSearchStatus status) {
		SearchStatus = status;
	}
	
	/**
	 * queries the edge for status of the last search
	 * @return the status
	 */
	public RouteSearchStatus getSearchStatus() {
		return SearchStatus;
	}
	
	/**
	 * queries the edge for the search node
	 * @return the search node, which may be null;
	 */
	public RouteNode getNode() {
		return SearchNode;
	}
	
	/**
	 * removes a RouteNode from the RouteEdge's search link.  If
	 * the search link contains the node, the search link is cleared;
	 * otherwise, nothing happens.
	 * @param node is a node possibly in the search link.  It should not be null.
	 */
	public void releaseNode(final RouteNode node) {
//		RouteNode next;
		if (SearchNode == node) {
			SearchNode = null;
			SearchStatus = RouteSearchStatus.UNSEARCHED;
		}
//		else if (RouteStack != null) {
//			if (RouteStack == node) {
//				RouteStack = RouteStack.getStackNode();
//			}
//			else {
//				for (RouteNode link = RouteStack; (next = link.getStackNode()) != null; link = next) {
//					if (next == node) {
//						link.setStackNode(null);
//						break;
//					}
//				}
//			}
//		}
	}

	/**
	 * is a query of the number of times the Block associated with the
	 * RouteEdge has received an occupancy report
	 * @return the number of reports received
	 */
	public long getOccupancyCount() {
		return (MyBlock != null) ? MyBlock.getDetectionCount() : 0;
	}

	/**
	 * steps through all intermediate signals and opposing CPEdges
	 * between this RouteEdge and the next, setting or clearing the
	 * StackedRoute flag.
	 * @param stackWaiting is true for the edge to tell StackOfRoutes
	 * when a lock changes and false to not tell it
	 * @param NodeSet is the set of all RouteEdges.  Registering a node for stacking involves
	 * setting the node's index in the set.  NodeSet must not be null.
	 */
	public void registerStackNode(final boolean stackWaiting, BitSet NodeSet) {
		activateStacking(stackWaiting, NodeSet);
		if (OpposingSignalList != null) {
			for (CPEdge opposing : OpposingSignalList) {
				opposing.activateStacking(stackWaiting, NodeSet);
			}
		}
		if (OpposingEdges != null) {
			for (AbstractTrackEdge intermediate : OpposingEdges) {
				intermediate.activateStacking(stackWaiting, NodeSet);
			}
		}
	}
	
	/**
	 * this method sets/clears the StackedRoute flag, which is used by the RouteEdge
	 * to inform the StackOfRoutes that a potential route through it had
	 * a blocking condition clear.
	 * @param alert is true to indicate that clearing locks should trigger
	 * StackOfRoutes and false to indicate the RouteEdge is not in a potential
	 * route.
	 * @param NodeSet is the set of all RouteEdges.  Registering a node for stacking involves
	 * setting the node's index in the set.  NodeSet must not be null.
	 */
	public void activateStacking(final boolean alert, BitSet nodeSet) {
		int was = StackedRoute;
		if (alert) {
			if (!nodeSet.get(RouteStatusIndex)) {
				nodeSet.set(RouteStatusIndex);
				StackedRoute = Math.max(StackedRoute + 1, 1);
			}
		}
		else {
			if (nodeSet.get(RouteStatusIndex)) {
				nodeSet.clear(RouteStatusIndex);
				StackedRoute = Math.max(StackedRoute - 1, 0);
			}
		}
		if (Crandic.Details.get(DebugBits.ROUTESTACKING)) {
			System.out.println("Track Edge " + toString() + " was on " + was + " now on " + StackedRoute + " routes");
		}
	}

	/**
	 * retrieves the index into a Route BitSet of the node
	 * @return the index
	 */
	public int getRouteStatusIndex() {
		return RouteStatusIndex;
	}
	
//	/**
//	 * removes a RouteNode from the route construction phase and puts it on
//	 * the Edge's stack.
//	 * @param node is a RouteNode to be stacked.  It should not be null.
//	 */
//	public void stackNode(final RouteNode node) {
//		RouteNode link;
//		if (SearchNode == node) {
//			SearchNode = null;
//		}		
//		node.setNodeStatus(RouteNodeStatus.PENDING);
//		if (RouteStack == null) {
//			RouteStack = node;
//		}
//		else {
//			for (link = RouteStack; link.getStackNode() != null; link = link.getStackNode()) {	
//				// empty body.  The loop looks for the last node in the chain.
//			}
//			link.setStackNode(node);
//		}
//	}
	
//	/**
//	 * finishes off the active route.  If another route has been stacked,
//	 * it is popped from the route stack and made the active route.
//	 * @param node is the RouteNode that is expected to be active
//	 */
//	public void clearActiveRoute(final RouteNode node) {
//		if (ActiveRoute == node) {
//			if (!(RouteStack == null)) {
//				ActiveRoute = RouteStack;
//				RouteStack = RouteStack.getStackNode();
//			}
//			else {
//				ActiveRoute = null;
//			}
//		}
//	}
		
//	/**
//	 * a query to determine if the node stack is empty
//	 */
//	public boolean isStackEmpty() {
//		return RouteStack == null;
//	}

	
	/**
	 * retrieves the list of opposing CPEdges between this RouteEdge and the next in advance.
	 * @return if the list is not empty, then the list; else null.
	 */
	public ArrayList<CPEdge> getOpposingEdges() {
		if ((OpposingSignalList != null) && (OpposingSignalList.size() > 0)) {
			return OpposingSignalList;
		}
		return null;
	}
	
//	/**
//	 * a mapping routine similar to startSearch(), but used for mapping RouteEdges
//	 * in advance of a CPEdge, up to the next CPEdge, if it exists.
//	 */
//	public void findAdvanceRouteEdge() {
//		if (OpposingEdges == null) {
//			NextRouteEdge = findNextRouteEdge();
//			if (NextRouteEdge != null) {
//				if (NextRouteEdge instanceof PtsEdge) {
//					((PtsEdge) NextRouteEdge).findAdvanceFrogEdge();
//				}
//				else if (NextRouteEdge instanceof FrogEdge) {
//					((FrogEdge) NextRouteEdge).findAdvancePtsEdge();
//				}
//			}
//		}
//	}
	/**************************************************************************
	 * End of RouteEdge methods
	 **************************************************************************/
	
	/**
	 * this method clears the Locks (and any edge specific variables) not tied to
	 * hardware.  It is intended to clean things up when the GUI gets confused.
	 * It should be invoked only when everything else fails.
	 * 
	 * CommonLocks are not cleared because they are set/cleared by something other
	 * than this edge
	 */
	public void resetLocks() {
		for (GuiLocks lock : GuiLocks.SoftLocks) {
			if (EdgeLocks.contains(lock)) {
				clearEdgeLock(lock);
			}
		}
		if (CommonLocks.contains(GuiLocks.ENTRYLOCK)) {
			clearCommonLock(GuiLocks.ENTRYLOCK);
		}
		if (CommonLocks.contains(GuiLocks.EXITLOCK)) {
			clearCommonLock(GuiLocks.EXITLOCK);
		}
		setRouteAnchor(null);
	}
	
	/***************************************************************************
	 * the following four classes are used for walking through all the Tracks
	 * in advance (behind) this TrackEdge.	 
	 ***************************************************************************/
	
	/***************************************************************************
	 * Enumerates through the Tracks from the TrackEdge to the first opposing
	 * TrackEdge behind it.
	 ***************************************************************************/
	public class SequenceWalker implements Enumeration<Track> {
		/**
		 * the index into TrackSpan of the next Track to hand out
		 */
		private int nextTrack = 0;
		
		@Override
		public boolean hasMoreElements() {
			return nextTrack <  TrackSpan.size();
		}

		@Override
		public Track nextElement() {
			if (nextTrack < TrackSpan.size()) {
				return TrackSpan.get(nextTrack++);
			}
			return null;
		}
		
		/**
		 * is a query for the opposing TrackEdge on the very
		 * last Track handed out.
		 * @return the TrackEdge on the other end of this sequence
		 */
		public AbstractTrackEdge lastEdge() {
			return OppositeEnd;
		}
		
	}
	
	/***************************************************************************
	 * Enumerates through the Tracks from the TrackEdge to the first opposing
	 * BlkEdge behind it.
	 ***************************************************************************/
	public class BlockWalker implements Enumeration<Track> {
		
		// the opening AbstractTrackEdge of the sequence being walked
		private SecEdge firstEdge = AbstractTrackEdge.this;
		
		// the current sequence walker
		private SequenceWalker walker = new SequenceWalker();
		
		// the terminating AbstractTrackEdge of the last sequence walked
		private AbstractTrackEdge lastEdge = OppositeEnd;
		
		@Override
		public boolean hasMoreElements() {
			if (walker.hasMoreElements()) {
				return true;
			}
			else {
				lastEdge = walker.lastEdge();
				if ((lastEdge != null) && (!lastEdge.isBlkEdge())) {
					firstEdge = lastEdge.NeighborEdge;
					if ((firstEdge != null) && (firstEdge instanceof AbstractTrackEdge)) {
						// this kludge is for the case of facing points
						walker = ((AbstractTrackEdge) firstEdge).new SequenceWalker();
						// the following kludge is to handle the special case of facing points.
						// there is probably a better way to handle this case
						if (!walker.hasMoreElements() && (firstEdge instanceof PtsEdge) && 
								(firstEdge.Joint != null) && (firstEdge.Joint instanceof PtsEdge)
								&& (((AbstractTrackEdge) firstEdge.Joint).NeighborEdge != null)) {
							firstEdge = ((AbstractTrackEdge) firstEdge.Joint).NeighborEdge;
							walker = ((AbstractTrackEdge) firstEdge).new SequenceWalker();
						}
					}
				}				
			}
			return walker.hasMoreElements();
		}

		@Override
		public Track nextElement() {
			if (walker.hasMoreElements()) {
				return walker.nextElement();
			}
			return null;
		}
				
		/**
		 * is a query for the opposing TrackEdge on the very
		 * last Track handed out.
		 * @return the TrackEdge on the other end of this sequence
		 */
		public AbstractTrackEdge lastEdge() {
			return lastEdge;
		}		
	}
	
	/***************************************************************************
	 * Enumerates through the Tracks from the TrackEdge to the first Control Point
	 ***************************************************************************/
	public class RouteWalker extends BlockWalker {
		
		// the enumerator over blocks in the route
		private BlockWalker route = new BlockWalker();
		
		@Override
		public boolean hasMoreElements() {
			AbstractTrackEdge exitEdge;
			AbstractTrackEdge nextEdge;
			if (route.hasMoreElements()) {
				return true;
			}
			else {
				exitEdge = route.lastEdge();
				if ((exitEdge != null) && ((nextEdge = exitEdge.NeighborEdge) != null) &&
						!nextEdge.getEdgeType().equals(EDGE_TYPE.CP_EDGE)) {
					route = nextEdge.new BlockWalker();
				}
			}
			return route.hasMoreElements();
		}
		
		@Override
		public Track nextElement() {
			if (route.hasMoreElements()) {
				return route.nextElement();
			}
			return null;
		}
		
		@Override
		public AbstractTrackEdge lastEdge() {
			return route.lastEdge();
		}
	}
	
	/***************************************************************************
	 * Enumerates through the entry edges behind (in advance of) this edge.
	 * These are the feeders for this edge - the potential signals that
	 * a train entering through this edge will encounter.
	 * It just navigates OppositeEdge/NeighborEdge pairs.  
	 * 
	 * This method walks the entry edges - which are assured to be TrackEdges
	 * (or derivatives).  Because designer does not enforce a TrackEdge at the
	 * end of track, an exit TrackEdge may not exist.  This could create an NPE
	 * when looking for tracks based solely on exit edge.
	 * 
	 * This edge is the first edge returned by nextElement().
	 ***************************************************************************/
	public class EntryEdgeWalker implements Enumeration<AbstractTrackEdge> {

		// the opening AbstractTrackEdge of the sequence being walked
		private AbstractTrackEdge EntryEdge = AbstractTrackEdge.this;

		// the terminating AbstractTrackEdge of the last sequence walked
		private AbstractTrackEdge ExitEdge = OppositeEnd;

		@Override
		public boolean hasMoreElements() {
			return EntryEdge != null;
		}

		@Override
		public AbstractTrackEdge nextElement() {
			AbstractTrackEdge lastEdge;
			if (EntryEdge == null) {
				return null;
			}
			else {
				lastEdge = EntryEdge;
				ExitEdge = EntryEdge.OppositeEnd;
				if (ExitEdge == null) {
					EntryEdge = null;
				}
				else {
//					// the following kludge is to handle the special case of facing points.
//					// there is probably a better way to handle this case
//					if ((ExitEdge instanceof PtsEdge) && 
//							(ExitEdge.Joint != null) && (ExitEdge.Joint instanceof PtsEdge)
//							&& (((AbstractTrackEdge) ExitEdge.Joint).NeighborEdge != null)) {
//						ExitEdge = ((AbstractTrackEdge) ExitEdge.Joint).NeighborEdge;
//					}
					EntryEdge = ExitEdge.NeighborEdge;
				}				
			}
			return lastEdge;
		}
	}
	
//	/***************************************************************************
//	 * Enumerates through the exit edges behind (in advance of) this edge.
//	 * These are the opposite protection edges for the track protected by
//	 * this edge. Normally, it just navigates ApproachEdges.  There are complications:
//	 * - end of track - no neighbor
//	 * - end of track - last edge is a SecEdge
//	 * - points/frogs
//	 * In the cases of the latter, it stitches them together.
//	 * 
//	 * This method walks the exit edges - which are not assured to be block edges
//	 * (or derivatives).  Because designer does not enforce a block edge at the
//	 * end of track, an exit block edge may not exist.  This could create a gap
//	 * when looking for tracks based solely on exit edge.  Furthermore, one
//	 * can not use the EntryEdgeWalker and "backup" to the neighbor because the
//	 * next entry edge may not exist.
//	 * 
//	 * The AbstractTrackEdge at the end of the starting edge (if it exists) is
//	 * the first edge returned.
//	 ***************************************************************************/
//	public class ExitEdgeWalker implements Enumeration<AbstractTrackEdge> {
//
//		// the opening AbstractTrackEdge of the sequence being walked
//		private AbstractTrackEdge EntryEdge = AbstractTrackEdge.this;
//
//		// the terminating AbstractTrackEdge of the last sequence walked
//		private AbstractTrackEdge ExitEdge = OppositeEnd;
//
//		@Override
//		public boolean hasMoreElements() {
//			return ExitEdge != null;
//		}
//
//		@Override
//		public AbstractTrackEdge nextElement() {
//			AbstractTrackEdge lastEdge;
//			if (ExitEdge == null) {
//				return null;
//			}
//			else {
//				lastEdge = ExitEdge;
//				EntryEdge = ExitEdge.NeighborEdge;
//				if (EntryEdge == null) {
//					ExitEdge = null;
//				}
//				else {
////					// the following kludge is to handle the special case of facing points.
////					// there is probably a better way to handle this case
////					if ((EntryEdge instanceof PtsEdge) && 
////							(EntryEdge.Joint != null) && (EntryEdge.Joint instanceof PtsEdge)
////							&& (((AbstractTrackEdge) EntryEdge.Joint).NeighborEdge != null)) {
////						EntryEdge = ((AbstractTrackEdge) EntryEdge.Joint).NeighborEdge;
////					}
//					ExitEdge = EntryEdge.OppositeEnd;
//				}				
//			}
//			return lastEdge;
//		}
//	}
	
//	/***************************************************************************
//	 * walks through the entry edges on a Route.  The first edge is easy - the code
//	 * knows where it wants to start.  The enumeration isn't hard - follow the
//	 * EntryEdgeWalker.  Knowing when to quit is the harder part.  CTC
//	 * routes don't distinguish between BlkEdges and IntermediateEdges.  A block
//	 * is a block is a block.  The locks are merged.  So, the core piece of a route
//	 * is from the entry edge to the first opposing control point (or end of track).
//	 * 
//	 * However, if the exit edge is not itself a control point, then there is a 
//	 * pseudo route up to the next exit edge that is a control point.  This is
//	 * to stop an opposing train from passing the terminating control point before
//	 * it sees a signal.
//	 * 
//	 * This edge is the first edge returned by nextElement().  The ending control
//	 * point edge is not returned by nextElement().  Because clients may want to
//	 * know why the Enumeration ended, I added a method (getLastEdge()) that returns
//	 * the last edge visited.  Thus, if there is no more edge, it will return null.
//	 * If the Enumeration ended because a control point was found, the control
//	 * point is returned.
//	 ****************************************************************************/
//	public class RouteEnumerator implements Enumeration<AbstractTrackEdge> {
//
//		// the next AbstractTrackEdge passed out
//		private AbstractTrackEdge NextEdge;
//		
//		// the last AbstractTrackEdge passed out
//		private AbstractTrackEdge LastEdge = null;
//		
//		// the reason for the Enumeration to end
//		private AbstractTrackEdge FinalEdge = null;
//		
//		// the EdgeWalker - for accessing the next entry edge in advance
//		private final EntryEdgeWalker Walker = new EntryEdgeWalker();
//		
//		public RouteEnumerator() {
//			// this fills the pipeline
//			NextEdge = Walker.nextElement();
//		}
//		
//		@Override
//		public boolean hasMoreElements() {
//			return (NextEdge != null);
//		}
//
//		@Override
//		public AbstractTrackEdge nextElement() {
//			if (NextEdge != null) {
//				LastEdge = NextEdge;
//				if ((!Walker.hasMoreElements() || (NextEdge = Walker.nextElement()) == null) ||
//						NextEdge.getEdgeType().equals(EDGE_TYPE.CP_EDGE)) {
//					FinalEdge = NextEdge;
//					NextEdge = null;
//				}
//			}
//			return LastEdge;
//		}
//		
//		/**
//		 * returns the edge that the Enumeration terminated on
//		 * @return the control point in advance or null
//		 */
//		public AbstractTrackEdge getLastEdge() {
//			return FinalEdge;
//		}
//	}
	
	/**
	 * is a query for checking if a block is safe - meaning that it has no hazards
	 * (such as occupancy, conflicting signals, etc.).  Starting at the current
	 * AbstractTrackEdge, it follows the entry (same side of the track) edges in
	 * advance until:
	 * a. there are no more
	 * b. a BlkEdge (or derivative) is encountered
	 * c. an obstruction is encountered
	 * 
	 * @return true for the first two cases and false for the last
	 */
	public boolean isBlockSafe() {
		// the EdgeWalker - for accessing the next entry edge in advance
		EntryEdgeWalker walker = new EntryEdgeWalker();

		// the edge being examined
		AbstractTrackEdge edge = walker.nextElement();
		do {
			if (!GuiLocks.intersection(GuiLocks.ObstructedBlock, edge.EdgeLocks).isEmpty()) {
				return false;
			}
		} while (walker.hasMoreElements() && (!((edge = walker.nextElement()).isBlkEdge())
				|| edge instanceof XBlkEdge));
		return true;
//		return GuiLocks.intersection(EdgeLocks, GuiLocks.ObstructedBlock).isEmpty() && GuiLocks.intersection(CommonLocks, GuiLocks.ObstructedBlock).isEmpty();
	}
	
//	/***************************************************************************
//	 * is a companion Enumeration to RouteEnumerator in that it picks up where the
//	 * former ended, looking for an exit edge that is a control point - it walks
//	 * the pseudo route, completing the route.
//	 * 
//	 * If the starting edge has an OppositeEnd that is an AbstractTrackEdge,
//	 * that edge is the first one returned.
//	 ***************************************************************************/
//	public class OverlapEnumerator implements Enumeration<AbstractTrackEdge> {
//
//		// the next AbstractTrackEdge passed out
//		private AbstractTrackEdge NextEdge = null;
//		
//		// the last AbstractTrackEdge passed out
//		private AbstractTrackEdge LastEdge = null;
//		
//		// the TrackWalker - for accessing the next exit edge in advance
//		private ExitEdgeWalker Walker = new ExitEdgeWalker();
//		
//		public OverlapEnumerator() {
//			if (Walker.hasMoreElements()) {
//				NextEdge = Walker.nextElement();
//			}
//		}
//		
//		@Override
//		public boolean hasMoreElements() {
//			return (NextEdge != null);
//		}
//
//		@Override
//		public AbstractTrackEdge nextElement() {
//			if (NextEdge != null) {
//				LastEdge = NextEdge;
//				if (LastEdge.getEdgeType().equals(EDGE_TYPE.CP_EDGE)) {
//					NextEdge = null;
//				}
//				else {
//					NextEdge = Walker.nextElement();
//				}
//			}
//			else {
//				NextEdge = null;
//			}
//			return LastEdge;
//		}
//	}
	/***************************************************************************
	 * defines the field that holds the lock.  the other methods are abstract because
	 * the actions taken by each lock are different.  It is tempting to set or clear
	 * GuiLock here, but that is handled within setGui()/clearGui() in such a manner
	 * that most locks do not need specific handlers.
	 **************************************************************************/
	protected abstract class AbstractLockIndicationHandler implements LockIndicationHandler {
		/**
		 * the lock being watched
		 */
		protected final GuiLocks WatchedLock;
		
		/**
		 * the ctor
		 * @param lock is the GuiLocks element handled
		 */
		public AbstractLockIndicationHandler(GuiLocks lock) {
			WatchedLock = lock;
		}

		@Override
		abstract public void requestLockSet();

		@Override
		abstract public void requestLockClear();

		@Override
		abstract public void advanceLockSet();

		@Override
		abstract public void advanceLockClear();

		@Override
		public GuiLocks getLock() {
			return WatchedLock;
		}
		
//		@Override
//		abstract public void blockLock();
//		
//		@Override
//		abstract public void unblockLock();
	}
	/***************************************************************************
	 * end of AbstractLockIndicationHandler
	 **************************************************************************/

	/**************************************************************************
	 * DefaultLockIndicationHandler - the simplest LockIndicationHandler - it does nothing.
	 * If a lock has no affect on GUI, then it need not be defined (the handler can be set to
	 * null).  However, this provides a handler that can be decorated.
	 **************************************************************************/
	protected class DefaultLockIndicationHandler extends AbstractLockIndicationHandler {

		/**
		 * the ctor
		 * @param lock is the LogicLocks element handled
		 */
		public DefaultLockIndicationHandler(GuiLocks lock) {
			super(lock);
		}

		@Override
		public void requestLockSet() {			
		}

		@Override
		public void requestLockClear() {
		}

		@Override
		public void advanceLockSet() {
		}

		@Override
		public void advanceLockClear() {
		}
	}
	/***************************************************************************
	 * end of DefaultLockIndicationHandler
	 **************************************************************************/
	
	/**************************************************************************
	 * BasicLockIndicationHandler - it just records the Lock change of state
	 **************************************************************************/
	protected class BasicLockIndicationHandler extends AbstractLockIndicationHandler {

		/**
		 * the ctor
		 * @param lock is the LogicLocks element handled
		 */
		public BasicLockIndicationHandler(GuiLocks lock) {
			super(lock);
		}

		@Override
		public void requestLockSet() {	
			EdgeLocks.add(WatchedLock);
		}

		@Override
		public void requestLockClear() {
			EdgeLocks.remove(WatchedLock);
		}

		@Override
		public void advanceLockSet() {
			CommonLocks.add(WatchedLock);
		}

		@Override
		public void advanceLockClear() {
			CommonLocks.remove(WatchedLock);
		}
	}
	/***************************************************************************
	 * end of DefaultLockIndicationHandler
	 **************************************************************************/
	
//	/**************************************************************************
//	 * DetectionLockIndicationHandler - it processes the side effects of a
//	 * block reporting occupied and unoccupied.
//	 **************************************************************************/
//	protected class DetectionLockIndicationHandler extends BasicLockIndicationHandler {
//
//		/**
//		 * the ctor
//		 */
//		public DetectionLockIndicationHandler() {
//			super(GuiLocks.DETECTIONLOCK);
//		}
//
//		@Override
//		public void requestLockSet() {
//			super.requestLockSet();
//			if (CommonLocks.contains(GuiLocks.ENTRYLOCK)) {
//				setEdgeLock(GuiLocks.ENTRYLOCK);
//			}
//			if (CommonLocks.contains(GuiLocks.EXITLOCK)) {
//				setEdgeLock(GuiLocks.EXITLOCK);
//			}
//		}
//		
//		@Override
//		public void requestLockClear() {
//			super.requestLockClear();
//			clearEdgeLock(GuiLocks.ENTRYLOCK);
//			clearEdgeLock(GuiLocks.EXITLOCK);
//		}
//	}
//	/***************************************************************************
//	 * end of DefaultLockIndicationHandler
//	 **************************************************************************/
	
	/**************************************************************************
	 * AbstractHandlerDecorator is an abstract class from which other IndicationHandler
	 *  decorators are derived.
	 * 
	 * As described in Design Patterns, a decorator is a class that wraps another class,
	 * thus, extending its functionality without using inheritance.  To make this work
	 * the decorator and the decorated class must implement the same interface; thus, the
	 * decorator can be used in place of the decorated.
	 * 
	 * 
	 **************************************************************************/
	protected abstract class AbstractHandlerDecorator implements LockIndicationHandler {
		
		/**
		 * the handler being decorated
		 */
		final protected LockIndicationHandler Handler;

		/**
		 * the Lock being encapsulated
		 */
		final protected GuiLocks WatchedLock;

		/**
		 * the ctor
		 * @param handler is the lock handler being decorated
		 */
		public AbstractHandlerDecorator(LockIndicationHandler handler) {
			Handler = handler;
			WatchedLock = handler.getLock();
		}

		@Override
		abstract public void requestLockSet();

		@Override
		abstract public void requestLockClear();

		@Override
		abstract public void advanceLockSet();

		@Override
		abstract public void advanceLockClear();
//		
//		@Override
//		public void blockLock() {
//			Handler.blockLock();
//		}
//		
//		@Override
//		public void unblockLock() {
//			Handler.unblockLock();
//		}
//
		@Override
		public GuiLocks getLock() {
			return WatchedLock;
		}
	}
	/***************************************************************************
	 * end of AbstractHandlerDecorator
	 **************************************************************************/

	/**************************************************************************
	 * PropagationDecorator - this is a decorator on a Lock Handler to
	 * simulate polymorphism in Java.  Its purpose is to forward the lock to
	 * the Edge in approach.  The rules is:
	 * Edge level locks are degraded in importance and forwarded as common
	 * locks.  Common locks are forwarded as common locks.  This creates
	 * two ways for a common lock to be set, resulting in ambiguity.
	 **************************************************************************/
	protected class PropagationDecorator extends AbstractHandlerDecorator {

		//		/**
		//		 * is true if the Lock should not propagate to the edge in advance
		//		 */
		//		protected boolean Blocked = false;

		/**
		 * the ctor
		 * @param handler is the lock handler being decorated
		 */
		public PropagationDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {			
			if (ApproachEdge != null) {
				//				if ((ApproachEdge != null) && !TrafficLocks.contains(WatchedLock) && !Blocked) {
				ApproachEdge.setCommonLock(WatchedLock);
			}
			Handler.requestLockSet();
		}

		@Override
		public void requestLockClear() {
			if (ApproachEdge != null) {
				//				if ((ApproachEdge != null) && !TrafficLocks.contains(WatchedLock)) {
				ApproachEdge.clearCommonLock(WatchedLock);
			}
			Handler.requestLockClear();
		}

		@Override
		public void advanceLockSet() {
			if (ApproachEdge != null) {
				//				if ((ApproachEdge != null) && !TrafficLocks.contains(WatchedLock) && !Blocked) {
				ApproachEdge.setCommonLock(WatchedLock);
			}
			Handler.advanceLockSet();
		}

		@Override
		public void advanceLockClear() {
			if (ApproachEdge != null) {
				//				if ((ApproachEdge != null) && !TrafficLocks.contains(WatchedLock)) {
				ApproachEdge.clearCommonLock(WatchedLock);
			}
			Handler.advanceLockClear();
		}
//		
//		@Override
//		public void blockLock() {
//			Blocked = true;
//			if ((EdgeLocks.contains(WatchedLock) || CommonLocks.contains(WatchedLock)) &&
//					(ApproachEdge != null)) {
//				ApproachEdge.clearCommonLock(WatchedLock);
//			}
//		}
//		
//		@Override
//		public void unblockLock() {
//			Blocked = false;
//			if ((EdgeLocks.contains(WatchedLock) || CommonLocks.contains(WatchedLock)) &&
//					(ApproachEdge != null)) {
//				ApproachEdge.setCommonLock(WatchedLock);
//			}
//		}
	}
	/***************************************************************************
	 * end of PropagationDecorator
	 **************************************************************************/
	
	/**************************************************************************
	 * DetectionDecorator - it processes the side effects of a
	 * block reporting occupied and unoccupied.
	 **************************************************************************/
	protected class DetectionDecorator extends AbstractHandlerDecorator {

		/**
		 * the ctor
		 */
		public DetectionDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {
			if (CommonLocks.contains(GuiLocks.ENTRYLOCK)) {
				setEdgeLock(GuiLocks.ENTRYLOCK);
			}
			if (CommonLocks.contains(GuiLocks.EXITLOCK)) {
				setEdgeLock(GuiLocks.EXITLOCK);
			}
			Handler.requestLockSet();
		}
		
		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
			clearEdgeLock(GuiLocks.ENTRYLOCK);
			clearEdgeLock(GuiLocks.EXITLOCK);
		}
		
		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
		}
		
		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
		}
	}
	/***************************************************************************
	 * end of DefaultLockIndicationHandler
	 **************************************************************************/
	
	/**************************************************************************
	 * A decorator for merging the affects of setting/clearing the edge lock
	 * or the common lock.
	 * 1. a lock is set only if the other (common for edge locks and edge for
	 * common locks) has not been set
	 * 2. a lock is cleared only if the other (common for edge locks and edge for
	 * common locks) has not been set
	 * 
	 * If one lock (Edge or Common) should always be processed, then its decorator
	 * should decorate this filter.
	 **************************************************************************/
	protected class MergeDecorator extends AbstractHandlerDecorator {

		/**
		 * the ctor
		 * @param handler is the lock handler being decorated
		 */
		public MergeDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {			
			if (!CommonLocks.contains(WatchedLock) && !EdgeLocks.contains(WatchedLock)) {
				Handler.requestLockSet();
			}
			else {
				EdgeLocks.add(WatchedLock);
			}
		}

		@Override
		public void requestLockClear() {
			if (!CommonLocks.contains(WatchedLock) && EdgeLocks.contains(WatchedLock)) {
				Handler.requestLockClear();
			}
			else {
				EdgeLocks.remove(WatchedLock);
			}
		}

		@Override
		public void advanceLockSet() {
			if (!CommonLocks.contains(WatchedLock) && !EdgeLocks.contains(WatchedLock)) {
				Handler.advanceLockSet();
			}
			else {
				CommonLocks.add(WatchedLock);
			}
		}

		@Override
		public void advanceLockClear() {
			if (CommonLocks.contains(WatchedLock) && !EdgeLocks.contains(WatchedLock)) {
				Handler.advanceLockClear();
			}
			else {
				CommonLocks.remove(WatchedLock);
			}
		}
	}
	/***************************************************************************
	 * end of MergeDecorator
	 **************************************************************************/
	
	/**************************************************************************
	 * A decorator for EdgeLocks only.  Changes in CommonLocks does nothing; thus,
	 * if actions are needed when CommonLocks are changed, those decorators
	 * should decorate before this one.  This decorator must decorate a handler
	 * that adds/removes the EdgeLock.
	 **************************************************************************/
	protected class EdgeLocksDecorator extends AbstractHandlerDecorator {

		/**
		 * the ctor
		 * @param handler is the lock handler being decorated
		 */
		public EdgeLocksDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {			
			if (!EdgeLocks.contains(WatchedLock)) {
				Handler.requestLockSet();
			}
		}

		@Override
		public void requestLockClear() {
			if (EdgeLocks.contains(WatchedLock)) {
				Handler.requestLockClear();
			}
		}

		@Override
		public void advanceLockSet() {
			CommonLocks.add(WatchedLock);
		}

		@Override
		public void advanceLockClear() {
			CommonLocks.remove(WatchedLock);
		}
	}
	/***************************************************************************
	 * end of EdgeLocksDecorator
	 **************************************************************************/
	
	/**************************************************************************
	 * A decorator for CommonLocks only.  Changes in EdgeLocks does nothing; thus,
	 * if actions are needed when EdgeLocks are changed, those decorators
	 * should decorate before this one.
	 **************************************************************************/
	protected class CommonLocksDecorator extends AbstractHandlerDecorator {

		/**
		 * the ctor
		 * @param handler is the lock handler being decorated
		 */
		public CommonLocksDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {			
			EdgeLocks.add(WatchedLock);
		}

		@Override
		public void requestLockClear() {
			EdgeLocks.remove(WatchedLock);
		}

		@Override
		public void advanceLockSet() {
			if (!CommonLocks.contains(WatchedLock)) {
				Handler.advanceLockSet();
			}
			CommonLocks.add(WatchedLock);
		}

		@Override
		public void advanceLockClear() {
			if (CommonLocks.contains(WatchedLock)) {
				Handler.advanceLockClear();
			}
			CommonLocks.remove(WatchedLock);
		}
	}
	/***************************************************************************
	 * end of CommonLocksDecorator
	 **************************************************************************/
	
	/**************************************************************************
	 * CrossingDecorator - this is a decorator on a Lock Handler to
	 * simulate polymorphism in Java.  It resides on one of the four edges
	 * of a crossing.  Its purpose is to block cross traffic when its lock
	 * is set.
	 * <p>
	 * Care must be taken that a lock can be set from multiple sources and should
	 * remain set until the last source removes it.  For example, both sides of
	 * a crossing set a common lock on both sides of the perpendicular track.
	 * <p>
	 * Care must also be taken with shared locks - for example a single detector
	 * could trip all four edges on a crossing.  Beware of recursion!
	 **************************************************************************/
	protected class CrossingDecorator extends AbstractHandlerDecorator {

		// the Edges on the perpendicular crossing
		final protected AbstractTrackEdge Crossing[] = new AbstractTrackEdge[2];;
		
		// the lock to use on the crossing edges
		final protected GuiLocks CrossLock;
		
		/**
		 * the ctor
		 * @param handler is the lock handler being decorated
                 * @param crossing is an array containing the perpendicular crossing edges
                 * @param crossLock is the GUI lock to apply to the crossing edges
		 */
		public CrossingDecorator(LockIndicationHandler handler, XTrackEdge crossing[], GuiLocks crossLock) {
			super(handler);
			Crossing[0] = (AbstractTrackEdge) crossing[0];
			Crossing[1] = (AbstractTrackEdge) crossing[1];
			CrossLock = crossLock;
		}

		@Override
		public void requestLockSet() {			
			Handler.requestLockSet();
			Crossing[0].setEdgeLock(CrossLock);
			Crossing[1].setEdgeLock(CrossLock);
		}

		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
			Crossing[0].clearEdgeLock(CrossLock);
			Crossing[1].clearEdgeLock(CrossLock);
		}

		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
			Crossing[0].setEdgeLock(CrossLock);
			Crossing[1].setEdgeLock(CrossLock);
		}

		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
			Crossing[0].clearEdgeLock(CrossLock);
			Crossing[1].clearEdgeLock(CrossLock);
		}
	}
	/***************************************************************************
	 * end of CrossingDecorator
	 **************************************************************************/

	/**************************************************************************
	 * A generic decorator that propagates a lock over the Protection Zone from a
	 * TrackEdge up to the next facing TrackEdge (in the opposite direction).  The
	 * lock comes in on EdgeLocks and out on the OppositeEnd's CommonLocks.  This
	 * asymmetric behavior is to make Protection Zone locks directional.
	 **************************************************************************/
	protected class ProtectionZoneDecorator extends AbstractHandlerDecorator {
		public ProtectionZoneDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {
			Handler.requestLockSet();
			if (OppositeEnd != null) {
				OppositeEnd.setCommonLock(WatchedLock);
			}
		}

		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
			if (OppositeEnd != null) {
				OppositeEnd.clearCommonLock(WatchedLock);
			}
		}

		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
			if ((NeighborEdge != null) && !(NeighborEdge instanceof CPEdge)) {
				NeighborEdge.setEdgeLock(WatchedLock);
			}
		}

		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
			if ((NeighborEdge != null) && !(NeighborEdge instanceof CPEdge)) {
				NeighborEdge.clearEdgeLock(WatchedLock);
			}
		}
	}
	/***************************************************************************
	 * end of ProtectionZoneDecorator
	 **************************************************************************/
	
//	/**************************************************************************
//	 * A generic decorator that propagates a lock over the other half of a Safety
//	 * Zone, from a TrackEdge across the NeighborEdge.  The lock comes in on
//	 * CommonLocks and out on the NeighborEdge's EdgeLocks; thus, it complements
//	 * ProtectionZoneDecorator.
//	 **************************************************************************/
//	protected class SafetyZoneDecorator extends AbstractHandlerDecorator {
//		public SafetyZoneDecorator(LockIndicationHandler handler) {
//			super(handler);
//		}
//
//		@Override
//		public void requestLockSet() {
//			Handler.requestLockSet();
//		}
//
//		@Override
//		public void requestLockClear() {
//			Handler.requestLockClear();
//		}
//
//		@Override
//		public void advanceLockSet() {
//			Handler.advanceLockSet();
//			NeighborEdge.setEdgeLock(WatchedLock);
//		}
//
//		@Override
//		public void advanceLockClear() {
//			Handler.advanceLockClear();
//			NeighborEdge.clearEdgeLock(WatchedLock);
//		}
//	}
//	/***************************************************************************
//	 * end of SafetyZoneDecorator
//	 **************************************************************************/

	/**************************************************************************
	 * the ExitDecorator.  It sets the exit lock on a block and the entry lock on
	 * its neighbor.  It should be decorated with a MergeDecorator.
	 **************************************************************************/
	protected class ExitDecorator extends AbstractHandlerDecorator {
		public ExitDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {
			Handler.requestLockSet();
			NeighborEdge.setEdgeLock(GuiLocks.ENTRYLOCK);
		}

		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
			NeighborEdge.clearEdgeLock(GuiLocks.ENTRYLOCK);
		}

		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
			NeighborEdge.setCommonLock(GuiLocks.ENTRYLOCK);
		}

		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
			NeighborEdge.clearCommonLock(GuiLocks.ENTRYLOCK);
		}
	}
	/***************************************************************************
	 * end of ExitDecorator
	 **************************************************************************/
	
	/**************************************************************************
	 * the BlockEntry decorator.  It turns colors the track.
	 * It should be decorated with a MergeDecorator.
	 **************************************************************************/
	protected class EntryDecorator extends AbstractHandlerDecorator {
		public EntryDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {
			Handler.requestLockSet();
//			paintTrackBlock(RouteDecoration.SET_ROUTE);
			paintTrackSequence(RouteDecoration.SET_ROUTE);
			setOppositeExit();
		}

		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
//			paintTrackBlock(RouteDecoration.CLEAR_ROUTE);
			paintTrackSequence(RouteDecoration.CLEAR_ROUTE);
			clearOppositeExit();
		}

		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
//			paintTrackBlock(RouteDecoration.SET_ROUTE);
			paintTrackSequence(RouteDecoration.SET_ROUTE);
			setOppositeExit();
		}

		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
//			paintTrackBlock(RouteDecoration.CLEAR_ROUTE);
			paintTrackSequence(RouteDecoration.CLEAR_ROUTE);
			clearOppositeExit();
		}

		/**
		 * checks the neighbor to see if the route
		 * should be propagated via changing its EntryLock
		 */
		private void setOppositeExit() {
			if (OppositeEnd != null) {
				OppositeEnd.setCommonLock(GuiLocks.EXITLOCK);
			}
		}

		/**
		 * checks the neighbor to see if the route
		 * should be cleared via changing its EntryLock
		 */
		private void clearOppositeExit() {
			if (OppositeEnd != null) {
				OppositeEnd.clearCommonLock(GuiLocks.EXITLOCK);
			}
		}
	}
	/***************************************************************************
	 * end of EntryDecorator
	 **************************************************************************/

	/**************************************************************************
	 * the RouteCheckDecorator.  It decorates the actions when a blocking lock
	 * clears.  It creates and queues a trigger to a stacked route anchor
	 * to check the DAG for a clear path.
	 **************************************************************************/
	protected class RouteCheckerDecorator extends AbstractHandlerDecorator {
		public RouteCheckerDecorator(LockIndicationHandler handler) {
			super(handler);
		}

		@Override
		public void requestLockSet() {
			Handler.requestLockSet();
		}

		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
			if (StackedRoute > 0) {
				StackOfRoutes.triggerBlockCheck();
			}
		}

		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
		}
		
		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
			if (StackedRoute > 0) {
				StackOfRoutes.triggerBlockCheck();
			}
		}
	}
	/***************************************************************************
	 * end of RouteCheckDecorator
	 **************************************************************************/

	/**************************************************************************
	 * the VitalLogicDecorator.  It short circuits the architectural division
	 * between GUI and Vital Logic by directly manipulating the Logic Locks
	 * in the associated Vital Logic.  The Vital Logic must be instantiated
	 * before any of these methods are invoked.
	 **************************************************************************/
	protected class VitalLogicDecorator extends AbstractHandlerDecorator {

		/**
		 * the corresponding Logic Lock in the Vital Logic
		 */
		private final LogicLocks VLLock;

		/**
		 * the ctor
		 * @param handler is the GUI lock handler being decorated
		 * @param vlLock is the corresponding Logic Lock in the Vital Logic
		 */
		public VitalLogicDecorator(LockIndicationHandler handler, final LogicLocks vlLock) {
			super(handler);
			VLLock = vlLock;
		}

		@Override
		public void requestLockSet() {
			Handler.requestLockSet();
			MyLogic.setLocalLock(VLLock);
		}

		@Override
		public void requestLockClear() {
			Handler.requestLockClear();
			MyLogic.clearLocalLock(VLLock);
		}

		@Override
		public void advanceLockSet() {
			Handler.advanceLockSet();
//			MyLogic.setLocalLock(VLLock);
		}

		@Override
		public void advanceLockClear() {
			Handler.advanceLockClear();
//			MyLogic.clearLocalLock(VLLock);
		}
	}
	
	/***************************************************************************
	 * end of RouteCheckDecorator
	 **************************************************************************/
//	/**************************************************************************
//	 * OverlapDecorator - this is a decorator on a Lock Handler that replaces
//	 * PropagationDecorator on a BlkEdge when its neighbor is a signal edge.
//	 * It is the GUI counterpart to the Vital Logic OverlapLockRequestProcessor.
//	 **************************************************************************/
//	protected class OverlapDecorator extends PropagationDecorator {
//
//		/**
//		 * the ctor
//		 * @param lock is the LogicLocks element handled
//		 */
//		public OverlapDecorator(LockIndicationHandler handler) {
//			super(handler);
//		}
//
//		@Override
//		public void requestLockSet() {			
//			if (!CommonLocks.contains(WatchedLock)) {
//				if ((ApproachEdge != null) && !TrafficLocks.contains(WatchedLock) && !Blocked) {
//					setOverlapLock(WatchedLock);
//				}
//				Handler.requestLockSet();
//			}
//		}
//
//		@Override
//		public void requestLockClear() {
//			if (!CommonLocks.contains(WatchedLock)) {
//				if ((ApproachEdge != null) && !TrafficLocks.contains(WatchedLock)) {
//					clearOverlapLock(WatchedLock);
//				}
//				Handler.requestLockClear();
//			}
//		}
//
//		@Override
//		public void advanceLockSet() {
//			if (!EdgeLocks.contains(WatchedLock)) {
//				if ((ApproachEdge != null) && !TrafficLocks.contains(WatchedLock) && !Blocked) {
//					setOverlapLock(WatchedLock);
//				}
//				Handler.advanceLockSet();
//			}
//		}
//
//		@Override
//		public void advanceLockClear() {
//			if (!EdgeLocks.contains(WatchedLock)) {
//				if ((ApproachEdge != null) && !TrafficLocks.contains(WatchedLock)) {
//					clearOverlapLock(WatchedLock);
//				}
//				Handler.advanceLockClear();
//			}
//		}
//		
//		@Override
//		public void blockLock() {
//			Blocked = true;
//			if ((EdgeLocks.contains(WatchedLock) || CommonLocks.contains(WatchedLock)) &&
//					(ApproachEdge != null)) {
//				ApproachEdge.clearCommonLock(WatchedLock);
//			}
//		}
//		
//		@Override
//		public void unblockLock() {
//			Blocked = false;
//			if ((EdgeLocks.contains(WatchedLock) || CommonLocks.contains(WatchedLock)) &&
//					(ApproachEdge != null)) {
//				ApproachEdge.setCommonLock(WatchedLock);
//			}
//		}
//		/**
//		 * handles the case of Protected Track spanning two Safety Zones (i.e. the neighbor
//		 * to this BlkEdge is a signal edge).  This block's locks cannot be merged into the approach
//		 * block's locks.  This block's locks do need to propagate to the Protecting Signal to alert
//		 * opposing trains that there is a STOP condition on this block's side of the track joint.
//		 * 
//		 * This method converts the STOP lock to an OVERLAP lock and passes it along.  There is
//		 * some book work involved in that the reason for the conversion must be remembered.  There
//		 * could be multiple active STOP conditions and clearing one should not clear the OVERLAP.
//		 * @param l is the unterminated lock being set
//		 */
//		protected void setOverlapLock(final LogicLocks l) {
//			setEdgeLock(LogicLocks.OVERLAP);
//		}
//		
//		/**
//		 * handles the case of Protected Track spanning two Safety Zones (i.e. the neighbor
//		 * to this BlkEdge is a signal edge).  This block's locks cannot be merged into the approach
//		 * block's locks.  This block's locks do need to propagate to the Protecting Signal to alert
//		 * opposing trains that there is a STOP condition on this block's side of the track joint.
//		 * 
//		 * This method converts the STOP lock to an OVERLAP lock and removes it.  There is
//		 * some book work involved in that the reason for the conversion must be remembered.  There
//		 * could be multiple active STOP conditions and clearing one should not clear the OVERLAP.
//		 * @param l is the unterminated lock being cleared
//		 */
//		protected void clearOverlapLock(final LogicLocks l) {
//			if (LogicLocks.intersection(CommonLocks, LogicLocks.AbsoluteLocks).isEmpty() &&
//					LogicLocks.intersection(EdgeLocks, LogicLocks.AbsoluteLocks).isEmpty()) {
//				clearEdgeLock(LogicLocks.OVERLAP);
//			}
//		}
//	}
//	/***************************************************************************
//	 * end of OverlapDecorator
//	 **************************************************************************/
}
/* @(#)AbstractTrackEdge.java */
