/* Name: IntermediateEdge.java
 *
 * What:
 * is derived from SecEdge for supporting an intermediate signal.  An intermediate
 * signal is on the layout, but not the dispatcher panel.  The dispatcher can neither
 * see not directly control it.  Thus, an IntermediateEdge, is a BlkEdge with a signal
 * on the layout, but not the dispatcher panel.
 */
package cats.layout.items;

import org.jdom2.Element;

import cats.layout.Discipline;
import cats.layout.codeLine.packetFactory.EdgePacketFactory;
import cats.layout.vitalLogic.BasicVitalLogic;
import cats.layout.vitalLogic.IntermediateVitalLogic;

/**
 * is derived from SecEdge for supporting an intermediate signal.  An intermediate
 * signal is on the layout, but not the dispatcher panel.  The dispatcher can neither
 * see not directly control it.  Thus, an IntermediateEdge, is a BlkEdge with a signal
 * on the layout, but not the dispatcher panel.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2011, 2012, 2018, 2021, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class IntermediateEdge extends BlkEdge implements SignalEdge {

	/**
	 * is the SecSignal associated with the track circuit boundary
	 */
	protected SecSignal MySignal;

	/**
	 * is the IntermediateVitalLogic
	 */
	protected IntermediateVitalLogic MyVitalLogic;
	
	/**
	 * the ctor
	 * @param edge identifies the side of the GridTile the SecEdge is on.
	 * @param shared describes the shared edge, if it is not adjacent.
	 *    A null value means it is adjacent.
	 * @param block is the Block definition.  If it is null, then the
	 *   a default block is created, which may be overwritten during the
	 *   bind process.
	 * @param blkSignal is a Signal.  It may not be null.
	 */
	public IntermediateEdge(int edge, Edge shared, Block block, SecSignal blkSignal) {
		super(edge, shared, block);
		addSignalEdge(this);
		MySignal = blkSignal;
	}

	@Override
	public void clrEdgeHistory() {
		MySignal.clrSignalHistory();
		MySignal.setApproachState(neighborOccupied());
	}

	/**
	 * returns the signal's name
	 * @return the SignalMast user name if the signal has been defined or "unnamed" if it
	 * has not been defined or the name was not defined
	 */
	public String getSignalName() {
		if (MySignal != null) {
			return MySignal.getSignalName();
		}
		return "unnamed";
	}
	
	/**************************************************************************
	 * the implementation of TrackEdge
	 **************************************************************************/
	@Override
	public SignalEdge signalEdgeProbe() {
		return this;
	}

	@Override
	public BasicVitalLogic createVitalLogic() {
		MyVitalLogic = new IntermediateVitalLogic(new EdgePacketFactory(this), toString(),
				MySignal.getLayoutSignal());
		return MyVitalLogic;
	}

	/**************************************************************************
	 *  end of TrackEdge implementation
	 **************************************************************************/

	/*****************************************************************************************
	 * Implementation of SignalEdge
	 *****************************************************************************************/
	/**
	 * this method initiates probing for the signal in approach of this one.
	 * It follows the chain of TrackEdges in approach until one containing
	 * a signal is encountered.  That TrackEdge is returned and becomes
	 * the approach signal.
	 */
	public void discoverApproachSignal() {
		MyVitalLogic.connectSignal();
	}

	/*****************************************************************************************
	 * End of SignalEdge implementation
	 *****************************************************************************************/
//	/*****************************************************************************************
//	 * Implementation of RouteEdge
//	 *****************************************************************************************/
//	@Override
//	public RouteNode createNode(final RouteNode nextNode, final int searchID) {
//		if ((SearchNode == null) || (SearchNode.getRouteNumber() != searchID)) {
//			SearchNode = RouteNode.newNode(this, searchID, nextNode);
//			RouteSearchID = searchID;
//		}
//		return SearchNode;
//	}
//	/*****************************************************************************************
//	 * End of RouteEdge implementation
//	 *****************************************************************************************/

	@Override
	public Element dumpDerivativeState(boolean basic) {
		if (MySignal != null) {
			return MySignal.dumpSignalState(basic);
		}
		return null;
	}

	@Override
	public void setHandlers() {
		super.setHandlers();
		if (!MyBlock.getDiscipline().equals(Discipline.ABS) && !MyBlock.getDiscipline().equals(Discipline.UNDEFINED)) {
			DetectionIndication = new RouteCheckerDecorator(
					new BlkDetectionDecorator(
							new DetectionDecorator(
									new MergeDecorator(
											new BasicLockIndicationHandler(GuiLocks.DETECTIONLOCK)))));
		}
		SwitchUnlockIndication = new RouteCheckerDecorator(
				new MergeDecorator(
						new BasicLockIndicationHandler(GuiLocks.SWITCHUNLOCK)));
		TimeIndication = new RouteCheckerDecorator(
				new EdgeLocksDecorator(
						new BasicLockIndicationHandler(GuiLocks.TIMELOCK)));
		EntryTrafficIndication = new RouteCheckerDecorator(EntryTrafficIndication);
		ExitTrafficIndication = new RouteCheckerDecorator(ExitTrafficIndication);
		LocalControlLock = new RouteCheckerDecorator(
				new MergeDecorator(
						new BasicLockIndicationHandler(GuiLocks.LOCALCONTROLLOCK)));
		ExternalIndication = new RouteCheckerDecorator(
				new MergeDecorator(
						new BasicLockIndicationHandler(GuiLocks.EXTERNALLOCK)));
	}
	
//	@Override
//	protected void setHandlers () {
//		super.setHandlers();
//		if (MyBlock.getDiscipline() == Discipline.APB) {
//			DetectionIndication = new IntermediateDetectionIndication();
//			ApproachIndication = new IntermediateApproachIndication();
//		}
//	}

//	/**************************************************************************
//	 *  lock indication handlers
//	 **************************************************************************/
//	/**
//	 * the Detection Lock handler for APB Intermediates.  It is similar to the BlkEdge
//	 * APB detection handler except on clearing.  The OpposingSignal is not cleared
//	 * if the neighbor is occupied.  This provides the reverse movement protection
//	 * described by Chubb.
//	 */
//	protected class IntermediateDetectionIndication extends BlkDetectionIndication {
//
//		@Override
//		public void requestLockClear() {
//			if (NeighborEdge != null) {
//				NeighborEdge.clearEdgeLock(GuiLocks.APPROACHLOCK);
//			}
//		}
//	}
//
//	/**
//	 * Something needs to clear the OpposingSignal lock when it is not cleared above.
//	 * It is cleared when the Neighbor is unoccupied.
//	 */
//	protected class IntermediateApproachIndication extends DefaultLockIndicationHandler {
//
//		public IntermediateApproachIndication() {
//			super(GuiLocks.APPROACHLOCK);
//		}
//
//		@Override
//		public void requestLockSet() {
//			Tsr.setApproach();
//		}
//		
//		@Override
//		public void requestLockClear() {
//			Tsr.clearApproach();
//		}	 
//	}
}
/* @(#)IntermediateEdge.java */