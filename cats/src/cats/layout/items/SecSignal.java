/* Name: SecSignal.java
 *
 * What:
 *   This class is the container for all the information about a Section's
 *   (actually a Block's) Signal.
 *   <p>
 *   There are two pieces to a SecSignal - the icon description that appears on the
 *   panel and the physical description that is on the layout.  Both pieces are
 *   optional, though one must exist for there to be a signal.  The physical part
 *   is a JMRI SignalMast.
 */

package cats.layout.items;

import org.jdom2.Element;

import cats.layout.SignalTemplate;
import cats.layout.TemplateStore;
import cats.gui.GridTile;
import cats.layout.xml.*;

/**
 *   This class is the container for all the information about a Section's
 *   (actually a Block's) Signal.
 *   <p>
 *   There are two pieces to a SecSignal - the icon description that appears on the
 *   panel and the physical description that is on the layout.  Both pieces are
 *   optional, though one must exist for there to be a signal.  The physical part
 *   is a JMRI SignalMast.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2011, 2012, 2014, 2016, 2020, 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class SecSignal
implements XMLEleObject {
  /**
   * is the tag for identifying a SecSignal in the XMl file.
   */
  static final String XML_TAG = "SECSIGNAL";

  /**
   * is the Name of the SecSignal.
   */
  private String SigName;

  /**
   * describes the Signal's Icon on the dispatcher's panel.
   */
  private PanelSignal MyIcon;

  /**
   * describes the actual signal on the layout.
   */
  private PhysicalSignal MySignal;

  /**
   * is the state of the tracks being protected - those between
   * this signal and the next
   */
  
  /**
   * constructs a SecSignal, given no other information
   */
  public SecSignal() {
    super();
  }

  /**
   * is called to get the physical signal controlled
   * by this SecSignal
   * @return the PhysicalSignal
   */
  public PhysicalSignal getLayoutSignal() {
	  return MySignal;
  }
  
  /**
   * is called to determine if the signal is an intermediate.  An
   * intermediate is a signal on the layout, but not the panel.
   * 
   * @return true if it is an intermediate.
   */
  public boolean isIntermediate() {
    return (MyIcon == null) && (MySignal != null);
  }

  /**
   * is called to determine if the signal is a placeholder or has
   * a presence on the layout.
   *
   * @return true if the signal exists on the panel or layout.
   */
  public boolean isRealSignal() {
    return (MyIcon != null) || (MySignal != null);
  }

  /**
   * retrieves the SecSignal's Icon information.
   *
   * @return the SecSignal's Icon.  Null is a valid value.
   *
   * @see PanelSignal
   */
  public PanelSignal getSigIcon() {
    return MyIcon;
  }

  /**
   * retrieves the SecSignal's name
   * @return the SignalMast user name if it exists or "unnamed" if it does not.
   */
  public String getSignalName() {
	  if ((SigName != null) && !SigName.isEmpty()) {
		  return SigName;
	  }
	  return "unnamed";
  }
  
  /**
   * sets the aspect of the signal to a rule.  It also colors the icon
   * @param rule is the rule (from AspectMap)
   */
  public void setAspectforRule(int rule) {
	  if (MyIcon != null) {
		  MyIcon.setIndicationType(rule);
	  }
  }
  
  /**
   * clears the Signal's history.  For a virtual Signal, there is none.
   */
  public void clrSignalHistory() {
    if (MySignal != null) {
      MySignal.refresh();
    }
  }

  /**
   * tells a Signal if it should be on due to approach lighting
   * or not.
   * 
   * @param lit is true for on and false for off.
   */
  public void setApproachState(boolean lit) {
    if (MySignal != null) {
      MySignal.lightUpSignal(lit);
    }
  }

  /**
   * records the state specific to a Signal.
   * @param basic is true to record only basic state information.  It is false
   * to record detailed state information.
   * @return an Element, if the state has changed since the last snapshot; otherwise,
   * null.
   */
  protected Element dumpSignalState(boolean basic) {
    boolean changed = false;
    Element newState = new Element(XML_TAG);
    Element child;
    if (SigName != null) {
      newState.setAttribute("NAME", SigName);
    }
    if ((MySignal != null) && ((child = MySignal.dumpSignalState(basic)) != null)) {
      newState.addContent(child);
      changed = true;
    }
    if ((MyIcon != null) && ((child = MyIcon.dumpIconState(basic)) != null)) {
      newState.addContent(child);
      changed = true;
    }
    return (changed) ? newState : null;
  }
  
  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    SigName = new String(eleValue);
    if (MySignal != null) {
      MySignal.setName(SigName);
    }
    return null;
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    String resultMsg = null;
    if (PanelSignal.XML_TAG.equals(objName)) {
      MyIcon = (PanelSignal) objValue;
    }
    else if (PhysicalSignal.XML_TAG.equals(objName)) {
      MySignal = (PhysicalSignal) objValue;
      if (SigName != null) {
    	  MySignal.setName(SigName);
      }
      MySignal.setSecSignal(this);
    }
    else {
      resultMsg = new String("A " + XML_TAG + " cannot contain an Element ("
          + objName + ").");
    }
    return resultMsg;
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error checking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
	  if (MySignal != null) {
		  MySignal.getSignalMast();
	  }
	  return null;
  }

  /**
   * tells the SecSignal to link itself into the various data structures.
   *
   * @param tile is the GridTile where painting happens.
   */
  public void install(GridTile tile) {
    int heads = 1;
    boolean lamp = true;

    //    SignalTile = tile;
    if (MyIcon != null) {
      if (MySignal != null) {
        SignalTemplate template = TemplateStore.SignalKeeper.
        find(MySignal.getTemplateName());
        if (template != null) {
          heads = template.getNumHeads();
          if (heads == 0) {
            heads = 1;
          }
          else {
            lamp = template.isLights();
          }
          MyIcon.setParms(lamp, heads);
        }
      }
      MyIcon.install(tile);
    }
  }

  /**
   * registers a SecSignalFactory with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new SecSignalFactory());
    PanelSignal.init();
    PhysicalSignal.init();
  }
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SecSignal.class.getName());
}

/**
 * is a Class known only to the SecSignal class for creating SecSignals from
 * an XML document.  Its purpose is to pick up the location of the SecSignal
 * in the GridTile, its orientation, and physical attributes on the layout.
 */
class SecSignalFactory
implements XMLEleFactory {

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;

    resultMsg = new String("A " + SecSignal.XML_TAG +
        " XML Element cannot have a " + tag +
    " attribute.");
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    return new SecSignal();
  }

}
/* @(#)SecSignal.java */