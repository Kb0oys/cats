/* Name: LockIndicationHandler.java
 *
 * What:
 *   This is an interface implemented by classes that process lock add and
 *   lock remove requests for the GUI.  They could be tied to VitalLogic locks
 *   (received via the code line) or internal, in response to changes in
 *   TrackEdge states.  This architecture is flexible enough to easily
 *   convert from one stimulus source to the other; however, the
 *   instantiations typically will not support both sources or dynamically
 *   switch between them.
 *   <p>
 *   Most GUI Locks just record the state.  However, some (e.g. Detection)
 *   cause changes on the CTC panel (e.g. change the color of track in a
 *   block).  The former do not require custom handlers and are implemented by
 *   the enclosing class setGUILock() and clearGUILock() methods.  The latter
 *   are customized for the actions that they instigate.  
 */
package cats.layout.items;

/**
 *   This is an interface implemented by classes that process lock add and
 *   lock remove requests for the GUI.  They could be tied to VitalLogic locks
 *   (received via the code line) or internal, in response to changes in
 *   TrackEdge states.  This architecture is flexible enough to easily
 *   convert from one stimulus source to the other; however, the
 *   instantiations typically will not support both sources or dynamically
 *   switch between them.
 *   <p>
 *   Most GUI Locks just record the state.  However, some (e.g. Detection)
 *   cause changes on the CTC panel (e.g. change the color of track in a
 *   block).  The former do not require custom handlers.  The latter
 *   are customized for the actions that they instigate.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2015, 2016, 2018</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface LockIndicationHandler {

	  /**
	   * request that the lock be set.  This is invoked when the stimulus is internal to the Block.
	   */
	  public void requestLockSet();
	  
	  /**
	   * request that the lock be cleared.  This is invoked when the stimulis is internal to the Block.
	   */
	  public void requestLockClear();
	  
	  /**
	   * forwards along the event that a Block in advance has set its internal lock
	   */
	  public void advanceLockSet();
	  
	  /**
	   * forwards along the event that a Block in advance has cleared its internal lock
	   */
	  public void advanceLockClear();
	  
	  /**
	   * queries for the Lock
	   * @return the Lock being encapsulated
	   */
	  public GuiLocks getLock();
	  
//	  /**
//	   * tells the Lock that it is blocked - that it should not propagate
//	   * to the approach edge or neighbor.
//	   */
//	  public void blockLock();
//	  
//	  /**
//	   * tells the Lock that it is unblocked - that it can propagate to
//	   * the approach edge or neighbor.
//	   */
//	  public void unblockLock();
}
/* @(#)LockIndicationHandler.java */