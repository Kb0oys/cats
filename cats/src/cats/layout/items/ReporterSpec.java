/*
 * Name: ReporterSpec.java
 *
 * What:
 *  This is a class for holding the information for constructing a JMRI
 *  Reporter object
 */
package cats.layout.items;

import jmri.InstanceManager;
import jmri.Reporter;
import jmri.ReporterManager;
import jmri.managers.ProxyReporterManager;
import cats.layout.xml.XMLEleFactory;
import cats.layout.xml.XMLEleObject;
import cats.layout.xml.XMLReader;

/**
 * is a class for holding the information needed to construct a JMRI
 * Reporter object.
 *
 * <p>Title: designer</p>
 * <p>Description: A program for designing dispatcher panels</p>
 * <p>Copyright: Copyright (c) 2012, 2013, 2015, 2018, 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class ReporterSpec implements XMLEleObject  {
    /**
     * is the tag for identifying an ReporterSpec in the XML file.
     */
    static final String XML_TAG = "REPORTERSPEC";

    /**
     * is the attribute tag for identifying the decoder address.
     */
    static final String DECADDR = "DECADDR";
    
    /**
     * is the attribute tag for identifying the Loconet message type.
     */
    static final String JMRIPREFIX = "JMRIPREFIX";
    
    /**
     * is the XML tag for the UserName attribute.
     */
    static final String USER_NAME = "USER_NAME";
        
    /**
     * is the address of the associated JMRI Reporter.
     */
    private String DecAddr;

    /**
     * is the JMRI user name
     */
    private String UserName;
    
    /**
     * is the JMRI prefix name used for finding the specific decoder.
     */
    private String JmriPrefix;
    
    /**
     * the constructor.  It is called from the GUI.
     *
     * @param prefix is the JMRI prefix of the reporter
     * @param address is the address the command should be sent to.
     * @param name is the user name (which may be null)
     */
    public ReporterSpec(String prefix, String address, String name) {
      JmriPrefix = prefix;
      DecAddr = address;
      UserName = name;
    }

    /**
     * locates the JMRI reporter by user name (if non-null) or by system name
     * (if a Reporter with the requested user name cannot be found).
     * @return the requested JMRI Reporter, if found.  Return null, if not found.
     */
    public Reporter getReporter() {
      ReporterManager rm = InstanceManager.getDefault(ReporterManager.class);
      Reporter rep = null;
      if (rm == null) {
        log.error("JMRI reporter manager was not created.");
        rm = new ProxyReporterManager();
      }
      if ((UserName != null)  && !UserName.isEmpty()) {
        rep = rm.getByUserName(UserName);
      }
      if ((rep == null) && (JmriPrefix != null) && (JmriPrefix.length() > 1) &&
          (DecAddr != null) && (DecAddr.length() > 0)) {
        rep = rm.provideReporter(JmriPrefix + DecAddr);
        if (UserName != null) {
        	rep.setUserName(UserName);
        }
      }
      return rep;
    }
    
    /* Override */
    public String toString() {
    	return ((JmriPrefix == null) ? "" : JmriPrefix) +
    			((DecAddr == null) ? "" : DecAddr) + " " +
    			((UserName == null) ? "" : UserName);
    }
    
    /*
     * is the method through which the object receives the text field.
     *
     * @param eleValue is the Text for the Element's value.
     *
     * @return if the value is acceptable, then null; otherwise, an error
     * string.
     */
    public String setValue(String eleValue) {
        return new String("A " + XML_TAG + "cannot have a value.");
    }

    /*
     * is the method through which the object receives embedded Objects.
     *
     * @param objName is the name of the embedded object
     * @param objValue is the value of the embedded object
     *
     * @return null if the Object is acceptible or an error String
     * if it is not.
     */
    public String setObject(String objName, Object objValue) {
      return new String("A " + XML_TAG + " cannot contain an Element ("
                        + objName + ").");
    }

    /*
     * returns the XML Element tag for the XMLEleObject.
     *
     * @return the name by which XMLReader knows the XMLEleObject (the
     * Element tag).
     */
    public String getTag() {
      return new String(XML_TAG);
    }

    /*
     * tells the XMLEleObject that no more setValue or setObject calls will
     * be made; thus, it can do any error checking that it needs.
     *
     * @return null, if it has received everything it needs or an error
     * string if something isn't correct.
     */
    public String doneXML() {
      String resultMsg = null;
      if (((DECADDR == null) || (DECADDR.equals(""))) &&
              ((USER_NAME == null) || (USER_NAME.equals("")))) {
          resultMsg = new String("A " + XML_TAG + " is missing is address.");
      }
      return resultMsg;
    }
    
    /**
     * registers an IOSpecFactory with the XMLReader.
     */
    static public void init() {
      XMLReader.registerFactory(XML_TAG, new ReporterSpecFactory());
      new LockedDecoders();
    }
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
        ReporterSpec.class.getName());
  }

  /**
   * is a Class known only to the IOSpec class for creating IOSpecs from
   * an XML document.
   */
  class ReporterSpecFactory
      implements XMLEleFactory {

    /**
     * is the address to send the command to.
     */
    String Recipient;

    /**
     * is the JMRI name prefix
     */
    String Prefix;

    /**
     * is the User Name.
     */
    String UserName;

    /*
     * tells the factory that an XMLEleObject is to be created.  Thus,
     * its contents can be set from the information in an XML Element
     * description.
     */
    public void newElement() {
      Recipient = "";
      Prefix = null;
      UserName = null;
    }

    /*
     * gives the factory an initialization value for the created XMLEleObject.
     *
     * @param tag is the name of the attribute.
     * @param value is it value.
     *
     * @return null if the tag:value are accepted; otherwise, an error
     * string.
     */
    public String addAttribute(String tag, String value) {
      String resultMsg = null;

      if (ReporterSpec.DECADDR.equals(tag)) {
          Recipient = value;
      }
      else if (ReporterSpec.JMRIPREFIX.equals(tag)) {
        Prefix = value;
      }
      else if (IOSpec.USER_NAME.equals(tag)) {
          UserName = value;
      }
      else {
        resultMsg = new String("A " + IOSpec.XML_TAG +
                               " XML Element cannot have a " + tag +
                               " attribute.");
      }
      return resultMsg;
    }

    /*
     * tells the factory that the attributes have been seen; therefore,
     * return the XMLEleObject created.
     *
     * @return the newly created XMLEleObject or null (if there was a problem
     * in creating it).
     */
    public XMLEleObject getObject() {
      return new ReporterSpec(Prefix, Recipient, UserName);
    }
}
/* @(#)ReporterSpec.java */
