/* Name: InternalXEdge.java
 *
 * What:
 * This class is an internal SecEdge that sits on a crossing track (diamond).
 * It is the GUI half, with the real work being performed by XVitalLogic.
 * <p>
 * It appears in two places:
 * <ol>
 * <li>in the interior of Section, where the two tracks cross at right angles</li>
 * <li>on the edge of a Section without a block boundary</li>
 * </ol>
 */
package cats.layout.items;


import java.util.BitSet;
import java.util.EnumSet;

import cats.layout.vitalLogic.BasicVitalLogic;
import cats.layout.vitalLogic.CrossingVitalLogic;
import cats.layout.vitalLogic.InternalXVitalLogic;
import cats.layout.vitalLogic.LogicLocks;

/**
 * This class is an internal SecEdge that sits on a crossing track (diamond).
 * It is the GUI half, with the real work being performed by XVitalLogic.
 * <p>
 * It appears in two places:
 * <ol>
 * <li>in the interior of a Section, where the two tracks cross at right angles</li>
 * <li>on the edge of a Section without a block boundary</li>
 * </ol>
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2008, 2009, 2012, 2014, 2018, 2019, 2021, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class InternalXEdge extends DefaultTrackEdge implements XTrackEdge {

	/**
	 * the crossing edges
	 */
	private XTrackEdge[] CrossingEdge = new XTrackEdge[2];
	
	/**
	 * the ctor
	 * @param edge is the side of the section the InternalXEdge is a proxy for
	 */
	public InternalXEdge(int edge) {
		super(edge, null);
	}
	
	@Override
	public void setHandlers() {
//		DetectionIndication = new PropagationDecorator(new DefaultLockIndicationHandler(GuiLocks.DETECTIONLOCK));
		/**
		 * The GuiLocks are:
		 * DETECTIONLOCK
		 * CONFLICTINGSIGNALLOCK
		 * SWITCHUNLOCK
		 * APPROACHLOCK
		 * TIMELOCK
		 * TRAFFICLOCK
		 * ENTRYLOCK
		 * EXITLOCK
		 * LOCALCONTROLLOCK
		 * CALLONLOCK
		 * WARRANTLOCK
		 * FLEET
		 * CROSSING
		 * PREPARATION
		 * EXTERNALLOCK
		 **/

		/**
		 * Most locks are simply passed through.  The only lock injected is
		 * CROSSING and it is injected into the crossing track.  So, EdgeLocks is
		 * not used and there is no need for merging locks.
		 */
//		DetectionIndication = new VitalLogicDecorator(
//				new PropagationDecorator(
//						new RouteCheckerDecorator(
//								new BasicLockIndicationHandler(GuiLocks.DETECTIONLOCK))), LogicLocks.DETECTIONLOCK);
		DetectionIndication = new VitalLogicDecorator(
				new PropagationDecorator(
						new RouteCheckerDecorator(
								new BasicLockIndicationHandler(GuiLocks.DETECTIONLOCK))), LogicLocks.DETECTIONLOCK);
		ConflictingSignalIndication = new PropagationDecorator(
				new BasicLockIndicationHandler(GuiLocks.CONFLICTINGSIGNALLOCK));
		SwitchUnlockIndication = new PropagationDecorator(
				new RouteCheckerDecorator(
						new BasicLockIndicationHandler(GuiLocks.SWITCHUNLOCK)));
		// APPROACHLOCK should never be set or cleared
		// TIMELOCK should never be set or cleared
		// TRAFFICLOCK should never be set or cleared
		// ENTRYLOCK is created in setBlockingLogic, when the crossing tracks are known
		EntryTrafficIndication = new EntryDecorator(
				new RouteCheckerDecorator(
						new BasicLockIndicationHandler(GuiLocks.ENTRYLOCK)));
		ExitTrafficIndication = new ExitDecorator(
				new RouteCheckerDecorator(
						new BasicLockIndicationHandler(GuiLocks.EXITLOCK)));
		LocalControlLock = new VitalLogicDecorator(
				new PropagationDecorator(
						new RouteCheckerDecorator(
								new BasicLockIndicationHandler(GuiLocks.LOCALCONTROLLOCK))), LogicLocks.MAINTENANCELOCK);
		// CALLONLOCK should never be set or cleared
		// WARRANTLOCK should never be set or cleared
		// FLEETLOCK should never be set or cleared
		//		CrossingIndication = new MergeDecorator(
		//				new PropagationDecorator(
		//						new BasicLockIndicationHandler(GuiLocks.CROSSING)));
		PreparationIndication = new PropagationDecorator(
				new RouteCheckerDecorator(
						new BasicLockIndicationHandler(GuiLocks.PREPARATION)));
		ExternalIndication = new PropagationDecorator(
				new RouteCheckerDecorator(
						new BasicLockIndicationHandler(GuiLocks.EXTERNALLOCK)));
	}
	
	@Override
	public EdgeInterface.EDGE_TYPE getEdgeType() {
		return EdgeInterface.EDGE_TYPE.INTERNAL_EDGE;
	}
	
	@Override
	public BasicVitalLogic createVitalLogic() {
		// this is not called from an internal crossing because it is not on an edge
		return new InternalXVitalLogic(toString(), MyEdge);
	}
	
	@Override
	public void setBlock(Block block) {
		super.setBlock(block);
		if (block != null) {
			block.registerTrackEdge(this);
		}
	}
	
	@Override
	public void setBlockingLogic(XTrackEdge blocks[]) {
		CrossingVitalLogic b[] = new CrossingVitalLogic[2];
		CrossingEdge[0] = blocks[0];
		CrossingEdge[1] = blocks[1];
		b[0] = blocks[0].getXVitalLogic();
		b[1] = blocks[1].getXVitalLogic();
		getXVitalLogic().linkCrossing(b);
//		EntryTrafficIndication = new EntryDecorator(
//					new CrossingDecorator(
//							new BasicLockIndicationHandler(GuiLocks.ENTRYLOCK), CrossingEdge, GuiLocks.CROSSING));
	}
	
	@Override
    public void bindTo(XTrackEdge edge) {
    if (Joint == null) {
       Joint = edge.castEdge();
     }
   }
	
	@Override
	public SecEdge castEdge() {
		return this;
	}

	@Override
	public void setVitalLogic() {
		MyLogic = createVitalLogic();
	}
	
	@Override
	public CrossingVitalLogic getXVitalLogic() {
		return (CrossingVitalLogic) MyLogic;
	}
	
//	@Override
//	protected boolean isEdgeBlocked(final EnumSet<GuiLocks> blockingConditions) {
//		if (super.isEdgeBlocked(blockingConditions)) {
//			return true;
//		}
//		if (CrossingEdge[0].xIsEdgeBlocked(blockingConditions)) {
//			return true;
//		}
//		if (CrossingEdge[1].xIsEdgeBlocked(blockingConditions)) {
//			return true;
//		}
//		return false;
//	}
	
	@Override
	protected boolean extendedIsEdgeBlocked(final EnumSet<GuiLocks> blockingConditions) {
		if (super.extendedIsEdgeBlocked(blockingConditions)) {
			return true;
		}
		if (CrossingEdge[0].xIsEdgeBlocked(blockingConditions)) {
			return true;
		}
		if (CrossingEdge[1].xIsEdgeBlocked(blockingConditions)) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean xIsEdgeBlocked(final EnumSet<GuiLocks> blockingConditions) {
		return super.extendedIsEdgeBlocked(blockingConditions);
	}
	
	@Override
	public void activateStacking(final boolean alert, BitSet nodeSet) {
		super.activateStacking(alert, nodeSet);
		CrossingEdge[0].xActivateStacking(alert, nodeSet);
		CrossingEdge[1].xActivateStacking(alert, nodeSet);
	}
	
	@Override
	public void xActivateStacking(final boolean alert, BitSet nodeSet) {
		super.activateStacking(alert, nodeSet);
	}
}
/* @(#)InternalXEdge.java */
