/* Name: TrackEdge.java
 *
 * What:
 *   This interface specializes a SecEdge for those edges that are at the end of a track
 *   (e.g. BlkEdge - with or without signals - and OSEdge).  The edges have associated
 *   VitalLogic objects.  This interface is used to link adjacent VitalLogic objects
 *   together so that changes in one can propagate to the one in approach.
 */
package cats.layout.items;

import cats.layout.codeLine.CodeMessage;
import cats.layout.vitalLogic.BasicVitalLogic;

/**
 *   This interface specializes a SecEdge for those edges that are at the end of a track
 *   (e.g. BlkEdge - with or without signals - and OSEdge).  The edges have associated
 *   VitalLogic objects.  This interface is used to link adjacent VitalLogic objects
 *   together so that changes in one can propagate to the one in approach.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014, 2018, 2021, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface TrackEdge extends EdgeInterface {

	/**
	 * creates the VitalLogic to match the TrackEdge
	 * @return the VitalLogic for the TrackEdge type
	 */
	public BasicVitalLogic createVitalLogic();

	/**
	 * A method to group Track Segments (represented by inter-sectional "lines") into higher
	 * order objects - Track Sequences.  A Track Sequence is a chain of Track Segments
	 * bounded on the ends by TrackEdges.  This method performs the following functions:
	 * 1. traverses the Track Segments, adding them to the Track Span for the block.
	 * The block Track Span is used for coloring the Track Segments.
	 * 2. computes the minimum track speed over the Track Sequence.
	 * 3. identifies the TrackEdge at the "other" end of the Track Sequence.  This
	 * is used for rapidly traversing Track Sequences.
	 * 4. identifies the Track Edge in advance.  If that Track Edge exists, it is
	 * told about this Track Edge.  The resulting linkage is used for computing
	 * signal indications.
	 * 5. configures its associated VitalLogic
	 * 6. links the two associated VitalLogics together.
	 * 
	 * Key Track Sequence fields set by this method are:
	 * - NeighborEdge
	 * - OppositeEnd
	 * - the ApproachEdge in the TrackEdge in advance
	 * 
	 * Turnouts throw a monkey wrench into things because the PtsEdge and FrogEdges
	 * are TrackEdges on the neighbor to opposite edge, as opposed to the being the
	 * opposite edge.  This creates the exception case - a Track Sequence may be
	 * physically ended by a SecEdge, whose neighbor is a PtsEdge; however, logically,
	 * the PtsEdge is both the Opposite TrackEdge and the TrackEdge in advance.
	 * 
	 * FrogEdges have a similar inverted relationship with the Track Edge at the
	 * other end of the Track Sequence, except the FrogEdge is the SecEdge and not
	 * the neighbor SecEdge as with PtsEdges.
	 */
	public void discoverAdvanceVitalLogic();

	/**
	 * sets the ApproachEdge TrackEdge
	 * @param approach is the TrackEdge in approach
	 */
	public void setApproachEdge(AbstractTrackEdge approach);

	/**
	 * This method is analogous to VitalLogicProbe, in that it is
	 * used to find the immediate signal in approach.  Since the linkages to
	 * the TrackEdge in approach are known, it traverses that chain to find
	 * the signal in approach, rather than running through all the SecEdges.
	 * @return the TrackEdge in approach containing a Signal
	 */
	public SignalEdge signalEdgeProbe();

	/**
	 * requests the neighbor, in a search for the approach
	 * TrackEdge.  Typically, this is just the neighbor edge;
	 * however, because a Frog edge is the approach edge for
	 * its peer, a Frog edge will return itself.
	 * @return the neighbor
	 */
	public TrackEdge nextTrackEdge();
	
	/**
	 * retrieves the VitalLogic associated with the TrackEdge
	 * @return the VitalLogic
	 */
	public BasicVitalLogic getVitalLogic();

	/**
	 * is needed for determining when to stop painting track.
	 * Typically, the color of track is the same between the BlkEdges
	 * of one or more tracks segments, but can change when on the
	 * tracks in the next Block.
	 * @return true if the TrackEdge resides on a Block boundary
	 */
	public boolean isBlkEdge();
	
	/**
	 * is a predicate for querying if a TrackEdge has a particular
	 * lock set.
	 * @param lock is the LogicLocks in question
	 * @return true if it is set and false if not
	 */
	public boolean isLockSet(GuiLocks lock);

	/**
	 * this method receives messages (responses and reports) from the field
	 * equipment and processes them.  It returns the parsed message to allow
	 * the method to be used in the Decorator design pattern.
	 * @param response is the message from the field equipment
	 * @return the message received, parsed
	 */
	public CodeMessage processFieldResponse(String response);
	
	/**
	 * requests that the TrackEdge tell its associated VitalLogic
	 * to send its state to the approach VitalLogic.  This should be called
	 * only after all the VitalLogic are connected.
	 */
	public void startFeeding();
	
	/**
	 * constructs the lock handlers for the edge and common locks.  Because the handlers
	 * are dependent upon the kind of TrackEdge, its discipline, and often its neighbors,
	 * this method can be invoked only after the edges are tied together in
	 * discoverAdvanceVitalLogic().
	 */
	public void setHandlers();
}
/* @(#)TrackEdge.java */