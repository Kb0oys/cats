/* Name: ApproachTrigger.java
 *
 * What:
 *  This class defines an object that holds the specifications of the trigger
 *  for turning on and off a signal mast with approach lighting.  A signal is
 *  "approach lit" if it is off (dark) until a train is detected nearby.  The
 *  detection triggers the signal to light up and show its current aspect.
 *  <p>
 *  The default trigger is the block in front of the signal (the approach block).
 *  However, some railroads will light a signal when there is any detection on
 *  any block between two control points.  Some railroads will trigger approach
 *  on a multi-track mainline when any block on any track between two control
 *  points is occupied.  This class addresses the latter.  It provides a way
 *  for the layout owner to specify the conditions that light/extinguish
 *  a signal.
 */
package cats.layout.items;

import cats.layout.xml.XMLEleFactory;
import cats.layout.xml.XMLEleObject;


/**
 *  This class defines an object that holds the specifications of the trigger
 *  for turning on and off a signal mast with approach lighting.  A signal is
 *  "approach lit" if it is off (dark) until a train is detected nearby.  The
 *  detection triggers the signal to light up and show its current aspect.
 *  <p>
 *  The default trigger is the block in front of the signal (the approach block).
 *  However, some railroads will light a signal when there is any detection on
 *  any block between two control points.  Some railroads will trigger approach
 *  on a multi-track mainline when any block on any track between two control
 *  points is occupied.  This class addresses the latter.  It provides a way
 *  for the layout owner to specify the conditions that light/extinguish
 *  a signal.
 *
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A model railroad dispatching program</p>
 * <p>Copyright: Copyright (c) 2015, 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class ApproachTrigger implements XMLEleObject {


	/**
	 * is the tag for identifying an ApproachTrigger in the XMl file.
	 */
	static final String XML_TAG = "APPROACHTRIGGER";

	/**
	 * is the specifications for the trigger.  It may be null if the user
	 * does not have approach lighting or uses the default approach block
	 */
	private IOSpec Trigger = null;

	/**
	 * retrieves the user defined IOSpec that controls approach
	 * lighting.  If there is no approach lighting or the default
	 * (approach block detection) is used, null is returned.
	 * @return the trigger for approach lighting
	 */
	public IOSpec getTrigger() {
		return Trigger;
	}
	
	/**
	 * sets the user defined IOSpec that controls approach
	 * lighting.
	 * @param trigger is the IOSPec.  It can be null.
	 */
	public void setTrigger(IOSpec trigger) {
		Trigger = trigger;
	}
	
	@Override
	public String setValue(String eleValue) {
	    return new String("A " + XML_TAG + " cannot contain a text field ("
                + eleValue + ").");
	}

	@Override
	public String setObject(String objName, Object objValue) {
		if (IOSpec.XML_TAG.equals(objName)) {
			Trigger = (IOSpec) objValue;
		}
		else {
			return new String("A " + XML_TAG + " cannot contain an Element ("
                    + objName + ").");
		}
		return null;
	}

	@Override
	public String getTag() {
	    return new String(XML_TAG);
	}

	@Override
	public String doneXML() {
	    return null;
	}
}
/**
 * is a Class known only to the ApproachTrigger class for creating ApproachTriggers from
 * an XML document.  Its purpose is to collect the decoder instruction for the
 * approach lighting trigger.
 */
class ApproachTriggerFactory
    implements XMLEleFactory {

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;

    resultMsg = new String("An " + ApproachTrigger.XML_TAG +
                           " XML Element cannot have a " + tag +
                           " attribute.");
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    return new ApproachTrigger();
  }
}
/* @(#)ApproachTrigger.java */
