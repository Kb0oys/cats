/* Name: ChainActionManager.java
 *
 * What:
 *   This file contains a class that becomes the thread that receives
 *   actions from Chains and executes them. 
 *   <p>
 *   What makes it unique is that each Chain action has a piece to be
 *   executed and a delay.  The delay is in milliseconds  
 */

package cats.layout.items;

import cats.layout.AsyncDelayLine;
import cats.layout.DelayLineListener;
import cats.layout.ManagedQueue;
import cats.rr_events.EventInterface;

/**
 *   This file contains a class that becomes the thread that receives
 *   RREvents from a queue and executes them.
 *   <p>
 *   What makes it unique is that each Chain action has a piece to be
 *   executed and a delay.  The delay is in milliseconds  
 *   @see cats.rr_events.RREvent
 *   @see cats.layout.Queue
 *   @see java.lang.Runnable
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class ChainActionManager implements DelayLineListener<EventInterface> {

	/**
	 * is the event consumer constructor.
	 */
	public ChainActionManager() {
		ChainHandler = new AsyncDelayLine<>("ChainActionManager", 10, 0, Thread.NORM_PRIORITY, this);
		EventQue = ChainHandler.Que;
	}

	/**
	 * a FIFO for holding RREvent objects.
	 */
	public static ManagedQueue EventQue;

	/**
	 * the asynchronous queue.  No delay should be used.
	 */
	public static AsyncDelayLine<EventInterface> ChainHandler;

	@Override
	public void consume(EventInterface item) {
		item.doIt();
	}
}
/* @(#)ChainActionManager.java */
