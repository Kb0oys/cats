/* Name: XBlkEdge.java
 *
 * What:
 *   This class is the container for all the information about one
 *   of a Section's Edges which "is a" Block boundary on a crossing
 *   (diamond).
 */
package cats.layout.items;

import java.util.BitSet;
import java.util.EnumSet;

import cats.layout.codeLine.packetFactory.EdgePacketFactory;

import cats.layout.vitalLogic.BasicVitalLogic;
import cats.layout.vitalLogic.CrossingVitalLogic;
import cats.layout.vitalLogic.XBlkVitalLogic;

/**
 *   This class is the container for all the information about one
 *   of a Section's Edges which "is a" Block boundary on a crossing
 *   (diamond).
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2009, 2010, 2011, 2012, 2015, 2018, 2019, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class XBlkEdge extends BlkEdge implements XTrackEdge {
	
	/**
	 * the crossing edges
	 */
	private XTrackEdge[] CrossingEdge = new XTrackEdge[2];
	

	/**
	 * the ctor
	 * @param edge is the side of the section the InternalXEdge is a proxy for
	 * the blocking XVitalLogics
	 * @param block is the Block the edge is on
	 */
	public XBlkEdge(int edge, Block block) {
		super(edge, null, block);
	}

	@Override
	public EdgeInterface.EDGE_TYPE getEdgeType() {
		return EdgeInterface.EDGE_TYPE.XBLOCK_EDGE;
	}

	@Override
	public BasicVitalLogic createVitalLogic() {
		return new XBlkVitalLogic(new EdgePacketFactory(this), toString(), MyEdge);
	}
	
//	@Override
//	public void setHandlers() {
//		super.setHandlers();
//		EntryTrafficIndication = new CrossingDecorator(EntryTrafficIndication, CrossingEdge, GuiLocks.CROSSING);
//		CrossingIndication = new MergeDecorator(
//				new PropagationDecorator(
//						new BasicLockIndicationHandler(GuiLocks.EXTERNALLOCK)));
//		CallOnIndication = new MergeDecorator(
//				new PropagationDecorator(
//						new BasicLockIndicationHandler(GuiLocks.CALLONLOCK)));
//	}
	
	@Override
	public void setBlockingLogic(XTrackEdge blocks[]) {
		CrossingVitalLogic b[] = new CrossingVitalLogic[2];
		CrossingEdge[0] = blocks[0];
		CrossingEdge[1] = blocks[1];
		b[0] = blocks[0].getXVitalLogic();
		b[1] = blocks[1].getXVitalLogic();
		getXVitalLogic().linkCrossing(b);
	}
	
	@Override
     public void bindTo(XTrackEdge edge) {
     if (Joint == null) {
        Joint = edge.castEdge();
        edge.bindTo(this);
      }
    }
	
	@Override
	public SecEdge castEdge() {
		return this;
	}

	@Override
	public void setVitalLogic() {
		MyLogic = createVitalLogic();
	}
	
	@Override
	public CrossingVitalLogic getXVitalLogic() {
		return (CrossingVitalLogic) MyLogic;
	}
	
//	@Override
//	protected boolean isEdgeBlocked(final EnumSet<GuiLocks> blockingConditions) {
//		if (super.isEdgeBlocked(blockingConditions)) {
//			return true;
//		}
//		if (CrossingEdge[0].xIsEdgeBlocked(blockingConditions)) {
//			return true;
//		}
//		if (CrossingEdge[1].xIsEdgeBlocked(blockingConditions)) {
//			return true;
//		}
//		return false;
//	}
	
	@Override
	public boolean xIsEdgeBlocked(final EnumSet<GuiLocks> blockingConditions) {
		return extendedIsEdgeBlocked(blockingConditions);
	}
	
	@Override
	public void activateStacking(final boolean alert, BitSet nodeSet) {
		super.activateStacking(alert, nodeSet);
		CrossingEdge[0].xActivateStacking(alert, nodeSet);
		CrossingEdge[1].xActivateStacking(alert, nodeSet);
	}
	
	@Override
	public void xActivateStacking(final boolean alert, BitSet nodeSet) {
		super.activateStacking(alert, nodeSet);
	}
}
/* @(#)BlkEdge.java */