/* Name LogicLocks.java
 *
 * What:
 *  This class holds an enumeration defining the various kinds of locks that can affect
 *  Signals and Switches.  They correspond to relays in the vital logic (field equipment).
 *  <p>
 *  The defined locks are:
 *  <ul>
 *  <li> Traffic Lock - the signals and turnouts are locked because a route has
 *  been reserved through the track circuit.
 *  <li> Detection (or Route) Lock - the signals and turnouts are locked because
 *  the track circuit shows occupied.
 *  <li> Opposing Signal Lock - the signal is locked on Halt because an opposing signal
 *  has been cleared.
 *  <li> Conflicting Signal Lock - the signal is locked on Halt because a conflicting
 *  signal has been cleared.
 *  <li> Switch Indication Lock (fouling) - the points are fouling
 *  <li> Time Lock - signals and turnouts are locked because the dispatcher has knocked
 *  down a route and time is running before they can be reset.
 *  <li> Approach Lock - a precondition for time lock.  Time is not locked if the approach
 *  traffic circuit is unoccupied.
 *  <li> Switch Unlocked - the field has unlocked a switch
 *  </ul>
 */
package cats.layout.items;

import java.util.EnumSet;

/**
 *  This class holds an enumeration defining the various kinds of locks that can affect
 *  the presentation of Signals and Switches in the GUI.  They are functionally analogous
 *  to the Vital Logic locks, 
 *  <p>
 * Title: CATS - Crandic Automated Traffic System
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2014, 2015, 2016, 2017, 2018, 2022
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public enum GuiLocks {
	/** (x00001) the track circuit reports occupied */
	DETECTIONLOCK('D'),
	/** (x00002) a conflicting signal has been cleared (crossing) or a switch is fouling */
	CONFLICTINGSIGNALLOCK('C'),
	/** (x00004) a controlled electric lock or automatic electric lock switch has been unlocked in the field. */
	SWITCHUNLOCK('U'),
	/** (x0008) the adjacent block is occupied */
	APPROACHLOCK('A'),
	/** (x00010) a signal protecting the track circuit is running time */
	TIMELOCK('I'),
	/** (x00020) the Vital Logic has approved/removed a route */
	TRAFFICLOCK('T'),
	/** (x00040) a route has been set to enter the Block through this edge */
	ENTRYLOCK('E'),
	/** (x00080) a route has been set to exit the Block through this edge */
	EXITLOCK('X'),
	/** (0x00100) the dispatcher has given the field crew local control over a block
	 * protected by the signal (track authority or out of service).
	 */
	LOCALCONTROLLOCK('M'),
	/**
	 * (0x00200) the dispatcher has overridden the safety of the signal system to allow
	 * a train to pass a signal showing Stop.
	 */
	CALLONLOCK('B'),
	/** (x00400) a Track Warrant (DTC) has been created on the Block through this edge */
	WARRANTLOCK('W'),
	/** (0x00800) the dispatcher has set up fleeting so that a route does not clear - signals
	 * may clear after the train exits the block - probably not needed
	 */
	FLEET('F'),
	/** (x01000) a route has been reserved through a crossing track*/
	CROSSING('V'),
	/** (x02000) an Extended Route is being prepared */
	PREPARATION('P'),
	/** (x04000) something happened to drop the protecting signals */
	EXTERNALLOCK('Z');

	/**
	 * the identifier of the lock.  These match the code identifiers.
	 */
	private final char LockType;

	/**
	 * the ctor
	 * @param id is the one character mnemonic identifying the lock
	 */
	GuiLocks(char id) {
		LockType = id;
	}

	/**
	 * a utility for converting a lock to a mnemonic
	 * @return the mnemonic letter for the lock
	 */
	public char toMnemomic() {
		return LockType;
	}

	/**
	 * the set of locks in EdgeLocks that indicate the protected block(s) have activity in them; thus,
	 * blocking a route from being created.  Should DETECTIONLOCK be in this list?  Including it prevents
	 * manual fleeting - sending one train behind another after an intermediate.
	 */
	static public final EnumSet<GuiLocks> RouteBlocker = EnumSet.of(CONFLICTINGSIGNALLOCK,
			SWITCHUNLOCK, TIMELOCK, EXITLOCK, LOCALCONTROLLOCK, CALLONLOCK, CROSSING, PREPARATION, EXTERNALLOCK);
	/**
	 * the set of locks in COmmonLocks that indicate the protected block(s) have activity in them; thus,
	 * preventing a route from being created.  These locks are propagated across the track segment against
	 * the direction of traffic.
	 */
	static public final EnumSet<GuiLocks> CommonBlock = EnumSet.of(EXITLOCK, PREPARATION);
	
	/**
	 * the set of locks indicating that the block contains an obstruction.  It is used for setting Enable/Disable/Cancel
	 * on right mouse menu items.
	 */
	static public final EnumSet<GuiLocks> ObstructedBlock = EnumSet.of(CONFLICTINGSIGNALLOCK,
			SWITCHUNLOCK, TIMELOCK, CALLONLOCK, CROSSING, EXTERNALLOCK);

	/**
	 * the set of locks in EdgeLocks preventing an eXtended Route from being immediately set through
	 * a Route segment.  Note that CONFLICTINGSIGMNALLOCK is not included because the eXtendedRoute
	 * can move points to set the optimal route.
	 */
	static public final EnumSet<GuiLocks> ExtendedRouteBlock = EnumSet.of(DETECTIONLOCK, SWITCHUNLOCK,
			TIMELOCK, ENTRYLOCK, EXITLOCK, LOCALCONTROLLOCK, CALLONLOCK, FLEET, CROSSING, PREPARATION, EXTERNALLOCK);
	
	/**
	 * the set of locks in the opposite end of an eXtended RouteEdge that would prevent setting
	 * a route.
	 */
	static public final EnumSet<GuiLocks> OppositeExtendedRouteBlock = EnumSet.of(TIMELOCK, ENTRYLOCK,
			CALLONLOCK, CROSSING, PREPARATION);
	
	/**
	 * the set of locks set/cleared by the owning edge which are not tied to hardware or
	 * a neighbor edge
	 */
	static public final EnumSet<GuiLocks> SoftLocks = EnumSet.of(CONFLICTINGSIGNALLOCK,
			TRAFFICLOCK, ENTRYLOCK, EXITLOCK, CALLONLOCK, WARRANTLOCK, FLEET, PREPARATION);
	
	/**
	 * these EdgeLocks prevent the dispatcher from moving the points
	 */
	static public final EnumSet<GuiLocks> EdgeSwitchBlocks = EnumSet.of(DETECTIONLOCK, SWITCHUNLOCK,
			ENTRYLOCK, EXITLOCK, LOCALCONTROLLOCK, PREPARATION, EXTERNALLOCK);

	/**
	 * these CommonLocks prevent the dispatcher from moving the points due to locks in the same
	 * direction.  Note that PREPARATION is not included because it blocks automatically moving points
	 * without feedback.
	 */
	static public final EnumSet<GuiLocks> CommonSwitchBlocks = EnumSet.of(
			ENTRYLOCK, EXITLOCK);

	/**
	 * these EdgeLocks prevent the points from being moved in the field
	 */
	static public final EnumSet<GuiLocks> LocalSwitchBlocks = EnumSet.of(CONFLICTINGSIGNALLOCK,
			TIMELOCK, TRAFFICLOCK, ENTRYLOCK, EXITLOCK, CALLONLOCK, WARRANTLOCK, FLEET, PREPARATION);
	
	/**
	 * the set of locks forwarded through points when the points move
	 */
	static public final EnumSet<GuiLocks> GUISwitchLocks = EnumSet.of(DETECTIONLOCK, CONFLICTINGSIGNALLOCK,
			SWITCHUNLOCK, ENTRYLOCK, EXITLOCK, CROSSING, EXTERNALLOCK);
	
	/**
	 * performs an intersection operation on two sets.  Because of the way the
	 * Java set operators work, one must be copied because it will be modified.
	 * @param varSet is the set that is copied (so that the original is not modified)
	 * @param constSet is the "mask" - it remains unchanged.
	 * @return the elements that are common to both
	 */
	static public final EnumSet<GuiLocks> intersection(final EnumSet<GuiLocks> varSet,
			final EnumSet<GuiLocks> constSet) {
		EnumSet<GuiLocks> copy = varSet.clone();
		copy.retainAll(constSet);
		return copy;    
	}
}


/* @(#)GuiLocks.java */
