/* Name: ManagedQueue.java
 *
 * What:
 *   This class defines a Queue of any type, which can be shared by multiple
 *   threads (implying that operations are synchronized for exclusive access).
 *   It is built on a linked list.  The elements in the linked list are reused,
 *   rather than "new" whenever something is added.  So, the resource pool is
 *   itself a linked list.  The new allocator is invoked only when the
 *   resource pool is empty.
 *
 * Special Considerations:
 *   This class is application independent.
 */
package cats.layout;

import cats.apps.Crandic;
import cats.common.DebugBits;

/**
 *   This class defines a Queue of any type, which can be shared by multiple
 *   threads (implying that operations are synchronized for exclusive access).
 *   It is built on a linked list.  The elements in the linked list are reused,
 *   rather than "new" whenever something is added.  So, the resource pool is
 *   itself a linked list.  The new allocator is invoked only when the
 *   resource pool is empty.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2020, 2021, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class ManagedQueue<T> {
	private int QueueSize = 0;
	private Element PoolHead = null;
	private Element PoolTail = null;
	private Element QueHead = null;
	private Element QueTail = null;
//	private boolean Flushed = false;
	private int Received = 0;
	private int Sent = 0;

	/**
	 *  The ManagedQueue constructor
	 *  @param initialLength is the number of items in the resource
	 *  pool to begin with
	 */
	public ManagedQueue(int initialLength) {
		QueueSize = initialLength;
		for (; initialLength > 0; --initialLength) {
			if (PoolTail == null) {
				PoolHead = new Element(null);
				PoolTail = PoolHead;
			}
			else {
				PoolTail.Next = new Element(null);
				PoolTail = PoolTail.Next;
			}
		}
	}

	/**
	 *  This method adds an item to the Queue.  It first tries
	 *  to get an unused Element from the pool and use it.  If
	 *  the pool is empty, it allocates a new Element.
	 *
	 * @param s is the item to be added to the Queue
	 */
	public synchronized void append(T s) {
		// remove from Pool
		Element e = PoolHead;
		if (s == null) {
			System.out.println("Trying to append nothing to queue");
		}
		if (PoolHead == null) {
			e = new Element(s);
			++QueueSize;
		}
		else {
			PoolHead = PoolHead.Next;
			if (PoolHead == null) {
				PoolTail = null;
			}
		}
		e.Content = s;
		e.Next = null;

		// add to queue
		if (QueTail == null) {
			QueTail = QueHead = e;
		}
		else {
			QueTail.Next = e;
			QueTail = e;
		}
		++Sent;
		notify(); /* let the consumer know the Queue is not empty */
	}

	/**
	 * This method pulls the oldest item off the Queue.  If
	 * there is one, the Element is returned to the free pool.
	 *
	 * @return The oldest item on the Queue
	 */
	public synchronized T get() {
		while (true) {
			try {
//				while ((QueHead == null) && !Flushed) {
				while (QueHead == null) {
					wait();
				}
			}
			catch (InterruptedException e) {
				Thread.dumpStack();
			}
			++Received;
			Element ele = QueHead;
			QueHead = QueHead.Next;
			if (QueHead == null) {
				QueTail = null;
			}
			T value = ele.Content;
			ele.Content = null;
			ele.Next = null;
			if (Crandic.Details.get(DebugBits.QUEUEBIT) && (value == null)) {
				System.out.println("Sent = " + Sent + " Received = " + Received);
			}
			if (PoolTail == null) {
				PoolHead = ele;
			}
			else {
				PoolTail.Next = ele;
			}
			PoolTail = ele;
			if (value != null) {
				return value;
			}
			log.warn("null item found in queue");
		}
	}

	/**
	 * a predicate for determining if the queue is empty,
	 * to avoid blocking
	 * @return true if there is an element in the queue
	 */
	public synchronized boolean isEmpty() {
		return QueHead == null;
	}

//	/**
//	 *  This method notifies any Queue consumers that the Queue
//	 *   should be flushed
//	 *
//	 */
//	public synchronized void flush() {
//		Flushed = true;
//		notify();
//	}

	/****************************************************************
	 * inner classes
	 ****************************************************************/
	/****************************************************************
	 *   An inner class object for providing the linkage between
	 *   entries in a Queue.
	 ****************************************************************/
	class Element {
		Element Next;
		T Content;

		/**
		 *  The constructor.  It creates a wrapper around the
		 *   value queued, containing the linkages that form the Queue
		 *
		 * @param   s is the thing being put in the Queue
		 */
		public Element(T s) {
			Content = s;
			Next = null;
		}
	}
	/****************************************************************
	 *   end of the Element class
	 ****************************************************************/

	/****************************************************************
	 *   An inner class that is an unsynchronized linked list
	 ****************************************************************/
	class LinkedList {
		Element Head = null; /* where items are removed */
		Element Tail = null; /* where items are added */

		/**
		 * adds an item to the end of the linked list
		 * @param e is the item to add
		 */
		public void add(Element e) {
			if (Tail == null) {
				Head = e;
			}
			else {
				Tail.Next = e;
			}
			e.Next = null;
			Tail = e;
		}

		/**
		 * returns the first item in the linked list
		 * @return the first item, if the list is not empty;
		 * else it returns null
		 */
		public Element remove() {
			Element ele = Head;
			if (Head != null) {
				Head = Head.Next;
				if (Head == null) {
					Tail = null;
				}
				return ele;
			}
			return null;
		}

		/**
		 * a predicate for determining if the LinkedList is empty
		 * @return true if there is an element in the linked list
		 */
		public boolean isEmpty() {
			return Head == null;
		}
	}
	/****************************************************************
	 *   end of the LinkedList class
	 ****************************************************************/
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
			ManagedQueue.class.getName());
}
/* @(#)ManagedQueue.java */
