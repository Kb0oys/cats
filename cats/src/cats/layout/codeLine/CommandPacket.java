/* Name: CommandPacket.java
 *
 * What:
 *   This file defines a code line packet for carrying information
 *   from the office equipment to the field equipment.  The payload
 *   is a string and the recipient is Vital Logic.
 */
package cats.layout.codeLine;

import cats.apps.Crandic;
import cats.common.DebugBits;
import cats.layout.vitalLogic.BasicVitalLogic;
import cats.rr_events.RREvent;

/**
 *   This file defines a code line packet for carrying information
 *   from the office equipment to the field equipment.  The payload
 *   is a string and the recipient is Vital Logic.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2013, 2020, 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
class CommandPacket extends RREvent {
  /**
   * the command
   */
  private final String Payload;
  
  /**
   * the field equipment receiving the command
   */
  private final BasicVitalLogic Recipient;
  
  /**
   * the ctor
   * @param payload is the command being sent to the field equipment
   * @param recipient is the field equipment addressed
   */
  public CommandPacket(String payload, BasicVitalLogic recipient) {
    Payload = payload;
    Recipient = recipient;    
  }

  public void doIt() {
	  if (Crandic.Details.get(DebugBits.CODELINEBIT)) {
		  System.out.println("Delivering command " + Payload + " to " + Recipient);
	  }
    Recipient.processCommand(Payload);
  } 
}
/* @(#)CommandPacket.java */