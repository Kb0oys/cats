/* Name: ReporterPacket.java
 *
 * What:
 *   This file defines a code line packet for carrying information
 *   to a JMRI Reporter recipient.  The Reporter can be associated with
 *   either the office equipment or the field equipment.  The payload
 *   is a string and the recipient is a JMRI Reporter.
 */
package cats.layout.codeLine;

import cats.rr_events.RREvent;
import jmri.Reporter;

/**
 *   This file defines a code line packet for carrying information
 *   from the field equipment to the office equipment.  The payload
 *   is a string and the recipient is a JMRI Reporter.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2013</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
class ReporterPacket extends RREvent {
  /**
   * the command
   */
  private final String Payload;
  
  /**
   * the field equipment receiving the comamnd
   */
  private final Reporter Recipient;
  
  public ReporterPacket(String payload, Reporter recipient) {
    Payload = payload;
    Recipient = recipient;    
  }

  public void doIt() {
    Recipient.setReport(Payload);
  }
}
/* @(#)ReporterPacket.java */
