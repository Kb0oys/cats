/* Name: ResponsePacket.java
 *
 * What:
 *   This file defines a code line packet for carrying information
 *   from field equipment to office equipment.  The payload
 *   is a string and the recipient is a JMRI Reporter.
 */
package cats.layout.codeLine;

import cats.apps.Crandic;
import cats.common.DebugBits;
import cats.layout.items.TrackEdge;
import cats.rr_events.RREvent;

/**
 *   This file defines a code line packet for carrying information
 *   from field equipment to office equipment.  The payload
 *   is a string and the recipient is a JMRI Reporter.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2013, 2020, 2021, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
class ResponsePacket extends RREvent {
  /**
   * the response
   */
  private final String Payload;
  
  /**
   * the field equipment receiving the comamnd
   */
  private final TrackEdge Recipient;
  
  public ResponsePacket(String payload, TrackEdge recipient2) {
    Payload = payload;
    Recipient = recipient2;    
  }

  public void doIt() {
	  if (Crandic.Details.get(DebugBits.CODELINEBIT)) {
		  System.out.println("Delivering response " + Payload + " to " + Recipient);
	  }
    Recipient.processFieldResponse(Payload);
  }

}
/* @(#)ResponsePacket.java */