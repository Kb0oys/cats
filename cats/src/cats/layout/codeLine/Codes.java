/* Name Codes.java
 *
 * What:
 *  This class holds an enumeration defining the identifier of the lock that has
 *  changed state in the code line messages.
 *  <p>
 *  The defined codes are:
 *  <ul>
 *  <li> Traffic Lock - the signals and turnouts are locked because a route has
 *  been reserved through the track circuit.
 *  <li> Detection (or Route) Lock - the signals and turnouts are locked because
 *  the track circuit shows occupied.
 *  <li> Opposing Signal Lock - the signal is locked on Halt because an opposing signal
 *  has been cleared.
 *  <li> Conflicting Signal Lock - the signal is locked on Halt because a conflicting
 *  signal has been cleared.
 *  <li> Switch Indication Lock (fouling) - the points are fouling
 *  <li> Time Lock - signals and turnouts are locked because the dispatcher has knocked
 *  down a route and time is running before they can be reset.
 *  <li> Approach Lock - a precondition for time lock.  Time is not locked if the approach
 *  traffic circuit is unoccupied.
 *  <li> Switch Unlocked - the field has unlocked a switch
 *  <li> Switch alignment - the position of the turnout
 *  <li> Aspect - the aspect being shown by a signal mast
 *  </ul>
 */
package cats.layout.codeLine;

import cats.layout.items.GuiLocks;
import cats.layout.vitalLogic.LogicLocks;

/**
 *  This class holds an enumeration defining the identifier of the code line messages.
 *  <p>
 *  The defined codes are:
 *  <ul>
 *  <li> Detection (or Route) Lock - an Indication that the signals and turnouts are locked because
 *  the track circuit shows occupied.  Detection cheats a little in that the GUI can tell the Vital Logic
 *  that the track circuit is occupied or cleared without going through the code line.
 *  <li> Hold Lock - a signal is held at Stop because of an external condition.  Not yet implemented.
 *  <li> Switch Unlocked - An Indication that the field has unlocked a switch
 *  <li> Time Lock - An Indication that signals and turnouts are locked because 
 *  a route has been knocked down unexpectedly and time is running before they can be reset.
 *  <li> Signal Indication - A Command and Indication to lock the signals and turnouts because a route has
 *  been reserved through the track circuit.
 *  <li> Maintenance Lock - a Command that the dispatcher has granted track authority or out of service on a block
 *  protected by the signal
 *  <li> Call On Lock - A command to temporarily override the Vital Logic safety checks to allow
 *  a train to move into an occupied track circuit and an Indication to say it has been done.
 *  <li> Fleet Lock - A command to automatically regenerate a Signal Indication after the detection has cleared
 *  on a signal.  Unused because the GUI does the regeneration.
 *  <li> External Lock - the signal is locked on Halt because of an External reason.  Not yet implemented.  Could be
 *  used in conjunction with LCC?
 *  <li> Switch Position - A Command to move the points to a specific track (0 to 3) and an Indication that the points
 *  have moved to a specific track (-1 to 3).
 *  <li> Aspect - an Indication containing the aspect being shown by a signal mast
 *  </ul>
 * Title: CATS - Crandic Automated Traffic System
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2013, 2014, 2015, 2016, 2017, 2018, 2024
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public enum Codes {
    /** the track circuit reports occupied */
  DETECTIONLOCK('D'),
    /** the signal is held at Halt because of an external reason */
  HOLDLOCK('H'),
    /** a switch has been unlocked in the field */
  SWITCHUNLOCK('U'),
  /** a signal protecting the track circuit is running time */
  TIMELOCK('I'),
  /** a route has been reserved through the track circuit, entering through this end */
  SIGNALINDICATIONLOCK('E'),
  /** the dispatcher has given field crew local control (track authority or out of service */
  MAINTENANCELOCK('M'),
  /** the dispatcher has requested call-on */
  CALLONLOCK('B'),
  /** dispatcher has turned on Fleeting */
  FLEET('F'),
  /** an external thing is holding a signal at Stop **/
  EXTERNALLOCK('Z'),
  /** the position of the points */
  SWITCHPOSITION('P'),
  /** the current aspect */
  ASPECT('S');
  
  /**
   * the identifier of the code in a message
   */
  private final char CodeType;
  
  /**
   * the ctor that associates a letter with each enum
   * @param id
   */
  Codes(char id) {
    CodeType = id;
  }
  
  /**
   * a utility for converting a code to a mnemonic
   * @return the mnemomic letter for the code
   */
  public char toMnemomic() {
    return CodeType;
  }
  
  @Override
  public String toString() {
	  switch (this) {
	  case DETECTIONLOCK: return "Detection";
	  case HOLDLOCK: return "External Hold";
	  case SWITCHUNLOCK: return "Switch Unlock";
	  case TIMELOCK: return "Running Time";
	  case SIGNALINDICATIONLOCK: return "Route";
	  case MAINTENANCELOCK: return "Maintenance";
	  case CALLONLOCK: return "Call-on";
	  case FLEET: return "Fleeting";
	  case EXTERNALLOCK: return "External";
	  case SWITCHPOSITION: return "Switch Position";
	  case ASPECT: return "Signal Aspect";
	  default: 
	  }
	  return null;	  
  }
  
  /**
   * a utility for converting a mnemonic to a code
   * @param mnemonic is the tag on a code line message
   * @return the enum code if it is defined or null.
   */
  static public Codes toCode(char mnemonic) {
	  switch (mnemonic) {
	  case 'B': return CALLONLOCK;
	  case 'D': return DETECTIONLOCK;
	  case 'E': return SIGNALINDICATIONLOCK;
	  case 'F': return FLEET;
	  case 'H': return HOLDLOCK;
	  case 'I': return TIMELOCK;
	  case 'M': return MAINTENANCELOCK;
	  case 'P': return SWITCHPOSITION;
	  case 'S': return ASPECT;
	  case 'U': return SWITCHUNLOCK;
	  case 'Z': return EXTERNALLOCK;
	  default:
	  }
	  return null;
  }
  
  /**
   * a utility to convert a Command Code value to a LogicLock.  Note that
   * null is a legal value because some Codes (SwitchPosition and Aspect)
   * have no equivalent LogicLock.
   * @param code is the Codes value to be converted
   * @return the LogicLocks equivalent (if there is one) or null. 
   */
  static public LogicLocks toLogicLock(Codes code) {
	  switch (code) {
	  case DETECTIONLOCK: return LogicLocks.DETECTIONLOCK;
	  case SWITCHUNLOCK: return LogicLocks.SWITCHUNLOCK;
	  case TIMELOCK: return LogicLocks.TIMELOCK;
	  case MAINTENANCELOCK: return LogicLocks.MAINTENANCELOCK;
	  case CALLONLOCK: return LogicLocks.CALLONLOCK;
	  case FLEET: return LogicLocks.FLEETLOCK;
	  case EXTERNALLOCK: return LogicLocks.EXTERNALLOCK;
	  default: 
	  }
	  return null;
  }
  
  /**
   * a utility to convert a LogicLocks value to an Indication Code value.
   * Some Codes cannot be used as Indications; thus, cannot be returned from this
   * method.
   * @param lock is the LogicLocks to convert
   * @return the equivalent Codes value.  A non-null value should
   * always be returned because every LogicLock has a Code
   */
  static public Codes fromLogicLock(LogicLocks lock) {
	  switch (lock) {
	  case DETECTIONLOCK: return DETECTIONLOCK;
	  case SWITCHUNLOCK: return SWITCHUNLOCK;
	  case TIMELOCK: return TIMELOCK;
	  case TRAFFICLOCK: return SIGNALINDICATIONLOCK;
	  case MAINTENANCELOCK: return MAINTENANCELOCK;
	  case CALLONLOCK: return CALLONLOCK;
	  case FLEETLOCK: return FLEET;
	  case EXTERNALLOCK: return EXTERNALLOCK;
	  default:
		break;
	  }
	  return null;
  }
  
  /**
   * a utility to convert an Indication Code value to a GuiLock.  Note that
   * null is a legal value because some Codes (SwitchPosition and Aspect)
   * have no equivalent GuiLock.
   * @param code is the Codes value to be converted
   * @return the LogicLocks equivalent (if there is one) or null. 
   */
  static public GuiLocks toGuiLock(Codes code) {
	  switch (code) {
	  case DETECTIONLOCK: return GuiLocks.DETECTIONLOCK;
	  case SWITCHUNLOCK: return GuiLocks.SWITCHUNLOCK;
	  case TIMELOCK: return GuiLocks.TIMELOCK;
	  case SIGNALINDICATIONLOCK: return GuiLocks.TRAFFICLOCK;
	  case MAINTENANCELOCK: return GuiLocks.LOCALCONTROLLOCK;
	  case CALLONLOCK: return GuiLocks.CALLONLOCK;
	  case FLEET: return GuiLocks.FLEET;
	  case EXTERNALLOCK: return GuiLocks.EXTERNALLOCK;
	  default: 
		  break;
	  }
	  return null;
  }
  
  /**
   * a utility to convert a GuiLocks value to a Command Code value.
   * @param lock is the GuiLocks to convert
   * @return the equivalent Codes value.  A non-null value should
   * always be returned because every GuiLock has a Code
   */
  static public Codes fromGuiLock(GuiLocks lock) {
	  switch (lock) {
	  case DETECTIONLOCK: return DETECTIONLOCK;
	  case SWITCHUNLOCK: return SWITCHUNLOCK;
	  case TIMELOCK: return TIMELOCK;
	  case TRAFFICLOCK: return SIGNALINDICATIONLOCK;
	  case LOCALCONTROLLOCK: return MAINTENANCELOCK;
	  case CALLONLOCK: return CALLONLOCK;
	  case FLEET: return FLEET;
	  case EXTERNALLOCK: return EXTERNALLOCK;
	default:
		break;
	  }
	  return null;
  }
}
/* @(#)Codes.java */