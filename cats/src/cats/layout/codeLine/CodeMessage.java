/* Name CodeMessage.java
 *
 * What:
 *  This class defines methods for creating and parsing the payloads
 *  in code messages.
 *  <p>
 *  A code message has the following format:
 *  <ol>
 *  <li>Purpose (1 character)
 *      <ul>
 *      <li>R for request; from office equipment to field equipment</li>
 *      <li>I for indication; reports a change in a lock from the field equipment to the office equipment</li>
 *      <li>D for denial; reports that the following request was denied</li>
 *      </ul></li>
 *  <li>lock identifier (1 character)
 *      <ul>
 *      <li>A for Approach lock</li>
 *      <li>C for Conflicting signal lock</li>
 *      <li>D for Detection lock</li>
 *      <li>E for direction of travel through an edge</li>
 *      <li>F for switch Fouling</li>
 *      <li>H for Hold lock</li>
 *      <li>I for tIme lock</li>
 *      <li>O for Opposing signal lock</li>
 *      <li>P for turnout Position</li>
 *      <li>S for Signal aspect</li>
 *      <li>T for Traffic lock</li>
 *      <li>U for turnout Unlocked for local control</li>
 *      </ul></li>
 *  <li>the value of the lock.  For binary locks, it will be "set" or "clear".
 *  For turnout position, it will be the edge that the turnout is lined for.  For signal aspect,
 *  it will be the name of the aspect.
 *  </ol>
 *  
 */
package cats.layout.codeLine;

/**
 *  This class defines methods for creating and parsing the payloads
 *  in code messages.
 * Title: CATS - Crandic Automated Traffic System
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2013
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public class CodeMessage {

  /**
   * is the purpose of the message
   */
  public final CodePurpose Purpose;
  
  /**
   * is the lock of interest
   */
  public final Codes Lock;
  
  /**
   * is the value of the lock
   */
  public final String Value;
  
  /**
   * the ctor
   * @param message is a code line message
   */
  public CodeMessage(String message) {
    if (message.length() > 2) {
      Lock = Codes.toCode(message.charAt(1));
      Purpose = (Lock == null) ? null : CodePurpose.toCode(message.charAt(0));
      Value = message.substring(2);
    }
    else {
      Purpose = null;
      Lock = null;
      Value = null;
    }
  }
  
  /**
   * builds a code line message
   * @param purpose what the message is for
   * @param why is what lock is being changed
   * @param value is the new value of the lock
   * @return the String sent to the code line
   */
  static public String constructMessage(CodePurpose purpose, Codes why, String value) {
    return new String(String.valueOf(purpose.toMnemomic()) + String.valueOf(why.toMnemomic()) + value);
  }
}
