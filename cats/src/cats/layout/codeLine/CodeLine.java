/* Name: CodeLine.java
 *
 * What:
 *   This simulates a code line.  A code line is the communications channel between the
 *   office equipment (non-vital logic) and the field equipment (vital logic).  Historically,
 *   the code line was pulse code modulated, carrying requests from the office to the field and
 *   responses from the field back to the office, over slow speed copper wire.  The railroads are
 *   replacing the copper wire with radio links, using protocols such as ATCS (Advanced Train
 *   Control System) as a replacement for the pulse modulation.
 *   <p>
 *   In the prototype, there is one code line between the office and field equipment.  Since it is
 *   half-duplex, there is only one message on the line at a time; thus, trafic in one direction
 *   interferes with traffic in the other.  In CATS there are two simplex lines, one in each
 *   direction, each with its distinct timer.  I deviated from the prototype because, if the
 *   the uplink timer is non-zero, it can take quite awhile for the initial layout settings
 *   to be sent from the vital logic to the office.
 *   <p>
 *   In the prototype, the protocol has address fields, indicating the field equipment being
 *   addressed.  In CATS the recipient object is carried in the code packet.  This is (ironically)
 *   to reduce the packet encode/decode time.
 */
package cats.layout.codeLine;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import jmri.Reporter;
import cats.gui.Adjuster;
import cats.gui.CounterFactory;
import cats.gui.Sequence;
import cats.layout.DelayLine;
import cats.layout.DelayLineListener;
import cats.layout.items.TrackEdge;
import cats.layout.vitalLogic.BasicVitalLogic;
import cats.rr_events.RREvent;

/**
 *   This simulates a code line.  A code line is the communications channel between the
 *   office equipment (non-vital logic) and the field equipment (vital logic).  Historically,
 *   the code line was pulse code modulated, carrying requests from the office to the field and
 *   responses from the field back to the office, over slow speed copper wire.  The railroads are
 *   replacing the copper wire with radio links, using protocols such as ATCS (Advanced Train
 *   Control System) as a replacement for the pulse modulation.
 *   <p>
 *   In the prototype, there is one code line between the office and field equipment.  Since it is
 *   half-duplex, there is only one message on the line at a time; thus, trafic in one direction
 *   interferes with traffic in the other.  In CATS there are two simplex lines, one in each
 *   direction, each with its distinct timer.  I deviated from the prototype because, if the
 *   the uplink timer is non-zero, it can take quite awhile for the initial layout settings
 *   to be sent from the vital logic to the office.  Thus, the uplink and downlink delays
 *   can be different.
 *   <p>
 *   In the prototype, the protocol has address fields, indicating the field equipment being
 *   addressed.  In CATS the recipient object is carried in the code packet.  This is (ironically)
 *   to reduce the packet encode/decode time.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 20013, 2019, 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class CodeLine implements DelayLineListener<RREvent> {

  /**
   * the singleton code line for office to vital logic
   */
  private static final CodeLine DownLink = new CodeLine(CounterFactory.CODEDELAYTAG);

  /**
   * the singleton code line for vital logic to office
   */
  private static final CodeLine UpLink = new CodeLine(CounterFactory.UPLINKDELAYTAG);
  
  /**
   * a FIFO for holding code packets.
   */
  private DelayLine<RREvent> CodeQue;
  
  /**
   * the initial size of the codeline delay queue.  It will be expanded
   * automatically in the DelayLine, if needed.
   */
  private final int INITIALQUEUESIZE = 10;

  /**
   * is the Sequence containing the user selectable delay time (in seconds)
   */
  private Sequence DelaySequence;
  
  /**
   * is a flag for disabling the delay.  If true, the delay is set to 0; if false,
   * the delay is set to the Sequence value * 1000 (to convert from seconds to
   * milliseconds).  The delay is disabled during startup and refresh.
   */
  private boolean DelayDisabled;
  
//  /**
//   * packet counter
//   */
//  private int packetCounter = 0;
  
  /**
   * is the ctor.
   * @param tag is the sequence tag on the delay value
   */
  public CodeLine(String tag) {
	  DelaySequence = CounterFactory.CountKeeper.findSequence(tag);
	  CodeQue = new DelayLine<RREvent>(INITIALQUEUESIZE, this, 0);
	  DelayDisabled = true;
	  DelaySequence.registerAdjustmentListener(
			  new PropertyChangeListener() {
				  @Override
				  public void propertyChange(PropertyChangeEvent arg0) {
					  if (!DelayDisabled && arg0.getPropertyName().equals(Adjuster.CHANGE_TAG)) {
						  CodeQue.adjustTimeout(((Integer)arg0.getNewValue()).intValue() * 1000);
					  }
				  }});
  }

  /**
   * queues a CodePacket up for transmission.  If the queue
   * is empty, the delay timer is started.
   * @param packet is the code message being delayed
   */
  private void transmit(RREvent packet) {
	  CodeQue.append(packet);
  }

  /**
   * simulates completing the transmission of a CodePacket.
   * The variable CodedEvent is the packet being transmitted.
   * It must not be null and should not be since it is set
   * at the same time that the timer is set.  It is not processed
   * immediately, but enqueued on the RREvent queue.
   */
  public void consume(RREvent packet) {
	  packet.queUp();
  }
  
  /**
   * sends a command from the office equipment to some field
   * equipment
   * @param command is the command being transmitted
   * @param recipient is the field equipment to which the command is addressed
   */
  static public void sendToField(String command, BasicVitalLogic recipient) {
//	  System.out.println("codeline packet to Vital Logic: " + (++packetCounter));
    DownLink.transmit(new CommandPacket(command, recipient));
  }

  /**
   * sends a response from some field equipment to the office equipment
   * @param command is the command being transmitted
   * @param recipient is the office equipment
   */
  static public void sendToOffice(String command, TrackEdge recipient) {
//	  System.out.println("codeline packet to office: " + (++packetCounter));
    UpLink.transmit(new ResponsePacket(command, recipient));
  }

  /**
   * sends a command or response to a JMRI Reporter.  This method is used to expose
   * the code line interface outside of CATS.  The external automation (Logix or
   * Jython) creates a String and designates the Reporter that it is addressed to.
   * This means that the vital Logic or office object is designated in designer.
   * <p>
   * Similarly, the external automation can listen to the Reporter for changes.
   * @param payload is the command or response being sent
   * @param recipient is the JMRI Reporter receiving the command or response
   */
  static public void sendToReporter(String payload, Reporter recipient) {
//	  System.out.println("codeline packet to reporter: " + (++packetCounter));
    DownLink.transmit(new ReporterPacket(payload, recipient));   
  }
  
  /**
   * the external access method to enable/disable the delay on both codelines.
   * @param delay is true to disable the delay and false to enable it
   */
  static public void disable(final boolean delay) {
	  DownLink.disableDelay(delay);
	  UpLink.disableDelay(delay);
  }
  
  /**
   * provides control over enabling/disabling the delay on the code line.
   * To prevent this feature from creeping into the DelayLine class, enabling and
   * disabling is performed by manipulating the DelayLne timeout value.  When
   * disabled, the timeout is set to 0.  When enabled, the timeout is set to the
   * Sequence value * 1000 (to convert to milliseconds).
   * @param delay is true to disable the delay and false to enable it.
   */
  private void disableDelay(final boolean delay) {
	  DelayDisabled = delay;
	  if (DelayDisabled) {
		  CodeQue.adjustTimeout(0);
	  }
	  else {
		  CodeQue.adjustTimeout(DelaySequence.getAdjustment() * 1000);
	  }
  }
}
/* @(#)CodeLine.java */