/* Name: ReporterPacketFactory.java
 *
 * What:
 * This file contains the PacketFactory for creating codeline packets whose destination
 * is a JMRI Reporter.
 */
package cats.layout.codeLine.packetFactory;

import jmri.Reporter;
import cats.layout.codeLine.CodeLine;
import cats.layout.codeLine.CodeMessage;
import cats.layout.codeLine.CodePurpose;
import cats.layout.codeLine.Codes;

/**
 * This file contains the PacketFactory for creating codeline packets whose destination
 * is a JMRI Reporter.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2013, 2014, 2019</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class ReporterPacketFactory implements PacketFactory {

  private Reporter Recipient;
  
  /**
   * the ctor
   * @param recipient is the Reporter receiving codes from the code line
   */
  public ReporterPacketFactory(Reporter recipient) {
    Recipient = recipient;
  }
  

  /**
   * creates and transmits a packet on the code line
   * @param purpose is the the reason for the packet - @see cats.layout.codeLine.CodePurpose
   * @param why - is the lock being referenced - @see cats.layout.codeLine.Codes
   * @param value - is the value of the lock
   * 
   * @return true if the packet was successfully created and sent; otherwise, false.
   */
 public boolean createPacket(CodePurpose purpose, Codes why, String value) {
    String message = CodeMessage.constructMessage(purpose, why, value);
    if (message != null) {
//      CodeLine.TheCodeLine.sendToReporter(message, Recipient);
    	CodeLine.sendToReporter(message, Recipient);
      return true;
    }
    return false;
  }
}
/* @(#)ReporterPacketFactory.java */
