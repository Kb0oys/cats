/* Name: PacketFactory.java
 *
 * What:
 * This file contains the interface for PacketFactories.  A PacketFactory creates code
 * packets for the CodeLine.  It follows one of the factory design patterns.
 */
package cats.layout.codeLine.packetFactory;

import cats.layout.codeLine.CodePurpose;
import cats.layout.codeLine.Codes;

/**
 * This file contains the interface for PacketFactories.  A PacketFactory creates code
 * packets for the CodeLine.  It follows one of the factory design patterns.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2013, 2014</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface PacketFactory {

  /**
   * there is only one method - createPacket
   */
  
  /**
   * creates and transmits a packet on the code line
   * @param purpose is the the reason for the packet - @see cats.layout.codeLine.CodePurpose
   * @param why - is the lock being referenced - @see cats.layout.codeLine.Codes
   * @param value - is the value of the lock
   * 
   * @return true if the packet was successfully created and sent; otherwise, false.
   */
  public boolean createPacket(CodePurpose purpose, Codes why, String value);
}
/* @(#)PacketFactory.java */