/* Name CodePurpose.java
 *
 * What:
 *  This class holds an enumeration defining the identifier of the message
 *  <p>
 *  The defined purposes are:
 *  <ul>
 *  <li>D for Denied - a request from the office equipment could not be honored</li>
 *  <li>I for Indication - a lock in the vital logic has changed value</li>
 *  <li>R for Request - a request is being made by the office equipment to change the
 *  a lock in the field equipment
 *  </ul>
 */
package cats.layout.codeLine;
/*
 *  This class holds an enumeration defining the identifier of the message
 *  <p>
 *  The defined purposes are:
 *  <ul>
 *  <li>D for Denied - a request from the office equipment could not be honored</li>
 *  <li>I for Indication - a lock in the vital logic has changed value</li>
 *  <li>R for Request - a request is being made by the office equipment to change the
 *  a lock in the field equipment
 *  </ul>
 * Title: CATS - Crandic Automated Traffic System
 * </p>
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2014
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public enum CodePurpose {
  DENY('D'),
  INDICATION('I'),
  REQUEST('R');
  
  /**
   * the identifier of the code in a message
   */
  private final char Purpose;
  
  /**
   * the ctor that associates a letter with each enum
   * @param id is the letter
   */
  CodePurpose(char id) {
    Purpose = id;
  }
  
  /**
   * a utility for converting a code to a mnemonic
   * @return the mnemomic letter for the code
   */
  public char toMnemomic() {
    return Purpose;
  }
  
  /**
   * a utility for converting a mnemonic to a code
   * @param mnemonic is the mnemonic being converted
   * @return the enum code if it is defined or null.
   */
  static public CodePurpose toCode(char mnemonic) {
    switch (mnemonic) {
    case 'D': return DENY;
    case 'I': return INDICATION;
    case 'R': return REQUEST;
        default:
    }
    return null;
  }

}
/* @(#)CodePurpose.java */