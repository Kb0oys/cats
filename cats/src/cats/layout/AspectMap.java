/*
 * Name: AspectMap.java
 *
 * What:
 *  This file contains a data structure for associating Indications (the
 *  meaning of signals) with aspects (the presentation of signals).
 *  <p>
 *  Its purpose is to translate a CATS SignalTemplate definition (created
 *  in designer) into the two critical data structures of a JMRI
 *  SignalAppearanceMap.  The result enables CATS to use JMRI SignalMasts
 *  completely for driving the signals on a layout.
 *  <p>
 *  The two SignalAppearanceMap structures are
 *  <ul>
 *  <li><B>aspectAttributeMap</B> - a Hashtable of Hashtables.  It contains
 *  the freeform "appearances" section of a JMRI Signals XML file.  Each
 *  element is a Hashtable, with the key being the appearance name.  For CATS,
 *  the keys are the XML field of the IndicationNames 2D array below.
 *  <p>
 *  Each value in the Hashtable is itself a Hashtable.  At the second level,
 *  the key is the name of an attribute and the value is a String.  The keys are
 *  not defined in JMRI, but are a contract between the XML file author and
 *  signal logic author.  For CATS, the keys are: "aspectname" (required);
 *  "show" (one entry for each head) of "red", "green", etc.; and "reference"
 *  - a string indicating the rule CATS is using.
 *  </li>
 *  <li><B>table</B> - a Hashtable of array of integers.  The key for each entry
 *  is again the name of the appearance.  The array of integers is the JMRI SignalHead
 *  color constant for each head.
 *  </li>
 *  </ul>
 * <p>
 * Note that there is no "Tumbledown" speed.  "Tumbledown" is a Stop that propagates
 * to the first Control Point or Head signal in approach.  Thus, it is variation on Stop.
 * It is not included in the template table, so not included here.  However, the FieldSignal
 * must deal with it.
 * 
 */

package cats.layout;

import cats.layout.items.PhysicalSignal;
import cats.layout.items.Track;
import cats.layout.xml.*;
import java.util.HashMap;
import java.util.StringTokenizer;


/**
 *  This file contains a data structure for associating Indications (the
 *  meaning of signals) with aspects (the presentation of signals).
 *  <p>
 *  Its purpose is to translate a CATS SignalTemplate definition (created
 *  in designer) into the two critical data structures of a JMRI
 *  SignalAppearanceMap.  The result enables CATS to use JMRI SignalMasts
 *  completely for driving the signals on a layout.
 *  <p>
 *  The two SignalAppearanceMap structures are
 *  <ul>
 *  <li><B>aspectAttributeMap</B> - a Hashtable of Hashtables.  It contains
 *  the freeform "appearances" section of a JMRI Signals XML file.  Each
 *  element is a Hashtable, with the key being the appearance name.  For CATS,
 *  the keys are the XML field of the IndicationNames 2D array below.
 *  <p>
 *  Each value in the Hashtable is itself a Hashtable.  At the second level,
 *  the key is the name of an attribute and the value is a String.  The keys are
 *  not defined in JMRI, but are a contract between the XML file author and
 *  signal logic author.  For CATS, the keys are: "aspectname" (required);
 *  "show" (one entry for each head) of "red", "green", etc.; and "reference"
 *  - a string indicating the rule CATS is using.
 *  </li>
 *  <li><B>table</B> - a Hashtable of array of integers.  The key for each entry
 *  is again the name of the appearance.  The array of integers is the JMRI SignalHead
 *  color constant for each head.
 *  </li>
 *  </ul>
 * <p>
 * Note that there is no "Tumbeldown" speed.  "Tumbledown" is a Stop that propagates
 * to the first Control Point or Head signal in approach.  Thus, it is a variation on Stop.
 * It is not included in the template table, so not included here.  However, the FieldSignal
 * must deal with it.
 * 
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * </p>
 * <p>Copyright: Copyright (c) 2004, 2010, 2012, 2014, 2015, 2016, 2017, 2018, 2019, 2022,
 * 2024
 * </p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class AspectMap
    implements XMLEleObject {

  /**
   * is the tag for identifying an AspectMap in the XML file.
   */
  static final String XML_TAG = "ASPECTMAP";

  /**
   * is the XML tag for the approach attribute.
   */
  static final String XML_APPROACH = "APPROACH";
  
  /**
   * is the value of the Approach attribute, when approach is enabled.
   */
  static final String ISAPPROACH = "true";
  
  /**
   * is the tag for the advance signal attribute
   */
  static final String XML_ADVANCE = "ADVANCE";
  
  /**
   * is the tag for the "Hold Only" attribute
   */
  static final String HOLD_TAG = "HOLD_ONLY";
  
  /**
   * is the separator between heads states for each indication.  It is a
   * String, even though it is a single character, so that it can be
   * passed to the Streamtokenzier without alteration.
   */
  static final String SEP = "|";

  /**
   * is the prefix on the Advance speed definitions
   */
  static private final String ADV_PREFIX = "ADV_";
  
  /**
   * is the tag for a Restricting definition from an older panel.  It is
   * used to populate the definitions in a new panel.
   */
  static final String RESTRICTING_TAG = "R290";

  /**
   * is the prefix on the Restricting definitions
   */
  static final String RESTRICTING_PREFIX = "RES_";
  
  /**
   * is the tag for a Halt definition from an older panel.  It is
   * used to populate the definitions in a new panel.
   */
  static final String HALT_TAG = "R292";

  /**
   * is the suffix on the Normal speed definition
   */
  static private final String NORMAL_SUFFIX = "NORM";
  
  /**
   * is the suffix on the Limited speed definition
   */
  static private final String LIMITED_SUFFIX = "LIM";
  
  /**
   * is the suffix on the Medium speed definition
   */
  static private final String MEDIUM_SUFFIX = "MED";
  
  /**
   * is the suffix on the SLow speed definition
   */
  static private final String SLO_SUFFIX = "SLO";
  
  /**
   * The following describes all the possible indications supported
   * by the dispatcher panel.  The list represents 4 speeds (normal,
   * limited, medium, slow), with 2 block (the protected block and
   * the following block).  The major index in the 2 dimensional array
   * is the indication.  The minor index has three values: a label which is
   * used only to print out the indication; the "rule", which is the
   * key field for identifying the indication (reverse table lookup);
   * and the name of a Color for a panel icon showing that indication.  Note
   * that there are four rules for "Restricting".  This is so that the speed
   * can distinguish between the variations.
   */
  public static final String[][] IndicationNames = {
      {
      "ARA 281 - Clear", "R281", ColorList.CLEAR}
      , {
      "ARA 281B - Normal Approach Limited", "R281B", ColorList.CLEAR}
      , {
      "ARA 282 - Normal Approach Medium", "R282", ColorList.CLEAR}
      , {
      "ARA 284 - Normal Approach Slow", "R284", ColorList.CLEAR}
      , {
      "Restricting Normal", RESTRICTING_PREFIX + NORMAL_SUFFIX, ColorList.STOP}
      , {
      "Advance Approach", ADV_PREFIX + NORMAL_SUFFIX, ColorList.APPROACH}
      , {
      "ARA 285 - Approach", "R285", ColorList.APPROACH}
      , {
      "ARA 281C - Limited Clear", "R281C", ColorList.CLEAR}
      , {
      "CROR 412 - Limited Approach Limited", "C412", ColorList.CLEAR}
      , {
      "CROR 413 - Limited Approach Medium", "C413", ColorList.CLEAR}
      , {
      "CROR 414 - Limited Approach Slow", "C414", ColorList.CLEAR}
      , {
      "Restricting Limited", RESTRICTING_PREFIX + LIMITED_SUFFIX, ColorList.STOP}
      , {
      "Advance Limited Approach", ADV_PREFIX + LIMITED_SUFFIX, ColorList.APPROACH}
      , {
      "ARA 281D - Limited Approach", "R281D", ColorList.APPROACH}
      , {
      "ARA 283 - Medium Clear", "R283", ColorList.CLEAR}
      , {
      "CROR 417 - Medium Approach Limited", "C417", ColorList.CLEAR}
      , {
      "ARA 283A - Medium Approach Medium", "R283A", ColorList.CLEAR}
      , {
      "ARA 283B - Medium Approach Slow", "R283B", ColorList.CLEAR}
      , {
      "Restricting Medium", RESTRICTING_PREFIX + MEDIUM_SUFFIX, ColorList.STOP}
      , {
      "Advance Medium Approach", ADV_PREFIX + MEDIUM_SUFFIX, ColorList.APPROACH}
      , {
      "ARA 286 - Medium Approach", "R286", ColorList.APPROACH}
      , {
      "ARA 287 - Slow Clear", "R287", ColorList.CLEAR}
      , {
      "CROR 422 - Slow Approach Limited", "C422", ColorList.CLEAR}
      , {
      "CROR 423 - Slow Approach Medium", "C423", ColorList.CLEAR}
      , {
      "CROR 424 - Slow Approach Slow", "C424", ColorList.CLEAR}
      , {
      "Restricting Slow", RESTRICTING_PREFIX + SLO_SUFFIX, ColorList.STOP}
      , {
      "Advance Slow Approach", ADV_PREFIX + SLO_SUFFIX, ColorList.APPROACH}
      , {
      "ARA 288 - Slow Approach", "R288", ColorList.APPROACH}
      , {
      "R291 - Stop and Proceed", "R291", ColorList.STOP}
      , {
      "ARA 292 - Stop", "R292", ColorList.STOP}
  };

  /**
   * constants defining the train speed behind a signal
   */
//  static final public int NORMAL_SPEED = 60;
//  static final public int LIMITED_SPEED = 45;
//  static final public int MEDIUM_SPEED = 30;
//  static final public int SLOW_SPEED = 15;
//  static final public int RESTRICTING_SPEED = 15;
//  static final public int STOP_SPEED = 0;
//  static final public int APPROACH_SPEED = 30;
  static final public String NORMAL_SPEED = "normal";
  static final public String LIMITED_SPEED = "limited";
  static final public String MEDIUM_SPEED = "medium";
  static final public String SLOW_SPEED = "slow";
  static final public String RESTRICTING_SPEED = "restricting";
  static final public String STOP_SPEED = "stop";
  static final public String APPROACH_SPEED ="approach";
 
  /**
   * the protected speed for each aspect, as a String.  These are used
   * in constructing the tables for working with JMRI Signal Masts.
   */
  //  static final public int Speed[] = {
  static final public String Speed[] = {
	  NORMAL_SPEED,			//ARA 281 - Clear
	  NORMAL_SPEED,			//ARA 281B - Normal Approach Limited
	  NORMAL_SPEED,			//ARA 282 - Normal Approach Medium
	  NORMAL_SPEED,			//ARA 284 - Normal Approach Slow
	  RESTRICTING_SPEED,	//Restricting Normal
	  LIMITED_SPEED,		//Advance Approach
	  APPROACH_SPEED,		//ARA 285 - Approach
	  LIMITED_SPEED,		//ARA 281C - Limited Clear
	  LIMITED_SPEED,		//CROR 412 - Limited Approach Limited
	  LIMITED_SPEED,		//CROR 413 - Limited Approach Medium
	  LIMITED_SPEED,		//CROR 414 - Limited Approach Slow
	  RESTRICTING_SPEED,	//Restricting Limited
	  LIMITED_SPEED,		//Advance Limited Approach
	  APPROACH_SPEED,		//ARA 281D - Limited Approach
	  MEDIUM_SPEED,			//ARA 283 - Medium Clear
	  MEDIUM_SPEED,			//CROR 417 - Medium Approach Limited
	  MEDIUM_SPEED,			//ARA 283A - Medium Approach Medium
	  MEDIUM_SPEED,			//ARA 283B - Medium Approach Slow
	  RESTRICTING_SPEED,	//Restricting Medium
	  MEDIUM_SPEED,			//Advance Medium Approach
	  APPROACH_SPEED,		//ARA 286 - Medium Approach
	  SLOW_SPEED,			//ARA 287 - Slow Clear
	  SLOW_SPEED,			//CROR 422 - Slow Approach Limited
	  SLOW_SPEED,			//CROR 423 - Slow Approach Medium
	  SLOW_SPEED,			//CROR 424 - Slow Approach Slow
	  RESTRICTING_SPEED,	//Restricting Slow
	  SLOW_SPEED,			//Advance Slow Approach
	  SLOW_SPEED,			//ARA 288 - Slow Approach
	  STOP_SPEED,			//R291 - Stop and Proceed
	  STOP_SPEED			//ARA 292 - Stop
  };
  
  /**
   * the above, but translated to an int.
   */
  static final public int ProtectedSpeed[] = {
		  Track.NORMAL,			//ARA 281 - Clear
		  Track.NORMAL,			//ARA 281B - Normal Approach Limited
		  Track.NORMAL,			//ARA 282 - Normal Approach Medium
		  Track.NORMAL,			//ARA 284 - Normal Approach Slow
		  Track.RESTRICTING,	//Restricting (None) Normal
		  Track.NORMAL,			//Advance Approach
		  Track.NORMAL,		//ARA 285 - Approach
		  Track.LIMITED,		//ARA 281C - Limited Clear
		  Track.LIMITED,		//CROR 412 - Limited Approach Limited
		  Track.LIMITED,		//CROR 413 - Limited Approach Medium
		  Track.LIMITED,		//CROR 414 - Limited Approach Slow
		  Track.RESTRICTING,	//Restricting (None) Limited
		  Track.LIMITED,		//Advance Limited Approach
		  Track.LIMITED,		//ARA 281D - Limited Approach
		  Track.MEDIUM,			//ARA 283 - Medium Clear
		  Track.MEDIUM,			//CROR 417 - Medium Approach Limited
		  Track.MEDIUM,			//ARA 283A - Medium Approach Medium
		  Track.MEDIUM,			//ARA 283B - Medium Approach Slow
		  Track.RESTRICTING,	//Restricting (None) Medium
		  Track.MEDIUM,			//Advance Medium Approach
		  Track.MEDIUM,		//ARA 286 - Medium Approach
		  Track.SLOW,			//ARA 287 - Slow Clear
		  Track.SLOW,			//CROR 422 - Slow Approach Limited
		  Track.SLOW,			//CROR 423 - Slow Approach Medium
		  Track.SLOW,			//CROR 424 - Slow Approach Slow
		  Track.RESTRICTING,	//Restricting (None) Slow
		  Track.SLOW,			//Advance Slow Approach
		  Track.SLOW,		//ARA 288 - Slow Approach
		  Track.STOP_AND_GO,	//R291 - Stop and Proceed
		  Track.STOP,			//ARA 292 - Stop
		  Track.STOP,			// Tumbledown - not used?
		  Track.STOP			// Opposing Signal - not used?
  };
  
  /**
   * the lookup table to translate speeds into Rules.  Given the speed of the protected
   * block and the speed of the next block, it returns the Rule (indication).
   * 
   * Though there are four indication speeds, the table is 10x10.  The additional "speeds" are
   * for DEFAULT, NONE, APPROACH, RESTRICTING, STOP_AND_GO, and STOP; thus, it is safest to not
   * hard code any indexes, but use the speed constants in the Track file.
   * 
   * This table is NOT the same size as the template table and the indexes do not match up!
   */
  private static int[][] SpeedTable;

  /**
   * The index into IndicationName of the dialog label.
   */
  public static final int LABEL = 0;

  /**
   * the index into IndicationName of the XML tag.
   */
  public static final int XML = 1;

  /**
   * the index into IndicationName of the panel icon color for the signal
   */
  public static final int ICON = 2;
  
  /**
   * the key fields for creating the attributes Hashtable
   */
  private static final String ASPECTNAME = "aspectname";
  private static final String SHOW = "show";

  /**********************************************************
   * some constants that are indexes into IndicationNames and
   * derived values
   **********************************************************/
  /**
   * the index of the STOP (last) entry
   */
  public static final int STOP_INDEX = IndicationNames.length - 1;

  /**
   * the index of Stop and Proceed
   */
  public static final int PROCEED_INDEX = STOP_INDEX - 1;
  
  /**
   * the pseudo index of the Tumbledown indication
   */
  public static final int TUMBLEDOWN_INDEX = STOP_INDEX + 1;
  
  /**
   * the pseudo index of the Opposing signal indication
   */
  public static final int OPPOSING_INDEX = TUMBLEDOWN_INDEX + 1;
  
  /**
   * the type of each indication.  This is needed primarily
   * for implementing advance approach.  Approach has
   * precedence over diverging.
   */
  public static enum INDICATION_TYPE {
	  CLEAR,
	  ADVANCEAPP,
	  APPROACH,
	  DIVERGING,
	  HALT
  }
  
  /**
   * the mapping from rule to type, for use in determining approach
   * and advance approach indications.
   */
  public static final INDICATION_TYPE[] IndicationType= {
		  INDICATION_TYPE.CLEAR			//ARA 281 - Clear
      ,INDICATION_TYPE.CLEAR			//ARA 281B - Normal Approach Limited or Approach Diverging
      ,INDICATION_TYPE.CLEAR			//ARA 282 - Normal Approach Medium or Approach Diverging
      ,INDICATION_TYPE.CLEAR			//ARA 284 - Normal Approach Slow or Approach Diverging
      ,INDICATION_TYPE.HALT			//Restricting Normal or Restricting
      ,INDICATION_TYPE.ADVANCEAPP	//Advance Approach
      ,INDICATION_TYPE.APPROACH		//ARA 285 - Approach
      ,INDICATION_TYPE.DIVERGING		//ARA 281C - Limited Clear or Diverging
      ,INDICATION_TYPE.DIVERGING		//CROR 412 - Limited Approach Limited or Diverging Approach Diverging
      ,INDICATION_TYPE.DIVERGING		//CROR 413 - Limited Approach Medium or Diverging Approach Diverging
      ,INDICATION_TYPE.DIVERGING		//CROR 414 - Limited Approach Slow or Diverging Approach Diverging
      ,INDICATION_TYPE.HALT			//Restricting Limited or Restricting
      ,INDICATION_TYPE.ADVANCEAPP	//Advance Limited Approach or Advance Approach
      ,INDICATION_TYPE.APPROACH		//ARA 281D - Limited Approach or Diverging Approach
      ,INDICATION_TYPE.DIVERGING		// ARA 283 - Medium Clear or Diverging
      ,INDICATION_TYPE.DIVERGING		//CROR 417 - Medium Approach Limited or Diverging Approach Diverging
      ,INDICATION_TYPE.DIVERGING		//ARA 283A - Medium Approach Medium or Diverging Approach Diverging
      ,INDICATION_TYPE.DIVERGING		//ARA 283B - Medium Approach Slow or Diverging Approach Diverging
      ,INDICATION_TYPE.HALT			//Restricting Medium or Restricting
      ,INDICATION_TYPE.ADVANCEAPP	//Advance Medium Approach or Advance Approach
      ,INDICATION_TYPE.APPROACH		//ARA 286 - Medium Approach  or Diverging Approach
      ,INDICATION_TYPE.DIVERGING		//ARA 287 - Slow Clear or Diverging
      ,INDICATION_TYPE.DIVERGING		//CROR 422 - Slow Approach Limited or Diverging Approach Diverging
      ,INDICATION_TYPE.DIVERGING		//CROR 423 - Slow Approach Medium or Diverging Approach Diverging
      ,INDICATION_TYPE.DIVERGING		//CROR 424 - Slow Approach Slow or Diverging Approach Diverging
      ,INDICATION_TYPE.HALT			//Restricting Slow or Restricting
      ,INDICATION_TYPE.ADVANCEAPP	//Advance Slow Approach or Advance Approach
      ,INDICATION_TYPE.APPROACH		//ARA 288 - Slow Approach  or Diverging Approach
      ,INDICATION_TYPE.HALT			//R291 - Stop and Proceed
      ,INDICATION_TYPE.HALT			//ARA 292 - Stop
      ,INDICATION_TYPE.HALT			// Tumbledown
      ,INDICATION_TYPE.HALT			// Opposing Signal
  };
    
  /**
   * the mapping of indication to lights/semaphore position.
   * There is one rule (indication) per row and one signal
   * head per column.
   */
  private String[][] Map;

  /**
   * the number of heads.  This should be 0 (for an object which has
   * been instantiated, but not initialized) or a positive.  Once set to
   * the latter, then all indications should have the same number of
   * heads (columns).
   */
  private int NumHeads;

  /**
   * is true if the signal uses approach lighting; false if it doesn't.
   * The default is false.  Approach lighting means the signal is lit
   * only when the block facing the signal is occupied.
   */
  private boolean HasApproach;

  /**
   * is true if the signal can display an Advanced indication; false
   * if it does not.  To be conmpatible with older versions, the default
   * is false.
   */
  private boolean HasAdvance;
  
  /**
   * a flag which is true if the signal should set the SignalMast
   * aspect (if false) or only set and clear hold (for SignalMasts
   * that have their own aspect logic).
   */
  private boolean HasHoldOnly;

  /**
   * constructs the table of signal aspects for each indication.
   */
  public AspectMap() {
    Map = new String[IndicationNames.length][];
    NumHeads = 0;
    HasApproach = false;
    HasAdvance = false;
    HasHoldOnly = false;
  }

  /**
   * constructs the table of signal aspects for each indication from
   * an array of strings.
   *
   * @param settings is an array of Strings.  Each entry is one indication.
   * An entry could be null, meaning there is no indication for it.  Each
   * string should have the same number of SEP separated substrings,
   * for each substring describes the head's state for that indication.
   *
   * @param app is true if the signal has approach lighting and false if
   * it doesn't.
   * 
   * @param adv is true if the signal can display advanced indications and
   * false if it cannot.
   * 
   * @param hold is true if CATS does not set the SignalMast aspect, but
   * defers to the SignalMast logic.  CATS will will hold and remove the hold for
   * CTC signals.
   */
  public AspectMap(String[] settings, boolean app, boolean adv, boolean hold) {
    this();
    if (settings.length == IndicationNames.length) {
      StringTokenizer st;
      String[] indication;
      int heads;
      int position;

      HasApproach = app;
      HasAdvance = adv;
      HasHoldOnly = hold;
      // Extract the composite Strings from each indication.
      for (int ind = 0; ind < IndicationNames.length; ++ind) {
        if (settings[ind] != null) {
          // Find out how many heads are represented by counting separators.
          heads = 1;
          position = 0;

          while (position < settings[ind].length()) {
            if (settings[ind].charAt(position) == SEP.charAt(0)) {
              ++heads;
            }
            ++position;
          }
          st = new StringTokenizer(settings[ind], SEP);
          indication = new String[heads];
          heads = 0;
          while (st.hasMoreElements()) {
            indication[heads] = st.nextToken();
            // Flush the separator.
            if (!indication[heads].equals(SEP)) {
              ++heads;
            }
          }
          addAspect(ind, indication);
        }
      }
    }
    else {
      System.out.println(
          "Error in creating an AspectMap - wrong number of indications:"
          + settings.length);
    }
  }

  /**
   * retrieves the number of heads in the Aspect.
   *
   * @return the number of heads contributing to the aspect.
   */
  public int getHeadCount() {
    return NumHeads;
  }

//  /**
//   * retrieves the signal presentation for a particular head for a particular
//   * indication.  This method can be removed when CATS is converted to using
//   * SignalMasts.
//   *
//   * @param ind is the index of the indication.
//   * @param head is the head.
//   *
//   * @return a String describing the Light/Semaphore.  Null is legal if the
//   * light/semaphore does not exist or has no value.
//   */
//  public String getPresentation(int ind, int head) {
//    if ( (ind >= 0) && (ind < IndicationNames.length) && (head >= 0) &&
//        (head < NumHeads)) {
//      if ( (Map[ind] != null) && (Map[ind][head] != null)) {
//        return new String(Map[ind][head]);
//      }
//    }
//    return null;
//  }

//  /**
//   * retrieves the set of all presentations defined for a head.
//   *
//   * @param head is the number of the head.
//   *
//   * @return an Enumeration over the Strings describing the expected presentation
//   * of the head or null, if there are none.
//   */
//  public Enumeration getState(int head) {
//    if ( (head >= 0) && (head < NumHeads)) {
//      return new HeadEnumeration(head);
//    }
//    return null;
//  }

  /**
   * fills out a row in the Map, based on index.
   *
   * @param index is where in the array to place the Signal information.
   * @param aspects is the signal information
   */
  private void addAspect(int index, String[] aspects) {
    int heads;
    if (aspects == null) {
      Map[index] = null;
    }
    else {
      heads = aspects.length;
      Map[index] = new String[heads];
      if (NumHeads == 0) {
        NumHeads = heads;
      }
      else if (NumHeads != heads) {
        System.out.println("Inconsistent number of heads for an AspectMap:"
                           + " expecting " + NumHeads + " received " +
                           heads);
        heads = 0;
      }
      for (int a = 0; a < heads; ++a) {
    	  // these are needed to convert semaphore aspects to lights because
    	  // JMRI does not know about semaphores.
    	  if ("horizontal".equals(aspects[a])) {
    		  aspects[a] = "red";
    	  }
    	  else if ("diagonal".equals(aspects[a])) {
    		  aspects[a] = "yellow";
    	  }
    	  else if ("vertical".equals(aspects[a])) {
    		  aspects[a] = "green";
    	  }
    	  Map[index][a] = new String(aspects[a]);
      }
    }
  }

  /**
   * returns the approach flag.
   *
   * @return true if the signal should be dark unless the facing block
   * is occupied; false if it shoul dbe lit at all times.
   */
  public boolean getApproachLight() {
    return HasApproach;
  }

  /**
   * returns the Advanced flag.
   * @return true if the signal has aspects for advanced approach
   * indications.
   */
  public boolean getAdvanced() {
    return HasAdvance;
  }
  
  /**
   * returns the value of HoldOnly.
   *
   * @return true if CATS defers setting the SignalMast aspect to
   * the mast logic.  If true, CATS holds the signal when set to STOP.
   * When false, CATS sets the aspect.
   */
  public boolean getHoldOnly() {
      return HasHoldOnly;
  }

//  /**
//   * returns the index into IndicationNames of Stop
//   * @return the index of the Stop appearance
//   */
//  public static final int getStopIndex() {
//	  return IndicationNames.length - 1;
//  }
//  
//  /**
//   * returns the index into IndcationNames of Stop and Proceed
//   * @return the index of the Stop and Proceed index
//   */
//  public static final int getStopAndProceed() {
//	  return IndicationNames.length - 2;
//  }
 
  /**
   * returns the "protected" speed for an indication.  This allows
   * the track speed exchanged between signals to be extracted from
   * the indication of the signal in advance.  The speed corresponds to
   * the row number in the aspect table of the indication.  The simple
   * way to compute it is to divide by 7 (the number of columns
   * in a row) and add 1.  Speed 0 is the "default" speed.
   * @param indication is one of the 30 indications
   * @return the "protected" speed (row) of the indication.
   */
  public static final int getSpeed(int indication) {
	  if ((indication < 0) || (indication >= ProtectedSpeed.length)) {
		  return Track.NONE;
	  }
	  return ProtectedSpeed[indication];
  }
  
//  /**************************************************************************
//   * An inner class.
//   ************************************************************************/
//
//  /**
//   * is an inner class for providing an Enumeration over the Strings
//   * describing the presentation of a head.  This is not thread safe.
//   */
//  class HeadEnumeration
//      implements Enumeration<String> {
//
//    /**
//     * is the number of the head being traversed.
//     */
//    int EnumHead;
//
//    /**
//     * is the Indication that will be given out next.
//     */
//    int EnumInd;
//
//    /**
//     * constructs the Enumeration.
//     * @param head is the number of the SignalHead
//     */
//    HeadEnumeration(int head) {
//      EnumHead = head;
//      EnumInd = search(0);
//    }
//
//    /**
//     * indicates if all the presentations have been given out.
//     *
//     * @return true if nextElement() has been called for all the Indications or
//     * false if there are some remaining.
//     */
//
//    public boolean hasMoreElements() {
//
//      return (EnumInd < IndicationNames.length);
//    }
//
//    /**
//     * returns the next String that has not been given out.
//     *
//     * @return the next String, starting with the current, that has not been
//     * given out.
//     */
//    public String nextElement() {
//      int result = EnumInd;
//      EnumInd = search(EnumInd + 1);
//      if (result < IndicationNames.length) {
//        return Map[result][EnumHead];
//      }
//      return null;
//    }
//
//    /**
//     * finds the next String that has not been given out.
//     *
//     * @param start is the index of the place to start looking from.
//     *
//     * @return a the index of a String if one is found which is not at a
//     * lower index or IndicationNames.length, if none is found.
//     */
//    private int search(int start) {
//      int ind;
//      while (start < IndicationNames.length) {
//        if ( (Map[start] != null) && (Map[start][EnumHead] != null)) {
//          // Let's see if the String is at a lower indication.
//          for (ind = 0; ind < start; ++ind) {
//            if ( (Map[ind] != null) && (Map[ind][EnumHead] != null) &&
//                Map[ind][EnumHead].equals(Map[start][EnumHead])) {
//              break;
//            }
//          }
//          if (start == ind) {
//            break;
//          }
//        }
//        ++start;
//      }
//      return start;
//    }
//  }
  /**
   * creates the DefaultSignalAppearanceMap aspectAttributeMap Hashtable.  The only properties
   * needed by CATS are aspectname and show.
   * @param keys is the array of SignalMapAppearance keys associated with the SignalTemplate
   * @return the Hashtable, with one entry for each indication that CATS understands.
   */
  public java.util.Hashtable<String, java.util.Hashtable<String, String>> createAttributeMap(final String[] keys) {
	  java.util.Hashtable<String, java.util.Hashtable<String, String>> map = 
			  new java.util.Hashtable<String, java.util.Hashtable<String, String>>();
	  java.util.Hashtable<String, String> attributes;
	  for (int i = 0; i < IndicationNames.length; i++) {
		  attributes = new java.util.Hashtable<String, String>();
		  attributes.put(ASPECTNAME, keys[i]);
		  if (Map[i] != null) {
			  for (int head = 0; head < Map[i].length; head++) {
				  attributes.put(SHOW, Map[i][head]);
			  }
		  }
		  map.put(keys[i], attributes);
	  }
	  return map;
  }

  /**
   * creates the table of JMRI aspect names for each head in each appearance.
   * @param keys is the array of SignalAppearanceMap keys for the aspects
   * @return the table by which a user can get the JMRI SignalHead definition "color"
   * for each head of each appearance
   */
//  public java.util.Hashtable<String, int[]> createAspectMap(String keys[]) {
//	  java.util.Hashtable<String, int[]> table = new jmri.util.OrderedHashtable<String, int[]>();
//	  int[] aspects;
//	  for (int i = 0; i < keys.length; i++) {
//		  if (Map[i] != null) {
//			  aspects = new int[Map[i].length];
//			  for (int head = 0; head < Map[i].length; head++) {
//				  aspects[head] = PhysicalSignal.toJMRI(Map[i][head]);
//			  }	
//			  table.put(keys[i], aspects);
//		  }
//	  }
//	  return table;
//  }
  public java.util.HashMap<String, int[]> createAspectMap(String keys[]) {
	  java.util.HashMap<String, int[]> map = new HashMap<String, int[]>(keys.length);
	  int[] aspects;
	  for (int i = 0; i < keys.length; i++) {
		  if (Map[i] != null) {
			  aspects = new int[Map[i].length];
			  for (int head = 0; head < Map[i].length; head++) {
				  aspects[head] = PhysicalSignal.toJMRI(Map[i][head]);
			  }	
			  map.put(keys[i], aspects);
		  }
	  }
	  return map;
  }

  /**
   * generates an array of Strings, each the default key for a CATS
   * indication.  This method is encapsulated in the AspectMap class
   * to facilitate changes to the list of aspects.
   * @return an array of Strings, the XML field from IndicationNames
   */
  static public String[] getDefaultAppearanceKeys() {
	  String name[] = new String[IndicationNames.length];
	  for (int n = 0; n < IndicationNames.length; n++) {
		  name[n] = new String(IndicationNames[n][XML]);
	  }
	  return name;
  }
  
  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    return new String("A " + XML_TAG + " cannot contain a text field ("
                      + eleValue + ").");
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    return new String("A " + XML_TAG + " cannot contain an Element ("
                      + objName + ").");
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error checking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    return null;
  }

  /**
   * does a table lookup for a rule, based on the block being protected
   * and the following block.
   *
   * @param near is the speed through the block being protected
   * @param next is the speed through the next block.  It may be -1.
   *
   * @return an index into IndicationNames of the rule governing the
   * indication.  Rule 292 (STOP) is returned if either index is
   * out of bounds.
   */
  public static int getRule(int near, int next) {

    if ((near < 0) || (near > Track.STOP)) {
//      near = findRule("R292");
      near = Track.STOP;
    }
    if ((next < 0) || (next > Track.STOP)) {
      next = Track.STOP;
    }
    return SpeedTable[near][next];
  }

  /**
   * searches IndicationNames for the index of a Rule.
   *
   * @param rule is the rule being searched for
   *
   * @return the index in IndicationNames where it is found.  If
   * not found, -1 is returned.
   */
  public static int findRule(String rule) {
    for (int r = 0; r < IndicationNames.length; ++r) {
      if (IndicationNames[r][XML].equals(rule)) {
        return r;
      }
    }
    System.out.println("Could not find the rule named " + rule);
    return -1;
  }

  /**
   * registers an AspectMapFactory with the XMLReader and constructs the
   * lookup table from protected speed and next speed to Rule (indication).
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new AspectMapFactory());

    // This creates the speed lookup table
    SpeedTable = new int[Track.TrackSpeed.length + 3][Track.TrackSpeed.length +
        3];
    SpeedTable[Track.DEFAULT][Track.DEFAULT] =
        SpeedTable[Track.DEFAULT][Track.NORMAL] =
        SpeedTable[Track.NORMAL][Track.DEFAULT] =
        SpeedTable[Track.NORMAL][Track.NORMAL] = findRule("R281");
    SpeedTable[Track.DEFAULT][Track.LIMITED] =
        SpeedTable[Track.NORMAL][Track.LIMITED] = findRule("R281B");
    SpeedTable[Track.DEFAULT][Track.MEDIUM] =
        SpeedTable[Track.NORMAL][Track.MEDIUM] = findRule("R282");
    SpeedTable[Track.DEFAULT][Track.SLOW] =
        SpeedTable[Track.NORMAL][Track.SLOW] = findRule("R284");
    SpeedTable[Track.DEFAULT][Track.STOP] =
        SpeedTable[Track.NORMAL][Track.STOP] = findRule("R285");
    SpeedTable[Track.DEFAULT][Track.APPROACH] =
        SpeedTable[Track.NORMAL][Track.APPROACH] = findRule(ADV_PREFIX + NORMAL_SUFFIX);
    SpeedTable[Track.DEFAULT][Track.NONE] =
    		SpeedTable[Track.NORMAL][Track.NONE] = findRule(RESTRICTING_PREFIX + NORMAL_SUFFIX);

    SpeedTable[Track.LIMITED][Track.DEFAULT] =
        SpeedTable[Track.LIMITED][Track.NORMAL] = findRule("R281C");
    SpeedTable[Track.LIMITED][Track.LIMITED] = findRule("C412");
    SpeedTable[Track.LIMITED][Track.MEDIUM] = findRule("C413");
    SpeedTable[Track.LIMITED][Track.SLOW] = findRule("C414");
    SpeedTable[Track.LIMITED][Track.STOP] = findRule("R281D");
    SpeedTable[Track.LIMITED][Track.APPROACH] = findRule(ADV_PREFIX + LIMITED_SUFFIX);
    SpeedTable[Track.LIMITED][Track.NONE] = findRule(RESTRICTING_PREFIX + LIMITED_SUFFIX);

    SpeedTable[Track.MEDIUM][Track.DEFAULT] =
        SpeedTable[Track.MEDIUM][Track.NORMAL] = findRule("R283");
    SpeedTable[Track.MEDIUM][Track.LIMITED] = findRule("C417");
    SpeedTable[Track.MEDIUM][Track.MEDIUM] = findRule("R283A");
    SpeedTable[Track.MEDIUM][Track.SLOW] = findRule("R283B");
    SpeedTable[Track.MEDIUM][Track.STOP] = findRule("R286");
    SpeedTable[Track.MEDIUM][Track.APPROACH] = findRule(ADV_PREFIX + MEDIUM_SUFFIX);
    SpeedTable[Track.MEDIUM][Track.NONE] = findRule(RESTRICTING_PREFIX + MEDIUM_SUFFIX);

    SpeedTable[Track.SLOW][Track.DEFAULT] =
        SpeedTable[Track.SLOW][Track.NORMAL] = findRule("R287");
    SpeedTable[Track.SLOW][Track.LIMITED] = findRule("C422");
    SpeedTable[Track.SLOW][Track.MEDIUM] = findRule("C423");
    SpeedTable[Track.SLOW][Track.SLOW] = findRule("C424");
    SpeedTable[Track.SLOW][Track.STOP] = findRule("R288");
    SpeedTable[Track.SLOW][Track.APPROACH] = findRule(ADV_PREFIX + SLO_SUFFIX);
    SpeedTable[Track.SLOW][Track.NONE] = findRule(RESTRICTING_PREFIX + SLO_SUFFIX);

    SpeedTable[Track.STOP][Track.DEFAULT] =
    		SpeedTable[Track.STOP][Track.NORMAL] = findRule("R292");
    SpeedTable[Track.STOP][Track.LIMITED] = findRule("R292");
    SpeedTable[Track.STOP][Track.MEDIUM] = findRule("R292");
    SpeedTable[Track.STOP][Track.SLOW] = findRule("R292");
    SpeedTable[Track.STOP][Track.STOP] = findRule("R292");
    SpeedTable[Track.STOP][Track.APPROACH] = findRule("R292");
}
}

/**
 * is a Class known only to the AspectMap class for creating AspectMaps from
 * an XML document.
 */
class AspectMapFactory
    implements XMLEleFactory {

  /**
   * is the array of strings representing the head values for each indication.
   */
  private String[] HeadStr;

  /**
   * is the approach lighting flag.
   */
  private boolean ApproachFlag = false;

  /**
   * is the Advance indication flag.
   */
  private boolean AdvanceFlag = false;
  
  /**
   * is a flag indicating that CATS should only "hold" the signal mast (toggle between
   * STOP and whatever the SignalMast Logic wants).
   */
  private boolean SetHoldFlag;

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    HeadStr = new String[AspectMap.IndicationNames.length];
    ApproachFlag = false;
    AdvanceFlag = false;
    SetHoldFlag = false;
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;

    // locate the indication that the tag corresponds to.
    for (int ind = 0; ind < AspectMap.IndicationNames.length; ++ind) {
      if (AspectMap.IndicationNames[ind][AspectMap.XML].equals(tag)) {
        HeadStr[ind] = new String(value);
        return null;
      }
    }

    // this provides a conversion from older panels (where there was a single
    // Restricting indication) to the version where there is a restricting for
    // each speed
    if (AspectMap.RESTRICTING_TAG.equals(tag)) {
        for (int ind = 0; ind < AspectMap.IndicationNames.length; ++ind) {
            if (AspectMap.IndicationNames[ind][AspectMap.XML].startsWith(AspectMap.RESTRICTING_PREFIX)) {
                HeadStr[ind] = new String(value);
            }
        }
        return null;
    }
    
// looks for the approach flag
    if (AspectMap.XML_APPROACH.equals(tag)) {
      if (AspectMap.ISAPPROACH.equals(value)) {
        ApproachFlag = true;
      }
      return null;
    }

    // looks for the advanced flag
    if (AspectMap.XML_ADVANCE.equals(tag)) {
      if (AspectMap.ISAPPROACH.equals(value)) {
        AdvanceFlag = true;
      }
      return null;
    }

    // looks for the hold only flag
    if (AspectMap.HOLD_TAG.equals(tag)) {
        if (AspectMap.ISAPPROACH.equals(value)) {
            SetHoldFlag = true;
        }
        return null;
    }

    resultMsg = new String("A " + AspectMap.XML_TAG +
                           " XML Element cannot have a " + tag +
                           " attribute.");
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    return new AspectMap(HeadStr, ApproachFlag, AdvanceFlag, SetHoldFlag);
  }
}
