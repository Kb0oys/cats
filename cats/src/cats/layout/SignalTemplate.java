/*
 * Name: SignalTemplate.java
 *
 * What:
 *  This class contains the information about the physical characteristics
 *  of a signal - number of heads, possible states of each head, and so on.
 *  <p>
 *  Its role is to transform a CATS template into a JMRI SignalAppearanceMap.  The
 *  transformation is not complete because a CATS SignalTemplate has some
 *  attributes that a JMRI SignalAppearanceMap does not and vice versa.
 *  A CATS SignalTemplate has
 *  <ul>
 *  <li>a name (String)
 *  </li>
 *  <li>number of SignalHeads (int)
 *  </li>
 *  <li>lamp/semaphore (boolean) for the icon on the panel
 *  </li>
 *  <li>link to an AspectMap, which contains the aspects for each appearance
 *  </li>
 *  </ul>
 *  <p>
 *  On the other hand, a JMRI SignalAppearanceMap contains
 *  <ul>
 *  <li>aspectAttributeMap, a Hashtable of Hashtables, which are the mapping from appearance
 *   name to aspect, functionally similar to the CATS AspectMap.  The key on each entry is
 *   the name of the appearance.  The value of each entry is a Hashtable.  Each second level
 *   key is an attribute name (e.g. "aspectname", "show", "reference", etc.) and the second
 *   level value is the value of the attribute.  CATS must populate this structure.
 *  </li>
 *  <li>aspectImageMap, a Hashtable which is the mapping from appearance name to icon for the appearance,
 *  which CATS does not use because it draws the signal icons
 *  </li>
 *  <li>specificMaps, a Hashtable for specific mappings (HELD, DANGER, PERMISSIVE, DARK), of
 *  which CATS uses only DARK directly.  The key is the integer value of the appearance and the value
 *  is the name of the corresponding appearance (e.g. "Stop")
 *  </li>
 *  <li>aspectRelationshipMap, a Hashtable for aspect relationship mappings (ADVANCED).  The key
 *  is the name of an appearance on the advance SignalMast.  The value is an array of appearance
 *  names that could appear on this mast.  It appears to be used only by JMRI SignalLogic,
 *  which is not invoked by CATS.
 *  </li>
 *  <li>systemDefn, a link to a SignalSystem (which implies CATS must generate a SignalSystem
 *  for each unique SignalAppearanceMap)
 *  </li>
 *  <li>table, a Hashtable mapping an appearance name to an array of JMRI SignalHead
 *  aspects, one entry per signal head.  CATS must populate this structure.
 *  </li>
 *  <li>maps is a static Hashtable containing all of the SignalAppearanceMaps.  The key for
 *  each entry is constructed as "map:"+SignalSystemName+":"+AspectMapName.  CATS entries will
 *  have a CATS SignalSystemName and the SignalTemplate name as the AspectMapName.  Because
 *  this field has no protection attribute, it is invisible outside of the java/src/jmri/implementation
 *  folder; However, a static method exists in DefaultSignalAppearanceMap that allows a DefaultSignalAppearanceMap
 *  object (or derived object) to be added to maps.
 *  </li>
 *  </ul>
 */
package cats.layout;

import jmri.implementation.DefaultSignalAppearanceMap;
import cats.layout.items.CatsAppearanceMap;
import cats.layout.items.CatsSignalSystem;
import cats.layout.xml.*;

/**
 *  This class contains the information about the physical characteristics
 *  of a signal - number of heads, possible states of each head, and so on.
 *  <p>
 *  Its role is to transform a CATS template into a JMRI SignalAppearanceMap.  The
 *  transformation is not complete because a CATS SignalTemplate has some
 *  attributes that a JMRI SignalAppearanceMap does not and vice versa.
 *  A CATS SignalTemplate has
 *  <ul>
 *  <li>a name (String)
 *  </li>
 *  <li>number of SignalHeads (int)
 *  </li>
 *  <li>lamp/semaphore (boolean) for the icon on the panel
 *  </li>
 *  <li>link to an AspectMap, which contains the aspects for each appearance
 *  </li>
 *  </ul>
 *  <p>
 *  On the other hand, a JMRI SignalAppearanceMap contains
 *  <ul>
 *  <li>aspectAttributeMap, a Hashtable of Hashtables, which are the mapping from appearance
 *   name to aspect, functionally similar to the CATS AspectMap.  The key on each entry is
 *   the name of the appearance.  The value of each entry is a Hashtable.  Each second level
 *   key is an attribute name (e.g. "aspectname", "show", "reference", etc.) and the second
 *   level value is the value of the attribute.  CATS must populate this structure.
 *  </li>
 *  <li>aspectImageMap, a Hashtable which is the mapping from appearance name to icon for the appearance,
 *  which CATS does not use because it draws the signal icons
 *  </li>
 *  <li>specificMaps, a Hashtable for specific mappings (HELD, DANGER, PERMISSIVE, DARK), of
 *  which CATS uses only DARK directly.  The key is the integer value of the appearance and the value
 *  is the name of the corresponding appearance (e.g. "Stop")
 *  </li>
 *  <li>aspectRelationshipMap, a Hashtable for aspect relationship mappings (ADVANCED).  The key
 *  is the name of an appearance on the advance SignalMast.  The value is an array of appearance
 *  names that could appear on this mast.  It appears to be used only by JMRI SignalLogic,
 *  which is not invoked by CATS.
 *  </li>
 *  <li>systemDefn, a link to a SignalSystem (which implies CATS must generate a SignalSystem
 *  for each unique SignalAppearanceMap)
 *  </li>
 *  <li>table, a Hashtable mapping an appearance name to an array of JMRI SignalHead
 *  aspects, one entry per signal head.  CATS must populate this structure.
 *  </li>
 *  <li>maps is a static Hashtable containing all of the SignalAppearanceMaps.  The key for
 *  each entry is constructed as "map:"+SignalSystemName+":"+AspectMapName.  CATS entries will
 *  have a CATS SignalSystemName and the SignalTemplate name as the AspectMapName.  Because
 *  this field has no protection attribute, it is invisible outside of the java/src/jmri/implementation
 *  folder; However, a static method exists in DefaultSignalAppearanceMap that allows a DefaultSignalAppearanceMap
 *  object (or derived object) to be added to maps.
 *  </li>
 *  </ul>
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2014, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class SignalTemplate
    implements XMLEleObject {

  /**
   * is the tag for identifying a SignalTemplate in the XMl file.
   */
  static final String XML_TAG = "SIGNALTEMPLATE";

  /**
   * is the attribute tag for the number of heads in the XML file.
   */
  static final String TEMPLATEHEADS = "TEMPLATEHEADS";

  /**
   * is the kind of signal in the template.
   */
  static final String TEMPLATEKIND = "TEMPLATEKIND";

  /**
   * is the name of the template.
   */
 static final String TEMPLATENAME = "TEMPLATENAME";

  /**
   * is the value of TEMPLATEKIND for a lamp.
   */
  static final String LAMPKIND = "Lamp";

  /**
   * is the value of TEMPLATEKIND for a Semaphore.
   */
  static final String SEMKIND = "Semaphore";

  /**
   * the name of the SignalType.
   */
  private String TypeName;

  /**
   * the number of heads in the signal.
   */
  private int SignalHeads;

  /**
   * a true flag means the heads are lights.  A false flag means they are
   * semaphores.  This method is not really needed for
   * operating the layout.  However, it makes the screen presentation nicer
   * because a different icon can be used for semaphores as for lights.  Note
   * that all heads must be of the same type.
   */
  private boolean IsLights;

  /**
   * the table that maps indications to aspects.  The entries in the
   * map are entries in the state table.  This contributes heavily
   * to the JMRI SignalAppearanceMap.
   */
  private AspectMap AspectTbl;

  /**
   * the SignalAppearanceMap generated from the AspectMap
   */
  private DefaultSignalAppearanceMap AppearanceMap;
  
  /**
   * JMRI uses Strings as keys into a SignalAppearanceMap to retrieve
   * the aspect for the mast.  The keys are free form - user definable.
   * CATS uses indexes into the AspectMap for retrieving the aspect.
   * Any aspect can be requested, so in a worst case scenario, CATS
   * needs a key for each entry in the AspectMap.  To convert an AspectMap
   * to a SignalAppearanceMap, CATS needs to make the keys user definable.
   * This allows the user to link to JMRI defined SignalAppearanceMaps.
   * The following array translated CATS keys (by index) to the user defined
   * keys.
   */
  private final String AppearanceKey[];
  
  /**
   * the SignalTemplate constructor.
   *
   * @param heads is the number of heads (1 - 3)
   * @param lamp is true for lamp and false for semaphore.
   * @param name is the name (must not be blank)
   * @param aspects is the array of SignalAppearanceMap keys.  The factory
   * guarantees that they will not be null.
   */
  public SignalTemplate(int heads, boolean lamp, String name, String[] aspects) {
    SignalHeads = heads;
    IsLights = lamp;
    TypeName = new String(name);
    AppearanceKey = aspects;
    TemplateStore.SignalKeeper.add(this);
 }

  /**
   * returns the name of the template.
   *
   * @return the template's name.
   */
  public String getName() {
    return new String(TypeName);
  }

  /**
   * returns the number of signal heads.
   *
   * @return the number of heads.
   */
  public int getNumHeads() {
    return SignalHeads;
  }

  /**
   * returns the kind of Signal.
   *
   * @return true is the Signal is composed of lights or false if it is
   * composed of semaphore blades.
   */
  public boolean isLights() {
    return IsLights;
  }

  /**
   * replaces the AspectMap.
   *
   * @param map is the new AspectMap.
   *
   * @see AspectMap
   */
//  public void setAspectMap(AspectMap map) {
//    AspectTbl = map;
//  }

  /**
   * retrieves the AspectMap.
   *
   * @return the signal presentation for each Indication.
   *
   * @see AspectMap
   */
  public AspectMap getAspectMap() {
    return AspectTbl;
  }

  /**
   * retrieves the SignalAppearanceMap key for a CATS aspect.
   * @param aspect is an index into the AspectMap for an aspect
   * @return the associated key, if it is within bounds
   */
  public String getAspectKey(int aspect) {
	  return AppearanceKey[aspect];
  }
  
  /**
   * searches through the key translation table for the SignalAppearanceMap
   * key that corresponds to a CATS aspect name.
   * @param aspect is the name of a CATS AspectMap XML field.
   * @return the index of the CATS aspect, if found; else -1
   */
  public int getAspectKey(String aspect) {
//	  int index = AspectMap.findRule(aspect);
//	  if (index != -1) {
//		  return AppearanceKey[index];
//	  }
	  int index = 0;
	  while (index < AppearanceKey.length) {
		  if (AppearanceKey[index].equals(aspect)) {
			  return index;
		  }
		  ++index;
	  }
	  return -1;
  }
  
  /**
   * creates a list of SignalAppearanceMap keys, sorted by the 
   * order CATS uses them.
   * @return the SignalAppearanceMap keys for the SignalTemplate
   */
  public String[] getAppearanceKeys() {
	  return AppearanceKey;
  }

  /**
   * attempts to return the SignalAppearanceMap that is derived from the ApsectMap.
   * The SignalAppearanceMap is deferred instantiation.
   * If it has been generated, it is returned. If it has not been generated and
   * the AspectMap exists, it is generated and returned.  If it cannot be generated,
   * null is returned.
   * @return the corresponding SignalAppearanceMap or null
   */
  public DefaultSignalAppearanceMap getAppearanceMap() {
	  if (AppearanceMap == null) {
		  AppearanceMap = createAppearanceMap();
	  }
	  return AppearanceMap;
  }
  
  /**
   * creates a DefaultSignalAppearanceMap instance from the contents
   * of the SignalTemplate.  This code is highly influenced by how
   * JMRI creates SignalAppearanceMaps and SignalSystems.  The name of
   * the latter must be known for creating the former.  In following that
   * example, CATS creates a CatsSignalSystem (or finds one) before it
   * can create a SignalAppearanceMap.  This method is called only after
   * the SignalTemplate is complete.
   * @return a DefaultSignalAppearanceMap, which may be new or may already exist.
   */
  private DefaultSignalAppearanceMap createAppearanceMap() {
	  String sSystemName = CatsSignalSystem.getSignalSystem(AppearanceKey).getDisplayName();
	  String mapName = CatsAppearanceMap.buildSystemName(sSystemName, TypeName);
	  DefaultSignalAppearanceMap map = DefaultSignalAppearanceMap.findMap(mapName);
	  if (map == null) {
		  map = CatsAppearanceMap.createMap(sSystemName, TypeName);
		  if (AspectTbl != null) {
			  ((CatsAppearanceMap) map).completeMap(this);
		  }
	  }
	  return map;
  }
  
  /**
   * retrieves the name of the SignalSystem associated with the template
   * @return the name of the SignalSystem associated with the AppearanceMap.
   * It may be the empty String, if there is no AppearanceMap.
   */
  public String getSignalSystemName() {
	  CatsSignalSystem css = CatsSignalSystem.getSignalSystem(AppearanceKey);
	  if (css == null) {
		  css = CatsSignalSystem.createSignalSystem(AppearanceKey);
	  }
	  return css.getSystemName();
  }
  
  /*
   * is the method through which the object receives the text field.
   *
   * @param eleValue is the Text for the Element's value.
   *
   * @return if the value is acceptable, then null; otherwise, an error
   * string.
   */
  public String setValue(String eleValue) {
    return new String("A " + XML_TAG + " cannot contain a text field ("
                      + eleValue + ").");
  }

  /*
   * is the method through which the object receives embedded Objects.
   *
   * @param objName is the name of the embedded object
   * @param objValue is the value of the embedded object
   *
   * @return null if the Object is acceptible or an error String
   * if it is not.
   */
  public String setObject(String objName, Object objValue) {
    if (AspectMap.XML_TAG.equals(objName)) {
      AspectTbl = (AspectMap) objValue;
      return null;
    }
    return new String("A " + XML_TAG + " cannot contain an Element ("
                      + objName + ").");
  }

  /*
   * returns the XML Element tag for the XMLEleObject.
   *
   * @return the name by which XMLReader knows the XMLEleObject (the
   * Element tag).
   */
  public String getTag() {
    return new String(XML_TAG);
  }

  /*
   * tells the XMLEleObject that no more setValue or setObject calls will
   * be made; thus, it can do any error checking that it needs.
   *
   * @return null, if it has received everything it needs or an error
   * string if something isn't correct.
   */
  public String doneXML() {
    return null;
  }

  /**
   * registers a SignalTemplateFactory with the XMLReader.
   */
  static public void init() {
    XMLReader.registerFactory(XML_TAG, new SignalTemplateFactory());
    AspectMap.init();
  }
}

/**
 * is a Class known only to the SignalTemplate class for creating
 * SignalTemplates from an XML document.
 */
class SignalTemplateFactory
    implements XMLEleFactory {

  private String tempName;
  private boolean lamp;
  private int heads;
  
  // the keys start out with the CATS defaults
  private String aspects[] = AspectMap.getDefaultAppearanceKeys();

  /*
   * tells the factory that an XMLEleObject is to be created.  Thus,
   * its contents can be set from the information in an XML Element
   * description.
   */
  public void newElement() {
    tempName = null;
    lamp = true;
    heads = 1;
    aspects = new String[AspectMap.IndicationNames.length];
    for (int i = 0; i < aspects.length; i++) {
    	aspects[i] = AspectMap.IndicationNames[i][AspectMap.XML];
    }
  }

  /*
   * gives the factory an initialization value for the created XMLEleObject.
   *
   * @param tag is the name of the attribute.
   * @param value is it value.
   *
   * @return null if the tag:value are accepted; otherwise, an error
   * string.
   */
  public String addAttribute(String tag, String value) {
    String resultMsg = null;
    int aspect;

    if (SignalTemplate.TEMPLATEKIND.equals(tag)) {
      if (SignalTemplate.LAMPKIND.equals(value)) {
        lamp = true;
      }
      else if (SignalTemplate.SEMKIND.equals(value)) {
        lamp = false;
      }
      else {
        resultMsg = new String("A " + value + " is not a " +
                               SignalTemplate.XML_TAG + "XML " +
                               SignalTemplate.TEMPLATEKIND + " attribute.");
      }
    }
    else if (SignalTemplate.TEMPLATEHEADS.equals(tag))
    {
      try {
        heads = Integer.parseInt(value);
        if ((heads < 1) || (heads > 3)) {
          heads = 1;
          resultMsg = new String(value + ": Illegal value for " +
                                 SignalTemplate.XML_TAG + "XML " +
                                 SignalTemplate.TEMPLATEKIND + " attribute.");
        }
      }
      catch (NumberFormatException nfe) {
        resultMsg = new String(value + ": Illegal " +
                               SignalTemplate.XML_TAG + "XML " +
                               SignalTemplate.TEMPLATEKIND + " attribute.");
      }
    }
    else if (SignalTemplate.TEMPLATENAME.equals(tag)) {
      if ((value == null) || (value.trim().length() == 0)) {
        resultMsg = new String("A " + SignalTemplate.XML_TAG +
                               " XML " + SignalTemplate.TEMPLATENAME +
                               " attribute can not be blank.");
      }
      else {
        tempName = new String(value);
      }
    }
    else {
    	for (aspect = 0; aspect < AspectMap.IndicationNames.length; ++aspect) {
    		if (AspectMap.IndicationNames[aspect][AspectMap.XML].equals(tag)) {
    			aspects[aspect] = new String(value);
    			break;
    		}
    	}
    	if (aspect == AspectMap.IndicationNames.length)  {
    		resultMsg = new String("A " + SignalTemplate.XML_TAG +
    				" XML Element cannot have a " + tag +
    				" attribute.");
    	}
    }
    return resultMsg;
  }

  /*
   * tells the factory that the attributes have been seen; therefore,
   * return the XMLEleObject created.
   *
   * @return the newly created XMLEleObject or null (if there was a problem
   * in creating it).
   */
  public XMLEleObject getObject() {
    if (tempName == null) {
      System.out.println("Missing " + SignalTemplate.XML_TAG + " " +
                         SignalTemplate.TEMPLATENAME + " attribute.");
      return null;
    }
    return new SignalTemplate(heads, lamp, tempName, aspects);
  }
}
