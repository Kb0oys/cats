/* Name: RefreshScheduler.java
 *
 * What:
 * This class is a timeout event. It's purpose is to wait for a short
 * period of time, then read back all JMRI devices, updating their CATS
 * equivalents.  The reason for the delay is that performing the
 * read back immediately after the panel is initialized sometimes locks
 * up so tightly that the CATS process must be manually killed.  Running
 * the same code from a menu pull down does not seem to have similar
 * issues.  Since the lockups appear random, it is possible that the
 * thread the read backs are running on could be a factor.  This class
 * both delays the read back activity and moved it off the startup thread.
 */
package cats.layout;

import cats.layout.items.IOSpec;
import cats.rr_events.TimeoutEvent;

/**
  * This class is a timeout event. It's purpose is to wait for a short
 * period of time, then read back all JMRI devices, updating their CATS
 * equivalents.  The reason for the delay is that performing the
 * read back immediately after the panel is initialized sometimes locks
 * up so tightly that the CATS process must be manually killed.  Running
 * the same code from a menu pull down does not seem to have similar
 * issues.  Since the lockups appear random, it is possible that the
 * thread the read backs are running on could be a factor.  This class
 * both delays the read back activity and moved it off the startup thread.
 * <p>Title: CATS - Computer Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class RefreshScheduler implements TimeoutObserver {

	public RefreshScheduler() {
		MasterClock.MyClock.setTimeout(new TimeoutEvent(this, 1));
	}
	
	@Override
	public void acceptTimeout() {
		IOSpec.refreshScreen();
	}
}
/* @(#)RefreshScheduler.java */
