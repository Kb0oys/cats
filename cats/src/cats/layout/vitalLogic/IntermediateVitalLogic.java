/* Name: IntermediateVitalLogic.java
 *
 * What:
 * This file contains the definition for the VitalLogic sub-class associated with an
 * Intermediate signal.  An intermediate signal has a signal on the  layout, but not
 * the dispatcher panel.  Thus, it can receive only a limited set of commands from
 * the dispatcher.
 */
package cats.layout.vitalLogic;

import java.util.EnumSet;

import cats.apps.Crandic;
import cats.common.DebugBits;
import cats.gui.CounterFactory;
import cats.gui.Sequence;
import cats.layout.AspectMap;
import cats.layout.Discipline;
import cats.layout.MasterClock;
import cats.layout.TimeoutObserver;
import cats.layout.codeLine.packetFactory.PacketFactory;
import cats.layout.items.PhysicalSignal;
import cats.layout.items.Track;
import cats.rr_events.TimeoutEvent;
import cats.trackCircuit.TrackCircuit;


/**
 * This file contains the definition for the VitalLogic sub-class associated with an
 * Intermediate signal.  An intermediate signal is an automatic, permissive signal.
 * it has a signal on the layout, but not
 * the dispatcher panel.  Thus, it cannot receive any commands directly from the dispatcher.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014, 2015, 2016, 2018, 2019, 2021, 2022, 2924</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class IntermediateVitalLogic extends BlkVitalLogic {
	/**
	 * are the aspects that the VitalLogic sends to the SignalVitalLogic
	 */
	public enum FORCED_ASPECT {
		NONE,       	// no conditions (locks) exist in the VitalLogic that restrict an aspect
		STOP,       	// a condition exists for displaying a STOP, but on this signal only
//		TUMBLEDOWN,		// a condition exists for displaying a STOP, that should propagate to the approach signal
//		OPPOSINGTRAFFIC,// a condition exists for displaying a STOP, that should propagate to the next opposing signal
		RESTRICTING,	// a condition exists for displaying a flashing red aspect - restricting
		STOP_AND_GO		// the protected track is under local control
	};

	/**
	 * are the signal types
	 */
	public enum SIGNAL_TYPE {
//		VIRTUAL,	// the artificial signal on a block whose neighbor is real
//		AUTOMATIC,	// an intermediate signal
		ISOLATED,	// an absolute signal whose neighbor is not a signal
		PAIRED		// an absolute signal whose neighbor is an absolute signal
	};
	
	/**
	 * the embedded signal
	 */
	protected final SignalMastVitalLogic MySignalHead;
	
	/**
	 * the TrackCircuit that this one feeds
	 */
	protected TrackCircuit ApproachCircuit;
	
	/**
	 * the traffic stick - traffic stick relay - for determining direction of travel
	 */
	private TSR_StateMachine Tsr;

	/**
	 * the set of locks that will force a STOP indication, based on discipline
	 */
	protected EnumSet<LogicLocks> StopLocks;	

	/**
	 * the set of locks that have triggered TIMELOCK - the reason time is running.
	 * This is needed so that the TIMELOCK can be removed (before running out),
	 * if all the reasons are removed
	 */
	protected EnumSet<LogicLocks> TimeLockCause;	

	/**
	 * the last indication reported by the signal
	 */
	protected AspectMap.INDICATION_TYPE LastIndication = AspectMap.INDICATION_TYPE.HALT;
	
	/**
	 * the next VitalLogic in Approach with a signal
	 */
	protected SignalVitalLogic NextSignal;
	
	/**
	 * the route locks from the SignalVitalLogic in advance
	 */
	protected EnumSet<LogicLocks> AdvanceRouteLocks = EnumSet.noneOf(LogicLocks.class);
	
//	/**
//	 * the merger of the MergedGlobalLocks and AdvanceRouteLocks
//	 */
//	protected EnumSet<LogicLocks> MergedRouteLocks = EnumSet.noneOf(LogicLocks.class);
	
	/**
	 * the aspect to use when protecting local locks
	 */
//	private FORCED_ASPECT DefaultStop = FORCED_ASPECT.TUMBLEDOWN;
	private FORCED_ASPECT DefaultStop = FORCED_ASPECT.STOP;

	
	/**
	 * the ctor
	 * @param transmitter is the CodeLine message constructor
	 * @param id is a String used for identifying this VitalLogic in debugging
	 * @param sig is the physical signal controlled by the vital logic
	 */
	
	public IntermediateVitalLogic(final PacketFactory transmitter, final String id, final PhysicalSignal sig) {
		super(transmitter, id);
		MySignalHead = new FieldSignal(this, sig);
	}

//	/**
//	 * is invoked to tell the VitalLogic where its signal is.
//	 * @param newHead is the signal
//	 */
//	public void setSignalHead(final SignalVitalLogic newHead) {
//		MySignalHead = newHead;
//	}
	
	@Override
	public void setSpeed(final int speed) {
		super.setSpeed(speed);
		MySignalHead.setSpeed(speed);
	}

	@Override
	public void configureLockProcessing() {
		NeighborBlk = (BlkVitalLogic) NeighborLogic;
		if ((NeighborBlk != null) && (NeighborBlk instanceof CPVitalLogic) && MyDiscipline.equals(Discipline.APB)) {
//			MySignalHead.setApproachTranslation(SIGNAL_TYPE.PAIRED);
			DefaultStop = FORCED_ASPECT.STOP;
		}
//		// DETECTION is discipline specific - see below
//		LockProcessor.put(LogicLocks.DETECTIONLOCK,
//				new LocalCodeLockDecorator(new SignalMergedRequestLockProcessor(LogicLocks.DETECTIONLOCK)));
//		LockProcessor.put(LogicLocks.CONFLICTINGSIGNALLOCK, new SignalMergedRequestLockProcessor(LogicLocks.CONFLICTINGSIGNALLOCK));
//		// HOLD is discipline specific - see below
//		LockProcessor.put(LogicLocks.SWITCHUNLOCK, new SignalMergedRequestLockProcessor(LogicLocks.SWITCHUNLOCK));
//		LockProcessor.put(LogicLocks.APPROACHLOCK, new DefaultRequestLockProcessor(LogicLocks.APPROACHLOCK));
//		LockProcessor.put(LogicLocks.TIMELOCK, new TimeLockProcessor());
//		// ROUTE depends upon the type of the neighbor - see below
//		LockProcessor.put(LogicLocks.ROUTELOCK, new SignalAdvanceRequestLockProcessor(LogicLocks.ROUTELOCK));
////		LockProcessor.put(LogicLocks.FLEETLOCK, new AdvanceRequestLockProcessor(LogicLocks.FLEETLOCK));
//		LockProcessor.put(LogicLocks.FLEETLOCK, new DefaultRequestLockProcessor(LogicLocks.FLEETLOCK));
//		// PROTECTION depends upon the neighbor - see below
//		LockProcessor.put(LogicLocks.PROTECTIONLOCK, new SignalAdvanceRequestLockProcessor(LogicLocks.PROTECTIONLOCK));
//		LockProcessor.put(LogicLocks.MAINTENANCELOCK, new SignalMergedRequestLockProcessor(LogicLocks.MAINTENANCELOCK));
//		// CALLON is not used by an Intermediate edge
//		// FLEET
//		LockProcessor.put(LogicLocks.EXTERNALLOCK,
//				new LocalCodeLockDecorator(new SignalMergedRequestLockProcessor(LogicLocks.EXTERNALLOCK)));
//
//		if (NeighborBlk != null) {
//			if (NeighborBlk instanceof CPVitalLogic) {
//				NeighborCP = true;
//				LockProcessor.put(LogicLocks.DETECTIONLOCK, new NeighborApproachLockDecorator(LockProcessor.get(LogicLocks.DETECTIONLOCK)));
//				LockProcessor.put(LogicLocks.ROUTELOCK, new RouteLockConverter());
//			}
//			else if (NeighborBlk instanceof IntermediateVitalLogic) {
//				NeighborIntermediate = true;
//				LockProcessor.put(LogicLocks.DETECTIONLOCK, new NeighborApproachLockDecorator(LockProcessor.get(LogicLocks.DETECTIONLOCK)));
//				if (NeighborBlk.MyDiscipline.equals(Discipline.CTC) || NeighborBlk.MyDiscipline.equals(Discipline.DTC)) {
//					LockProcessor.put(LogicLocks.ROUTELOCK, new HoldNeighborDecorator(LockProcessor.get(LogicLocks.ROUTELOCK)));									
//				}
//				LockProcessor.put(LogicLocks.FLEETLOCK, new AdvanceLockPropagationDecorator(LockProcessor.get(LogicLocks.FLEETLOCK)));
//			}
//			else {
//				LockProcessor.put(LogicLocks.FLEETLOCK, new AdvanceLockPropagationDecorator(LockProcessor.get(LogicLocks.FLEETLOCK)));
//			}
//			if (MyDiscipline.equals(Discipline.CTC) || MyDiscipline.equals(Discipline.DTC) || MyDiscipline.equals(Discipline.APB)) {
////				LockProcessor.put(LogicLocks.DETECTIONLOCK, new RouteDecorator(LockProcessor.get(LogicLocks.DETECTIONLOCK)));
////				LockProcessor.put(LogicLocks.CONFLICTINGSIGNALLOCK, new RouteDecorator(LockProcessor.get(LogicLocks.CONFLICTINGSIGNALLOCK)));
////				LockProcessor.put(LogicLocks.SWITCHUNLOCK, new RouteDecorator(LockProcessor.get(LogicLocks.SWITCHUNLOCK)));
//				LockProcessor.put(LogicLocks.ROUTELOCK, new RouteLockDecorator(LockProcessor.get(LogicLocks.ROUTELOCK)));
////				LockProcessor.put(LogicLocks.ROUTELOCK,
////						new RouteDecorator(new RouteLockDecorator(LockProcessor.get(LogicLocks.ROUTELOCK))));
////				LockProcessor.put(LogicLocks.PROTECTIONLOCK, new RouteDecorator(LockProcessor.get(LogicLocks.PROTECTIONLOCK)));
////				LockProcessor.put(LogicLocks.MAINTENANCELOCK, new RouteDecorator(LockProcessor.get(LogicLocks.MAINTENANCELOCK)));
////				LockProcessor.put(LogicLocks.FLEETLOCK, new RouteDecorator(LockProcessor.get(LogicLocks.FLEETLOCK)));
//				LockProcessor.put(LogicLocks.DETECTIONLOCK, new CTCDetectionDecorator(LockProcessor.get(LogicLocks.DETECTIONLOCK)));
//				LockProcessor.put(LogicLocks.ROUTELOCK, new AdvanceLockPropagationDecorator(LockProcessor.get(LogicLocks.ROUTELOCK)));
//				LockProcessor.put(LogicLocks.PROTECTIONLOCK, new LockPropagationDecorator(LockProcessor.get(LogicLocks.PROTECTIONLOCK)));
//				if (MyDiscipline.equals(Discipline.APB)) {
//					Tsr = new TSR_StateMachine();
//					LockProcessor.put(LogicLocks.DETECTIONLOCK, new APBDetectionDecorator(LockProcessor.get(LogicLocks.DETECTIONLOCK)));
//					LockProcessor.put(LogicLocks.APPROACHLOCK, new APBApproachDecorator(LockProcessor.get(LogicLocks.APPROACHLOCK)));
//				}
//				else {
//					LockProcessor.put(LogicLocks.HOLDLOCK, new SignalLocalRequestLockProcessor(LogicLocks.HOLDLOCK));
//					setLocalLock(LogicLocks.HOLDLOCK);					
//				}
//			}
//		}
		LockRequestProcessor processor;
		// DETECTIONLOCK
		processor = new SignalDecorator(
						new MergedRequestLockProcessor(LogicLocks.DETECTIONLOCK));
		if (NeighborBlk != null) { 
			if (MyDiscipline.equals(Discipline.APB)) {
				processor = new APBDetectionDecorator(processor);
			}
			processor = new MergedLockFilterDecorator(processor);
//			if ((NeighborBlk instanceof IntermediateVitalLogic) || (NeighborBlk instanceof CPVitalLogic)) {
			if (NeighborBlk instanceof IntermediateVitalLogic) {
				processor = new NeighborApproachLockDecorator(processor);	
			}
		}
		else {
			processor = new MergedLockFilterDecorator(processor);
		}
		LockProcessor.put(LogicLocks.DETECTIONLOCK,
				new LockFilterDecorator(
						new LocalCodeLockDecorator(
								new CommonDetectionDecorator(processor))));

		// CONFLICTINGSIGNALLOCK
//		if (NeighborBlk != null) {
			LockProcessor.put(LogicLocks.CONFLICTINGSIGNALLOCK, 
					new MyLocksFilterDecorator(
							new MergedLockFilterDecorator(
									new SignalDecorator(
											new MergedRequestLockProcessor(LogicLocks.CONFLICTINGSIGNALLOCK)))));
//		}

			// HOLDLOCK
			if (MyDiscipline.equals(Discipline.CTC) || MyDiscipline.equals(Discipline.DTC)) {
				LockProcessor.put(LogicLocks.HOLDLOCK,
						new IgnoreAdvanceLocksDecorator(
								new MergedLockFilterDecorator(
										new SignalDecorator(
												new LockDumpDecorator(
												new MergedRequestLockProcessor(LogicLocks.HOLDLOCK), DebugBits.DEBUGFLAG)))));
				setLocalLock(LogicLocks.HOLDLOCK);
			}
		
		// SWITCHUNLOCK
		if (NeighborBlk != null) {
			LockProcessor.put(LogicLocks.SWITCHUNLOCK, 
					new MyLocksFilterDecorator(
							new MergedLockFilterDecorator(
									new SignalDecorator(
											new MergedRequestLockProcessor(LogicLocks.SWITCHUNLOCK)))));
		}
		// APPROACHLOCK
		processor = new MergedRequestLockProcessor(LogicLocks.APPROACHLOCK);
		if (MyDiscipline.equals(Discipline.APB)) {
			processor = new APBApproachDecorator(processor);
		}
		LockProcessor.put(LogicLocks.APPROACHLOCK, 
				new IgnoreAdvanceLocksDecorator(
						new MergedLockFilterDecorator(
								new SignalDecorator(processor))));
		// TIMELOCK
//		LockProcessor.put(LogicLocks.TIMELOCK, new TimeLockProcessor());
		LockProcessor.put(LogicLocks.TIMELOCK, 
				new IgnoreAdvanceLocksDecorator(
						new TimeLockDecorator(
								new DefaultRequestLockProcessor(LogicLocks.TIMELOCK))));

		// TRAFFICLOCK - never should be set/cleared
		// ROUTELOCK
		if (NeighborBlk != null) {
//			if (NeighborBlk instanceof CPVitalLogic) {
//				processor = new RouteLockConverter();
//			}
//			else {
//				processor = new MergedRequestLockProcessor(LogicLocks.ROUTELOCK);
//			}
			processor = new MergedRequestLockProcessor(LogicLocks.ROUTELOCK);
			if (NeighborBlk instanceof CPVitalLogic) {
				processor = new LockConverter(processor, LogicLocks.PROTECTIONLOCK);
			}
			else {
				processor = new LockPropagationDecorator(processor);
			}
			if ((NeighborBlk instanceof IntermediateVitalLogic) && !(NeighborBlk instanceof CPVitalLogic) &&
					(NeighborBlk.MyDiscipline.equals(Discipline.CTC) || NeighborBlk.MyDiscipline.equals(Discipline.DTC))){
				processor = new NeighborHoldDecorator(processor);
			}
		}
		else {
			processor = new MergedRequestLockProcessor(LogicLocks.ROUTELOCK);
		}
		LockProcessor.put(LogicLocks.ROUTELOCK, new MergedLockFilterDecorator(
				new SignalDecorator(processor)));

		// PROTECTIONLOCK
		processor = new SignalDecorator(
				new MergedRequestLockProcessor(LogicLocks.PROTECTIONLOCK));
		if ((NeighborBlk != null) && !MyDiscipline.equals(Discipline.ABS) && !MyDiscipline.equals(Discipline.UNDEFINED)) {
			new LockPropagationDecorator(processor);
		}
		LockProcessor.put(LogicLocks.PROTECTIONLOCK,
				new MergedLockFilterDecorator(processor));

		// MAINTENANCE
		LockProcessor.put(LogicLocks.MAINTENANCELOCK,
				new MergedLockFilterDecorator(
						new SignalDecorator(
								new MergedRequestLockProcessor(LogicLocks.MAINTENANCELOCK))));

		// CALLONLOCK - never should be set/cleared
		// FLEETLOCK - never should be set/cleared
//		if ((NeighborBlk != null) && !(NeighborBlk instanceof CPVitalLogic)
//				&& !MyDiscipline.equals(Discipline.ABS) && !MyDiscipline.equals(Discipline.UNDEFINED)) {
//			installAdvancePropagationProcessor(LogicLocks.FLEETLOCK);
//		}

		// EXTERNALLOCK
		LockProcessor.put(LogicLocks.EXTERNALLOCK,
				new LockFilterDecorator(
						new LocalCodeLockDecorator(
								new MergedLockFilterDecorator(
										new SignalDecorator(
												new MergedRequestLockProcessor(LogicLocks.EXTERNALLOCK))))));
	}

	@Override
	public void setApproachLogic(final BasicVitalLogic approach) {
		ApproachLogic = approach;
	}

	@Override
	public void tailorDiscipline(final Discipline behavior) {	
		super.tailorDiscipline(behavior);
		StopLocks = (MyDiscipline.equals(Discipline.CTC) || MyDiscipline.equals(Discipline.DTC)) ?
			LogicLocks.CTCDTC_LOCKS : LogicLocks.ABSAPB_LOCKS;
		if (MyDiscipline.equals(Discipline.APB) && !(this instanceof CPVitalLogic)) {
			Tsr = new TSR_StateMachine();
		}
	}
	
	@Override
	public void mergeSpeeds(final int advSpeed) {
		int speed = MergedSpeed;
		AdvanceSpeed = advSpeed;
		MergedSpeed = Track.getSlower(Speed, AdvanceSpeed);
		if (speed != MergedSpeed) {
			MySignalHead.setSpeed(MergedSpeed);
		}
	}

	/**
	 * starts up the search for the TrackCircuit in approach of this signal.  The probe transits
	 * VitalLogics in order to detect points, frogs, and crossings.  The subtle part is that
	 * the probe is the stimulus for an unpaired BlkVitalLogic to insert a dummy TrackCircuit.
	 */
	public void connectSignal() {
		if (ApproachLogic != null) {
			MySignalHead.getTrackCircuit().setApproachTrackCircuit(ApproachLogic.trackCircuitProbe());
//			NextSignal = ApproachLogic.signalVitalLogicProbe();
		}
	}
	
	@Override
	protected TrackCircuit trackCircuitProbe() {
		return MySignalHead.getTrackCircuit();
	}
	
//	@Override
//	protected SignalVitalLogic signalVitalLogicProbe() {
//		return this;
//	}

	
//	@Override
//	@Deprecated
//	protected boolean mergeGlobalLocks(final LogicLocks lock) {
//		if (super.mergeGlobalLocks(lock)) {
//			MySignalHead.forceAspect(determineAspect());
//			return true;
//		}
//		return false;
//	}
	
	
//	/**
//	 * This method computes the global field MergedRouteLocks.  MergedRouteLocks
//	 * is the merger of the local Route Lock and the Route Lock from the advance VitalLogic (the
//	 * feeder).  It is used in determining what is fed to the approach VitalLogic
//	 * and signal.  If the merged
//	 * value changes, the MergedRouteLocks is adjusted and true is returned.
//	 * Otherwise, false is returned.  It does not forward any lock changes.  That task
//	 * is done by other methods.
//	 * <p>
//	 * This method looks at only one Lock!
//	 * @param l is the lock being merged
//	 * @return true if the merged route state changed and false if not
//	 */
//	protected boolean mergeRouteLocks(final LogicLocks lock) {
//		EnumSet<LogicLocks> oldState = MergedRouteLocks.clone();
//		boolean oldLock = MergedRouteLocks.contains(lock);
//		boolean newLock = AdvanceRouteLocks.contains(lock)	|| MergedGlobalLocks.contains(lock);
//		if (oldLock == newLock) {
//			return false;
//		}
//		if (newLock) {
//			MergedRouteLocks.add(lock);
//		}
//		else {
//			MergedRouteLocks.remove(lock);
//		}
//		/******************** DEBUG code *****************************/
//		if (Crandic.Details) {
//			System.out.println(Identity + ": Route Locks were " + oldState.toString() + " now " + MergedRouteLocks.toString());			
//		}
//	    /*************************************************************/
//		return true;
//	}
	
//	@Override
//	public void setRouteLock(final LogicLocks l) {
//		// this really should call a method in the Processor
//		AdvanceRouteLocks.add(l);
//		if (mergeRouteLocks(l) && !(NextSignal == null)) {
//			NextSignal.setRouteLock(l);
//		}
//	}
//	
//	@Override
//	public void clearRouteLock(final LogicLocks l) {
//		// this really should call a method in the Processor
//		AdvanceRouteLocks.remove(l);
//		if (mergeRouteLocks(l) && !(NextSignal == null)) {
//			NextSignal.clearRouteLock(l);
//		}
//	}
	
	@Override
	protected void setAdvanceLock(final LogicLocks l) {
		LockRequestProcessor processor = LockProcessor.get(l);
		if (processor != null) {
			processor.advanceLockSet();
		}
		else {  // this is the difference with BlkEdge - locks do not propagate
			AdvanceLocks.add(l);
		}		
	}
	
	@Override
	protected void clearAdvanceLock(final LogicLocks l) {
		LockRequestProcessor processor = LockProcessor.get(l);
		if (processor != null) {
			processor.advanceLockClear();
		}
		else {  // this is the difference with BlkEdge - locks do not propagate
			AdvanceLocks.remove(l);
		}		
	}

	/**
	 * this method analyzes locks to determine what affect the set
	 * locks have upon the embedded signal's indication.  Without input from the
	 * VitalLogic, there is no reason for a signal to display anything less than
	 * Clear or Approach.  This method looks for a reason why the signal
	 * should show something more restrictive.  The common restriction is to
	 * show STOP; however, the dispatcher machine could upgrade that to RESTRICTING.
	 * @return  NONE if there are no restricting locks or STOP if there are any.
	 */
	protected FORCED_ASPECT determineAspect() {
		if (MergedGlobalLocks.contains(LogicLocks.MAINTENANCELOCK)) {
			return FORCED_ASPECT.STOP_AND_GO;
		}
//		if (!LogicLocks.intersection(MergedGlobalLocks, StopLocks).isEmpty()) {
//			return (Tsr == null) ? FORCED_ASPECT.STOP :
//					((Tsr.isStickSet()) ? FORCED_ASPECT.STOP : FORCED_ASPECT.TUMBLEDOWN);
//		}
//		else if ((Tsr != null) && Tsr.isOtherStickSet()) {  // this forces a STOP behind a train 
//			return FORCED_ASPECT.TUMBLEDOWN;
//		}
		if (!LogicLocks.intersection(MergedGlobalLocks, StopLocks).isEmpty()) {
			if ((Tsr == null) || Tsr.isStickSet()) {
				return FORCED_ASPECT.STOP;
			}
			else {
				return DefaultStop;
			}
		}
		if ((Tsr != null) && Tsr.isOtherStickSet()) {  // this forces a STOP behind a train 
			return FORCED_ASPECT.STOP;
		}
		return FORCED_ASPECT.NONE;
	}

	/**
	 * receives a change in the signal's indication.  If changing from non-STOP
	 * to STOP, it can set a TIMELOCK.  A CP will update the GUI.
	 * @param newIndication is the index of the rule for the new value of the signal's indication
	 * */
	public void updateIndication(final int newIndication) {
		AspectMap.INDICATION_TYPE newRule = AspectMap.IndicationType[newIndication];
		if (LastIndication != newRule) {
			// the first check is for starting up running time.  It is started if
			// time is not running and
			// the signal is knocked down (old indication was not HALT and new indication is HALT) and
			// detection is not set (detection will knock down a signal) and
			// the approach block is occupied
//			if (!LockProcessor.get(LogicLocks.DETECTIONLOCK).isLockSet() &&
			if (!MergedGlobalLocks.contains(LogicLocks.DETECTIONLOCK) &&
					newRule.equals(AspectMap.INDICATION_TYPE.HALT) && !LastIndication.equals(AspectMap.INDICATION_TYPE.HALT) &&
					MyLocks.contains(LogicLocks.APPROACHLOCK)) {
				setLocalLock(LogicLocks.TIMELOCK);
			}
			else if ((!newRule.equals(AspectMap.INDICATION_TYPE.HALT) && LastIndication.equals(AspectMap.INDICATION_TYPE.HALT)) ||
					!MyLocks.contains(LogicLocks.APPROACHLOCK)) {
				clearLocalLock(LogicLocks.TIMELOCK);
			}
			LastIndication = newRule;
		}
	}
	
	/**
	 * is a query to determine if if a STOP indication should be promoted to TUMBLEDOWN
	 * @return true if the VitalLogic has an associated TSR and if that TSR is not in an Entry state
	 */
	public boolean isTumbleDown() {
		return (Tsr != null) && !Tsr.isStickSet();
	}
	
	@Override
	public void primeApproach() {
		super.primeApproach();
		if (MySignalHead != null) {
			MySignalHead.setApproachLighting(this);
			MySignalHead.forceAspect(determineAspect());
		}
	}
	
//	/**********************************************************************************
//	 * Type 1 - local.  The only stimulus comes from the track.  Nothing
//	 * comes from an edge in advance.  Only the track is monitored.  StateChanged
//	 * mirrors only a change in MyLocks.
//	 *********************************************************************************/
//	protected class SignalLocalRequestLockProcessor extends DefaultRequestLockProcessor {
//
//		public SignalLocalRequestLockProcessor(LogicLocks lock) {
//			super(lock);
//		}
//
//		@Override
//		public void requestLockSet() {
//			super.requestLockSet();
////			if (StateChanged) {
//				MySignalHead.forceAspect(determineAspect());
////			}
//		}
//
//		@Override
//		public void requestLockClear() {
//			super.requestLockClear();
////			if (StateChanged) {
//				MySignalHead.forceAspect(determineAspect());
////			}
//		}
//	}
//	
	
//	/**********************************************************************************
//	 * Type 2 - global.  The only stimulus comes from an edge in advance.  Nothing
//	 * comes from the track.  Only the advance Vital Logic is monitored.  StateChanged
//	 * mirrors only a change in AdvanceLocks.
//	 *********************************************************************************/
////	protected class SignalAdvanceRequestLockProcessor extends AdvanceRequestLockProcessor {
//	protected class SignalAdvanceRequestLockProcessor extends DefaultRequestLockProcessor {
//
//		public SignalAdvanceRequestLockProcessor(LogicLocks lock) {
//			super(lock);
//		}
//
//		@Override
//		public void advanceLockSet() {
//			super.advanceLockSet();
////			if (StateChanged) {
//				MySignalHead.forceAspect(determineAspect());
////			}
//		}
//
//		@Override
//		public void advanceLockClear() {
//			super.advanceLockClear();
////			if (StateChanged) {
//				MySignalHead.forceAspect(determineAspect());
////			}
//		}		
//	}
//	
//	/**********************************************************************************
//	 * Type 3 - merged.  The stimulus can come from either the track or the edge
//	 * in advance.  The value depends upon both.  StateChange changes when the first
//	 * is set or the last cleared.
//	 *********************************************************************************/
//	protected class SignalMergedRequestLockProcessor extends MergedRequestLockProcessor {
//
//		public SignalMergedRequestLockProcessor(LogicLocks lock) {
//			super(lock);
//		}
//
//		@Override
//		public void requestLockSet() {
//			super.requestLockSet();
////			if (StateChanged) {
//				MySignalHead.forceAspect(determineAspect());
////			}
//		}
//
//		@Override
//		public void requestLockClear() {
//			super.requestLockClear();
////			if (StateChanged) {
//				MySignalHead.forceAspect(determineAspect());
////			}
//		}
//
//		@Override
//		public void advanceLockSet() {
//			super.advanceLockSet();
////			if (StateChanged) {
//				MySignalHead.forceAspect(determineAspect());
////			}
//		}
//
//		@Override
//		public void advanceLockClear() {
//			super.advanceLockClear();
////			if (StateChanged) {
//				MySignalHead.forceAspect(determineAspect());
//			}
////		}		
//	}
	
	/**********************************************************************************
	 * Lock processors and decorators.
	 **********************************************************************************/
	/**************************************************************
	 * The detection decorator for an APB intermediate signal.  It extends
	 * the Detection lock processing by running the TrafficStick state machine.
	 * 
	 * It is intended to look for any changes in the MergedLocks, to cover detection
	 * in the SafetyZone.
	 *************************************************************/
	class APBDetectionDecorator extends DefaultProcessorDecorator {
		
		/**
		 * the constructor
		 * @param processor is the LockRequestProcess being decorated
		 */
		public APBDetectionDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			Tsr.setOccupy();
			Processor.requestLockSet();
		}
		
		@Override
		public void requestLockClear() {
			Tsr.clearOccupy();
			Processor.requestLockClear();
		}
		
		@Override
		public void advanceLockSet() {
			Tsr.setOccupy();
			Processor.advanceLockSet();
		}
		
		@Override
		public void advanceLockClear() {
			Tsr.clearOccupy();
			Processor.advanceLockClear();
		}
	}
	/**************************************************************************
	 *  end of APBDetectionDecorator
	 **************************************************************************/

//	/**************************************************************
//	 * The detection decorator for CTC.  It extends
//	 * the detection lock processing by clearing the Route Lock if
//	 * not fleeting (which, in turn, may clear the neighbor's Hold Lock)
//	 *************************************************************/
//	class CTCDetectionDecorator extends AbstractProcessorDecorator {
//		
//		/**
//		 * the constructor
//		 * @param processor is the LockRequestProcess being decorated
//		 */
//		public CTCDetectionDecorator(LockRequestProcessor processor) {
//			super(processor);
//		}
//
//		@Override
//		public void requestLockSet() {
//			Processor.requestLockSet();
//		}
//		
//		@Override
//		public void requestLockClear() {
//			Processor.requestLockClear();
//			clearLocalLock(LogicLocks.ROUTELOCK);
//		}
//
//		@Override
//		public void advanceLockSet() {
//			Processor.advanceLockSet();
//		}
//
//		@Override
//		public void advanceLockClear() {
//			Processor.advanceLockClear();
//		}
//	}
//	/**************************************************************************
//	 *  end of CTCDetectionDecorator
//	 **************************************************************************/
//
	/**************************************************************
	 * The detection decorator for stimulating signal computation.
	 * 
	 * It looks for changes to the MergedLocks.
	 *************************************************************/
	class SignalDecorator extends DefaultProcessorDecorator {

		/**
		 * the constructor
		 * @param processor is the LockRequestProcess being decorated
		 */
		public SignalDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
			MySignalHead.forceAspect(determineAspect());
		}

		@Override
		public void requestLockClear() {
			Processor.requestLockClear();
			MySignalHead.forceAspect(determineAspect());
		}

		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
			MySignalHead.forceAspect(determineAspect());
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
			MySignalHead.forceAspect(determineAspect());
		}
	}
	/**************************************************************************
	 *  end of SignalDecorator
	 **************************************************************************/

	/**************************************************************
	 * The Approach Lock decorator for an APB intermediate signal. 
	 * 
	 * Approach should only be set/cleared on MyLocks, but processing on
	 * AdvanceLocks is a pass-through.
	 * 
	 * The primary function of this lock processor is to run the TSR state machine.
	 * The order makes a difference because the signal indication looks at
	 * the TSR state.
	 *************************************************************/
	class APBApproachDecorator extends AbstractProcessorDecorator {
		/**
		 * the constructor
		 */
		public APBApproachDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			Tsr.setApproach();
			Processor.requestLockSet();
		}

		@Override
		public void requestLockClear() {
			Tsr.clearApproach();
			Processor.requestLockClear();
		}

		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
		}
	}
	/**************************************************************************
	 *  end of APBApproachDecorator
	 **************************************************************************/

//	/**************************************************************************
//	 * A lock decorator that copies the lock from the global lock bus to the route
//	 * lock bus so that it will influence the control point at the end of the route,
//	 * but not the Intermediate signals.  Its affect on those is handled by the
//	 * TrackCircuit.
//	 **************************************************************************/
//	class RouteDecorator extends AbstractProcessorDecorator {
//		
//		/**
//		 * the constructor
//		 * @param processor is the LockRequestProcess being decorated
//		 */
//		public RouteDecorator(LockRequestProcessor processor) {
//			super(processor);
//		}
//
//		@Override
//		public void requestLockSet() {
//			Processor.requestLockSet();
//			if (mergeRouteLocks(Processor.getLock()) && (NextSignal != null)) {
//				NextSignal.setRouteLock(Processor.getLock());
//			}
//		}
//		
//		@Override
//		public void requestLockClear() {
//			Processor.requestLockClear();
//			if (mergeRouteLocks(Processor.getLock()) && (NextSignal != null)) {
//				NextSignal.clearRouteLock(Processor.getLock());
//			}
//		}
//		
//		@Override
//		public void advanceLockSet() {
//			Processor.advanceLockSet();
//			if (mergeRouteLocks(Processor.getLock()) && (NextSignal != null)) {
//				NextSignal.setRouteLock(Processor.getLock());
//			}
//		}
//		
//		@Override
//		public void advanceLockClear() {
//			Processor.advanceLockClear();
//			if (mergeRouteLocks(Processor.getLock()) && (NextSignal != null)) {
//				NextSignal.clearRouteLock(Processor.getLock());
//			}
//		}
//	}
//	/**************************************************************************
//	 *  end of RouteLockDecorator
//	 **************************************************************************/

	/**************************************************************************
	 * The traffic stick relay.  This object determines the direction a train
	 * is traveling through a block when the block's occupancy detector trips.
	 * 
	 * The traffic stick relay can be modeled as a state machine with 2 stimuli
	 * (my block occupied, neighbor block occupied), and 4 binary state variables
	 * (my block occupied, my stick set for entry, neighbor occupied, neighbor
	 * stick sent for entry).  However, the occupied and stick set can be combined
	 * into a field of enum type StickValue).  The state machine has an inherent
	 * symmetry around which occupancy happened first.  Thus, the state machine
	 * tracks both the stick state for this BlkEdge, but also the stick state
	 * for the neighbor.  This removes a direct coupling between the two edges
	 * (neither side has to ask the other what state it is in), but makes the
	 * assumption that the two machines are in unison.  Furthermore, this algorithm
	 * allows two block edges to be unmatched - one can be running the state machine
	 * and the other not.
	 * 
	 * The implementation is not a pure state machine - though it could be.  A
	 * state machine seemed to be overkill.  Thus, there is no "state" variable.
	 * Instead, the state can be inferred from looking at the StickValue of this
	 * edge and the StickValue of the neighbor edge.
	 */
	public class TSR_StateMachine {
		/**
		 * the value of the traffic stick for this edge
		 */
		private StickValue MyStick = StickValue.NODIRECTION;

		/**
		 * the value of the traffic stick for the neighboredge
		 */
		private StickValue OtherStick = StickValue.NODIRECTION;
		
		/**
		 * is called when the block containing the edge is occupied.
		 * This forces an execution of the state machine.
		 */
		public void setOccupy() {
			if (OtherStick == StickValue.EXITEDGE) {
				MyStick = StickValue.ENTRYEDGE;
			}
			else {
				MyStick = StickValue.EXITEDGE;
			}
			/******************** DEBUG code *****************************/
			if (Crandic.Details.get(DebugBits.TRAFFICSTICKBIT)) {
				System.out.println(Identity + ": Stick value is " + MyStick.toString());			
			}
		    /*************************************************************/
		}
		
		/**
		 * is called whenever occupancy is cleared.  In any state,
		 * it sets my direction to none.
		 */
		public void clearOccupy() {
			MyStick = StickValue.NODIRECTION;
			/******************** DEBUG code *****************************/
			if (Crandic.Details.get(DebugBits.TRAFFICSTICKBIT)) {
				System.out.println(Identity + ": Stick value is " + MyStick.toString());			
			}
		    /*************************************************************/
		}
		
		/**
		 * is called whenever the neighbor block is occupied.
		 */
		public void setApproach() {
			if (MyStick == StickValue.EXITEDGE) {
				OtherStick = StickValue.ENTRYEDGE;
			}
			else {
				OtherStick = StickValue.EXITEDGE;
			}
			/******************** DEBUG code *****************************/
			if (Crandic.Details.get(DebugBits.TRAFFICSTICKBIT)) {
				System.out.println(Identity + ": OtherStick value is " + OtherStick.toString());			
			}
		    /*************************************************************/
		}
		
		/**
		 * is called when occupancy ends on the neighbor block
		 */
		public void clearApproach() {
			OtherStick = StickValue.NODIRECTION;
//			MySignalHead.forceAspect(determineAspect());
			/******************** DEBUG code *****************************/
			if (Crandic.Details.get(DebugBits.TRAFFICSTICKBIT)) {
				System.out.println(Identity + ": OtherStick value is " + OtherStick.toString());			
			}
		    /*************************************************************/
		}
		
		/**
		 * In the literature, a traffic stick is set, when it is determined
		 * that a train has entered a block through the edge containing the
		 * traffic stick; thus, though the stick appears to have three values,
		 * only two are actually used.  This predicate reduces the three values
		 * to two - entry and not entry.
		 * @return true if the value is entry.
		 */
		public boolean isStickSet() {
			return MyStick == StickValue.ENTRYEDGE;
		}
		
		/**
		 * the same predicate as above, but for the neighbor's traffic stick
		 * @return true if the neighbor's traffic stick is set.  It is queried
		 * only when the neighbor's occupancy status changes.
		 */
		public boolean isOtherStickSet() {
			return OtherStick == StickValue.ENTRYEDGE;
		}
}
	/**************************************************************************
	 *  end of TSR_StateMachine
	 **************************************************************************/
//	/**********************************************************************************
//	 * The TimeLockProcessor
//	 **********************************************************************************/
//	/**
//	 * The class handling requests to set or clear the TimeLock.  The time lock
//	 * is used in conjunction with "running time".  Running time happens when a
//	 * cleared signal is knocked down in front of a train.  The train's approach
//	 * signal drops, but the rest of the route components are frozen for a certain
//	 * amount of time.  This allows the train to overrun the signal without doing
//	 * any damage.  After time expires, the route can be modified.
//	 * <p>
//	 * This lock is counter-intuitive.  On first examination it appears to work backwards
//	 * from others.
//	 * - it is set when a signal is knocked down and a train is approaching the signal
//	 * - it is cleared when the timer expires or
//	 * - it is cleared when the signal clears again or
//	 * - the approach block becomes unoccupied
//	 * 
//	 * The lock does not knock down a signal or hold one at STOP.  It only locks
//	 * the turnouts.  It does this by setting the TIMELOCK on the peer VitalLogic
//	 * advance lock set.  If the peer is a signal, nothing happens.  If the peer
//	 * is a simple block end, then the lock handler can be the default, decorated
//	 * with the propagation decorator.  This propagates the lock through any
//	 * intervening Points and Frogs to the opposite protecting signal.
//	 * 
//	 * The advance methods are not used because the lock is local to the VitalLogic.
//	 * 
//	 * Because TIMELOCK does not merge into the merged locks and does not enter
//	 * into the signal indication determination, then if the Halt condition clears
//	 * before the timer fires, exterior logic will stop the timer.
//	 * 
//	 * Because running time may be disabled, this class handles sending the TIMELOCK
//	 * code to the GUI.
//	 */
//	protected class TimeLockProcessor extends MergedRequestLockProcessor {
//		
//		/**
//		 * is where to find the current value for how much time to delay
//		 * when running time
//		 */
//		private final Sequence RunningTimeDuration;
//		
//		/**
//		 * is a Timer for running time
//		 */
//		private Timer RunningTime; 
//		
//		/**
//		 * the constructor
//		 */
//		public TimeLockProcessor() {
//			super(LogicLocks.TIMELOCK);
//			RunningTimeDuration = CounterFactory.CountKeeper.findSequence(CounterFactory.TIMELOCKTAG);
//			RunningTime = new Timer( 0, new ActionListener() {
//				public void actionPerformed(ActionEvent ae) {
//					LockProcessor.get(LogicLocks.TIMELOCK).requestLockClear();
//				}
//			});
//			RunningTime.setRepeats(false);
//		}
//
//		/**
//		 * the request is invoked when a signal is knocked down.  The conditions
//		 * for satisfying the request are:
//		 * 1. the lock is not set (time is not running)
//		 * 2. a route exists
//		 * 3. a train is approaching
//		 * 4. a non-zero running time has been defined
//		 * 
//		 * If all of these are true, then the timer is started.  The lock does not
//		 * check the conditions.  They should be checked before setting the lock.
//		 */
//		public void requestLockSet() {
//			if (!MyLocks.contains(Lock)) {
//				int timeout = RunningTimeDuration.getAdjustment() * 1000;
//				if (timeout != 0) {
//					MyLocks.add(Lock);
//					PeerLogic.setAdvanceLock(Lock);
//
//					// starts the timer.  the route will be cancelled when it expires
//					RunningTime.setDelay(timeout);
//					RunningTime.setInitialDelay(timeout);
//					RunningTime.start();
//					Transmitter.createPacket(CodePurpose.INDICATION, Codes.TIMELOCK, Constants.TRUE);
//				}
//			}
//		}
//		
//		/**
//		 * invoked when the timer fires or the set conditions are cleared.  
//		 * It stops the timer and clears the lock.
//		 */
//		public void requestLockClear() {
//			RunningTime.stop();
//			if (MyLocks.contains(Lock)) {
//				MyLocks.remove(Lock);
//				PeerLogic.clearAdvanceLock(Lock);
//				Transmitter.createPacket(CodePurpose.INDICATION, Codes.TIMELOCK, Constants.FALSE);
//				if (MyLocks.contains(LogicLocks.TRAFFICLOCK)) {
//					Transmitter.createPacket(CodePurpose.INDICATION, Codes.SIGNALINDICATIONLOCK, Constants.FALSE);
//				}
//			}
//		}
//		
////		/**
////		 * is invoked whenever the dispatcher attempts to set a route.  The backwards piece
////		 * of this code is that the timer maybe running.  That case signifies that a route was
////		 * active, the dispatcher attempted to cancel it, which caused time to run.  Then, the
////		 * dispatcher decided to resume the route, cancelling the cancellation.
////		 */
////		public void cancelSetRequest() {
////			if (isLockSet()) {
////				RunningTime.stop();
////				MyLocks.remove(Lock);
////				Transmitter.createPacket(CodePurpose.INDICATION, TimeCode, Constants.TRUE);
////			}
////			else {
////				LockRequestProcessor entryLogic = LockProcessor.get(LogicLocks.ROUTELOCK);
////				entryLogic.requestLockSet();
////				Transmitter.createPacket(CodePurpose.INDICATION, TimeCode, 
////						entryLogic.isLockSet() ? Constants.TRUE : Constants.FALSE);
////			}
////			MySignalHead.forceAspect(determineAspect());
////		}
//	}
//	/**************************************************************************
//	 *  end of TimeLock processor
//	 **************************************************************************/
	/**********************************************************************************
	 * The TimeLockDecorator
	 **********************************************************************************/
	/**
	 * The class handling requests to set or clear the TimeLock.  The time lock
	 * is used in conjunction with "running time".  Running time happens when a
	 * cleared signal is knocked down in front of a train.  The train's approach
	 * signal drops, but the rest of the route components are frozen for a certain
	 * amount of time.  This allows the train to overrun the signal without doing
	 * any damage.  After time expires, the route can be modified.
	 * <p>
	 * This lock is counter-intuitive.  On first examination it appears to work backwards
	 * from others.
	 * - it is set when a signal is knocked down and a train is approaching the signal
	 * - it is cleared when the timer expires or
	 * - it is cleared when the signal clears again or
	 * - the approach block becomes unoccupied
	 * 
	 * The lock does not knock down a signal or hold one at STOP.  It only locks
	 * the turnouts.  It does this by setting the TIMELOCK on the peer VitalLogic
	 * advance lock set.  If the peer is a signal, nothing happens.  If the peer
	 * is a simple block end, then the lock handler can be the default, decorated
	 * with the propagation decorator.  This propagates the lock through any
	 * intervening Points and Frogs to the opposite protecting signal.
	 * 
	 * The advance methods are not used because the lock is local to the VitalLogic.
	 * 
	 * Because TIMELOCK does not merge into the merged locks and does not enter
	 * into the signal indication determination, then if the Halt condition clears
	 * before the timer fires, exterior logic will stop the timer.
	 * 
	 * Because running time may be disabled, this class handles sending the TIMELOCK
	 * code to the GUI.  It should be decorated with a AdvanceLockFilterDecorator.
	 *******************************************************************************/
	protected class TimeLockDecorator extends DefaultProcessorDecorator implements TimeoutObserver {
		
		/**
		 * is where to find the current value for how much time to delay
		 * when running time
		 */
		private final Sequence RunningTimeDuration;
		
		/**
		 * is a Timer for running time
		 */
//		private Timer RunningTime; 
		private TimeoutEvent RunningTime;
		
		/**
		 * the constructor
		 * @param processor is the processor being decorated
		 */
		public TimeLockDecorator(LockRequestProcessor processor) {
			super(processor);
			RunningTimeDuration = CounterFactory.CountKeeper.findSequence(CounterFactory.TIMELOCKTAG);
//			RunningTime = new Timer( 0, new ActionListener() {
//				public void actionPerformed(ActionEvent ae) {
//					LockProcessor.get(LogicLocks.TIMELOCK).requestLockClear();
//				}
//			});
//			RunningTime.setRepeats(false);
			RunningTime = new TimeoutEvent(this, RunningTimeDuration.getAdjustment());
		}
		
		/**
		 * the request is invoked when a signal is knocked down.  The conditions
		 * for satisfying the request are:
		 * 1. the lock is not set (time is not running)
		 * 2. a route exists
		 * 3. a train is approaching
		 * 4. a non-zero running time has been defined
		 * 
		 * If all of these are true, then the timer is started.  The lock does not
		 * check the conditions.  They should be checked before setting the lock.
		 */
		public void requestLockSet() {
//			int timeout = RunningTimeDuration.getAdjustment() * 1000;
			int timeout = RunningTimeDuration.getAdjustment();
			if (timeout != 0) {
				PeerLogic.setAdvanceLock(Lock);
				Processor.requestLockSet();
				// starts the timer.  the route will be cancelled when it expires
//				RunningTime.setDelay(timeout);
//				RunningTime.setInitialDelay(timeout);
//				RunningTime.start();
				if (RunningTime.EventFree) {
					RunningTime.Count = timeout;
					MasterClock.MyClock.setTimeout(RunningTime);
				}
			}
		}
		
		/**
		 * invoked when the timer fires or the set conditions are cleared.  
		 * It stops the timer and clears the lock.
		 */
		public void requestLockClear() {
//			RunningTime.stop();
			if (!RunningTime.EventFree) {
				MasterClock.MyClock.cancelTimer(RunningTime);
			}
			PeerLogic.clearAdvanceLock(Lock);
			Processor.requestLockClear();
		}	
		
		/**
		 * processes the timeout event
		 */
		public void acceptTimeout() {
			LockProcessor.get(LogicLocks.TIMELOCK).requestLockClear();
		}
	}
	/**************************************************************************
	 *  end of TimeLockDecorator
	 **************************************************************************/
}
/* @(#)IntermediateVitalLogic.java */
