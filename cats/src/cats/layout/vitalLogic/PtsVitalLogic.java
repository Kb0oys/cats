/* Name: PtsVitalLogic.java
 *
 * What:
 * This file contains the definition for the VitalLogic sub-class associated with the points
 * end of a turnout.  PtsVitalLogic and FrogVitalLogic are Siamnese twins on a SecEdge.  The
 * normal flow of lock changes is from the advance Vital Logic, which comes through the
 * other end of the Track (FrogVitalLogic) first.
 * <p>
 * The PtsVitalLogic can also be thought of as a surrogate for the SecEdge neighbor; thus,
 * it receives lock flow from VitalLogic in advance and relays it through the frog.  This
 * behavior confuses things because the VitalLogic in approach (opposite the normal flow)
 * does not distinguish the PtsVitalLogic from any other VitalLogic, so the "standard" transport
 * interfaces will be invoked by the approach VitalLogic.  This is OK because the frog
 * VitalLogic will receive the normal lock flow and invoke methods in the points that
 * are complimentary.  However, to leverage on code in BasicVitalLogic, some fields will also
 * be used as neighbor "surrogate" fields.
 * <p>
 * The PtsVitalLogic holds the ApproachLogic target for the current FrogVitalLogic.  
 * <p>
 * For the normal lock flow, the following are used:
 * <ul>
 * <li>ApproachLogic - the VitalLogic fed by the frog, in approach of the points</li>
 * <li>mergeFrogSpeeds() - the method called to merge the frog speed with Speed</li>
 * </ul>
 * For the reverse flow, the following are used:
 * <ul>
 * <li>ProxySignal - an immutable SignalVitalLogic that is the target of the VitalLogic
 * in the approach direction</li>
 * <li>AdvanceLocks - the locks received from the VitalLogic in approach</li>
 * <li>MergedGlobalLocks - the merger of MyLocks and AdvanceLocks</li>
 * <li>mergeGlobalLocks() - the method to generate MergedGlobalLocks</li>
 * <li>ApproachLocks - the last locks sent through the frog (MergedGlobalLocks with ApproachFilter
 * removed)</li>
 * <li>mergeSpeeds() - the method called to merge the approach speed into this speed,
 * which is forwarded through the frog</li>
 * </ul>
 * For both, the following are used:
 * <ul>
 * <li>MyLocks - the local locks</li>
 * <li>MergedLocalLocks - the merger of MyLocks, AdvanceLocks, and FrogAdvanceLocks</li>
 * <li>Speed - the slowest speed of the tracks in advance<li>
 * <li>ApproachFilter - locks removed from GlobalLocks before being forwarded</li>
 * <ul>
 * The following are not used:
 * <ul>
 * <li>NeighborLogic because it could be null (SecEdge) or FrogVitalLogic</li>
 * <li>PeerLogic because it changes, depending upon how the points are lined</li>
 * </ul>
 * <p>
 * For PtsVitalLogic, there are three sources of Lock changes and they are treated
 * differently:
 * <ul>
 * <li>
 * from the "advance" (actually neighbor).  Most of these are handled by recording the
 * change and invoking the same change in the current Frog's approach direction.  This
 * forwards the change around the frog.
 * </li>
 * <li>
 * from the code line (GUI).  These changes are sent in both directions - towards the
 * neighbor and towards all frogs.
 * </li>
 * <li>
 * from the current frog.  Most of these are simply towards the neighbor.
 * </li>
 * </ul>
 */
package cats.layout.vitalLogic;

import java.util.EnumMap;
import java.util.EnumSet;

import cats.common.Constants;
import cats.gui.LocalEnforcement;
import cats.layout.Discipline;
import cats.layout.codeLine.CodeMessage;
import cats.layout.codeLine.CodePurpose;
import cats.layout.codeLine.Codes;
import cats.layout.codeLine.packetFactory.PacketFactory;
import cats.layout.items.IOSpec;
import cats.layout.items.PtsEdge;
import cats.layout.items.RouteInfo;
import cats.layout.items.SwitchPoints;
import cats.layout.items.Track;
import cats.trackCircuit.RelayTrackCircuit;
import cats.trackCircuit.TrackCircuit;

/**
 * This file contains the definition for the VitalLogic sub-class associated with the points
 * end of a turnout.  PtsVitalLogic and FrogVitalLogic are Siamese twins on a SecEdge.  
 * <p>
 * Unlike other VitalLogic, locks flow through PtsVitalLogic in both directions:
 * <ol>
 * <li>advance VitalLogic->PtsVitalLogic->FrogVitalLogic->Frog approach VitalLogic</li>
 * <li>approach VitalLogic<-PtsVitalLogic<-FrogVitalLogic<-Frog advance VitalLogic</li>
 * </ol>
 * <B>The two streams must not be mixed!</B>
 * </p>
 * <p>
 * The PtsVitalLogic can also be thought of as a surrogate for the SecEdge neighbor; thus,
 * it receives lock flow from VitalLogic in advance and relays it through the frog.  This
 * behavior confuses things because the VitalLogic in advance 
 * does not distinguish the PtsVitalLogic from any other VitalLogic, so the "standard" transport
 * interfaces will be invoked by the advance VitalLogic.  Thus, the fields and methods inherited
 * from the BasicVitalLogic will be used for direction 1, above.  Specifically:
 * <ul>
 * <li>
 * setLocalLock(), clearLocalLock(), setAdvanceLock(), clearAdvanceLock(),
 * MyLocks, AdvanceLocks, MergedGlobalLocks
 * </li>
 * <li>
 * mergeSpeed(), AdvanceSpeed is the speed of the tracks between the advance VitalLogic and the PtsVitalLogic
 * </li>
 * <li>setApproachLogicLock(), clearApproachLogicLock(), ApproachLocks</li>
 * <li>FedCircuit is the TrackCircuit receiving signal indications</li>
 * <li>primeApproach()</li>
 * </ul>
 * Note that ApproachLogic is <B>not</B> included because the ApproachLogic varies by the points
 * alignment.  So, locks intended for approach VitalLogic are forwarded through all Frogs.
 * All fouling ends will also have CONFLICTINGSIGNAL set.
 * For the second direction (2 above), analogous locks fields and methods are created that
 * the FrogVitalLogic use to stimulate the PtsVitalLogic approach VitalLogic.
 * <ul>
 * <li>
 * setAdvanceLogicLock(), clearAdvanceLogicLock(), but if the PtsVitalLogic does not use the MyLocks
 * equivalent, then locks from the FrogVitalLogic are not remembered
 * </li>
 * <li>
 * AdvanceMyLocks, AdvanceAdvanceLocks, AdvanceMergedLocks are the equivalents.  Because they
 * are Enumset&lt;Locks&gt;, the handlers can be LockRequestProcessor, stored in AdvanceLockProcessor.
 * </li>
 * <li>ApproachLogic is the VitalLogic in approach, in the Frog->Points direction</li>
 * <li>mergeFrogSpeed()</li>
 * <li>ApproachCircuit is the next TrackCircuit, in the Frog->Points direction</li>
 * </ul>
 * Some BasicVitalLogic fields are not used:
 * <ul>
 * <li>NeighborLogic, PeerLogic</li>
 * </ul>
 * @see cats.layout.items.PtsEdge
 * @see cats.layout.vitalLogic.FrogVitalLogic
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014, 2015, 2016, 2018, 2019, 2020, 2021, 2022, 2024</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class PtsVitalLogic extends BasicVitalLogic {

//	/**
//	 * the locks pushed down the fouling routes
//	 */
//	private static final EnumSet<LogicLocks> FOULING_LOCK = EnumSet.of(LogicLocks.CONFLICTINGSIGNALLOCK);
	
//	/**
//	 * the locks which are broadcast to all ends
//	 */
//	private static final EnumSet<LogicLocks> OMNI_LOCKS = EnumSet.of(LogicLocks.DETECTIONLOCK, LogicLocks.SWITCHUNLOCK, LogicLocks.MAINTENANCELOCK);
	
	/**
	 * the VitalLogics of the routes through the turnout.  These are oriented
	 * for exit from the legs of the turnout into the PtsEdge.  There is one
	 * element for each possible leg (where the index corresponds to the Section
	 * edge that the track terminates on).  One element (the one the points are
	 * on) must be empty.
	 */
	private FrogVitalLogic[] Frog = new FrogVitalLogic[PtsEdge.MAX_ROUTES];

	/**
	 * is a Boolean for each frog route.  It is true if the points have dispatcher
	 * control or the route is the normal route.  This is used to retain the
	 * CONFLICTINGSIGNAL lock when lined for the non-normal track.
	 */
	private boolean[] UnlockRoute = {
			false,
			false,
			false,
			false
	};
	
	/**
	 * The index of the currently selected route.  -1 signifies no route -
	 * "out of correspondence".
	 */
	private int CurrentRoute = Constants.OutofCorrespondence;

	/**
	 * the index of the route for which a movement command has been sent.  This
	 * is the index of the "make" event
	 */
	private int ExpectedMakeEvent = Constants.OutofCorrespondence;
	
	/**
	 * the index of the "current" route, which is used for exact feedback.  A "break"
	 * event; however, it may be seen before or after the make event.
	 */
	private int ExpectedBreakEvent = Constants.OutofCorrespondence;
	
	/**
	 * the index of a route event to trigger the override operation on.  When an
	 * unexpected feedback event is received and the conditions the points are
	 * CTC locked with overridden, there may be one or two events received.  The first is
	 * ignored and the second triggers the override command.  This field holds the
	 * index of the second.
	 */
	private int UnexpectedSecondEvent;
	
	/**
	 * the hardware interface to the points
	 */
	private SwitchPoints Points;
	
	/**
	 * is the approach Track Circuit - the one that relays to the Frog
	 */
	private RelayTrackCircuit FedCircuit;
	
//	/**
//	 * is the SignalVitalLogic the Frogs feed.  It changes depending upon
//	 * the point alignment.
//	 */
//	private SignalVitalLogic ApproachSignal;
//	
//	/**
//	 * the route locks from the SignalVitalLogic in advance
//	 */
//	private EnumSet<LogicLocks> AdvanceRouteLocks = EnumSet.noneOf(LogicLocks.class);
//	
//	/**
//	 * the merger of the MergedGlobalLocks and AdvanceRouteLocks
//	 */
//	private EnumSet<LogicLocks> MergedRouteLocks = EnumSet.noneOf(LogicLocks.class);
//	
	/**********************************************************************************
	 * The analogous lock handlers for Frog->Points traffic flow
	 **********************************************************************************/
	/**
	 * the logic locks generated by the PtsVitalLogic
	 */
	private EnumSet<LogicLocks> AdvanceMyLocks = EnumSet.noneOf(LogicLocks.class);
	
	/**
	 * the logic locks generated by the current FrogVitalLogic
	 */
	private EnumSet<LogicLocks> AdvanceAdvanceLocks = EnumSet.noneOf(LogicLocks.class);

	/**
	 * the merged Locks
	 */
	private EnumSet<LogicLocks> AdvanceMergedLocks = EnumSet.noneOf(LogicLocks.class);

	/**
	 * the lock processors
	 */
	protected EnumMap<LogicLocks, LockRequestProcessor> AdvanceLockProcessor = new EnumMap<LogicLocks, LockRequestProcessor>(LogicLocks.class);
	
	/**
	 * is the Track Circuit the Frogs feed
	 */
	protected TrackCircuit ApproachCircuit;
	
	/**********************************************************************************
	 * the local locks requiring special handling by PtsVitalLogic
	 **********************************************************************************/
//	private LockRequestProcessor MaintenanceLock = new DefaultCodeRequestLockProcessor(new MaintenanceLockProcessor());
//	private LockRequestProcessor MaintenanceLock = new MaintenanceLockProcessor();
	private LockRequestProcessor MaintenanceLock;
	
	/**
	 * the ctor
	 * @param transmitter is the place to send and receive code line messages to the GUI
	 * @param id identifies the PtsVitalLogic for debugging
	 */
	public PtsVitalLogic(final PacketFactory transmitter, final String id) {
		super(transmitter, id);
		FedCircuit = new RelayTrackCircuit(id);
	}

	@Override
	public void configureLockProcessing() {	

		// DETECTIONLOCK - a local lock is sent to all nodes by the GUI; an advance lock is sent to the Peers
		// of all Frogs
		LockProcessor.put(LogicLocks.DETECTIONLOCK, 
				new LockFilterDecorator(
						new CommonDetectionDecorator(
								new MergedLockFilterDecorator(
										new BlastPtsLockDecorator(
												new MergedRequestLockProcessor(LogicLocks.DETECTIONLOCK))))));
		// CONFLICTINGSIGNALLOCK - a local lock is ignored;  an advance lock
		// is sent to the Peers of all Frogs
		LockProcessor.put(LogicLocks.CONFLICTINGSIGNALLOCK, 
				new MyLocksFilterDecorator(
						new SinglePtsLockDecorator(
//								new MergedRequestLockProcessor(LogicLocks.CONFLICTINGSIGNALLOCK))));
								new DefaultRequestLockProcessor(LogicLocks.CONFLICTINGSIGNALLOCK))));

		// HOLDLOCK - should never be set or cleared
		// SWITCHUNLOCK - a local lock is sent to just these points by the GUI; an advance lock
		// is sent to the Peers of all Frogs
		LockProcessor.put(LogicLocks.SWITCHUNLOCK, 
				new LockFilterDecorator(
						new LocalCodeLockDecorator(
								new PtsLockDecorator(
										new MergedLockFilterDecorator(
												new BlastPtsLockDecorator(
														new MergedRequestLockProcessor(LogicLocks.SWITCHUNLOCK)))))));
		// APPROACHLOCK - should never be set or cleared
		// TIMELOCK
		// TRAFFICLOCK - should never be set or cleared
		// ROUTELOCK - a local lock is copied from advance on detection and cleared when detection is cleared;
		// an advance lock must be remembered and propagated to the Peer of the active Frog; however, moving
		// the points clears both (cancels the route)
		LockProcessor.put(LogicLocks.ROUTELOCK, 
				new MergedLockFilterDecorator(
						new SinglePtsLockDecorator(
								new MergedRequestLockProcessor(LogicLocks.ROUTELOCK))));
		// PROTECTIONLOCK - a local lock is copied from advance on detection and cleared when detection is cleared;
		// an advance lock must be remembered and propagated to Peer of the active Frog; however, moving
		// the points clears both (cancels the route)
		LockProcessor.put(LogicLocks.PROTECTIONLOCK, 
				new MergedLockFilterDecorator(
						new SinglePtsLockDecorator(
								new MergedRequestLockProcessor(LogicLocks.PROTECTIONLOCK))));
		// MAINTENANCELOCK - a local lock is sent to all nodes by the GUI; an advance lock is sent to the Peers
		// of all Frogs
		MaintenanceLock = new LockFilterDecorator(
				new MaintenanceLockDecorator(
						new PtsLockDecorator(
								new MergedLockFilterDecorator(
										new BlastPtsLockDecorator(
												new MergedRequestLockProcessor(LogicLocks.MAINTENANCELOCK))))));
		LockProcessor.put(LogicLocks.MAINTENANCELOCK, MaintenanceLock);
		// CALLONLOCK
		LockProcessor.put(LogicLocks.CALLONLOCK,
				new MyLocksFilterDecorator(
						new BlastPtsLockDecorator(
								new DefaultRequestLockProcessor(LogicLocks.CALLONLOCK))));
		// FLEETLOCK - a local lock never set or cleared;
		// an advance lock must be remembered and propagated to Peer of the active Frog; however, moving
		// the points clears both (cancels the route)
//		LockProcessor.put(LogicLocks.FLEETLOCK, 
//				new MyLocksFilterDecorator(
//						new BlastPtsLockDecorator(
//								new DefaultRequestLockProcessor(LogicLocks.FLEETLOCK))));
		// EXTERNALLOCK - a local lock is sent to all nodes by the GUI; an advance lock is sent to the Peers
		// of all Frogs
		LockProcessor.put(LogicLocks.EXTERNALLOCK, 
				new LockFilterDecorator(
						new LocalCodeLockDecorator(
								new PtsLockDecorator(
										new MergedLockFilterDecorator(
												new BlastPtsLockDecorator(
														new MergedRequestLockProcessor(LogicLocks.EXTERNALLOCK)))))));
		
		// The lock processing for the frog->points direction
		// DETECTIONLOCK
		AdvanceLockProcessor.put(LogicLocks.DETECTIONLOCK, new ReverseLockRequestProcessor(LogicLocks.DETECTIONLOCK));
		// CONFLICTINGSIGNALLOCK
		AdvanceLockProcessor.put(LogicLocks.CONFLICTINGSIGNALLOCK, new ReverseLockRequestProcessor(LogicLocks.CONFLICTINGSIGNALLOCK));
		// HOLDLOCK - should never be set or cleared
		// SWITCHUNLOCK
		AdvanceLockProcessor.put(LogicLocks.SWITCHUNLOCK, new ReverseLockRequestProcessor(LogicLocks.SWITCHUNLOCK));
		// APPROACHLOCK - should never be set or cleared
		// TIMELOCK
		AdvanceLockProcessor.put(LogicLocks.TIMELOCK, new SimpleReverseLockRequestProcessor(LogicLocks.TIMELOCK));
		// TRAFFICLOCK - should never be set or cleared
		// ROUTELOCK
		AdvanceLockProcessor.put(LogicLocks.ROUTELOCK, new SimpleReverseLockRequestProcessor(LogicLocks.ROUTELOCK));
		// PROTECTIONLOCK
		AdvanceLockProcessor.put(LogicLocks.PROTECTIONLOCK, new SimpleReverseLockRequestProcessor(LogicLocks.PROTECTIONLOCK));
		// MAINTENANCELOCK
		AdvanceLockProcessor.put(LogicLocks.MAINTENANCELOCK, new ReverseLockRequestProcessor(LogicLocks.MAINTENANCELOCK));
		// CALLONLOCK
		AdvanceLockProcessor.put(LogicLocks.CALLONLOCK, new ReverseLockRequestProcessor(LogicLocks.CALLONLOCK));
		// FLEETLOCK - should never be set or cleared
//		AdvanceLockProcessor.put(LogicLocks.FLEETLOCK, new SimpleReverseLockRequestProcessor(LogicLocks.FLEETLOCK));
		// EXTERNALLOCK
		AdvanceLockProcessor.put(LogicLocks.EXTERNALLOCK , new ReverseLockRequestProcessor(LogicLocks.EXTERNALLOCK));
	}
	
	/**
	 * assigns the SwitchPoints to the PtsVitalLogic
	 * @param points is the SwitchPoints.  It must not be null.
	 */
	public void setPoints(final SwitchPoints points) {
		int normalRoute;
		Points = points;
		Points.setPtsLogic(this);
		normalRoute = Points.getNormal();
		if (Points.getSpur()) {
			UnlockRoute[normalRoute] = true;
		}
		else {
			for (int f = 0; f < UnlockRoute.length; ++f) {
				UnlockRoute[f] = true;
			}
		}
	}

	@Override
	protected TrackCircuit trackCircuitProbe() {
		// this keeps the probe going through each frog route
		for (FrogVitalLogic f : Frog) {
			if (f != null) {
				f.proxyCircuitProbe();
			}
		}
		return FedCircuit;
	}
	
	/**
	 * probes the approach direction for the TrackCircuit in the Frogs->Points direction
	 */
	protected void pointsCircuitProbe() {
		if (ApproachCircuit == null) {
			if (ApproachLogic != null) {
				ApproachCircuit = ApproachLogic.trackCircuitProbe();
			}
		}
	}
	
	/**
	 * initiates TrackCircuit probes from the GUI.
	 */
	public void connectTrackCircuits() {
		pointsCircuitProbe();
	}
	
	@Override
	public void mergeSpeeds(final int advSpeed) {
		int speed = MergedSpeed;
		AdvanceSpeed = advSpeed;
		MergedSpeed = Track.getSlower(Speed, AdvanceSpeed);
		if ((speed != MergedSpeed) && (CurrentRoute != Constants.OutofCorrespondence)) {
			Frog[CurrentRoute].mergeOpposingSpeed(MergedSpeed);
		}
	}
	
	/**
	 * is called to tell the PtsVitalLogic that the points have moved.
	 * The PtsVitalLogic can either accept the move or reject it.
	 * @param route is the index of the Track reporting the movement
	 * @param inPlace is true if the points moved to that Track and
	 * false if they moved from that Track
	 * @return true if the PtsVitalLogic accepts the move or false if
	 * it rejects the move.
	 */
	public boolean receiveFeedback(final int route, final boolean inPlace) {
		if (LocalEnforcement.TheEnforcementType.getFlagValue()
				&& !MaintenanceLock.isLockSet()
				&& (MyDiscipline.equals(Discipline.CTC) || MyDiscipline.equals(Discipline.DTC))
//				&& inPlace
				&& (route != ExpectedMakeEvent)
				&& (ExpectedMakeEvent != Constants.OutofCorrespondence)) {
			return false;
		}
		if (inPlace) {
			Transmitter.createPacket(CodePurpose.INDICATION, Codes.SWITCHPOSITION, String.valueOf(route));
			setSelectedTrack(route);
		}
		else {
			Transmitter.createPacket(CodePurpose.INDICATION, Codes.SWITCHPOSITION, String.valueOf(Constants.OutofCorrespondence));
			setSelectedTrack(Constants.OutofCorrespondence);
		}
		return true;
	}
	
	/**
	 * this method is used by the lined Frog to set a lock in the Frog->Points
	 * direction.
	 * @param l is the lock to set
	 */
	protected void setAdvanceLogicLock(final LogicLocks l) {
		LockRequestProcessor processor = AdvanceLockProcessor.get(l);
		if (processor != null) {
			processor.advanceLockSet();
		}
		else {
			AdvanceAdvanceLocks.add(l);
		}

	}

	/**
	 * this method is used by the lined Frog to clear a lock in the Frog->Points
	 * direction.
	 * @param l is the lock to set
	 */
	protected void clearAdvanceLogicLock(final LogicLocks l) {
		LockRequestProcessor processor = AdvanceLockProcessor.get(l);
		if (processor != null) {
			processor.advanceLockClear();
		}
		else {
			AdvanceAdvanceLocks.remove(l);
		}

	}

	@Override
	public void processCommand(final String command) {
		CodeMessage cmd = new CodeMessage(command);
		String state = cmd.Value;
		if (cmd.Purpose == CodePurpose.REQUEST) {
			switch (cmd.Lock) {
			case MAINTENANCELOCK:
				if (new Boolean(state).booleanValue()) {
					MaintenanceLock.requestLockSet();
				}
				else {
					MaintenanceLock.requestLockClear();
				}
				break;

			case SWITCHPOSITION:
				int position = Integer.parseInt(state);
				if ((position > Constants.OutofCorrespondence) && (position < PtsEdge.MAX_ROUTES)) {  // feedback will generate the indication
					// this must be the last command in this method because Points could call back into PtsVitalLogic for generating feedback
//					Points.selectRoute(position);
					movePoints(position);
				}
				else {
					Transmitter.createPacket(CodePurpose.DENY, Codes.SWITCHPOSITION, state);
				}
				break;

			default:
				Transmitter.createPacket(CodePurpose.INDICATION, cmd.Lock, state);
			}
		}
	}

	@Override
	public void primeApproach() {
		// this is the only method needed to initialize the frogs.
		for (LogicLocks l : LogicLocks.values()) {
			clearLocalLock(l);
		}
		for (int f = 0; f < Frog.length; f++) {
			if ((Frog[f] != null) && (Frog[f].PeerLogic != null)) {
				Frog[f].PeerLogic.setAdvanceLock(LogicLocks.CONFLICTINGSIGNALLOCK);
//				Frog[f].PeerLogic.setLocalLock(LogicLocks.CONFLICTINGSIGNALLOCK);

			}
		}
	}

	/**
	 * remembers the VitalLogic associated with each route through the
	 * turnout
	 * @param route is the index of the frog leg
	 * @param logic is the FrogVitalLogic on that leg
	 */
	public void setRouteLogic(final int route, final FrogVitalLogic logic) {
		Frog[route] = logic;
	}

	/**
	 * is a request from a RouteInfo to move the points to an
	 * alignment in response to a local request.  This method does
	 * not sanity check lined because it is issued only from a valid
	 * RouteInfo.
	 * @param lined is the Track to line the points to
	 * @return true if the operation can be safely performed and
	 * false if not
	 */
	public boolean requestLocalMovement(final int lined) {
		return Points.requestRoute(lined);
	}
	
	/**
	 * sets the route to a particular track.  If the alignment changes, then the
	 * old alignment must be set to fouling and the new alignment refreshed from
	 * the PtsVitalLogic state.  "Refreshing" includes forwarding local locks and
	 * locks from the logic in advance.
	 * @param selection is the index of the route (0 for left, 1 for bottom, etc.)
	 * that the points are lined to.  OutofCorrespondence is accepted.
	 */
	public void setSelectedTrack(final int selection) {
		if (CurrentRoute != selection) {
			// mark the current route as fouling
			if (CurrentRoute != Constants.OutofCorrespondence) {
				if (UnlockRoute[CurrentRoute]) {
					AdvanceLockProcessor.get(LogicLocks.CONFLICTINGSIGNALLOCK).requestLockSet();
				}
				Frog[CurrentRoute].foulRoute();  // clears frog locks
				if (Frog[CurrentRoute].PeerLogic != null) {
					clearAdvanceLock(LogicLocks.ROUTELOCK);
					clearAdvanceLock(LogicLocks.PROTECTIONLOCK);
					Frog[CurrentRoute].PeerLogic.setAdvanceLock(LogicLocks.CONFLICTINGSIGNALLOCK);
					FedCircuit.setApproachTrackCircuit(null);
				}
			}
			CurrentRoute = selection;
			ExpectedMakeEvent = Constants.OutofCorrespondence;
			if (CurrentRoute != Constants.OutofCorrespondence) {
				if (UnlockRoute[CurrentRoute]) {
					AdvanceLockProcessor.get(LogicLocks.CONFLICTINGSIGNALLOCK).requestLockClear();
				}
				FedCircuit.setApproachTrackCircuit(Frog[CurrentRoute].lineRoute(ApproachCircuit));
				// lineRoute() invoked mergeFrogSpeed() and set its locks
				Frog[CurrentRoute].mergeOpposingSpeed(MergedSpeed);
				if (!AdvanceLocks.contains(LogicLocks.CONFLICTINGSIGNALLOCK) && (Frog[CurrentRoute].PeerLogic != null)) {
					Frog[CurrentRoute].PeerLogic.clearAdvanceLock(LogicLocks.CONFLICTINGSIGNALLOCK);
				}
			}
		}
	}

	/**
	 * tells the VitalLogic that a command will be sent to move the points to a
	 * new route, so the VitalLogic should expect a feedback command that the points
	 * have been moved to that route
	 * @param next is the route
	 */
	public void setNextRoute(final int next) {
		ExpectedMakeEvent = next;
	}
	
	/**
	 * has 2 tasks:
	 * 1. identify the expected feedback events as a result of the points moving
	 * 2. issue the movement command
	 * @param destination is the track index for the movement command.  It must be 0 - 3
	 */
	private void movePoints(final int destination) {
		RouteInfo.FEEDBACK feedback = Points.getfeedbackEventCount(CurrentRoute, ExpectedMakeEvent);
		switch (feedback) {
		case EXACT_FEEDBACK:
			ExpectedMakeEvent = destination;
			ExpectedBreakEvent = CurrentRoute;
			break;
		case POSITIVE_FEEDBACK:
			ExpectedMakeEvent = destination;
			ExpectedBreakEvent = Constants.OutofCorrespondence;
			break;
		default:
			ExpectedMakeEvent = ExpectedBreakEvent = Constants.OutofCorrespondence;
		}
		Points.selectRoute(destination);
	}
	
	/**
	 * this method accepts and processes the feedback event generated when the points move into place
	 * on a route.  Its function is to stimulate connecting the linkages between the points, frogs, and
	 * other things in approach and to change the panel presentation.  "Make" events may be expected
	 * as a result of an action from the GUI.  They may also be unexpected due to a field action.  For the latter,
	 * command override may be desired to provide dispatcher locking of points.  In which case, the vital logic
	 * moves the points back to the CTC sanctioned position.  This can get complicated because the command's
	 * "known state" may not have changed with the field action.  Which means the command device must be set to
	 * the opposite polarity before the correct command can be issued, again.
	 * 
	 * @param leg is the index of track for which the points are now lined.
	 */
	public void acceptMakeEvent(final int leg) {
		if (leg == ExpectedMakeEvent) {
			if (ExpectedBreakEvent == Constants.OutofCorrespondence) {
				// no more events from layout expected
				receiveFeedback(ExpectedMakeEvent, true);
			}
			// else hold off on feedback for second event
			ExpectedMakeEvent = Constants.OutofCorrespondence;
		}
		else if (UnexpectedSecondEvent == leg) {
			// this is the second of two for exact feedback
			if (reverseLocalTest()) {

			}
			else {
				Transmitter.createPacket(CodePurpose.INDICATION, Codes.SWITCHPOSITION, String.valueOf(leg));
				setSelectedTrack(leg);
			}
		}
		else {
			if (RouteInfo.FEEDBACK.EXACT_FEEDBACK == Points.getfeedbackEventCount(CurrentRoute, leg)) {
				UnexpectedSecondEvent = CurrentRoute;
			}
			else if (reverseLocalTest()) {
				
			}
			else {
				
			}
		}
	}
	
	
	/**
	 * this method accepts and processes the feedback event generated when the points move out of place
	 * on a route.  Like its cousin on a "make" event, its function is to stimulate connecting the linkages
	 * between the points, frogs, and other things in approach and to change the panel presentation.
	 * "Break" events may be expected as a result of an action from the GUI.  They may also be unexpected
	 * due to a field action.  For the latter, command override may be desired to provide dispatcher locking
	 * of points.  In which case, the vital logic moves the points back to the CTC sanctioned position.
	 * 
	 * An added complication is that "break" events are optional.  They are generated only by points using
	 * "exact feedback".  Furthermore, though the expected and usual order of events is "break" before "make",
	 * this cannot be guaranteed.  It all depends upon how things are wired up.  The events may occur
	 * "simultaneously" (in a macro sense), but due to how they are reported through JMRI, they maybe presented
	 * to CATS in either order.  That situation is also handled.
	 * 
	 * @param leg is the index of track for which the points are now lined.
	 */
	public void acceptBreakEvent(final int leg) {
		
	}
	
	/**
	 * this is a predicate for testing if points movement due to a local (field) operation should
	 * be reversed.  A pre-condition to invoking this routine includes receiving unexpected feedback from
	 * points movement.  There are many conditions to check for before allowing the local operation to be
	 * countermanded and they are collected here.
	 * @return true if the local operations should be overridden.
	 */
	private boolean reverseLocalTest() {
		if (LocalEnforcement.TheEnforcementType.getFlagValue()
				&& !MaintenanceLock.isLockSet()
				&& (MyDiscipline.equals(Discipline.CTC) || MyDiscipline.equals(Discipline.DTC))) {
			return true;
		}
		return false;
	}
	/**
	 * is a predicate to query the interlocking status of the points
	 * for movement from a dispatcher request.  Almost any of the AdvanceLocks
	 * (except for Detection) will block a dispatcher request.  And, local
	 * detection will also block the request.
	 * @return true if the dispatcher can safely move the points and
	 * false if they are locked into position.
	 */
	public boolean checkDispatcherLocks() {
		return LogicLocks.intersection(MergedGlobalLocks, LogicLocks.SwitchLocked).isEmpty()  &&
				!MyLocks.contains(LogicLocks.DETECTIONLOCK) && LogicLocks.intersection(AdvanceMergedLocks, LogicLocks.SwitchLocked).isEmpty();

	}
	
	/**
	 * is a predicate to query the interlocking status of the points
	 * for movement from a local request.
	 * @return true if the local crew can safely move the points and
	 * false if they are locked into position.
	 */
	public boolean checkLocalLocks() {
		return MaintenanceLock.isLockSet() || (LogicLocks.intersection(MergedGlobalLocks, LogicLocks.LocalSwitchLocked).isEmpty() && 
				LogicLocks.intersection(AdvanceMergedLocks, LogicLocks.LocalSwitchLocked).isEmpty());
	}
	
	/**********************************************************************************
	 * The following methods are analogs to those with similar names in BasicVitalLogic;
	 * however, they are for relaying lock changes from the FrogVitalLogic
	 **********************************************************************************/
	
	/**
	 * accepts the speed from the tracks in advance to (protected by) the
	 * frogs and passes it along to the VitalLogic in approach
	 * of the points.
	 * @param speed is the speed of the tracks in advance
	 * of the frog
	 */
	void mergeFrogSpeed(final int speed) {
		if (ApproachLogic != null) {
			ApproachLogic.mergeSpeeds(Track.getSlower(Speed, speed));
		}
	}
	
	/**********************************************************************************
	 * the PtsVitalLogic methods
	 **********************************************************************************/
	/**********************************************************************************
	 * The LockProcessor for local locks that propagate in both directions.
	 **********************************************************************************/
	public class PtsLockDecorator extends DefaultProcessorDecorator {

		/**
		 * the constructor
		 * @param processor is the lock processor being decorated
		 */
		public PtsLockDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
			AdvanceLockProcessor.get(Lock).requestLockSet();
		}

		@Override
		public void requestLockClear() {
			Processor.requestLockClear();
			AdvanceLockProcessor.get(Lock).requestLockClear();
		}
		
		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
		}
	}
	/**************************************************************************
	 *  end of PtsLockDecorator
	 **************************************************************************/
	
	/**********************************************************************************
	 * The generic lock processor decorator for propagating Points lock
	 * changes to the Peer of all Frogs.  This bypasses the Frog locks!
	 **********************************************************************************/
	private class BlastPtsLockDecorator extends DefaultProcessorDecorator {

		/**
		 * the constructor
		 * @param lock is the lock being processed
		 */
		public BlastPtsLockDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
			setAllFrogsLock();
		}

		@Override
		public void requestLockClear() {
			Processor.requestLockClear();
			clearAllFrogsLock();
		}

		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
			setAllFrogsLock();
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
			clearAllFrogsLock();
		}

		/**
		 * sets this lock in the Peer of all Frogs
		 */
		private void setAllFrogsLock() {
			for (int frog = 0; frog < Frog.length; frog++) {
				if (Frog[frog] != null) {
					if ((Frog[frog].PeerLogic != null)) {
						Frog[frog].PeerLogic.setAdvanceLock(Lock);
					}
				}
			}
		}

		/**
		 * clears this lock in the Peer of all Frogs
		 */
		private void clearAllFrogsLock() {
			for (int frog = 0; frog < Frog.length; frog++) {
				if (Frog[frog] != null) {
					if ((Frog[frog].PeerLogic != null)) {
						Frog[frog].PeerLogic.clearAdvanceLock(Lock);
					}
				}
			}
		}
	}
	/**************************************************************************
	 *  end of MergedPtsLockDecorator
	 **************************************************************************/

	/**********************************************************************************
	 * The generic lock processor decorator for propagating Points lock
	 * changes to the Peer of only the lined Frog.
	 **********************************************************************************/
	private class SinglePtsLockDecorator extends DefaultProcessorDecorator {

		public SinglePtsLockDecorator(LockRequestProcessor processor) {
			super(processor);
		}
	
		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
			setFrogLock();
		}

		@Override
		public void requestLockClear() {
			clearFrogLock();
			Processor.requestLockClear();
		}

		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
			setFrogLock();
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
			clearFrogLock();
		}
		
		/**
		 * sets the AdvanceLock on the Peer of the lined Frog
		 * (if there is one)
		 */
		private void setFrogLock() {
			if (CurrentRoute != Constants.OutofCorrespondence) {
				if ((Frog[CurrentRoute].PeerLogic != null)) {
					Frog[CurrentRoute].PeerLogic.setAdvanceLock(Lock);
				}
			}
		}
		
		/**
		 * clears the AdvanceLockon the Peer of the lined Frog
		 * (if there is one)
		 */
		private void clearFrogLock() {
			if (CurrentRoute != Constants.OutofCorrespondence) {
				if ((Frog[CurrentRoute].PeerLogic != null)) {
					Frog[CurrentRoute].PeerLogic.clearAdvanceLock(Lock);
				}
			}			
		}
	}
	/**************************************************************************
	 *  end of MergedPtsLockDecorator
	 **************************************************************************/

	/**********************************************************************************
	 * The specific lock processor for PtsVitalLogic that handles the maintenance lock.
	 * It controls the Lock/Unlock light.
	 **********************************************************************************/
	private class MaintenanceLockDecorator extends DefaultProcessorDecorator {

		/**
		 * the constructor
		 */
		public MaintenanceLockDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			IOSpec light;
			if (Points != null) {
				if ((light = Points.getLockCmds(SwitchPoints.UNLOCKONCMD)) != null) {
					light.sendCommand();
				}
				if ((light = Points.getLockCmds(SwitchPoints.LOCKONCMD)) != null) {
					light.sendUndoCommand();
				}
			}
			Processor.requestLockSet();
		}

		@Override
		public void requestLockClear() {
			IOSpec light;
			if (Points != null) {
				if ((light = Points.getLockCmds(SwitchPoints.UNLOCKONCMD)) != null) {
					light.sendUndoCommand();
				}
				if ((light = Points.getLockCmds(SwitchPoints.LOCKONCMD)) != null) {
					light.sendCommand();
				}
			}
			Processor.requestLockClear();
		}
	}
	/**************************************************************************
	 *  end of MaintenanceLockDecorator
	 **************************************************************************/
	/**************************************************************************
	 * These lock request processors are similar to the ones in BasicVitalLogic,
	 * but for locks generated by the FrogVitalLogic advance VitalLogic and
	 * sent to the PtsVitalLogic approach VitalLogic.
	 **************************************************************************/
	private class SimpleReverseLockRequestProcessor extends DefaultRequestLockProcessor {

		/**
		 * the ctor
		 * @param lock is the LogicLock being transported
		 */
		public SimpleReverseLockRequestProcessor(LogicLocks lock) {
			super(lock);
		}

		/**
		 * set through local action
		 */
		public void requestLockSet() {
		}

		/**
		 * clear through local action
		 */
		public void requestLockClear() {
		}

		/**
		 * set through Frog action
		 */
		public void advanceLockSet() {
			AdvanceMergedLocks.add(Lock);
			AdvanceAdvanceLocks.add(Lock);
			if (ApproachLogic != null) {
				ApproachLogic.setAdvanceLock(Lock);
			}
		}

		/**
		 * clear through Frog action
		 */
		public void advanceLockClear() {
			AdvanceMergedLocks.remove(Lock);
			AdvanceAdvanceLocks.remove(Lock);
			if (ApproachLogic != null) {
				ApproachLogic.clearAdvanceLock(Lock);
			}
		}
	}

	/**************************************************************************
	 * These lock request processors are similar to the ones in BasicVitalLogic,
	 * but for locks generated by the FrogVitalLogic advance VitalLogic and
	 * sent to the PtsVitalLogic approach VitalLogic.  It is for those locks
	 * that can be generated by either the PtVitalLogic or the FrogVitalLogic.
	 **************************************************************************/
	private class ReverseLockRequestProcessor extends DefaultRequestLockProcessor {

		/**
		 * the ctor
		 * @param lock is the LogicLock being transported
		 */
		public ReverseLockRequestProcessor(LogicLocks lock) {
			super(lock);
		}

		/**
		 * set through local action
		 */
		public void requestLockSet() {
			if (!AdvanceAdvanceLocks.contains(Lock)) {
				AdvanceMergedLocks.add(Lock);
				if (ApproachLogic != null) {
					ApproachLogic.setAdvanceLock(Lock);
				}
			}
			AdvanceMyLocks.add(Lock);
		}

		/**
		 * clear through local action
		 */
		public void requestLockClear() {
			if (!AdvanceAdvanceLocks.contains(Lock)) {
				AdvanceMergedLocks.remove(Lock);
				if (ApproachLogic != null) {
					ApproachLogic.clearAdvanceLock(Lock);
				}
			}
			AdvanceMyLocks.remove(Lock);
		}

		/**
		 * set through Frog action
		 */
		public void advanceLockSet() {
			if (!AdvanceMyLocks.contains(Lock)) {
				AdvanceMergedLocks.add(Lock);
				if (ApproachLogic != null) {
					ApproachLogic.setAdvanceLock(Lock);
				}
			}
			AdvanceAdvanceLocks.add(Lock);
		}

		/**
		 * clear through Frog action
		 */
		public void advanceLockClear() {
			if (!AdvanceMyLocks.contains(Lock)) {
				AdvanceMergedLocks.remove(Lock);
				if (ApproachLogic != null) {
					ApproachLogic.clearAdvanceLock(Lock);
				}
			}
			AdvanceAdvanceLocks.remove(Lock);
		}
	}
}
/* @(#)PtsVitalLogic.java */
