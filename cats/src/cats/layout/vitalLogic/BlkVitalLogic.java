/* Name: BlkVitalLogic.java
 *
 * What:
 * This file contains the definition for the VitalLogic sub-class associated with the block
 * boundary end of a section of track.  The track has a BlkEdge
 * at one end and any other kind of edge at the other.  The neighbor Vital Logic is always an instance
 * (or derivative) of BlkEdge.
 * 
 * There are two dimensions to BlkVitalLogic:
 * <ul>
 * <li>
 * The discipline - ABS, APB, CTC, or DTC.  When the BlkVitalLogic is alerted that the
 * protected Block is occupied, it needs to know how to process that alert.  It must note the
 * occupancy and merge it into any composite state.  However, for APB, it may also stimulate generation
 * of a pseudo route.  This requires determining the train's direction of travel.  Other disciplines
 * may be able to take advantage of knowing direction of travel, so direction is always
 * computed.  Direction of travel may not affect the signals in other disciplines.
 * </li>
 * <li>
 * The second dimension is when the BlkEnd contains an intermediate signal or a control
 * point.  There is a lot of common processing.  This begs for multiple inheritance, not
 * supported in Java.
 * </li>
 * </ul>
 * <p>
 * Neighboring BlkEdges are "welded" together to form a single block from the standpoint of
 * containing locks.  Chubb logic for APB traffic stick, signal propagation, traffic circuits,
 * etc. is handled between signals.
 * @see cats.layout.items.BlkEdge
 */
package cats.layout.vitalLogic;

import cats.layout.Discipline;
import cats.layout.codeLine.packetFactory.PacketFactory;
import cats.trackCircuit.OverlapCircuit;
import cats.trackCircuit.TrackCircuit;

/**
 * This file contains the definition for the VitalLogic sub-class associated with the block
 * boundary end of a section of track.  The track has a BlkEdge
 * at one end and any other kind of edge at the other.  The neighbor Vital Logic is always an instance
 * (or derivative) of BlkEdge.
 * 
 * There are two dimensions to BlkVitalLogic:
 * <ul>
 * <li>
 * The discipline - ABS, APB, CTC, or DTC.
 * </li>
 * <li>
 * The second dimension is when the BlkEnd contains an intermediate signal or a control
 * point.  There is a lot of common processing.  This begs for multiple inheritance, not
 * supported in Java.
 * </li>
 * </ul>
 * <p>
 * Neighboring BlkEdges are "welded" together to form a single block from the standpoint of
 * containing locks.  Chubb logic for APB traffic stick, signal propagation, traffic circuits,
 * etc. is handled between signals.
 * @see cats.layout.items.BlkEdge
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014, 2015, 2016, 2018, 2019, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class BlkVitalLogic extends BasicVitalLogic {

	/**
	 * the possible values for the Stick Relay:
	 * ENTRYEDGE means that a train is expected to enter/has entered protected track through this VitalLogic
	 * EXITEDGE means a train is expected to exit/has exited protected track through this vital logic
	 * NODIRECTION means a train is not expected to cross the VitalLogic
	 * In Chubb's algorithms, the StickValue has only two states: NODIRECTION and ENYTRYEDGE;
	 * thus, this could be a boolean.
	 */
	public enum StickValue {ENTRYEDGE, EXITEDGE, NODIRECTION};
	
	/**
	 * the neighbor VitalLogic, coerced to be a BlkVitalLogic (or derivative)
	 */
	protected BlkVitalLogic NeighborBlk;
	
//	/**
//	 * the neighbor VitalLogic is an intermediate
//	 */
//	protected boolean NeighborIntermediate = false;
	
//	/**
//	 * is true if the neighbor Vital Logic is a Control Point
//	 */
//	protected boolean NeighborCP = false;
	
	/**
	 * the ctor
	 * @param transmitter is the factory for creating and sneding indications to the GUI
	 * @param id is the identity of the BlkVitalLogic
	 */
	public BlkVitalLogic(final PacketFactory transmitter, final String id) {
		super(transmitter, id);
	}

	@Override
//	public void configureLockProcessing() {
//		NeighborBlk = (BlkVitalLogic) NeighborLogic;
//		
//		LockProcessor.put(LogicLocks.DETECTIONLOCK,
//				new LocalCodeLockDecorator(new MergedRequestLockProcessor(LogicLocks.DETECTIONLOCK)));
////		LockProcessor.put(LogicLocks.CONFLICTINGSIGNALLOCK, new AdvanceRequestLockProcessor(LogicLocks.CONFLICTINGSIGNALLOCK));
//		LockProcessor.put(LogicLocks.CONFLICTINGSIGNALLOCK, new DefaultRequestLockProcessor(LogicLocks.CONFLICTINGSIGNALLOCK));
//		// HOLD is not used by a simple BlkEdge
////		LockProcessor.put(LogicLocks.SWITCHUNLOCK, new AdvanceRequestLockProcessor(LogicLocks.SWITCHUNLOCK));
//		LockProcessor.put(LogicLocks.SWITCHUNLOCK, new DefaultRequestLockProcessor(LogicLocks.SWITCHUNLOCK));
//		// APPROACH is not used by a simple BlkEdge
////		LockProcessor.put(LogicLocks.TIMELOCK, new AdvanceRequestLockProcessor(LogicLocks.TIMELOCK));
//		LockProcessor.put(LogicLocks.TIMELOCK, new DefaultRequestLockProcessor(LogicLocks.TIMELOCK));
//		// TRAFFIC is not used by a simple BlkEdge
//		// ROUTE depends upon the type of the neighbor - see below
////		LockProcessor.put(LogicLocks.ROUTELOCK, new AdvanceRequestLockProcessor(LogicLocks.ROUTELOCK));
//		LockProcessor.put(LogicLocks.ROUTELOCK, new DefaultRequestLockProcessor(LogicLocks.ROUTELOCK));
//		// PROTECTION depends upon the type of the neighbor - see below
////		LockProcessor.put(LogicLocks.PROTECTIONLOCK, new AdvanceRequestLockProcessor(LogicLocks.PROTECTIONLOCK));
//		LockProcessor.put(LogicLocks.ROUTELOCK, new DefaultRequestLockProcessor(LogicLocks.ROUTELOCK));
//		LockProcessor.put(LogicLocks.MAINTENANCELOCK, new MergedRequestLockProcessor(LogicLocks.MAINTENANCELOCK));
//		// CALLON is not used by a simple BlkEdge
//		// FLEET should be handled like ROUTE, but without changing when the neighbor is a CP
////		LockProcessor.put(LogicLocks.FLEETLOCK, new AdvanceRequestLockProcessor(LogicLocks.FLEETLOCK));
//		LockProcessor.put(LogicLocks.FLEETLOCK, new DefaultRequestLockProcessor(LogicLocks.FLEETLOCK));
//		LockProcessor.put(LogicLocks.EXTERNALLOCK, new LocalCodeLockDecorator(new MergedRequestLockProcessor(LogicLocks.EXTERNALLOCK)));
//
//		if (NeighborBlk != null) {
//			if (!MyDiscipline.equals(Discipline.ABS) && !MyDiscipline.equals(Discipline.UNDEFINED)) {
//				LockProcessor.put(LogicLocks.DETECTIONLOCK, new MergedLockPropagationDecorator(LockProcessor.get(LogicLocks.DETECTIONLOCK)));
//				LockProcessor.put(LogicLocks.CONFLICTINGSIGNALLOCK, new MergedLockPropagationDecorator(LockProcessor.get(LogicLocks.CONFLICTINGSIGNALLOCK)));
//				// HOLD is not used by a simple BlkEdge
//				LockProcessor.put(LogicLocks.SWITCHUNLOCK, new MergedLockPropagationDecorator(LockProcessor.get(LogicLocks.SWITCHUNLOCK)));
//				// APPROACH is not used by a simple BlkEdge
//				LockProcessor.put(LogicLocks.TIMELOCK, new MergedLockPropagationDecorator(LockProcessor.get(LogicLocks.TIMELOCK)));
//				LockProcessor.put(LogicLocks.PROTECTIONLOCK, 
//						new MergedLockPropagationDecorator(LockProcessor.get(LogicLocks.PROTECTIONLOCK)));
//				LockProcessor.put(LogicLocks.MAINTENANCELOCK, new MergedLockPropagationDecorator(LockProcessor.get(LogicLocks.MAINTENANCELOCK)));
//				// CALLON is not used by a simple BlkEdge
//				// TRAFFICSTICK is not used by a simple BlkEdge
//				LockProcessor.put(LogicLocks.EXTERNALLOCK, new MergedLockPropagationDecorator(LockProcessor.get(LogicLocks.EXTERNALLOCK)));
//
//				if (NeighborBlk instanceof CPVitalLogic) {
//					NeighborCP = true;
//					LockProcessor.put(LogicLocks.ROUTELOCK, new MergedLockPropagationDecorator(new RouteLockConverter()));
//				}
//				else if (NeighborBlk instanceof IntermediateVitalLogic) {
//					NeighborIntermediate = true;
//					if (NeighborBlk.MyDiscipline.equals(Discipline.CTC) || NeighborBlk.MyDiscipline.equals(Discipline.DTC)) {
//						LockProcessor.put(LogicLocks.ROUTELOCK, 
//								new AdvanceLockPropagationDecorator(new RouteLockDecorator(new HoldNeighborDecorator(LockProcessor.get(LogicLocks.ROUTELOCK)))));
//						LockProcessor.put(LogicLocks.FLEETLOCK, new AdvanceLockPropagationDecorator(LockProcessor.get(LogicLocks.FLEETLOCK)));
//					}
//				}
//				else {
//					LockProcessor.put(LogicLocks.ROUTELOCK, 
//							new AdvanceLockPropagationDecorator(new RouteLockDecorator(LockProcessor.get(LogicLocks.ROUTELOCK))));
//					LockProcessor.put(LogicLocks.FLEETLOCK, new AdvanceLockPropagationDecorator(LockProcessor.get(LogicLocks.FLEETLOCK)));
//				}
//			}
//			if ((NeighborBlk instanceof IntermediateVitalLogic) || (NeighborBlk instanceof CPVitalLogic)) {
//				LockProcessor.put(LogicLocks.DETECTIONLOCK, new NeighborApproachLockDecorator(LockProcessor.get(LogicLocks.DETECTIONLOCK)));				
//			}
//		}
//	}
	public void configureLockProcessing() {
		NeighborBlk = (BlkVitalLogic) NeighborLogic;
		
		LockRequestProcessor processor;
		// DETECTIONLOCK
		if (NeighborBlk != null) {
			processor = new LockPropagationDecorator(
					new MergedRequestLockProcessor(LogicLocks.DETECTIONLOCK));
			processor = new MergedLockFilterDecorator(processor);
//			if ((NeighborBlk instanceof IntermediateVitalLogic) || (NeighborBlk instanceof CPVitalLogic)) {
			if (NeighborBlk instanceof IntermediateVitalLogic) {
				processor = new NeighborApproachLockDecorator(processor);				
			}
		}
		else {
			processor = new DefaultRequestLockProcessor(LogicLocks.DETECTIONLOCK);
		}
		LockProcessor.put(LogicLocks.DETECTIONLOCK,
				new LockFilterDecorator(
						new LocalCodeLockDecorator(
								new CommonDetectionDecorator(processor))));

		// CONFLICTINGSIGNALLOCK
		if (NeighborBlk != null) {
//			LockProcessor.put(LogicLocks.CONFLICTINGSIGNALLOCK, 
//					new MyLocksFilterDecorator(
//							new LockPropagationDecorator(
//									new DefaultRequestLockProcessor(LogicLocks.CONFLICTINGSIGNALLOCK))));
			installAdvancePropagationProcessor(LogicLocks.CONFLICTINGSIGNALLOCK);
		}

		// HOLDLOCK - never should be set/cleared
		// SWITCHUNLOCK
		if (NeighborBlk != null) {
//			LockProcessor.put(LogicLocks.SWITCHUNLOCK, 
//					new MyLocksFilterDecorator(
//							new LockPropagationDecorator(
//									new DefaultRequestLockProcessor(LogicLocks.SWITCHUNLOCK))));
			installAdvancePropagationProcessor(LogicLocks.SWITCHUNLOCK);
		}
		// APPROACHLOCK - never should be set/cleared
		// TIMELOCK
		if (NeighborBlk != null) {
//			LockProcessor.put(LogicLocks.TIMELOCK, 
//					new MyLocksFilterDecorator(
//							new LockPropagationDecorator(
//									new DefaultRequestLockProcessor(LogicLocks.TIMELOCK))));
			installAdvancePropagationProcessor(LogicLocks.TIMELOCK);
		}

		// TRAFFICLOCK - never should be set/cleared
		// ROUTELOCK
		if ((NeighborBlk != null) && !MyDiscipline.equals(Discipline.ABS) && !MyDiscipline.equals(Discipline.UNDEFINED)) {
			//			processor = new MergedLockFilterDecorator(
			//					new LockPropagationDecorator(
			//							new MergedRequestLockProcessor(LogicLocks.ROUTELOCK)));
			//			if  ((NeighborBlk instanceof IntermediateVitalLogic) && 
			//					(NeighborBlk.MyDiscipline.equals(Discipline.CTC) || NeighborBlk.MyDiscipline.equals(Discipline.DTC))) {
			//				processor = new LockFilterDecorator(
			//						new NeighborHoldDecorator(processor));
			//			}
			processor = new MergedRequestLockProcessor(LogicLocks.ROUTELOCK);
			if  (!NeighborBlk.MyDiscipline.equals(Discipline.ABS) && !NeighborBlk.MyDiscipline.equals(Discipline.UNDEFINED)) {
				if (!(NeighborBlk instanceof CPVitalLogic)) {
					processor = new LockPropagationDecorator(processor);
					if (NeighborBlk instanceof IntermediateVitalLogic) {
						processor = new NeighborHoldDecorator(processor);
					}
				}
			}
			LockProcessor.put(LogicLocks.ROUTELOCK, 
					new MergedLockFilterDecorator(processor));
		}
		else {
			LockProcessor.put(LogicLocks.ROUTELOCK, new DefaultRequestLockProcessor(LogicLocks.ROUTELOCK));
		}

		// PROTECTIONLOCK
		if ((NeighborBlk != null) && !MyDiscipline.equals(Discipline.ABS) && !MyDiscipline.equals(Discipline.UNDEFINED)) {
//			LockProcessor.put(LogicLocks.PROTECTIONLOCK,
//					new MergedLockFilterDecorator(
//							new LockPropagationDecorator(
//									new MergedRequestLockProcessor(LogicLocks.PROTECTIONLOCK))));
			installMergePropagationProcessor(LogicLocks.PROTECTIONLOCK);
		}
		else {
			LockProcessor.put(LogicLocks.PROTECTIONLOCK, new DefaultRequestLockProcessor(LogicLocks.PROTECTIONLOCK));
		}

		// MAINTENANCE
		if (NeighborBlk != null) {
//		LockProcessor.put(LogicLocks.MAINTENANCELOCK,
//				new MergedLockFilterDecorator(
//						new LockPropagationDecorator(
//								new MergedRequestLockProcessor(LogicLocks.MAINTENANCELOCK))));
			installMergePropagationProcessor(LogicLocks.MAINTENANCELOCK);
		}

		// CALLONLOCK - never should be set/cleared
		// FLEETLOCK - never should be set/cleared
//		if ((NeighborBlk != null) && !MyDiscipline.equals(Discipline.ABS) && !MyDiscipline.equals(Discipline.UNDEFINED)) {
//			installAdvancePropagationProcessor(LogicLocks.FLEETLOCK);
//		}

		// TRAFFICSTICK - never should be set or cleared
		// EXTERNALLOCK
		processor = new MergedRequestLockProcessor(LogicLocks.EXTERNALLOCK);
		if (NeighborBlk != null) {
			processor = new LockPropagationDecorator(processor);
		}
		LockProcessor.put(LogicLocks.EXTERNALLOCK,
				new LockFilterDecorator(
						new LocalCodeLockDecorator(
								new MergedLockFilterDecorator(processor))));
	}
	
	@Override
	protected TrackCircuit trackCircuitProbe() {
		TrackCircuit virtualSignal;
//		if (NeighborCP) {
		if ((NeighborBlk != null) && ((NeighborBlk instanceof CPVitalLogic))) {
			if (ApproachLogic == null) {
				return null;
			}
			virtualSignal = new OverlapCircuit(Identity);
			virtualSignal.setApproachTrackCircuit(ApproachLogic.trackCircuitProbe());
			return virtualSignal;
		}
		return super.trackCircuitProbe();
	}

//	/**************************************************************
//	 * the APB Detection Lock Processor.  It determines the Traffic Stick when
//	 * the lock is set.  If there is no neighbor, then the traffic direction
//	 * cannot be determined; therefore the Stick is never set not cleared.
//	 * 
//	 * If there is a neighbor, then the value of the neighbor's stick determines
//	 * the state of this stick.  Specifically, if the neighbor is exit, then this
//	 * stick's value is enter.  If the neighbor's stick is anything else, then
//	 * this stick is exit.  By doing it this way (as opposed to Chubb's focus
//	 * on enter), the case of unexpected occupancy in the middle of single track,
//	 * radiating in both directions to the head block, is handled naturally.
//	 * 
//	 * It should be decorated with Propagation and Code Request, based on the
//	 * discipline and edge type.  Because all detection events should be reported
//	 * to the GUI, then all instantiations should be decorated with a
//	 * LocalCodeRequestLockProcessor.  Only the local lock changes should be
//	 * sent to the GUI.
//	 * 
//	 * The advanceLock methods use the default because no local action is triggered.
//	 * If the lock processor is decorated with the Propagation decorator,
//	 * then the detection lock will flow through the advance lock sets naturally.
//	 * 
//	 **************************************************************/
//	protected class APBDetectionLockProcessor extends DefaultRequestLockProcessor {
//
//		public APBDetectionLockProcessor() {
//			super(LogicLocks.DETECTIONLOCK);
//		}
//		
//		@Override
//		public void requestLockSet() {
//			// if there is no neighbor, then there is no need to compute the traffic stick
//			if (NeighborBlk != null) {
////				if (LogicLocks.intersection(NeighborBlk.MergedGlobalLocks, LogicLocks.TrafficStickBlockers).isEmpty())  {
////					setStickDirection();
////				}
//				if (StickRelay == StickValue.NODIRECTION) {
//					if (LogicLocks.intersection(NeighborBlk.MergedGlobalLocks, LogicLocks.TrafficStickBlockers).isEmpty()) {
//						StickRelay = StickValue.EXITEDGE;
////						LockProcessor.get(LogicLocks.TUMBLEDOWN).requestLockSet();
//					}
//					else {
//						StickRelay = StickValue.ENTRYEDGE;
//					}
//				}				
//				NeighborBlk.setLocalLock(LogicLocks.APPROACHLOCK);
//			}
//			
//			//the following two tests are for the case of a route
//			if (AdvanceLocks.contains(LogicLocks.ROUTELOCK)) {
//				setLocalLock(LogicLocks.ROUTELOCK);
//			}
////			if (AdvanceLocks.contains(LogicLocks.SIGNALINDICATIONLOCK)) {
////				setLocalLock(LogicLocks.SIGNALINDICATIONLOCK);
////			}
//			super.requestLockSet();
//		}
//
//		@Override
//		public void requestLockClear() {
//			// Note: clearDetection is unprotected for checking if the Lock is already set
////			LockRequestProcessor entryLock = LockProcessor.get(LogicLocks.SIGNALINDICATIONLOCK);
//			LockRequestProcessor exitLock = LockProcessor.get(LogicLocks.ROUTELOCK);
////			LockRequestProcessor overlapLock = LockProcessor.get(LogicLocks.OPPOSINGSIGNALLOCK);
//			// unoccupancy does not clear a Vital Logic tumbledown (stick exit edge)  Unoccupancy
//			// clears the neighbor's tumbledown.  If there is no neighbor, then the stick relay was
//			// never set.
////			clearStickDirection();
//			if (NeighborBlk != null) {
//				if (!LockProcessor.get(LogicLocks.APPROACHLOCK).isLockSet()) {
//					// this clears the one block buffer behind.  StickRelay is ENTRYEDGE or NODIRECTION
//					NeighborBlk.clearStickRelay();
//				}
//				NeighborBlk.clearLocalLock(LogicLocks.APPROACHLOCK);
//			}
//			
//			//the following two tests are for the case of a route
//			if (entryLock.isLockSet()) {
//				entryLock.requestLockClear();
//			}
//			if (exitLock.isLockSet()) {
//				exitLock.requestLockClear();
//			}
////			if (overlapLock.isLockSet()) {
////				overlapLock.requestLockClear();
////			}
//			super.requestLockClear();
//		}
//
////		/**
////		 * processes occupancy on a block edge for APB.  This sets the stick
////		 * relay (if not already set) and propagates it in the direction
////		 * of train travel.  The effect is to tumble down the Red signal.
////		 */
////		protected void setStickDirection() {
////			// if the stick relay is set, then the Block occupancy
////			// is expected, so do nothing
////			if (StickRelay == StickValue.NODIRECTION) {
////				if (LockProcessor.get(LogicLocks.APPROACHLOCK).isLockSet()) {
////					StickRelay = StickValue.ENTRYEDGE;
////				}
////				else {
//////					setTrafficExit();
////					StickRelay = StickValue.EXITEDGE;
////					LockProcessor.get(LogicLocks.DIRECTIONOFTRAVEL).requestLockSet();
////				}
////			}
////		}
//		
////		/**
////		 * processes unoccupancy on a block edge for APB.  This always clears
////		 * the Stick Relay and optionally clears the neighbor's Stick Relay.
////		 * The latter is to provide the one block buffer behind that Chubb
////		 * points out.
////		 * <p>
////		 * StickRelay will be EXITEDGE or ENTRYEDGE when this method is invoked
////		 * because it is set by setDetectionLock() and traffic
////		 */
////		protected void clearStickDirection() {
////			clearStickRelay();
////			if (NeighborBlk != null) {
////				if (!LockProcessor.get(LogicLocks.APPROACHLOCK).isLockSet()) {
////					// this clears the one block buffer behind.  StickRelay is ENTRYEDGE or NODIRECTION
////					NeighborBlk.clearStickRelay();
//////					StickRelay = StickValue.NODIRECTION;
////				}
////				NeighborBlk.clearLocalLock(LogicLocks.APPROACHLOCK);
////			}
////		}
//	}
//	/**************************************************************************
//	 *  end of APBDetectionLockProcessor
//	 **************************************************************************/

//	/**************************************************************
//	 * the non-APB Detection Lock Processor.  It is used with ABS,
//	 * CTC, and DTC.  It differs from the
//	 * Default Code Lock Processor in that it sets/clears the neighbor's
//	 * Approach Lock and clears the Traffic Lock
//	 * on unoccupancy.  The reason for the latter is that a route can be created through
//	 * any block and the route uses a TrafficLock.
//	 * <p>
//	 * This lock should be decorated with a DefaultCodeRequestLockProcessor
//	 **************************************************************/
//	protected class CommonDetectionLockProcessor extends DefaultRequestLockProcessor {
//
//		public CommonDetectionLockProcessor() {
//			super(LogicLocks.DETECTIONLOCK);
//		}
//		
//		@Override
//		public void requestLockSet() {
//			if (AdvanceLocks.contains(LogicLocks.OPPOSINGSIGNALLOCK)) {
//				setLocalLock(LogicLocks.OPPOSINGSIGNALLOCK);
//			}
//			if (AdvanceLocks.contains(LogicLocks.DIRECTIONOFTRAVEL)) {
//				setLocalLock(LogicLocks.DIRECTIONOFTRAVEL);
//			}
//			if (AdvanceLocks.contains(LogicLocks.EXITTRAFFICLOCK)) {
//				setLocalLock(LogicLocks.EXITTRAFFICLOCK);
//			}
//			if (AdvanceLocks.contains(LogicLocks.SIGNALINDICATIONLOCK)) {
//				setLocalLock(LogicLocks.SIGNALINDICATIONLOCK);
//			}
//			if (NeighborBlk != null) {
//				NeighborBlk.setLocalLock(LogicLocks.APPROACHLOCK);
//			}
//			super.requestLockSet();
//		}
//
//		@Override
//		public void requestLockClear() {
//			LockRequestProcessor entryLock = LockProcessor.get(LogicLocks.SIGNALINDICATIONLOCK);
//			LockRequestProcessor exitLock = LockProcessor.get(LogicLocks.EXITTRAFFICLOCK);
//			LockRequestProcessor opposingLock = LockProcessor.get(LogicLocks.OPPOSINGSIGNALLOCK);
//			LockRequestProcessor directionLock = LockProcessor.get(LogicLocks.DIRECTIONOFTRAVEL);			
//			super.requestLockClear();
//			if (NeighborBlk != null) {
//				// this should clear the neighbor's traffic stick, if the neighbor
//				// is APB.
//				NeighborBlk.clearLocalLock(LogicLocks.APPROACHLOCK);
//			}
//			if (entryLock.isLockSet()) {
//				entryLock.requestLockClear();
//			}
//			if (exitLock.isLockSet()) {
//				exitLock.requestLockClear();
//			}
//			if (opposingLock.isLockSet()) {
//				opposingLock.requestLockClear();
//			}
//			if (directionLock.isLockSet()) {
//				directionLock.requestLockClear();
//			}
//		}
//	}
//	/**************************************************************************
//	 *  end of CommonDetectionLockProcessor
//	 **************************************************************************/
	
	/**********************************************************************************
	 * This Lock Decorator is for setting and clearing a neighbor's ApproachLock when
	 * this Safety Zone is occupied/unoccupied.  It assumes that the neighbor has been checked
	 * to exist and to contain a Signal before it is instantiated and applied.
	 * 
	 * It must not be decorated by a MergeLockFilterDecorator because the set methods behave like
	 * merged; the clear advance behaves like merge; but, the clear local must always
	 * be executed.  This is to make approach lighting and the red signal behind a train
	 * work correctly.  It duplicates 3/4 of the merge filtering.  It decorates only the
	 * DetectionLock.
	 **********************************************************************************/
	protected class NeighborApproachLockDecorator extends DefaultProcessorDecorator {

		/**
		 * the constructor
		 */
		public NeighborApproachLockDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
			if (!AdvanceLocks.contains(LogicLocks.DETECTIONLOCK)) {
				NeighborLogic.setLocalLock(LogicLocks.APPROACHLOCK);
			}
		}

		@Override
		public void requestLockClear() {
			Processor.requestLockClear();
			NeighborLogic.clearLocalLock(LogicLocks.APPROACHLOCK);
		}

		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
			if (!MyLocks.contains(LogicLocks.DETECTIONLOCK)) {
				NeighborLogic.setLocalLock(LogicLocks.APPROACHLOCK);
			}
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
			if (!MyLocks.contains(LogicLocks.DETECTIONLOCK)) {
				NeighborLogic.clearLocalLock(LogicLocks.APPROACHLOCK);
			}
		}
	}
	/**************************************************************************
	 *  end of NeighborApproachLockDecorator
	 **************************************************************************/
	
	/**********************************************************************************
	 * This Lock Decorator is for edges whose neighbor is a CTC Intermediate.
	 * It sets or clears the HOLD lock on the Intermediate.
	 * 
	 * It is appropriate for MyLocks only.
	 **********************************************************************************/
	protected class NeighborHoldDecorator extends DefaultProcessorDecorator {
		/**
		 * the constructor
		 */
		public NeighborHoldDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
			NeighborBlk.clearLocalLock(LogicLocks.HOLDLOCK);
		}

		@Override
		public void requestLockClear() {
			Processor.requestLockClear();
			NeighborBlk.setLocalLock(LogicLocks.HOLDLOCK);
		}

		@Override
		public void advanceLockSet() {
			Processor.requestLockSet();
			NeighborBlk.clearLocalLock(LogicLocks.HOLDLOCK);
		}

		@Override
		public void advanceLockClear() {
			Processor.requestLockClear();
			NeighborBlk.setLocalLock(LogicLocks.HOLDLOCK);
		}
}
	/**************************************************************************
	 *  end of NeighborHoldDecorator
	 **************************************************************************/
	/**********************************************************************************
	 * This LockRequestProcessor is a helper for setting the PROTECTIONLOCK.  It comes
	 * into play on a non-CP block end, when the neighbor is a control point.  It is
	 * installed on the route exit lock (ROUTELOCK).  It converts the ROUTELOCK
	 * to a PROTECTIONLOCK for propagation in the approach direction to force
	 * approach signals to STOP and prevent the dispatcher from setting an opposing
	 * route.
	 * <p>
	 * It replaces a PropagationRequestLockProcessor.  The ROUTELOCK
	 * stops at this Vital Logic.  However, it tells the decorator
	 * to forward a PROTECTIONLOCK, thus, performing the conversion in a rather devious
	 * manner.
	 * <p>
	 * This class is written to be general; thus, could be used in other situations,
	 * if needed
	 **********************************************************************************/
	protected class LockConverter extends AbstractProcessorDecorator {
		private final LogicLocks Converted;
		public LockConverter(LockRequestProcessor processor, LogicLocks converted) {
			super(processor);
			Converted = converted;
		}
		
		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
			setApproachLogicLock(Converted);
		}

		@Override
		public void requestLockClear() {
			Processor.requestLockClear();
			clearApproachLogicLock(Converted);
		}

		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
			setApproachLogicLock(Converted);
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
			clearApproachLogicLock(Converted);
		}

	}
	/**************************************************************************
	 *  end of RouteLockConverter
	 **************************************************************************/
}
/* @(#)BlkVitalLogic.java */
