/* Name LogicLocks.java
 *
 * What:
 *  This class holds an enumeration defining the various kinds of locks that can affect
 *  Signals and Switches.  They correspond to relays in the vital logic (field equipment).
 *  <p>
 *  There are three classes of locks:
 *  <ol>
 *  <li>intra-block.  Intra-block signals are confined to a single block.  They do not
 *   propagate outside of the block.
 *  </li>
 *  <li>intra-signal.  Intra-signal locks propagate to the nearest signal in both directions.
 *   They span blocks.
 *  </li>
 *  <li>intra-route.  Intra-route locks are uni-directional and propagate to the next signal,
 *   in approach.
 *  </li>
 *  </ol>
 *  The MergedGlobalLocks EnumSet<LogicLocks> is the vehicle for propagating both
 *  intra-signal and intra-route locks.
 */
package cats.layout.vitalLogic;

import java.util.EnumSet;

/**
 *  This class holds an enumeration defining the various kinds of locks that can affect
 *  Signals and Switches.  They correspond to relays in the vital logic (field equipment).
 *  <p>
 *  There are three classes of locks:
 *  <ol>
 *  <li>intra-block.  Intra-block locks are confined to a single block.  They do not
 *   propagate outside of the block.
 *  </li>
 *  <li>intra-signal.  Intra-signal locks propagate to the nearest signal in both directions.
 *   They span blocks.
 *  </li>
 *  <li>intra-route.  Intra-route locks are uni-directional and propagate to the next signal,
 *   in approach.
 *  </li>
 *  </ol>
 *  The MergedGlobalLocks EnumSet&lt;LogicLocks&gt; is the vehicle for propagating both
 *  intra-signal and intra-route locks.
 * Title: CATS - Crandic Automated Traffic System
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2014, 2015, 2016, 2017, 2018, 2019, 2022
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public enum LogicLocks {
	/** (x00001) the track circuit reports occupied.  Intra-signal */
	DETECTIONLOCK,
//	/** (x00002) an opposing signal has been cleared */
//	OPPOSINGSIGNALLOCK,
	/** (x00002) a conflicting signal has been cleared (crossing) or a switch is fouling.  Intra-signal  */
	CONFLICTINGSIGNALLOCK,
//	/**(x00008) set means the dispatcher has locked a switch (i.e CTC without track and time or OOS)  */
//	REMOTELOCK,
	/** (x00004) the signal is held at STOP on CTC or DTC because no route has been lined.  It is
	 * logically equivalent to !TRAFFICLOCK, but that complicates testing for the STOP condition.
	 * Intra-signal
	 */
	HOLDLOCK,
	/** (x00008) a controlled electric lock or automatic electric lock switch has been unlocked in the field.
	 * Intra-signal
	 */
	SWITCHUNLOCK,
//	/** (x00040) a route through a traffic circuit exits on this edge (Stick Relay) */
//	TUMBLEDOWN,
	/** (x00010) the track circuit on the signal approach is occupied.  Intra-block */
	APPROACHLOCK,
	/** (x00020) a signal protecting the track circuit is running time. Intra-signal */
	TIMELOCK,
	/** (x00040) a route has been reserved through the interlocking.  This is sometimes
	 * called RouteLock - local lock only (is not merged into the global locks for the
	 * approach signal), but is converted to ROUTELOCK in the opposing lock global lock flow.
	 * This lock is used on the entry end only.  Eventually, intra-route
	 */
//	SIGNALINDICATIONLOCK,
	TRAFFICLOCK,
	/** (x00080) a route has been reserved through the track circuit.  This lock is used
	 * on the exit end.  It starts on the exit end of the block the dispatcher set
	 * TRAFFICLOCK on and continues to the first control point in advance of the
	 * TRAFFICLOCK end.  Intra-route
	 */
	ROUTELOCK,
	/** (0x00100) this is a continuation of a ROUTELOCK through the first control point
	 * to the next facing control point.  This prevents a dispatcher from setting an
	 * unsafe route that looks safe because there is no opposing route on the neighbor.
	 * Intra-signal
	 */
	PROTECTIONLOCK,
	/** (0x00200) the dispatcher has given the field crew local control over a block
	 * protected by the signal (track authority or out of service).  Intra-signal
	 */
	MAINTENANCELOCK,
	/**
	 * (0x00400) the dispatcher has overridden the safety of the signal system to allow
	 * a train to pass a signal showing Stop.  Intra-block
	 */
	CALLONLOCK,
	/**
	 * (0x00800) the dispatcher has enabled fleeting entry from this edge.  Not used.
	 */
	FLEETLOCK,
	/**
	 * (0x01000) direction of traffic allows a train to enter the protected track
	 * from the adjacent block.  Not used.
	 */
	TRAFFICSTICK,
	/**
	 * (0x002000) an external sensor has forced a STOP indication.  Intra-signal
	 */
	EXTERNALLOCK;
	
	/**
	 * the set of no locks
	 */
	public final static EnumSet<LogicLocks> NoLogicLocks = EnumSet.noneOf(LogicLocks.class);
	
//	/**
//	 * the set of Locks on GlobalMergedState that prevent a reservation (CTC Route) from being set
//	 * through a control point.  DetectionLock
//	 * is not included because it would prevent fleeting one train behind another.  ROUTELOCK and
//	 * PROTECTIONLOCK are the critical locks because they prevent opposing routes.
//	 */
//	public final static EnumSet<LogicLocks> CPReservationBlockers = EnumSet.of(
//			CONFLICTINGSIGNALLOCK, SWITCHUNLOCK, TIMELOCK, ROUTELOCK, PROTECTIONLOCK, MAINTENANCELOCK, CALLONLOCK,
//			EXTERNALLOCK);
	
	/**
	 * the set of locks in a neighbor that block Traffic Stick exit
	 */
//	public final static EnumSet<LogicLocks> TrafficStickBlockers = EnumSet.of(OPPOSINGSIGNALLOCK,
//			CONFLICTINGSIGNALLOCK, TUMBLEDOWN, ROUTELOCK);
	
	/**
	 * the set of locks that force an unconditional STOP in CTC/DTC  
	 */
	public final static EnumSet<LogicLocks> CTCDTC_LOCKS = EnumSet.of(DETECTIONLOCK, 
			CONFLICTINGSIGNALLOCK, HOLDLOCK, SWITCHUNLOCK, TIMELOCK, ROUTELOCK, PROTECTIONLOCK, EXTERNALLOCK);	

	/**
	 * the set of locks that force an unconditional STOP in ABS/APB/UNDEFINED.  The difference is
	 * that HOLD is ignored.  
	 */
	public final static EnumSet<LogicLocks> ABSAPB_LOCKS = EnumSet.of(DETECTIONLOCK, 
			CONFLICTINGSIGNALLOCK, SWITCHUNLOCK, TIMELOCK, TRAFFICLOCK, ROUTELOCK, PROTECTIONLOCK, EXTERNALLOCK);


//	/**
//	 * the set of locks that get translated to OpposingSignal
//	 */
//	public final static EnumSet<LogicLocks> OpposingSignalLocks = EnumSet.of(TUMBLEDOWN, ROUTELOCK);
	
	/**
	 * the locks that prevent call-on
	 */
//	public final static EnumSet<LogicLocks> CallOnBlockers = EnumSet.of(TUMBLEDOWN,
//			CONFLICTINGSIGNALLOCK, MAINTENANCELOCK, SWITCHUNLOCK, SIGNALINDICATIONLOCK);
	public final static EnumSet<LogicLocks> CallOnBlockers = EnumSet.of(
			CONFLICTINGSIGNALLOCK, SWITCHUNLOCK, TIMELOCK, MAINTENANCELOCK, EXTERNALLOCK);
//	/** 
//	 * the set of common locks that prevent the dispatcher from moving switch points.  DETECTIONLOCK
//   * is not included because doing so would lock a turnout when an adjacent block is occupied
//	 */
//	public final static EnumSet<LogicLocks> SwitchLocked = EnumSet.of(DETECTIONLOCK, SIGNALINDICATIONLOCK, ROUTELOCK, SWITCHUNLOCK,
//			CALLONLOCK, MAINTENANCELOCK, OPPOSINGSIGNALLOCK);
	public final static EnumSet<LogicLocks> SwitchLocked = EnumSet.of(ROUTELOCK, SWITCHUNLOCK,
			CALLONLOCK, MAINTENANCELOCK);
	
	/** 
	 * the set of locks that prevent local switch point movement
	 */
	public final static EnumSet<LogicLocks> LocalSwitchLocked = EnumSet.of(ROUTELOCK, CALLONLOCK);
//	public final static EnumSet<LogicLocks> LocalSwitchLocked = EnumSet.of(SIGNALINDICATIONLOCK, ROUTELOCK,
//	CALLONLOCK);

//	/**
//	 * the set of locks that force a crossing to set its local ConflictingSignal
//	 * lock, to indicate that the crossing is busy in the perpendicular direction
//	 */
//	public final static EnumSet<LogicLocks> CrossingBusyFilter = EnumSet.of(DETECTIONLOCK, TUMBLEDOWN, MAINTENANCELOCK,
//			APPROACHLOCK, ROUTELOCK);
	/**
	 * the set of locks that force a crossing to set its local ConflictingSignal
	 * lock, to indicate that the crossing is busy in the perpendicular direction
	 */
	public final static EnumSet<LogicLocks> CrossingBusyFilter = EnumSet.of(DETECTIONLOCK, 
			APPROACHLOCK, MAINTENANCELOCK, ROUTELOCK, EXTERNALLOCK);
	
//	/**
//	 * the set of locks that are blocked in maintenance mode (Track Authority or Out of Service)
//	 */
//	public final static EnumSet<LogicLocks> MaintenanceBlock = EnumSet.of(TUMBLEDOWN, ROUTELOCK, OPPOSINGSIGNALLOCK,
//			SIGNALINDICATIONLOCK);
//	
//	/**
//	 * The set of locks that the GUI must pass through a turnout when the points move
//	 */
//	public final static EnumSet<LogicLocks> GUISwitchLocks = EnumSet.of(OPPOSINGSIGNALLOCK, CONFLICTINGSIGNALLOCK, SWITCHUNLOCK,
//			TUMBLEDOWN, SIGNALINDICATIONLOCK, ROUTELOCK, CALLONLOCK);

	/**
	 * performs an intersection operation on two sets.  Because of the way the
	 * Java set operators work, one must be copied because it will be modified.
	 * @param varSet is the set that is copied (so that the original is not modified)
	 * @param constSet is the "mask" - it remains unchanged.
	 * @return the elements that are common to both
	 */
	static public final EnumSet<LogicLocks> intersection(final EnumSet<LogicLocks> varSet,
			final EnumSet<LogicLocks> constSet) {
		EnumSet<LogicLocks> copy = varSet.clone();
		copy.retainAll(constSet);
		return copy;    
	}
}
/* @(#)LogicLocks.java */
