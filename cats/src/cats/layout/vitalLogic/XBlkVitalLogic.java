/* Name: XBlkVitalLogic.java
 *
 * What:
 * This file contains the definition for the VitalLogic that forms one side of a
 * crossing (diamond) that is on a block boundary.  As such, it is a fusion of
 * BlkVitalLogic and XVitalLogic.  It takes the event handling of the former and
 * augments it with the cross traffic locks of the latter.
 */
package cats.layout.vitalLogic;

import cats.layout.codeLine.packetFactory.PacketFactory;

/**
 * This file contains the definition for the VitalLogic that forms one side of a
 * crossing (diamond) that is on a block boundary.  As such, it is a fusion of
 * BlkVitalLogic and XVitalLogic.  It takes the event handling of the former and
 * augments it with the cross traffic locks of the latter.
 * 
 * I MIGHT BE ABLE TO USE InternalXVitalLogic INSTEAD!!!!
 * 
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014, 2015, 2016, 2019, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class XBlkVitalLogic extends BlkVitalLogic implements CrossingVitalLogic {

	private XVitalLogic XLogic;
	
	/**
	 * the ctor
	 * @param transmitter is the PacketFactory that constructs and sends indications to the GUI
	 * @param id is the idenetity of the xBlkVitalLogic
	 * @param index is the number of the side of the crossing the XBlkVitalLogic is on
	 */
	public XBlkVitalLogic(PacketFactory transmitter, String id, int index) {
		super(transmitter, id);
		XLogic = new XVitalLogic(index, this);
	}
	
	@Override
	public void configureLockProcessing() {
//		super.configureLockProcessing();
//		LockProcessor.put(LogicLocks.DETECTIONLOCK, new CrossingLockDecorator(LockProcessor.get(LogicLocks.DETECTIONLOCK), XLogic));
//		// Do nothing with CONFLICTINGSIGNAL
//		// Do nothing with HOLDLOCK
//		// Do nothing with SWITCHUNLOCK
//		// Do nothing with APPROACHLOCK
//		// Do nothing with TIMELOCK
//		LockProcessor.put(LogicLocks.TRAFFICLOCK, new CrossingLockDecorator(LockProcessor.get(LogicLocks.TRAFFICLOCK), XLogic));
//		LockProcessor.put(LogicLocks.ROUTELOCK, new CrossingLockDecorator(LockProcessor.get(LogicLocks.ROUTELOCK), XLogic));
//		LockProcessor.put(LogicLocks.PROTECTIONLOCK, new CrossingLockDecorator(LockProcessor.get(LogicLocks.PROTECTIONLOCK), XLogic));
//		LockProcessor.put(LogicLocks.MAINTENANCELOCK, new CrossingLockDecorator(LockProcessor.get(LogicLocks.MAINTENANCELOCK), XLogic));
//		// Do nothing with CALLONLOCK
//		//		LockProcessor.put(LogicLocks.FLEETLOCK, new MergedLockPropagationDecorator(new AdvanceRequestLockProcessor(LogicLocks.FLEETLOCK)));
//		LockProcessor.put(LogicLocks.FLEETLOCK, new LockPropagationDecorator(new DefaultRequestLockProcessor(LogicLocks.FLEETLOCK)));
//		// Do nothing with EXTERNALLOCK
//		//		LockProcessor.put(LogicLocks.DETECTIONLOCK, new CrossingLockDecorator (
//		//		new LockPropagationDecorator(new MergedRequestLockProcessor(LogicLocks.DETECTIONLOCK)), XLogic));
		
		// These are a fusion of BlkVitalLogic and InternalXVitalLogic
		NeighborBlk = (BlkVitalLogic) NeighborLogic;
		
		// DETECTIONLOCK
		LockProcessor.put(LogicLocks.DETECTIONLOCK, 
				new LockFilterDecorator(
						new LocalCodeLockDecorator(
								new CommonDetectionDecorator(
										new MergedLockFilterDecorator(
												new CrossingLockDecorator(
														new LockPropagationDecorator(
																new MergedRequestLockProcessor(LogicLocks.DETECTIONLOCK)), XLogic))))));

		// CONFLICTINGSIGNALLOCK
		if (NeighborBlk != null) {
			LockProcessor.put(LogicLocks.CONFLICTINGSIGNALLOCK, 
					new LockFilterDecorator(
							new LockPropagationDecorator(
									new DefaultRequestLockProcessor(LogicLocks.CONFLICTINGSIGNALLOCK))));
		}

		// HOLDLOCK - never should be set/cleared
		// SWITCHUNLOCK
		if (NeighborBlk != null) {
			LockProcessor.put(LogicLocks.SWITCHUNLOCK, 
					new LockFilterDecorator(
							new LockPropagationDecorator(
									new DefaultRequestLockProcessor(LogicLocks.SWITCHUNLOCK))));
		}
		// APPROACHLOCK - never should be set/cleared
		// TIMELOCK
		if (NeighborBlk != null) {
			LockProcessor.put(LogicLocks.TIMELOCK, 
					new MyLocksFilterDecorator(
							new LockPropagationDecorator(
									new DefaultRequestLockProcessor(LogicLocks.TIMELOCK))));
		}

		// TRAFFICLOCK - never should be set/cleared
		// ROUTELOCK
		LockProcessor.put(LogicLocks.ROUTELOCK, 
				new MergedLockFilterDecorator(
						new CrossingLockDecorator(
								new LockPropagationDecorator(
										new MergedRequestLockProcessor(LogicLocks.ROUTELOCK)), XLogic)));

		// PROTECTIONLOCK
		LockProcessor.put(LogicLocks.PROTECTIONLOCK, 
				new MergedLockFilterDecorator(
						new LockPropagationDecorator(
								new MergedRequestLockProcessor(LogicLocks.PROTECTIONLOCK))));

		// MAINTENANCE
		LockProcessor.put(LogicLocks.MAINTENANCELOCK, 
				new MergedLockFilterDecorator(
						new CrossingLockDecorator(
								new LockPropagationDecorator(
										new MergedRequestLockProcessor(LogicLocks.MAINTENANCELOCK)), XLogic)));

		// CALLONLOCK - never should be set/cleared
		// FLEETLOCK - never should be set/cleared
//		if ((NeighborBlk != null) && !MyDiscipline.equals(Discipline.ABS) && !MyDiscipline.equals(Discipline.UNDEFINED)) {
//			LockProcessor.put(LogicLocks.FLEETLOCK,
//					new MyLocksFilterDecorator(
//							new LockPropagationDecorator(
//									new DefaultRequestLockProcessor(LogicLocks.FLEETLOCK))));
//		}

		// EXTERNALLOCK
		LockProcessor.put(LogicLocks.EXTERNALLOCK, 
				new LockFilterDecorator(
						new LocalCodeLockDecorator(
								new CrossingLockDecorator(
										new MergedLockFilterDecorator(
												new LockPropagationDecorator(
														new DefaultRequestLockProcessor(LogicLocks.EXTERNALLOCK))), XLogic))));
	}
	
	/**
	 * connects this Vital Logic to its crossing Vital Logics
	 * @param crossing are the two perpendicular CrossingVitalLogics
	 */
	public void linkCrossing(final CrossingVitalLogic crossing[]) {
		CrossingInterface c[] = new CrossingInterface[2];
		c[0] = crossing[0].getCrossing();
		c[1] = crossing[1].getCrossing();
		XLogic.setCrossings(c);
	}
	
	@Override
	public CrossingInterface getCrossing() {
		return XLogic;
	}
	
}
/* @(#)XBlkVitalLogic.java */
