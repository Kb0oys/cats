/* Name: LockRequestProessor.java
 *
 * What:
 *   This is an interface implemented by classes that process lock add and
 *   lock remove requests.  It is intended to provide the abstraction for
 *   both simple locks and those with associated JMRI devices (the API).
 *   <p>
 *   Some locks are set by changes in local conditions (e.g. in ABS, DETECTION
 *   occupancy can set TUMBLEDOWN).  Others are set in response to a dispatcher
 *   request, via the CodeLine (e.g. SIGNALINDICATION).  Similarly, some locks
 *   may be cleared by changes in local conditions (e.g. clearing DETECTION can
 *   clear TUMBLEDOWN).  Some may be cleared in response to a dispatcher action
 *   (e.g. removing maintenance).  A few locks may be cleared by both a change
 *   in local conditions (e.g. clearing DETECTION) and also in response to a
 *   dispatcher action. In addition to simply clearing the lock, there may be
 *   different actions associated with the two clearing paths.  Thus, this
 *   interface defines two clearing methods.
 *   <ol>
 *   <li>clear is used by a change in local conditions</li>
 *   <li>cancel is used by a request from the dispatcher via the CodeLine
 *   </ol> 
 *   A Lock can be set directly or as a result of Vital Logic in advance (the
 *   feeder Vital Logic).  Since both can be active, there are essentially
 *   two Locks.  The clear specifies which Lock is cleared.  Cancel clears
 *   both.
 */
package cats.layout.vitalLogic;

/**
 *   This is an interface implemented by classes that process lock add and
 *   lock remove requests.  It is intended to provide the abstraction for
 *   both simple locks and those with associated JMRI devices (the API).
 *   <p>
 *   Some locks are set by changes in local conditions (e.g. in ABS, DETECTION
 *   occupancy can set TUMBLEDOWN).  Others are set in response to a dispatcher
 *   request, via the CodeLine (e.g. SIGNALINDICATION).  Similarly, some locks
 *   may be cleared by changes in local conditions (e.g. clearing DETECTION can
 *   clear TUMBLEDOWN).  Some may be cleared in response to a dispatcher action
 *   (e.g. removing maintenance).  A few locks may be cleared by both a change
 *   in local conditions (e.g. clearing DETECTION) and also in response to a
 *   dispatcher action. In addition to simply clearing the lock, there may be
 *   different actions associated with the two clearing paths.  Thus, this
 *   interface defines two clearing methods.
 *   <ol>
 *   <li>clear is used by a change in local conditions</li>
 *   <li>cancel is used by a request from the dispatcher via the CodeLine</li>
 *   </ol>
 *   </p>
 *   <p>
 *   A Lock can be set directly or as a result of Vital Logic in advance (the
 *   feeder Vital Logic).  Since both can be active, there are essentially
 *   two Locks.  The clear specifies which Lock is cleared.  Cancel clears
 *   both.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2015, 2016, 2019, 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface LockRequestProcessor {

  /**
   * request that the lock be set.
   */
  public void requestLockSet();
  
  /**
   * request that the lock be cleared
   */
  public void requestLockClear();
  
  /**
   * forwards along the event that a VitalLogic in advance has set its internal lock
   */
  public void advanceLockSet();
  
  /**
   * forwards along the event that a VitalLogic in advance has cleared its internal lock
   */
  public void advanceLockClear();

//  /**
//   * a hook to perform a LockProcessor specific function
//   */
//  public void cancelSetRequest();

  /**
   * queries to determine if a lock is set
   * @return true if it is and false if it is not
   */
  public boolean isLockSet();
  
  /**
   * queries for the Lock
   * @return the Lock being encapsulated
   */
  @Deprecated
  public LogicLocks getLock();
  
//  /**
//   * queries for the results of the last set or clear.  If true, then the
//   * merged state of the lock between local and advance locks changed;
//   * otherwise, it did not change.
//   * @return the result of the last merger
//   */
//  public boolean getChangedState();
}
/* @(#)LockRequestProcessor.java */
