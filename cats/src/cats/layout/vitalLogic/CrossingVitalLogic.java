/* Name: XBlkVitalLogic.java
 *
 * What:
 * This file contains the definition for the interface supported by VitalLogic on the
 * side of a crossing (diamond).  It encapsulates the protocol that is used to set
 * and clear locks on crossing tracks.
 */
package cats.layout.vitalLogic;

/**
 * This file contains the definition for the interface to Vital Logic on a crossing
 * and the GUI.  Note that this is for the whole Vital Logic - not the communication
 * between this Vital Logic and its peers.  That interface is defined by CrossingInterface.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface CrossingVitalLogic {
	/**
	 * connects this Vital Logic to its crossing Vital Logics
	 * @param crossing is the two CrossingVitalLogics, containing the two
	 * CrossingInterfaces
	 */
	public void linkCrossing(final CrossingVitalLogic crossing[]);
	
	/**
	 * retrieves the contained CrossingInterface
	 * @return the CrossingInterface
	 */
	public CrossingInterface getCrossing();
}
/* @(#)CrossingVitalLogic.java */
