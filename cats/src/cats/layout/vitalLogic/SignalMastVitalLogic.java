/* Name: SignalMastVitalLogic.java
 *
 * What:
 *   This class determines and sets the aspects on layout signals.  It receives
 *   its stimuli from the encompassing IntermediateVitalLogic or CPVitalLogic and
 *   from the SignalVitalLogic in advance (up stream).  When it receives stimuli,
 *   it recomputes the associated signal's aspect, changes it, forwards the change
 *   down stream to the SignalVitalLogic in advance.  The VitalLogic handles sending
 *   the new aspect to the GUI for updating the icons.
 */
package cats.layout.vitalLogic;

import cats.layout.vitalLogic.IntermediateVitalLogic.FORCED_ASPECT;
import cats.trackCircuit.TrackCircuit;

/**
 *   This class determines and sets the aspects on layout signals.  It receives
 *   its stimuli from the encompassing IntermediateVitalLogic or CPVitalLogic and
 *   from the SignalVitalLogic in advance (up stream).  When it receives stimuli,
 *   it recomputes the associated signal's aspect, changes it, forwards the change
 *   down stream to the SignalVitalLogic in advance.  The VitalLogic handles sending
 *   the new aspect to the GUI for updating the icons.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014, 2015, 2017, 2018, 2019, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface SignalMastVitalLogic {

	/**
	 * This method is invoked by the VitalLoigc to override the computed
	 * aspect.  It takes a circuitous route to get to the signal:
	 * GUI->VitalLogic->SignalVitalLogic
	 * @param override is the commanded aspect
	 */
	public void forceAspect(final FORCED_ASPECT override);
	
	/**
	 * sets the speed of the track being protected by the signal.
	 * @param speed is the speed
	 * @see cats.layout.items.Track
	 */
	public void setSpeed(final int speed);
	
	/**
	 * sets up the linkages for approach lighting
	 * @param myLogic is the VitalLogic that owns the signal.  It is passed in to
	 * complete approach light linkage
	 */
	public void setApproachLighting(IntermediateVitalLogic myLogic);
	
	/**
	 * retrieves the TrackCircuit through which this Signal receives information
	 * from the approach Signal.  It is the conduit through which Signals
	 * talk to each other.
	 * @return the embedded TrackCircuit.  If all initialization completes, it
	 * should not be null.
	 */
	public TrackCircuit getTrackCircuit();
	
	/**
	 * retrieves the current aspect of the signal
	 * @return the aspect
	 */
	public int getAspect();
	
//	/**
//	 * tells the signal how to handle sending OPPOSING and TUMBLEDOWN
//	 * aspects to the signal in approach
//	 * @param vital is one of four signal types defined in IntermediateVitalLogic
//	 */
//	public void setApproachTranslation(SIGNAL_TYPE vital);
}
/* @(#)SignalMastVitalLogic.java */