/* Name: XVitalLogic.java
 *
 * What:
 * This file contains the definition for the VitalLogic that forms one side of an
 * internal crossing.  It is internal in the sense that it is not on a block boundary,
 * thus, has no specialization for APB reservations.  It also does not have an interface
 * to the GUI, so no code line is implemented.  It is a crossing (diamond) so that its neighbor
 * is its twin.  The other two XVitalLogic objects are on the crossing track.  For the
 * most part, XVitalLogic relays events through its neighbor.  However, when an event
 * blocks cross traffic, it sets the ConfictingSignal lock on the two perpendicular
 * XVitalLogics.  Conversely, when a blocking event is detected on the cross track,
 * one (or both) of the perpendicular XVitalLogics will set this VitalLogic's ConflictingSignal
 * lock.  Because both could be indicating the conflict, there is a status for each,
 * independently.  The sum is an "or" of the two individuals.
 */
package cats.layout.vitalLogic;

import java.util.ArrayList;
import java.util.EnumSet;

import cats.common.Sides;

/**
 * This file contains the definition for the VitalLogic that forms one side of an
 * internal crossing.  It is internal in the sense that it is not on a block boundary,
 * thus, has no specialization for APB reservations.  It also does not have an interface
 * to the GUI, so no code line is implemented.  It is a crossing (diamond) so that its neighbor
 * is its twin.  The other two XVitalLogic objects are on the crossing track.  For the
 * most part, XVitalLogic relays events through its neighbor.  However, when an event
 * blocks cross traffic, it sets the ConfictingSignal lock on the two perpendicular
 * XVitalLogics.  Conversely, when a blocking event is detected on the cross track,
 * one (or both) of the perpendicular XVitalLogics will set this VitalLogic's ConflictingSignal
 * lock.  Because both could be indicating the conflict, there is a status for each,
 * independently.  The sum is an "or" of the two individuals.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014, 2015, 2016, 2019</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class XVitalLogic implements CrossingInterface {
	
	/**
	 *  the Logic Lock used to indicate the crossing track is locked
	 */
	private final LogicLocks CrossLock = LogicLocks.CONFLICTINGSIGNALLOCK;
	
	/**
	 * the index that this XVitalLogic uses in communicating with the
	 * crossing XVitalLogics.
	 */
	private final int MyIndex;
	
	/**
	 * the crossing XVitalLogics.  The crossing XVitalLogic is identified
	 * by an index, which is established when the object is instantiated.
	 */
	private CrossingInterface Crossing[];
	
	/**
	 * the enclosing VitalLogic, whose ConflictingSignal lock is set and cleared
	 */
	private final BasicVitalLogic Parent;
	
	/**
	 * the locks from crossing tracks that force a ConflictingSignal on this edge
	 */
	private ArrayList<EnumSet<LogicLocks>> BlockingLocks = new ArrayList<EnumSet<LogicLocks>>(Sides.EDGENAME.length);
	
	/*********************************************************************************************
	 * These are the interfaces to the locks.
	 *********************************************************************************************/
	
	/**
	 * the ctor
	 * @param index is the number of the side of the crossing the XVitalLogic is on
	 * @param parent is the BasicVitalLogic that this XVitalLogic belongs to
	 */
	public XVitalLogic(int index, BasicVitalLogic parent) {
		MyIndex = index;
		Parent = parent;
		for (int side = 0; side < Sides.EDGENAME.length; ++side) {
			BlockingLocks.add(EnumSet.noneOf(LogicLocks.class));
		}
	}

	/**
	 * tells this XVitalLogic where its blocking XVitalLogics are.
	 * @param crossings are the blocking XVitalLogics
	 */
	public void setCrossings(final CrossingInterface crossings[]) {
		Crossing = crossings;
	}
	
	// the next two methods are for the originating Vital Logic
	@Override
	public void blockCrossTraffic(final LogicLocks xLock) {
		Crossing[0].setCrossLock(xLock, MyIndex);
		Crossing[1].setCrossLock(xLock, MyIndex);
	}	
	
	@Override
	public void unblockCrossTraffic(final LogicLocks xLock) {
		Crossing[0].clearCrossLock(xLock, MyIndex);
		Crossing[1].clearCrossLock(xLock, MyIndex);
	}	
	
	// the next two methods are for the receiving Vital Logic
	@Override
	public void setCrossLock(LogicLocks lock, int who) {
		if (BlockingLocks.get(0).isEmpty() && BlockingLocks.get(1).isEmpty()) {
			Parent.setLocalLock(CrossLock);
		}
		BlockingLocks.get(who).add(CrossLock);
	}

	@Override
	public void clearCrossLock(LogicLocks lock, int who) {
		BlockingLocks.get(who).remove(CrossLock);
		if (BlockingLocks.get(0).isEmpty() && BlockingLocks.get(1).isEmpty()) {
			Parent.clearLocalLock(CrossLock);
		}
	}
}
/* @(#)XVitalLogic.java */
