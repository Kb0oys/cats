/* Name: BasicVitalLogic.java
 *
 * What:
 * This file contains the definition for the simplest field equipment simulation -
 * the vital logic.  This level is the base class for vital logic, whose main purpose
 * is to receive state changes (changes to locks) from the code line, internal sensors, and
 * other vital logic and
 * pass them on to downstream (in approach) vital logic and the GUI.  This most basic class simply relays
 * changes.  Other derived classes enhance its functionality by processing the changes, possibly
 * stimulating the approach vital logic, alerting the GUI, etc.
 * <p>
 * This class is influenced by the Composite design pattern described in "Patterns in
 * Java" by Mark Grand.  Because the components are hidden from the objects that interact
 * with a concrete instantiation, the interfaces required by the components must bubble
 * the interface to this class (see Grand pg. 170).  Thus, simplicity through structure hiding
 * trumps high cohesion.
 * <p>
 * The primary components are:
 * <ul>
 * <li> function: a core that receives lock changes from the vital logic in advance, possibly processes
 * them, stimulates the vital logic in approach, and possibly reports the changes to the GUI.  The functions
 * are: block edge, points edge, frog edge,
 * diamond (cross over) edge, and crossing edge (cross over on a section boundary)
 * </li>
 * <li> discipline: how the vital logic handles specific locks.  The disciplines are: ABS, APB, CTC,
 * and DTC.
 * </li>
 * <li> codeline: the communications channel to the office equipment.  The channel may transmit
 * only (the panel is affected by changes in the vital logic, but can not affect changes), or
 * bi-directional (where commands are sent to the vital logic and changes reported back to the office
 * equipment).  Unlike the prototype, all vital logic must have a codeline.
 * </li>
 * <li> signal: if the vital logic drives a signal or not.  The signal may be none; panel only;
 * physical only, or panel and physical.
 * </li>
 * </ul>
 * Some of these components overlap.  For example, CTC implies a codeline to control points
 * (vital logic with block edge functionality and panel signals).  However, CTC intermediate
 * signals implies vital logic with block edge functionality, physical signals, and no
 * panel signals.
 */
package cats.layout.vitalLogic;

import java.util.EnumMap;
import java.util.EnumSet;

import cats.apps.Crandic;
import cats.common.Constants;
import cats.common.DebugBits;
import cats.layout.BiDecoderObserver;
import cats.layout.Discipline;
import cats.layout.codeLine.CodeMessage;
import cats.layout.codeLine.CodePurpose;
import cats.layout.codeLine.Codes;
import cats.layout.codeLine.packetFactory.PacketFactory;
import cats.layout.items.IOSpec;
import cats.layout.items.Track;
import cats.trackCircuit.TrackCircuit;


/**
 * This file contains the definition for the simplest field equipment simulation -
 * the vital logic.  This level is the base class for vital logic, whose main purpose
 * is to receive state changes (changes to locks) from the code line, internal sensors, and
 * other vital logic and
 * pass them on to downstream (in approach) vital logic and the GUI.  This most basic class simply relays
 * changes.  Other derived classes enhance its functionality by processing the changes, possibly
 * stimulating the approach vital logic, alerting the GUI, etc.
 * <p>
 * This class is influenced by the Composite design pattern described in "Patterns in
 * Java" by Mark Grand.  Because the components are hidden from the objects that interact
 * with a concrete instantiation, the interfaces required by the components must bubble up
 * the interface to this class (see Grand pg. 170).  Thus, simplicity through structure hiding
 * trumps high cohesion.
 * <p>
 * The primary components are:
 * <ul>
 * <li> function: a core that receives lock changes from the vital logic in advance, possibly processes
 * them, stimulates the vital logic in approach, and possibly reports the changes to the GUI.  The functions
 * are: block edge, points edge, frog edge,
 * diamond (cross over) edge, and crossing edge (cross over on a section boundary)
 * </li>
 * <li> discipline: how the vital logic handles specific locks.  The disciplines are: ABS, APB, CTC,
 * and DTC.
 * </li>
 * <li> codeline: the communications channel to the office equipment.  The channel may transmit
 * only (the panel is affected by changes in the vital logic, but can not affect changes), or
 * bi-directional (where commands are sent to the vital logic and changes reported back to the office
 * equipment).  Unlike the prototype, all vital logic must have a codeline.
 * </li>
 * <li> signal: if the vital logic drives a signal or not.  The signal may be none; panel only;
 * physical only, or panel and physical.
 * </li>
 * </ul>
 * Some of these components overlap.  For example, CTC implies a codeline to control points
 * (vital logic with block edge functionality and panel signals).  However, CTC intermediate
 * signals implies vital logic with block edge functionality, physical signals, and no
 * panel signals.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014, 2015, 2016, 2017, 2018, 2019, 2021, 2022, 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class BasicVitalLogic {

	/**
	 * the local locks (the state of the VitalLogic).  These have some persistence.
	 * They may trigger action and be forwarded to the approach Vital Logic.
	 */
	protected EnumSet<LogicLocks> MyLocks = EnumSet.noneOf(LogicLocks.class);

	/**
	 * the state of the locks of the VitalLogic in advance (behind this one).  The
	 * VitalLogic in advance feeds this one.
	 */
	protected EnumSet<LogicLocks> AdvanceLocks = EnumSet.noneOf(LogicLocks.class);

	/**
	 * the last merger of the local locks and advance locks.  It is used for determining
	 * signal indications
	 */
	protected EnumSet<LogicLocks> MergedGlobalLocks = LogicLocks.NoLogicLocks.clone();

	/**
	 * the Vital Logic that touches (shares a Section edge) with this one.  It faces
	 * against opposing traffic.  If the Vital Logic is at the end of track, there will be
	 * no NeighborLogic.
	 */
	protected BasicVitalLogic NeighborLogic = null;

	/**
	 * the Vital Logic at the other end of the shared track, facing against opposing
	 * traffic.  If a train enters the shared track through this Vital Logic, it is
	 * expected to exit
	 * through the Peer Vital Logic and vice versa.  Together with NeighborLogic,
	 * all the Vital Logics are chained together.
	 * 
	 * There will always be PeerLogic - the other end of the track segmeny.
	 */
	protected BasicVitalLogic PeerLogic;

	/**
	 * the Vital Logic that this one feeds - the Peer of the Neighbor.  It can be null
	 * if there is no Neighbor.
	 */
	protected BasicVitalLogic ApproachLogic = null;

	/**
	 * the Locks last sent to the Approach Vital Logic - the filtered results
	 * from combining the filtered local locks with the filtered advance locks
	 */
	protected EnumSet<LogicLocks> ApproachLocks = EnumSet.noneOf(LogicLocks.class);

	/**
	 * the object that creates and transmits messages on the code line.  FrogVitalLogic
	 * do not have code line connections.
	 */
	protected final PacketFactory Transmitter;

	/**
	 * the signaling discipline
	 */
	protected Discipline MyDiscipline = Discipline.UNDEFINED;

	/**
	 *  the slowest speed between this VitalLogic and the one in advance - behind
	 *  it.  This is the speed being protected.  Typically, this speed is a constant
	 *  and final.  However, for Points, it changes based on how the tracks are lined.
	 */
	protected int Speed = Track.NORMAL;

	/**
	 * the Speed of the block(s) in advance of this one.  It is merged with this.Speed (if
	 * not behind a signal) to form MergedSpeed.  For points, it is the speed of the track
	 * in Approach, to the next VitalLogic.
	 */
	protected int AdvanceSpeed = Track.NORMAL;

	/*
	 * the merger of Speed and AdvanceSpeed, which is either
	 * 1. forwarded to the approach VitalLogic (no signal)
	 * 2. given to the attached signal
	 * 3. forwarded to the approach VitalLogic (points)
	 */
	protected int MergedSpeed = Track.NORMAL;

	/**
	 * a marker for recognizing the VitalLogic when debugging
	 */
	protected String Identity;

	/*********************************************************************************************
	 * These are the interfaces to the locks, common to all VitalLogic
	 *********************************************************************************************/
	protected EnumMap<LogicLocks, LockRequestProcessor> LockProcessor = new EnumMap<LogicLocks, LockRequestProcessor>(LogicLocks.class);

	/**
	 * the ctor
	 * @param transmitter is the object that creates code line packets sent
	 * by the VitalLogic.  It can be null.
	 * @param id is a String used to identify the VitalLogic in debugging
	 */
	public BasicVitalLogic(PacketFactory transmitter, String id) {
		Transmitter = transmitter;
		Identity = id;
	}

	/**
	 * identifies the Vital Logic that touches this one.
	 * @param neighbor is that logic.  It can be null, meaning that
	 * there is no neighbor.
	 */
	public void setNeighborLogic(final BasicVitalLogic neighbor) {
		NeighborLogic = neighbor;
		//		configureLockProcessing();
	}

	/**
	 * this method sets up the LockProcessors.  In some cases, lock processing
	 * depends upon both the discipline and the kind of neighbor.  So, it is important to
	 * know that the discipline is set (tailorDiscipline()) before the neighbor.
	 */
	public void configureLockProcessing() {		
		// DETECTIONLOCK
		// CONFLICTINGSIGNALLOCK
		// HOLDLOCK
		// SWITCHUNLOCK
		// APPROACHLOCK
		// TIMELOCK
		// TRAFFICLOCK
		// ROUTELOCK
		// PROTECTIONLOCK
		// MAINTENANCELOCK
		// CALLONLOCK
		// FLEETLOCK
		// TRAFFICSTICK
		// EXTERNALLOCK
	}

	/**
	 * identifies the Vital Logic that this Vital Logic feeds
	 * @param approach is that logic  It cannot be null.
	 */
	public void setApproachLogic(final BasicVitalLogic approach) {
		ApproachLogic = approach;
		ApproachLogic.mergeSpeeds(MergedSpeed);
	}

	/**
	 * identifies the Vital Logic at the opposite end of the track.
	 * @param peer is that logic
	 */
	final public void setPeerLogic(final BasicVitalLogic peer) {
		PeerLogic = peer;
	}

	/**
	 * modifies the VitalLogic for the idiosyncrasies of the discipline
	 * controlling the signals.
	 * @param behavior is the discipline defined in the Block
	 * @see cats.layout.Discipline
	 * @see cats.layout.items.Block
	 */
	public void tailorDiscipline(final Discipline behavior) {	
		MyDiscipline = behavior;
	}

	/**
	 * sets the speed of the track being protected by the Vital Logic
	 * @param speed is the speed
	 * @see cats.layout.items.Track
	 */
	public void setSpeed(final int speed) {
		Speed = speed;
		mergeSpeeds(AdvanceSpeed);
	}

	/**
	 * merges the speed of the tracks protected by this VitalLogic
	 * with the speed of the track protected by the advance VitalLogic and
	 * if is changes, forwards to the approach VitalLogic
	 * @param advSpeed is the speed of the track in advance
	 */
	protected void mergeSpeeds(final int advSpeed) {
		int speed = MergedSpeed;
		AdvanceSpeed = advSpeed;
		MergedSpeed = Track.getSlower(Speed, AdvanceSpeed);
//		System.out.println(Identity + " merging speed " + advSpeed + " to block speed " + Speed + " was " + speed + " now " + MergedSpeed);
		if ((speed != MergedSpeed) && (ApproachLogic != null)) {
			ApproachLogic.mergeSpeeds(MergedSpeed);
		}
	}

	/**
	 * assigns the VitalLogic a String that is useful in identifying it during
	 * debugging.  This string cannot be set when the VitalLogic is created
	 * because the coordinates of the enclosing TrackEdge are not known at
	 * that time.
	 * @param identity is the coordinates of the section and edge of the
	 * TrackEdge, as a String
	 */
	public void setIdentity(final String identity) {
		Identity = identity;
	}

	/**
	 * is used to connect the TrackCircuits together.  I am not sure of where to do this - in the Vital
	 * Logic or the GUI.  I am doing it here because it reduces the linkages between the GUI and Vital Logic.
	 * In addition, a hardware implementation could have its own way of determining connectivity.
	 * @return the next TrackCircuit in Approach - the one fed by an optional associated TrackCircuit.
	 */
	protected TrackCircuit trackCircuitProbe() {
		return (ApproachLogic != null)  ? ApproachLogic.trackCircuitProbe() : null;
	}

	/**
	 * is used to find the first Vital Logic in approach, containing a signal mast.  It is
	 * used for transporting route level locks.  It could be combined with the TrackCircuit
	 * probes, but that also creates TrackCircuits as a side affect.
	 * @return the first VitalLogic in approach containing a signal or signal forwarding.
	 * The result could also be null;
	 */
	@Deprecated
	protected SignalVitalLogic signalVitalLogicProbe() {
		return (ApproachLogic != null) ? ApproachLogic.signalVitalLogicProbe() : null;
	}

//	/**
//	 * This method computes the global field MergedGlobalLocks.  MergedGlobalLocks
//	 * is the merger of the local Lock and the Lock from the advance VitalLogic (the
//	 * feeder).  It is used in determining what is fed to the approach VitalLogic
//	 * and signal.  If the merged
//	 * value changes, the MergedGlobalLocks is adjusted and true is returned.
//	 * Otherwise, false is returned.  It does not forward any lock changes.  That task
//	 * is done by the PropagationDecorator on the lock.
//	 * <p>
//	 * This method looks at only one Lock!
//	 * @param l is the lock being merged
//	 * @return true if the merged global state changed and false if not
//	 */
//	@Deprecated
//	protected boolean mergeGlobalLocks(final LogicLocks l) {
//		EnumSet<LogicLocks> oldState = MergedGlobalLocks.clone();
//		boolean oldLock = MergedGlobalLocks.contains(l);
//		boolean newLock = AdvanceLocks.contains(l)	|| MyLocks.contains(l);
//		if (oldLock == newLock) {
//			return false;
//		}
//		if (newLock) {
//			MergedGlobalLocks.add(l);
//		}
//		else {
//			MergedGlobalLocks.remove(l);
//		}
//		/******************** DEBUG code *****************************/
//		if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT)) {
//			System.out.println(Identity + ": Merged Global Locks were " + oldState.toString() + " now " + MergedGlobalLocks.toString() + " MyLocks=" +
//					MyLocks + " Advanced=" + AdvanceLocks);			
//		}
//		/*************************************************************/
//		return true;
//	}

	/**
	 * the method through which the field equipment receives commands
	 * from the office.  This method is tailored for each derived class
	 * because each derived class accepts a unique set of commands on the
	 * code line.
	 * <p>
	 * In general, the routine that sets/clears the lock will acknowledge
	 * the message.
	 * @param command is the command
	 */
	public void processCommand(final String command) {
		CodeMessage cmd = new CodeMessage(command);
		String state = cmd.Value;
		/******************** DEBUG code *****************************/
		if (Crandic.Details.get(DebugBits.CODELINEBIT)) {
			System.out.println(Identity + " vital logic received " + command);
		}
		/*************************************************************/
		if (cmd.Purpose == CodePurpose.REQUEST) {
			if (Boolean.valueOf(state)) {
				setLocalLock(Codes.toLogicLock(cmd.Lock));
			}
			else {
				clearLocalLock(Codes.toLogicLock(cmd.Lock));
			}
		}
	}

	/**
	 * determines if a LockRequestProcessor exists for the requested lock.
	 * If it exists, the method to set the lock is invoked
	 * @param l is the desired lock.
	 */
	public void setLocalLock(final LogicLocks l) {
		LockRequestProcessor processor = LockProcessor.get(l);
		if (processor != null) {
			processor.requestLockSet();
		}
		else {
			MyLocks.add(l);
		}
	}

	/**
	 * determines if a LockRequestProcessor exists for the requested lock.
	 * If it exists, the method to clear the lock is invoked
	 * @param l is the desired lock.
	 */
	public void clearLocalLock(final LogicLocks l) {
		LockRequestProcessor processor = LockProcessor.get(l);
		if (processor != null) {
			processor.requestLockClear();
		}
		else {
			MyLocks.remove(l);
		}
	}

	/**
	 * determines if a LockRequestProcessor exists for the requested lock.
	 * If it exists, its method to handle setting a Lock in the Advance locks
	 * is invoked.  This could trigger recomputation of MergedGlobalLocks.
	 * <p>
	 * If the LockProcessor does not exist, it is set only in MergedGlobalLocks
	 * and forwarded to the approach Vital Logic.
	 * @param l is the desired lock
	 */
	protected void setAdvanceLock(final LogicLocks l) {
		LockRequestProcessor processor = LockProcessor.get(l);
		if (processor != null) {
			processor.advanceLockSet();
		}
		else {
			AdvanceLocks.add(l);
			//			if (mergeGlobalLocks(l)) {
			setApproachLogicLock(l);
			//			}
		}		
	}

	/**
	 * determines if a LockRequestProcessor exists for the requested lock.
	 * If it exists, its method to handle clearing a Lock in the Advance locks
	 * is invoked.  This could trigger recomputation of MergedGlobalLocks.
	 * <p>
	 * If the LockProcessor does not exist, it is cleared only in MergedGlobalLocks
	 * and forwarded to the approach Vital Logic.
	 * @param l is the desired lock
	 */
	protected void clearAdvanceLock(final LogicLocks l) {
		LockRequestProcessor processor = LockProcessor.get(l);
		if (processor != null) {
			processor.advanceLockClear();
		}
		else {
			AdvanceLocks.remove(l);
			//			if (mergeGlobalLocks(l)) {
			clearApproachLogicLock(l);
			//			}
		}		
	}

	/**
	 * feeds a change in a set lock to the approach VitalLogic.
	 * 
	 * It might be possible to simplify things a little by pulling
	 * the operations of this method into PropagationDecorator.
	 * 
	 * @param l is the Lock set
	 */
	protected void setApproachLogicLock(final LogicLocks l) {
		if (ApproachLogic != null) {
			ApproachLogic.setAdvanceLock(l);
		}
	}

	/**
	 * feeds a change in a cleared lock to the approach VitalLogic.
	 * 
	 * It might be possible to simplify things a little by pulling
	 * the operations of this method into PropagationDecorator.
	 * 
	 * @param l is the Lock cleared
	 */
	protected void clearApproachLogicLock(final LogicLocks l) {
		if (ApproachLogic != null) {
			ApproachLogic.clearAdvanceLock(l);
		}
	}

	//	/**
	//	 * is called to receive (or refresh) the MyLocks and AdvanceLocks state.
	//	 * It is called when the advance VitalLogic is points and the points
	//	 * change.  The Locks set by the Peer or Neighbor should not be touched.
	//	 * @param localLocks are the Locks that were set or cleared by calling setLocalLock()
	//	 * or clearLocalLock()
	//	 * @param advanceLocks are the locks that were set or cleared by calling
	//	 * setAdvanceLock() or clearAdvanceLock()
	//	 */
	//	protected void refreshLocks(final EnumSet<LogicLocks> localLocks, final EnumSet<LogicLocks> advanceLocks) {
	//		for (LogicLocks l : EnumSet.allOf(LogicLocks.class)) {
	//			if (MyLocks.contains(l) != localLocks.contains(l)) {
	//				if (localLocks.contains(l)) {
	//					setLocalLock(l);
	//				}
	//				else {
	//					clearLocalLock(l);
	//				}
	//			}
	//			if (AdvanceLocks.contains(l) != advanceLocks.contains(l)) {
	//				if (advanceLocks.contains(l)) {
	//					setAdvanceLock(l);
	//				}
	//				else {
	//					clearAdvanceLock(l);
	//				}
	//			}
	//		}
	//	}

	/**
	 * start communications with the Approach VitalLogic by sending the
	 * current state
	 */
	public void primeApproach() {
		for (LogicLocks l : ApproachLocks) {
			setApproachLogicLock(l);
		}
	}

	/**
	 * Links a LockRequestProcessor to an IOSpec so that the Lock can be monitored or
	 * changed outside of CATS.  This method decorates an existing LockRequestProcessor
	 * (or creates a new one) and places it in the processor table, which may replace
	 * the existing one.  It also attaches the newly created processor to the IOSpec.
	 * @param lock is the LogicLock made visible outside of CATS
	 * @param externalLock is the IOSpec linked to the lock
	 * @return the external decorator on the LockRequestProcessor
	 */
	@Deprecated
	public LockRequestProcessor makeExternalLock(final LogicLocks lock, IOSpec externalLock) {
		LockRequestProcessor processor = LockProcessor.get(lock);
		ExternalLockDecorator p;
		if (processor == null) {
			processor = new MergedRequestLockProcessor(lock);
		}
		p = new ExternalLockDecorator(processor, externalLock);
		LockProcessor.put(lock, processor);
		externalLock.registerListener(p);
		externalLock.registerOtherListener(p);
		return processor;
	}

	/**
	 * Links an IOSpec to a LockRequestProcessor so that a DecoderObserver can monitor changes
	 * to the lock.  This method decorates an existing LockRequestProcessor
	 * (or creates a new one) and places it in the processor table, which may replace
	 * the existing one.  It also attaches the newly created processor to the IOSpec.
	 * @param lock is the LogicLock being monitored
	 * @param observer is the object watching for changes
	 * @return the docorated lock
	 */
	public EventLockDecorator makeLockListener(final LogicLocks lock, final BiDecoderObserver observer) {
		LockRequestProcessor processor = LockProcessor.get(lock);
		EventLockDecorator p;
		if (processor == null) {
			processor = new MergedRequestLockProcessor(lock);
		}
		p = new EventLockDecorator(processor, observer);
		LockProcessor.put(lock, p);
		return p;
	}

	/**
	 * constructs a common lock request processor chain that propagates a change
	 * in merged MyLocks or AdvanceLocks to the edge in approach and stores it
	 * in the LockProcessor table
	 * @param lock is the lock being processed
	 */
	public void installMergePropagationProcessor(LogicLocks lock){
		LockProcessor.put(lock,
				new MergedLockFilterDecorator(
						new LockPropagationDecorator(
								new MergedRequestLockProcessor(lock))));
	}

	/**
	 * constructs a common lock request processor chain that propagates a change
	 * in only AdvanceLocks to the edge in approach and stores it
	 * in the LockProcessor table
	 * @param lock is the lock being processed
	 */
	public void installAdvancePropagationProcessor(LogicLocks lock){
		LockProcessor.put(lock,
				new MyLocksFilterDecorator(
						new LockPropagationDecorator(
								new DefaultRequestLockProcessor(lock))));
	}

	/**********************************************************************************
	 * The following classes handle changes in the Lock status.  They are based on the
	 * Decorator design pattern.  The intent is that they can be stacked together to
	 * provide ala carte behavior for each lock.
	 * 
	 * As described in Design Patterns, a decorator is a class that wraps another class,
	 * thus, extending its functionality without using inheritance.  To make this work
	 * the decorator and the decorated class must implement the same interface; thus, the
	 * decorator can be used in place of the decorated.
	 **********************************************************************************/
	/**********************************************************************************
	 * This is a skeleton for RequestLockProcessors.  It provides common functions, deferring
	 * the major actions to the derived classes.  It could be looked at as containing
	 * utility methods and basic fields.  This forms the basis of the Decorator pattern.
	 * The implementing classes handle the combinations.
	 * <p>
	 *   There are multiple dimensions:
	 * <ul>
	 * <li>The lock identity</li>
	 * <li>The operation - set or clear</li>
	 * <li>The extent to which the Lock change propagates (local or to the approach block)</li>
	 * </ul>
	 * </p>
	 **********************************************************************************/
	protected abstract class AbstractRequestLockProcessor implements LockRequestProcessor {
		/**
		 * the Lock identity
		 */
		protected final LogicLocks Lock;

		//		/**
		//		 * a flag denoting if the action changed the merged value
		//		 * of the local and advance locks.
		//		 */
		//		public boolean StateChanged;
		//		
		/**
		 * used for debugging
		 */
		protected EnumSet<LogicLocks> OldState;

		/**
		 * the constructor
		 * @param lock is the lock being processed
		 */
		public AbstractRequestLockProcessor(final LogicLocks lock) {
			Lock = lock;
		}

		@Override
		abstract public void requestLockSet();

		@Override
		abstract public void requestLockClear();

		@Override
		abstract public void advanceLockSet();

		@Override
		abstract public void advanceLockClear();

		@Override
		public boolean isLockSet() {
			return MyLocks.contains(Lock);
		}


		//		@Override
		//		public void cancelSetRequest() {
		//		}
		//		
		@Override
		public LogicLocks getLock() {
			return Lock;
		}
		//		
		//		@Override
		//		public boolean getChangedState() {
		//			return StateChanged;
		//		}
	}
	/**************************************************************************
	 *  end of AbstractRequestLockProcessor
	 **************************************************************************/

	/**********************************************************************************
	 * Here are the three kinds of edge locks:
	 * 1. local - the lock source is only within the Vital Logic
	 * 2. global - the lock source is only Vital Logic in advance
	 * 3. merged - the advance lock and local conditions are "ored" together
	 **********************************************************************************/

	//	/**********************************************************************************
	//	 * Type 1 - local.  The only stimulus comes from the track.  Nothing
	//	 * comes from an edge in advance.  Only the track is monitored.  StateChanged
	//	 * mirrors only a change in MyLocks.
	//	 *********************************************************************************/
	//	protected class LocalRequestLockProcessor extends AbstractRequestLockProcessor {
	//
	//		public LocalRequestLockProcessor(LogicLocks lock) {
	//			super(lock);
	//		}
	//
	//		@Override
	//		public void requestLockSet() {
	//			StateChanged = false;
	//			if (!MyLocks.contains(Lock)) {
	//				OldState = MergedGlobalLocks.clone();
	//				MyLocks.add(Lock);
	//				MergedGlobalLocks.add(Lock);
	//				StateChanged = true;
	//				/******************** DEBUG code *****************************/
	//				if (Crandic.Details) {
	//					System.out.println(Identity + ": Global Locks were " + OldState.toString() + " now " + MergedGlobalLocks.toString());			
	//				}
	//			    /*************************************************************/
	//			}
	//		}
	//
	//		@Override
	//		public void requestLockClear() {
	//			StateChanged = false;
	//			if (MyLocks.contains(Lock)) {
	//				OldState = MergedGlobalLocks.clone();
	//				MyLocks.remove(Lock);
	//				MergedGlobalLocks.remove(Lock);
	//				StateChanged = true;
	//				/******************** DEBUG code *****************************/
	//				if (Crandic.Details) {
	//					System.out.println(Identity + ": Global Locks were " + OldState.toString() + " now " + MergedGlobalLocks.toString());			
	//				}
	//			    /*************************************************************/
	//			}
	//		}
	//
	//		@Override
	//		public void advanceLockSet() {
	//			// this method should never be called
	//			AdvanceLocks.add(Lock);
	//			StateChanged = false;
	//		}
	//
	//		@Override
	//		public void advanceLockClear() {
	//			// this method should never be called
	//			AdvanceLocks.remove(Lock);
	//			StateChanged = false;
	//		}		
	//	}
	//	
	//	
	/**********************************************************************************
	 * Types 1 and 2.
	 * The default lock request processor.  It does nothing but set/clear the lock.
	 * It does provide a class that can be derived for more elaborate activities and
	 * a class that can be decorated.  It is intended for local lock, advance lock,
	 * or both independent processing.
	 *********************************************************************************/
	protected class DefaultRequestLockProcessor extends AbstractRequestLockProcessor {

		public DefaultRequestLockProcessor(LogicLocks lock) {
			super(lock);
		}

		@Override
		public void requestLockSet() {
			EnumSet<LogicLocks> oldState = MyLocks.clone();
			MyLocks.add(Lock);
			/******************** DEBUG code *****************************/
			if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(MyLocks)) {
				System.out.println(Identity + ": MyLocks were " + oldState.toString() + " now " + MyLocks.toString() +
						" Advance=" + AdvanceLocks + "Merged=" + MergedGlobalLocks);			
			}
			/*************************************************************/
		}

		@Override
		public void requestLockClear() {
			EnumSet<LogicLocks> oldState = MyLocks.clone();
			MyLocks.remove(Lock);
			/******************** DEBUG code *****************************/
			if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(MyLocks)) {
				System.out.println(Identity + ": MyLocks were " + oldState.toString() + " now " + MyLocks.toString() +
						" Advance=" + AdvanceLocks + "Merged=" + MergedGlobalLocks);			
			}
			/*************************************************************/
		}

		@Override
		public void advanceLockSet() {
			EnumSet<LogicLocks> oldState = AdvanceLocks.clone();
			AdvanceLocks.add(Lock);
			/******************** DEBUG code *****************************/
			if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(AdvanceLocks)) {
				System.out.println(Identity + ": AdvanceLocks were " + oldState.toString() + " now " + AdvanceLocks.toString() +
						" MyLocks=" + MyLocks + " Merged=" + MergedGlobalLocks);			
			}
			/*************************************************************/
		}

		@Override
		public void advanceLockClear() {
			EnumSet<LogicLocks> oldState = AdvanceLocks.clone();
			AdvanceLocks.remove(Lock);
			/******************** DEBUG code *****************************/
			if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(AdvanceLocks)) {
				System.out.println(Identity + ": AdvanceLocks were " + oldState.toString() + " now " + AdvanceLocks.toString() +
						" MyLocks=" + MyLocks + " Merged=" + MergedGlobalLocks);			
			}
			/*************************************************************/
		}		
	}


	//	/**********************************************************************************
	//	 * Type 2 - global.  The only stimulus comes from a Vital Logic in advance.  Nothing
	//	 * comes from this Vital Logic.  Only the advance Vital Logic is monitored.  StateChanged
	//	 * mirrors only a change in AdvanceLocks.
	//	 *********************************************************************************/
	//	protected class AdvanceRequestLockProcessor extends AbstractRequestLockProcessor {
	//
	//		public AdvanceRequestLockProcessor(LogicLocks lock) {
	//			super(lock);
	//		}
	//
	//		@Override
	//		public void requestLockSet() {
	//			// this method should never be called
	//			MyLocks.add(Lock);
	//			StateChanged = false;
	//		}
	//
	//		@Override
	//		public void requestLockClear() {
	//			// This method should never be called
	//			MyLocks.remove(Lock);
	//			StateChanged = false;
	//		}
	//
	//		@Override
	//		public void advanceLockSet() {
	//			StateChanged = false;
	//			if (!AdvanceLocks.contains(Lock)) {
	//				OldState = MergedGlobalLocks.clone();
	//				AdvanceLocks.add(Lock);
	//				MergedGlobalLocks.add(Lock);
	//				StateChanged = true;
	//				/******************** DEBUG code *****************************/
	//				if (Crandic.Details) {
	//					System.out.println(Identity + ": Global Locks were " + OldState.toString() + " now " + MergedGlobalLocks.toString());			
	//				}
	//			    /*************************************************************/
	//			}
	//		}
	//
	//		@Override
	//		public void advanceLockClear() {
	//			StateChanged = false;
	//			if (AdvanceLocks.contains(Lock)) {
	//				OldState = MergedGlobalLocks.clone();
	//				AdvanceLocks.remove(Lock);
	//				MergedGlobalLocks.remove(Lock);
	//				StateChanged = true;
	//				/******************** DEBUG code *****************************/
	//				if (Crandic.Details) {
	//					System.out.println(Identity + ": Global Locks were " + OldState.toString() + " now " + MergedGlobalLocks.toString());			
	//				}
	//			    /*************************************************************/
	//			}
	//		}		
	//	}

	/**********************************************************************************
	 * Type 3 - merged.  The stimulus can come from either this Vital Logic or the Vital
	 * Logic in advance.  The value depends upon both.  StateChange changes when the first
	 * is set or the last cleared.  Since no checks are performed (the lock is unconditionally
	 * set/cleared) it should be preceded by a MergeFilter.
	 *********************************************************************************/
	protected class MergedRequestLockProcessor extends AbstractRequestLockProcessor {

		public MergedRequestLockProcessor(LogicLocks lock) {
			super(lock);
		}

		@Override
		public void requestLockSet() {
			//			StateChanged = false;
			//			if (!MyLocks.contains(Lock)) {
			//				if (!AdvanceLocks.contains(Lock)) {
			//					OldState = MergedGlobalLocks.clone();
			//					MergedGlobalLocks.add(Lock);
			//					StateChanged = true;
			//					/******************** DEBUG code *****************************/
			//					if (Crandic.Details) {
			//						System.out.println(Identity + ": Global Locks were " + OldState.toString() + " now " + MergedGlobalLocks.toString());			
			//					}
			//				    /*************************************************************/
			//				}
			//				MyLocks.add(Lock);
			//			}
			EnumSet<LogicLocks> oldState = MergedGlobalLocks.clone();
			MergedGlobalLocks.add(Lock);
			/******************** DEBUG code *****************************/
			if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(MergedGlobalLocks)) {
				System.out.println(Identity + ": MergedGlobalLocks were " + oldState.toString() + " now " + MergedGlobalLocks.toString() + " from merging local set");			
			}
			/*************************************************************/
		}

		@Override
		public void requestLockClear() {
			//			StateChanged = false;
			//			if (MyLocks.contains(Lock)) {
			//				if (!AdvanceLocks.contains(Lock)) {
			//					OldState = MergedGlobalLocks.clone();
			//					MergedGlobalLocks.remove(Lock);
			//					StateChanged = true;
			//					/******************** DEBUG code *****************************/
			//					if (Crandic.Details) {
			//						System.out.println(Identity + ": Global Locks were " + OldState.toString() + " now " + MergedGlobalLocks.toString());			
			//					}
			//				    /*************************************************************/
			//				}
			//				MyLocks.remove(Lock);
			//			}
			EnumSet<LogicLocks> oldState = MergedGlobalLocks.clone();
			MergedGlobalLocks.remove(Lock);
			/******************** DEBUG code *****************************/
			if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(MergedGlobalLocks)) {
				System.out.println(Identity + ": MergedGlobalLocks were " + oldState.toString() + " now " + MergedGlobalLocks.toString() + " from merging local clear");			
			}
			/*************************************************************/
		}

		@Override
		public void advanceLockSet() {
			//			StateChanged = false;
			//			if (!AdvanceLocks.contains(Lock)) {
			//				if (!MyLocks.contains(Lock)) {
			//					OldState = MergedGlobalLocks.clone();
			//					MergedGlobalLocks.add(Lock);
			//					StateChanged = true;
			//					/******************** DEBUG code *****************************/
			//					if (Crandic.Details) {
			//						System.out.println(Identity + ": Global Locks were " + OldState.toString() + " now " + MergedGlobalLocks.toString());			
			//					}
			//				    /*************************************************************/
			//				}
			//				AdvanceLocks.add(Lock);
			//			}
			EnumSet<LogicLocks> oldState = MergedGlobalLocks.clone();
			MergedGlobalLocks.add(Lock);
			/******************** DEBUG code *****************************/
			if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(MergedGlobalLocks)) {
				System.out.println(Identity + ": MergedGlobalLocks were " + oldState.toString() + " now " + MergedGlobalLocks.toString() + " from merging advance set");			
			}
			/*************************************************************/
		}

		@Override
		public void advanceLockClear() {
			//			StateChanged = false;
			//			if (AdvanceLocks.contains(Lock)) {
			//				if (!MyLocks.contains(Lock)) {
			//					OldState = MergedGlobalLocks.clone();
			//					MergedGlobalLocks.remove(Lock);
			//					StateChanged = true;
			//					/******************** DEBUG code *****************************/
			//					if (Crandic.Details) {
			//						System.out.println(Identity + ": Global Locks were " + OldState.toString() + " now " + MergedGlobalLocks.toString());			
			//					}
			//				    /*************************************************************/
			//				}
			//				AdvanceLocks.remove(Lock);
			//			}
			EnumSet<LogicLocks> oldState = MergedGlobalLocks.clone();
			MergedGlobalLocks.remove(Lock);
			/******************** DEBUG code *****************************/
			if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(MergedGlobalLocks)) {
				System.out.println(Identity + ": MergedGlobalLocks were " + oldState.toString() + " now " + MergedGlobalLocks.toString() + " from merging advance clear");			
			}
			/*************************************************************/
		}		
	}

	/**********************************************************************************
	 * An abstract class from which other LockProcessor decorators are derived.
	 * It, too, provides some utility methods which derived classes can use.
	 *********************************************************************************/
	protected abstract class AbstractProcessorDecorator implements LockRequestProcessor {

		// The LockRequestProcessor being decorated
		protected final LockRequestProcessor Processor;

		// The lock associated with the LockProcessor being decorated
		protected final LogicLocks Lock;

		/**
		 * the constructor
		 * @param processor is the LockRequestProcess being decorated
		 */
		public AbstractProcessorDecorator(LockRequestProcessor processor) {
			Processor = processor;
			Lock = Processor.getLock();
		}

		@Override
		abstract public void requestLockClear();

		@Override
		abstract public void requestLockSet();

		@Override
		abstract public void advanceLockSet();

		@Override
		abstract public void advanceLockClear();

		@Override
		public boolean isLockSet() {
			return Processor.isLockSet();
		}

		@Override
		@Deprecated
		public LogicLocks getLock() {
			return Lock;
		}
	}
	/**************************************************************************
	 *  end of AbstractProcessorDecorator
	 **************************************************************************/

	/**********************************************************************************
	 * The default decorator, from which other decorators can be derived.  It just invokes
	 * the decorated processor.  Consequently, it provides a pass-through for methods
	 * that are not overridden.
	 *********************************************************************************/
	protected class DefaultProcessorDecorator extends AbstractProcessorDecorator {

		/**
		 * the constructor
		 * @param processor is the LockRequestProcess being decorated
		 */
		public DefaultProcessorDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
		}

		@Override
		public void requestLockClear() {
			Processor.requestLockClear();
		}

		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
		}
	}
	/**************************************************************************
	 *  end of DefaultProcessorDecorator
	 **************************************************************************/

	/**********************************************************************************
	 * A decorator that filters out redundant changes in both MyLocks or AdvanceLocks.
	 * If the value of the corresponding lock does not change by the method being
	 * invoked, then nothing happens.  However, if the lock does change, then the
	 * decorated processor is invoked and the change is made in the base request lock
	 * processor.
	 * <p>
	 * Because redundant actions may be ignored and changing the lock is the final step,
	 * the decorated processors do not have to take these steps.  Furthermore, this
	 * decorator should be the last added to a decoration chain (first invoked).  Other decorators can
	 * follow the chain either before or after taking their actions.
	 * <p>
	 * Pay particular attention to the order:
	 * 1. lock set is tested
	 * 2. decorated processor is called
	 **********************************************************************************/
	protected class LockFilterDecorator extends AbstractProcessorDecorator {
		/**
		 * the constructor
		 * @param processor is the LockRequestProcess being decorated
		 */
		public LockFilterDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			if (!MyLocks.contains(Lock)) {
				//				EnumSet<LogicLocks> oldState = MyLocks.clone();
				Processor.requestLockSet();
				//				/******************** DEBUG code *****************************/
				//				if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(MyLocks)) {
				//					System.out.println(Identity + ": MyLocks were " + oldState.toString() + " now " + MyLocks.toString());			
				//				}
				//			    /*************************************************************/
			}
		}

		@Override
		public void requestLockClear() {
			if (MyLocks.contains(Lock)) {
				//				EnumSet<LogicLocks> oldState = MyLocks.clone();
				Processor.requestLockClear();
				//				/******************** DEBUG code *****************************/
				//				if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(MyLocks)) {
				//					System.out.println(Identity + ": MyLocks were " + oldState.toString() + " now " + MyLocks.toString());			
				//				}
				//			    /*************************************************************/
			}
		}

		@Override
		public void advanceLockSet() {
			if (!AdvanceLocks.contains(Lock)) {
				//				EnumSet<LogicLocks> oldState = AdvanceLocks.clone();
				Processor.advanceLockSet();
				//				/******************** DEBUG code *****************************/
				//				if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(AdvanceLocks)) {
				//					System.out.println(Identity + ": AdvanceLocks were " + oldState.toString() + " now " + AdvanceLocks.toString());			
				//				}
				//			    /*************************************************************/
			}
		}

		@Override
		public void advanceLockClear() {
			if (AdvanceLocks.contains(Lock)) {
				//				EnumSet<LogicLocks> oldState = AdvanceLocks.clone();
				Processor.advanceLockClear();
				//				/******************** DEBUG code *****************************/
				//				if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(AdvanceLocks)) {
				//					System.out.println(Identity + ": AdvanceLocks were " + oldState.toString() + " now " + AdvanceLocks.toString());			
				//				}
				//			    /*************************************************************/
			}
		}
	}
	/**************************************************************************
	 *  end of LockFilterDecorator
	 **************************************************************************/

	/**********************************************************************************
	 * A decorator that ignores changes in AdvanceLocks only.  Any sets or clears
	 * are dropped without changing the locks.
	 **********************************************************************************/
	protected class IgnoreAdvanceLocksDecorator extends LockFilterDecorator {
		/**
		 * the constructor
		 * @param processor is the LockRequestProcess being decorated
		 */
		public IgnoreAdvanceLocksDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void advanceLockSet() {
		}

		@Override
		public void advanceLockClear() {
		}
	}
	/**************************************************************************
	 *  end of IgnoreAdvanceLocksDecorator
	 **************************************************************************/

	/**********************************************************************************
	 * A decorator that filters out processing changes in AdvanceLocks only.  Changes to
	 * MyLocks are passed on to enclosed decorators in the chain.  In contrast to
	 * IgnoreAdvanceLocksDecorator, the AdanceLock is set or cleared.
	 **********************************************************************************/
	protected class AdvanceLocksFilterDecorator extends DefaultProcessorDecorator {
		/**
		 * the constructor
		 * @param processor is the LockRequestProcess being decorated
		 */
		public AdvanceLocksFilterDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void advanceLockSet() {
			AdvanceLocks.add(Lock);
		}

		@Override
		public void advanceLockClear() {
			AdvanceLocks.remove(Lock);
		}
	}
	/**************************************************************************
	 *  end of AdvanceLocksFilterDecorator
	 **************************************************************************/

	/**********************************************************************************
	 * A decorator that filters out processing changes in MyLocks only.  Changes to
	 * AdvanceLocks are passed on to enclosed decorators in the chain.  MyLocks is set
	 * or cleared.
	 **********************************************************************************/
	protected class MyLocksFilterDecorator extends LockFilterDecorator {
		/**
		 * the constructor
		 * @param processor is the LockRequestProcess being decorated
		 */
		public MyLocksFilterDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			MyLocks.add(Lock);
		}

		@Override
		public void requestLockClear() {
			MyLocks.remove(Lock);
		}
	}
	/**************************************************************************
	 *  end of MyLocksFilterDecorator
	 **************************************************************************/

	/**********************************************************************************
	 * A decorator that filters out redundant changes in merging MyLocks and AdvanceLocks.
	 * If changing either changes the merged locks, then the appropriate method in the decorated
	 * lock processor is invoked.
	 * <p>
	 * The placement of this decorator in the decoration chain is critical.  Actions which
	 * must be taken on individual changes should come before this filter in the chain.
	 **********************************************************************************/
	protected class MergedLockFilterDecorator extends AbstractProcessorDecorator {
		/**
		 * the constructor
		 * @param processor is the LockRequestProcess being decorated
		 */
		public MergedLockFilterDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			if (!AdvanceLocks.contains(Lock)) {
				EnumSet<LogicLocks> oldState = MergedGlobalLocks.clone();
				Processor.requestLockSet();
				/******************** DEBUG code *****************************/
				if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(MergedGlobalLocks)) {
					System.out.println(Identity + ": Merged Locks were " + oldState.toString() + " now " + MergedGlobalLocks.toString() + " from setting local locks.");			
				}
				/*************************************************************/
			}
			MyLocks.add(Lock);
		}

		@Override
		public void requestLockClear() {
			if (!AdvanceLocks.contains(Lock)) {
				EnumSet<LogicLocks> oldState = MergedGlobalLocks.clone();
				Processor.requestLockClear();
				/******************** DEBUG code *****************************/
				if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(MergedGlobalLocks)) {
					System.out.println(Identity + ": Merged Locks were " + oldState.toString() + " now " + MergedGlobalLocks.toString() + " from clearing local locks.");			
				}
				/*************************************************************/
			}
			MyLocks.remove(Lock);
		}

		@Override
		public void advanceLockSet() {
			if (!MyLocks.contains(Lock)) {
				EnumSet<LogicLocks> oldState = MergedGlobalLocks.clone();
				Processor.advanceLockSet();
				/******************** DEBUG code *****************************/
				if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(MergedGlobalLocks)) {
					System.out.println(Identity + ": Merged Locks were " + oldState.toString() + " now " + MergedGlobalLocks.toString() + " from setting advance locks.");			
				}
				/*************************************************************/
			}
			AdvanceLocks.add(Lock);
		}

		@Override
		public void advanceLockClear() {
			if (!MyLocks.contains(Lock)) {
				EnumSet<LogicLocks> oldState = MergedGlobalLocks.clone();
				Processor.advanceLockClear();
				/******************** DEBUG code *****************************/
				if (Crandic.Details.get(DebugBits.VITALLOGICLOCKBIT) && !oldState.equals(MergedGlobalLocks)) {
					System.out.println(Identity + ": Merged Locks were " + oldState.toString() + " now " + MergedGlobalLocks.toString() + " from clearing advance locks.");			
				}
				/*************************************************************/
			}
			AdvanceLocks.remove(Lock);
		}
	}

	/**********************************************************************************
	 * The LockPropagationDecorator
	 *
	 * This decorator to a RequestLockDecorator is for those Locks that are forwarded
	 * to approach edges.  Though intended for forwarding the state of an unsignaled
	 * BlkEdge to a protecting signal edge, it can be used
	 * for Intermediates and ControlPoints.
	 * 
	 * DESIGN WARNING: do not pass Lock.  Use getLock() so that RouteLockConverter
	 * can propagate a different lock!!!
	 **********************************************************************************/
	protected class LockPropagationDecorator extends AbstractProcessorDecorator {

		/**
		 * the constructor
		 * @param processor is the LockRequestProcess being decorated
		 */
		public LockPropagationDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
			setApproachLogicLock(getLock());
		}

		@Override
		public void requestLockClear() {
			Processor.requestLockClear();
			clearApproachLogicLock(getLock());
		}

		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
			setApproachLogicLock(getLock());
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
			clearApproachLogicLock(getLock());
		}
	}

	/**************************************************************************
	 *  end of LockPropagationDecorator
	 **************************************************************************/

	/**********************************************************************************
	 * This Lock Decorator is for setting and clearing block occupancy - detection -
	 * for disciplines that support routes and fleeting (i.e. ABS, CTC, and DTC).
	 **********************************************************************************/
	protected class CommonDetectionDecorator extends AbstractProcessorDecorator {

		public CommonDetectionDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			if (AdvanceLocks.contains(LogicLocks.ROUTELOCK)) {
				setLocalLock(LogicLocks.ROUTELOCK);
			}
			if (AdvanceLocks.contains(LogicLocks.PROTECTIONLOCK)) {
				setLocalLock(LogicLocks.PROTECTIONLOCK);
			}
			Processor.requestLockSet();
		}

		@Override
		public void requestLockClear() {
			Processor.requestLockClear();
			if (MyLocks.contains(LogicLocks.ROUTELOCK)) {
				clearLocalLock(LogicLocks.ROUTELOCK);
			}
			if (MyLocks.contains(LogicLocks.PROTECTIONLOCK)) {
				clearLocalLock(LogicLocks.PROTECTIONLOCK);
			}
		}

		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();			
		}
	}
	/**************************************************************************
	 *  end of CommonDetectionLockProcessor
	 **************************************************************************/

	/**********************************************************************************
	 * This decorator to a RequestLockProcessor is for those local Locks (only) that
	 * report changes to the GUI.  Thus, they augment the default by sending
	 * their state change to the code line.  An instance is an implied filter on local
	 * locks - if the decorator does not exist, then no lock change is sent to the GUI.
	 **********************************************************************************/
	protected class LocalCodeLockDecorator extends AbstractProcessorDecorator {

		/**
		 * the code line identity of the lock
		 */
		protected final Codes CodeId;

		/**
		 * the constructor
		 * @param processor is the LockRequestProcess being decorated
		 */
		public LocalCodeLockDecorator(LockRequestProcessor processor) {
			super(processor);
			CodeId = Codes.valueOf(Lock.toString());
		}

		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
			Transmitter.createPacket(CodePurpose.INDICATION, CodeId, Constants.TRUE);
		}

		@Override
		public void requestLockClear() {
			Processor.requestLockClear();
			Transmitter.createPacket(CodePurpose.INDICATION, CodeId, Constants.FALSE);
		}

		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
		}
	}	
	/**************************************************************************
	 *  end of LocalCodeLockDecorator
	 **************************************************************************/
	/**********************************************************************************
	 * This LockRequestProcessor is a temporary lock decorator for locks that need
	 * a place to attach a break point.  It should not be included in any production
	 * processing chains.
	 * 
	 * It can be used for changes in both MyLocks and AdvanceLocks.
	 **********************************************************************************/
	protected class DebugLockDecorator extends AbstractProcessorDecorator {

		/**
		 * the constructor
		 * @param processor is the LockRequestProcessor being decorated
		 * @param trigger is the IOSpec for the external lock
		 */
		public DebugLockDecorator(LockRequestProcessor processor) {
			super(processor);
		}

		@Override
		public void requestLockSet() {
			System.out.println("Local lock " + Lock + " set");
			Processor.requestLockSet();
		}

		@Override
		public void requestLockClear() {
			System.out.println("Local lock " + Lock + " cleared");
			Processor.requestLockClear();
		}

		@Override
		public void advanceLockSet() {
			System.out.println("Advance lock " + Lock + " set");
			Processor.advanceLockSet();
		}

		@Override
		public void advanceLockClear() {
			System.out.println("Local lock " + Lock + " cleared");
			Processor.advanceLockClear();
		}
	}
	/**************************************************************************
	 *  end of DebugLockDecorator
	 **************************************************************************/


	/**********************************************************************************
	 * This LockRequestProcessor is for Locks which have visibility outside of CATS.
	 * Logix can monitor these locks for changes and trigger changes.  The LockProcessor
	 * is based on the Decorator design pattern so that any LockProcessor can be
	 * adapted to interface to the external world.
	 * 
	 * It can be used for changes in both MyLocks and AdvanceLocks.
	 **********************************************************************************/
	protected class ExternalLockDecorator extends AbstractProcessorDecorator implements BiDecoderObserver {

		// The IOSpec that is the external Lock
		protected final IOSpec ExternalLock;

		/**
		 * the constructor
		 * @param processor is the LockRequestProcessor being decorated
		 * @param trigger is the IOSpec for the external lock
		 */
		public ExternalLockDecorator(LockRequestProcessor processor, IOSpec trigger) {
			super(processor);
			ExternalLock = trigger;
		}

		@Override
		public void acceptIOEvent() {
			Processor.requestLockSet();
		}

		@Override
		public void acceptOtherIOEvent() {
			Processor.requestLockClear();
		}

		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
			ExternalLock.sendCommand();
		}

		@Override
		public void requestLockClear() {
			Processor.requestLockClear();
			ExternalLock.sendUndoCommand();
		}

		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
		}
	}
	/**************************************************************************
	 *  end of ExternalLockDecorator
	 **************************************************************************/

	/**********************************************************************************
	 * This LockRequestProcessor is another LockRequestProcessor decorator.
	 * The decoration allows an object to listen for changes to the lock.  It alerts
	 * that object via AcceptIOEvent and AcceptOtherIOEvent method calls.  Thus, that
	 * object can attach to this object or to IOSpec objects.
	 * <p>
	 * This class registers only one listener, unlike the listener pattern.  The reasons
	 * for this are:
	 * 1. it will be rare to have a single listener, let alone multiple
	 * 2. because the EventLockDecorator may not be at the top of the chain for the second
	 * listener, I would have to search the chain whenever one is instantiated.  It
	 * probably is not worth the effort.
	 * 
	 * This decorator can be used with changes in both MyLocks and AdvanceLocks
	 **********************************************************************************/
	protected class EventLockDecorator extends AbstractProcessorDecorator {

		/**
		 * the list of listeners
		 */
		private final BiDecoderObserver Listener;

		/**
		 * the constructor
		 * @param processor is the LockRequestProcessor being decorated
		 * @param listener is the object watching for changes
		 */
		public EventLockDecorator(LockRequestProcessor processor, BiDecoderObserver listener) {
			super(processor);
			Listener = listener;
		}

		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
			Listener.acceptIOEvent();
		}

		@Override
		public void requestLockClear() {
			Processor.requestLockClear();
			Listener.acceptOtherIOEvent();
		}

		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
		}
	}
	/**************************************************************************
	 *  end of EventLockDecorator
	 **************************************************************************/

	/**********************************************************************************
	 * This LockRequestProcessor is another LockRequestProcessor decorator.
	 * The decoration allows locks on either MyLocks or CommonLocks to affect crossing tracks.
	 **********************************************************************************/
	protected class CrossingLockDecorator extends AbstractProcessorDecorator {

		/**
		 * the Vital Logic for the crossing
		 */
		final private CrossingInterface Crossing;

		/**
		 * the constructor
		 * @param processor is the LockRequestProcessor being decorated
		 * @param crossing is the Vital Logic protecting the crossing
		 */
		public CrossingLockDecorator(final LockRequestProcessor processor, final CrossingInterface crossing) {
			super(processor);
			Crossing = crossing;
		}

		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
			Crossing.blockCrossTraffic(Lock);
		}

		@Override
		public void requestLockClear() {
			Processor.requestLockClear();
			Crossing.unblockCrossTraffic(Lock);
		}

		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
			Crossing.blockCrossTraffic(Lock);
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
			Crossing.unblockCrossTraffic(Lock);
		}
	}
	/**************************************************************************
	 *  end of CrossingLockDecorator
	 **************************************************************************/
	
	/**********************************************************************************
	 * This LockRequestProcessor is another LockRequestProcessor decorator.
	 * The decoration allows locks on only MyLocks to affect crossing tracks.
	 **********************************************************************************/
	protected class MyLocksCrossingLockDecorator extends CrossingLockDecorator {

		/**
		 * the constructor
		 * @param processor is the LockRequestProcessor being decorated
		 * @param crossing is the Vital Logic protecting the crossing
		 */
		public MyLocksCrossingLockDecorator(final LockRequestProcessor processor, final CrossingInterface crossing) {
			super(processor, crossing);
		}
		
		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
		}

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
		}
	}
	/**************************************************************************
	 *  end of CrossingLockDecorator
	 **************************************************************************/

	/**********************************************************************************
	 * This LockRequestProcessor is another LockRequestProcessor decorator.
	 * The decoration injects a filter into the processing chain to dump the locks when
	 * a debug flag is set
	 **********************************************************************************/
	protected class LockDumpDecorator extends AbstractProcessorDecorator {

		/**
		 * the Vital Logic for the crossing
		 */
		final private int DebugFlag;

		/**
		 * the constructor
		 * @param processor is the LockRequestProcessor being decorated
		 * @param debugFlag is the name of the flag controlling tracing
		 */
		public LockDumpDecorator(final LockRequestProcessor processor, final int debugFlag) {
			super(processor);
			DebugFlag = debugFlag;
		}

		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
            if (Crandic.Details.get(DebugFlag)) {
            	log.info(Constants.ROUTE_CREATION +  "Vital logic " + BasicVitalLogic.this.Identity + " Setting " + Lock + " Locks=" + MyLocks + " AdvanceLocks=" + AdvanceLocks +
            			" MergedLocks=" + MergedGlobalLocks);
            }
		}

		@Override
		public void requestLockClear() {
			Processor.requestLockClear();
            if (Crandic.Details.get(DebugFlag)) {
            	log.info(Constants.ROUTE_CREATION +  "Vital logic " + BasicVitalLogic.this.Identity + " Clearing " +  Lock + " Locks=" + MyLocks + "  AdvanceLocks=" + AdvanceLocks +
            			" MergedLocks=" + MergedGlobalLocks);
            }
		}

		@Override
		public void advanceLockSet() {
			Processor.advanceLockSet();
            if (Crandic.Details.get(DebugFlag)) {
            	log.info(Constants.ROUTE_CREATION +  "Vital logic " + BasicVitalLogic.this.Identity + "Setting advance " + Lock + " Locks=" + MyLocks + " AdvanceLocks=" + AdvanceLocks +
            			" MergedLocks=" + MergedGlobalLocks);
            }
        }

		@Override
		public void advanceLockClear() {
			Processor.advanceLockClear();
            if (Crandic.Details.get(DebugFlag)) {
            	log.info(Constants.ROUTE_CREATION +  "Vital logic " + BasicVitalLogic.this.Identity + " Clearing advance " + Lock + " Locks=" + MyLocks + " AdvanceLocks=" + AdvanceLocks +
            			" MergedLocks=" + MergedGlobalLocks);
            }
		}
	}
	/**************************************************************************
	 *  end of CrossingLockDecorator
	 **************************************************************************/
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
        BasicVitalLogic.class.getName());
}
/* @(#)BasicVitalLogic.java */
