/* Name: FrogVitalLogic.java
 *
 * What:
 * This file contains the definition for the VitalLogic class associated with the Frog
 * end of a section of track.  The track has a FrogEdge
 * at one end and any other kind of edge at the other.  The FrogEdge is tightly linked to
 * a PtsEdge which maintains how the turnout is lined.  The FrogVitalLogic is mostly passive -
 * it responds to actions from other objects.  Specifically, its ApproachLogic is around
 * the PtsVitalLogic.  The FrogVitalLogic is oriented on the exit route from the
 * points; thus, it receives lock changes from one leg of the turnout and passes them around
 * the points vital logic, when lined for this route.  When not lined, it holds the
 * up stream state to refresh the points when lined.  It can distinguish between being lined
 * or not by the Fouling field.  If set, it is fouling.  It not set, it is lined.
 * <p>
 * In general, the FrogVitalLogic behaves like BasicVitalLogic, except it receives
 * locks that flow in the opposite direction from the PtsVitalLogic and passes them
 * in the advance direction. Like BasicVitalLogic, a FrogEdge does not have a GUI
 * interface; thus, MergedGlobalLocks and MergedLocalLocks are not used.
 * <p>
 * The second thing that the FrogVitalLogic holds is the identity of the next
 * VitalLogic and Signal in the upstream direction (in advance).  This is provided
 * to the points VitalLogic as the latter's ApproachLogic and ApproachSignal.
 * <p>
 * Thus,
 * <ul>
 * <li>AdvanceLocks are the standard usages - locks which feed into the Frog locks, via
 * the normal direction of propagation flow (from the frog end).  They are held or
 * passed to the PtsVitalLogic</li>
 * <li>ProxySignal is owned by the frog as a receiver from the advance signal.  Its state
 * changes are held or passed through the points SignalVitalLogic</li>
 * <li>ApproachLocks are the locks forwarded through the PtsVitalLogic.  The locks sent
 * the other direction come from the PtsVitalLogic or are simply ConflictingSignal</li>
 * <li>ApproachLogic is the VitalLogic that is fed by the act of relaying lock changes
 * from the points.  This can be used because the normal ApproachLogic is the PtsVitalLogic.</li>
 * <li>AdvanceSignal is the upstream signal, in the opposite direction</li>
 * </ul>
 */
package cats.layout.vitalLogic;

import cats.layout.items.Track;
import cats.trackCircuit.RelayTrackCircuit;
import cats.trackCircuit.TrackCircuit;

/**
 * This file contains the definition for the VitalLogic class associated with the Frog
 * end of a section of track.  The track has a FrogEdge
 * at one end and any other kind of edge at the other.  The FrogEdge is tightly linked to
 * a PtsEdge which maintains how the turnout is lined.  The FrogVitalLogic is mostly passive -
 * it responds to actions from other objects.  Specifically, its ApproachLogic is
 * the PtsVitalLogic.  The FrogVitalLogic is oriented on the exit route from the
 * points; thus, it receives lock changes from one leg of the turnout and passes them to
 * the points vital logic, when lined for this route.  When not lined, it holds the
 * up stream state to refresh the points when lined.  It can distinguish between being lined
 * or not by the Fouling field.  If set, it is fouling.  It not set, it is lined.
 * <p>
 * In general, the FrogVitalLogic behaves like BasicVitalLogic, except the PtsVitalLogic
 * bypasses the FrogVitalLogic for Points->Frog traffic. Like BasicVitalLogic, a FrogEdge does not have a GUI
 * interface; thus, MergedGlobalLocks and MergedLocalLocks are not used.  FrogVitalLogic
 * do not use MyLocks because they have no stimulus to inject into the flow in either
 * direction.
 * <p>
 * The second thing that the FrogVitalLogic holds is the identity of the next
 * VitalLogic and TrackCircuit in the upstream direction (Points->Frog).  This is provided
 * to the points via the Frog's PeerLogic.
 * <p>
 * Thus,
 * <ul>
 * <li>MyLocks, AdvanceLocks, MergedGlobalLocks, setLocalLock(), clearLocalLock(),
 * setAdvanceLock(), clearAdvanceLock are the standard usages - locks which feed
 * into the Frog locks, via the normal direction of propagation flow (from the frog end).
 * They are held or passed to the PtsVitalLogic</li>
 * <li>FedCircuit is owned by the frog as a receiver from the advance signal.  Its state
 * changes are held or passed through the points Trackcircuit</li>
 * <li>ApproachLocks are not used.  MergedGlobalLocks holds the locks queued for the Points</li>
 * <li>ApproachLogic is the VitalLogic that is fed by the act of relaying lock changes
 * from the points.  This can be used because the normal ApproachLogic is the PtsVitalLogic.</li>
 * <li>AdvanceCircuit is the upstream TrackCircuit, in the opposite direction</li>
 * <li>PeerLogic is the VitalLogic at the other end of the track, as is the usual definition</li>
 * 
 * </ul>
 * @see cats.layout.items.FrogEdge
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014, 2015, 2016, 2018, 2019</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class FrogVitalLogic extends BasicVitalLogic {

	/**
	 * the PtsVitalLogic.  It is final.  Fouling is the
	 * flag for indicating if the points are lined to this route or not.
	 */
	private final PtsVitalLogic Points;

//	/**
//	 * the SignalVitalLogic used to relay the aspect from the advance
//	 * signal (the advance signal in the normal direction of propagation)
//	 * to the approach signal (the points signal).
//	 */
//	private SignalVitalLogic ProxySignal;
//
//	/**
//	 * the SignalVitalLogic fed by the Points ProxySignal.  This is the signal
//	 * behind the frog, but in the opposite direction of normal propagation.
//	 * It could be the signal at the opposite end of the track.
//	 */
//	private SignalVitalLogic AdvanceSignal;

	/**
	 * the Track Circuit fed by the TrackCircuit in advance - Frog->Points
	 */
	private final RelayTrackCircuit FedCircuit; 
	
	/**
	 * The relay for forwarding TrackCircuit changes from the Points
	 * Points->Frog
	 */
	private RelayTrackCircuit CircuitRelay;
	
//	/**
//	 * The relay for forwarding route locks from the signal in advance
//	 * through the Points. It will remain fixed.
//	 */
//	private SignalVitalLogic SignalRelay;
//	
//	/**
//	 * the route locks from the SignalVitalLogic in advance
//	 */
//	protected EnumSet<LogicLocks> AdvanceRouteLocks = EnumSet.noneOf(LogicLocks.class);
	
//	/**
//	 * the merger of the MergedGlobalLocks and AdvanceRouteLocks
//	 */
//	protected EnumSet<LogicLocks> MergedRouteLocks = EnumSet.noneOf(LogicLocks.class);
	
	/**
	 * the speed received from the tracks in approach to the points
	 */
	private int OpposingSpeed = Track.NORMAL;
	
	/**
	 * is true when the points are fouling
	 */
	private boolean Fouling = true;
	
	/**
	 * the ctor
	 * @param pts is the PtsVitalLogic connected to teh FrogVitalLogic
	 * @param id is the identity of the FrogVitalLogic
	 */
//	public FrogVitalLogic(PacketFactory transmitter, PtsVitalLogic pts, String id) {
//		super(transmitter, id);
	public FrogVitalLogic(PtsVitalLogic pts, String id) {
		super(null, id);
		Points = pts;
		FedCircuit = new RelayTrackCircuit(id);
//		LockProcessor.put(LogicLocks.ROUTELOCK, new FrogExitRequestProcessor());
	}

	@Override
	public void configureLockProcessing() {	
		// NOTE: Frog never receive a local lock request, so MyLocks is never used
		// DETECTIONLOCK
		// CONFLICTINGSIGNALLOCK
		// HOLDLOCK
		// SWITCHUNLOCK
		// APPROACHLOCK
		// TIMELOCK
		// TRAFFICLOCK
		// ROUTELOCK
		// PROTECTIONLOCK
		// MAINTENANCELOCK
		// CALLONLOCK
		// FLEETLOCK
		// EXTERNALLOCK
	}
	
//	/**
//	 * is invoked to tell the VitalLogic where its signal is.
//	 * @param newHead is the signal
//	 */
//	public void setSignalHead(final SignalVitalLogic newHead) {
//		ProxySignal = newHead;
//	}

	@Override
	public void mergeSpeeds(final int advSpeed) {
		int speed = MergedSpeed;
		AdvanceSpeed = advSpeed;
		MergedSpeed = Track.getSlower(Speed, AdvanceSpeed);
		if ((speed != MergedSpeed) && !Fouling) {
			Points.mergeFrogSpeed(MergedSpeed);
		}
	}

	/**
	 * accepts the speed from the tracks in approach to the
	 * points and passes it along to the VitalLogic at the
	 * other end of the tracks - the opposite direction
	 * @param speed is the speed of the tracks in approach
	 * of the points
	 */
	void mergeOpposingSpeed(final int speed) {
		OpposingSpeed = speed;
		if (PeerLogic != null) {
			PeerLogic.mergeSpeeds(Track.getSlower(Speed, OpposingSpeed));
		}
	}
	
//	/**
//	 * tells the embedded signal where its approach signal is
//	 * @param target is the approach signal
//	 */
//	public void setProxyTargetSignal(final SignalVitalLogic target) {
//		ProxySignal.setApproachSignal(target);
//	}
//
//	/**
//	 * sets the approach signal for aspects flowing against
//	 * the normal traffic
//	 * @param advance is the advance signal
//	 */
//	public void setAdvanceSignal(final SignalVitalLogic advance) {
//		AdvanceSignal = advance;
//	}
//
//	/**
//	 * retrieves the SignalVitalLogic which relays aspects
//	 * from the advance (up stream) direction to the
//	 * approach
//	 * @return the virtual signal.  It should never be null.
//	 */
//	public SignalVitalLogic getAdvanceSignal() {
//		return AdvanceSignal;
//	}

//	/**
//	 * tells the embedded TrackCircuit where its approach circuit can be found.
//	 * @param target is the approach TrackCircuit
//	 */
//	public void setProxyTargetCircuit(final TrackCircuit target) {
//		CircuitRelay = target;
//	}
	
	@Override
	protected TrackCircuit trackCircuitProbe() {		
//		// this keeps the probe going through the Points
//		if (Points != null) {
//			Points.pointsCircuitProbe();
//		}
		return FedCircuit;
	}
	
	/**
	 * continues a TrackCircuit probe from the Points in order to locate
	 * the TrackCircuit fed by the Points when lined for this route.
	 */
	protected void proxyCircuitProbe() {
		TrackCircuit probe;
//		CircuitRelay = (PeerLogic == null) ? null : PeerLogic.trackCircuitProbe();
		if (PeerLogic != null) {
			probe = PeerLogic.trackCircuitProbe();
			if (probe != null) {
				CircuitRelay = new RelayTrackCircuit(Identity);
				CircuitRelay.setApproachTrackCircuit(probe);
			}
		}
	}
	
//	@Override
//	protected SignalVitalLogic signalVitalLogicProbe() {
//		// this keeps the probe going through the Points
//		if (Points != null) {
//			Points.pointsSignalProbe();
//		}
//		return this;
//	}
//	
//	/**
//	 * continues a signal probe from the Points in order to locate
//	 * the SignalVitalLogic fed by the Points when lined for this route.
//	 */
//	protected void proxySignalProbe() {
//		SignalRelay = (PeerLogic == null) ? null : PeerLogic.signalVitalLogicProbe();
//	}
	
	@Override
	public void setLocalLock(final LogicLocks l) {
//		if (!MyLocks.contains(l)) {
//			LockRequestProcessor processor = LockProcessor.get(l);
//			if (processor != null) {
//				processor.requestLockSet();
//			}
//			else {
//				MyLocks.add(l);
//				if (mergeGlobalLocks(l)) {
//					if (!Fouling && (Points.ApproachLogic != null)) {
//							Points.ApproachLogic.setLocalLock(l);
//					}
//				}
//			}
//		}
		MyLocks.add(l);
		if (!AdvanceLocks.contains(l)) {
			MergedGlobalLocks.add(l);
//			if (!Fouling && (Points.ApproachLogic != null)) {
//				Points.ApproachLogic.setAdvanceLock(l);
//			}
			if (!Fouling ) {
				Points.setAdvanceLogicLock(l);
			}
		}
	}
	
	@Override
	public void clearLocalLock(LogicLocks l) {
//		if (MyLocks.contains(l)) {
//			LockRequestProcessor processor = LockProcessor.get(l);
//			if (processor != null) {
//				processor.requestLockClear();
//			}
//			else {
//				MyLocks.remove(l);
//				if (mergeGlobalLocks(l)) {
//					if (!Fouling && (Points.ApproachLogic != null)) {
//						Points.ApproachLogic.clearLocalLock(l);
//					}
//				}
//			}
//		}
		MyLocks.remove(l);
		if (!AdvanceLocks.contains(l)) {
			MergedGlobalLocks.remove(l);
//			if (!Fouling && (Points.ApproachLogic != null)) {
//				Points.ApproachLogic.clearAdvanceLock(l);
//			}
			if (!Fouling) {
				Points.clearAdvanceLogicLock(l);
			}
		}
	}
	
	@Override
	protected void setAdvanceLock(final LogicLocks l) {
//		LockRequestProcessor processor = LockProcessor.get(l);
//		if (processor != null) {
//			processor.advanceLockSet();
//		}
//		else {
//			AdvanceLocks.add(l);
//			setApproachLock(l);
//		}
		AdvanceLocks.add(l);
		if (!MyLocks.contains(l)) {
			MergedGlobalLocks.add(l);
//			if (!Fouling && (Points.ApproachLogic != null)) {
//				Points.ApproachLogic.setAdvanceLock(l);
//			}
			if (!Fouling) {
				Points.setAdvanceLogicLock(l);
			}
		}
	}

	@Override
	protected void clearAdvanceLock(final LogicLocks l) {
//		LockRequestProcessor processor = LockProcessor.get(l);
//		if (processor != null) {
//			processor.advanceLockClear();
//		}
//		else {
//			AdvanceLocks.remove(l);
//			clearApproachLock(l);
//		}
		AdvanceLocks.remove(l);
		if (!MyLocks.contains(l)) {
			MergedGlobalLocks.remove(l);
//			if (!Fouling && (Points.ApproachLogic != null)) {
//				Points.ApproachLogic.clearAdvanceLock(l);
//			}
			if (!Fouling) {
				Points.clearAdvanceLogicLock(l);
			}
		}
	}

	@Override
	protected void setApproachLogicLock(final LogicLocks l) {
//		if (!ApproachLocks.contains(l) && !Fouling && (Points.ApproachLogic != null)) {
//			ApproachLocks.add(l);
//			Points.ApproachLogic.setAdvanceLock(l);
//		}
//		if (!ApproachLocks.contains(l)) {
			if (!Fouling) {
				Points.setAdvanceLogicLock(l);
			}
//			ApproachLocks.add(l);
//		}
	}

	@Override
	protected void clearApproachLogicLock(final LogicLocks l) {
//		if (ApproachLocks.contains(l) && !Fouling && (Points.ApproachLogic != null)) {
//			ApproachLocks.remove(l);
//			Points.ApproachLogic.clearAdvanceLock(l);
//		}
//		if (ApproachLocks.contains(l)) {
			if (!Fouling) {
				Points.clearAdvanceLogicLock(l);
			}
//			ApproachLocks.remove(l);
//		}
	}

//	/**
//	 * this method receives notification from the Points VitalLogic that a Lock
//	 * has been set.  Locks arriving from the Points affect only MergedGlobalLocks
//	 * and are passed on to the ApproachLogic.
//	 * @param lock is the Lock that was set
//	 */
//	void pointsLockSet(final LogicLocks lock) {
//		if (ApproachLogic != null) {
//			ApproachLogic.setAdvanceLock(lock);
//		}
//	}
//	
//	/**
//	 * this method receives notification from the Points VitalLogic that a Lock
//	 * has been cleared.  Locks arriving from the Points affect only MergedGlobalLocks
//	 * and are passed on to the ApproachLogic.
//	 * @param lock is the Lock that was cleared.
//	 */
//	void pointsLockCleared(final LogicLocks lock) {
//		if (ApproachLogic != null) {
//			ApproachLogic.clearAdvanceLock(lock);
//		}		
//	}
//	
	/**
	 * this should never be invoked.
	 */
	@Override
	public void processCommand(final String command) {
	}

	/**
	 * is called to tell the FrogVitalLogic that it is the lined route through
	 * the turnout.  In addition to changing the alignment, it forwards the
	 * advance locks for the opposite direction up stream.
	 * @param appCircuit is the approach TrackCircuit on the other side of the Points
	 * @return the TrackCircuit created for talking to the Vital logic
	 */
	public TrackCircuit lineRoute(final TrackCircuit appCircuit) {
		Fouling = false;
		Points.mergeFrogSpeed(MergedSpeed);
//		if (Points.ApproachLogic != null) {
//			Points.ApproachLogic.refreshLocks(MyLocks, AdvanceLocks);
//			Points.ApproachLogic.refreshLocks(AdvanceLocks);
//			for (LogicLocks l : MyLocks) {
//				Points.ApproachLogic.setLocalLock(l);
//			}
//			for (LogicLocks l : ApproachLocks) {
//				Points.ApproachLogic.setAdvanceLock(l);
//			}
			for (LogicLocks l : MergedGlobalLocks) {
				Points.setAdvanceLogicLock(l);
//			}
//			if (!MyLocks.contains(LogicLocks.CONFLICTINGSIGNALLOCK)) {
//				Points.ApproachLogic.clearLocalLock(LogicLocks.CONFLICTINGSIGNALLOCK);
//			}
		}
//		if (MergedGlobalLocks.contains(LogicLocks.ROUTELOCK)) {
//			Points.setFrogRoute();
//		}
		FedCircuit.setApproachTrackCircuit(appCircuit);
		return CircuitRelay;
	}

	/**
	 * is called to tell the FrogVitalLogic that it is no
	 * longer the lined route.
	 */
	void foulRoute() {
		//		setProxyTargetSignal(null);
		Fouling = true;
		//		if (Points.ApproachLogic != null) {
		////			Points.ApproachLogic.refreshLocks(PtsVitalLogic.FOULING_LOCK, LogicLocks.NoLogicLocks);
		////			Points.ApproachLogic.refreshLocks(LogicLocks.NoLogicLocks);
		////			for (LogicLocks l : MyLocks) {
		////				Points.ApproachLogic.clearLocalLock(l);
		////			}
		////			for (LogicLocks l : ApproachLocks) {
		////				Points.ApproachLogic.clearAdvanceLock(l);
		////			}
		//			for (LogicLocks l : MergedGlobalLocks) {
		//				Points.ApproachLogic.clearAdvanceLock(l);
		//			}
		for (LogicLocks l : MergedGlobalLocks) {
			Points.clearAdvanceLogicLock(l);
		}
		////			Points.ApproachLogic.setLocalLock(LogicLocks.CONFLICTINGSIGNALLOCK);
		//		}
		////		if (MergedGlobalLocks.contains(LogicLocks.ROUTELOCK)) {
		////			Points.clearFrogRoute();
		////		}
		FedCircuit.setApproachTrackCircuit(null);
	}
	
//	/**
//	 * is a predicate to query the interlocking status of the points
//	 * for movement from a dispatcher request.  Almost any of the AdvanceLocks
//	 * (except for Detection) will block a dispatcher request.  And, local
//	 * detection will also block the request.
//	 * @return true if the dispatcher can safely move the points and
//	 * false if they are locked into position.
//	 */
//	public boolean checkDispatcherLocks() {
//		return LogicLocks.intersection(MergedGlobalLocks, LogicLocks.SwitchLocked).isEmpty();
//	}
	
//	/**
//	 * is a predicate to query the interlocking status of the points
//	 * for movement from a local request.
//	 * @return true if the local crew can safely move the points and
//	 * false if they are locked into position.
//	 */
//	public boolean checkLocalLocks() {
//		return (LogicLocks.intersection(MergedGlobalLocks, LogicLocks.LocalSwitchLocked).isEmpty());
//	}
//

	/**********************************************************************************
	 * the SignalVitalLogic methods
//	 **********************************************************************************/
//	@Override
//	public void setRouteLock(LogicLocks l) {
//		if (!AdvanceRouteLocks.contains(l)) {
//			AdvanceRouteLocks.add(l);
//			if (!MergedRouteLocks.contains(l)) {
//				MergedRouteLocks.add(l);
//				if (Points != null) {
//					Points.proxySetRouteLock(l);	
//				}
//			}
//		}
//	}
//
//	@Override
//	public void clearRouteLock(LogicLocks l) {
//		if (AdvanceRouteLocks.contains(l)) {
//			AdvanceRouteLocks.remove(l);
//			if (MergedRouteLocks.contains(l)) {
//				MergedRouteLocks.remove(l);
//				if (Points != null) {
//					Points.proxyClearRouteLock(l);
//				}
//			}
//		}
//	}
//	
//	/**
//	 * receives a route lock set from the Points and forwards it to the opposite
//	 * end.
//	 * @param l is the lock
//	 */
//	protected void proxySetRouteLock(LogicLocks l) {
//		SignalRelay.setRouteLock(l);
//	}
//
//	/**
//	 * receives a route lock clear from the Points and forwards it to the opposite
//	 * end.
//	 * @param l is the lock
//	 */
//	protected void proxyClearRouteLock(LogicLocks l) {
//		SignalRelay.clearRouteLock(l);
//	}
//
//	/**********************************************************************************
//	 * This LockRequestProcessor is for handling ExitTrafficLocks.  Normally, ExitTrafficLocks
//	 * are sent to the GUI as an indication; however, a FrogVitalLogic has no access to
//	 * the codeline.  Consequently, when a change is received, the FrogVitalLogic 
//	 * tells the PtsVitalLogic, who sends an indication.
//	 **********************************************************************************/
////	protected class FrogExitRequestProcessor extends AdvanceRequestLockProcessor {
//	protected class FrogExitRequestProcessor extends DefaultRequestLockProcessor {
//
//		/**
//		 * the constructor
//		 */
//		public FrogExitRequestProcessor() {
//			super(LogicLocks.ROUTELOCK);
//		}
//
//		@Override
//		public void requestLockSet() {
//			if (!MyLocks.contains(Lock)) {
//				MyLocks.add(Lock);
//				if (mergeGlobalLocks(Lock)) {
//					if (!Fouling && (Points.ApproachLogic != null)) {
//						Points.ApproachLogic.setLocalLock(Lock);
//					}
//					Points.setFrogRoute();
//				}
//			}
//		}
//
//		@Override
//		public void requestLockClear() {
//			if (MyLocks.contains(Lock)) {
//				MyLocks.remove(Lock);
//				if (mergeGlobalLocks(Lock)) {
//					if (!Fouling && (Points.ApproachLogic != null)) {
//						Points.ApproachLogic.clearLocalLock(Lock);
//					}
//					Points.clearFrogRoute();
//				}
//			}
//		}
//		
//		@Override
//		public void advanceLockSet() {
//			if (!AdvanceLocks.contains(Lock)) {
//				AdvanceLocks.add(Lock);
//				if (mergeGlobalLocks(Lock)) {
//					setApproachLock(Lock);
//					Points.setFrogRoute();
//				}
//			}
//		}
//
//		@Override
//		public void advanceLockClear() {
//			if (AdvanceLocks.contains(Lock)) {
//				AdvanceLocks.remove(Lock);
//				if (mergeGlobalLocks(Lock)) {
//					clearApproachLock(Lock);
//					Points.clearFrogRoute();
//				}
//			}
//		}
//	}
//	/**************************************************************************
//	 *  end of FrogExitRequestProcessor
//	 **************************************************************************/
}
/* @(#)FrogVitalLogic.java */
