/* Name: SignalVitalLogic.java
 *
 * What:
 *   This class is an attribute on all VitalLogic containing signals (except BlkVitalLogic
 *   with virtual signals) and signal relays (points, frog, crossings).  Its purpose is
 *   to link together signals for propagating route level locks.  This is needed for CTC/DTC
 *   because the GUI does not know about Intermediates.  Perhaps this could be handled
 *   in the GUI, but the architecture is that the Vital Logic provides route level checking.
 */
package cats.layout.vitalLogic;

/**
 *   This class is an attribute on all VitalLogic containing signals (except BlkVitalLogic
 *   with virtual signals) and signal relays (points, frog, crossings).  Its purpose is
 *   to link together signals for propagating route level locks.  This is needed for CTC/DTC
 *   because the GUI does not know about Intermediates.  Perhaps this could be handled
 *   in the GUI, but the architecture is that the Vital Logic provides route level checking.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface SignalVitalLogic {
	/**
	 * receives a request from the SignalVitalLogic in advance to set a route lock
	 * @param l is the lock
	 */
	public void setRouteLock(final LogicLocks l);
	
	/**
	 * receives a request from the SignalVitalLogic in advance to clear a route lock
	 * @param l is the lock
	 */
	public void clearRouteLock(final LogicLocks l);
}
/* @(#)SignalVitalLogic.java */