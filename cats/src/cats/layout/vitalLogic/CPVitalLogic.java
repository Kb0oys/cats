/* Name: CPVitalLogic.java
 *
 * What:
 * This file contains the definition for the VitalLogic sub-class associated with an
 * Control Point (DTC/CTC) or Home signal (ABS/APB).  A Control Point has an icon on the
 * Dispatcher panel and the dispatcher sends commands to the VitalLogic by way of the
 * icon.  A Home signal has special properties in the ABS/APB world in that routes
 * terminate on a Home signal.
 */
package cats.layout.vitalLogic;

import cats.apps.Crandic;
import cats.common.Constants;
import cats.common.DebugBits;
import cats.layout.Discipline;
import cats.layout.OccupancySpectrum;
import cats.layout.codeLine.CodeMessage;
import cats.layout.codeLine.CodePurpose;
import cats.layout.codeLine.Codes;
import cats.layout.codeLine.packetFactory.PacketFactory;
import cats.layout.items.PhysicalSignal;


/**
 * This file contains the definition for the VitalLogic sub-class associated
 * with a Control Point (DTC/CTC) or Home signal (ABS/APB). A Control Point has
 * an icon on the Dispatcher panel and the dispatcher sends commands to the
 * VitalLogic by way of the icon. A Home signal has special properties in the
 * ABS/APB world in that routes terminate on a Home signal.
 * <p>
 * Title: CATS - Crandic Automated Traffic System</p>
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2014, 2015, 2016, 2018, 2019, 2022</p>
 * <p>
 * Company: </p>
 *
 * @author Rodney Black
 * @version $Revision$
 */
public class CPVitalLogic extends IntermediateVitalLogic {

	/**
	 * the last signal indication sent to the GUI
	 */
	private int SentIndication = -1;
	
    /**
     * is an aspect forced by the CTC machine
     */
    private FORCED_ASPECT AspectOverride = FORCED_ASPECT.NONE;

    /**
     * the Traffic Lock processor
     */
    private final CPTrafficLockProcessor TrafficLock = new CPTrafficLockProcessor();

//    /**
//     * the Fleet Lock processor
//     */
//    private final LockRequestProcessor FleetLock = new FleetLockProcessor();

    public CPVitalLogic(PacketFactory transmitter, String id, PhysicalSignal sig) {
        super(transmitter, id, sig);
    }

    @Override
    public void configureLockProcessing() {
        NeighborBlk = (BlkVitalLogic) NeighborLogic;
//        MySignalHead.setApproachTranslation(SIGNAL_TYPE.ISOLATED);
////        if ((NeighborBlk != null) && ((NeighborBlk instanceof CPVitalLogic) || (NeighborBlk instanceof IntermediateVitalLogic))) {
//            MySignalHead.setApproachTranslation(SIGNAL_TYPE.PAIRED);
////        }

//		LockProcessor.put(LogicLocks.DETECTIONLOCK,
//				new LocalCodeLockDecorator(new SignalMergedRequestLockProcessor(LogicLocks.DETECTIONLOCK)));
//		LockProcessor.put(LogicLocks.CONFLICTINGSIGNALLOCK, 
//				new SignalMergedRequestLockProcessor(LogicLocks.CONFLICTINGSIGNALLOCK));
//		LockProcessor.put(LogicLocks.SWITCHUNLOCK, new SignalMergedRequestLockProcessor(LogicLocks.SWITCHUNLOCK));
//		LockProcessor.put(LogicLocks.APPROACHLOCK, new DefaultRequestLockProcessor(LogicLocks.APPROACHLOCK));
//		LockProcessor.put(LogicLocks.TIMELOCK, new LocalCodeLockDecorator(new TimeLockProcessor()));
//		LockProcessor.put(LogicLocks.TRAFFICLOCK, TrafficLock);
//		LockProcessor.put(LogicLocks.ROUTELOCK, 
//				new SignalMergedRequestLockProcessor(LogicLocks.ROUTELOCK));
//		LockProcessor.put(LogicLocks.PROTECTIONLOCK, new SignalAdvanceRequestLockProcessor(LogicLocks.PROTECTIONLOCK));
//		LockProcessor.put(LogicLocks.MAINTENANCELOCK, 
//				new SignalAdvanceRequestLockProcessor(LogicLocks.MAINTENANCELOCK));
//		LockProcessor.put(LogicLocks.CALLONLOCK, new CallOnLockProcessor());
//		LockProcessor.put(LogicLocks.FLEETLOCK, FleetLock);
//		LockProcessor.put(LogicLocks.EXTERNALLOCK, 
//				new LocalCodeLockDecorator(new SignalMergedRequestLockProcessor(LogicLocks.EXTERNALLOCK)));
//	
//		MySignalHead.setApproachTranslation(SIGNAL_TYPE.ISOLATED);
//		
//		if ((NeighborBlk != null)) {
//			if (NeighborBlk instanceof CPVitalLogic) {
////				NeighborCP = true;
//				MySignalHead.setApproachTranslation(SIGNAL_TYPE.PAIRED);
//				LockProcessor.put(LogicLocks.DETECTIONLOCK, new NeighborApproachLockDecorator(LockProcessor.get(LogicLocks.DETECTIONLOCK)));
//			}
//			else {
//				if (NeighborBlk instanceof IntermediateVitalLogic) {
////					NeighborIntermediate = true;
//					LockProcessor.put(LogicLocks.ROUTELOCK, new HoldNeighborDecorator(LockProcessor.get(LogicLocks.ROUTELOCK)));
//					LockProcessor.put(LogicLocks.DETECTIONLOCK, new NeighborApproachLockDecorator(LockProcessor.get(LogicLocks.DETECTIONLOCK)));
//				}
//				if (!NeighborBlk.MyDiscipline.equals(Discipline.ABS) && !NeighborBlk.MyDiscipline.equals(Discipline.UNDEFINED)) {
//					LockProcessor.put(LogicLocks.ROUTELOCK, 
//							new RouteLockDecorator(new LockPropagationDecorator(LockProcessor.get(LogicLocks.ROUTELOCK))));
////					LockProcessor.put(LogicLocks.PROTECTIONLOCK, new MergedLockPropagationDecorator(LockProcessor.get(LogicLocks.PROTECTIONLOCK)));
//					LockProcessor.put(LogicLocks.FLEETLOCK, new AdvanceLockPropagationDecorator(LockProcessor.get(LogicLocks.FLEETLOCK)));
//				}
//			}
//		}
//		if (MyDiscipline.equals(Discipline.CTC) || MyDiscipline.equals(Discipline.DTC) || MyDiscipline.equals(Discipline.APB)) {
//
//			LockProcessor.put(LogicLocks.DETECTIONLOCK, new CPDetectionDecorator(LockProcessor.get(LogicLocks.DETECTIONLOCK)));
//			//			LockProcessor.put(LogicLocks.ROUTELOCK, new CTCRouteDecorator(LockProcessor.get(LogicLocks.ROUTELOCK)));
//			if (!MyDiscipline.equals(Discipline.APB)) {
//				LockProcessor.put(LogicLocks.HOLDLOCK, 
//						new SignalLocalRequestLockProcessor(LogicLocks.HOLDLOCK));
//				setLocalLock(LogicLocks.HOLDLOCK);
//			}
//		}
//		LockProcessor.put(LogicLocks.DETECTIONLOCK,
//				new LocalCodeLockDecorator(new SignalMergedRequestLockProcessor(LogicLocks.DETECTIONLOCK)));
//		LockProcessor.put(LogicLocks.CONFLICTINGSIGNALLOCK, 
//				new SignalMergedRequestLockProcessor(LogicLocks.CONFLICTINGSIGNALLOCK));
//		LockProcessor.put(LogicLocks.SWITCHUNLOCK, new SignalMergedRequestLockProcessor(LogicLocks.SWITCHUNLOCK));
//		LockProcessor.put(LogicLocks.APPROACHLOCK, new DefaultRequestLockProcessor(LogicLocks.APPROACHLOCK));
//		LockProcessor.put(LogicLocks.TIMELOCK, new LocalCodeLockDecorator(new TimeLockProcessor()));
//		LockProcessor.put(LogicLocks.TRAFFICLOCK, TrafficLock);
//		LockProcessor.put(LogicLocks.ROUTELOCK, 
//				new SignalMergedRequestLockProcessor(LogicLocks.ROUTELOCK));
//		LockProcessor.put(LogicLocks.PROTECTIONLOCK, new SignalAdvanceRequestLockProcessor(LogicLocks.PROTECTIONLOCK));
//		LockProcessor.put(LogicLocks.MAINTENANCELOCK, 
//				new SignalAdvanceRequestLockProcessor(LogicLocks.MAINTENANCELOCK));
//		LockProcessor.put(LogicLocks.CALLONLOCK, new CallOnLockProcessor());
//		LockProcessor.put(LogicLocks.FLEETLOCK, FleetLock);
//		LockProcessor.put(LogicLocks.EXTERNALLOCK, 
//				new LocalCodeLockDecorator(new SignalMergedRequestLockProcessor(LogicLocks.EXTERNALLOCK)));
        LockRequestProcessor processor;
        // DETECTIONLOCK
        processor = new MergedLockFilterDecorator(
        		new SignalDecorator(
        				new MergedRequestLockProcessor(LogicLocks.DETECTIONLOCK)));
//        if ((NeighborBlk != null) && ((NeighborBlk instanceof IntermediateVitalLogic) || (NeighborBlk instanceof CPVitalLogic))) {
        if ((NeighborBlk != null) && (NeighborBlk instanceof IntermediateVitalLogic)) {
            processor = new NeighborApproachLockDecorator(processor);
        }
//        if (MyDiscipline.equals(Discipline.CTC)  || (MyDiscipline.equals(Discipline.DTC))) {
//            processor = new CPVacancyDecorator(processor);
//        }
//        if (MyDiscipline.equals(Discipline.CTC)  || (MyDiscipline.equals(Discipline.DTC))) {
//        	processor = new CPDetectionDecorator(
//                    new CommonDetectionDecorator(processor));
//        }
//        LockProcessor.put(LogicLocks.DETECTIONLOCK,
//                new LockFilterDecorator(
//                        new LocalCodeLockDecorator(processor)));
        // to distinguish between a dispatcher canceling a route and a train rolling up a route, the detection
        // code must be sent to the GUI before the TRAFFIC clear
        processor = new LocalCodeLockDecorator(processor);
        if (MyDiscipline.equals(Discipline.CTC)  || (MyDiscipline.equals(Discipline.DTC))) {
        	processor = new CPDetectionDecorator(
                    new CommonDetectionDecorator(processor));
        }
        LockProcessor.put(LogicLocks.DETECTIONLOCK,
                new LockFilterDecorator(
                        processor));

        // CONFLICTINGSIGNALLOCK
        installSignalProcessor(LogicLocks.CONFLICTINGSIGNALLOCK);

        // HOLDLOCK
        if (MyDiscipline.equals(Discipline.CTC) || MyDiscipline.equals(Discipline.DTC)) {
        	LockProcessor.put(LogicLocks.HOLDLOCK,
        			new IgnoreAdvanceLocksDecorator(
        					new MergedLockFilterDecorator(
        							new SignalDecorator(
        									new MergedRequestLockProcessor(LogicLocks.HOLDLOCK)))));
        	setLocalLock(LogicLocks.HOLDLOCK);
        }

        // SWITCHUNLOCK
        if (NeighborBlk != null) {
            installSignalProcessor(LogicLocks.SWITCHUNLOCK);
        }

        // APPROACHLOCK
        LockProcessor.put(LogicLocks.APPROACHLOCK,
                new IgnoreAdvanceLocksDecorator(
                        new MergedLockFilterDecorator(
                                new SignalDecorator(
                                        new MergedRequestLockProcessor(LogicLocks.APPROACHLOCK)))));

        // TIMELOCK
        LockProcessor.put(LogicLocks.TIMELOCK, 
        		new IgnoreAdvanceLocksDecorator(
        				new TimeLockDecorator(
        						new CPTimeLockDecorator(
        								new DefaultRequestLockProcessor(LogicLocks.TIMELOCK)))));

        // TRAFFICLOCK - instantiated in the local field TrafficLock
        LockProcessor.put(LogicLocks.TRAFFICLOCK, TrafficLock);
        // ROUTELOCK
//		processor = new SignalDecorator(
//				new MergedRequestLockProcessor(LogicLocks.ROUTELOCK));
//		if ((NeighborBlk != null) && !(NeighborBlk instanceof CPVitalLogic)) {
//			if ((NeighborBlk instanceof IntermediateVitalLogic) &&
//					!(NeighborBlk.MyDiscipline.equals(Discipline.ABS) && !NeighborBlk.MyDiscipline.equals(Discipline.UNDEFINED))){
//				processor = new NeighborHoldDecorator(processor);
//			}
//		}
//		processor = new MergedLockFilterDecorator(processor);
//		LockProcessor.put(LogicLocks.ROUTELOCK, processor);
        if ((NeighborBlk != null) && !MyDiscipline.equals(Discipline.ABS) && !MyDiscipline.equals(Discipline.UNDEFINED)) {
            processor = new MergedRequestLockProcessor(LogicLocks.ROUTELOCK);
            if (!NeighborBlk.MyDiscipline.equals(Discipline.ABS) && !NeighborBlk.MyDiscipline.equals(Discipline.UNDEFINED)) {
                if (!(NeighborBlk instanceof CPVitalLogic)) {
                    processor = new LockPropagationDecorator(processor);
                    if (NeighborBlk instanceof IntermediateVitalLogic) {
                        processor = new NeighborHoldDecorator(processor);
                    }
                }
            }
            LockProcessor.put(LogicLocks.ROUTELOCK,
                    new MergedLockFilterDecorator(processor));
        } else {
            LockProcessor.put(LogicLocks.ROUTELOCK, new DefaultRequestLockProcessor(LogicLocks.ROUTELOCK));
        }

        // PROTECTIONLOCK
        processor = new SignalDecorator(
                new MergedRequestLockProcessor(LogicLocks.PROTECTIONLOCK));
//        if ((NeighborBlk != null) && !(NeighborBlk instanceof CPVitalLogic)
//                && !MyDiscipline.equals(Discipline.ABS) && !MyDiscipline.equals(Discipline.UNDEFINED)) {
//            processor = new LockPropagationDecorator(processor);
//        }
        LockProcessor.put(LogicLocks.PROTECTIONLOCK,
                new MergedLockFilterDecorator(processor));

        // MAINTENANCE
        LockProcessor.put(LogicLocks.MAINTENANCELOCK,
                new MergedLockFilterDecorator(
                        new SignalDecorator(
                                new MergedRequestLockProcessor(LogicLocks.MAINTENANCELOCK))));

        // CALLONLOCK
        LockProcessor.put(LogicLocks.CALLONLOCK,
        		new LockFilterDecorator(
        				new SignalDecorator(
        						new CallOnLockProcessor())));

        // FLEETLOCK
//		if ((NeighborBlk != null) && !(NeighborBlk instanceof CPVitalLogic)
//				&& !MyDiscipline.equals(Discipline.ABS) && !MyDiscipline.equals(Discipline.UNDEFINED)) {
//					FleetLock = new MyLocksFilterDecorator(
//							new LockPropagationDecorator(FleetLock));
//		}
//        LockProcessor.put(LogicLocks.FLEETLOCK, FleetLock);

        // EXTERNALLOCK
        LockProcessor.put(LogicLocks.EXTERNALLOCK,
                new LockFilterDecorator(
                        new LocalCodeLockDecorator(
                                new MergedLockFilterDecorator(
                                        new SignalDecorator(
                                                new MergedRequestLockProcessor(LogicLocks.EXTERNALLOCK))))));

    }

    @Override
    public void processCommand(final String command) {
        CodeMessage cmd = new CodeMessage(command);
        String state = cmd.Value;
        if (cmd.Purpose == CodePurpose.REQUEST) {
            switch (cmd.Lock) {
                case SIGNALINDICATIONLOCK:
                    if (Boolean.valueOf(state)) {
                        TrafficLock.requestLockSet();
                    } else {
                        // the acknowledge will be returned when the TrafficLock
                        // clears
//					((CPTrafficLockProcessor) EntryTrafficLock).requestLockClear();
                        TrafficLock.requestLockClear();
                    }
                    break;

                case ASPECT:
                    try {
                        AspectOverride = FORCED_ASPECT.valueOf(state);
                        MySignalHead.forceAspect(AspectOverride);
                    } catch (IllegalArgumentException e) {
                        Transmitter.createPacket(CodePurpose.DENY, Codes.ASPECT, state);
                    }
                    break;

//                case FLEET:
//                    if (Boolean.valueOf(state)) {
//                        FleetLock.requestLockSet();
//                    } else {
//                        FleetLock.requestLockClear();
//                    }
//                    break;
//				
//			case LOCALLOCK:
//				if (new Boolean(state).booleanValue()) {
//					LocalControlLock.requestLockSet();
//				}
//				else {
//					LocalControlLock.requestLockClear();
//				}
//				break;

                default:
//				Transmitter.createPacket(response, cmd.Lock, state);
                    super.processCommand(command);
            }
        }
    }

//	@Override
//	public void setRouteLock(final LogicLocks l) {
//		// this really should call a method in the Processor
//		AdvanceRouteLocks.add(l);
//		mergeRouteLocks(l);
//	}
//	
//	@Override
//	public void clearRouteLock(final LogicLocks l) {
//		// this really should call a method in the Processor
//		AdvanceRouteLocks.remove(l);
//		if (mergeRouteLocks(l) && l.equals(LogicLocks.DETECTIONLOCK) && !MyLocks.contains(LogicLocks.FLEETLOCK)) {
//			clearLocalLock(LogicLocks.TRAFFICLOCK);
//		}
//	}
    @Override
    protected FORCED_ASPECT determineAspect() {
        if (AspectOverride == FORCED_ASPECT.NONE) {
            if (MergedGlobalLocks.contains(LogicLocks.MAINTENANCELOCK)) {
                return FORCED_ASPECT.STOP_AND_GO;
            }
            if (MyLocks.contains(LogicLocks.CALLONLOCK)) {
                return FORCED_ASPECT.RESTRICTING;
            }
            if (!LogicLocks.intersection(MergedGlobalLocks, StopLocks).isEmpty()) {
                return FORCED_ASPECT.STOP;
            }
        }
        return AspectOverride;
    }

    @Override
    public void updateIndication(final int newIndication) {
        super.updateIndication(newIndication);
        if ((Transmitter != null) && (newIndication != SentIndication)) {
        	SentIndication = newIndication;
            Transmitter.createPacket(CodePurpose.INDICATION, Codes.ASPECT, String.valueOf(newIndication));
        }
    }

    /**
     * constructs a common lock request processor chain that propagates a change
     * in only AdvanceLocks to the edge in approach and stores it in the
     * LockProcessor table
     *
     * @param lock is the lock being processed
     */
    public void installSignalProcessor(LogicLocks lock) {
        LockProcessor.put(lock,
                new MyLocksFilterDecorator(
                        new MergedLockFilterDecorator(
                                new SignalDecorator(
                                        new MergedRequestLockProcessor(lock)))));
    }

//	/**********************************************************************************
//	 * The TimeLockProcessor
//	 **********************************************************************************/
//	/**
//	 * The base class handling requests to set or clear the TimeLock.  The time lock
//	 * is used in conjunction with "running time".  Running time happens when a
//	 * cleared signal is knocked down in front of a train.  The train's approach
//	 * signal drops, but the rest of the route components are frozen for a certain
//	 * amount of time.  This allows the train to overrun the signal without doing
//	 * any damage.  After time expires, the dispatcher can change the route
//	 * components.
//	 * <p>
//	 * instantiations of this class should be decorated with DefaultCodeRequestLockProcessor
//	 * for results to be sent to the GUI.
//	 * <p>
//	 * This lock is counter-intuitive.  On first examination it appears to work backwards
//	 * from others.
//	 * - it is set when the dispatcher "knocks down" (cancels) a route and there is a
//	 * train approaching the control point
//	 * - it is cleared when the timer expires
//	 * - it is canceled when it is running and the dispatcher resumes the route (the route
//	 * is not cancelled until the lock is cleared) or the approaching train disappears
//	 * 
//	 * The advance methods are not used because the lock is local to the VitalLogic.
//	 */
//	private class TimeLockProcessor extends DefaultRequestLockProcessor {
//		
//		/**
//		 * is where to find the current value for how much time to delay
//		 * when running time
//		 */
//		private final Sequence RunningTimeDuration;
//		
//		/**
//		 * is a Timer for running time
//		 */
//		private Timer RunningTime; 
//		
//		/**
//		 * is the code to send for confirming or denying that a route exists
//		 */
//		private final Codes RouteCode = Codes.valueOf(LogicLocks.ROUTELOCK.toString());
//		
//		/**
//		 * the constructor
//		 */
//		public TimeLockProcessor() {
//			super(LogicLocks.TIMELOCK);
//			RunningTimeDuration = CounterFactory.CountKeeper.findSequence(CounterFactory.TIMELOCKTAG);
//			RunningTime = new Timer( 0, new ActionListener() {
//				public void actionPerformed(ActionEvent ae) {
////					LockProcessor.get(LogicLocks.TIMELOCK).cancelSetRequest();
//					LockProcessor.get(LogicLocks.TIMELOCK).requestLockClear();
////					LockProcessor.get(LogicLocks.SIGNALINDICATIONLOCK).cancelSetRequest();
//				}
//			});
//			RunningTime.setRepeats(false);
//		}
//
////		/**
////		 * request that the lock be set.  This method starts the timer using the current
////		 * timer value, sets the lock, and tells the GUI
////		 * @return true if the lock can be set and false if not
////		 */
////		public void requestLockSet() {
////			if (!MyLocks.contains(Lock)) {
////				MyLocks.add(Lock);
////				RunningTime.setDelay(RunningTimeDuration.getAdjustment() * 1000);
////				RunningTime.setInitialDelay(RunningTimeDuration.getAdjustment() * 1000);
////				RunningTime.start();			
////			}
////		}
////
////		/**
////		 * request that the lock be cleared.  It stops the timer, clears the lock, and
////		 * tells the GUI.
////		 * @return true if the lock can be cleared and false if not
////		 */
////		public void requestLockClear() {
////			if (MyLocks.contains(Lock)) {
////				MyLocks.remove(Lock);
////				RunningTime.stop();
////			}
////		}
////
////		/**
////		 * request that the local lock only be cleared - that it not be
////		 * merged or forwarded.  This method does not do anything except
////		 * stop the timer, clear the lock, and inform the GUI.  It is essentially
////		 * the same as requestLockClear().
////		 * @return true if the lock can be clear and false if not
////		 */
////		public void cancelSetRequest() {
////			if (isLockSet()) {
////				RunningTime.stop();
////				MyLocks.remove(Lock);
////				Transmitter.createPacket(CodePurpose.INDICATION, Codes.TIMELOCK, Constants.FALSE);
////			}
////		}
//		/**
//		 * the request is invoked when a signal is knocked down.  The conditions
//		 * for satisfying the request are:
//		 * 1. the lock is not set (time is not running)
//		 * 2. a route exists
//		 * 3. a train is approaching
//		 * 4. a non-zero running time has been defined
//		 * 
//		 * If all of these are true, then the timer is started.  When it expires, the
//		 * route will be cleared and confirmation sent to the GUI.  The lock does not
//		 * check the conditions.  They should be checked before setting the lock.
//		 * 
//		 * If the lock is set nothing is returned because it is a redundant request
//		 * 
//		 * if the lock is not set and a route exists, but no train is approaching or
//		 * the timer is 0, then the lock is immediately cleared.
//		 */
//		public void requestLockSet() {
//			StateChanged = false;
//			if (!MyLocks.contains(Lock)) {
//				int timeout = RunningTimeDuration.getAdjustment() * 1000;
//				if (MyLocks.contains(LogicLocks.ROUTELOCK)) {
//					if (!MyLocks.contains(LogicLocks.APPROACHLOCK) && (timeout != 0)) {
//						MyLocks.add(Lock);
//						StateChanged = true;
//						
//						// starts the timer.  the route will be cancelled when it expires
//						RunningTime.setDelay(timeout);
//						RunningTime.setInitialDelay(timeout);
//						RunningTime.start();
//						MySignalHead.forceAspect(determineAspect());
//
//					}
//					else {
//						LockProcessor.get(LogicLocks.ROUTELOCK).cancelSetRequest();
//						Transmitter.createPacket(CodePurpose.INDICATION, RouteCode, Constants.FALSE);
//					}
//					MySignalHead.forceAspect(determineAspect());
//				}
//				else {  // no route to cancel, so just repeat state of route
//					Transmitter.createPacket(CodePurpose.INDICATION, RouteCode, Constants.FALSE);
//				}
//			}
//			// else redundant request, so do nothing
//		}
//		
//		/**
//		 * invoked when the timer fires.  It stops the timer, clears the lock, and
//		 * tells the GUI.
//		 * @return true if the lock can be cleared and false if not
//		 */
//		public void requestLockClear() {
//			RunningTime.stop();
//			if (MyLocks.contains(Lock)) {
//				MyLocks.remove(Lock);
//				LockProcessor.get(LogicLocks.ROUTELOCK).cancelSetRequest();
//				Transmitter.createPacket(CodePurpose.INDICATION, RouteCode, Constants.FALSE);
//				MySignalHead.forceAspect(determineAspect());
//			}
//		}
//		
//		/**
//		 * is invoked whenever the dispatcher attempts to set a route.  The backwards piece
//		 * of this code is that the timer maybe running.  That case signifies that a route was
//		 * active, the dispatcher attempted to cancel it, which caused time to run.  Then, the
//		 * dispatcher decided to resume the route, cancelling the cancellation.
//		 */
//		public void cancelSetRequest() {
//			if (isLockSet()) {
//				RunningTime.stop();
//				MyLocks.remove(Lock);
//				Transmitter.createPacket(CodePurpose.INDICATION, RouteCode, Constants.TRUE);
//			}
//			else {
//				LockRequestProcessor entryLogic = LockProcessor.get(LogicLocks.ROUTELOCK);
//				entryLogic.requestLockSet();
//				Transmitter.createPacket(CodePurpose.INDICATION, RouteCode, 
//						entryLogic.isLockSet() ? Constants.TRUE : Constants.FALSE);
//			}
//			MySignalHead.forceAspect(determineAspect());
//		}
//	}
//	/**************************************************************************
//	 *  end of TimeLock processor
//	 **************************************************************************/
    /***************************************************************************
     * The CPDetectionDecorator
     *
     * This decorator to a Block Detection Processor is for the Control Point
     * (CTC) end of a route. It monitors occupancy over the block protected by
     * the Control Point.
     * <p>
     * It is intended to decorate a processor that handles block occupancy,
     * including sending the detection codes to the GUI.
     **************************************************************************/
    private class CPDetectionDecorator extends DefaultProcessorDecorator {

        /**
         * the constructor
         *
         * @param processor is the LockRequestProcess being decorated
         */
        public CPDetectionDecorator(LockRequestProcessor processor) {
            super(processor);
        }

		@Override
		public void requestLockSet() {
			Processor.requestLockSet();
            clearLocalLock(LogicLocks.CALLONLOCK);
			setLocalLock(LogicLocks.HOLDLOCK);
			if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
				log.info(Constants.OCCUPANCY_TRIGGER + "Setting detector for CPVitalLogic " + CPVitalLogic.this.Identity);
			}

		}
		
        @Override
        public void requestLockClear() {
            Processor.requestLockClear();
//			if (!MyLocks.contains(LogicLocks.FLEETLOCK)  && MyLocks.contains(LogicLocks.TRAFFICLOCK)) {
//			if (!MergedGlobalLocks.contains(LogicLocks.DETECTIONLOCK) && MyLocks.contains(LogicLocks.TRAFFICLOCK)) {
			if (MyLocks.contains(LogicLocks.TRAFFICLOCK)) {
                if (OccupancySpectrum.instance().isRecording()) {
                	log.info("Local detection removed for " + CPVitalLogic.this.Identity);
                }
				clearLocalLock(LogicLocks.TRAFFICLOCK);
				if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
					log.info(Constants.OCCUPANCY_TRIGGER + "Clearing detector for CPVitalLogic " + CPVitalLogic.this.Identity);
				}
			}
        }
        
		@Override
		public void advanceLockClear() {
            Processor.advanceLockClear();
//			if (!MergedGlobalLocks.contains(LogicLocks.DETECTIONLOCK) && !MyLocks.contains(LogicLocks.FLEETLOCK)  && MyLocks.contains(LogicLocks.TRAFFICLOCK)) {
			if (!MergedGlobalLocks.contains(LogicLocks.DETECTIONLOCK) && MyLocks.contains(LogicLocks.TRAFFICLOCK)) {
                if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
                	log.info("Advance detection removed for " + CPVitalLogic.this.Identity);
                }
				clearLocalLock(LogicLocks.TRAFFICLOCK);
				
			}
		}
    }

    /**************************************************************************
     * end of CPDetectionDecorator
     **************************************************************************/
//    /**************************************************************************
//     * The CPVacancyDecorator
//     *
//     * This decorator to a Block Detection Processor is for the Control Point
//     * (CTC) end of a route. It monitors occupancy over the Safety Zone
//     * protected by the Control Point. If a route originates on the Control
//     * Point, it kills the route when the last detector reports unoccupied.
//     * <p>
//     * It is intended to decorate a processor that handles block occupancy,
//     * including sending the detection codes to the GUI. Because the last
//     * detector could be for the immediate block or one deeper in the Safety
//     * Zone, it checks the merged locks. 
//      *
//      * This doesn't worknbecause it covers only the Safety Zone and a Route
//      * exists over the Route Zone.
//     **************************************************************************/
//    private class CPVacancyDecorator extends DefaultProcessorDecorator {
//
//        public CPVacancyDecorator(LockRequestProcessor processor) {
//            super(processor);
//        }
//
//        @Override
//        public void requestLockClear() {
//            Processor.requestLockClear();
//            cancelTrafficLock();
//        }
//
//        @Override
//        public void advanceLockClear() {
//            Processor.advanceLockClear();
//            cancelTrafficLock();
//        }
//        
//        private void cancelTrafficLock() {
//            if (MyLocks.contains(LogicLocks.TRAFFICLOCK) && !MyLocks.contains(LogicLocks.FLEETLOCK)) {
//                clearLocalLock(LogicLocks.TRAFFICLOCK);
//            }
//        }
//    }

    /**
     * ************************************************************************
     * end of CPVacancyDecorator
     **************************************************************************/

    /***************************************************************************
     * The TrafficLockProcessor
     * 
     * This class handles requests to initiate or clear a route through
     * protected track. It checks for
     * <ul>
     * <li>TrafficLock being set</li>
     * <li>conditions are safe enough to allow a route to be created (i.e.
     * interlocking)</li>
     * </ul>
     * This class handles confirm/deny indications back to the GUI. Note that
     * there are a lot of interactions with other locks (detection, time lock,
     * maintenance), making this code fairly complex.
     *
     * This lock should not be decorated.
     **************************************************************************/
    private class CPTrafficLockProcessor extends DefaultRequestLockProcessor {

        public CPTrafficLockProcessor() {
            super(LogicLocks.TRAFFICLOCK);
        }

        @Override
        public void requestLockSet() {
//            EnumSet<LogicLocks> l = LogicLocks.intersection(MergedGlobalLocks, LogicLocks.CPReservationBlockers);
//            if (!isLockSet()) {
//                if (l.isEmpty()) {
                    MyLocks.add(Lock);
                    clearLocalLock(LogicLocks.HOLDLOCK);
                    // The Route did not exist and it is OK to create it
                    if (PeerLogic != null) {
                        PeerLogic.setAdvanceLock(LogicLocks.ROUTELOCK);
//						if (MyLocks.contains(LogicLocks.FLEETLOCK)) {
//							PeerLogic.setAdvanceLock(LogicLocks.FLEETLOCK);
//						}
                    }
                    Transmitter.createPacket(CodePurpose.INDICATION, Codes.SIGNALINDICATIONLOCK, Constants.TRUE);
                    MySignalHead.forceAspect(determineAspect());
                    if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
                    	log.info(Constants.ROUTE_CREATION +  CPVitalLogic.this.Identity + " after set route Locks" + MyLocks + " Advance=" + AdvanceLocks + " Merged=" + 
                    			MergedGlobalLocks + " expected indication=" + SentIndication);
                    }
//                } 
//                else {
////                    Transmitter.createPacket(CodePurpose.DENY, Codes.SIGNALINDICATIONLOCK, Constants.FALSE);
//                    Transmitter.createPacket(CodePurpose.INDICATION, Codes.SIGNALINDICATIONLOCK, Constants.FALSE);
//                }
//            } else {
//                Transmitter.createPacket(CodePurpose.INDICATION, Codes.SIGNALINDICATIONLOCK, Constants.TRUE);
//            }
        }

        @Override
        public void requestLockClear() {
            if (isLockSet()) {
//                if (MyLocks.contains(LogicLocks.DETECTIONLOCK)) {
//                    Transmitter.createPacket(CodePurpose.DENY, Codes.SIGNALINDICATIONLOCK, Constants.FALSE);
//                } else {
                    if (PeerLogic != null) {
                        PeerLogic.clearAdvanceLock(LogicLocks.ROUTELOCK);
//                        if (MyLocks.contains(LogicLocks.FLEETLOCK)) {  // dispatcher cleared the route, so clear fleet
////							PeerLogic.clearAdvanceLock(LogicLocks.FLEETLOCK);
//                            FleetLock.requestLockClear();
//                        }
                    }
                    setLocalLock(LogicLocks.HOLDLOCK);
                    // this order is critical.  Changing the signal indication could start time lock.
                    MySignalHead.forceAspect(determineAspect());
                    if (!MyLocks.contains(LogicLocks.TIMELOCK)) {
                        MyLocks.remove(Lock);
                    	Transmitter.createPacket(CodePurpose.INDICATION, Codes.SIGNALINDICATIONLOCK, Constants.FALSE);
                    }
                    if (Crandic.Details.get(DebugBits.DEBUGFLAG)) {
                    	log.info(Constants.ROUTE_CREATION +  CPVitalLogic.this.Identity + " after cleared route Locks " + MyLocks + " Advance=" + AdvanceLocks + " Merged=" + 
                    			MergedGlobalLocks + " expected indication=" + SentIndication);
                    }
//                }
            }
        }
    }

    /**************************************************************************
     * end of CPTrafficLock request processor
     **************************************************************************/

//    /**************************************************************************
//     * The FleetLockProcessor
//     * 
//     * This class handles requests to initiate or clear fleeting through the
//     * protected track.
//     *
//     * This lock should not be decorated.
//     **************************************************************************/
//    private class FleetLockProcessor extends DefaultRequestLockProcessor {
//
//        public FleetLockProcessor() {
//            super(LogicLocks.FLEETLOCK);
//        }
//
//        @Override
//        public void requestLockSet() {
//            if (!isLockSet()) {
//                MyLocks.add(Lock);
//                if (MyLocks.contains(LogicLocks.TRAFFICLOCK)) {
//                    //					if (PeerLogic != null) {
//                    //						PeerLogic.setAdvanceLock(LogicLocks.FLEETLOCK);
//                    //					}
//                    if (PeerLogic != null) {  // ensure that the route is completely established
//                        PeerLogic.setAdvanceLock(LogicLocks.ROUTELOCK);
//                    }
//                    Transmitter.createPacket(CodePurpose.INDICATION, Codes.FLEET, Constants.TRUE);
//                } else {
//                    Transmitter.createPacket(CodePurpose.DENY, Codes.FLEET, Constants.FALSE);
//                }
//            } else {
//                Transmitter.createPacket(CodePurpose.INDICATION, Codes.FLEET, Constants.TRUE);
//            }
//        }
//
//        @Override
//        public void requestLockClear() {
//            if (isLockSet()) {
////				if (PeerLogic != null) {
////					PeerLogic.clearAdvanceLock(LogicLocks.FLEETLOCK);
////				}
//                Transmitter.createPacket(CodePurpose.INDICATION, Codes.FLEET, Constants.FALSE);
//                MyLocks.remove(Lock);
//            }
//        }
//
////		@Override
////		public void advanceLockSet() {
////			AdvanceLocks.add(LogicLocks.FLEETLOCK);
////		}
////
////		@Override
////		public void advanceLockClear() {
////			AdvanceLocks.remove(LogicLocks.FLEETLOCK);
////		}
//    }
//
//    /**************************************************************************
//     * end of FleetLockProcessor
//     **************************************************************************/

//	/**********************************************************************************
//	 * The CTCRouteDecorator
//	 *
//	 * This decorator to a Block Detection Processor is for the Control Point (CTC) end
//	 * of a route.  It merges the local and global ROUTELOCKS into the MergedRouteLock
//	 * <p>
//	 * It is intended to decorate a CTC Control Point.
//	 * 
//	 * - ROUTE (CTC, DTC)(Control Point)
//	 **********************************************************************************/
//	private class CTCRouteDecorator extends AbstractProcessorDecorator {
//		
//		/**
//		 * the constructor
//		 * @param processor is the LockRequestProcess being decorated
//		 */
//		public CTCRouteDecorator(LockRequestProcessor processor) {
//			super(processor);
//		}
//
//		@Override
//		public void requestLockSet() {
//			Processor.requestLockSet();
//			mergeRouteLocks(Processor.getLock());
//		}
//
//		@Override
//		public void requestLockClear() {
//			Processor.requestLockClear();
//			mergeRouteLocks(Processor.getLock());
//		}
//
//		@Override
//		public void advanceLockSet() {
//			Processor.advanceLockSet();
//			mergeRouteLocks(Processor.getLock());
//		}
//
//		@Override
//		public void advanceLockClear() {
//			Processor.advanceLockClear();
//			mergeRouteLocks(Processor.getLock());
//		}
//	}
//	
//	/**************************************************************************
//	 *  end of CTCDetectionDecorator
//	 **************************************************************************/
    /**************************************************************************
     * The CallOnLockProcessor
     * 
     * This class handles requests to initiate or cancel a Call-on request
     * through a control point. Call-On shares some similarities with
     * CPTrafficLockProcessor: 1. it is set only on a Control point 2. it sets
     * the DirectionOfTravel lock on the peer, but adds a filter, so that the
     * lock does not propagate 3. it is not valid if DirectionOfTravel is set,
     * as that implies an opposing train has not seen a Stop 4. unoccupancy
     * clears it
     *
     * Unlike CPTrafficLockProcessor, it 1. does not run time when being cleared
     * from the GUI. 2. Never is set in AdvanceLock
     *
     * The way that Call-on works is that the DirectionOfTravel is set on the
     * Peer's ApproachFilter (to block propagation) and OpposingSignal is
     * cleared. This sets the arrow on only the exit, yet drops opposing
     * signals.
     *
     * This lock should be decorated with a DefaultCodeRequestLockProcessor
     *
     * - CALLON (CTC, DTC)(Control Point)
     **************************************************************************/
    private class CallOnLockProcessor extends DefaultRequestLockProcessor {

//		/**
//		 * for remembering the values of the Peer's DirectionOfTravel and OpposingSignal
//		 * filters prior to Call-on, so that they can be restored.
//		 */
//		private boolean oldDirectionFilter;
//		private boolean oldOpposingFilter;
        public CallOnLockProcessor() {
            super(LogicLocks.CALLONLOCK);
        }

        @Override
        public void requestLockSet() {
//            if (LogicLocks.intersection(MergedGlobalLocks, LogicLocks.CallOnBlockers).isEmpty()
//                    && MergedGlobalLocks.contains(LogicLocks.DETECTIONLOCK) && !MyLocks.contains(LogicLocks.DETECTIONLOCK)) {
//                super.requestLockSet();
//                if (PeerLogic != null) {
////					PeerLogic.blockRoutes(Lock);
//					oldDirectionFilter = PeerLogic.ApproachFilter.contains(LogicLocks.DIRECTIONOFTRAVEL);
////					PeerLogic.removeApproachFilter(LogicLocks.DIRECTIONOFTRAVEL);
//					PeerLogic.addApproachFilter(LogicLocks.DIRECTIONOFTRAVEL);
//					PeerLogic.setLocalLock(LogicLocks.DIRECTIONOFTRAVEL); // sets the arrow
//					oldOpposingFilter = PeerLogic.ApproachFilter.contains(LogicLocks.OPPOSINGSIGNALLOCK);
//					PeerLogic.addApproachFilter(LogicLocks.OPPOSINGSIGNALLOCK);
//					PeerLogic.setLocalLock(LogicLocks.OPPOSINGSIGNALLOCK);
//					PeerLogic.setLocalLock(LogicLocks.OPPOSINGSIGNALLOCK);
//                }
//            }
        	if (PeerLogic != null) {
        		PeerLogic.setAdvanceLock(Lock);
        	}
            Transmitter.createPacket(CodePurpose.INDICATION, Codes.CALLONLOCK, Constants.TRUE);
            MyLocks.add(Lock);
        }

        @Override
        public void requestLockClear() {
//            super.requestLockClear();
//			if (PeerLogic != null) {
//				PeerLogic.clearLocalLock(LogicLocks.OPPOSINGSIGNALLOCK);
//			}
//			PeerLogic.clearLocalLock(LogicLocks.DIRECTIONOFTRAVEL);
//			// restore the ApproachFilter
//			if (oldDirectionFilter) {
//				PeerLogic.addApproachFilter(LogicLocks.DIRECTIONOFTRAVEL);
//			}
//			PeerLogic.clearLocalLock(LogicLocks.OPPOSINGSIGNALLOCK);
//			if (!oldOpposingFilter) {
//				PeerLogic.removeApproachFilter(LogicLocks.OPPOSINGSIGNALLOCK);
//			}
        	if (PeerLogic != null) {
        		PeerLogic.clearAdvanceLock(Lock);
        	}
            Transmitter.createPacket(CodePurpose.INDICATION, Codes.CALLONLOCK, Constants.FALSE);
            MyLocks.remove(Lock);
        }
    }

    /***************************************************************************
     * end of CallOn request processor
	 **************************************************************************/
    /***************************************************************************
     * The CPTimeLockDecorator
     *
     * This decorator extends the TimeLockDecorator by clearing TrafficLock
     * (only valid on a CPVitalLogic) when time expires or the timer is cancelled
     * and by sending a code to the GUI.
     **************************************************************************/
    private class CPTimeLockDecorator extends DefaultProcessorDecorator {
    	/**
    	 * the constructor
    	 */
    	public CPTimeLockDecorator(LockRequestProcessor processor) {
    		super(processor);
    	}
    	
    	@Override
    	public void requestLockSet() {
    		Processor.requestLockSet();
    		Transmitter.createPacket(CodePurpose.INDICATION, Codes.TIMELOCK, Constants.TRUE);
    	}
    	
    	@Override
    	public void requestLockClear() {
    		Processor.requestLockClear();
			Transmitter.createPacket(CodePurpose.INDICATION, Codes.TIMELOCK, Constants.FALSE);
			if (MyLocks.contains(LogicLocks.TRAFFICLOCK)) {
				Transmitter.createPacket(CodePurpose.INDICATION, Codes.SIGNALINDICATIONLOCK, Constants.FALSE);
			}
    	}
    }
    /***************************************************************************
     * end of CPTimeLockDecorator
	 **************************************************************************/
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
            CPVitalLogic.class.getName());
}
/* @(#)CPVitalLogic.java */
