/* Name FieldSignal.java
 *
 * What:
 * This class provides the logic for determining what aspect a signal should be displaying
 * based on the recommendation from the encompassing VitalLogic (either IntermediateVitalLogic or
 * CPVitalLogic) and the FieldSignal in advance (behind or upstream).  Whenever the aspect
 * changes, the new aspect is forwarded to the GUI over the code line.
 * <p>
 * All but one aspect is defined in the ApsectMap file.  "Tumbledown" is missing because it is
 * a variation on Stop that is propagated to the first Control Point or Head signal in approach.
 * It is not included because it is not in the aspect template table.  It is exchanged only between
 * FieldSignals.  Consequently, some contorted code exists to identify it and handle it.
 */
package cats.layout.vitalLogic;

import cats.layout.AspectMap;
import cats.layout.BiDecoderObserver;
import cats.layout.AspectMap.INDICATION_TYPE;
import cats.layout.items.IOSpec;
import cats.layout.items.PhysicalSignal;
import cats.layout.items.Track;
import cats.layout.vitalLogic.BasicVitalLogic.EventLockDecorator;
import cats.layout.vitalLogic.IntermediateVitalLogic.FORCED_ASPECT;
import cats.trackCircuit.DefaultTrackCircuit;
import cats.trackCircuit.TrackCircuit;
import cats.trackCircuit.TrackCircuitListener;

/**
 * This class provides the logic for determining what aspect a signal should be displaying
 * based on the recommendation from the encompassing VitalLogic (either IntermediateVitalLogic or
 * CPVitalLogic) and the FieldSignal in advance (behind or upstream).  Whenever the aspect
 * changes, the new aspect is forwarded to the GUI over the code line.
 * <p>
 * All but two aspects are defined in the AspectMap file.  "Tumbledown" and "Opposing" are missing
 * because they are a variation on Stop that is propagated to the first Control Point or Head signal in approach.
 * They are not included because they are not in the aspect template table.  They are exchanged only between
 * FieldSignals.  Consequently, some contorted code exists to identify them and handle them.
 * 
 * Title: CATS - Crandic Automated Traffic System
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2014, 2015, 2017, 2018, 2019, 2024
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public class FieldSignal implements SignalMastVitalLogic, BiDecoderObserver, TrackCircuitListener {
	
	/**
	 * the VitalLogic containing the Signal
	 */
	private final IntermediateVitalLogic VitalLogic;
	
	/**
	 * the last recommended aspect sent from the VitalLogic
	 */
	private FORCED_ASPECT VitalLogicState = FORCED_ASPECT.NONE;
	
	/**
	 * a flag designating if Advance aspects should be used or not.  True means
	 * they are used.
	 */
	private final boolean UseAdvance;
	
	/**
	 * the current aspect
	 */
	private int MyAspectIndex = -1;
	
	/**
	 * the aspect forwarded to the signal in approach.  Usually, this
	 * is the same as MyAspectIndex.  The exception is when it is
	 * Tumbledown, then MyAspectIndex will be Stop.
	 */
	private int ApproachAspectIndex = -1;
	
	/**
	 * the speed of the track being protected
	 */
	private int MySpeed = -1;
	
	/**
	 * the last aspect sent from the FieldSignal in Advance.  This is
	 * an index into AspectMap.IndicationNames, the name of a cell in a template.
	 * -1 says that no advance FieldSignal has sent anything, which could
	 * mean that the signal is protecting unbonded track.
	 * @see cats.layout.AspectMap.IndicationNames
	 */
	private int NextAspectIndex = -1;
	
	/**
	 * is the PhysicalSignal driven by this FieldSignal
	 */
	private final PhysicalSignal MySignal;
	
	/**
	 * the TrackCircuit that this signal terminates.
	 */
	protected TrackCircuit MyTrackCircuit;
	
	/**
	 * the ctor
	 * @param vitalLogic is the Vital Logic that the signal is attached to
	 * @param sig is the signal representation in the GUI
	 */
	public FieldSignal(final IntermediateVitalLogic vitalLogic, final PhysicalSignal sig) {
		VitalLogic = vitalLogic;
		MySignal = sig;
		if (MySignal != null) {
			UseAdvance = MySignal.isUsingAdvance();
			MyTrackCircuit = new DefaultTrackCircuit(MySignal.SignalName);
		}
		else {
			UseAdvance = false;
			MyTrackCircuit = new DefaultTrackCircuit(null);
		}
		MyTrackCircuit.addListener(this);
	}

	@Override
	public void forceAspect(final FORCED_ASPECT override) {
		VitalLogicState = override;
		determineIndication();
	}

	@Override
	public void setSpeed(final int speed) {
		if (speed != MySpeed) {
			MySpeed = speed;	
			determineIndication();
		}
	}

	@Override
	public void setAdvanceAspect(final int aspect) {
		if (aspect == -1) {
			NextAspectIndex = -1;
		}
		else {
//			NextAspectIndex = (UseAdvance || (AspectMap.IndicationType[aspect] != INDICATION_TYPE.APPROACH)) ?
//					aspect : (aspect - 1);
			NextAspectIndex = aspect;
		}
		determineIndication();
	}
	
	@Override
	public void setApproachLighting(final IntermediateVitalLogic myLogic) {
		IOSpec trigger;
		EventLockDecorator approachLock;
		if ((MySignal != null) && MySignal.isUsingApproachLighting()) {
			if ((trigger = MySignal.getApproachTrigger()) == null) {
				approachLock = VitalLogic.makeLockListener(LogicLocks.APPROACHLOCK, this);
				MySignal.lightUpSignal(approachLock.isLockSet());
			}
			else {
				trigger.registerListener(this);
				trigger.registerOtherListener(this);
			}
		}
	}
	
	/**
	 * this method performs the heavy lifting of figuring out
	 * what the indication is.  If the indication changed,
	 * then the method stimulates the hardware, updates the
	 * GUI, and relays the new indication to the approach signal.
	 * 
	 * The priority of inputs is:
	 * 1. from VitalLogic
	 * 2. unbonded or end of track
	 * 3. the indication of the signal in advance
	 */
	private void determineIndication() {
		int oldAspect = MyAspectIndex;
		int nextSpeed = AspectMap.getSpeed(NextAspectIndex);
		int lastApproachIndex = ApproachAspectIndex;
		
		if ((VitalLogicState == FORCED_ASPECT.RESTRICTING) || 
				((nextSpeed == Track.NONE) && (VitalLogicState == FORCED_ASPECT.NONE))) {
			// CALL-ON or last track with no blockages
			MyAspectIndex = AspectMap.getRule(MySpeed, Track.NONE);
		}
		else if ((VitalLogicState == FORCED_ASPECT.STOP_AND_GO)) {
			// TNT or OOS
			MyAspectIndex = AspectMap.PROCEED_INDEX;
		}
		else if (VitalLogicState == FORCED_ASPECT.STOP) {
			// Blockage without traffic stick
			MyAspectIndex = AspectMap.STOP_INDEX;
		}
		else if (NextAspectIndex > AspectMap.STOP_INDEX) {
			// bounds check
			MyAspectIndex = NextAspectIndex;
		}
		else {
//			switch (AspectMap.IndicationType[NextAspectIndex]) {
//			case HALT:
//				nextSpeed = Track.STOP;
//				break;
//			case APPROACH:
//				if (UseAdvance) {
//					nextSpeed = Track.APPROACH;
//				}
//				break;
//			default:
//			}
			if ((AspectMap.IndicationType[NextAspectIndex] == INDICATION_TYPE.APPROACH) && UseAdvance && (MySpeed == Track.NORMAL)) {
				MyAspectIndex = AspectMap.getRule(nextSpeed, Track.APPROACH);
			}
			else {
				MyAspectIndex = AspectMap.getRule(MySpeed, nextSpeed);
			}
		}
		if (MyAspectIndex != oldAspect) {
			ApproachAspectIndex = determineApproachAspect();
			if (ApproachAspectIndex != lastApproachIndex) {
				MyTrackCircuit.sendAspectChange(ApproachAspectIndex);

			}
			if (MySignal != null) {
				MySignal.setAspect(Math.min(AspectMap.STOP_INDEX, MyAspectIndex));
				// this is really the place to generate the code from the field indication
			}
		}
		if (VitalLogic != null) {  // always send the indication
			VitalLogic.updateIndication(Math.min(AspectMap.STOP_INDEX, MyAspectIndex));
		}
	}
	
	@Override
	public void acceptIOEvent() {
		if (MySignal != null) {
			MySignal.lightUpSignal(true);
		}
	}

	@Override
	public void acceptOtherIOEvent() {
		if (MySignal != null) {
			MySignal.lightUpSignal(false);
		}
	}
	
	@Override
	public TrackCircuit getTrackCircuit() {
		return MyTrackCircuit;
	}

	/**
	 * This method post-processes the current aspect before forwarding
	 * it to the approach signal.  The post processing translates the
	 * propagation STOP aspects to a hard STOP when this signal is absolute.
	 * @return the post-processed aspect index
	 */
	protected int determineApproachAspect() {
		if (MyAspectIndex >= AspectMap.STOP_INDEX) {
			if (VitalLogic.isTumbleDown()) {
				return AspectMap.TUMBLEDOWN_INDEX;
			}
			else {
				return AspectMap.STOP_INDEX;
			}
		}
		return MyAspectIndex;
	}

	@Override
	public int getAspect() {
		return MyAspectIndex;
	}
}
/* @(#)FieldSignal.java */
