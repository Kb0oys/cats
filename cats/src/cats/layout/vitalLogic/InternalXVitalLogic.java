/* Name: BasicVitalLogic.java
 *
 * What:
 * This class extends BasicVitalLogic to support the Vital Logic at an internal crossing (diamond).
 * */
package cats.layout.vitalLogic;


/**
 * This class extends BasicVitalLogic to support the Vital Logic at an internal crossing (diamond).
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2018, 2019</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class InternalXVitalLogic extends BasicVitalLogic implements CrossingVitalLogic {

	/**
	 * The crossing heavy lifter
	 */
	private XVitalLogic XLogic;

	/**
	 * the ctor
	 * @param id is a String for identifying the Vital Logic in debugging
	 * @param index is an integer for marking the source of a lock.  This is the edge
	 * the Vital Logic is associated with, since it is unique.
	 */
	public InternalXVitalLogic(String id, int index) {
		super(null, id);
		XLogic = new XVitalLogic(index, this);
	}

	@Override
	public void configureLockProcessing() {
		// MyLocks DETECTION is set/cleared by Block.
		//		LockProcessor.put(LogicLocks.DETECTIONLOCK, new CrossingLockDecorator (
		//				new LockPropagationDecorator(new MergedRequestLockProcessor(LogicLocks.DETECTIONLOCK)), XLogic));
		LockProcessor.put(LogicLocks.DETECTIONLOCK, 
//				new LockFilterDecorator(
//						new CommonDetectionDecorator(
//								new MergedLockFilterDecorator(
//										new MyLocksCrossingLockDecorator(
//												new LockPropagationDecorator(
//														new MergedRequestLockProcessor(LogicLocks.DETECTIONLOCK)), XLogic)))));
				new LockFilterDecorator(
						new CommonDetectionDecorator(
								new MyLocksCrossingLockDecorator(
										new MyLocksFilterDecorator(
												new LockPropagationDecorator(
														new DefaultRequestLockProcessor(LogicLocks.DETECTIONLOCK))), XLogic))));
		//		LockProcessor.put(LogicLocks.CONFLICTINGSIGNALLOCK, new LockPropagationDecorator(new MergedRequestLockProcessor(LogicLocks.CONFLICTINGSIGNALLOCK)));
		LockProcessor.put(LogicLocks.CONFLICTINGSIGNALLOCK,
				new LockFilterDecorator(
						new LockPropagationDecorator(
								new DefaultRequestLockProcessor(LogicLocks.CONFLICTINGSIGNALLOCK))));
		// HOLDLOCK is never set or cleared
		// SWITCHUNLOCK
		LockProcessor.put(LogicLocks.SWITCHUNLOCK,
				new LockFilterDecorator(
						new DefaultRequestLockProcessor(LogicLocks.SWITCHUNLOCK)));
		// APPROACHLOCK is never set or cleared
		//		LockProcessor.put(LogicLocks.TIMELOCK, new MergedLockPropagationDecorator(new AdvanceRequestLockProcessor(LogicLocks.TIMELOCK)));
		//		LockProcessor.put(LogicLocks.TIMELOCK, new LockPropagationDecorator(new DefaultRequestLockProcessor(LogicLocks.TIMELOCK)));
		LockProcessor.put(LogicLocks.TIMELOCK,
				new MyLocksFilterDecorator(
						new LockPropagationDecorator(
								new DefaultRequestLockProcessor(LogicLocks.TIMELOCK))));
		// TRAFFICLOCK is never set or cleared
		// MyLocks ROUTELOCK will mirror AdvanceLocks via DETECTION
		//		LockProcessor.put(LogicLocks.ROUTELOCK, new CrossingLockDecorator(
		////				new MergedLockPropagationDecorator(new AdvanceRequestLockProcessor(LogicLocks.ROUTELOCK)), XLogic));
		//				new LockPropagationDecorator(new DefaultRequestLockProcessor(LogicLocks.ROUTELOCK)), XLogic));
		LockProcessor.put(LogicLocks.ROUTELOCK, 
				new MergedLockFilterDecorator(
						new CrossingLockDecorator(
								new LockPropagationDecorator(
										new MergedRequestLockProcessor(LogicLocks.ROUTELOCK)), XLogic)));
		// MyLocks PROTECTIONLOCK will mirror AdvanceLocks via DETECTION
		//		LockProcessor.put(LogicLocks.PROTECTIONLOCK, new CrossingLockDecorator(
		////				new MergedLockPropagationDecorator(new AdvanceRequestLockProcessor(LogicLocks.PROTECTIONLOCK)), XLogic));
		//				new LockPropagationDecorator(new DefaultRequestLockProcessor(LogicLocks.PROTECTIONLOCK)), XLogic));
		LockProcessor.put(LogicLocks.PROTECTIONLOCK, 
				new MergedLockFilterDecorator(
						new LockPropagationDecorator(
								new MergedRequestLockProcessor(LogicLocks.PROTECTIONLOCK))));
		// MAINTENANCELOCK
		//		LockProcessor.put(LogicLocks.MAINTENANCELOCK, new CrossingLockDecorator(
		////				new MergedLockPropagationDecorator(new AdvanceRequestLockProcessor(LogicLocks.MAINTENANCELOCK)), XLogic));
		//				new LockPropagationDecorator(new DefaultRequestLockProcessor(LogicLocks.MAINTENANCELOCK)), XLogic));
		LockProcessor.put(LogicLocks.MAINTENANCELOCK, 
				new LockFilterDecorator(
						new MyLocksCrossingLockDecorator(
								new MyLocksFilterDecorator(
										new LockPropagationDecorator(
												new DefaultRequestLockProcessor(LogicLocks.MAINTENANCELOCK))), XLogic)));
		// CALLONLOCK is never set or cleared
		//		LockProcessor.put(LogicLocks.FLEETLOCK, new MergedLockPropagationDecorator(new AdvanceRequestLockProcessor(LogicLocks.FLEETLOCK)));
		//		LockProcessor.put(LogicLocks.FLEETLOCK, new LockPropagationDecorator(new DefaultRequestLockProcessor(LogicLocks.FLEETLOCK)));
		// FLEETLOCK should never be set/cleared
		//		LockProcessor.put(LogicLocks.FLEETLOCK,
		//				new MyLocksFilterDecorator(
		//						new LockPropagationDecorator(
		//								new DefaultRequestLockProcessor(LogicLocks.FLEETLOCK))));
		//		LockProcessor.put(LogicLocks.EXTERNALLOCK, new MergedLockPropagationDecorator(new AdvanceRequestLockProcessor(LogicLocks.EXTERNALLOCK)));
		//		LockProcessor.put(LogicLocks.EXTERNALLOCK, new LockPropagationDecorator(new DefaultRequestLockProcessor(LogicLocks.EXTERNALLOCK)));
		LockProcessor.put(LogicLocks.EXTERNALLOCK, 
				new LockFilterDecorator(
						new MyLocksCrossingLockDecorator(
								new MergedLockFilterDecorator(
										new DefaultRequestLockProcessor(LogicLocks.EXTERNALLOCK)), XLogic)));
	}

	@Override
	public void linkCrossing(final CrossingVitalLogic crossing[]) {
		CrossingInterface c[] = new CrossingInterface[2];
		c[0] = crossing[0].getCrossing();
		c[1] = crossing[1].getCrossing();
		XLogic.setCrossings(c);
	}

	@Override
	public CrossingInterface getCrossing() {
		return XLogic;
	}
}
/* @(#)InternalXVitalLogic.java */
