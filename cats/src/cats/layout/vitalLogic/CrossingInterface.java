/* Name: XBlkVitalLogic.java
 *
 * What:
 * This file contains the definition for the interface supported by VitalLogic on the
 * side of a crossing (diamond).  It encapsulates the protocol that is used to set
 * and clear locks on crossing tracks.
 */
package cats.layout.vitalLogic;

/**
 * This file contains the definition for the interface supported by VitalLogic on the
 * side of a crossing (diamond).  It encapsulates the protocol that is used to set
 * and clear locks on crossing tracks.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public interface CrossingInterface {
	/**
	 * tells this XVitalLogic where its blocking XVitalLogics are.
	 * @param crossings are the blocking XVitalLogics
	 */
	public void setCrossings(final CrossingInterface crossings[]);


	/**
	 * is called by one of the VitalLogics protecting
	 * the crossing tracks to set the ConflictingSignal
	 * lock.
	 * @param xLock is the lock to set on the crossing track
	 */
	public void blockCrossTraffic(final LogicLocks xLock);

	/**
	 * is called by one of the VitalLogics protecting
	 * the crossing tracks to clear the ConflictingSignal
	 * lock.
	 * @param xLock is the lock to clear on the crossing track
	 */
	public void unblockCrossTraffic(final LogicLocks xLock);
	
	/**
	 * called by a crossing track to set a lock in this track
	 * @param lock is the lock to set
	 * @param who is the index of the blocknig track
	 */
	public void setCrossLock(LogicLocks lock, int who);
	
	/**
	 * called by a crossing track to clear a lock in this track
	 * @param lock is the lock to set
	 * @param who is the index of the blocknig track
	 */
	public void clearCrossLock(LogicLocks lock, int who);

}
/* @(#)CrossingInterface.java */