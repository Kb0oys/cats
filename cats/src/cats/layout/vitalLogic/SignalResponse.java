/* Name: ArrowEvent.java
 *
 * What:
 *   This class queues a change in a signal's indication for processing by VitalLogic.
 */
package cats.layout.vitalLogic;

import cats.rr_events.RREvent;

/**
 *   This class queues a change in a signal's indication for processing by VitalLogic
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class SignalResponse extends RREvent {
	/**
	 * the index into the Aspect table of the new indication
	 */
	private final int Indication;
	
	/**
	 * the VitalLogic containing the signal
	 */
	private final IntermediateVitalLogic VitalLogic;
	
	public SignalResponse(final int indication, final IntermediateVitalLogic who) {
		Indication = indication;
		VitalLogic = who;
	}
	
	@Override
	  public void doIt() {
		VitalLogic.updateIndication(Indication);
	  }
}
/* @(#)SignalResponse.java */