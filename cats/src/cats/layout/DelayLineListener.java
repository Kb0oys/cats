/* Name: DelayLineListener.java
 *
 * What:
 *   This class defines an interface for objects that consume
 *   items from a DelayLine.  Implementing classes have a single
 *   method - consume() - for receiving something from the
 *   DelayLine
 *
 * Special Considerations:
 *   This class is application independent.
 */
package cats.layout;

/**
 *   This class defines an interface for objects that consume
 *   items from a DelayLine.  Implementing classes have a single
 *   method - consume() - for receiving something from the
 *   DelayLine
 * <p>
 * Title: CATS - Crandic Automated Traffic System
 * </p>
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2020
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public interface DelayLineListener<T> {

	/**
	 * is called to pass something to the consumer
	 * @param item is something recived through the DelayLine
	 */
	public void consume(T item);
}
/* @(#)DelayLineListener.java */

