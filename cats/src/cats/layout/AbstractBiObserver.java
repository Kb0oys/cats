/* Name: AbstractBiObserver.java
 *
 * What:
 *   This class is the abstract class for objects that want to listen
 *   to both the trigger on an IOEvent and when it disappears
 */
package cats.layout;

import cats.layout.items.IOSpec;

/**
 *   This class is the abstract class for objects that want to listen
 *   to both the trigger on an IOEvent and when it disappears
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2004, 2009, 2010, 2011, 2012</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
abstract public class AbstractBiObserver implements BiDecoderObserver {

  /**
   * the ctor
   * @param decoder is the decoder to monitor
   */
  public AbstractBiObserver(IOSpec decoder) {
    decoder.registerListener(this);
    decoder.registerOtherListener(this);
  }
  
  /**
   * the method invoked when the device is triggered
   */
  abstract public void acceptIOEvent();
  
  /**
   * the method invoked when the device is untriggered
   */
  abstract public void acceptOtherIOEvent();

}
/* @(#)AbstractBiObserver.java */