/* Name: MasterClock.java
 *
 * What:
 * This class is a singleton, one second timer.  On every "tick", it
 * walks the list of registered TimeoutEvents and decrements their
 * internal count.  For each event that reaches 0, it removes the event and
 * queues it for delivery to the event observer.  Thus, it forms an
 * approximate timer.  It is approximate because the event will be
 * registered between "ticks".  It is approximate because the event
 * will reside on the RREvent queue for delivery an indeterminate
 * amount of time.
 * <p>
 * An ambiguous condition exists while the event is on the delivery
 * queue.  In that interval, the timer has expired, but the observer
 * does not know it.  Thus, a "free" flag is cleared when the event is queued
 * by this class and cleared when it is delivered.  Though this seems
 * overly complex and the event could have been delivered inline with
 * the count becoming zero, that solutions had several problems.
 * <ol>
 * <li>
 * the event list must be locked to prevent corruption should
 * an object attempt to add (or remove) an event while MasterClock processes
 * the "tick"
 * </li>
 * <li>
 * Since the event list is locked, the "tick" processing must be short.
 * Thus, when an expired timer is found, it is dumped on the delivery queue.
 * The actual event processing is deferred.
 * </li>
 * <li>
 * Though unlikely, "tick" processing could take longer than a second. Things
 * could get really messed up if the observer list was being processed
 * when another second elapsed.
 * </li>
 * </ol>
 * <p>
 * Why does this class exist?  The Java class performs a lot of work not
 * needed by users.  They do not need high resolution timings.  Furthermore,
 * anecdotal storied indicate that a Java system can bog down when a lot of
 * one second range timers are in use.  So, this class attempts to be less
 * high precision, but less time consuming.
 */
package cats.layout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.swing.Timer;

import cats.rr_events.TimeoutEvent;

/**
 * This class is a singleton, one second timer.  On every "tick", it
 * walks the list of registered TimeoutEvents and decrements their
 * internal count.  For each event that reaches 0, it removes the event and
 * queues it for delivery to the event observer.  Thus, it forms an
 * approximate timer.  It is approximate because the event will be
 * registered between "ticks".  It is approximate because the event
 * will reside on the RREvent queue for delivery an indeterminate
 * amount of time.
 * <p>
 * An ambiguous condition exists while the event is on the delivery
 * queue.  In that interval, the timer has expired, but the observer
 * does not know it.  Thus, a "free" flag is set when the event is queued
 * by this class and cleared when it is delivered.  Though this seems
 * overly complex and the event could have been delivered inline with
 * the count becoming zero, that solutions had several problems.
 * <ol>
 * <li>
 * the event list must be locked to prevent corruption should
 * an object attempt to add (or remove) an event while MasterClock processes
 * the "tick"
 * </li>
 * <li>
 * Since the event list is locked, the "tick" processing must be short.
 * Thus, when an expired timer is found, it is dumped on the delivery queue.
 * The actual event processing is deferred.
 * </li>
 * <li>
 * Though unlikely, "tick" processing could take longer than a second. Things
 * could get really messed up if the observer list was being processed
 * when another second elapsed.
 * </li>
 * </ol>
 * <p>
 * Why does this class exist?  The Java class performs a lot of work not
 * needed by users.  They do not need high resolution timings.  Furthermore,
 * anecdotal stories indicate that a Java system can bog down when a lot of
 * one second range timers are in use.  So, this class attempts to be less
 * high precision, but less time consuming.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2019, 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */

public class MasterClock {
	
	/**
	 * the 1 second timeout - 1000 milliseconds
	 */
	private final static int OneSecond = 1000;
	
	/**
	 * the external view of one second.  It is derived from
	 * OneSecond
	 */
	
	public final static int ONE_SECOND = 1;
	
	/**
	 * the singleton
	 */
	public final static MasterClock MyClock = new MasterClock();

	/**
	 * the 1 second timer
	 */
	private Timer TickTimer;

	/**
	 * the list of objects waiting on timeouts
	 */
	List Observers = Collections.synchronizedList(new ArrayList());
	
	/**
	 * the ctor
	 */
	public MasterClock() {
		TickTimer = new Timer(0, new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				synchronized(Observers) {
					int index = Observers.size();
					while (--index >= 0) {
						if (--((TimeoutEvent) Observers.get(index)).Count <= 0) {
							((TimeoutEvent) Observers.get(index)).queUp();	
							Observers.remove(index);
							if (Observers.isEmpty()) {
								TickTimer.stop();
							}
						}
					}
				}
			}
		});
		TickTimer.setRepeats(true);
		TickTimer.setDelay(OneSecond);
		TickTimer.setInitialDelay(OneSecond);
	}
	
	/**
	 * registers the request for the timeout
	 * @param event is the event
	 */
	public void setTimeout(TimeoutEvent event) {
		if (event != null) {
			synchronized(Observers) {
				Iterator i = Observers.iterator(); // Must be in synchronized block
				while (i.hasNext()) {
					if (((TimeoutEvent) i.next()).equals(event)) {
						return;
					}
				}
				event.EventFree = false;
				Observers.add(event);
				if (Observers.size() == 1) {
					TickTimer.start();
				}
			}
		}
	}
	
	/**
	 * cancel the timer - remove from observer list
	 * @param event true if found.  False if not.
	 * @return false if the cancellation failed
	 */
	public boolean cancelTimer(TimeoutEvent event) {
		int index = 0;
		if (event == null) {
			return true;
		}
		synchronized(Observers) {
			Iterator i = Observers.iterator(); // Must be in synchronized block
			while (i.hasNext()) {
				if (((TimeoutEvent) i.next()).equals(event)) {
					Observers.remove(index);
					if (Observers.isEmpty()) {
						TickTimer.stop();
					}
					return true;
				}
				else {
					++index;
				}
			}  
		}
		return false;
	}
}
/* @(#)MasterClock.java */
