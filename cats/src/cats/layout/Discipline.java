/* Name Block.java
 *
 * What:
 *  This class holds an enumeration defining the disciplines for controlling the layout.
 *  They are:
 *  <ul>
 *  <li>UNDEFINED - which is distorted to mean "unbonded".  In the prototype, "unbonded"
 *  means that a chunk of track has no occupancy detection.  However, CATS is designed
 *  to let a layout grow and to operate as a magnet board.  Thus, a Block could be
 *  defined as something other than UNDEFINED and not have any detectors defined.
 *  As such, the layout should be controlled as though the detection was defined.  The
 *  dispatcher would just have to manually occupy and unoccupy Blocks.
 *  </li>
 *  <li>ABS - the signals are the simplest form of "automatic".  Any Block containing
 *  a non-safe condition (occupied, fouling points, etc.) is protected by Halt signals.
 *  The next signal out will show Approach (assuming it, too, is not protecting an
 *  unsafe condition), and the next one out from that will show Advance Approach.
 *  <li>APB - an enhancement on ABS such that when a train passes a control point,
 *  all signals opposing it drop to HALT.
 *  </li>
 *  <li>CTC - All signals are Halt, until the dispatcher sets a route.  Then, only
 *  the entry signal will show a non-Halt.
 *  </li>
 *  <li>DTC - Similar to CTC, but intended for non-signalled layouts.  The track
 *  behind a train remains colored until the dispatcher clears them, simulating a
 *  simple for form of track warrants.  The coloring shows the limits of the
 *  warrant.
 *  </li>
 *  </ul>
 *  <p>
 *  A look up table is needed from discipline name to the enumerated value because
 *  when I first wrote this, I did not understand APB, so three APB names are accepted,
 *  but they all do the same thing.
 */
package cats.layout;

/**
 *  This class holds an enumeration defining the disciplines for controlling the layout.
 *  They are:
 *  <ul>
 *  <li>UNDEFINED - which is distorted to mean "unbonded".  In the prototype, "unbonded"
 *  means that a chunk of track has no occupancy detection.  However, CATS is designed
 *  to let a layout grow and to operate as a magnet board.  Thus, a Block could be
 *  defined as something other than UNDEFINED and not have any detectors defined.
 *  As such, the layout should be controlled as though the detection was defined.  The
 *  dispatcher would just have to manually occupy and unoccupy Blocks.
 *  </li>
 *  <li>ABS - the signals are the simplest form of "automatic".  Any Block containing
 *  a non-safe condition (occupied, fouling points, etc.) is protected by Halt signals.
 *  The next signal out will show Approach (assuming it, too, is not protecting an
 *  unsafe condition), and the next one out from that will show Advance Approach.
 *  <li>APB - an enhancement on ABS such that when a train passes a control point,
 *  all signals opposing it drop to HALT.
 *  </li>
 *  <li>CTC - All signals are Halt, until the dispatcher sets a route.  Then, only
 *  the entry signal will show a non-Halt.
 *  </li>
 *  <li>DTC - Similar to CTC, but intended for non-signalled layouts.  The track
 *  behind a train remains colored until the dispatcher clears them, simulating a
 *  simple for form of track warrants.  The coloring shows the limits of the
 *  warrant.
 *  </li>
 *  </ul>
 *  <p>
 *  A look up table is needed from discipline name to the enumerated value because
 *  when I first wrote this, I did not understand APB, so three APB names are accepted,
 *  but they all do the same thing.
 * 
 * Title: CATS - Crandic Automated Traffic System
 * </p>
 * <p>
 * Description: A program for dispatching trains on Pat Lana's Crandic model
 * railroad.
 * <p>
 * Copyright: Copyright (c) 2012
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Rodney Black
 * @version $Revision$
 */
public enum Discipline {
  UNDEFINED,      // the block has no defined disciplined.  It will be treated as dark or unbonded track.
  CTC,            // Centralized Traffic Control
  ABS,            // Automatic Block System
  APB,            // Absolute Permissive
  DTC;            // Direct Train Control

  /**
   * looks up the Discipline associated with a String
   * @param name is the name of the Discipline, defined in designer
   * @return the internal Enum for the name if found; otherwise, null.
   */
  static public final Discipline toDiscipline(String name) {
    if ("Unknown".equals(name)) {
      return UNDEFINED;
    }
    if ("CTC".equals(name)) {
      return CTC;
    }
    if ("ABS".equals(name)) {
      return ABS;
    }
    if ("APB-2".equals(name)) {
      return APB;
    }
    if ("APB-3".equals(name)) {
      return APB;
    }
    if ("DTC".equals(name)) {
      return DTC;
    }
    return null;
  }
}
/* @(#)Discipline.java */