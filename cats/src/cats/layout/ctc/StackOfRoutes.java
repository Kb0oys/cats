/* Name: InternalXEdge.java
 *
 * What:
 * This class is a Singleton that holds the routes that have been stacked.  One design
 * had a stack for each Block, but that design seemed to have race conditions (in a multi-
 * block route) or the execution order may not be the same as the stacking order.  Another
 * design maintained the route stack in the originating CPEdge, but it also had problems in
 * being alerted when the route reached the top of the stack and maintaining the order.
 * Thus, this design centralizes the stack.
 * <p>
 * When a route reaches the end of the stack (is ready for execution), it is checked for
 * immediate execution.  If ready, it is executed, similar to N/X extended routes (i.e.
 * turnouts can be thrown).  If it is not ready then each block on the route is told who the
 * route is.  As they clear, they poke the rooute anchor to scan the route.  If it is ready
 * for execution, then turnouts are set and quick routes made, like N/X.  If even one block
 * is not ready, then the route waits as next to execute.
 */
package cats.layout.ctc;

import java.awt.Point;
import java.util.BitSet;
import java.util.Vector;

import cats.apps.Crandic;
import cats.common.DebugBits;
import cats.gui.CounterFactory;
import cats.gui.jCustom.RouteEditPane;
import cats.gui.jCustom.RouteManagerModel;
import cats.layout.MasterClock;
import cats.layout.TimeoutObserver;
import cats.layout.items.CPEdge;
import cats.rr_events.StackedRouteTrigger;
import cats.rr_events.TimeoutEvent;

/**
 * This class is a Singleton that holds the routes that have been stacked (actually FIFO).  One design
 * had a stack for each Block, but that design seemed to have race conditions (in a multi-
 * block route) or the execution order may not be the same as the stacking order.  Another
 * design maintained the route stack in the originating CPEdge, but it also had problems in
 * being alerted when the route reached the top of the stack and maintaining the order.
 * Thus, this design centralizes the stack.
 * <p>
 * When a route reaches the end of the stack (is ready for execution), it is checked for
 * immediate execution.  If ready, it is executed, similar to N/X extended routes (i.e.
 * turnouts can be thrown).  If it is not ready then each block on the route is told who the
 * route is.  As they clear, they poke the route anchor to scan the route.  If it is ready
 * for execution, then turnouts are set and quick routes made, like N/X.  If even one block
 * is not ready, then the route waits as next to execute.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class StackOfRoutes {

//	enum ACTIVATION_STATES  {
//		IDLE,				// no node at top of stack
//		WAITING_FOR_CHANGE,	// at TOS, waiting for nodes to report that locks have cleared
//		WAITING_FOR_CHECK,	// the change trigger has been queued, so waiting for it to dequeue
//		STABILITY_TIMER		// at least one path looks clear; delaying to see if it was transient
//	}
	
	/**
	 * the Singleton
	 */
	static private StackOfRoutes TheStack;
	
	/**
	 * the "top of stack" DAG pointer - the root of the stack.
	 */
	private StackEntry OldestRoute;
	
//	/**
//	 * the DAG in the process of transitioning from stacked to active.  It serves a secondary purpose
//	 * of maintaining the state of the stack.  Null means that the stack is waiting for a trigger event
//	 * from the layout, indicating that a blocking condition has been removed.  A non-null value holds
//	 * the DAG that was found to be non-blocking is waiting for the stability timer to expire to
//	 * check that the DAG still has a non-blocking path.
//	 */
//	private RouteNode Activating;
//	
//	/**
//	 * Recheck is a flag, which is set during the stability state when a layout event is received.
//	 * Receiving such an event may indicate that another path has opened up so processing is deferred
//	 * until the current activating DAG is complete - either found blocking or ready for conversion to
//	 * N/X.
//	 */
//	private boolean Recheck;
//	
//	/**
//	 * Cancel is a flag which is set during the stability state when the dispatcher has cancelled the
//	 * DAG that is activating.  If discovered set after the timeout, the DAG will be removed from the
//	 * stack and thrown away without doing anything with it.
//	 */
//	private boolean Cancel;
	
//	/**
//	 * the state machine for walking a DAG from "top of stack" to active routes
//	 */
//	private RouteActivation Machine = new RouteActivation();
	
	/**
	 * the accessor which guarantees that the RouteStack exists and protects the Singleton
	 * @return the singleton that is the stack of routes
	 */
	public static StackOfRoutes instance() {
		if (TheStack == null) {
			TheStack = new StackOfRoutes();
		}
		return TheStack;
	}
	
	/**
	 * the ctor
	 */
	private StackOfRoutes() {
		OldestRoute = null;
//		Activating = null;
	}
	
	/**
	 * pushes the anchor for a route (DAG) onto the stack.
	 * @param route is a route waiting for execution
	 */
	public void pushRoute(final RouteNode route) {
		StackEntry newEntry = new StackEntry(route);
		StackEntry p;
		route.setAnchorNode(route);  // for constructing the Extended Route name
//		route.setStackNode(null);
		route.setNodeStatus(RouteNodeStatus.STACKED);
		if (OldestRoute == null) {
			OldestRoute = newEntry;
//			route.makeActiveRoute();
//			Machine.startProcess();
		}
		else {
			// this adds the DAG at the end of the stack.  The stack should be short,
			// so this should not take long
			for (p = OldestRoute; p.NextEntry != null; p = p.NextEntry) {}
			p.NextEntry = newEntry;
		}
		route.registerEdge(true, newEntry.NodeIndex);
	}
	
	/**
	 * removes a DAG from the stack.  The caller must ensure that the requested DAG is not
	 * in the process of being activated, because this method does not check.
	 * @param dag is a DAG (route) to remove from the stack.  It must not be null.
	 */
	private void unStackRoute(final StackEntry dag) {
		StackEntry lead;
		StackEntry follow = OldestRoute;
		
		dag.TheRoute.registerEdge(false, dag.NodeIndex);
		if (OldestRoute == dag) {
			OldestRoute = OldestRoute.NextEntry;
		}
		else {
			for (lead = OldestRoute; lead != null; lead = lead.NextEntry) {
				if (lead == dag) {
					follow.NextEntry = dag.NextEntry;
					return;
				}
				follow = lead;
			}
		}
	}
	
	/**
	 * a query for testing is any routes have been stacked
	 * @return false if there is any routes stacked and true if there are none.
	 */
	public boolean isStackEmpty() {
		return OldestRoute == null;
	}
	
//	/**
//	 * removes the route (if found) from the stack.  If the route is the oldest entry on the stack,
//	 * then the next is prepared for execution
//	 * @param route is the route requested for removal
//	 */
//	public void cancelRoute(final RouteNode route) {
//		RouteNode p;
////		if ((route != null) && (OldestRoute != null)) {
////			if (OldestRoute == route) {
////				Machine.reset();
////				OldestRoute = OldestRoute.getStackNode();
//////				OldestRoute.removeActiveRouteRoute();
////				if (OldestRoute != null) {
//////					OldestRoute.makeActiveRoute();
////					Machine.startProcess();
////				}
////			}
////			else {
////				for (p = OldestRoute; p !=  null; p = p.getStackNode()) {
////					if (p.getStackNode() == route) {
////						p.setStackNode(route.getStackNode());
////						break;
////					}
////				}
////			}
////			route.removeNodes(null);
////		}
//		if ((route != null) && (OldestRoute != null)) {
//			if (Activating == route) {
//				Machine.reset();
//				OldestRoute = OldestRoute.getStackNode();
////				OldestRoute.removeActiveRouteRoute();
//				if (OldestRoute != null) {
////					OldestRoute.makeActiveRoute();
//					Machine.startProcess();
//				}
//			}
//			else {
//				for (p = OldestRoute; p !=  null; p = p.getStackNode()) {
//					if (p.getStackNode() == route) {
//						p.setStackNode(route.getStackNode());
//						break;
//					}
//				}
//			}
//			route.removeNodes(null);
//		}
//	}
	
	/**
	 * counts the number of routes stacked
	 * @return the count
	 */
	public int stackSize() {
		int count = 0;
		for (StackEntry route = OldestRoute; route != null; route = route.NextEntry) {
			++count;
		}
		return count;
	}
	
	/**
	 * create a Vector of the stacked anchors
	 * @return the stack as a Vector of RouteAnchor
	 */
	public Vector<StackEntry> createList() {
		int size = stackSize();
		if (size == 0) {
			return null;
		}
		Vector<StackEntry> list = new Vector(size);
		for (StackEntry route = OldestRoute; route != null; route = route.NextEntry) {
			list.add(route);
		}
		return list;
	}
	
	/**
	 * This method is the heart of the algorithm for determining when one of the stacked DAGs
	 * can be activated.  The criteria are that it must contain an unblocked path and it must
	 * not overlap any older DAG that is blocked; thus, preserving the FIFO property of when
	 * DAGs were stacked.
	 */
	private void findUnblockedPath() {
		StackEntry q;
		for (StackEntry p = OldestRoute; p != null; p = p.NextEntry) {
			if (p.TheRoute.routeCheck()) {
				for (q = OldestRoute; q != p; q = q.NextEntry) {
					if (q.NodeIndex.intersects(p.NodeIndex)) {
						break;
					}
				}
				if (q == p) {
					p.startTiming();
				}
			}
		}
	}
	
	/**
	 * triggers a search for an unblocked DAG (route), that does not have
	 * an overlapping DAG that is older
	 */
	static public void triggerBlockCheck() {
		new StackedRouteTrigger().queUp();
	}
	
	/**
	 * stimulates the state machine to check for lock changes
	 */
	static public void examineRoute() {
		TheStack.findUnblockedPath();
	}
	
	/**
	 * constructs a JTable listing all the stacked routes and allows the dispatcher
	 * to edit the stack.  The dispatcher can delete routes and reorder them.
	 * @param location is where on the screen to place the table panel
	 */
	static public void displayRoutes(Point location) {
		Vector<StackEntry> stack = TheStack.createList();
		Vector<StackEntry> deletedNodes = new Vector<StackEntry>();
		StackEntry p;
		if ((stack != null) && RouteEditPane.editList(new RouteManagerModel(stack, deletedNodes), location)) {
			if (stack.size() == 0) {  // the case of all routes being deleted
				// recycle all routes
				while (TheStack.OldestRoute != null) {
					p = TheStack.OldestRoute.NextEntry;
					TheStack.OldestRoute.NextEntry = null;
					TheStack.OldestRoute.cancelRoute();
					TheStack.OldestRoute = p;
				} ;
			}
			else {
				// deconstruct the stack
				for (StackEntry ptr = TheStack.OldestRoute; ptr != null; ) {
					p = ptr.NextEntry;
					ptr.NextEntry = null;
					ptr = p;
				}
				
				// rebuild the stack, based on the contents of stack.  There must be at least one element
				// because this didn't take the "if" branch
				TheStack.OldestRoute = p = stack.elementAt(0);
				for (int i = 1; i < stack.size(); ++i) {
					p.NextEntry = stack.elementAt(i);
					p = p.NextEntry;
				}
				// this frees up the deleted routes;
				// note that entries in the stability timing state have no links
				// to them except through the timing queue
				for (int i = 0; i < deletedNodes.size(); ++i) {
					deletedNodes.elementAt(i).cancelRoute();
				}
				
			}
		}		
	}
	
//	/******************************************************************************************************************
//	 * The following state machine handles moving a DAG from Top of Stack through active routes on CP Edges
//	 ******************************************************************************************************************/
//	class RouteActivation implements TimeoutObserver {
//
//		private ACTIVATION_STATES State = ACTIVATION_STATES.IDLE;
//
//		private TimeoutEvent TimeOut = new TimeoutEvent(this, 1);
//
//		public void reset() {
//			if (Crandic.Details.get(DebugBits.ROUTESTACKING)) {
//				System.out.println("Route stacking state machine received reset()");
//			}
//			
//			if (Activating != null) {
//				Activating.removeActiveRouteRoute();
//			}
//			State = ACTIVATION_STATES.IDLE;
//		}
//
//		/**
//		 * a node has reached TOS, so start the process going
//		 */
//		public void startProcess() {
//			if (Crandic.Details.get(DebugBits.ROUTESTACKING)) {
//				System.out.println("Route stacking state machine received startProcess()");
//			}
//			if (State == ACTIVATION_STATES.IDLE) {				
//				if (OldestRoute.routeCheck()) {
//					if (Crandic.Details.get(DebugBits.ROUTESTACKING)) {
//						System.out.println("Route stacking state machine received fall through");
//					}
//					TimeOut.Count = CounterFactory.CountKeeper.findSequence(CounterFactory.TURNOUTSAFETYTAG).getAdjustment();
//					MasterClock.MyClock.setTimeout(TimeOut);
//					State = ACTIVATION_STATES.STABILITY_TIMER;
//				}
//				else {
//					State = ACTIVATION_STATES.WAITING_FOR_CHANGE;	
//				}
//			}
//			else {
//				log.warn("Stack machine received startProcess() when not IDLE (" + State + ")");    
//			}
//		}
//
//		/**
//		 * a RouteEdge has reported a change in a lock.  The action is to queue a search of the DAG
//		 * for an unblocked route.
//		 */
//		public void routeChange() {
//			if (Crandic.Details.get(DebugBits.ROUTESTACKING)) {
//				System.out.println("Route stacking state machine checking changes");
//			}
//			switch(State) {
//			case WAITING_FOR_CHANGE:
//				State = ACTIVATION_STATES.WAITING_FOR_CHECK;
////				new StackedRouteTrigger(OldestRoute).queUp();
//				new StackedRouteTrigger().queUp();
//
//				break;
//			case WAITING_FOR_CHECK:
//				break;
//			default:
//				log.warn("Stack machine received routeChange() when not waiting for it (" + State + ")");    
//			}
//		}
//		
//		/**
//		 * the check trigger has worked its way through the queuing system, so run the check
//		 */
//		public void checkTrigger() {
//			if (Crandic.Details.get(DebugBits.ROUTESTACKING)) {
//				System.out.println("Route stacking state machine received queued trigger");
//			}
//			if (State == ACTIVATION_STATES.WAITING_FOR_CHECK) {
//				if (OldestRoute.routeCheck()) {
//					TimeOut.Count = CounterFactory.CountKeeper.findSequence(CounterFactory.TURNOUTSAFETYTAG).getAdjustment();
//					MasterClock.MyClock.setTimeout(TimeOut);
//					State = ACTIVATION_STATES.STABILITY_TIMER;					
//				}
//				else {
//					State = ACTIVATION_STATES.WAITING_FOR_CHANGE;
//				}
//			}
//		}
//		
//		@Override
//		public void acceptTimeout() {
//			if (Crandic.Details.get(DebugBits.ROUTESTACKING)) {
//				System.out.println("Route stacking state machine received timeout");
//			}
//			if (State == ACTIVATION_STATES.STABILITY_TIMER) {
//				if (OldestRoute.routeCheck()) {
//					((CPEdge) OldestRoute.Edge).deferredRoute(OldestRoute);
//					State = ACTIVATION_STATES.IDLE;
//					reset();
////					popRoute();
//					unStackRoute(Activating);
//				}
//				else {
//					State = ACTIVATION_STATES.WAITING_FOR_CHANGE;
//				}
//			}
//			else {
//				log.warn("Stack machine received acceptTimeout() when not running the timer (" + State + ")");
//			}
//		}
//	}
	
	/******************************************************************************************************************
	 * The following data structure contains the information needed for stacking the DAG that describes a route.
	 ******************************************************************************************************************/
	private class StackEntry implements TimeoutObserver {
		/**
		 * a BitSet for identifying the RouteNodes in the DAG.  Each RouteNode has a unique creation number.  Each RpouteNode has a 
		 * one-to-one correlation with a RouteEdge (CPEdge, FrogEdge, or PtsEdge)  Thus, The BitSet has one
		 * bit for each RouteEdge in the layout.  When a DAG is stacked, the creation number of each RouteEdge is retrieved and used
		 * to set the corresponding bit.  This allows CATS to use set operators to identify when two (or more) stacked routes overlap.
		 */
		public BitSet NodeIndex = new BitSet(TrackEdgeCounter.getEdgeCount());
		
		/**
		 * the linkage to the next entry, which forms the skeleton of the stack
		 */
		public StackEntry NextEntry;
		
		/**
		 * the anchor for the DAG
		 */
		public final RouteNode TheRoute;
		
		/**
		 * a flag set to true if the dispatcher cancels a stacked route while it
		 * is in the stability timeout period.
		 */
		private boolean Cancelled = false;
		
		/**
		 * the stability timer.  When a clear path in the DAG is discovered, 
		 * a timer is set to filter out transient events that might have erroneously
		 * provided the clear path.  The path must remain clear at the end of the
		 * timer inorder for the DAG to be converted to N/X.
		 */
		private TimeoutEvent TimeOut = new TimeoutEvent(this, 1);

		/**
		 * a flag, when true, indicating that the instability timer is running
		 */
		private boolean InStabilityTiming = false;
		
		
		/**
		 * the ctor
		 * @param startNode the RouteNode that anchors the route
		 * @param endNode the last RouteNode in the route
		 */
		public StackEntry(final RouteNode startNode) {
			TheRoute = startNode;
		}
		
		/**
		 * This method is called when the stack determines that a DAG has a clear path.  It starts a timer
		 * whose purpose is to stimulate checking the DAG later to be sure that the path was not the result
		 * of a transient.  The intent is to avoid throwing a turnout under a train because something like
		 * occupancy glitched.
		 */
		public void startTiming() {
			if (!InStabilityTiming) {
				TimeOut.Count = CounterFactory.CountKeeper.findSequence(CounterFactory.TURNOUTSAFETYTAG).getAdjustment();
				MasterClock.MyClock.setTimeout(TimeOut);
				InStabilityTiming = true;
			}
		}
		
		/**
		 * is called when a stacked route is cancelled by the dispatcher.  Note that the DAG is not
		 * in the stack.  The table editing placed it in a separate Vector of cancelled routes.  There are two
		 * cases to consider:
		 * 1. if the StackEntry is not waiting for the stability timer to expire, then the DAG is immediately
		 * recycled.
		 * 2. if the stability timer is running then the DAG will be recycled by the timeout.
		 * In either case, the call back linkage in the Edge is broken.
		 */
		public void cancelRoute() {
			TheRoute.registerEdge(false, NodeIndex);
			if (InStabilityTiming) {
				Cancelled = true;
			}
			else {
				TheRoute.removeNodes(null);
			}
		}
		
		@Override
		public void acceptTimeout() {
			if (Crandic.Details.get(DebugBits.ROUTESTACKING)) {
				System.out.println("Route stacking state machine received timeout");
			}
			if (Cancelled) {
				TheRoute.removeNodes(null);
			}
			else if (TheRoute.routeCheck()) {
				unStackRoute(this);
//				TheRoute.removeNodes(null);
				((CPEdge) TheRoute.Edge).directRoute(TheRoute);
			}
			else {
				InStabilityTiming = false;
			}
		}
		
		@Override
		public String toString() {
			return TheRoute.toString();
		}
	}
	
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(
			StackOfRoutes.class.getName());
}
/* @(#)StackOfRoutes.java */
