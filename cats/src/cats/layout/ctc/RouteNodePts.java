/* Name: RouteEdge.java
 *
 * What:
 *   A RouteNode customized for points.  Most RouteNodes for a linear chain -
 *   one link to the next RouteNode.  Points are different, because there are
 *   up to 3 next RouteNodes in advance.  These are what make a DAG network
 *   a DAG.  Traversing a RouteNode adds 1 to the route complexity metric.
 *   The alignment need not reflect the current alignment of points, particularly
 *   when searching for the route exit.
 */
package cats.layout.ctc;

import cats.apps.Crandic;
import cats.common.Constants;
import cats.common.DebugBits;
import cats.common.Sides;
import cats.layout.items.RouteEdge;
import cats.layout.items.FrogEdge;
import cats.layout.items.OSEdge;
import cats.layout.items.PtsEdge;

/**
 *   A RouteNode customized for points.  Most RouteNodes for a linear chain -
 *   one link to the next RouteNode.  Points are different, because there are
 *   up to 3 next RouteNodes in advance.  These are what make a DAG network
 *   a DAG.  Traversing a RouteNode adds 1 to the route complexity metric.
 *   The alignment need not reflect the current alignment of points, particularly
 *   when searching for the route exit.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class RouteNodePts extends RouteNode {
	/**
	 * the link to free RouteNodes.  Because of the DAG network creates multiple
	 * links to RouteNodes, the Java garbage collector may not be able to determine
	 * when a RouteNode is free or not.  Because of the frequency of route
	 * creation, "new" and "dispose" could happen a lot; consequently, CATS
	 * will do its own recycling of RouteNodes.  RouteNodePts has its own
	 * pool of nodes, separate from this.
	 */
	static private RouteNodePts FreeListPts = null;
		
	/**
	 * the number of RouteNodePts created (via (new())
	 */
	static private int RouteNodePtsCount = 0;
	
	/**
	 * the possible RouteNodes in advance
	 */
	private RouteNode[] FrogNodes = new RouteNode[Sides.EDGENAME.length];
	
	/**
	 * the index of the frog the points should be lined to
	 */
	private int Alignment = Constants.OutofCorrespondence;
	
	/**
	 * the number of paths (frogs) that lead to the destination signal
	 */
	private int Paths;
	
	/**
	 * block occupancy counter.  Used to identify stable occupancy 
	 */
	private long OccupancyCounter = 0;
	
//	/**
//	 * the states of the occupancy timer
//	 */
//	private enum TURNOUT_STATE {
//		INACTIVE,		// the RouteNodePts is not at the top of stack on a PtsEdge
//		STACK_TOP,		// it is at the top of the stack - the active Route
//		TIMING,			// the unoccupancy timer is running
//		MOVE_PENDING,	// the command to move the points has been sent and awaiting feedback
//		LINED			// the points are lined, so the route can be finalized
//	};
//	
//	/**
//	 * the current value of the turnout control state machine
//	 */
//	TURNOUT_STATE TurnoutState;
	
//	/**
//	 * the timer that stretches out the unoccupancy report to
//	 * reduce a false unoccupancy (which could trigger the RouteNodePts
//	 * to move the points, while a train is on them).
//	 */
//	private TimeoutEvent DelayEvent;
	
//	/**
//	 * the timeout value
//	 */
//	private int SafetyTimeoutValue;
	
	/**
	 * the ctor
	 * @param edge is the PtsEdge associated with the RouteNode
	 * @param search identifies the Route incarnation
	 * @param next  is the next node for the next RouteEdge in advance
	 */
	public RouteNodePts(final RouteEdge edge, final int search, final RouteNode next) {
		super(edge, search, next);
//		DelayEvent = new TimeoutEvent(new SafetyDelay(), SafetyTimeoutValue);
		
	}
	
	@Override
	protected void initializeNode(final RouteEdge edge, final int search, final RouteNode next) {
		super.initializeNode(edge, search, next);
		if (FrogNodes == null) {
			FrogNodes = new RouteNode[Sides.EDGENAME.length];
		}
		else {
			for (int f = 0; f < FrogNodes.length; ++f) {
				FrogNodes[f] = null;
			}
		}
		Alignment = Constants.OutofCorrespondence;
		Paths = 0;
//		TurnoutState = TURNOUT_STATE.INACTIVE;
	}

	/**
	 * prepares the associated PtsNode for moving the points.
	 * it uses the same methods that are invoked when the dispatcher moves the points.
	 * @return true if the command is given and false if not
	 */
	public boolean prepareNode() {
		if ((Edge != null) && !isLined()) {
			if (Edge instanceof OSEdge) {
				if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
					System.out.println("Moving points " + toString());
				}
				((OSEdge) Edge).requestAlignment(Alignment);
			}
			return false;
		}
		return true;
	}

	/**
	 * tells the RouteNodePts how the points should be aligned for the route
	 * @param lined is the index of the frog the points should be lined to
	 */
	public void setAlignment(final int lined) {
		Alignment = lined;
	}
	
	/**
	 * checks to determine if points are lined to the preferred route
	 * @return true if they are and false if not
	 */
	public boolean isLined() {
		return (((PtsEdge) Edge).getAlignment() == Alignment);
	}
	
	/**
	 * converts a frog identity to an index
	 * @param node is the RouteNode of the frog
	 * @return its track number.  -1 is a valid result, meaning
	 * the frog could not be found.
	 */
	public int getFrogIndex(final RouteNode node) {
		if (node != null) {
			for (int f = 0; f < FrogNodes.length; ++f) {
				if (FrogNodes[f] == node) {
					return f;
				}
			}
		}
		return -1;
	}
	
	/**
	 * retrieves the frog node for a particular side.
	 * @param frog is the index.  It must be between 0 and 3.
	 * @return the RouteNode corresponding to the requested side.
	 * It can be null.
	 */
	public RouteNode getFrogNode(final int frog) {
		return FrogNodes[frog];
	}
	
	/**
	 * sets the frog node on a particular path
	 * @param frog is the frog node
	 * @param side is the side of the grid holding the track
	 */
	public void setFrogNode(final RouteNode frog, final int side) {
		FrogNodes[side] = frog;
	}
	
	/**
	 * steps through the frogs, dumping them
	 * @param nodeCount is an incrementing integer for locating parallel paths
	 */
	public void dumpPointsNode(final int nodeCount) {
		for (int node = 0; node < FrogNodes.length; ++node) {
			if (FrogNodes[node] != null) {
				FrogNodes[node].commonTraversal(nodeCount);
			}
		}
	}
	
	/**
	 * counts the number of exits from a facing point node
	 * @return the number of defined frogs
	 */
	public int countPaths() {
		int Paths = 0;
		for (int i =0; i < FrogNodes.length; ++i) {
			if (FrogNodes[i] != null) {
				++Paths;
			}
		}
		return Paths;
	}
	
	/**
	 * gets the number of paths exiting from the points
	 * @return the number of non-null frogs (1, 2, or 3)
	 */
	public int getPaths() {
		return Paths;
	}
	
	/**
	 * visits all the RouteNodes that are frogs between this node and endNode, performing the command on them
	 * @param marker a search identifier
	 * @param endNode the node at which walking ends.  It is not included in the walk.
	 * @param command the operation to perform on the nodes.
	 */
	protected void visitPoints(final int marker, final RouteNode endNode, final VisitorCommand command) {
		RouteNode fNode;
//		if (VisitID != marker) {
//			VisitID = marker;
			for (int node = 0; node < FrogNodes.length; ++node) {
				if (((fNode = FrogNodes[node]) != null) && (fNode.VisitID != marker)) {
					fNode.VisitID = marker;
					if (fNode.NextNode != null) {
						fNode.NextNode.visitCommon(marker, endNode, command);
					}
					command.executeCommand(fNode);
				}
			}
			command.executeCommand(this);
//		}
	}

	/**
	 * the equivalent of calculateBlockages, but for facing points.  It has
	 * to look at the blockages on each alignment and pick the "best"
	 * @param seqNum seqNum is the search number for identifying previous visits
	 */
	protected void calculatePtsBlockages(final int seqNum) {
		// approaching a turnout from the points end.  Thus, the metrics are 
		// path with the fewest obstructions, followed by path with fewest
		// number of fouled turnouts
		int blockages;
		int complexity;
		int divergences;
		int desired;

		desired = Alignment = ((PtsEdge) Edge).getAlignment();
		if (VisitID < seqNum) {
			VisitID = seqNum;
//			Edge.registerStackNode(true);
			AdvanceBlockageCount = 
					ComplexityCount =
					DivergingCount = Integer.MAX_VALUE;
			for (int frog = 0; frog < FrogNodes.length; ++frog) {
				if (FrogNodes[frog] != null) {
					FrogNodes[frog].calculateBlockages(seqNum);
					blockages = FrogNodes[frog].getBlockageCount();
					if (!((PtsEdge) Edge).isRoutable((FrogEdge) FrogNodes[frog].Edge)) {
						++blockages;
					}
					complexity = FrogNodes[frog].getComplexity();
					if (Alignment != frog) {
						complexity = ++FrogNodes[frog].ComplexityCount;
					}
					divergences = FrogNodes[frog].getDivergingCount();
					if (!((PtsEdge) Edge).isNormalFrog((FrogEdge) FrogNodes[frog].Edge)) {
						divergences = ++FrogNodes[frog].DivergingCount;
					}
					if ((blockages < AdvanceBlockageCount) ||
//							((blockages == AdvanceBlockageCount) && (complexity < getComplexity())))	{
							((blockages == AdvanceBlockageCount) && (divergences < getDivergingCount())))	{
						AdvanceBlockageCount = blockages;
						ComplexityCount = complexity;
						DivergingCount = divergences;
						NextNode = FrogNodes[frog];
						desired = frog;
					}
				}
			}
			Alignment = desired;
		}
		if (Crandic.Details.get(DebugBits.ROUTENODEBIT)) {
			System.out.printf("%d Points Node %s: blockages = %d diverges = %d complexity = %d%n", VisitID, toString(), AdvanceBlockageCount, DivergingCount, ComplexityCount);
		}
	}
	
	/**
	 * determines if the occupancy on the Block has changed since the last time looked.
	 * @return false if it has changed and true if it has not
	 */
	public boolean isStable() {
		long lastLook = OccupancyCounter;
		OccupancyCounter = (Edge == null) ? 0 :	Edge.getOccupancyCount();
		return (lastLook == OccupancyCounter);
	}
	
	/**
	 * "allocates" a new RouteNode from the list of recycled RouteNodes.  If the list is
	 * empty one is created through the Java new() operation.
	 * @param edge is the RouteEdge associated with the node
	 * @param search identifies the CTCRoute
	 * @param next is the next node for the next RouteEdge in advance
	 * @return a clean RouteNode
	 */
	static public RouteNodePts newNode(final RouteEdge edge, final int search, final RouteNode next) {
		RouteNodePts node;
		synchronized(Lock) {
			if (FreeListPts == null) {
				++RouteNodePtsCount;
				return new RouteNodePts(edge, search, next);
			}
			node = FreeListPts;
			FreeListPts = (RouteNodePts) FreeListPts.getNextNode();
		}
		node.initializeNode(edge, search, next);
		return node;
	}
	
	@Override
	protected void unLinkNode() {
		RouteNode frog;
		super.unLinkNode();
		for (int node = 0; node < FrogNodes.length; ++node) {
			if ((frog = FrogNodes[node]) != null) {
				frog.removeLinks(this);
				FrogNodes[node] = null;
			}
		}
	}

	@Override
	protected void removeLinks(final RouteNode jetison) {
		super.removeLinks(jetison);
		for (int node = 0; node < FrogNodes.length; ++node) {
			if (jetison == FrogNodes[node]) {
				FrogNodes[node] = null;
			}
		}
	}
	
	@Override
	protected void recycleNode() {
		if (Crandic.Details.get(DebugBits.ROUTENODEBIT)) {
			System.out.println("Recycling " + Edge.toString());
		}
		synchronized(Lock) {
			NextNode = FreeListPts;
			FreeListPts = this;
		}
	}
	
//	/**
//	 * kills the occupancy delay timer
//	 */
//	private void killDelayTimer() {
//		if (TurnoutState.equals(TURNOUT_STATE.TIMING)) {
//			MasterClock.MyClock.cancelTimer(DelayEvent);
//		}		
//	}
	
	/**
	 * gets the number of RouteNodePts that have been created
	 * @return the creation counter
	 */
	static int getNodeCount() {
		return RouteNodePtsCount;
	}
	
	/**
	 * counts the number of RouteNodes on the free list
	 * @return the number of nodes on the free list
	 */
	static int availablePtsNodes() {
		int count = 0;
		for (RouteNode node = FreeListPts; node != null; node = node.NextNode) {
			++count;
		}
		return count;
	}

//	/************************************************************************************
//	 * The next methods, along with TurnoutState constitute the turnout control
//	 * state machine.  These receive and process the stimulus to the state machine,
//	 ************************************************************************************/
//	/**
//	 * tells the RouteNodePts that it is now the active Route, top of stack
//	 */
//	public void activateNode() {
//		if (TurnoutState == TURNOUT_STATE.INACTIVE) {
//			if (((PtsEdge) Edge).getAlignment() == Alignment) {
//				TurnoutState = TURNOUT_STATE.LINED;
//			}
//			else if (((PtsEdge)Edge).isLockSet(GuiLocks.DETECTIONLOCK)) {
//				TurnoutState = TURNOUT_STATE.STACK_TOP;
//			}
//			else {
//				// check for current timeout value, then
//				MasterClock.MyClock.setTimeout(DelayEvent);
//				TurnoutState = TURNOUT_STATE.TIMING;				
//			}
//		}	 // else ignore the stimulus
//	}
//	
//	/**
//	 * tells the RouteNodePts that the points are occupied; therefore, the 
//	 * points should not be moved.  This is needed to cancel the timer.  It
//	 * may not be the last blocking factor on moving the points, either.
//	 */
//	public void setOccupancy() {
//		switch (TurnoutState) {
//		case TIMING:
//			MasterClock.MyClock.cancelTimer(DelayEvent);
//			//$FALL-THROUGH$
//		case MOVE_PENDING:
//			TurnoutState = TURNOUT_STATE.STACK_TOP;
//			break;
//		case LINED:
//			TurnoutState = TURNOUT_STATE.INACTIVE;
//			break;
//		default:
//			// do nothing
//		}
//	}
//	
//	/**
//	 * tells the RouteNodePts that the points are unoccupied; therefore,
//	 * the points can be moved (after the timer expires, if needed).
//	 */
//	public void clearOccupancy() {
//		if (Edge != null) {
//			if ((((PtsEdge) Edge).getAlignment() != Alignment) && (Edge instanceof OSEdge) &&
//					!(TurnoutState.equals(TURNOUT_STATE.TIMING))) {
//				// this is the place to query for the timeout value
//				// DelayEvent.Count = timeout;
//				MasterClock.MyClock.setTimeout(DelayEvent);
//				TurnoutState = TURNOUT_STATE.TIMING;
//			}
//		}
//	}
//	/************************************************************************************
//	 * A class to wait after the PtsEdge goes unoccupied before trying to move the points.
//	 * This is to reduce the possibility of moving the points under a train.
//	 ************************************************************************************/
//	public class SafetyDelay implements TimeoutObserver {
//		
//		@Override
//		public void acceptTimeout() {
//			if (TurnoutState.equals(TURNOUT_STATE.TIMING)) {
//				if (((OSEdge) Edge).requestAlignment(Alignment)) {
//					TurnoutState = TURNOUT_STATE.MOVE_PENDING;
//				}
//				else {	// do it again
//					MasterClock.MyClock.setTimeout(DelayEvent);
//				}
//			}  // else ignore the timeout
//		}
//	}
}
/* @(#)RouteNodePts.java */
