/* Name: RouteCreator.java
 *
 * What:
 *   This file contains a class that creates eXtended Routes.  An eXtended route is a sequence
 *   of one or more Control Points, cleared for a train to pass through the protected track.
 *   The protected track begins immediately in advance of a signal and continues up to
 *   the next control point in the same direction of travel.  It also includes any turnouts
 *   between the two signals.  Mathematically, an eXtended Route forms a Directed Acyclic
 *   Graph (DAG).  However, there can be one of three possible results of searching for
 *   a control point when starting at a different control point:
 *   <ol>
 *   <li>they are not connected ("you can't get there from here"), in which case, no graph is built
 *   </li>
 *   <li>there is a path from the starting control point to the destination control point
 *   </li>
 *   <li> there are two paths from the starting control point to the destination because the
 *   destination can be reached from two directions.  This happens if the destination is in
 *   a reversing loop.
 *   </li>
 *   </ol>
 *   <p>
 *   Since the GUI prohibits two or more eXtended Routes to be created simultaneously,
 *   the creator can be a programming Singleton.
 *   </p>
 *   <p>
 *   The process of creating an eXtended route begins at an entry signal.  The creator walks the tracks
 *   in advance of the signal until it finds the requested destination signal.  To complicate
 *   things, the destination signal can be facing either direction - the walk may encounter it
 *   from the signal/s advance (opposing) direction or from the signal's approach direction.
 *   If the former, then the opposing signal is included in the route, as the route continues
 *   to the next signal the train would obey.  If the destination signal is in the same
 *   direction of travel, then all the tracks in approach of the signal are included in the
 *   eXtended Route, but not the signal, itself.  The first case is classic entry/exit behavior.
 *   The second is more like "route stacking".
 *   </p>
 *   <p>
 *   There are multiple ways that a route may behave:
 *   <ul>
 *   <li>eNtry/eXit: the route is created immediately.  The path between signals may involve
 *   moving points, but all existing traffic between signals is assumed to remain stopped.
 *   Creation fails if a path cannot be found.
 *   </li>
 *   <li>route stacking: a route is found and the route can include tracks currently
 *   occupied.  When the blockage (occupancy) is removed, the route will be created at
 *   the control point level.  Furthermore, previous routes may have been created and are waiting
 *   for tracks to clear up.  Later routes will be "stacked" until the previous routes are
 *   satisfied.
 *   </li>
 *   <li>automatic movement: the exact route is not known between the two signals.  Rather,
 *   a route is discovered in pieces as the train moves.  This needs to be designed.
 *   </li>
 *   </ul>
 *   </p>
 *   Potentially, searching can take some time, especially for the case of "you can't get there
 *   from here", as every track will be traversed possibly multiple times.  To reduce the search
 *   tree, every RouteEdge will be marked when visited with a search generation number and status.
 *   The search generation number is incremented every time a request is made to search for an
 *   eXtended Route.  A candidate Route Edge is checked to see if it has been visited before
 *   (i.e. the edge's search number is compared against the current generation number and if they
 *   match, the edge has been visited in this generation).  This differs from a more traditional
 *   "mark/sweep" algorithm as there is no need to go back and unmark an edge in preparation for the
 *   next search generation.  When a dead end is reached, the dead end edge status will become DEADEND
 *   and the search will be rolled back to the previous PtsEdge (in the advance direction) with
 *   unvisited frogs (paths).  All edges backed over, will also be changed to status DEADEND (except for
 *   PtsEdges), so that if encountered on another path, they will not be revisited.  If the requested
 *   CPEdge is found, its approach edge will be marked as SUCCESS and the search will also be rolled back,
 *   with edges marked as SUCCESS, so if encountered on another search, they will not be revisited.
 *   </p>
 *   <p>
 *   Loops cause problems, possibly yielding infinite searches.  The search generation number helps
 *   identify loops.  An oval of track is a loop from a CPEdge to itself.  A return loop (balloon
 *   loop) is also a loop.  Note that with the exception of an oval,  only PtsEdges need to be tested
 *   for a prior visit.  The control structure will explicitly look for the oval case.
 *   </p>
 */
package cats.layout.ctc;

import java.util.ArrayList;

import cats.apps.Crandic;
import cats.common.DebugBits;
import cats.layout.items.CPEdge;
import cats.layout.items.FrogEdge;
import cats.layout.items.GuiLocks;
import cats.layout.items.PtsEdge;
import cats.layout.items.RouteEdge;
import cats.layout.items.RouteEdge.RouteSearchStatus;

/**
 *   This file contains a class that creates eXtended Routes.  An eXtended route is a sequence
 *   of one or more Control Points, cleared for a train to pass through the protected track.
 *   The protected track begins immediately in advance of a signal and continues up to
 *   the next control point in the same direction of travel.  It also includes any turnouts
 *   between the two signals.  Mathematically, an eXtended Route forms a Directed Acyclic
 *   Graph (DAG).  However, there can be one of three possible results of searching for
 *   a control point when starting at a different control point:
 *   <ol>
 *   <li>they are not connected ("you can't get there from here"), in which case, no graph is built
 *   </li>
 *   <li>there is a path from the starting control point to the destination control point
 *   </li>
 *   <li> there are two paths from the starting control point to the destination because the
 *   destination can be reached from two directions.  This happens if the destination is in
 *   a reversing loop.
 *   </li>
 *   </ol>
 *   <p>
 *   Since the GUI prohibits two or more eXtended Routes to be created simultaneously,
 *   the creator can be a programming Singleton.
 *   </p>
 *   <p>
 *   The process of creating an eXtended route begins at an entry signal.  The creator walks the tracks
 *   in advance of the signal until it finds the requested destination signal.  To complicate
 *   things, the destination signal can be facing either direction - the walk may encounter it
 *   from the signal/s advance (opposing) direction or from the signal's approach direction.
 *   If the former, then the opposing signal is included in the route, as the route continues
 *   to the next signal the train would obey.  If the destination signal is in the same
 *   direction of travel, then all the tracks in approach of the signal are included in the
 *   eXtended Route, but not the signal, itself.  The first case is classic entry/exit behavior.
 *   The second is more like "route stacking".
 *   </p>
 *   <p>
 *   There are multiple ways that a route may behave:
 *   <ul>
 *   <li>eNtry/eXit: the route is created immediately.  The path between signals may involve
 *   moving points, but all existing traffic between signals is assumed to remain stopped.
 *   Creation fails if a path cannot be found.
 *   </li>
 *   <li>route stacking: a route is found and the route can include tracks currently
 *   occupied.  When the blockage (occupancy) is removed, the route will be created at
 *   the control point level.  Furthermore, previous routes may have been created and are waiting
 *   for tracks to clear up.  Later routes will be "stacked" until the previous routes are
 *   satisfied.
 *   </li>
 *   <li>automatic movement: the exact route is not known between the two signals.  Rather,
 *   a route is discovered in pieces as the train moves.  This needs to be designed.
 *   </li>
 *   </ul>
 *   </p>
 *   <p>
 *   Potentially, searching can take some time, especially for the case of "you can't get there
 *   from here", as every track will be traversed possibly multiple times.  To reduce the search
 *   tree, every RouteEdge will be marked when visited with a search generation number and status.
 *   The search generation number is incremented every time a request is made to search for an
 *   eXtended Route.  A candidate Route Edge is checked to see if it has been visited before
 *   (i.e. the edge's search number is compared against the current generation number and if they
 *   match, the edge has been visited in this generation).  This differs from a more traditional
 *   "mark/sweep" algorithm as there is no need to go back and unmark an edge in preparation for the
 *   next search generation.  When a dead end is reached, the dead end edge status will become DEADEND
 *   and the search will be rolled back to the previous PtsEdge (in the advance direction) with
 *   unvisited frogs (paths).  All edges backed over, will also be changed to status DEADEND (except for
 *   PtsEdges), so that if encountered on another path, they will not be revisited.  If the requested
 *   CPEdge is found, its approach edge will be marked as SUCCESS and the search will also be rolled back,
 *   with edges marked as SUCCESS, so if encountered on another search, they will not be revisited.
 *   </p>
 *   <p>
 *   Loops cause problems, possibly yielding infinite searches.  The search generation number helps
 *   identify loops.  An oval of track is a loop from a CPEdge to itself.  A return loop (balloon
 *   loop) is also a loop.  Note that with the exception of an oval,  only PtsEdges need to be tested
 *   for a prior visit.  The control structure will explicitly look for the oval case.
 *   </p>
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class RouteCreator {
	/**
	 * the Singleton  DAGs are created sequentially - not in parallel -
	 * so one must finish before the next is begun.
	 */
	static private RouteCreator Singleton;
	
	/**
	 * the maximum depth of the DAG before calling it failed
	 */
	static private final int Depth = 100;

	/**
	 * the RouteEdges traversed in searching for the destination.  It may increase
	 * in size, as needed, but won't shrink.
	 */
	private ArrayList<RouteEdge> Nodes = new ArrayList(1);

	/**
	 * the first empty element in the Node list - where the next Node will be placed
	 */
	private int NextNodeIndex;

	/**
	 * is the last RouteEdge traversed
	 */
	private RouteEdge LastEdge;

	/**
	 * a counter for uniquely identifying each search
	 */
	private int SearchNumber = 0;

	/**
	 * the first RouteNode in the DAG or quick route - the head
	 */
	private RouteNode HeadNode;

//	/**
//	 * the last RouteNode in the quick route
//	 */
//	private RouteNode LastNode;
	
	/**
	 * the maximum number of exit CPNodes before the search
	 * ends.  This is to reduce the search time at the risk
	 * of missing the destination.
	 */
	private int Span;

	/**
	 * the ctor
	 */
	private RouteCreator() {		
	}

	/**
	 * the Singleton enforcer
	 * @return the RouteCreator Singleton
	 */
	static public RouteCreator getRouteFactory() {
		if (Singleton == null) {
			Singleton = new RouteCreator();
		}
		return Singleton;
	}

	/**
	 * initializes the data structures for a search
	 * @param entryEdge is the CPEdge the search begins at
	 * concluding the destination cannot be reached
	 */
	private void initializeRoute(final CPEdge entryEdge) {
		++SearchNumber;
		LastEdge = entryEdge;
		NextNodeIndex = 0;
		HeadNode = null;
//		LastNode = null;
	}

	/**
	 * rolls back the search to the last facing PtsEdge with a leg not traversed.
	 * The top RouteEdge (variable previousEdge) unstacked can be
	 * 1. a CPEdge
	 * 2. a PtsEdge (entered from the frog side - trailing point points)
	 * 3. a FrogEdge (entered from the points side - facing point points)
	 * 
	 * This method is called as a result of three possible cases:
	 * 1. the destination is in the advance tracks from the top unstacked RouteEdge
	 * 2. end of track is found in the advance track (there is no successor RouteEdge)
	 * 3. the top unstacked RouteEdge has been visited before.  In this case, the RouteEdge
	 * must be a PtsEdge.  The result of this leg of the search is the status of the PtsEdge,
	 * unless the status is SEARCHING, in which case the PtsEdge is the wye for a reversing
	 * loop and the status is changed to DEADEND.
	 * 
	 * @param searchResult is the result of searching thus far
	 * @return a FrogEdge that has not been traversed or the starting CPEdge or null
	 */
	private RouteEdge rollBack(RouteSearchStatus searchResult) {
		FrogEdge[] frogs;
		RouteEdge previousEdge;
		PtsEdge pEdge;		// a PtsEdge traversed
		FrogEdge fEdge;		// a FrogEdge traversed
		RouteNodePts pNode;	// the RouteNode attached to pEdge
		RouteNode fNode;	// the RouteNode attached to fEdge
		int complexity = 0;				// the minimum number of turnouts that have to be moved
		int divergences = 0;			// the minimum number of diverging routes taken
		int advanceBlockages = 0;  		// the number of blockages in advance routes
		boolean branchBlockage = false; // true if this route segment is blocked

		do {
			previousEdge = Nodes.get(--NextNodeIndex);
			if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
				System.out.println("Rolling back edge " + previousEdge.toString() + " with " + searchResult.toString());
			}
			if (previousEdge instanceof PtsEdge) {  //must be trailing PtsEdge
				pEdge = (PtsEdge) previousEdge;
				if ((pNode = (RouteNodePts) pEdge.getNode()) != null) {  // being revisited - start of rollback
					HeadNode = pNode;
					complexity	= HeadNode.getComplexity();
					divergences = HeadNode.getDivergingCount();
					advanceBlockages = HeadNode.getBlockageCount();
					searchResult = pEdge.getSearchStatus();
				}
				else if (RouteSearchStatus.SUCCESS.equals(searchResult)) {  // add only SUCCESS nodes to DAG
					pNode = (RouteNodePts) (HeadNode = previousEdge.createNode(HeadNode, SearchNumber));
					HeadNode.setComplexity(complexity);
					HeadNode.setDivergingCount(divergences);
					if (branchBlockage || pEdge.isRouteSegmentBlocked(GuiLocks.ExtendedRouteBlock)) {
						++advanceBlockages;
						if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
							System.out.println("blocked points: " + pEdge.toString() + " branchBlockage=" + branchBlockage);
						}
					}
					HeadNode.setBlockageCount(advanceBlockages);
				}
				branchBlockage = false;
				pEdge.setSearchStatus(searchResult);

				previousEdge = Nodes.get(--NextNodeIndex);	// previousEdge is the FrogEdge
				previousEdge.setSearchStatus(searchResult);				
				if (searchResult.equals(RouteSearchStatus.SUCCESS)) {
					HeadNode = previousEdge.createNode(HeadNode, SearchNumber);
					if (((FrogEdge) previousEdge).isFoulingEdge()) {
						++complexity;
					}
					HeadNode.setComplexity(complexity);
					if (!pEdge.isNormalFrog((FrogEdge) previousEdge)) {
						++divergences;
					}
					HeadNode.setDivergingCount(divergences);
					if (!pEdge.isRoutable((FrogEdge) previousEdge)) {
						++advanceBlockages;
					}
					HeadNode.setBlockageCount(advanceBlockages);
					if (pNode != null) {
						pNode.setFrogNode(HeadNode, ((FrogEdge) previousEdge).getIndex());
					}
				}
			}
			else if (previousEdge instanceof FrogEdge) {  // facing PtsEdge - this is the case to resume searching
				fEdge = (FrogEdge) previousEdge;
				previousEdge = Nodes.get(--NextNodeIndex);	// previousEdge is the corresponding PtsEdge
				pEdge = (PtsEdge) previousEdge;
				if (RouteSearchStatus.SUCCESS.equals(searchResult)) {
					HeadNode = fEdge.createNode(HeadNode, SearchNumber);
					fNode = HeadNode;
					if (fEdge.isFoulingEdge()) {
						++complexity;
					}
					fNode.setComplexity(complexity);
					if (!pEdge.isNormalFrog(fEdge)) {
						++divergences;
					}
					fNode.setDivergingCount(divergences);
					if (branchBlockage || fEdge.isRouteSegmentBlocked(GuiLocks.ExtendedRouteBlock)) {
						++advanceBlockages;
						if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
							System.out.println("blocked frog: " + fEdge.toString());
						}
					}
					branchBlockage = false;
					fNode.setBlockageCount(advanceBlockages);
						// focus shifts to points
					pEdge.setSearchStatus(RouteSearchStatus.SUCCESS);
					HeadNode = pEdge.createNode(HeadNode, SearchNumber);
					pNode = (RouteNodePts) HeadNode;
					pNode.setFrogNode(fNode, fEdge.getIndex());
					if ((advanceBlockages < pNode.getBlockageCount()) || 
//							((advanceBlockages == pNode.getBlockageCount()) && (complexity < pNode.getComplexity()))) {
							((advanceBlockages == pNode.getBlockageCount()) && (divergences < pNode.getDivergingCount()))) {
						pNode.setBlockageCount(advanceBlockages);
						pNode.setDivergingCount(divergences);
						pNode.setComplexity(complexity);
						pNode.setNextNode(fNode);
						pNode.setAlignment(fEdge.getIndex());
					}
					else  {
						advanceBlockages = pNode.getBlockageCount();
						complexity = pNode.getComplexity();
						divergences = pNode.getDivergingCount();
					}
				}
				else {  // though this frog route is not SUCCESS, another may be
					HeadNode = pEdge.getNode();
					if (HeadNode != null) {
						complexity = HeadNode.getComplexity();
						divergences = HeadNode.getDivergingCount();
						advanceBlockages = HeadNode.getBlockageCount();
					}
				}
				if (pEdge.getSearchStatus().equals(RouteSearchStatus.SUCCESS)) {
					searchResult = RouteSearchStatus.SUCCESS; 
				}
				else {
					pEdge.setSearchStatus(searchResult = RouteSearchStatus.DEADEND);
				}
				if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
					System.out.println("Frog " + fEdge.toString() + " Side " + fEdge.getIndex());
				}
				frogs = pEdge.getFrogs();
				for (int i = 0; i < frogs.length; ++i) {
					if ((frogs[i] != null) && (frogs[i].getSearchID() != SearchNumber)
							&& pEdge.isRoutable(frogs[i])) {
						// resume searching
						HeadNode = null;
						++NextNodeIndex;
						return frogs[i];  // this is the only place that searching resumes from
					}
				}
			}
			else { // Control Point
				if (RouteSearchStatus.SUCCESS.equals(searchResult)) {
					HeadNode = previousEdge.createNode(HeadNode, SearchNumber);
					HeadNode.setComplexity(complexity);
					HeadNode.setDivergingCount(divergences);
					if (previousEdge.isRouteSegmentBlocked(GuiLocks.ExtendedRouteBlock)) {
						branchBlockage = true;
						++advanceBlockages;
					}
					HeadNode.setBlockageCount(advanceBlockages);
				}
				previousEdge.setSearchStatus(searchResult);
				--Span;
			}
		} while (NextNodeIndex > 0);
		return Nodes.get(0);  // done searching - this is the only time a non-FrogEdge is returned
	}

	/**
	 * adds a new RouteEdge to the route path.  NextNodeIndex is the index of the first open
	 * spot.
	 * @param edge is the edge being added
	 */
	private void addRouteEdge(final RouteEdge edge) {
		if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
			System.out.println("Stacking Route Edge " + edge.toString());
		}
		if (NextNodeIndex >= Nodes.size()) {
			Nodes.add(LastEdge);
			NextNodeIndex = Nodes.size();
		}
		else {
			Nodes.set(NextNodeIndex++, LastEdge);
		}
	}

	/**
	 * a method to search for a requested CPEdge, beginning from a specific
	 * CPEdge.  The search algorithm performs multiple probes through turnouts.
	 * When approaching a turnout through the frog end, it ignores the alignment
	 * of the points, if the dispatcher can move the points.  If he can't, then
	 * the search ends with DEADEND.  When approaching a turnout from the points end, the search
	 * can take all legs of the turnout.
	 * 
	 * To make the search a little faster, each Route segment is tagged.  When a tagged
	 * segment is found, it (and the rest) are added to the path, which will result
	 * in a DAG, rather than directed graph.  So, there may be multiple ways to get
	 * from the start to the destination.
	 * 
	 * There are two important points:
	 * 1. when a DEADEND or SUCCESS is reached, rollback is invoked to look for other paths
	 * 2. the return from rollback will be either the starting CPEdge (indicating searching is complete)
	 * or a FrogEdge that is an exit from facing points - initiation of another probe on a parallel path
	 * @param entryEdge is the starting edge for the search
	 * @param exitEdge is the requested destination edge
	 * @return the path of RouteEdges to get from start to destination,
	 * which will be null if not found
	 */
	public RouteNode DAGRouteSearch (final CPEdge entryEdge, final CPEdge exitEdge) {
		initializeRoute(entryEdge);
		do {
			// LastEdge is either CPEdge, facing points PtsEdge, or trailing points FrogEdge
			addRouteEdge(LastEdge);	// to pick up status
			if (LastEdge.getSearchID() == SearchNumber) {  // Can only happen on trailing points PtsEdge
					// or back to entry edge, so roll back to frog.
				if (Crandic.Details.get(DebugBits.CTCCREATIONBIT)) {
					System.out.println("revisiting " + LastEdge.toString() + " status=" + LastEdge.getSearchStatus());
				}
				if (LastEdge instanceof PtsEdge) {
					LastEdge = rollBack((LastEdge.getSearchStatus() == RouteSearchStatus.SEARCHING) ? 
							RouteSearchStatus.DEADEND : LastEdge.getSearchStatus());  // reverse loop check
				}
				else {
					LastEdge = rollBack(LastEdge.getSearchStatus());
				}
			}
			else {
				// searching for:
				// 1. tracks in advance of CPEdge
				// 2. tracks in advance of FrogEdge (facing point points)
				// 3. tracks in approach to PtsEdge (trailing point points)
				if ((LastEdge = LastEdge.startSearch(exitEdge, SearchNumber)) == null) {
					// DEADEND or SUCCESS
					LastEdge = rollBack(Nodes.get(NextNodeIndex - 1).getSearchStatus());
				}
				else {
					if (LastEdge instanceof CPEdge) {
						if ((++Span) >= Depth){
							// exceeded search range - this is a safety net
							LastEdge = rollBack(RouteSearchStatus.DEADEND);
						}
					} 
					else if (LastEdge instanceof PtsEdge) {  // facing PtsEdge, so search from frogs
						addRouteEdge(LastEdge);				// pushes PtsEdge
						if ((LastEdge = ((PtsEdge) LastEdge).startFrogSearch()) == null) {
							--NextNodeIndex;	// removes the PtsEdge because there is no matching FrogEdge
							LastEdge = rollBack(RouteSearchStatus.DEADEND);
						}
					}
					else if (LastEdge instanceof FrogEdge) { // trailing FrogEdge, so search from points
						addRouteEdge(LastEdge);			// pushes FrogEdge
						if ((LastEdge = ((FrogEdge) LastEdge).startPointsSearch()) == null) {
							--NextNodeIndex;	// removes the FrogEdge because there is no matching PtsEdge
							LastEdge = rollBack(RouteSearchStatus.DEADEND);							
						}
						else {
							((PtsEdge) LastEdge).resetSearchFlag();
						}
					}
				}
			}
		} while (NextNodeIndex > 0);
		if (RouteSearchStatus.SUCCESS == entryEdge.getSearchStatus()) {
			HeadNode.setExit(exitEdge);
			return HeadNode;
		}
		return null;				
	}
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(RouteCreator.class.getName());
}
/* @(#)RouteCreator.java */
