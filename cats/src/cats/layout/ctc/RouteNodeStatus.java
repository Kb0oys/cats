/* Name: RouteNodeStatus.java
 *
 * What:
 *   This enum defines the status of RouteNode.  It can be
 *   <ul>
 *   <li>UNTOUCHED - at creation
 *   <li>VISITED - by the Enumerator, when it is in the Extended Route under construction</li>
 *   <li>REJECTED - by the Enumeration, when the Extended Route creator rejects it</li>
 *   <li>PENDING - stacked, waiting for the node to handle earlier events<li>
 *   <li>ACTIVE - being operated on by the node</li>
 *   <li>COMPLETE - a train has passed through the node, so the node will be destroyed
 *   when the last node in the route is complete.
 *   </ul>
 */
package cats.layout.ctc;

/**
 *   This enum defines the status of RouteNode.  It can be
 *   <ul>
 *   <li>UNTOUCHED - at creation
 *   <li>VISITED - by the Enumerator, when it is in the Extended Route under construction</li>
 *   <li>REJECTED - by the Enumeration, when the Extended Route creator rejects it</li>
 *   <li>PENDING - stacked, waiting for the node to handle earlier events<li>
 *   <li>ACTIVE - being operated on by the node</li>
 *   <li>COMPLETE - a train has passed through the node, so the node will be destroyed
 *   when the last node in the route is complete.
 *   </ul>
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public enum RouteNodeStatus {
	UNTOUCHED,	// freshly created, ready for use
	VISITED,	// node is in the Extended Route being constructed
	REJECTED,	// node cannot be included in the Extended Route
	PENDING,	// node is queued, waiting for prior events to complete
	STACKED,	// the node is in the Route Stack
	ACTIVE,		// the event being worked by the node
	COMPLETE	// a train has passed through the node and the route is waiting for completion on the last node
}
/* @(#)RouteNodeStatus.java */
