/* Name: TrackEdgeCounter.java
 *
 * What:
 * <p>
 * This class is a Singleton that maintains a count of the number of TrackEdges which
 * are involved in stacked routes.  The number of edges is used to determine the
 * size of the BitSet used for testing which Edges are included in a specific stacked route.
 * </p>
 * <p>
 * Furthermore, as each edge is created, the edge asks for the current size, which serves
 * as an index into the BitSet for the edge.  The size is then incremented by one.
 * </p>
 */
package cats.layout.ctc;

/**
 * <p>
 * This class is a Singleton that maintains a count of the number of TrackEdges which
 * are involved in stacked routes.  The number of edges is used to determine the
 * size of the BitSet used for testing which Edges are included in a specific stacked route.
 * </p>
 * <p>
 * Furthermore, as each edge is created, the edge asks for the current size, which serves
 * as an index into the BitSet for the edge.  The size is then incremented by one.
 * </p>
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2023</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class TrackEdgeCounter {

	/**
	 * the count of TrackEdges
	 */
	private static int TotalEdges = 0;
	
	/**
	 * returns the ordinal number of the requesting edge and then increments it
	 * @return the count of edges (ordinal number) prior to the method call
	 */
	static public int getEdgeOrdinal() {
		return TotalEdges++;
	}
	
	/**
	 * returns the count of number of TrackEdges that could be included in a stacked route
	 * @return the number of TrackEdges defined in the layout
	 */
	static public int getEdgeCount() {
		return TotalEdges;
	}
}
/* @(#)TrackEdgeCounter.java */

