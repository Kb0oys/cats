/* Name: RouteEdge.java
 *
 * What:
 *   A RouteNode is one step in a stored Route.  It is associated with
 *   <ul>
 *   <li> a CPEdge</li>
 *   <li> a PtsEdge</li>
 *   <li> a FrogEdge</li>
 *   </ul>
 */
package cats.layout.ctc;

import java.util.BitSet;

import cats.apps.Crandic;
import cats.common.DebugBits;
import cats.layout.items.CPEdge;
import cats.layout.items.FrogEdge;
import cats.layout.items.GuiLocks;
import cats.layout.items.PtsEdge;
import cats.layout.items.RouteEdge;

/**
 *   A RouteNode is one step in a stored Route.  It is associated with
 *   <ul>
 *   <li> a CPEdge</li>
 *   <li> a PtsEdge</li>
 *   <li> a FrogEdge</li>
 *   </ul>
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2021, 2022</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class RouteNode {
	
	/**
	 * the DAG traversal generation number that uniquely identifies each time
	 * a traversal has been started.
	 */
	static private int VisitGeneration = 1;
	
	/**
	 * the link to free RouteNodes.  Because of the DAG network creates multiple
	 * links to RouteNodes, the Java garbage collector may not be able to determine
	 * when a RouteNode is free or not.  Because of the frequency of route
	 * creation, "new" and "dispose" could happen a lot; consequently, CATS
	 * will do its own recycling of RouteNodes.  RouteNodePts has its own
	 * pool of nodes, separate from this.
	 */
	static private RouteNode FreeList = null;
	
	/**
	 * is the number of RouteNodes created (via new()).
	 */
	static private int NodeCount = 0;
	
	/**
	 * an object that is locked (via synchronization) to access FreeList
	 */
	static final protected Boolean Lock = true;
	
	/**
	 * the current traversal generation number for recognizing earlier visits.
	 * It is used after the DAG is generated for optimized traversal. Any node with
	 * that number (or higher) has already been visited.
	 */
	protected int VisitID;
	
	/**
	 * the status of the node
	 */
	private RouteNodeStatus NodeStatus;
	
	/**
	 * the unique route creation number, as supplied by RouteCreator
	 */
	private int RouteNumber;
	
	/**
	 * the edge associated with the node
	 */
	protected RouteEdge Edge;
	
	/**
	 * the next node in the DAG.  Traversal is via a doubly linked list.
	 */
	protected RouteNode NextNode;
	
	/**
	 * the previous node in the DAG.  It is set during
	 * traversal.
	 */
	private RouteNode PreviousNode;
	
	/**
	 * the control point containing this RouteNode.  It is needed
	 * so that enclosed blocks and points can report on state
	 * changes.  For a CPEdge, the value is a link to the next control
	 * point node in the route.
	 */
	protected RouteNode ControlNode;
	
	/**
	 * the RouteNode attached to the Control Point that anchors
	 * the Extended Route
	 */
	protected RouteNode RouteAnchor;
	
	/**
	 * a simple estimate of the complexity of the route from
	 * entry to route exit. It simply counts the minimum number of
	 * turnouts that are fouled - that have to be moved to
	 * traverse to the destination.  It initializes to the
	 * Java MAX_VALUE, which should be much larger than the number of
	 * turnouts in the largest layouts.
	 */
	protected int ComplexityCount;
	
	/**
	 * a count of the minimum number of diverging routes that are
	 * traversed to get from here to the destination.  It is a crude
	 * way of measuring "shortest path".  It initializes to the
	 * Java MAX_VALUE, which should be much larger than the number of
	 * turnouts in the largest layouts.
	 */
	protected int DivergingCount;

	/**
	 * a count of the number of route segments in advance of a RouteNode
	 * that contain conditions that would prevent creating a route.  Note
	 * that CONFLICTINGSIGNALLOCK is not a condition because searching for a
	 * route considers all paths through points, not just the current alignment.
	 */
	protected int AdvanceBlockageCount;
	
	/**
	 * a flag indicating that there is a blockage between a frog or points
	 * edge and the next frog or points edge in advance.  The flags are accumulated
	 * in the AdvanceBlockageCount.
	 */
	protected boolean BranchBlockageFlag;
	
//	/**
//	 * a flag indicating that the associated RouteEdge is entered from the approach
//	 * direction - that a train following the route sees the RouteEdge.  True means
//	 * entry and false means the RouteEdge is an exit edge.
//	 */
//	protected boolean EntryFlag;
//	
	/**
	 * for the RouteAnchor only, this is the CPEdge the dispatcher asked to be the exit
	 */
	private CPEdge Destination;
	
	/**
	 * the ctor
	 * @param edge is the RouteEdge associated with the node
	 * @param search identifies the CTCRoute
	 * @param next is the next node for the next RouteEdge in advance
	 */
	protected RouteNode(final RouteEdge edge, final int search, final RouteNode next) {
		this.initializeNode(edge, search, next);
	}
	
	/**
	 * initializes the key fields in a RouteNode for use
	 * @param edge is the RouteEdge associated with the node
	 * @param search identifies the CTCRoute
	 * @param next is the next node for the next RouteEdge in advance
	 */
	protected void initializeNode(final RouteEdge edge, final int search, final RouteNode next) {
		Edge = edge;
		RouteNumber = search;
		NodeStatus = RouteNodeStatus.UNTOUCHED;
		NextNode = next;
		PreviousNode = null;
		ControlNode = null;
		RouteAnchor = null;
		ComplexityCount = Integer.MAX_VALUE;
		DivergingCount = AdvanceBlockageCount = ComplexityCount;
		VisitID = 0;
		Destination = null;
	}
	
	/**
	 * sets the status of the node
	 * @param status the new status
	 */
	public void setNodeStatus(final RouteNodeStatus status) {
		NodeStatus = status;
	}
	
	/**
	 * retrieves the node's status
	 * @return the node's status
	 */
	public RouteNodeStatus getNodeStatus() {
		return NodeStatus;
	}
	
	/**
	 * retrieves the node's route number
	 * @return the route number
	 */
	public int getRouteNumber() {
		return RouteNumber;
	}
	
	/**
	 * retrieves the RouteEdge the node is attached to
	 * @return the RouteEdge
	 */
	public RouteEdge getRouteEdge() {
		return Edge;
	}
	
	/**
	 * sets the link to the next RouteNode.  Typically, the
	 * next node is known at node creation time and is
	 * immutable, because nodes are created on rollback,
	 * after a successful path is found.  One exception is
	 * the anchor (origin node) because its next is not known
	 * until the full DAG has been constructed.  The "next"
	 * node for points is not a single node, but one for
	 * each defined route, though it could be the frog
	 * node of the currently lined route.
	 * @param next is the next RouteNode, which could be
	 * null.
	 */
	public void setNextNode(final RouteNode next) {
		NextNode = next;
	}
	
	/**
	 * retrieves the next node in the DAG.
	 * @return the next node from source to destination
	 */
	public RouteNode getNextNode() {
		return NextNode;
	}
	
	/**
	 * sets the previous node - one whose next is this node.
	 * It is used by the Enumeration for back tracking.  It is
	 * set by the Enumeration.
	 * @param previous the first node between this node and the root
	 */
	public void setPreviousNode(final RouteNode previous) {
		PreviousNode = previous;
	}
	
	/**
	 * returns the previous node in the DAG - the one visited just prior
	 * to reaching this node.
	 * @return the previous node in the DAG
	 */
	public RouteNode getPreviousNode() {
		return PreviousNode;
	}
	
	/**
	 * remembers the controlling CP Node
	 * @param control is the controlling CP node
	 */
	public void setControlNode(final RouteNode control) {
		ControlNode = control;
	}
	
	/**
	 * retrieves the controlling node (link to CPEDGE)
	 * @return the RouteNode attached to the Control Point
	 * or the next Control Point
	 */
	public RouteNode getControlNode() {
		return ControlNode;
	}
	
	/**
	 * remembers the RouteNoe that anchors an Extended Route
	 * @param anchor is the anchor CP node
	 */
	public void setAnchorNode(final RouteNode anchor) {
		RouteAnchor = anchor;
	}
	
	/**
	 * retrieves the RouteNode attached to the Extended route anchor
	 * @return the anchor RouteNode, which should
	 * never be null.
	 */
	public RouteNode getAnchorNode() {
		return RouteAnchor;
	}
	
	/**
	 * sets the complexity of the node in advance
	 * @param advanceComplexity is a metric of how
	 * straight forward the route in advance is
	 */
	public void setComplexity(final int advanceComplexity) {
		ComplexityCount = advanceComplexity;
	}
	
	/**
	 * returns the accumulated complexity through the Nodes in advance
	 * @return the complexity
	 */
	public int getComplexity() {
		return ComplexityCount;
	}
	
	/**
	 * sets the minimum number of diverging paths thru
	 * turnouts from here to the destination.
	 * @param diverging is the count
	 */
	public void setDivergingCount(final int diverging) {
		DivergingCount = diverging;
	}
	
	/**
	 * returns the minimum number of diverging
	 * paths through turnouts
	 * @return the count of diverging paths
	 */
	public int getDivergingCount() {
		return DivergingCount;
	}

	/**
	 * sets the number of blockage segments in advance
	 * @param blockage is the minimum number of segments
	 * in advance containing blockages
	 */
	public void setBlockageCount(final int blockage) {
		AdvanceBlockageCount = blockage;
	}
	
	/**
	 * returns the minimum number of blockages in segments
	 * in advance.
	 * @return the minimum number of segments in advance containing
	 * blockages
	 */
	public int getBlockageCount() {
		return AdvanceBlockageCount;
	}
	
	/**
	 * marks a blockage between the RouteNode and the next frog/points
	 * in advance.  There is no equivalent "clear" operation because
	 * the RouteNode is initialized as false and can only become true
	 * before being initialized again.
	 */
	public void setBlockageFlag() {
		BranchBlockageFlag = true;
	}
	
	/**
	 * returns a flag indicating that there is a blockage between this
	 * node and the next frog/points edge in advance.
	 * @return true if there is blockage between this node and the next
	 * in advance
	 */
	public boolean getBlockageFlag() {
		return BranchBlockageFlag;
	}
	
//	/**
//	 * sets the route direction flag.
//	 * @param direction is true when the route enters the RouteEdge
//	 * for a RouteEdge in an adjacent track and false when the
//	 * route exits the track through the associated RouteEdge
//	 */
//	public void setEntryFlag(final boolean direction) {
//		EntryFlag = direction;
//	}
//	
//	/**
//	 * returns a flag indicating that the associated RouteEdge is entered
//	 * from the approach direction or exited from the track that it is an edge.
//	 * @return return true if it is an entry edge and false if it is an exit edge.
//	 */
//	public boolean getEntryFlag() {
//		return EntryFlag;
//	}
//	
	/**
	 * records the CPEdge that the dispatcher selected to end the route
	 * @param destination is the target of the second mouse click, used in delimiting
	 * the end of the DAG search
	 */
	public void setExit(final CPEdge destination) {
		Destination = destination;
	}
	
	/**
	 * disconnects a CPEdge RouteNode from its attached CPEdge
	 * - breaks the linkage
	 * - changes the status
	 * - examines the chain of RouteNodes from RouteAnchor to end and
	 * if all have been released, recycles the DAG.
	 */
	public void releaseEdge() {
		Edge = null;
		NodeStatus = RouteNodeStatus.COMPLETE;
		checkRouteCompletion();
	}

	/********************************************************************************
	 * The next set of methods operate on a chain.  They are invoked after the "best"
	 * path through the DAG has been discovered
	 ********************************************************************************/
	/**
	 * walks the route from the RouteAnchor to the end, checking
	 * the status of the nodes associated with CPEdges.  If all have
	 * been released, then the DAG is recycled.
	 * @return true if the route is complete and false if not
	 */
	public boolean checkRouteCompletion() {
//		System.out.println("checkRouteCompletion on thread " + Thread.currentThread().getName());
		for (RouteNode node = RouteAnchor; node != null; node = node.ControlNode) {
			if (node.Edge != null) {
				return false;
			}			
		}
//		System.out.println("Removing Nodes");
		RouteAnchor.removeNodes(null);
//		System.out.println("Done removing DAG nodes");
		return true;
	}
	
	/**
	 * walks the preferred route alignment (chain) from here to the next CPEdge or route end,
	 * looking for FrogEdges.  It counts the FrogEdges that are not
	 * lined to the preferred route.  It has a prerequisite that the route is known
	 * before being invoked.
	 * @return number of points that have to be moved to set the preferred route
	 */
	public int countMislinedPoints() {
		int misLined = 0;
		for (RouteNode node = this.NextNode; (node != null) && (node.Edge != null) && (!(node.Edge instanceof CPEdge));
				node = node.NextNode) {
			if ((node.Edge instanceof FrogEdge)) {
				if (((FrogEdge) node.Edge).isFoulingEdge()) {
					++misLined;
				}
			}
		}
		return misLined;
	}
	
	/**
	 * walks the preferred route alignment (chain) from here to the next CPEdge or route end,
	 * looking for FrogEdges.  It counts the FrogEdges that are not
	 * lined to the preferred route and commands the PtsEdge to line to the FrogEdge.
	 * It has a prerequisite that the route is known before being invoked.
	 * @return number of points that have been commanded to move.
	 */
	public int setPoints() {
		int misLined = 0;
		for (RouteNode node = this.NextNode; (node != null) && (node.Edge != null) && (!(node.Edge instanceof CPEdge));
				node = node.NextNode) {
			if (node instanceof RouteNodePts) {
				if (!((RouteNodePts) node).prepareNode()) {
					++misLined;
				}
			}
		}
		return misLined;
	}
	
	/**
	 * walks the preferred route alignment (chain) from here to the next CPEdge or route end,
	 * looking for PtsEdges.  It asks each PtsEdge if occupancy is stable (i.e. there have
	 * been no new occupancy reports since the last query).  The query does not end at the
	 * first unstable PtsRoute, but continues so that the occupancy counts on all PtsEdges
	 * are refershed. It has a prerequisite that the
	 * route (chain) is known before being invoked.
	 * @return true if no PtsEdges recorded a recent occupancy report.
	 */
	public boolean checkStability() {
		boolean stable = true;
		for (RouteNode node = this.NextNode; (node != null) && (node.Edge != null) && (!(node.Edge instanceof CPEdge));
				node = node.NextNode) {
			if (node instanceof RouteNodePts) {
				stable &= ((RouteNodePts) node).isStable();
			}
		}
		return stable;
	}
	
	/**************************************************************************
	 * the nest set of methods operate primarily on stacked nodes
	 **************************************************************************/
	
	/**
	 * changes the NodeStatus value to reflect what the Node is doing
	 * @param status is the new status of all nodes in teh DAG
	 */
	public void setDAGStatus(final RouteNodeStatus status) {
		int id = ++VisitGeneration;
		visitCommon(id, null, new SetNodeStatus(status));
	}
	

	/**************************************************************************
	 * a method for walking a DAG or chain.  turnouts complicate
	 * the walk because there are multiple, diverging paths, which eventually
	 * join.  Each non-branching segment will be traversed by visitCommon().
	 * Points facing in a fanout will be visited by visitPoints.
	 **************************************************************************/
	
	/**
	 * visits all the RouteNodes between this node and endNode, performing the command on them
	 * @param marker a search identifier
	 * @param endNode the node at which walking ends.  It is not included in the walk.
	 * @param command the operation to perform on the nodes.
	 */
	protected void visitCommon(final int marker, final RouteNode endNode, final VisitorCommand command) {
		RouteNode node = this;
		RouteNode next;
		while ((node != endNode) && (node != null)) {
			if (node.VisitID == marker) {
				break;
			}
			node.VisitID = marker;
			if (node.Edge instanceof FrogEdge) {
				next = node.NextNode;
				command.executeCommand(node);
				if ((next != endNode) && (next != null)) {
					if (next.VisitID == marker) {
						break;
					}
					next.VisitID = marker;
					node = next.NextNode;
					command.executeCommand(next);
				}
				else {
					node = null;
				}
			}
			else if (node.Edge instanceof PtsEdge) {
				((RouteNodePts) node).visitPoints(marker, endNode, command);
				node = null;
			}
			else {
				next = node.NextNode;
				command.executeCommand(node);
				node = next;
			}
		}
	}
	
	/**************************************************************************
	 * the next group of methods are for dumping the contents of a DAG or
	 * chain.
	 **************************************************************************/
	/**
	 * starts up a traversal of the DAG from this node
	 */
	public void dumpDAG() {
		++VisitGeneration;
		traverseDAG(1);
	}
	
	/**
	 * this method is for testing and viewing the DAG formed from
	 * the CPEdge search.  It prints information about this RouteNode
	 * @param nodeCount is an incrementing integer for locating parallel paths
	 * @param nodeType is a String identifying the type of the node
	 */
	public void dumpNode(final int nodeCount, final String nodeType) {
		System.out.println(Integer.toString(nodeCount) + ": " + nodeType + " " + ((Edge == null) ? "null" : Edge.toString()) + " Blockages=" + Integer.toString(AdvanceBlockageCount) + 
				" Complexity=" + Integer.toString(ComplexityCount) + " Diverging=" + Integer.toString(DivergingCount) +
				" Block flag=" + BranchBlockageFlag + " Route Number=" + Integer.toString(RouteNumber) + " Traversal Number=" + Integer.toString(VisitID));
	}
	
	/**
	 * dumps the contents of this node, then figures out how to dump the contents of the
	 * next node in the DAG.
	 * @param nodeCount is an incrementing integer for locating parallel paths
	 */
	private void traverseDAG(final int nodeCount) {
		if (VisitID == VisitGeneration) {
			System.out.println("Revisiting traversed node :" + Edge.toString());
		}
		else {
			VisitID = VisitGeneration;
			if (Edge instanceof CPEdge) {
				dumpNode(nodeCount, "CPEdge");
				if (NextNode == null) {
					System.out.println("End of DAG");
				}
				else {
					NextNode.traverseDAG(nodeCount + 1);
				}
			}
			else if (Edge instanceof FrogEdge) {
				dumpNode(nodeCount, "FrogEdge " + ((FrogEdge) Edge).getIndex());
				if (NextNode == null) {
					System.out.println("Unexpected broken DAG");
				}
				else {
					NextNode.commonTraversal(nodeCount + 1);
				}
			}
			else if (Edge instanceof PtsEdge) {
				dumpNode(nodeCount, "PtsEdge ");
				if (NextNode == null) {
					System.out.println("Unexpected broken DAG");
				}
				else {
					((RouteNodePts) this).dumpPointsNode(nodeCount + 1);
				}
			}
			else {
				System.out.println(Integer.toString(nodeCount) + ": Unknown Node Type");
			}
		}
	}
	
	/**
	 * dumps the contents of this, then figures out how to dump the contents of the
	 * next node in the DAG.  The analysis of the next node is not type dependent.
	 * @param nodeCount is an incrementing integer for locating parallel paths
	 */
	protected void commonTraversal(final int nodeCount) {
		if (VisitID == VisitGeneration) {
			System.out.println("Revisiting traversed node: " + Edge.toString());
		}
		else {
			VisitID = VisitGeneration;
			if (Edge instanceof CPEdge) {
				dumpNode(nodeCount, "CPEdge");
			}
			else if (Edge instanceof FrogEdge) {
				dumpNode(nodeCount, "FrogEdge " + ((FrogEdge) Edge).getIndex());
			}
			else if (Edge instanceof PtsEdge) {
				dumpNode(nodeCount, "PtsEdge ");
			}
			else {
				System.out.println(Integer.toString(nodeCount) + ": Unknown Node Type");
			}
			if (NextNode == null) {
				System.out.println("End of DAG");
			}
			else {
				NextNode.traverseDAG(nodeCount + 1);
			}
		}
	}
	
	/**
	 * "allocates" a new RouteNode from the list of recycled RouteNodes.  If the list is
	 * empty one is created through the Java new() operation.
	 * @param edge is the RouteEdge associated with the node
	 * @param search identifies the CTCRoute
	 * @param next is the next node for the next RouteEdge in advance
	 * @return a clean RouteNode
	 */
	static public RouteNode newNode(final RouteEdge edge, final int search, final RouteNode next) {
		RouteNode node;
		synchronized(Lock) {
			if (FreeList == null) {
				++NodeCount;
				return new RouteNode(edge, search, next);
			}
			node = FreeList;
			FreeList = FreeList.NextNode;
		}
		node.initializeNode(edge, search, next);
		return node;
	}
	
	/***************************************************************************
	 * the following methods are used in conjunction with reclaiming RouteNodes
	 * from a DAG or chain
	 ***************************************************************************/
	
	/**
	 * sets up to remove one or more RouteNodes from a DAG.
	 * 
	 * This is the starting point of the walk.  It need not be the anchor.
	 * The method visits all the RouteNodes between this node and endNode and deletes them.
	 * Because there are parallel paths in a DAG and this node may be on one,
	 * the way the algorithm works is to mark all nodes from endNode to the end
	 * with a unique number.  The marked nodes are not to be deleted.  The second step is
	 * walk the nodes from this node to any of
	 * 	- the end
	 *  - the endNode
	 *  - a node whose mark is the same as the current mark
	 * @param endNode is where to stop removal.  If it is null, then
	 * all nodes in advance from this node are removed.  If endMode is not null,
	 * then removal will stop with it, but it will not be removed.
	 */
	public void removeNodes(final RouteNode endNode) {
		int id = ++VisitGeneration;
		if (endNode != null) {
			endNode.visitCommon(id, null, new MarkNode());
		}
		visitCommon(id, endNode, new RecycleGarbage());
		if (Crandic.Details.get(DebugBits.ROUTENODEBIT)) {
			System.out.println(NodeCount + " RouteNodes created and " + availableNodes() + " available nodes.  " +
					RouteNodePts.getNodeCount() + " RouteNodePts created and " + RouteNodePts.availablePtsNodes() +
					" available nodes");
		}
	}
	
	/**
	 * removes this RouteNode from the DAG
	 */
	protected void unLinkNode() {
		if (NextNode != null) {
			NextNode.removeLinks(this);
			NextNode = null;
		}
		if (PreviousNode != null) {
			PreviousNode.removeLinks(this);
			PreviousNode = null;
		}
		ControlNode = null;
		if (Edge != null ) {  // this should never be called as the links were broken after the search
			Edge.releaseNode(this);
		}
	}
	
	/**
	 * removes the linkages from this RouteNode back to a RouteNode
	 * @param jetison is the RouteNode whose links are being removed
	 */
	protected void removeLinks(final RouteNode jetison) {
		if (NextNode == jetison) {
			NextNode = null;
		}
		if (PreviousNode == jetison) {
			PreviousNode = null;
		}
	}
	
	/**
	 * adds the RouteNode back to the free list.  All of its links to other
	 * objects should be nulled out before invoking this method.
	 */
	protected void recycleNode() {
		if (Crandic.Details.get(DebugBits.ROUTENODEBIT) && (Edge != null)) {
			System.out.println("Recycling " + Edge.toString());
		}
		synchronized(Lock) {
			NextNode = FreeList;
			FreeList = this;
		}
	}
	
	/**
	 * removes the search link from the Edges to the DAG so that
	 * the DAG is preserved and a new search can be created.
	 */
	public void disconnectSearchNodes() {
		int id = ++VisitGeneration;
		visitCommon(id, null, new UnlinkSearchEdge());
	}
	
	/**
	 * counts the number of RouteNodes on the free list
	 * @return the number of nodes on the free list
	 */
	static public int availableNodes() {
		int count = 0;
		for (RouteNode node = FreeList; node != null; node = node.NextNode) {
			++count;
		}
		return count;
	}
	
	@Override
	public String toString() {
		String part1;
		String part2;
		if (this == RouteAnchor) {
			part1 = (Edge instanceof CPEdge) ? ((CPEdge) Edge).getSignalName() :
				"unnamed";
			part2 = (Destination == null) ? "unnamed" : Destination.getSignalName();
			return new String(part1 + " to " + part2);
		}
		else if (Edge != null) {
			return Edge.toString();
		}
		return "unknown";
	}
	
	/**************************************************************************
	 * The following methods are involved with stacking/unstacking routes
	 *************************************************************************/
	
	/**
	 * a method that handles the paper work for stacking a DAG.  Two operations must
	 * be performed on each node in the DAG:
	 * 1. the call back on the Edge must be incremented so that it will tell the
	 * 	Stack Machine when the Edge is unblocked
	 * 2. the Edge's index must be set in the edge index BitSet so that the
	 * 	Stack Machine can identify overlapping DAGS
	 * @param register is true to increment the Edge counter of DAGS that it
	 * is a member of and false to decrement the counter
	 * @param nodeIndex is the BitSet containing all Edges in the DAG
	 */
	public void registerEdge(final boolean register, BitSet nodeIndex) {
		int id = ++VisitGeneration;
		visitCommon(id, null, new RegisterStackedEdge(register, nodeIndex));
	}
	
	/**
	 * a recursive routine to traverse the DAG calculating the blocking conditions
	 * downstream from this node
	 * @param seqNum is the search number for identifying previous visits
	 */
	protected void calculateBlockages(final int seqNum) {
		RouteNodePts pNode;
//		Edge.registerStackNode(true);
		if (NextNode == null) {
			AdvanceBlockageCount = (Edge.isRouteSegmentBlocked(GuiLocks.ExtendedRouteBlock)) ? 1 : 0;
			ComplexityCount =
					DivergingCount = 0;
		}
		else {
			if (VisitID < seqNum) {
				VisitID = seqNum;
				if (NextNode instanceof RouteNodePts) {
					((RouteNodePts) NextNode).calculatePtsBlockages(seqNum);
				}
				else if (NextNode.Edge instanceof FrogEdge) {
					pNode = (RouteNodePts) NextNode.NextNode;
					pNode.calculateBlockages(seqNum);
					if (((PtsEdge) pNode.Edge).isRoutable((FrogEdge) NextNode.Edge)) {
						NextNode.AdvanceBlockageCount = pNode.AdvanceBlockageCount;
					}
					else {
						NextNode.AdvanceBlockageCount = pNode.AdvanceBlockageCount + 1;
					}
					NextNode.ComplexityCount = pNode.ComplexityCount;
					if (((FrogEdge) NextNode.Edge).isFoulingEdge()) {
						++NextNode.ComplexityCount;
					}
					NextNode.DivergingCount = pNode.DivergingCount;
					if (!((PtsEdge) pNode.Edge).isNormalFrog((FrogEdge) NextNode.Edge)) {
						++NextNode.DivergingCount;
					}
				}
				else {
					NextNode.calculateBlockages(seqNum);
				}
				AdvanceBlockageCount = NextNode.getBlockageCount();
				if (Edge.isRouteSegmentBlocked(GuiLocks.ExtendedRouteBlock)) {
					++AdvanceBlockageCount;
				}
				ComplexityCount = NextNode.getComplexity();
				DivergingCount = NextNode.getDivergingCount();
			}
			// else been there, so do nothing
		}
		if (Crandic.Details.get(DebugBits.ROUTENODEBIT)) {
			System.out.printf("%d Route Node %s: blockages = %d diverges = %d complexity = %d%n",
					VisitID, toString(), AdvanceBlockageCount, DivergingCount, ComplexityCount);
		}
	}
	
	/**
	 * a trigger on the anchor for the active stacked route.  It is invoked
	 * by a RouteEdge when a potentially blocking condition clears.  The
	 * result is this RouteNode walks the DAG, looking for a clear path.
	 * @return true if a clear path can be found and false if not.
	 */
	public boolean routeCheck() {
		String result;
		calculateBlockages(++VisitGeneration);
		if (Crandic.Details.get(DebugBits.ROUTENODEBIT)) {
			result = AdvanceBlockageCount == 0 ? "clear" : "blocked";
			System.out.printf("Route Node %s is %s%n%n", toString(), result);
		}

		return (AdvanceBlockageCount == 0);
	}
	
	/**************************************************************************
	 * the following interface and classes implement the command design pattern.
	 * The command design pattern encapsulates an action as an object 
	 * so that it can be passed around
	 **************************************************************************/
	
	/**************************************************************************
	 * the interface to which all commands must adhere
	 **************************************************************************/
	protected interface VisitorCommand {
		/**
		 * performs the action encapsulated by the command
		 * @param node is the RouteNode to perform the action on
		 * @return true if the walking is complete (for this branch) and false
		 * if it should continue
		 */
		public boolean executeCommand(final RouteNode node);
	}

	/**************************************************************************
	 * a command to mark a node by setting the CurrentSearch count.  The framework
	 * methods do this automatically, so this class does nothing.
	 **************************************************************************/
	protected class MarkNode implements VisitorCommand {
		
		@Override
		public boolean executeCommand(final RouteNode node) {
			return false;
		}
	}
	
	/**************************************************************************
	 * a command to unlink a node from any chain that it may be in and add
	 * it to the free list
	 **************************************************************************/
	protected class RecycleGarbage implements VisitorCommand {
		
		@Override
		public boolean executeCommand(final RouteNode node) {
			node.unLinkNode();
			node.recycleNode();
			return false;
		}
	}
	
	/**************************************************************************
	 * a command to unlink a node from the Edge search link, freeing the
	 * link for another search.
	 **************************************************************************/
	protected class UnlinkSearchEdge implements VisitorCommand {
		
		@Override
		public boolean executeCommand(final RouteNode node) {
			if (node.Edge != null) {
				node.Edge.releaseNode(node);
			}
			return false;
		}
	}
	
	/**************************************************************************
	 * a command to inform Edges on the DAG that they need to tell the Stack Machine when a
	 * blocking condition on the Edge clears. The Stack Machine then searches all DAGs
	 * for a clear path.  The Edge is also informed when the DAG is
	 * transformed into N/X or cancelled.  I think that only opposing edges need to
	 * register and this algorithm does both opposing and direction of travel, which
	 * is redundant; the problem is that DOT is not kept in the DAG.
	 **************************************************************************/
	protected class RegisterStackedEdge implements VisitorCommand {

		/**
		 * the anchor of the waiting route
		 */
		private final boolean Stacked;
		
		/**
		 * the nodes that could be involved in a Route.  It is initially empty, but is
		 * filled in by each node in the DAG.
		 */
		private BitSet NodeSet;
		
		/**
		 * the ctor
		 * @param stacked is a flag set to true when the Edge is in the pending DAG
		 * and false when the DAG is transformed into N/X or cancelled.
		 */
		public RegisterStackedEdge(final boolean stacked, BitSet nodes) {
			Stacked = stacked;
			NodeSet = nodes;
		}
		
		@Override
		public boolean executeCommand(RouteNode node) {
			if (node.Edge != null) {
				node.Edge.registerStackNode(Stacked, NodeSet);
			}
			return false;
		}
		
	}

	/**************************************************************************
	 * a command to set the NodeStatus on all nodes in the DAG
	 **************************************************************************/
	protected class SetNodeStatus implements VisitorCommand {

		/**
		 * the anchor of the waiting route
		 */
		private final RouteNodeStatus Status;
		
		/**
		 * the ctor
		 * @param stacked is a flag set to true when the Edge is in the pending DAG
		 * and false when the DAG is transformed into N/X or cancelled.
		 */
		public SetNodeStatus(final RouteNodeStatus status) {
			Status = status;
		}
		
		@Override
		public boolean executeCommand(RouteNode node) {
			node.setNodeStatus(Status);
			return false;
		}
	}
}
/* @(#)RouteNode.java */

