/* Name: CPEdge.java
 *
 * What:
 * This file contains the definition for a Traffic Stick Relay
 * (TSR).  In the prototype, a TSR is a mechanical relay that drops to
 * indicate which direction a train is moving through a block.  It is the
 * thing that makes APB work.  In the GUI, each block edge has a TSR.  The
 * TSR assists in tracking the movement of train labels.
 */
package cats.layout;

/**
 * This file contains the definition for a Traffic Stick Relay
 * (TSR).  In the prototype, a TSR is a mechanical relay that drops to
 * indicate which direction a train is moving through a block.  It is the
 * thing that makes APB work.  In the GUI, each block edge has a TSR.  The
 * TSR assists in tracking the movement of train labels.
 * <p>Title: CATS - Crandic Automated Traffic System</p>
 * <p>Description: A program for dispatching trains on Pat Lana's
 * Crandic model railroad.
 * <p>Copyright: Copyright (c) 2014, 2015, 2016, 2018</p>
 * <p>Company: </p>
 * @author Rodney Black
 * @version $Revision$
 */
public class TrafficStickRelay {
	/**
	 * the possible values for the Stick Relay:
	 * ENTRYEDGE means that a train is expected to enter/has entered protected track through this VitalLogic
	 * EXITEDGE means a train is expected to exit/has exited protected track through this vital logic
	 * NODIRECTION means a train is not expected to cross the VitalLogic
	 * In Chubb's algorithms, the StickValue has only two states: NODIRECTION and ENYTRYEDGE;
	 * thus, this could be a boolean.
	 */
	public enum StickValue {ENTRYEDGE, EXITEDGE, NODIRECTION};
	
	/**
	 * the value of the traffic stick for this edge
	 */
	private StickValue MyStick = StickValue.NODIRECTION;

	/**
	 * the value of the traffic stick for the neighboredge
	 */
	private StickValue OtherStick = StickValue.NODIRECTION;
	
	/**
	 * is called when the block containing the edge is occupied.
	 * This forces an execution of the state machine.
	 */
	public void setOccupy() {
		if (OtherStick == StickValue.EXITEDGE) {
			MyStick = StickValue.ENTRYEDGE;
		}
		else {
			MyStick = StickValue.EXITEDGE;
		}
	}
	
	/**
	 * is called whenever occupancy is cleared.  In any state,
	 * it sets my direction to none.
	 */
	public void clearOccupy() {
		MyStick = StickValue.NODIRECTION;
	}
	
	public void setApproach() {
		if (MyStick == StickValue.EXITEDGE) {
			OtherStick = StickValue.ENTRYEDGE;
			// set traffic stick here
		}
		else {
			OtherStick = StickValue.EXITEDGE;
		}
		
	}
	
	public void clearApproach() {
		OtherStick = StickValue.NODIRECTION;
		// clear traffic stick here
	}
	
	/**
	 * In the literature, a traffic stick is set, when it is determined
	 * that a train has entered a block through the edge containing the
	 * traffic stick; thus, though the stick appears to have three values,
	 * only two are actually used.  This predicate reduces the three values
	 * to two - entry and not entry.
	 * @return true if the value is entry.
	 */
	public boolean isStickSet() {
		return MyStick == StickValue.ENTRYEDGE;
	}
	
	/**
	 * the same predicate as above, but for the neighbor's traffic stick
	 * @return true if the neighbor's traffic stick is set.  It is queried
	 * only when the neighbor's occupancy status changes.
	 */
	public boolean isOtherStickSet() {
		return OtherStick == StickValue.ENTRYEDGE;
	}

}
